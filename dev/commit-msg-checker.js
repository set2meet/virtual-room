/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/**
 * Validate git message with angularjs recommended style
 * (https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md)
 * https://github.com/angular/angular.js/blob/master/validate-commit-msg.js
 */

const util = require('util');
const fs = require('fs');

const PATTERN = /^(\[\w+-\d+])\s(\w*)\s(\(([\w\s\$\.\,\*/-]*)\))\: (.*)$/gim;
const TYPES = {
  build: true,
  ci: true,
  docs: true,
  feat: true,
  fix: true,
  perf: true,
  refactor: true,
  revert: true,
  style: true,
  test: true,
};

const error = function () {
  console.error('\x1b[31m INVALID COMMIT MSG: ' + util.format.apply(null, arguments) + '\x1b[0m \n');
};

const message = fs.readFileSync('./.git/COMMIT_EDITMSG', 'utf8');
let isValid = true;
const match = PATTERN.exec(message);

if (!match) {
  error('does not match "[JIRATICKET-NUMBER] <type> (<scope>): <subject>" ! \nwas: ' + message);
  isValid = false;
} else {
  var ticket = match[1];
  var type = match[2];
  var scope = match[4];
  var subject = match[5] || '';

  if (ticket === '') {
    error('JIRA Ticket must be non-empty!');
    isValid = false;
  }

  if (!TYPES.hasOwnProperty(type)) {
    error('"%s" is not allowed type !', type);
    isValid = false;
  }

  if (scope === '') {
    error('Scope must be non-empty!');
    isValid = false;
  }

  if (subject === '') {
    error('Subject must be non-empty!');
    isValid = false;
  }
}

if (message.indexOf('Merge branch ') === 0 || message.indexOf('Merge remote-tracking branch') === 0) {
  isValid = true;
}

if (!isValid) {
  process.exit(1);
} else {
  process.exit(0);
}
