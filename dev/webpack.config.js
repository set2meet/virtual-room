/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin').TsconfigPathsPlugin;
const LicensePlugin = require('webpack-license-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { WebpackBundleSizeLimitPlugin } = require('webpack-bundle-size-limit-plugin');
const getMyIp = require('get-my-ip');
const BASE_DIR = '../src';
const OUTPUT_DIR = '../dist';
const BINARY_FILE_MAX_SIZE = 8192;
const IP = getMyIp();
const readConfig = require('env-config').readConfig;

const isStorybook = process.env.NODE_ENV === 'storybook';
const isTest = process.env.NODE_ENV === 'test';
const isProduction = process.env.NODE_ENV === 'production';

module.exports = async function () {
  const config = await readConfig();

  const moduleLoadersSet = {
    font: {
      test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)/,
      loader: 'url-loader',
    },
    css: {
      test: /\.(css)$/,
      loaders: ['style-loader', 'css-loader'],
    },
    url: {
      test: /\.(png|jpg|jpeg|gif|svg)$/,
      loader: 'url-loader',
      query: {
        limit: BINARY_FILE_MAX_SIZE,
        name: 'images/[name].[ext]?[hash]',
      },
    },
    file: {
      test: /\.(mp3|mp4|ogg|wav)$/,
      loader: 'file-loader',
      options: {
        name: 'sounds/[name].[ext]',
      },
    },
    ts: {
      test: /\.tsx?$/,
      use: [
        {
          loader: 'cache-loader',
        },
        {
          loader: 'ts-loader',
          options: {
            happyPackMode: false,
            configFile: 'tsconfig.json',
            transpileOnly: true,
          },
        },
      ],
    },
    html: {
      test: /\.html$/i,
      loader: 'html-loader',
    },

    /**
     * Returns required for env specific loaders
     * @return []
     */
    get production() {
      return [this.html, this.css, this.file, this.font, this.ts, this.url];
    },

    /**
     * Returns required for env specific loaders
     * @return []
     */
    get development() {
      return [this.html, this.css, this.file, this.font, this.ts, this.url];
    },

    /**
     * Returns required for env specific loaders
     * @return []
     */
    get storybook() {
      return [this.html, this.file, this.font, this.ts, this.url];
    },

    get test() {
      return [this.html, this.css, this.file, this.font, this.ts, this.url];
    },
  };

  if (!isProduction && !isTest && !config.httpsOptions) {
    console.log('Error: not found "httpsOptions" in config');
    process.exit(1);
  }

  /**
   * Basic webpack config common for all envs
   */
  const webpackConfig = {
    mode: 'development',

    entry: [
      path.resolve(__dirname, BASE_DIR, 'client/index.tsx'),
      'react-hot-loader/patch',
      // activate HMR for React
      `webpack-dev-server/client?https://${IP}:8080`,
      // bundle the client for webpack dev server
      // and connect to the provided endpoint
      'webpack/hot/only-dev-server',
      // bundle the client for hot reloading
      // only- means to only hot reload for successful updates
    ],
    output: {
      filename: 'vr-bundle.js',
      chunkFilename: '[name].chunk.js',
      path: path.resolve(__dirname, OUTPUT_DIR, 'public'),
      publicPath: '/',
    },

    devtool: 'inline-source-map',

    plugins: [
      new ForkTsCheckerWebpackPlugin(),
      new WebpackBundleSizeLimitPlugin({
        include: [
          'CompositionRoot.chunk.js',
          'AuthProvider.chunk.js',
          'routerModule.chunk.js',
          'roomModule.chunk.js',
          'appModule.chunk.js',
          'Header.chunk.js',
          'vr-bundle.js',
        ],
      }),
    ],
    module: {
      rules: [],
    },

    watchOptions: {
      ignored: /node_modules/,
    },

    resolve: {
      plugins: [new TsconfigPathsPlugin({ configFile: 'tsconfig.json' })],
      extensions: ['.tsx', '.ts', '.js'],
      modules: [path.join(__dirname, BASE_DIR), 'node_modules'],
      alias: {
        'styled-components': path.resolve(__dirname, '../node_modules', 'styled-components'),
      },
    },
    externals: {
      '@opentok/client': 'OT',
    },
  };

  const getTestConfigProperties = () => ({
    externals: {
      config: {
        utils: {
          toObject: '',
        },
      },
    },
    plugins: [...webpackConfig.plugins],
  });

  /**
   * WebpackConfig overloads for specific env
   */
  let defaultConfig;

  /**
   * Required for production modules n loaders
   */
  if (isProduction) {
    const webpackProductionConfig = {
      mode: 'production',
      entry: [path.resolve(__dirname, BASE_DIR, 'client/index.tsx')],
      devServer: {},
      devtool: 'source-map',
      watchOptions: {},
      plugins: [
        new webpack.NamedChunksPlugin(),
        new HtmlWebpackPlugin({
          hash: true,
          template: path.resolve(__dirname, BASE_DIR, 'client/assets/index.html'),
        }),
        new LicensePlugin({
          outputFilename: 'virtual-room.oss-licenses.json',
          licenseOverrides: {
            'component-indexof@0.0.3': 'MIT',
            'component-bind@1.0.0': 'MIT',
            'component-inherit@0.0.3': 'MIT',
            'indexof@0.0.1': 'MIT',
            'typed-function@1.1.0': 'MIT',
            '@opentok/client@2.18.1': 'MIT',
            'url-template@2.0.8': 'BSD-3-Clause',
            '@s2m/resource-management@1.13.0': 'MIT',
            '@s2m/recording-db@1.13.0': 'MIT',
            '@s2m/logger@1.13.0': 'MIT',
          },
        }),
        ...webpackConfig.plugins,
      ],
      optimization: {
        minimize: true,
        minimizer: [
          new TerserPlugin({
            parallel: true,
            extractComments: {
              condition: /^\**!|@preserve|@license|@cc_on/i,
              filename: (file) => {
                return `${file.filename}.LICENSE`;
              },
              banner: (licenseFile) => {
                return `License information can be found in ${licenseFile} and virtual-room.oss-licenses.json`;
              },
            },
          }),
        ],
      },
      module: {
        rules: moduleLoadersSet.production,
      },
    };

    defaultConfig = { ...webpackConfig, ...webpackProductionConfig };
  } else if (isStorybook) {
    /**
     * Required for storybook modules n loaders
     */
    const webpackStorybookConfig = {
      module: {
        rules: moduleLoadersSet.storybook,
      },
      ...getTestConfigProperties(),
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_CONFIG': JSON.stringify(config.cleanupSecrets()),
        }),
      ],
    };

    defaultConfig = { ...webpackConfig, ...webpackStorybookConfig };
  } else if (isTest) {
    /**
     * Required for tests
     */
    const webpackTestConfig = {
      module: {
        rules: moduleLoadersSet.test,
      },
      ...getTestConfigProperties(),
    };

    defaultConfig = { ...webpackConfig, ...webpackTestConfig };
  } else {
    /**
     * Development config
     */
    const httpsOptions = Object.assign({}, config.httpsOptions); // by some unknown reason webpack trying to mutate read-only config object and then falls

    // https://github.com/auth0/node-jsonwebtoken/issues/642#issuecomment-585173594
    httpsOptions.cert = httpsOptions.cert.replace(/\\n/gm, '\n');
    httpsOptions.key = httpsOptions.key.replace(/\\n/gm, '\n');

    const webpackDevConfig = {
      plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
          'process.env': {
            IP: `"${IP}"`,
          },
        }),
        ...webpackConfig.plugins,
      ],
      devServer: {
        contentBase: path.join(__dirname, '../dist/public'),
        hot: true,
        host: IP,
        https: httpsOptions,
        historyApiFallback: true,
      },
      module: {
        rules: moduleLoadersSet.development,
      },
    };
    defaultConfig = { ...webpackConfig, ...webpackDevConfig };
  }

  return defaultConfig;
};
