/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const fs = require('fs-extra');

function getFilesFromPath(path, outputFile) {
  const files = fs.readdirSync(path);
  const licenceFile = fs.readJsonSync(outputFile);
  let result = [];

  files.forEach((el) => {
    let inputJSONFile;

    try {
      inputJSONFile = fs.readJsonSync(`${path}${el}`);
    } catch (e) {
      inputJSONFile = [];
    }
    result = [...result, ...inputJSONFile];
  });

  fs.writeFileSync(outputFile, JSON.stringify([...licenceFile, ...result]));
}

function aggregateLicense() {
  const args = process.argv.slice(2);

  switch (args[0]) {
    case '-p':
      getFilesFromPath(args[1], args[3]);
      break;
    default:
      console.log('Please use -p and specify path to the folder with files');
  }
}

aggregateLicense();
