/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
var fs = require('fs');
var http = require('http');
var path = require('path');

const port = 8008;

const mimeType = {
  '.js': 'text/javascript',
  '.css': 'text/css',
  '.json': 'application/json',
  '.png': 'image/png',
  '.jpg': 'image/jpg',
  default: 'text/html',
};

http
  .createServer((request, response) => {
    var filePath = request.url;

    if (filePath === '/') {
      filePath = '/index.html';
    }

    filePath = path.join(__dirname, filePath);

    var extName = path.extname(filePath);
    var contentType = mimeType[extName] || mimeType.default;

    fs.readFile(filePath, (error, content) => {
      if (error) {
        if (error.code == 'ENOENT') {
          fs.readFile('./404.html', (error, content) => {
            response.writeHead(200, { 'Content-Type': mimeType.default });
            response.end(content, 'utf-8');
          });
        } else {
          response.writeHead(500);
          response.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n');
          response.end();
        }
      } else {
        response.writeHead(200, { 'Content-Type': contentType });
        response.end(content, 'utf-8');
      }
    });
  })
  .listen(port);

console.log('...Creating simple http server on http://localhost:' + port);
