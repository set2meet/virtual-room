/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const fs = require('fs');
const get = require('lodash/get');
const readline = require('readline');

const graphHeight = 200; // in px
const volumeAmpStep = 400; // in ms
const graphCoordScaleY = graphHeight / 100;
const graphTickHeight = 20; // in px

const vadSequenceData = {
  owner: {
    displayName: '',
    startTime: NaN,
    timing: [],
  },
  peers: {},
};

const processError = (message) => {
  console.log(message);
  process.exit(1);
};

const addVadSequence = (json) => {
  const { message, time } = json;
  const from = get(json, 'meta.user.id');
  const isMessageFromTarget = from === userId;
  let dataTarget = isMessageFromTarget ? vadSequenceData.owner : vadSequenceData.peers[from];

  // if peer record not exists - create one
  if (!isMessageFromTarget && !dataTarget) {
    dataTarget = vadSequenceData.peers[from] = {
      startTime: NaN,
      timing: [],
    };
  }

  if (!dataTarget.displayName) {
    dataTarget.displayName = get(json, 'meta.user.name');
  }

  // vad sequence user b19b9bd9-d392-3a1b-7960-cec2121412fd track a0...: 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  const sequence = message.split(':')[1].split(',');
  const endTime = new Date(time).valueOf();
  const startTime = dataTarget.startTime;
  const len = sequence.length;
  let inxStop = NaN;

  dataTarget.startTime = endTime;

  for (let i = len; i--; ) {
    const strValue = sequence[i];
    const numValue = parseInt(strValue, 10);

    if (isNaN(numValue)) {
      inxStop = i;
      break;
    } else {
      dataTarget.timing.push({
        value: numValue,
        time: endTime - i * volumeAmpStep,
      });
    }
  }

  if (isNaN(inxStop)) {
    return;
  }

  if (isNaN(startTime)) {
    return;
  }

  for (let i = 0; i <= inxStop; i++) {
    const numValue = parseInt(sequence[i], 10);

    if (isNaN(numValue)) {
      dataTarget.timing.push({
        value: numValue,
        time: startTime + i * volumeAmpStep,
      });
      break;
    } else {
      dataTarget.timing.push({
        value: numValue,
        time: startTime + i * volumeAmpStep,
      });
    }
  }
};

const sortSequence = (a, b) => {
  return a.time - b.time;
};

const sortVarSequence = () => {
  vadSequenceData.owner.timing.sort(sortSequence);

  Object.keys(vadSequenceData.peers).forEach((peer) => {
    vadSequenceData.peers[peer].timing.sort(sortSequence);
  });
};

const defineTimeRange = () => {
  let minTime = +Infinity;
  let maxTime = -Infinity;

  const update = (timing) => {
    minTime = Math.min(minTime, timing[0].time);
    maxTime = Math.max(maxTime, timing[timing.length - 1].time);
  };

  update(vadSequenceData.owner.timing);

  Object.keys(vadSequenceData.peers).forEach((peer) => {
    update(vadSequenceData.peers[peer].timing);
  });

  vadSequenceData.range = { minTime, maxTime };
};

const htmlCreateCheckbox = () => {};

const htmlCreateGraphic = (id, name, timing, range) => {
  const width = (range.maxTime - range.minTime) / volumeAmpStep;
  const len = timing.length;
  const polygons = [];
  let seg = [];

  for (let i = 0; i < len; i++) {
    if (isNaN(timing[i].value)) {
      polygons.push(seg);
      seg = [];
    } else {
      seg.push(timing[i]);
    }
  }

  polygons.push(seg);

  // 1 tick = 1 minute
  const ticksLen = Math.floor((width * volumeAmpStep) / (1000 * 60));
  const tickDx = (1000 * 60) / volumeAmpStep;
  const ticks = new Array(ticksLen).fill(1);

  const coordX = (rec) => {
    return (rec.time - range.minTime) / volumeAmpStep;
  };

  const coordY = (rec) => {
    return graphHeight - graphCoordScaleY * rec.value;
  };

  return `
    <div id="${id}" class="graph-box">
      <div class="display-name">${name}${id === userId ? ' (publisher)' : ''}</div>
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="${graphHeight}" width="${width}">
      ${polygons
        .map(
          (seg) => `
        <path d="${seg
          .map((rec, i) => {
            const coords = ` ${coordX(rec)},${coordY(rec)}`;
            return i === 0 ? `M ${coords} L` : coords;
          })
          .join('')}"
        />`
        )
        .join('')}
      </svg>
      <div class="ticks" style="width: ${width}px">
      ${ticks.map((v, i) => `<span class="tick" style="left: ${(i + 1) * tickDx}px">${i + 1} min</span>`).join('')}
      </div>
    </div>
  `;
};

const createGraphicsAndSaveToFile = () => {
  const { maxTime, minTime } = vadSequenceData.range;
  const width = (maxTime - minTime) / volumeAmpStep;

  const outputStream = fs.createWriteStream(`vad-graphic-for-${userId}.html`);

  outputStream.write('<!DOCTYPE html>\n\n<html>\n<head>\n');
  outputStream.write('  <style>');
  outputStream.write(`
    #choose {
      margin-bottom: 16px;
      border-bottom: 2px solid grey;
    }

    #graphics {
      position: relative;
    }

    .graph-box {
      border-left: 1px solid #000;
      border-bottom: 1px solid #000;
      margin-bottom: 26px;
      position: relative;
      height: ${graphHeight}px;
    }

    .graph-box > svg > path {
      fill: none;
      stroke: red;
      stroke-width: 1;
    }

    .display-name {
      padding-left: 10px;
      position: absolute;
      color: grey;
      font-size: 14px;
      font-family: Arial;
      position: fixed;
    }

    .ticks {
      left: 0px;
      position: absolute;
      bottom: -${graphTickHeight}px;
    }

    .ticks > .tick {
      width: 1px;
      height: 100%;
      position: relative;
      top: 0;
      overflow: visible;
      display: inline-block;
      white-space: nowrap;
      font-size: 12px;
      padding-left: 4px;
      border-left: 1px solid grey;
    }
  `);
  outputStream.write('</style>\n');
  outputStream.write('</head>\n');
  outputStream.write('<body>\n');
  outputStream.write(`  <div id="choose" style="width: ${width}px">\n`);

  outputStream.write('  </div>\n');
  outputStream.write(`  <div id="graphics" style="width: ${width}px">\n`);

  outputStream.write(
    htmlCreateGraphic(userId, vadSequenceData.owner.displayName, vadSequenceData.owner.timing, vadSequenceData.range)
  );

  Object.keys(vadSequenceData.peers).forEach((peer) => {
    outputStream.write(
      htmlCreateGraphic(
        peer,
        vadSequenceData.peers[peer].displayName,
        vadSequenceData.peers[peer].timing,
        vadSequenceData.range
      )
    );
  });

  outputStream.write('  </div>\n');
  outputStream.write('</body>');
  outputStream.write('\n</html>');

  outputStream.close();
};

const args = process.argv;
const params = ['-user', '-session', '-input'];
const paramsInx_user = args.indexOf(params[0]);
const paramsInx_session = args.indexOf(params[1]);
const paramsInx_input = args.indexOf(params[2]);

if (paramsInx_user === -1) {
  processError('Please specify user id with param: -user USER-ID');
}

if (paramsInx_session === -1) {
  processError('Please specify session id with param: -session SESSION-ID');
}

if (paramsInx_input === -1) {
  processError('Please specify input file param: -input FILE/PATH');
}

const userId = process.argv[paramsInx_user + 1];
const sessionId = process.argv[paramsInx_session + 1];
const inputFile = process.argv[paramsInx_input + 1];

if (!userId || params.indexOf(userId) > -1) {
  processError('Possible wrong userId param');
}

if (!sessionId || params.indexOf(sessionId) > -1) {
  processError('Possible wrong sessionId param');
}

if (!inputFile || params.indexOf(inputFile) > -1) {
  processError('Possible wrong input file param');
}

const rl = readline.createInterface({
  input: fs.createReadStream(inputFile),
});

rl.on('line', (line) => {
  const json = JSON.parse(line);
  const sid = get(json, 'meta.sessionId');

  if (sid !== sessionId) {
    return;
  }

  if (json.message.indexOf('vad sequence user') === -1) {
    return;
  }

  if (json.message.indexOf(userId) === -1) {
    return;
  }

  addVadSequence(json);
});

rl.on('close', () => {
  sortVarSequence();
  defineTimeRange();
  createGraphicsAndSaveToFile();
});

console.log('Start');
