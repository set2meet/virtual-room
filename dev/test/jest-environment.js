/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const JSDOMEnvironment = require('jest-environment-jsdom');
const OpenTokMock = require('./__mocks__/@opentok/client');

class EnzymeEnvironment extends JSDOMEnvironment {
  constructor(config, context) {
    super(config, context);
    const testEnvironmentOptions = config.testEnvironmentOptions || {};
    this.global.enzymeAdapterDescriptor = testEnvironmentOptions.enzymeAdapter;
    this.global.bootstrapEnzymeEnvironment = false;

    this.global.jsdom = this.dom;
    this.global.OT = OpenTokMock;
  }

  async teardown() {
    this.global.jsdom = null;

    return super.teardown();
  }
}

module.exports = EnzymeEnvironment;
