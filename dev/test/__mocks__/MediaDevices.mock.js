/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/*
https://github.com/jsdom/jsdom/issues/2654
https://jestjs.io/docs/en/manual-mocks#mocking-methods-which-are-not-implemented-in-jsdom
*/
Object.defineProperty(window.navigator, 'mediaDevices', {
  writable: true,
  value: {
    ondevicechange: null,
    enumerateDevices: jest.fn().mockImplementation((query) => Promise.resolve(query)),
    getSupportedConstraints: jest.fn(),
    getUserMedia: jest.fn().mockImplementation((constraints) => Promise.reject(Error('NotFoundError'))),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
  },
});
