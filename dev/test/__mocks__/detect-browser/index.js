/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// for "detect-browser": "^3.0.1"
const { getNodeVersion, detectOS, parseUserAgent } = require('detect-browser');

function mockDetect() {
  // because detect-browser doesn't support jsdom
  // and usual detect() returns null
  if (typeof navigator !== 'undefined') {
    return mockParseUserAgent(navigator.userAgent);
  }

  return getNodeVersion();
}

function mockDetectOS(userAgentString) {
  // JsDom default User Agent:
  // `Mozilla/5.0 (${process.platform || "unknown OS"}) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/${jsdomVersion}`
  //
  // TODO: should check that function works correctly on different OS @p-mazhnik
  // Generally it is better to use our own copy of the 'detect-browser' package,
  // because even the latest version (5.1.1) doesn't have support for the 'jsdom' environment.
  // Or just make the pr's to original 'detect-browser'.
  // @p-mazhnik
  const winRule = {
    name: 'Windows',
    rule: /(win32)|(win64)/,
  };
  if (winRule.rule.test(userAgentString)) {
    return winRule.name;
  }
  const linuxRuleExtra = {
    name: 'Linux',
    rule: /(linux)/,
  };
  if (linuxRuleExtra.rule.test(userAgentString)) {
    return linuxRuleExtra.name;
  }

  return detectOS(userAgentString) || '';
}

function mockParseUserAgent(userAgentString) {
  if (!userAgentString) {
    return null;
  }

  // add rule for jsdom
  const jsdomRule = {
    name: 'jsdom',
    rule: /jsdom\/([0-9\.]+)/,
  };
  const match = jsdomRule.rule.exec(userAgentString);
  if (match) {
    const version = match[1].split(/[._]/).slice(0, 3);
    return {
      name: jsdomRule.name,
      version: version.join('.'),
      os: mockDetectOS(userAgentString),
    };
  }

  return parseUserAgent(userAgentString);
}

module.exports = {
  detect: mockDetect,
  detectOS: mockDetectOS,
  getNodeVersion: getNodeVersion,
  parseUserAgent: mockParseUserAgent,
};
