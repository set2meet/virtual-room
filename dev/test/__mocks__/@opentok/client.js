/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

/**
 * Mock @opentock package because currently we can't use external package in tests
 */
const OT = {
  checkScreenSharingCapability(callback) {},
  checkSystemRequirements() {},
  getDevices(callback) {},
  setProxyUrl(proxyUrl) {},
  getSupportedCodecs() {},
  getUserMedia(properties) {},
  initPublisher() {},
  log(message) {},
  off(eventName, callback, context) {},
  on(eventName, callback, context) {},
  once(eventName, callback, context) {},
  initSession(partnerId, sessionId, options) {},
  registerScreenSharingExtension(kind, id, version) {},
  reportIssue(callback) {},
  setLogLevel(level) {},
  upgradeSystemRequirements() {},
  unblockAudio() {},

  // classes
  OTEventEmitter() {},
  Session() {},
  Subscriber() {},
  Publisher() {},
  Connection() {},
  Stream() {},

  /**
   * Mock this function to fix issue with jest and jsdom
   * If you want to expand functionality, see https://github.com/pjchender/react-use-opentok/tree/master/src/__mocks__
   * Similar issue: https://github.com/opentok/opentok-web-samples/issues/100
   * @p-mazhnik
   */
  initSession: function () {
    return {
      on: jest.fn(),
      off: jest.fn(),
      connect: jest.fn(),
      disconnect: jest.fn(),
    };
  },
};

module.exports = OT;
