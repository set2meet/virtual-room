/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const fs = require('fs');
const path = require('path');
const { requireUncached } = require('./utils');
const mockExec = jest.fn().mockResolvedValue({ stdout: 'ok' });
const mockExecWithError = jest.fn().mockRejectedValueOnce(new Error('coverage error'));

function setupPromisifyExecMock(mockExec) {
  jest.resetModules();
  jest.unmock('util');
  jest.doMock('util', () => ({
    promisify: jest.fn().mockImplementation(() => {
      return mockExec;
    }),
    inspect: jest.fn(),
  }));
}

setupPromisifyExecMock(mockExec);

const { checkCoverage } = require('./check-coverage');

describe('CheckCoverageTest', () => {
  const oldReportPath = './123.old.json';
  const newReportPath = './123.json';
  const oldReportAbsolutePath = path.resolve(__dirname, '../../../', oldReportPath);
  const newReportAbsolutePath = path.resolve(__dirname, '../../../', newReportPath);

  const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
  const mockConsoleError = jest.spyOn(console, 'error').mockImplementation(() => {});
  const mockWriteFile = jest.spyOn(fs.promises, 'writeFile').mockImplementation(() => {});
  const prevReport = {
    total: {
      lines: { pct: 1.0 },
      statements: { pct: 1.0 },
      functions: { pct: 1.0 },
      branches: { pct: 1.0 },
    },
  };
  jest.mock(oldReportAbsolutePath, () => prevReport, { virtual: true });

  const newReportGood = {
    total: {
      ...prevReport.total,
      lines: { pct: 2.0 },
    },
  };
  jest.mock(newReportAbsolutePath, () => newReportGood, { virtual: true });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should run test coverage and add to commit', async (done) => {
    await checkCoverage(newReportPath, oldReportPath, 0, true);
    expect(mockWriteFile).toHaveBeenCalledWith(
      oldReportAbsolutePath,
      JSON.stringify({ total: newReportGood.total }, null, 2)
    );
    expect(mockExec).toHaveBeenCalledWith(`git add ${oldReportAbsolutePath}`);
    expect(mockExit).toHaveBeenCalledWith(0);
    done();
  });

  it('shouldn t add to commit', async (done) => {
    await checkCoverage(newReportPath, oldReportPath, 0, false);
    expect(mockExec).not.toHaveBeenCalledWith(`git add ${oldReportPath}`);
    expect(mockExit).toHaveBeenCalledWith(0);
    done();
  });

  it('should exit on decrease', async (done) => {
    const newReportBad = {
      total: {
        ...prevReport.total,
        lines: { pct: 0.5 },
      },
    };
    jest.unmock(newReportAbsolutePath);
    jest.resetModules();
    jest.mock(newReportAbsolutePath, () => newReportBad, { virtual: true });
    await checkCoverage(newReportPath, oldReportPath);
    expect(mockConsoleError).toHaveBeenNthCalledWith(1, expect.stringContaining('decreased test coverage percentage'));
    expect(mockExit).toHaveBeenCalledWith(1);
    jest.mock(newReportAbsolutePath, () => newReportGood, { virtual: true });
    done();
  });

  it('shouldn t exit on small decrease in one property and high increase in other', async (done) => {
    const newReport = {
      total: {
        ...prevReport.total,
        lines: { pct: 0.7 },
        branches: { pct: 1.5 },
      },
    };
    jest.unmock(newReportAbsolutePath);
    jest.resetModules();
    jest.mock(newReportAbsolutePath, () => newReport, { virtual: true });
    await checkCoverage(newReportPath, oldReportPath);
    expect(mockWriteFile).toHaveBeenCalledWith(
      oldReportAbsolutePath,
      JSON.stringify({ total: newReport.total }, null, 2)
    );
    jest.mock(newReportAbsolutePath, () => newReportGood, { virtual: true });
    done();
  });

  it('should exit on wrong `accept` argument', async (done) => {
    await checkCoverage(newReportPath, oldReportPath, '123a');
    expect(mockConsoleError).toHaveBeenNthCalledWith(1, expect.stringContaining('(0, 1)'));
    expect(mockExit).toHaveBeenCalledWith(1);
    done();
  });

  it('should exit on wrong reports paths', async (done) => {
    await checkCoverage(undefined, oldReportPath);
    expect(mockConsoleError).toHaveBeenNthCalledWith(1, expect.stringContaining('provide paths to json reports'));
    expect(mockExit).toHaveBeenCalledWith(1);
    done();
  });

  it('should handle exec errors', async (done) => {
    setupPromisifyExecMock(mockExecWithError);
    const { checkCoverage } = requireUncached('./check-coverage');
    await checkCoverage(newReportPath, oldReportPath, 0, true);
    expect(mockExecWithError).toHaveBeenCalledWith(`git add ${oldReportAbsolutePath}`);
    expect(mockConsoleError).toHaveBeenCalledWith('test coverage error: coverage error');
    expect(mockExit).toHaveBeenCalledWith(1);
    done();
  });
});
