/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const path = require('path');
const util = require('util');
const { promises: fs } = require('fs');
const exec = util.promisify(require('child_process').exec);
const jsonDiff = require('json-diff');

function handleError(msg) {
  console.error(`test coverage error: ${msg}`);
  process.exit(1);
}

/**
 * @param acceptableError
 * @param newReportPath
 * @param oldReportPath
 * @param addToGit
 * @returns {Promise<void>}
 */
async function checkCoverage(newReportPath, oldReportPath, acceptableError = 0.0, addToGit = false) {
  try {
    acceptableError = parseFloat(acceptableError);
    addToGit = addToGit === 'true' || addToGit === true;
    if (isNaN(acceptableError) || typeof acceptableError !== 'number' || acceptableError > 1 || acceptableError < 0) {
      throw Error('acceptable error should be a number in range(0, 1)');
    }
    if (!newReportPath || !oldReportPath) {
      throw Error('please, provide paths to json reports');
    }
    const oldReportAbsolutePath = path.resolve(__dirname, '../../../', oldReportPath);
    const newReportAbsolutePath = path.resolve(__dirname, '../../../', newReportPath);
    const newReport = require(newReportAbsolutePath);
    const prevReport = require(oldReportAbsolutePath);

    const {
      total: {
        branches: { pct: newBranchesPct },
        lines: { pct: newLinesPct },
        functions: { pct: newFunctionsPct },
        statements: { pct: newStatementsPct },
      },
    } = newReport;

    const {
      total: {
        branches: { pct: branchesPct },
        lines: { pct: linesPct },
        functions: { pct: functionsPct },
        statements: { pct: statementsPct },
      },
    } = prevReport;
    const newCoverageSummary = (newBranchesPct + newLinesPct + newFunctionsPct + newStatementsPct) / 4;
    const oldCoverageSummary = (branchesPct + linesPct + functionsPct + statementsPct) / 4;

    if (newCoverageSummary < (1 - acceptableError) * oldCoverageSummary) {
      throw Error(
        `decreased test coverage percentage:\n` +
          `summary - new: ${newCoverageSummary}, old: ${oldCoverageSummary},\n` +
          `diff -${jsonDiff.diffString(prevReport.total, newReport.total)}`
      );
    }
    // All is fine, save new report and add to commit
    await fs.writeFile(oldReportAbsolutePath, JSON.stringify({ total: newReport.total }, null, 2));
    if (addToGit) {
      await exec(`git add ${oldReportAbsolutePath}`);
    }
    process.exit(0);
  } catch (e) {
    handleError(e.message);
  }
}

module.exports = {
  checkCoverage,
  handleError,
};
