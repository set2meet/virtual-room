/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/**
 * Usage example:
 * npm run check-coverage --
 * --oldReport './dev/test/coverage/coverage-summary.old.json'
 * --newReport './dev/test/coverage/coverage-summary.json'
 */
const { Command } = require('commander');
const { checkCoverage, handleError } = require('./check-coverage');

const program = new Command();

program
  .version('1.0.0')
  .description(
    "Script for checking coverage report during pre-commit. Should prevent new changes which isn't covered with tests (decreased test coverage percentage / level)"
  )
  .option('--oldReport <string>', 'Path to old report')
  .option('--newReport <string>', 'Path to new report')
  .option('--git <boolean>', 'call `git add` on old report. Default: false')
  .option(
    '-a, --accept <float>',
    'defines the acceptable reduction in test coverage. Should be a float in range(0, 1) Default: 0'
  );

program.parse(process.argv);

checkCoverage(program.newReport, program.oldReport, program.accept, program.git).catch((e) => {
  handleError(e.toString());
});
