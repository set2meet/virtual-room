/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const { parseUserAgent, detect } = require('detect-browser');

describe('detect-browser-mock', () => {
  it('jsdom win32 useragent', () => {
    const parsed = parseUserAgent('Mozilla/5.0 (win32) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0');
    expect(parsed).toEqual({ name: 'jsdom', version: '16.4.0', os: 'Windows' });
  });
  it('jsdom unknown useragent', () => {
    const parsed = parseUserAgent('Mozilla/5.0 (unknown OS) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0');
    expect(parsed).toEqual({ name: 'jsdom', version: '16.4.0', os: '' });
  });
  it('jsdom random os useragent', () => {
    const parsed = parseUserAgent('Mozilla/5.0 (1234) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0');
    expect(parsed).toEqual({ name: 'jsdom', version: '16.4.0', os: '' });
  });
  it('jsdom linux os useragent', () => {
    const parsed = parseUserAgent('Mozilla/5.0 (linux) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0');
    expect(parsed).toEqual({ name: 'jsdom', version: '16.4.0', os: 'Linux' });
  })
});
