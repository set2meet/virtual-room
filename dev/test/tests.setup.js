/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
require('jest-enzyme');
require('jest-styled-components');
require('jest-canvas-mock');

const { configure } = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

// Setup mocks
require('jest-fetch-mock').enableMocks();

// https://github.com/jsdom/jsdom/issues/1721
if (typeof window.URL.createObjectURL === 'undefined') {
  Object.defineProperty(window.URL, 'createObjectURL', { value: jest.fn(), configurable: true, writable: true });
}
if (typeof window.URL.revokeObjectURL === 'undefined') {
  Object.defineProperty(window.URL, 'revokeObjectURL', { value: jest.fn(), configurable: true, writable: true });
}

require('./__mocks__/AudioContext.mock');
require('./__mocks__/MediaDevices.mock');
require('./__mocks__/MediaStream.mock');

global.requestAnimationFrame = function (callback) {
  return setTimeout(callback, 0);
};
global.cancelAnimationFrame = function (id) {
  clearTimeout(id);
};

const dateformat = require('dateformat');

jest.mock('dateformat', () => (date, mask, utc, gmt) => dateformat(date, mask, true, gmt));

// end mocks setup

configure({ adapter: new Adapter() });
