/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const path = require('path');

module.exports = {
  rootDir: path.resolve(__dirname, '../../'),
  roots: ['<rootDir>/src', '<rootDir>/dev/test', '<rootDir>/.storybook'],
  preset: 'ts-jest',
  transform: {
    '^.+\\.html?$': 'html-loader-jest',
  },
  setupFilesAfterEnv: ['<rootDir>/dev/test/tests.setup.js'],
  testEnvironment: '<rootDir>/dev/test/jest-environment.js',
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/dev/test/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/dev/test/__mocks__/styleMock.js',
  },
  modulePaths: ['<rootDir>'],
  coverageDirectory: '<rootDir>/dev/test/coverage',
  reporters: ['default', ['jest-junit', { outputDirectory: '<rootDir>/dev/test/' }]],
  coverageReporters: ['json-summary', 'text-summary', 'cobertura', 'lcov'],
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '<rootDir>/.storybook/**/*.{js,jsx,ts,tsx}',
    '!<rootDir>/dev/test/coverage/**',
    '!**/node_modules/**',
    '!**/dist/**',
  ],
  testTimeout: 20000,
};
