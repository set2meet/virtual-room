/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const need_version = 12;
const cur_version = parseFloat(process.versions.node);

if (cur_version < need_version) {
  throw new Error(`Need node with version >= ${need_version} (current: ${cur_version})\n`);
}
