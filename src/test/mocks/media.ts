/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// tslint:disable-next-line:no-magic-numbers
export const getFakeVideoDevicesInfo = (count: 1 | 2 | 3 = 1): MediaDeviceInfo[] => {
  const inputVideo1 = {
    deviceId: 'video1',
    groupId: '1',
    kind: 'videoinput',
    label: 'Default Camera 1',
  };
  const inputVideo2 = {
    deviceId: 'video2',
    groupId: '1',
    kind: 'videoinput',
    label: 'Camera 2',
  };
  const emptyLabel = {
    deviceId: 'video3',
    groupId: '1',
    kind: 'videoinput',
    label: '',
  };

  const devices: MediaDeviceInfo[] = [
    {
      ...inputVideo1,
      kind: 'videoinput',
      toJSON: () => inputVideo1,
    },
  ];

  if (count >= 2) {
    devices.push({
      ...inputVideo2,
      kind: 'videoinput',
      toJSON: () => inputVideo2,
    });
  }

  // tslint:disable-next-line:no-magic-numbers
  if (count >= 3) {
    devices.push({
      ...emptyLabel,
      kind: 'videoinput',
      toJSON: () => emptyLabel,
    });
  }

  return devices;
};
