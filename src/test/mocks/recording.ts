/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { RecordItemStatus, TRecordItem } from '@s2m/recording-db/lib/types';

export const buildRecordItem = ({ id, src, ...props }): TRecordItem => {
  const year = 2020;
  const month = 8;
  const date = 12;
  const other = 10;

  return {
    id,
    src,
    createdAt: new Date(Date.UTC(year, month, date, other, other, other, other)),
    createdBy: '',
    expiredAt: null,
    roomId: '',
    status: RecordItemStatus.processing,
    realmName: 'realmName1',
    duration: 0,
    size: '',
    resolution: '',
    subscribedTo: '',
    name: 'record name',
    type: '',
    ...props,
  };
};
