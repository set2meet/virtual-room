/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import {
  TAuthTokens,
  TRoomUser,
  TUser,
} from '../../ioc/client/providers/AuthProvider/types/client/AuthProvider.interfaces';
import { LoginStrategies } from '../../ioc/client/providers/AuthProvider/types/constants';

export const getUserWithTokens = (userProps?: Partial<TRoomUser>, tokensProps?: Partial<TAuthTokens>) => {
  const userWithTokens: TUser = {
    tokens: {
      accessToken: '',
      refreshToken: '',
      strategy: LoginStrategies.STRATEGY_FORM,
      ...tokensProps,
    },
    user: {
      id: '123',
      displayName: 'Pavel',
      email: 'test@epam.com',
      guest: false,
      roomId: '123',
      ...userProps,
    },
  };

  return userWithTokens;
};
