import React, { Component } from 'react';

class SampleAsyncLazyDeeper extends Component<any, any> {
  public render() {
    const { users } = this.props;

    return users.map((name) => {
      return <div key={name}>{name}</div>;
    });
  }
}

export default SampleAsyncLazyDeeper;
