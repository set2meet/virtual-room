import React from 'react';
import SampleAsync from './SampleAsync';

export default {
  title: 'SampleAsync (no-storyshots)',
  component: SampleAsync,
};

export const Default = () => <SampleAsync />;
