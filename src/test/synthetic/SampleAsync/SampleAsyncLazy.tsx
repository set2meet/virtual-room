import React, { Component, Suspense } from 'react';

const lazyTimeout = 3000;

const SampleAsyncLazyDeeper = React.lazy(
  () =>
    new Promise((resolve) =>
      // @ts-ignore
      setTimeout(() => resolve(import('./SampleAsyncLazyDeeper')), lazyTimeout)
    )
);

class SampleAsyncLazy extends Component<any, any> {
  public render() {
    return (
      <Suspense fallback={<div>Loading deeper...</div>}>
        <SampleAsyncLazyDeeper users={this.props.users} />
      </Suspense>
    );
  }
}

export default SampleAsyncLazy;
