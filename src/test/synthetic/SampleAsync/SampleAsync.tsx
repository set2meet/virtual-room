import React, { Component, Suspense } from 'react';
import users from './users';

const lazyTimeout = 3000;

const SampleAsyncLazy = React.lazy(
  () =>
    new Promise((resolve) =>
      // @ts-ignore
      setTimeout(() => resolve(import('./SampleAsyncLazy')), lazyTimeout)
    )
);

class SampleAsync extends Component<any, any> {
  constructor(props) {
    super(props);
  }

  public render() {
    return (
      <Suspense fallback={<div>Loading...</div>}>
        <SampleAsyncLazy users={users} />
      </Suspense>
    );
  }
}

export default SampleAsync;
