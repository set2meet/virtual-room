import React from 'react';
import LoadableSample from './LoadableSample';

export default {
  title: 'LoadableSample',
  component: LoadableSample,
};

export const Default = () => <LoadableSample />;
