import React, { Component } from 'react';
import users from './users';
import loadableWrapper from '../../../ioc/client/components/LoadableWrapper/loadableWrapper';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

/**
 * Simple endpoint of Wrapper for loadable-components
 */
class LoadableComponent extends Component {
  public render() {
    return this.props.children;
  }
}

const LoadableWrapper = loadableWrapper(
  () => clientEntityProvider
    .getUtils()
    .sleep()
    .then(() => ({ default: LoadableComponent }))
);
const textFallback = (text) => <h2>{text}</h2>;

class LoadableSample extends Component {
  public render() {
    return (
      <div style={{ display: 'flex' }}>
        <div
          style={{
            display: 'flex',
            width: '700px',
            justifyContent: 'space-between',
          }}
        >
          <LoadableWrapper fallback={textFallback('Loading Loadable 1...')}>
            <LoadableWrapper fallback={textFallback('Loading Loadable 2...')}>
              <LoadableWrapper fallback={textFallback('Loading Loadable 3...')}>
                <div>
                  {users.map((name) => {
                    return <div key={name}>{name}</div>;
                  })}
                </div>
              </LoadableWrapper>
            </LoadableWrapper>
            <LoadableWrapper fallback={textFallback('Loading Loadable Parallel 2...')}>
              <LoadableWrapper fallback={textFallback('Loading Loadable Parallel 3...')}>
                <div>
                  {users.map((name) => {
                    return <div key={name}>{name}</div>;
                  })}
                </div>
              </LoadableWrapper>
            </LoadableWrapper>
          </LoadableWrapper>
        </div>
      </div>
    );
  }
}

export default LoadableSample;
