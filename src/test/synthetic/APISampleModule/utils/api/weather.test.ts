/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { getWeather, WEATHER_LOCATION_ENDPOINT } from './weather';
import { HttpStatusCode } from '../../../../../types/httpStatusCode';
import { mockWeather } from '../mock/weather';

describe('weather api', () => {
  const mockAxios = new MockAdapter(axios);

  beforeEach(() => {
    mockAxios.reset();
  });

  it('should return `consolidated_weather` property from response', async () => {
    mockAxios
      .onGet(WEATHER_LOCATION_ENDPOINT)
      .reply(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather }));
    expect(await getWeather()).toEqual(mockWeather);
  });
});
