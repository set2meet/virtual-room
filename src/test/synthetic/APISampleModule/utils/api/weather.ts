/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import axios from 'axios';
export const WEATHER_ENDPOINT = 'http://www.metaweather.com';
const MOSCOW_WOEID = 2122265;

export interface IWeather {
  id: number;
  weather_state_name: string;
  weather_state_abbr: string;
  applicable_date: string;
}

interface IResponse {
  title: string;
  consolidated_weather: IWeather[];
}

export const WEATHER_LOCATION_ENDPOINT = `${WEATHER_ENDPOINT}/api/location/${MOSCOW_WOEID}/`;

export async function getWeather() {
  const response = await axios.get<IResponse>(WEATHER_LOCATION_ENDPOINT);

  return response.data.consolidated_weather;
}
