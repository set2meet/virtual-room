import React from 'react';
import { ClientEntityProviderContext } from '../../../../../ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import createAPISampleModule from '../../APISampleModule';
import { DynamicModuleLoader } from '../../../../../ioc/client/components/DynamicModuleLoader/DynamicModuleLoader';
import loadableWrapper from '../../../../../ioc/client/components/LoadableWrapper/loadableWrapper';

export const SampleModuleWrapper = ({ children }) => (
  <ClientEntityProviderContext.Consumer>
    {({ clientEntityProvider }) => {
      if (!clientEntityProvider) {
        return null;
      }
      const LoadableWrapper = loadableWrapper(async (props: { children: React.ReactNode }) => {
        const { reduxModules } = await createAPISampleModule(clientEntityProvider.getSocket(), 'sample');

        return {
          default: () => (
            <DynamicModuleLoader skipUnmountCleanup={true} modules={reduxModules}>
              {props.children}
            </DynamicModuleLoader>
          ),
        };
      });
      return <LoadableWrapper>{children}</LoadableWrapper>;
    }}
  </ClientEntityProviderContext.Consumer>
);

export const sampleModuleDecorator = (Story, context) => {
  return <SampleModuleWrapper>{Story(context)}</SampleModuleWrapper>;
};
