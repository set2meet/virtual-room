/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IWeather } from '../api/weather';

export const mockWeather: IWeather[] = [
  {
    id: 1,
    weather_state_abbr: 'c',
    weather_state_name: 'Clear',
    applicable_date: '',
  },
  {
    id: 2,
    weather_state_abbr: 'hc',
    weather_state_name: 'Heavy Cloud',
    applicable_date: '',
  },
];

export const mockWeather2: IWeather[] = [
  {
    id: 1,
    weather_state_abbr: 'sn',
    weather_state_name: 'Snow',
    applicable_date: '',
  },
  {
    id: 2,
    weather_state_abbr: 'sl',
    weather_state_name: 'Sleet',
    applicable_date: '',
  },
];
