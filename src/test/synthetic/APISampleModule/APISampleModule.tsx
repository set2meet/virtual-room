import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { sampleReducer } from './redux/reducer/reducer';
import WeatherPageConnected from './components/WeatherPage/WeatherPage';
import sampleMiddleware from './redux/middleware/middleware';
import Socket = SocketIOClient.Socket;

const createAPISampleModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'sample'
): Promise<IS2MModule> => {
  return {
    rootComponent: WeatherPageConnected,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: sampleReducer,
        },
        middlewares: [sampleMiddleware(stateKey)(socket)],
      },
    ],
  };
};

export default createAPISampleModule;
