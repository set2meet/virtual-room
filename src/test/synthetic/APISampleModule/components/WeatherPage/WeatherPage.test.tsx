import { loadAsyncTree, mountStory, setupStorybookForTest } from '../../../../utils/storybook';
import { enzymeCleanup, InitModulesWrapper, waitNextTick } from '../../../../utils/utils';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { TStore } from '../../../../../ioc/client/types/interfaces';
import { IStoreExt } from '../../../../utils/reduxMockEnhancer';
import { actionCreators } from '../../redux/actionCreators/actionCreators';
import EnzymeMountTracker from '../../../../utils/EnzymeMountTracker';
import React from 'react';
import WeatherPageConnected from './WeatherPage';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { WEATHER_LOCATION_ENDPOINT } from '../../utils/api/weather';
import { HttpStatusCode } from '../../../../../types/httpStatusCode';
import { mockWeather, mockWeather2 } from '../../utils/mock/weather';
import { SampleModuleWrapper } from '../../utils/storybook/sampleModuleDecorator';

describe('<WeatherPage />', () => {
  /**
   * Test interaction by mounting story
   */
  describe('Sample 1: stories-based ui test', () => {
    beforeAll(setupStorybookForTest());
    afterEach(enzymeCleanup);

    it('should fetch new data on update click', async (done) => {
      const wrapper = await mountStory('synthetic/WeatherPage', 'Connected', true);
      const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

      // Check that WeatherPage dispatch `fetchStart` on mount
      expect((store as TStore & IStoreExt).getActions()).toContainEqual(
        expect.objectContaining(actionCreators.fetchStart())
      );
      (store as TStore & IStoreExt).clearActions();

      wrapper.find('Button').simulate('click');
      await waitNextTick();

      // Check that WeatherPage dispatch `fetchStart` on button click
      expect((store as TStore & IStoreExt).getActions()).toContainEqual(
        expect.objectContaining(actionCreators.fetchStart())
      );

      done();
    });
  });
  /**
   * Test interaction by mounting component directly
   */
  describe('Sample 2: non-stories based ui test', () => {
    afterEach(enzymeCleanup);

    it('should fetch new data on update click', async (done) => {
      const mock = new MockAdapter(axios);

      mock
        .onGet(WEATHER_LOCATION_ENDPOINT)
        .replyOnce(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather }))
        .onGet(WEATHER_LOCATION_ENDPOINT)
        .reply(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather2 }));

      const wrapper = await loadAsyncTree(
        EnzymeMountTracker.mount(
          <InitModulesWrapper>
            <SampleModuleWrapper>
              <WeatherPageConnected />
            </SampleModuleWrapper>
          </InitModulesWrapper>
        )
      );

      wrapper.update();

      const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

      // Check that WeatherPage dispatch `fetchStart` on mount
      expect((store as TStore & IStoreExt).getActions()).toContainEqual(
        expect.objectContaining(actionCreators.fetchStart())
      );
      (store as TStore & IStoreExt).clearActions();

      wrapper.find('Button').simulate('click');
      // Check that WeatherPage dispatch `fetchStart` on button click
      expect((store as TStore & IStoreExt).getActions()).toContainEqual(
        expect.objectContaining(actionCreators.fetchStart())
      );

      done();
    });
  });
});
