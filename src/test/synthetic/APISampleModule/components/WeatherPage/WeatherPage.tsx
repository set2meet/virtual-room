import React from 'react';
import WeatherImage from '../Weather/WeatherImage';
import { IWeatherPageDispatchProps, TWeatherPageProps, TWeatherPageStateProps } from './WeatherPage.types';
import { WeatherLoadStatus } from '../../redux/WeatherLoadStatus';
import { WeatherPageStyled } from './WeatherPage.style';
import { connect } from 'react-redux';
import { actionCreators } from '../../redux/actionCreators/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { IButtonProps } from '../../../../../ioc/client/types/interfaces';

export class WeatherPage extends React.Component<TWeatherPageProps> {
  public componentDidMount() {
    this.props.onInit();
  }

  public render() {
    const { weatherArray, status, errorMsg } = this.props;

    if (!status || status === WeatherLoadStatus.LOADING || status === WeatherLoadStatus.INITIAL) {
      return <WeatherPageStyled>Loading...</WeatherPageStyled>;
    }

    if (status === WeatherLoadStatus.FAILURE) {
      return <WeatherPageStyled>{errorMsg}</WeatherPageStyled>;
    }
    const Button = clientEntityProvider.resolveComponentSuspended<IButtonProps>(ComponentRegistryKey.Button, <></>);
    const onUpdateButtonClick = () => this.props.onInit();

    return (
      <WeatherPageStyled>
        {weatherArray.map((obj) => {
          return (
            <WeatherImage key={obj.id} stateAbbreviation={obj.weather_state_abbr} stateName={obj.weather_state_name} />
          );
        })}
        <Button onClick={onUpdateButtonClick} title="Update" />
      </WeatherPageStyled>
    );
  }
}

export default connect<TWeatherPageStateProps, IWeatherPageDispatchProps>(
  (state: any) => ({
    weatherArray: state?.sample?.weatherArray,
    status: state?.sample?.status,
    errorMsg: state?.sample?.errorMsg,
  }),
  {
    onInit: actionCreators.fetchStart,
  }
)(WeatherPage);
