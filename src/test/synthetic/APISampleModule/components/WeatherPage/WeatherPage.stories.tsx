import React from 'react';
import ConnectedWeatherPage, { WeatherPage } from './WeatherPage';
import { action } from '@storybook/addon-actions';
import { WeatherLoadStatus } from '../../redux/WeatherLoadStatus';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { WEATHER_LOCATION_ENDPOINT } from '../../utils/api/weather';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { HttpStatusCode } from '../../../../../types/httpStatusCode';
import { TOnMockClientActionCallback } from '../../../../utils/serverSocket';
import { actionCreators } from '../../redux/actionCreators/actionCreators';
import { actionTypes } from '../../redux/actionTypes';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { sampleModuleDecorator } from '../../utils/storybook/sampleModuleDecorator';
import { mockWeather, mockWeather2 } from '../../utils/mock/weather';
import { select, text } from '@storybook/addon-knobs';
import { reduxMockEnhancer } from '../../../../utils/reduxMockEnhancer';
import { getStorybookIoCInitConfig } from '../../../../../../.storybook/common/utils/ioc/storybookInitConfig';
import withStoryRootIndicator from '../../../../../../.storybook/decorators/storyRootIndicator';

export default {
  title: 'synthetic/WeatherPage',
  component: WeatherPage,
};

const reduxDecorators = [connectDecorator(), withStoryRootIndicator, sampleModuleDecorator];

/**
 * Story contains component without redux, with custom properties
 */
export const Success = () => {
  return (
    <WeatherPage
      onInit={action('onInit')}
      weatherArray={mockWeather}
      errorMsg={text('errorMsg', null)}
      status={select('status', WeatherLoadStatus, WeatherLoadStatus.SUCCESS)}
    />
  );
};

/**
 * Story demonstrates api mocking, using `axios-mock-adapter`
 */
export const Connected = () => {
  const mock = new MockAdapter(axios);

  mock
    .onGet(WEATHER_LOCATION_ENDPOINT)
    .replyOnce(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather }))
    .onGet(WEATHER_LOCATION_ENDPOINT)
    .reply(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather2 }));
  return <ConnectedWeatherPage />;
};

export const WithNetworkError = () => {
  const mock = new MockAdapter(axios);

  mock.onGet(WEATHER_LOCATION_ENDPOINT).networkError();
  return <ConnectedWeatherPage />;
};

const reduxSettings = {
  decorators: reduxDecorators,
  parameters: {
    redux: {
      enable: true,
    },
  },
};

Connected.story = {
  ...reduxSettings,
};

WithNetworkError.story = {
  ...reduxSettings,
};

/**
 * Story demonstrates redux mocking, using `IoCInitializationOptions` parameter with `reduxMockEnhancer`
 */
export const Initial = () => {
  return <ConnectedWeatherPage />;
};

Initial.story = {
  ...reduxSettings,
  parameters: {
    ...reduxSettings.parameters,
    IoCInitializationOptions: {
      storeEnhancerFactory: () => [reduxMockEnhancer, ...getStorybookIoCInitConfig().storeEnhancerFactory()],
    },
  },
};

/**
 * Story demonstrates socket mocking, using `serverSocket` parameter
 */
export const WithSocket = (props) => {
  props.dispatch(clientEntityProvider.getAppActionCreators().connect());
  return <ConnectedWeatherPage />;
};

const onActionCallback: TOnMockClientActionCallback = (reduxAction, serverSocket) => {
  if (reduxAction.type === actionTypes.SAMPLE_WEATHER_LOADING) {
    const toClientAction = actionCreators.fetchFailure('error from socket');

    serverSocket.emit('action', {
      action: toClientAction,
    });
  }
};

WithSocket.story = {
  ...reduxSettings,
  parameters: {
    ...reduxSettings.parameters,
    serverSocket: {
      onActionCallback,
    },
  },
};
