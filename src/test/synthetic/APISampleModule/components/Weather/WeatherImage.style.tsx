import styled from 'styled-components';

export const WeatherImageStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  > img {
    margin: 1em;
  }
`;
