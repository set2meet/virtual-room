import React from 'react';
import WeatherImage from './WeatherImage';

export default {
  title: 'synthetic/WeatherImage',
  component: WeatherImage,
};

export const Default = () => <WeatherImage stateAbbreviation="c" stateName="Clear" />;
