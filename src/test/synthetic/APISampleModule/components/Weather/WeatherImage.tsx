import React from 'react';
import { WEATHER_ENDPOINT } from '../../utils/api/weather';
import { IWeatherProps } from './IWeatherProps';
import { WeatherImageStyled } from './WeatherImage.style';

class WeatherImage extends React.Component<IWeatherProps> {
  public render() {
    const { stateAbbreviation, stateName } = this.props;
    const tags = stateName.toLowerCase().replace(' ', ',');

    return (
      <WeatherImageStyled>
        <img src={`${WEATHER_ENDPOINT}/static/img/weather/png/64/${stateAbbreviation}.png`} alt={stateName} />
        <img src={`https://source.unsplash.com/320x180/?nature,weather,${tags}`} />
      </WeatherImageStyled>
    );
  }
}

export default WeatherImage;
