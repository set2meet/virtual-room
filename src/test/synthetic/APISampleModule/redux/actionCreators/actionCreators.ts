/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { actionTypes } from '../actionTypes';
import { IWeather } from '../../utils/api/weather';

export interface IFetchStartAction {
  type: typeof actionTypes.SAMPLE_WEATHER_LOADING;
}

export interface IFetchSuccessAction {
  type: typeof actionTypes.SAMPLE_WEATHER_SUCCESS;
  payload: IWeather[];
}

export interface IFetchFailureAction {
  type: typeof actionTypes.SAMPLE_WEATHER_FAILURE;
  payload: string;
}

export type TSampleActionCreators = IFetchSuccessAction | IFetchFailureAction | IFetchStartAction;

const fetchStart = (): IFetchStartAction => ({
  type: actionTypes.SAMPLE_WEATHER_LOADING,
});

const fetchSuccess = (weatherArray: IWeather[]): IFetchSuccessAction => ({
  type: actionTypes.SAMPLE_WEATHER_SUCCESS,
  payload: weatherArray,
});

const fetchFailure = (error: string): IFetchFailureAction => ({
  type: actionTypes.SAMPLE_WEATHER_FAILURE,
  payload: error,
});

export const actionCreators = {
  fetchStart,
  fetchSuccess,
  fetchFailure,
};
