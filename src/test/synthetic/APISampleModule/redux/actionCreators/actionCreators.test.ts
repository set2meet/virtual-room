/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { actionCreators } from './actionCreators';
import { actionTypes } from '../actionTypes';
import { mockWeather } from '../../utils/mock/weather';

/**
 * Here we perform simple Unit tests for our Action Creators
 */

describe('sample action creators', () => {
  it('fetchStart will return an action object of type SAMPLE_WEATHER_LOADING', () => {
    expect(actionCreators.fetchStart().type).toEqual(actionTypes.SAMPLE_WEATHER_LOADING);
  });
  it('fetchFailure', () => {
    expect(actionCreators.fetchFailure('error')).toEqual({
      type: actionTypes.SAMPLE_WEATHER_FAILURE,
      payload: 'error',
    });
  });
  it('fetchSuccess', () => {
    expect(actionCreators.fetchSuccess(mockWeather)).toEqual({
      type: actionTypes.SAMPLE_WEATHER_SUCCESS,
      payload: mockWeather,
    });
  });
});
