/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IApiSampleModuleInitialState } from '../IApiSampleModuleInitialState';
import { actionTypes } from '../actionTypes';
import { IFetchFailureAction, IFetchSuccessAction, TSampleActionCreators } from '../actionCreators/actionCreators';
import { WeatherLoadStatus } from '../WeatherLoadStatus';

export const INITIAL_STATE: IApiSampleModuleInitialState = {
  weatherArray: null,
  status: WeatherLoadStatus.INITIAL,
  errorMsg: null,
};

export function sampleReducer(state = INITIAL_STATE, action: TSampleActionCreators): IApiSampleModuleInitialState {
  switch (action.type) {
    case actionTypes.SAMPLE_WEATHER_LOADING:
      return {
        ...state,
        status: WeatherLoadStatus.LOADING,
        errorMsg: null,
      };
    case actionTypes.SAMPLE_WEATHER_SUCCESS:
      return {
        ...state,
        weatherArray: (action as IFetchSuccessAction).payload,
        status: WeatherLoadStatus.SUCCESS,
        errorMsg: null,
      };
    case actionTypes.SAMPLE_WEATHER_FAILURE:
      return {
        ...state,
        status: WeatherLoadStatus.FAILURE,
        errorMsg: (action as IFetchFailureAction).payload,
      };
    default:
      return state;
  }
}
