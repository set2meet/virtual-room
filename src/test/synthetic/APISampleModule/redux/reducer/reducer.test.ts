/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { sampleReducer } from './reducer';
import { actionCreators } from '../actionCreators/actionCreators';
import { IApiSampleModuleInitialState } from '../IApiSampleModuleInitialState';
import { WeatherLoadStatus } from '../WeatherLoadStatus';
import { IWeather } from '../../utils/api/weather';
import { mock } from 'jest-mock-extended';

describe('sample reducer', () => {
  it('shouldn t change state object and should fetch', () => {
    const state: IApiSampleModuleInitialState = {
      errorMsg: 'error',
      status: WeatherLoadStatus.FAILURE,
      weatherArray: null,
    };
    const stateCopy = {
      ...state,
    };

    expect(sampleReducer(state, actionCreators.fetchStart())).toEqual({
      status: WeatherLoadStatus.LOADING,
      errorMsg: null,
      weatherArray: null,
    });
    expect(stateCopy).toEqual(state);
  });
  it('shouldn t change state object + success', () => {
    const state: IApiSampleModuleInitialState = {
      errorMsg: 'error',
      status: WeatherLoadStatus.FAILURE,
      weatherArray: null,
    };
    const stateCopy = {
      ...state,
    };
    const weather = mock<IWeather[]>();

    expect(sampleReducer(state, actionCreators.fetchSuccess(weather))).toEqual({
      status: WeatherLoadStatus.SUCCESS,
      errorMsg: null,
      weatherArray: weather,
    });
    expect(stateCopy).toEqual(state);
  });
  it('shouldn t change state object + failure', () => {
    const weather = mock<IWeather[]>();
    const state: IApiSampleModuleInitialState = {
      errorMsg: null,
      status: WeatherLoadStatus.SUCCESS,
      weatherArray: weather,
    };
    const stateCopy = {
      ...state,
    };
    const error = 'error';

    expect(sampleReducer(state, actionCreators.fetchFailure(error))).toEqual({
      status: WeatherLoadStatus.FAILURE,
      errorMsg: error,
      weatherArray: weather,
    });
    expect(stateCopy).toEqual(state);
  });
  it('should return initial state by default', () => {
    expect(sampleReducer(undefined, { type: '123' })).toEqual({
      status: WeatherLoadStatus.INITIAL,
      errorMsg: null,
      weatherArray: null,
    });
  });
});
