/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IModuleReduxMiddleware } from '../../../../../ioc/common/ioc-interfaces';
import { mock } from 'jest-mock-extended';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;
import sampleMiddleware, { sampleMiddlewareEnum } from './middleware';
import { actionCreators } from '../actionCreators/actionCreators';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { WEATHER_LOCATION_ENDPOINT } from '../../utils/api/weather';
import { HttpStatusCode } from '../../../../../types/httpStatusCode';
import { mockWeather } from '../../utils/mock/weather';
import setupIoCClientForTest from '../../../../utils/ioc/testIoCClient';
import { AnyAction, Dispatch, Store } from 'redux';

const createMiddlewareInvoker = (
  testMiddleware: IModuleReduxMiddleware,
  socket: Socket = mock<Socket>(),
  store: Store = mock<Store>()
): {
  next: Dispatch;
  store: Store;
  invoke: (action: AnyAction) => any;
} => {
  const next = jest.fn();
  const middleware = testMiddleware('')(socket)(store)(next);

  const invoke = async (action) => {
    middleware(action);
    await clientEntityProvider.getUtils().sleep(10);
  };

  return { store, next, invoke };
};

beforeAll(setupIoCClientForTest());

describe('sample api module middleware', () => {
  const startAction = actionCreators.fetchStart();
  const mockAxios = new MockAdapter(axios);

  beforeEach(() => {
    mockAxios.reset();
  });

  it('should fetch weather data from api', async (done) => {
    const { store, next, invoke } = createMiddlewareInvoker(sampleMiddleware);

    mockAxios
      .onGet(WEATHER_LOCATION_ENDPOINT)
      .reply(HttpStatusCode.OK, JSON.stringify({ consolidated_weather: mockWeather }));

    await invoke(startAction);
    expect(store.dispatch).toHaveBeenNthCalledWith(1, actionCreators.fetchSuccess(mockWeather));
    expect(next).toHaveBeenNthCalledWith(1, startAction);
    done();
  });
  it('should fetch from socket if error and socket is active', async (done) => {
    const socket = mock<Socket>();

    socket.connected = true;
    const { store, next, invoke } = createMiddlewareInvoker(sampleMiddleware, socket);

    mockAxios.onGet(WEATHER_LOCATION_ENDPOINT).networkError();

    await invoke(startAction);
    expect(store.dispatch).toHaveBeenNthCalledWith(
      1,
      clientEntityProvider.getAppActionCreators().sendAction(actionCreators.fetchStart())
    );
    expect(next).toHaveBeenNthCalledWith(1, startAction);
    done();
  });
  it('should set error if socket is inactive', async (done) => {
    const socket = mock<Socket>();

    socket.connected = false;
    const { store, next, invoke } = createMiddlewareInvoker(sampleMiddleware, socket);

    mockAxios.onGet(WEATHER_LOCATION_ENDPOINT).reply(HttpStatusCode.NOT_FOUND);

    await invoke(startAction);
    expect(store.dispatch).toHaveBeenNthCalledWith(
      1,
      actionCreators.fetchFailure('Request failed with status code 404')
    );
    expect(next).toHaveBeenNthCalledWith(1, startAction);
    done();
  });
  /**
   * Here we check that actions that need to be processed in other middlewares
   * pass through our middleware successfully
   */
  it('should call next on actions not from current middleware', async (done) => {
    const { next, invoke } = createMiddlewareInvoker(sampleMiddleware);
    const action = { type: '123' };

    expect(sampleMiddlewareEnum[action.type]).toBeUndefined();

    await invoke(action);
    expect(next).toHaveBeenNthCalledWith(1, action);
    done();
  });
});
