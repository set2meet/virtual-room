/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../../ioc/common/ioc-interfaces';
import { AnyAction, Dispatch, Store } from 'redux';
import { actionTypes } from '../actionTypes';
import { getWeather, IWeather } from '../../utils/api/weather';
import { actionCreators } from '../actionCreators/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;

export const sampleMiddlewareEnum: IAppReduxEnumMiddleware = {
  async [actionTypes.SAMPLE_WEATHER_LOADING](store: Store, next, action, socket, storeKey) {
    try {
      const weather: IWeather[] = await getWeather();

      store.dispatch(actionCreators.fetchSuccess(weather));
    } catch (e) {
      // try fetching data from our server instead
      if (socket.connected) {
        store.dispatch(clientEntityProvider.getAppActionCreators().sendAction(actionCreators.fetchStart()));
      } else {
        store.dispatch(actionCreators.fetchFailure(e.message));
      }
    }
  },
};

const sampleMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => (
  next: Dispatch
) => (action: AnyAction) => {
  if (sampleMiddlewareEnum[action.type]) {
    sampleMiddlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default sampleMiddleware;
