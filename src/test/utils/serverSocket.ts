/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { Server } from 'mock-socket';
import resolveUrl from 'resolve-url';
import { AnyAction } from 'redux';
import { set as _set } from 'lodash';
import { Socket } from 'socket.io';
import { IAppConfigService } from '../../ioc/common/ioc-interfaces';

export type TOnMockClientActionCallback = (action: AnyAction, serverSocket: Socket) => void;

export const STORYBOOK_SOCKET_AUTHORIZED_ACTION = {
  type: 'STORYBOOK_SOCKET_AUTHORIZED',
};

export const createMockServerSocket = (
  serviceConfig: IAppConfigService,
  onActionCallback?: TOnMockClientActionCallback
): Server => {
  const remoteUrl = resolveUrl(serviceConfig.url, serviceConfig.path);
  const mockServer = new Server(remoteUrl);
  mockServer.on('connection', (clientSocket) => {
    const socket = (clientSocket as unknown) as Socket;
    socket.on('action', (action: AnyAction) => {
      _set(action, 'meta.clientSocketId', socket.id);
      if (action.type === STORYBOOK_SOCKET_AUTHORIZED_ACTION.type) {
        socket.emit('authorized');
      }
      if (onActionCallback) {
        onActionCallback(action, socket);
      }
    });
  });
  return mockServer;
};
