import { ClientApi } from '@storybook/addon-storyshots/dist/frameworks/Loader';
import { toRequireContext } from '@storybook/core/server';
import addons, { mockChannel } from '@storybook/addons';
import * as storybook from '@storybook/react';
import global from 'global';
import registerRequireContextHook from 'babel-plugin-require-context-hook/register';
import { raw } from '@storybook/react';
import { ReactWrapper } from 'enzyme';
import { RenderMainArgs } from '@storybook/react/dist/client/preview/types';
import EnzymeMountTracker from './EnzymeMountTracker';
import { TInitializationOptions } from '../../ioc/client/types/interfaces';

function getStoriesForKind(kind: string) {
  return raw().filter((story) => story.kind === kind);
}

export const getRawStory = (kind: string, name: string) => {
  // sometimes storybook adds spaces to camelCase titles e.g. DefaultWithTitle -> Default With Title
  return getStoriesForKind(kind).find(
    ({ name: currentName }) => currentName === name || currentName.replace(/ /g, '') === name
  );
};

export type TStoryStoreGetter = () => any;

export const mountStory = async (
  kind: string,
  name: string,
  waitForLazyResolution: boolean = true,
  IoCInitializationOptions?: Partial<TInitializationOptions> & { iocLoadComplete?: () => void }
): Promise<ReactWrapper> => {
  const story: RenderMainArgs = getRawStory(kind, name);
  const storyTree = EnzymeMountTracker.mount(story.storyFn({ IoCInitializationOptions }));

  if (!waitForLazyResolution) {
    return storyTree;
  } else {
    return await loadAsyncTree(storyTree);
  }
};

export const loadAsyncTree = (tree: ReactWrapper): Promise<ReactWrapper> => {
  return new Promise<ReactWrapper>((resolve, reject) => {
    const loadableContextWrapper = tree.find('ContextWrapper');

    if (loadableContextWrapper) {
      // Wait for the cascade rendering (case for the components with async import). See more in awaitTreeCascadePromises
      awaitTreeCascadePromises(loadableContextWrapper, () => resolve(tree.update()));
    } else {
      reject('Cant find LoadableContextWrapper in passed tree');
    }
  });
};

/**
 * This method get tree and wait for the all promises on the arr from state from LoadableContextWrapper and it's Context
 * When one bunch of Promises will be resolved this method compares old arr with new. If length are different then run
 * another instance of this function. If no - run callback function
 * @param tree - tree from Enzyme with state and loadablePromises arr in it
 * @param callback
 */
function awaitTreeCascadePromises(tree, callback) {
  const { loadablePromises } = tree.state();
  const loadablePromisesLength = loadablePromises.length;

  if (loadablePromisesLength !== 0) {
    const oldLength = loadablePromisesLength;

    Promise.all(loadablePromises).then(() => {
      setTimeout(() => {
        if (oldLength !== tree.state().loadablePromises.length) {
          awaitTreeCascadePromises(tree, callback);
        } else {
          callback();
        }
      }, 1);
    });
  } else {
    callback();
  }
}

export const configureStories = () => {
  registerRequireContextHook();
  addons.setChannel(mockChannel());
  // https://github.com/storybookjs/storybook/blob/v5.3.19/addons/storyshots/storyshots-core/src/frameworks/configure.ts

  require('../../../.storybook/preview');
  const { stories = [] } = require('../../../.storybook/main');
  const _stories = stories.map((pattern: string | { path: string; recursive: boolean; match: string }) => {
    const { path: basePath, recursive, match } = toRequireContext(pattern);
    // eslint-disable-next-line no-underscore-dangle
    return global.__requireContext('./.storybook', basePath, recursive, new RegExp(match.slice(1, -1)));
  });
  (storybook as ClientApi).configure(_stories, false);
};

export const setupStorybookForTest = () => async (done) => {
  configureStories();
  done();
};
