/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { IServerEntityProviderInternals } from '../../../ioc/server/providers/ServerEntityProvider/types/IServerEntityProvider';
import { TInitializationOptions } from '../../../ioc/server/providers/ServerEntityProvider/types/TInitializationOptions';

const setupIoCServerForTest = (initializationOptions: TInitializationOptions = {}) => async (done?) => {
  await (serverEntityProvider as IServerEntityProviderInternals).initialize(initializationOptions);

  if (done) {
    done();
  }
};

export default setupIoCServerForTest;
