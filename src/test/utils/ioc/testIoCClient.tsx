import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { getTestIoCInitConfig } from './config/testInitConfig';
import { TInitializationOptions } from '../../../ioc/client/types/interfaces';
import { IClientEntityProviderInternals } from '../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';

const setupIoCClientForTest = (initializationOptions: TInitializationOptions = {}) => async (done?) => {
  // WARN: you need to have ./config/test.json in order to run tests
  // @TODO: this should be documented inside readme
  await (clientEntityProvider as IClientEntityProviderInternals).initialize({
    ...getTestIoCInitConfig(),
    ...initializationOptions,
  });

  if (done) {
    done();
  }
};

export default setupIoCClientForTest;
