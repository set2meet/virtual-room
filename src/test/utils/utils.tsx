import React, { ComponentType, ReactNode } from 'react';
import { render, shallow } from 'enzyme';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { AssertionError } from 'assert';
import entityProvider from '../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import EnzymeMountTracker from './EnzymeMountTracker';
import { IClientEntityProviderInternals } from '../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import LoadableContextWrapper from '../../../.storybook/decorators/LazyResolver/LoadableContextWrapper';
import { AppModuleWrapper } from '../../../.storybook/decorators/appModuleDecorator';
import { ClientEntityProviderContext } from '../../ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { RouterModuleWrapper } from '../../../.storybook/decorators/routerDecorator';
import { TInitializationOptions } from '../../ioc/client/providers/ClientEntityProvider/types/TInitializationOptions';
import { LoadIndicator } from '../../../.storybook/common/components/LoadIndicator';
import CompositionRoot from '../../ioc/client/components/CompositionRoot/CompositionRoot';
import { getTestIoCInitConfig } from './ioc/config/testInitConfig';
import { ModuleRegistryKey } from '../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

const ThemeProviderWrapper = ({ theme, children }) => (
  <ThemeProvider theme={theme || entityProvider.defaultTheme}>{children}</ThemeProvider>
);

interface IInitModulesWrapperProps {
  IoCInitializationOptions?: TInitializationOptions;
  withRouterModule?: boolean;
  withRoomModule?: boolean;
  children?: ReactNode;
}

/**
 * Wrapper around testing Component; provides ioc with modules.
 * Use case: need to test lazy component with custom props
 *
 * @param IoCInitializationOptions
 * @param withRouterModule
 * @param withRoomModule
 * @param children
 *
 * @example:
 * const mounted = await loadAsyncTree(
 *  EnzymeMountTracker.mount(
 *   <InitModulesWrapper>
 *     <Wrapper />
 *   </InitModulesWrapper>
 *  )
 * );
 */
export const InitModulesWrapper = ({
  IoCInitializationOptions = {},
  withRouterModule = true,
  withRoomModule = false,
  children,
}: IInitModulesWrapperProps) => {
  const initializationOptions: TInitializationOptions = {
    ...getTestIoCInitConfig(),
    ...IoCInitializationOptions,
  };

  return (
    <LoadableContextWrapper>
      <CompositionRoot
        IoCInitializationOptions={initializationOptions}
        fallback={<LoadIndicator>{'Loading IoC...'}</LoadIndicator>}
      >
        <ClientEntityProviderContext.Consumer>
          {({ clientEntityProvider }) => {
            const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
            const ConnectionModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Connection);
            const AuthModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Auth);
            const MediaModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Media);

            let inner = children;

            if (withRoomModule) {
              const RoomModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.RoomModule);

              inner = <RoomModule>{inner}</RoomModule>;
            }

            inner = (
              <ConnectionModule>
                <AuthModule>
                  <AppModuleWrapper>
                    <MediaModule>{inner}</MediaModule>
                  </AppModuleWrapper>
                </AuthModule>
              </ConnectionModule>
            );

            if (withRouterModule) {
              inner = <RouterModuleWrapper>{inner}</RouterModuleWrapper>;
            }

            return <Provider store={store}>{inner}</Provider>;
          }}
        </ClientEntityProviderContext.Consumer>
      </CompositionRoot>
    </LoadableContextWrapper>
  );
};

export const shallowWithTheme = (theme?) => (tree) =>
  shallow(tree, {
    wrappingComponent: ThemeProviderWrapper,
    wrappingComponentProps: {
      theme,
    },
  });

export const mountWithTheme = (theme?) => (tree) =>
  EnzymeMountTracker.mount(tree, {
    wrappingComponent: ThemeProviderWrapper,
    wrappingComponentProps: {
      theme,
    },
  });

export const renderWithTheme = (theme?) => (tree) =>
  render(tree, {
    wrappingComponent: ThemeProviderWrapper,
    wrappingComponentProps: {
      theme,
    },
  });

export const waitNextTick = (): Promise<void> => new Promise((resolve) => process.nextTick(resolve));

export const waitForElement = (rootComponent, selector, maxTime = 2000, interval = 10) => {
  // Check correct usage
  if (!selector) {
    return Promise.reject(new AssertionError({ message: `No selector specified in waitForElement.` }));
  }

  if (!rootComponent) {
    return Promise.reject(new AssertionError({ message: `No root component specified in waitForElement.` }));
  }

  if (!rootComponent.length) {
    return Promise.reject(new AssertionError({ message: `Specified root component in waitForElement not found.` }));
  }

  // Race component search against maxTime
  return new Promise((resolve, reject) => {
    let remainingTime = maxTime;

    const intervalId = setInterval(() => {
      if (remainingTime < 0) {
        clearInterval(intervalId);
        return reject(
          new AssertionError({ message: `Expected to find ${selector} within ${maxTime}ms, but it was never found.` })
        );
      }

      const targetComponent = rootComponent.update().find(selector);
      if (targetComponent.length) {
        clearInterval(intervalId);
        return resolve(targetComponent);
      }

      remainingTime = remainingTime - interval;
    }, interval);
  });
};

export const withIoCFactory = <Props,>(componentFactory: () => ComponentType<Props>): ComponentType<Props> => {
  let CachedComponent = null;

  return (props) => {
    if (!CachedComponent) {
      CachedComponent = componentFactory();
    }
    return <CachedComponent {...props} />;
  };
};

export const enzymeCleanup = () => {
  EnzymeMountTracker.unmountTracked();
};
