/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { TAuthTokens, TRoomUser } from '../../ioc/client/types/interfaces';
import { TUser } from '../../ioc/client/providers/AuthProvider/types/client/AuthProvider.interfaces';
import { getUserWithTokens } from '../mocks/auth';
import clientEntityProvider from '../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export const setUser = (props, userProps?: Partial<TRoomUser>, tokensProps?: Partial<TAuthTokens>): TUser => {
  const user = getUserWithTokens(userProps, tokensProps);
  props.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(user));
  return user;
};

export const setRoom = (props, roomId, ownerId) => {
  const action: AnyAction = {
    type: clientEntityProvider.getAppActionTypes().ROOM_SET_INFO,
  };
  action.roomState = {
    state: {
      id: roomId,
      ownerId,
    },
  };
  props.dispatch(action);
  props.dispatch(
    clientEntityProvider.getAppActionCreators().updateParticipantInfo({
      userId: ownerId,
      userDisplayName: 'Test',
      isSharingScreen: false,
      isAudioStreamMuted: true,
      isVideoStreamMuted: true,
      hasAudioInputDevice: false,
      hasVideoInputDevice: false,
    })
  );
};
