/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, StoreEnhancer, StoreEnhancerStoreCreator } from 'redux';
import clientEntityProvider from '../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

function isFunction(value) {
  return 'function' === typeof value;
}

function isPlainObject(value) {
  return (
    Object.prototype.toString.call(value) === '[object Object]' && Object.getPrototypeOf(value) === Object.prototype
  );
}

export interface IStoreExt {
  getActions: () => AnyAction[];
  clearActions: () => void;
}

export const reduxMockEnhancer: StoreEnhancer = (next: StoreEnhancerStoreCreator) => (reducer, preloadedState?) => {
  const store = next(reducer, preloadedState);
  let actions = [];
  const listeners = [];
  return {
    ...store,
    getActions() {
      return actions;
    },
    clearActions() {
      actions = [];
    },
    dispatch(action) {
      if (!isPlainObject(action)) {
        throw new TypeError('Actions must be plain objects. ' + 'Use custom middleware for async actions.');
      }
      if (typeof action.type === 'undefined') {
        throw new TypeError(
          'Actions may not have an undefined "type" property. ' +
            'Have you misspelled a constant? ' +
            'Action: ' +
            JSON.stringify(action)
        );
      }
      if (clientEntityProvider.getUtils().isReduxDynamicModulesAction(action)) {
        store.dispatch(action);
      }

      actions.push(action);
      for (const listener of listeners) {
        listener(action);
      }
      return action;
    },
    subscribe(listener) {
      if (!isFunction(listener)) {
        throw new TypeError('Listener must be a function.');
      }
      listeners.push(listener);
      return unsubscribe;
      function unsubscribe() {
        const index = listeners.indexOf(listener);
        if (index === -1) {
          return;
        }
        listeners.splice(index, 1);
      }
    },
    replaceReducer(_nextReducer) {
      throw new Error('Mock stores do not support reducers. ' + 'Try supplying a function to getStore instead.');
    },
    /* istanbul ignore next */
    [Symbol.observable]() {
      throw new Error('Not implemented');
    },
  };
};
