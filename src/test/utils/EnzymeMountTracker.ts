/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { MountRendererProps, ReactWrapper, mount } from 'enzyme';
import { Component, ReactElement } from 'react';

class EnzymeMountTracker {
  private mountedList: ReactWrapper[] = [];
  private static _instance: EnzymeMountTracker;

  private constructor() {}

  public static getInstance() {
    return this._instance || (this._instance = new this());
  }

  public mount<P>(node: ReactElement<P>, options?: MountRendererProps): ReactWrapper<P, any>;
  public mount<P, S>(node: ReactElement<P>, options?: MountRendererProps): ReactWrapper<P, S>;
  public mount<C extends Component, P = C['props'], S = C['state']>(
    node: ReactElement<P>,
    options?: MountRendererProps
  ): ReactWrapper<P, S, C> {
    const mounted = mount<C, P, S>(node, options);

    this.track(mounted);
    return mounted;
  }

  public track(mountedWrapper: ReactWrapper) {
    this.mountedList.push(mountedWrapper);
  }

  public unmountTracked() {
    this.mountedList.forEach((currentMount) => {
      if (currentMount.exists()) {
        currentMount.unmount();
      }
    });
    this.mountedList = [];
  }
}

export default EnzymeMountTracker.getInstance();
