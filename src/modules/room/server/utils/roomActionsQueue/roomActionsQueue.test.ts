import { addQueueTask } from './roomActionsQueue';
import { TASK_TYPE } from './types/constants';

describe('Test room queues', () => {
  it('Calls functions in required order when we will wait until job id done', async () => {
    const obj = {
      arr: [],
      order: (num: string) => {
        obj.arr.push(num);
        return num;
      },
    };

    const calls = jest.spyOn(obj, 'order');
    const expectedCalls = 5;

    await addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('5') });
    await addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('4') });
    await addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('3') });
    await addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('2') });
    await addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('99') });

    expect(calls).toBeCalledTimes(expectedCalls);
    expect(obj.arr).toEqual(['5', '4', '3', '2', '99']);
  });

  it('Calls functions in required order when we wont wait until job is done', async (done) => {
    const obj = {
      arr: [],
      order: (num: string) => {
        obj.arr.push(num);
        return num;
      },
    };

    const calls = jest.spyOn(obj, 'order');
    const expectedCalls = 5;

    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('5') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('4') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('3') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('2') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('99') });
    addQueueTask({
      type: TASK_TYPE.setRoomExpire,
      fn: async () => {
        expect(calls).toBeCalledTimes(expectedCalls);
        expect(obj.arr).toEqual(['5', '4', '3', '2', '99']);
        done();
      },
    });
  });

  it('Calls functions in required priority', async (done) => {
    const obj = {
      arr: [],
      order: (num: string) => {
        obj.arr.push(num);
        return num;
      },
    };

    const calls = jest.spyOn(obj, 'order');
    const expectedCalls = 7;

    addQueueTask({ type: TASK_TYPE.setRoomExpire, fn: async () => obj.order('999') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('5') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('4') });
    addQueueTask({ type: TASK_TYPE.setRoomExpire, fn: async () => obj.order('100400800') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('3') });
    addQueueTask({ type: TASK_TYPE.getState, fn: async () => obj.order('2') });
    addQueueTask({ type: TASK_TYPE.task, fn: async () => obj.order('99') });
    addQueueTask({
      type: TASK_TYPE.setRoomExpire,
      fn: async () => {
        expect(calls).toBeCalledTimes(expectedCalls);
        expect(obj.arr).toEqual(['5', '4', '3', '99', '2', '999', '100400800']);
        done();
      },
    });
  });
});
