export const DELAY_AFTER_EXPIRE_ROOM = 2000;

export enum TASK_PRIORITY {
  LOWEST = 1,
  LOWER,
  MEDIUM,
  HIGH,
  HIGHEST,
}

export enum TASK_TYPE {
  setRoomExpire,
  setRoomState,
  getState,
  task = 999,
}
