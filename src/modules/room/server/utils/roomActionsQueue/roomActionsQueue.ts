import Queue from 'better-queue';
import { DELAY_AFTER_EXPIRE_ROOM, TASK_PRIORITY, TASK_TYPE } from './types/constants';

export type AnyRoomTask = { type: TASK_TYPE; fn: () => Promise<any> };

/**
 * Classic FIFO Queue for race-conditions
 * As there is still some issues with task queue (middleware updating state and nullifying expire at room),
 * There is delay for expire room.
 * Possibly good decision to pull all middleware into queue, but it require more time which we ain't have
 */
const roomActionsQueue = new Queue(
  async (task: AnyRoomTask, cb) => {
    const result = await task.fn();
    cb(null, result);
  },
  {
    priority: (task, cb) => {
      if (task.type === TASK_TYPE.setRoomExpire) {
        setTimeout(() => cb(null, TASK_PRIORITY.LOWEST), DELAY_AFTER_EXPIRE_ROOM);
        return;
      }
      if (task.type === TASK_TYPE.getState) {
        return cb(null, TASK_PRIORITY.HIGH);
      }

      cb(null, TASK_PRIORITY.HIGHEST);
    },
  }
);

export const addQueueTask = async <S>(task: AnyRoomTask): Promise<S> =>
  new Promise((resolve) => {
    roomActionsQueue.push(task).on('finish', resolve);
  });

export const addQueueTaskSync = <S>(task: AnyRoomTask): void => {
  roomActionsQueue.push(task);
};
