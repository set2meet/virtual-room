import * as ioredis from 'ioredis';
import _isEmpty from 'lodash/isEmpty';
import { IApiRoomParticipant, IRoomReduxState } from '../../../../../ioc/common/ioc-interfaces';
import { getRedisRoomId } from '../getRedisRoomId/getRedisRoomId';

interface IRoomSettings {
  something: true;
}

interface IRoomManager {
  deleteRoomData(roomId: string): Promise<number>;
  getRoomData(roomId: string): Promise<IRoomReduxState | null>;
  _getRoomData(roomId: string): Promise<IRoomReduxState | null>;
  getRoomsData(): Promise<object[]>;
  updateRoomSettings(room: { roomId: string; realm: string }, settings: IRoomSettings): Promise<string>;
  updateRoomParticipants(room: { roomId: string; realm: string }, participants: IApiRoomParticipant[]): Promise<string>;
  userHasRole(roles: string[], role?: string): boolean;
}

const REALM_ACCESS_ROLE = 'realm-manager';

export function createRoomManager(redisClient: ioredis.Redis): IRoomManager {
  return {
    async deleteRoomData(roomId) {
      return await redisClient.del(getRedisRoomId(roomId));
    },

    /**
     * Getting room data
     * @param roomId
     */
    async getRoomData(roomId) {
      if (!roomId) {
        throw new Error('No roomId provided');
      }

      const roomDataRaw: string = await redisClient.get(getRedisRoomId(roomId));
      const roomData: { state: IRoomReduxState } = JSON.parse(roomDataRaw);

      if (!_isEmpty(roomData)) {
        return roomData.state;
      }
      return null;
    },

    /**
     * For internal use
     * @param roomId
     */
    async _getRoomData(roomId) {
      if (!roomId) {
        throw new Error('No roomId provided');
      }

      const roomDataRaw: string = await redisClient.get(roomId);
      const roomData: { state: IRoomReduxState } = JSON.parse(roomDataRaw);

      if (!_isEmpty(roomData)) {
        return roomData.state;
      }
      return null;
    },

    getRoomsData() {
      const roomsAwaitingFetch = [];
      const wildcard = '*';

      return redisClient.keys(getRedisRoomId(wildcard)).then((rooms) => {
        if (rooms.length === 0) {
          return rooms;
        }

        rooms.forEach((roomId) => {
          // tslint:disable-next-line:no-invalid-this
          roomsAwaitingFetch.push(this._getRoomData(roomId));
        });

        return Promise.all(roomsAwaitingFetch);
      });
    },

    async updateRoomSettings(room, settings) {
      // tslint:disable-next-line:no-invalid-this
      const roomData = await this.getRoomData(getRedisRoomId(room.roomId));

      roomData.settings = settings;
      return roomData;
    },

    async updateRoomParticipants(room, participants) {
      // tslint:disable-next-line:no-invalid-this
      const roomData = await this.getRoomData(getRedisRoomId(room.roomId));

      roomData.participants = participants;
      return roomData;
    },

    /**
     * Checks that user has realm-manager role
     * Or pass any other to check this
     * @param roles
     * @param role
     * @return boolean
     */
    userHasRole(roles, role?) {
      return !!roles.includes(REALM_ACCESS_ROLE || role);
    },
  };
}
