import { IServerMetaObjects } from '../../../../../ioc/server/types/interfaces';
import roomStorageClient from '../../roomStorageClient';
import { roomReducer } from '../../serverMiddleware';
import { getUserTarget } from '../getUserTarget/getUserTarget';
import { getRoomTarget } from '../getRoomTarget/getRoomTarget';
import { ROOM_EVENTS } from '../../types/constants/constants';
import { IRoomInternalAction } from '../../../common/types/interfaces';

export const sendActionsByTargets = async (action: IRoomInternalAction, appMetaObjects: IServerMetaObjects) => {
  const users = getUserTarget(action);
  const roomId = getRoomTarget(action);

  if (roomId && !users) {
    const room = await roomStorageClient.getRoom(roomId, appMetaObjects, roomReducer);

    if (room) {
      await room.sendAction(action);
    }

    return;
  }

  if (!users) {
    return;
  }

  appMetaObjects.socketAdapter.emit({
    target: {
      roomId,
      users,
    },
    event: ROOM_EVENTS.ACTION,
    payload: action,
  });
};
