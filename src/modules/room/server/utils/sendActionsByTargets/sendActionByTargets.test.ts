import setupIoCServerForTest from '../../../../../test/utils/ioc/testIoCServer';
import { mock } from 'jest-mock-extended';
import IORedis from 'ioredis';
import Redis from 'ioredis-mock';
import { IRoomInternalAction } from '../../../common/types/interfaces';
import { sendActionsByTargets } from './sendActionByTargets';
import { IServerLogger, IServerMetaObjects, SocketIOAdapter } from '../../../../../ioc/server/types/interfaces';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

beforeAll(setupIoCServerForTest());

describe('Send Action by Targets', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const action: Readonly<IRoomInternalAction> = {
    type: 'any',
    meta: {
      target: {
        users: ['user1', 'user2'],
        roomId: 'wow!anroomid',
      },
    },
  };

  const actionWithoutUsers: Readonly<IRoomInternalAction> = {
    type: 'any',
    meta: {
      target: {
        roomId: 'wow!anroomid',
      },
    },
  };

  const actionWithoutRoom: Readonly<IRoomInternalAction> = {
    type: 'any',
    meta: {
      target: {
        users: ['user1', 'user2'],
        roomId: 'wow!anroomid',
      },
    },
  };

  const initAppMetaObjects = (): IServerMetaObjects => {
    const logger = mock<IServerLogger>();

    const socketAdapter: SocketIOAdapter = {
      emit: jest.fn(),
      getRoomClientSocket: jest.fn(),
      getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
    };

    return {
      socketAdapter,
      logger,
      redis: {
        pubClient: new Redis() as IORedis.Redis,
        subClient: new Redis() as IORedis.Redis,
      },
      dispatchAction: null,
      config: serverEntityProvider.getConfig(),
    };
  };

  let appMetaObjects: IServerMetaObjects = null;

  beforeAll(async () => {
    // ioc should be ready before this call
    appMetaObjects = initAppMetaObjects();
  });

  it('send action', () => {
    sendActionsByTargets(action, appMetaObjects);
    const { socketAdapter } = appMetaObjects;

    expect(socketAdapter.emit).toBeCalledTimes(1);
    expect(socketAdapter.emit).toBeCalledWith({
      event: 'action',
      payload: action,
      target: { roomId: action.meta.target.roomId, users: action.meta.target.users },
    });
  });

  it('send action', () => {
    sendActionsByTargets(actionWithoutUsers, appMetaObjects);
    const { socketAdapter } = appMetaObjects;

    expect(socketAdapter.emit).toBeCalledTimes(0);
  });

  it('send action', () => {
    sendActionsByTargets(actionWithoutRoom, appMetaObjects);
    const { socketAdapter } = appMetaObjects;

    expect(socketAdapter.emit).toBeCalledTimes(1);
    expect(socketAdapter.emit).toBeCalledWith({
      event: 'action',
      payload: action,
      target: { roomId: action.meta.target.roomId, users: action.meta.target.users },
    });
  });
});
