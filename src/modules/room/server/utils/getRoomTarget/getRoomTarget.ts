import { IRoomInternalAction } from '../../../common/types/interfaces';

export const getRoomTarget = (action: IRoomInternalAction): string | null => {
  return action?.meta?.target?.roomId || null;
};
