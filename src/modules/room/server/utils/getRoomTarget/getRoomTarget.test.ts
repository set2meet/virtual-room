import { getRoomTarget } from './getRoomTarget';

const roomId = 'wow! its an room id!';

const action = {
  type: 'any',
  meta: {
    target: {
      roomId,
    },
  },
};

describe('returns target room id', () => {
  it('get target and returns it', () => {
    const roomTarget = getRoomTarget(action);

    expect(roomTarget).toEqual(roomId);
  });
});
