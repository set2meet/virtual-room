import { IRoomInternalAction } from '../../../common/types/interfaces';

export const getUserTarget = (action: IRoomInternalAction): string[] => {
  const target = action?.meta?.target?.users;
  return target && target?.length ? target : null;
};
