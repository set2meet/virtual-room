import { getUserTarget } from './getUserTarget';

const usersExpected = ['user1', 'user2'];

const action = {
  type: 'any',
  meta: {
    target: {
      users: usersExpected,
    },
  },
};

const actionWithoutActualData = {
  type: 'any',
  meta: {
    target: {},
  },
};

describe('test for getUserTarget function, which simply returns data from object', () => {
  it('get target and returns it', () => {
    const userTarget = getUserTarget(action);

    expect(userTarget).toEqual(usersExpected);
  });

  it('target is null', () => {
    const userTarget = getUserTarget(actionWithoutActualData);

    expect(userTarget).toBeNull();
  });
});
