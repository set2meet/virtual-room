import { userColorFor } from './userColorFor';
import { IGlobalRoomReduxState } from '../../../common/redux/types/IGlobalRoomReduxState';

describe('Generating colors for user', () => {
  const definedUserColors = {
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5a': '#00e676',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5b': '#66bb6a',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5c': '#80deea',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5d': '#80deea',
  };

  const userIdUndefined = '403d3971-5b86-4344-b75a-0fc6b9b0aa5e';
  const userIdPredefined = '403d3971-5b86-4344-b75a-0fc6b9b0aa5a';

  const expectedRoomState: IGlobalRoomReduxState = {
    chat: {
      config: { playSoundOnNewMessage: true },
      history: [],
      unreadId: null,
    },
    graph2d: {
      angleMode: 'radians',
      equationsList: [],
      gridlinesMode: 'normal',
      precisionMode: 1,
    },
    presentations: {
      annotationPages: {},
      isLoading: false,
      isRendering: false,
      pages: {
        current: 0,
        max: 0,
      },
      shared: false,
      uploadedPresentationLink: '',
      viewport: null,
    },
    recording: {
      isRecording: false,
      isSuspend: false,
      recId: '',
    },
    state: {
      currentTool: '',
      error: {
        code: null,
      },
      id: 'test-room-test',
      isTemporary: true,
      ownerId: '5e0d2b52-3435-4275-89e8-cffe018eed45',
      participants: [],
      userColors: definedUserColors,
    },
    webphone: {},
    webrtcService: { serviceName: null, sessionId: null },
    whiteboard: {
      selected: [],
      shapes: [],
    },
  };

  it('Generate random color for new user', async () => {
    const roomStub = {
      sendAction: async () => {
        return;
      },
      updateState: async () => {
        return;
      },
      getState: async (): Promise<IGlobalRoomReduxState> => {
        return expectedRoomState;
      },
    };
    const color = await userColorFor(roomStub, userIdUndefined);
    const colorLength = 7;

    expect(color.length).toEqual(colorLength);
  });

  it('Generate random color for rejoined user', async () => {
    const roomStub = {
      sendAction: async () => {
        return;
      },
      updateState: async () => {
        return;
      },
      getState: async (): Promise<IGlobalRoomReduxState> => {
        return expectedRoomState;
      },
    };
    const color = await userColorFor(roomStub, userIdPredefined);

    expect(color).toEqual(definedUserColors[userIdPredefined]);
  });
});
