import { IRoom } from '../../roomStorageClient';
import getUserColor from '../colorModel/colorModel';

export const userColorFor = async (room: IRoom, userId: string) => {
  const { userColors } = (await room.getState()).state;
  return userColors[userId] || getUserColor(userColors);
};
