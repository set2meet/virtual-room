import colorModel from './colorModel';

describe('Generatin color for user', () => {
  const definedUserColors = {
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5a': '#00e676',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5b': '#66bb6a',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5c': '#80deea',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5d': '#80deea',
  };

  const undefinedUserColors = {
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5e': '',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5f': '',
    '403d3971-5b86-4344-b75a-0fc6b9b0aa5g': '',
  };
  const colorStringLength = 7;

  it('Generate random color', () => {
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(undefinedUserColors).length).toEqual(colorStringLength);
  });

  it('Returns defined color', () => {
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
    expect(colorModel(definedUserColors).length).toEqual(colorStringLength);
  });
});
