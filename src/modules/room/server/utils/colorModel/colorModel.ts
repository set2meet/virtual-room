/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _shuffle from 'lodash/shuffle';

type UserColorsMap = Record<string, 1>;

const hex = 16;
const hexColorLen = 6;
const letters = '0123456789abcdef';
const palette: string[] = `
#8d1616;#d32f2f;#f79f9f;#eb719a;#ce93d8;#9fa8da;#7e57c2;#90caf9;#039be5;#0277bd;
#546af5;#303f9f;#80deea;#61dcbd;#0097a7;#66bb6a;#00c853;#00e676;#1f8123;#aeea00;
#afb42b;#908521;#fcc717;#f5c790;#ff9800;#ff5722;#bb8379;#795548;#78909c;#576c76
`
  .replace(/\n/g, '')
  .split(';');

const generateRandomColor = (): string => {
  let color = '#';

  for (let i = 0; i < hexColorLen; i++) {
    color += letters[Math.floor(Math.random() * hex)];
  }

  return color;
};

export default (colors: Record<string, string> = {}) => {
  const used: UserColorsMap = Object.values(colors).reduce((res: UserColorsMap, color: string) => {
    res[color] = 1;
    return res;
  }, {});

  return _shuffle(palette).filter((color: string) => !used[color])[0] || generateRandomColor();
};
