import { IAppConfigProject } from '../../../../app/types/app';
import { getRedisRoomId, roomPrefix } from './getRedisRoomId';
import setupIoCServerForTest from '../../../../../test/utils/ioc/testIoCServer';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

describe('Returning prepared value', () => {
  beforeAll(setupIoCServerForTest());

  it('Returning room id with realm and room prefix', () => {
    const roomId = 'chupakabra';
    const { realmPrefix }: IAppConfigProject = serverEntityProvider.getConfig().project;

    const expectedValue = `${roomPrefix}__${realmPrefix}:${roomId}`;

    const testedValue = getRedisRoomId(roomId);

    expect(testedValue).toEqual(expectedValue);
  });
});
