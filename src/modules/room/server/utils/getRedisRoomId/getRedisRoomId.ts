import { IAppConfigProject } from '../../../../app/types/app';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

export const roomPrefix = '__room:';

/**
 * Simply concat required strings
 * @param roomId
 */
export const getRedisRoomId = (roomId: string): string => {
  const { realmPrefix }: IAppConfigProject = serverEntityProvider.getConfig().project;

  return `${roomPrefix}__${realmPrefix}:${roomId}`;
};
