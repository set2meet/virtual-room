import { IApiRoomParticipant } from '../../../common/types/interfaces';
import { PARTICIPANT_ROLES } from '../../types/constants/constants';
import _isEmpty from 'lodash/isEmpty';
import _head from 'lodash/head';

export const getRoomOwner = (participants: IApiRoomParticipant[]): string | null => {
  const isOwnerExists = participants.filter((p: any) => p.role === PARTICIPANT_ROLES.OWNER);

  if (!_isEmpty(isOwnerExists)) {
    if (isOwnerExists.length > 1) {
      return null;
    }
    const owner: IApiRoomParticipant = _head(isOwnerExists);
    return owner?.id;
  }
  return null;
};
