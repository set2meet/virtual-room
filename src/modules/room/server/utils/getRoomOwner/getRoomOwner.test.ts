import { PARTICIPANT_ROLES } from '../../types/constants/constants';
import { getRoomOwner } from './getRoomOwner';

describe('Return room owner from participants array', () => {
  it('Returns owner id', () => {
    const participants = [
      {
        displayName: 'Metting Partisipated',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a77',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Particimant Met',
        id: '63bac08f-6f72-4ba7-b4dc-bb96b4095a78',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Parzticipant Meetingson',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a79',
        role: PARTICIPANT_ROLES.OWNER,
      },
    ];

    expect(getRoomOwner(participants)).toEqual('6fbac08f-6f72-4ba7-b4dc-bb96b4095a79');
  });

  it('Returns null, as owner doesnt exists', () => {
    const participants = [
      {
        displayName: 'Metting Partisipated',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a77',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Particimant Met',
        id: '63bac08f-6f72-4ba7-b4dc-bb96b4095a78',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Parzticipant Meetingson',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a79',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
    ];

    expect(getRoomOwner(participants)).toBeNull();
  });

  it('Returns null, as owners more than two', () => {
    const participants = [
      {
        displayName: 'Metting Partisipated',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a77',
        role: PARTICIPANT_ROLES.OWNER,
      },
      {
        displayName: 'Particimant Met',
        id: '63bac08f-6f72-4ba7-b4dc-bb96b4095a78',
        role: PARTICIPANT_ROLES.OWNER,
      },
      {
        displayName: 'Parzticipant Meetingson',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a79',
        role: PARTICIPANT_ROLES.OWNER,
      },
    ];

    expect(getRoomOwner(participants)).toBeNull();
  });
});
