import { AnyAction } from 'redux';
import roomStorageClient from '../../roomStorageClient';
import { v4 as guid } from 'uuid';
import { roomReducer } from '../../serverMiddleware';
import { IServerMetaObjects } from '../../../../../ioc/common/ioc-interfaces';

export const createRoomIfNotExists = async (
  action: AnyAction,
  appMetaObjects: IServerMetaObjects,
  isApi?: boolean
): Promise<string> => {
  const room = await roomStorageClient.getRoom(action.roomId, appMetaObjects, roomReducer);
  if (!room) {
    if (action.isTemporary || typeof action.roomId === 'undefined') {
      action.roomId = action?.roomId || guid().toString();
    }

    if (isApi) {
      await roomStorageClient.createRoomAPI(
        {
          id: action.roomId,
          isTemporary: action.isTemporary,
          userId: action.ownerId,
          settings: action.settings || {},
          participants: action.participants || [],
        },
        appMetaObjects,
        roomReducer
      );
    } else {
      await roomStorageClient.createRoom(
        {
          id: action.roomId,
          isTemporary: action.isTemporary,
          userId: action.userId,
        },
        appMetaObjects,
        roomReducer
      );
    }
  }

  return action.roomId;
};
