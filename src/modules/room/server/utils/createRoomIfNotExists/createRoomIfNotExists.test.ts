import setupIoCServerForTest from '../../../../../test/utils/ioc/testIoCServer';
import { mock } from 'jest-mock-extended';
import IORedis from 'ioredis';
import Redis from 'ioredis-mock';
import RoomAction from '../../../common/redux/types/RoomAction';
import { createRoomIfNotExists } from './createRoomIfNotExists';
import { IServerLogger, IServerMetaObjects, SocketIOAdapter } from '../../../../../ioc/server/types/interfaces';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { getRedisRoomId } from '../getRedisRoomId/getRedisRoomId';

beforeAll(setupIoCServerForTest());

describe('create room if not exists function', () => {
  const initAppMetaObjects = (): IServerMetaObjects => {
    const logger = mock<IServerLogger>();
    const socketAdapter: SocketIOAdapter = {
      emit: jest.fn(),
      getRoomClientSocket: jest.fn(),
      getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
    };

    return {
      socketAdapter,
      logger,
      redis: {
        pubClient: new Redis() as IORedis.Redis,
        subClient: new Redis() as IORedis.Redis,
      },
      dispatchAction: null,
      config: serverEntityProvider.getConfig(),
    };
  };

  let appMetaObjects: IServerMetaObjects = null;

  beforeAll(async () => {
    // ioc should be ready before this call
    appMetaObjects = initAppMetaObjects();
  });

  it('should create temporary room if it not exists', async (done) => {
    const TEST_ROOM_ID = 'test-room-temp';

    const action = {
      type: RoomAction.ROOM_CREATE,
      userId: '5e0d2b52-3435-4275-89e8-cffe018eed45', // owner id
      roomId: TEST_ROOM_ID,
      isTemporary: true,
    };

    await createRoomIfNotExists(action, appMetaObjects);

    const roomState = await appMetaObjects.redis.pubClient.get(getRedisRoomId(TEST_ROOM_ID));

    const expectedRoomState = {
      chat: {
        config: { playSoundOnNewMessage: true },
        history: [],
        unreadId: null,
      },
      graph2d: {
        angleMode: 'radians',
        equationsList: [],
        gridlinesMode: 'normal',
        precisionMode: 1,
      },
      presentations: {
        annotationPages: {},
        isLoading: false,
        isRendering: false,
        pages: {
          current: 0,
          max: 0,
        },
        shared: false,
        uploadedPresentationLink: '',
        viewport: null,
      },
      recording: {
        isRecording: false,
        isSuspend: false,
        recId: '',
      },
      state: {
        currentTool: '',
        error: {
          code: null,
        },
        id: TEST_ROOM_ID,
        isTemporary: true,
        ownerId: '5e0d2b52-3435-4275-89e8-cffe018eed45',
        participants: [],
        userColors: {},
      },
      webphone: {},
      webrtcService: { serviceName: null, sessionId: null },
      whiteboard: {
        selected: [],
        shapes: [],
      },
    };

    expect(JSON.parse(roomState)).toEqual(expectedRoomState);
    done();
  });

  it('should create permanent room if it not exists', async (done) => {
    const TEST_ROOM_ID = 'test-room-permanent';

    const action = {
      type: RoomAction.ROOM_CREATE,
      userId: '5e0d2b52-3435-4275-89e8-cffe018eed45', // owner id
      roomId: TEST_ROOM_ID,
      isTemporary: false,
    };

    await createRoomIfNotExists(action, appMetaObjects);

    const roomState = await appMetaObjects.redis.pubClient.get(getRedisRoomId(TEST_ROOM_ID));

    const expectedRoomState = {
      chat: {
        config: { playSoundOnNewMessage: true },
        history: [],
        unreadId: null,
      },
      graph2d: {
        angleMode: 'radians',
        equationsList: [],
        gridlinesMode: 'normal',
        precisionMode: 1,
      },
      presentations: {
        annotationPages: {},
        isLoading: false,
        isRendering: false,
        pages: {
          current: 0,
          max: 0,
        },
        shared: false,
        uploadedPresentationLink: '',
        viewport: null,
      },
      recording: {
        isRecording: false,
        isSuspend: false,
        recId: '',
      },
      state: {
        currentTool: '',
        error: {
          code: null,
        },
        id: TEST_ROOM_ID,
        isTemporary: false,
        ownerId: '5e0d2b52-3435-4275-89e8-cffe018eed45',
        participants: [],
        userColors: {},
      },
      webphone: {},
      webrtcService: { serviceName: null, sessionId: null },
      whiteboard: {
        selected: [],
        shapes: [],
      },
    };

    expect(JSON.parse(roomState)).toEqual(expectedRoomState);
    done();
  });
});
