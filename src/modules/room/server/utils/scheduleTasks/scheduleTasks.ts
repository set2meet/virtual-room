/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _find from 'lodash/find';
import _remove from 'lodash/remove';
import IORedis from 'ioredis';
import { EventEmitter } from 'events';

const SHEDULE_KEY = 'schedule';
const SHEDULE_TASK_KEY = SHEDULE_KEY + ':task';
const SHEDULE_EVENT_TASK_EXEC = SHEDULE_KEY + '-task-exec';
const generateTaskId = (): string => Date.now().toString(32); // tslint:disable-line

type ScheduleTask = {
  date: number;
  action: any;
};

type ScheduledTask = ScheduleTask & {
  id: string;
};

type ScheduledTaskCallback = (task: ScheduledTask) => void;

class ScheduleTasks extends EventEmitter {
  private _pubClient: IORedis.Redis;
  private _tasks: ScheduledTask[] = [];
  private _timers: Record<string, NodeJS.Timer> = {};

  constructor(pubClient: IORedis.Redis) {
    super();

    this._pubClient = pubClient;
    this._restoreTasksFromRedis();
  }

  public add(task: ScheduleTask) {
    return new Promise(async (resolve) => {
      const id = generateTaskId();

      await this._pubClient.hset(SHEDULE_TASK_KEY, id, JSON.stringify(task));
      this._scheduleTask(id, task);
      resolve(id);
    });
  }

  public del(id: string) {
    return new Promise(async (resolve) => {
      await this._pubClient.hdel(SHEDULE_TASK_KEY, id);
      _remove(this._tasks, (task) => task.id === id);
      clearTimeout(this._timers[id]);
      this._timers[id] = null;
      resolve(id);
    });
  }

  public onTask(callback: ScheduledTaskCallback) {
    this.on(SHEDULE_EVENT_TASK_EXEC, callback);
  }

  public findTask(action: any): ScheduledTask {
    return _find(this._tasks, { action }) as ScheduledTask;
  }

  private _scheduleTask(id: string, task: ScheduleTask) {
    const scheduledTask = { id, ...task };

    this._tasks.push(scheduledTask);

    // console.log('TASK SCHEDULED', this._tasks);

    this._timers[id] = setTimeout(() => {
      this.emit(SHEDULE_EVENT_TASK_EXEC, scheduledTask);
    }, task.date - Date.now());
  }

  private async _restoreTasksFromRedis() {
    const tasks: any = await this._pubClient.hgetall(SHEDULE_TASK_KEY);

    Object.keys(tasks).forEach((id: string) => {
      this._scheduleTask(id, JSON.parse(tasks[id]));
    });
  }
}

export default ScheduleTasks;
