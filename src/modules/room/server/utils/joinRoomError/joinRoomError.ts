import { Socket } from 'socket.io';
import ROOM_JOIN_ERROR from '../../../common/types/constants/RoomJoinError';
import RoomAction from '../../../common/redux/types/RoomAction';

export const joinRoomError = (socket: Socket, errorCode: ROOM_JOIN_ERROR) => {
  socket.emit('action', {
    action: {
      errorCode,
      type: RoomAction.ROOM_CANNOT_JOIN,
    },
  });
};
