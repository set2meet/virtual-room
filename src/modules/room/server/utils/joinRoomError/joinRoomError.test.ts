import { SocketIOAdapter } from '../../../../../ioc/server/types/interfaces';
import { joinRoomError } from './joinRoomError';
import ROOM_JOIN_ERROR from '../../../common/types/constants/RoomJoinError';
import RoomAction from '../../../common/redux/types/RoomAction';

describe('Room Join Error test', () => {
  let socketAdapter: SocketIOAdapter;

  beforeEach(() => {
    socketAdapter = null;
    socketAdapter = {
      emit: jest.fn(),
      getRoomClientSocket: jest.fn(),
      getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
    };
  });

  it('Room join error: room is full', () => {
    joinRoomError(socketAdapter as any, ROOM_JOIN_ERROR.IS_FULL);

    expect(socketAdapter.emit).toBeCalledTimes(1);
    expect(socketAdapter.emit).toBeCalledWith('action', {
      action: { errorCode: ROOM_JOIN_ERROR.IS_FULL, type: RoomAction.ROOM_CANNOT_JOIN },
    });
  });

  it('Room join error: already joined', () => {
    joinRoomError(socketAdapter as any, ROOM_JOIN_ERROR.ALREADY_JOINED);

    expect(socketAdapter.emit).toBeCalledTimes(1);
    expect(socketAdapter.emit).toBeCalledWith('action', {
      action: { errorCode: ROOM_JOIN_ERROR.ALREADY_JOINED, type: RoomAction.ROOM_CANNOT_JOIN },
    });
  });

  it('Room join error: wrong room id', () => {
    joinRoomError(socketAdapter as any, ROOM_JOIN_ERROR.WRONG_ID);

    expect(socketAdapter.emit).toBeCalledTimes(1);
    expect(socketAdapter.emit).toBeCalledWith('action', {
      action: { errorCode: ROOM_JOIN_ERROR.WRONG_ID, type: RoomAction.ROOM_CANNOT_JOIN },
    });
  });
});
