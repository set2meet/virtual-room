import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import roomStorageClient from '../../roomStorageClient';
import { roomReducer } from '../../serverMiddleware';

export const setSuspendRecordingValue = async (recordId: string, appMetaObjects, value: boolean) => {
  const recordingDb = serverEntityProvider.getRecordingDb();
  const record = await recordingDb.getRecord(recordId);
  const action = { type: serverEntityProvider.getActionTypes().ROOM_RECORDING_SET_SUSPEND_VALUE, value };
  const room = await roomStorageClient.getRoom(record.roomId, appMetaObjects, roomReducer);
  if (!room) {
    return;
  }

  await room.sendAction(action);
};
