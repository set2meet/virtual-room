import { IGlobalRoomReduxState } from '../../../common/redux/types/IGlobalRoomReduxState';
import { getRoomOwner } from '../getRoomOwner/getRoomOwner';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

export const isRoomFull = (userId: string, roomState: IGlobalRoomReduxState): boolean => {
  const webrtcServiceName = roomState.webrtcService.serviceName;
  const { participants, ownerId } = roomState.state;
  const notOwner = ownerId !== userId;
  const roomOwner = getRoomOwner(participants);
  const ownerNotInTheRoom = roomOwner !== null;
  let maxUsers: number = serverEntityProvider.getConfig().webrtc?.maxUsers?.[webrtcServiceName];
  if (typeof maxUsers === 'undefined') {
    maxUsers = Infinity;
  }

  if (notOwner && ownerNotInTheRoom) {
    maxUsers--;
  }

  return maxUsers === participants.length;
};
