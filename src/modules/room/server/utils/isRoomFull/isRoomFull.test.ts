import { isRoomFull } from './isRoomFull';
import { IGlobalRoomReduxState } from '../../../common/redux/types/IGlobalRoomReduxState';
import { PARTICIPANT_ROLES } from '../../types/constants/constants';
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import setupIoCServerForTest from '../../../../../test/utils/ioc/testIoCServer';

const userId = '5e0d2b52-3435-4275-89e8-cffe018eed45';

const expectedRoomState: IGlobalRoomReduxState = {
  chat: {
    config: { playSoundOnNewMessage: true },
    history: [],
    unreadId: null,
  },
  graph2d: {
    angleMode: 'radians',
    equationsList: [],
    gridlinesMode: 'normal',
    precisionMode: 1,
  },
  presentations: {
    annotationPages: {},
    isLoading: false,
    isRendering: false,
    pages: {
      current: 0,
      max: 0,
    },
    shared: false,
    uploadedPresentationLink: '',
    viewport: null,
  },
  recording: {
    isRecording: false,
    isSuspend: false,
    recId: '',
  },
  state: {
    currentTool: '',
    error: {
      code: null,
    },
    id: 'test-room-test',
    isTemporary: true,
    ownerId: '5e0d2b52-3435-4275-89e8-cffe018eed45',
    participants: [],
    userColors: {},
  },
  webphone: {},
  webrtcService: { serviceName: null, sessionId: null },
  whiteboard: {
    selected: [],
    shapes: [],
  },
};

beforeAll(setupIoCServerForTest());

describe('Check that room is full', () => {
  it('room is not full', () => {
    expect(isRoomFull(userId, expectedRoomState)).toBeFalsy();
  });

  it('room is full for PEER-2-PEER', () => {
    expectedRoomState.webrtcService.serviceName = WebrtcServiceType.P2P;
    expectedRoomState.state.participants = [
      {
        displayName: 'Metting Partisipated',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a77',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Particimant Met',
        id: '63bac08f-6f72-4ba7-b4dc-bb96b4095a78',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Parzticipant Meetingson',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a79',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Parzticipante Meetinsone',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a80',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
      {
        displayName: 'Parzty Meetinsonec',
        id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a81',
        role: PARTICIPANT_ROLES.DEFAULT,
      },
    ];

    expect(isRoomFull(userId, expectedRoomState)).toBeTruthy();
  });
});
