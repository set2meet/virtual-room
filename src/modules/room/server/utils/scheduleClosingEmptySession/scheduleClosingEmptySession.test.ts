import { mock } from 'jest-mock-extended';
import IORedis from 'ioredis';
import Redis from 'ioredis-mock';
import setupIoCServerForTest from '../../../../../test/utils/ioc/testIoCServer';
import { scheduleClosingEmptySession, TEMP_ROOM_EXPIRE_TIME } from './scheduleClosingEmptySession';
import RoomAction from '../../../common/redux/types/RoomAction';
import { createRoomIfNotExists } from '../createRoomIfNotExists/createRoomIfNotExists';
import {
  IServerLogger,
  TLoggerServerModule,
  IServerMetaObjects,
  SocketIOAdapter,
} from '../../../../../ioc/server/types/interfaces';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { getRedisRoomId } from '../getRedisRoomId/getRedisRoomId';

beforeAll(setupIoCServerForTest());

const initAppMetaObjects = (): IServerMetaObjects => {
  const logger = ({
    roomServer: mock<TLoggerServerModule>(),
  } as any) as IServerLogger;
  const socketAdapter: SocketIOAdapter = {
    emit: jest.fn(),
    getRoomClientSocket: jest.fn(),
    getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
  };

  return {
    socketAdapter,
    logger,
    redis: {
      pubClient: new Redis() as IORedis.Redis,
      subClient: new Redis() as IORedis.Redis,
    },
    dispatchAction: null,
    config: serverEntityProvider.getConfig(),
  };
};

describe('Schedule Closing Empty Session', () => {
  let appMetaObjects: IServerMetaObjects = null;

  beforeAll(async () => {
    // ioc should be ready before this call
    appMetaObjects = initAppMetaObjects();
  });

  const roomId = 'room-id-room-id';
  const userId = '403d3971-5b86-4344-b75a-0fc6b9b0aa5a';

  const action = {
    meta: {
      key: 'value',
    },
  };

  it('Room without participants sentenced to EXPIRE', async (done) => {
    const roomAction = {
      type: RoomAction.ROOM_CREATE,
      userId,
      roomId,
      isTemporary: true,
      meta: {},
    };

    await createRoomIfNotExists(roomAction, appMetaObjects);

    await scheduleClosingEmptySession(appMetaObjects, roomId, userId, action.meta);

    const sentencedToClose = await appMetaObjects.redis.pubClient.ttl(getRedisRoomId(roomId));

    expect(sentencedToClose).toEqual(TEMP_ROOM_EXPIRE_TIME);
    done();
  });

  it('Room with participants is NOT sentenced to expire', async (done) => {
    const roomAction = {
      type: RoomAction.ROOM_CREATE,
      userId,
      roomId,
      isTemporary: true,
      meta: {},
    };

    await createRoomIfNotExists(roomAction, appMetaObjects);
    const room = await appMetaObjects.redis.pubClient.get(getRedisRoomId(roomId));

    const roomData = JSON.parse(room);

    roomData.state.participants = [1, 2];

    await appMetaObjects.redis.pubClient.set(getRedisRoomId(roomId), JSON.stringify(roomData));

    await scheduleClosingEmptySession(appMetaObjects, roomId, userId, action.meta);

    const sentencedToClose = await appMetaObjects.redis.pubClient.ttl(getRedisRoomId(roomId));

    expect(sentencedToClose).toEqual(-1);
    done();
  });
});
