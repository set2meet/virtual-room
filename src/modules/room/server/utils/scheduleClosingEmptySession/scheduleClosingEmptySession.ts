import { IServerMetaObjects } from '../../../../../ioc/server/types/interfaces';
import roomStorageClient from '../../roomStorageClient';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { roomReducer, scheduleTasks, storeKey } from '../../serverMiddleware';
import { IRoomReduxState } from '../../../common/redux/types/IRoomReduxState';

export const TEMP_ROOM_EXPIRE_TIME = 595; // 10 min = 600 sec, timeout to delete empty temp room
export const scheduleClosingEmptySession = async (
  appMetaObjects: IServerMetaObjects,
  roomId: string,
  userId: string,
  metadata: {}
) => {
  const room = await roomStorageClient.getRoom(roomId, appMetaObjects, roomReducer);

  const logger = appMetaObjects.logger.roomServer;
  const appState = await room.getState();
  const { sessionId, serviceName } = appState.webrtcService;
  const roomState: IRoomReduxState = (await room.getState())[storeKey];

  logger.info(`User '${userId}' left room (${roomId}) with session id (${sessionId})`);
  logger.info(`The following users remained in the room (${roomId}): [${roomState.participants.join(',')}]`);

  if (roomState.participants.length === 0) {
    logger.info('Zero participants detected...');
    if (roomState.isTemporary) {
      logger.info('Setting timeout to delete room...');
      logger.info(`The room ${roomId} is temporary, scheduled room removal`);
      await roomStorageClient.setRoomExpireTime(roomId, TEMP_ROOM_EXPIRE_TIME, appMetaObjects);
    }

    // schedule end session by timer

    const now = new Date();

    let SESSION_TIMEOUT = serverEntityProvider.getConfig().project?.session?.timeout;
    if (typeof SESSION_TIMEOUT === 'undefined') {
      SESSION_TIMEOUT = 10;
    } // in minutes

    const session = {
      sessionId,
      serviceName,
    };

    if (sessionId) {
      logger.info(`No any users in room (${roomId}) with the session (${sessionId}), scheduled closing session`);

      await scheduleTasks.add({
        date: now.setMinutes(now.getMinutes() + SESSION_TIMEOUT),
        action: {
          ...session,
          timeSessionClosed: now.valueOf(),
          meta: {
            ...metadata,
            target: { roomId: roomState.id },
            time: new Date(),
            session,
            roomId,
          },
          // TODO: SETMEET-1419
          type: serverEntityProvider.getConstants().SERVICE_ACTIONS.END_SESSION,
        },
      });
    }
  }
};
