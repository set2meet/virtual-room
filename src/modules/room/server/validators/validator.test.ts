import { CValidator } from './validator';
import _cloneDeep from 'lodash/cloneDeep';

const RoomsValidator = new CValidator();

interface ICtxStub {
  method: string;
  url: string;
  body?: any;
}

const generateRequestOptions = (options: ICtxStub) => {
  return { ...options };
};

let ctxStub;
const urls = {
  room: '/rooms',
  common: '/rooms/someroomid',
  narrow: {
    settings: '/rooms/someroomid/settings',
    participants: '/rooms/someroomid/participants',
  },
};

const participantsRoomRequestJSON = [
  {
    displayName: 'Metting Partisipated',
    id: '6fbac08f-6f72-4ba7-b4dc-bb96b4095a77',
    role: 0,
  },
  {
    displayName: 'Particimant Met',
    id: '63bac08f-6f72-4ba7-b4dc-bb96b4095a78',
    role: 1,
  },
  {
    displayName: 'Parzticipant Meetingson',
    id: '5e0d2b52-3435-4275-89e8-cffe018eed45',
    role: 2,
  },
];

const settingsRoomRequestJSON = {
  set: 'up!',
};

const createRoomRequestJSON = {
  participants: participantsRoomRequestJSON,
  settings: settingsRoomRequestJSON,
};

enum httpMethods {
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  GET = 'GET',
}
describe('RoomValidator create room tests', () => {
  beforeEach(() => {
    ctxStub = {};
  });

  it('Validate room create data via schema with fully required data', () => {
    ctxStub = generateRequestOptions({ body: createRoomRequestJSON, url: urls.common, method: httpMethods.POST });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeTruthy();
  });

  it('Validate room create data via schema without participants', () => {
    const createRoomRequestWithoutParticipants = _cloneDeep(createRoomRequestJSON);

    createRoomRequestWithoutParticipants.participants = [];

    ctxStub = generateRequestOptions({
      body: createRoomRequestWithoutParticipants,
      url: urls.common,
      method: httpMethods.POST,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });

  it('Validate room create data via schema without settings', () => {
    const createRoomRequestWithoutSettings = _cloneDeep(createRoomRequestJSON);

    createRoomRequestWithoutSettings.settings = null;
    ctxStub = generateRequestOptions({
      body: createRoomRequestWithoutSettings,
      url: urls.common,
      method: httpMethods.POST,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });
});

describe('RoomValidator update room tests', () => {
  beforeEach(() => {
    ctxStub = {};
  });

  it('Validate room update data via schema with fully required data', () => {
    ctxStub = generateRequestOptions({ body: createRoomRequestJSON, url: urls.common, method: httpMethods.PUT });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeTruthy();
  });

  it('Validate room update data via schema without participants', () => {
    const updateRoomRequestWithoutParticipants = _cloneDeep(createRoomRequestJSON);

    updateRoomRequestWithoutParticipants.participants = [];

    ctxStub = generateRequestOptions({
      body: updateRoomRequestWithoutParticipants,
      url: urls.common,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });

  it('Validate room update data via schema without settings', () => {
    const updateRoomRequestWithoutSettings = _cloneDeep(createRoomRequestJSON);

    updateRoomRequestWithoutSettings.settings = null;

    ctxStub = generateRequestOptions({
      body: updateRoomRequestWithoutSettings,
      url: urls.common,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });
});

describe('RoomValidator update room participants', () => {
  beforeEach(() => {
    ctxStub = {};
  });

  it('Validate room participants update data via schema with fully required data', () => {
    const updateParticipantsRoomRequestJSON = _cloneDeep(createRoomRequestJSON);

    delete updateParticipantsRoomRequestJSON.settings;

    ctxStub = generateRequestOptions({
      body: updateParticipantsRoomRequestJSON,
      url: urls.narrow.participants,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeTruthy();
  });

  it('Validate room participants update data via schema with not all data presents', () => {
    const updateParticipantsRoomRequestJSON = _cloneDeep(createRoomRequestJSON);

    delete updateParticipantsRoomRequestJSON.settings;
    delete updateParticipantsRoomRequestJSON.participants;

    ctxStub = generateRequestOptions({
      body: updateParticipantsRoomRequestJSON,
      url: urls.narrow.participants,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });
});

describe('RoomValidator update room settings', () => {
  it('Validate room settings update data via schema with fully required data', () => {
    const updateParticipantsRoomRequestJSON = _cloneDeep(createRoomRequestJSON);

    delete updateParticipantsRoomRequestJSON.participants;

    ctxStub = generateRequestOptions({
      body: updateParticipantsRoomRequestJSON,
      url: urls.narrow.settings,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeTruthy();
  });

  it('Validate room settings update data via schema with not all data presents', () => {
    const updateParticipantsRoomRequestJSON = _cloneDeep(createRoomRequestJSON);

    delete updateParticipantsRoomRequestJSON.settings;
    delete updateParticipantsRoomRequestJSON.participants;

    ctxStub = generateRequestOptions({
      body: updateParticipantsRoomRequestJSON,
      url: urls.narrow.settings,
      method: httpMethods.PUT,
    });

    const result = RoomsValidator.validate(ctxStub);

    expect(result).toBeFalsy();
  });
});
