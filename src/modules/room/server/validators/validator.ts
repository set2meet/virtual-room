import { Room, RoomCommon, RoomNarrow } from '../schema/room.schema';
export class CValidator {
  public validationMap = {
    '/rooms': {
      GET: Room.getRoomsSchema,
      POST: Room.createRoomsSchema,
      PUT: () => false,
      DELETE: () => false,
    },
    '/rooms/:id': {
      GET: RoomCommon.getRoomSchema,
      POST: RoomCommon.createRoomSchema,
      PUT: RoomCommon.updateRoomSchema,
      DELETE: RoomCommon.deleteRoomSchema,
    },
    '/rooms/:id/settings': {
      GET: RoomNarrow.getSettingsSchema,
      POST: () => false,
      PUT: RoomNarrow.updateSettingsSchema,
      DELETE: () => false,
    },
    '/rooms/:id/participants': {
      GET: RoomNarrow.getParticipantsSchema,
      POST: () => false,
      PUT: RoomNarrow.updateParticipantsSchema,
      DELETE: () => false,
    },
  };

  public validate = (koaCtx: { url: string; method: string; body?: any }): boolean => {
    const { url, method, body } = koaCtx;
    const route = this.routerify(url);

    const validationBodyResult = this.validationMap[route][method].validate(body);

    return !(validationBodyResult.errors || validationBodyResult.error);
  };

  public routerify = (url: string) => {
    const pathSegments = url.split('/');

    pathSegments.shift();
    if (pathSegments.length === 1) {
      return `/${pathSegments[0]}`;
    }

    if (pathSegments.length === 2) {
      return `/${pathSegments[0]}/:id`;
    }

    // tslint:disable-next-line:no-magic-numbers
    if (pathSegments.length === 3) {
      return `/${pathSegments[0]}/:id/${pathSegments[2]}`;
    }
  };
}
