/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, createStore, Reducer } from 'redux';
import { IServerMetaObjects } from '../../../ioc/common/ioc-interfaces';
import roomActionTypes from '../common/redux/types/RoomAction';
import { addQueueTask } from './utils/roomActionsQueue/roomActionsQueue';
import { ROOM_EVENTS } from './types/constants/constants';
import { TASK_TYPE } from './utils/roomActionsQueue/types/constants';
import { IGlobalRoomReduxState } from '../common/redux/types/IGlobalRoomReduxState';
import { IApiRoomParticipant, IApiRoomSettings } from '../../../ioc/common/ioc-interfaces';
import { getRedisRoomId } from './utils/getRedisRoomId/getRedisRoomId';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

export interface IRoom {
  sendAction: (action: AnyAction) => Promise<void>;
  updateState: (newState: IGlobalRoomReduxState) => Promise<void>;
  getState: () => Promise<IGlobalRoomReduxState>;
}

// const rooms: Record<string, IRoom> = {};

/**
 * Create new redux storage for existing room and fill it with data from redis
 * Room exists checks must be done before this method is called.
 * Also subscribing storage for some unknown 4 me events.
 * Returns fulfilled room with its own methods
 * @param id
 * @param realmName
 * @param socketAdapter
 * @param subClient
 * @param pubClient
 * @param roomReducer
 * @return IRoom
 */
const newRoomStorage = async (
  id: string,
  { socketAdapter, redis: { subClient, pubClient }, config }: IServerMetaObjects,
  roomReducer: Reducer
) => {
  const getStateFromRedis = async (): Promise<IGlobalRoomReduxState> => {
    const roomState = await pubClient.get(getRedisRoomId(id));
    const result = JSON.parse(roomState);

    if (result.webrtcService) {
      const { serviceName } = result.webrtcService;
      const webrtcConfig = config.webrtc;
      const { isWebrtcServiceConfigured, getFirstAvailableWebrtcService } = serverEntityProvider.getUtils();
      // we use this small hack for backward compatibility
      if (serviceName && !isWebrtcServiceConfigured(serviceName, webrtcConfig)) {
        result.webrtcService.serviceName = getFirstAvailableWebrtcService(webrtcConfig);
      }
    }

    return result;
  };

  const store = createStore(roomReducer, await getStateFromRedis());

  const room: IRoom = {
    sendAction: async (action: AnyAction) =>
      await addQueueTask({
        type: TASK_TYPE.task,
        fn: async () => {
          store.dispatch({
            type: roomActionTypes.SET_ROOM,
            roomState: await getStateFromRedis(),
          });
          store.dispatch(action);
          await room.updateState(store.getState());
          socketAdapter.emit({
            target: { roomId: id },
            event: ROOM_EVENTS.ACTION,
            payload: action,
          });
        },
      }),
    updateState: async (newState: IGlobalRoomReduxState) => {
      const newStateString = JSON.stringify(newState);
      await pubClient.set(getRedisRoomId(id), newStateString);

      await pubClient.publish(`rooms:${id}:state`, newStateString);
    },
    getState: async () =>
      await addQueueTask<IGlobalRoomReduxState>({
        type: TASK_TYPE.getState,
        fn: async () => {
          store.dispatch({
            type: roomActionTypes.SET_ROOM,
            roomState: await getStateFromRedis(),
          });

          return store.getState();
        },
      }),
  };

  // subClient.subscribe(`rooms:${id}:actions`);
  await subClient.subscribe(`__keyspace@0__:rooms:${id}:actions`);

  const subListener = function subListenerFunc(channel: string, message: string) {
    if (channel === `__keyspace@0__:rooms:${id}:actions`) {
      if (message === 'expired') {
        subClient.removeListener('message', subListenerFunc);
        pubClient.del(getRedisRoomId(id));
      }
    }
  };

  subClient.on('message', subListener);

  return room;
};
/**
 * Room storage client for all actions with redis
 */
const roomStorageClient = {
  /**
   * Creating room state. If there is one already- merging and updating exist
   * @param id
   * @param isTemporary
   * @param userId
   * @param realm
   * @param appMetaObjects
   * @param roomReducer
   */

  createRoom: async (
    {
      id,
      isTemporary = false,
      userId,
    }: {
      id: string;
      isTemporary: boolean;
      userId: string;
    },
    appMetaObjects: IServerMetaObjects,
    roomReducer: Reducer
  ) => {
    const store = createStore(roomReducer, JSON.parse('{}'));
    const currentState = store.getState();
    const roomState = {
      ...currentState,
      state: {
        ...currentState.state,
        id,
        isTemporary,
        ownerId: userId,
      },
    };
    await addQueueTask({
      type: TASK_TYPE.setRoomState,
      fn: async () => {
        await appMetaObjects.redis.pubClient.set(getRedisRoomId(id), JSON.stringify(roomState));
      },
    });
  },

  createRoomAPI: async (
    {
      id,
      isTemporary = false,
      userId,
      settings,
      participants,
    }: {
      id: string;
      isTemporary: boolean;
      userId: string;
      settings?: IApiRoomSettings;
      participants?: IApiRoomParticipant[];
    },
    appMetaObjects: IServerMetaObjects,
    roomReducer: Reducer
  ) => {
    const store = createStore(roomReducer, JSON.parse('{}'));
    const currentState = store.getState();

    const roomState = {
      ...currentState,
      state: {
        ...currentState.state,
        id,
        isTemporary,
        ownerId: userId,
        settings: settings || {},
        participants: participants || [],
      },
    };
    await addQueueTask({
      type: TASK_TYPE.setRoomState,
      fn: async () => {
        await appMetaObjects.redis.pubClient.set(getRedisRoomId(id), JSON.stringify(roomState));
      },
    });
  },

  updateRoomAPI: async (
    {
      id,
      ownerId,
      isTemporary,
      settings,
      participants,
    }: {
      id: string;
      ownerId: string;
      isTemporary: boolean;
      settings?: IApiRoomSettings;
      participants?: IApiRoomParticipant[];
    },
    appMetaObjects: IServerMetaObjects
  ) => {
    const roomDataRaw: string = await appMetaObjects.redis.pubClient.get(getRedisRoomId(id));
    const roomData = JSON.parse(roomDataRaw);

    roomData.state.ownerId = ownerId;
    roomData.state.isTemporary = isTemporary;
    roomData.state.settings = settings;
    roomData.state.participants = participants;

    await addQueueTask({
      type: TASK_TYPE.task,
      fn: async () => {
        await appMetaObjects.redis.pubClient.set(getRedisRoomId(id), JSON.stringify(roomData));
      },
    });
  },

  updateRoomSettingsAPI: async (
    {
      id,
      settings,
    }: {
      id: string;
      settings: IApiRoomSettings;
    },
    appMetaObjects: IServerMetaObjects
  ) => {
    const roomDataRaw: string = await appMetaObjects.redis.pubClient.get(getRedisRoomId(id));
    const roomData = JSON.parse(roomDataRaw);

    roomData.state.settings = settings;

    await addQueueTask({
      type: TASK_TYPE.task,
      fn: async () => {
        await appMetaObjects.redis.pubClient.set(getRedisRoomId(id), JSON.stringify(roomData));
      },
    });
  },

  updateRoomParticipantsAPI: async (
    {
      id,
      participants,
    }: {
      id: string;
      participants: IApiRoomParticipant[];
    },
    appMetaObjects: IServerMetaObjects
  ) => {
    const roomDataRaw: string = await appMetaObjects.redis.pubClient.get(getRedisRoomId(id));
    const roomData = JSON.parse(roomDataRaw);
    roomData.state.participants = participants;

    await addQueueTask({
      type: TASK_TYPE.task,
      fn: async () => {
        await appMetaObjects.redis.pubClient.set(getRedisRoomId(id), JSON.stringify(roomData));
      },
    });
  },

  /**
   * Getting room state from redis if it is exists
   * @param id
   * @param appMetaObjects
   * @param roomReducer
   * @return IRoom || null
   */
  getRoom: async (id: string, appMetaObjects: IServerMetaObjects, roomReducer: Reducer) => {
    const isRoomExists = await appMetaObjects.redis.pubClient.exists(getRedisRoomId(id));
    if (isRoomExists) {
      return await newRoomStorage(id, appMetaObjects, roomReducer);
    }
    return null;
  },
  /**
   * Setting expire
   * @param id
   * @param time
   * @param appMetaObjects
   * @return void
   */
  setRoomExpireTime: async (id: string, time: number, appMetaObjects: IServerMetaObjects): Promise<void> => {
    const isRoomExists = await appMetaObjects.redis.pubClient.exists(getRedisRoomId(id));
    if (isRoomExists) {
      await addQueueTask({
        type: TASK_TYPE.setRoomExpire,
        fn: async () => {
          await appMetaObjects.redis.pubClient.expire(getRedisRoomId(id), time);
        },
      });
    }
  },
};

export default roomStorageClient;
