/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import serverMiddleware from './serverMiddleware';
import { IServerMetaObjects, SocketIOAdapter } from '../../../ioc/server/types/interfaces';
import Redis from 'ioredis-mock';
import IORedis from 'ioredis';
import roomStorageClient from './roomStorageClient';
import setupIoCServerForTest from '../../../test/utils/ioc/testIoCServer';
import { getRedisRoomId } from './utils/getRedisRoomId/getRedisRoomId';

beforeAll(setupIoCServerForTest());

const create = async (roomId: string, userId: string) => {
  const roomReducer = serverEntityProvider.getAppReducers().room;
  const logger = serverEntityProvider.getLogger();
  const socketAdapter: SocketIOAdapter = {
    emit: jest.fn(),
    getRoomClientSocket: jest.fn(),
    getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
  };
  const appMetaObjects: IServerMetaObjects = {
    socketAdapter,
    logger,
    redis: {
      pubClient: new Redis() as IORedis.Redis,
      subClient: new Redis() as IORedis.Redis,
    },
    dispatchAction: null,
    config: serverEntityProvider.getConfig(),
  };

  await roomStorageClient.createRoom(
    {
      id: roomId,
      isTemporary: true,
      userId,
    },
    appMetaObjects,
    roomReducer
  );

  const next = jest.fn();
  const middleware = serverMiddleware('state', roomReducer);
  const invoke = async (action) => {
    return middleware(
      {
        action: {
          ...action,
          meta: {
            target: {
              roomId,
            },
          },
        },
        appMetaObjects,
      },
      next
    );
  };
  return { invoke, next, appMetaObjects };
};

describe('serverMiddleware test', () => {
  const ROOM_ID = '123';
  const USER_ID = 'user123';

  it('should call next', async (done) => {
    const { next, invoke } = await create(ROOM_ID, USER_ID);
    const action = { type: 'TEST' };

    await invoke(action);
    expect(next).toHaveBeenCalledTimes(1);
    done();
  });

  it('should work synchronously', async (done) => {
    const { invoke, appMetaObjects } = await create(ROOM_ID, USER_ID);
    const webrtcService1 = {
      sessionId: '1234',
      serviceName: '@PEER_TO_PEER',
    };
    const webrtcService2 = {
      sessionId: '12345',
      serviceName: '@PEER_TO_PEER',
    };
    const webrtcServiceNull = {
      sessionId: null,
      serviceName: null,
    };
    const setFakeSessionAction1 = { type: 'WEBRTC_SESSION_STARTED', ...webrtcService1 };
    const setFakeSessionAction2 = { type: 'WEBRTC_SESSION_STARTED', ...webrtcService2 };
    const setNullSessionAction = { type: 'WEBRTC_SESSION_STARTED', ...webrtcServiceNull };

    await invoke(setFakeSessionAction1);
    await invoke(setNullSessionAction);
    await invoke(setNullSessionAction);
    await invoke(setNullSessionAction);
    await invoke(setNullSessionAction);
    await invoke(setFakeSessionAction2);

    const roomState = await appMetaObjects.redis.pubClient.get(getRedisRoomId(ROOM_ID));

    expect(JSON.parse(roomState)).toEqual(
      expect.objectContaining({
        webrtcService: webrtcService2,
      })
    );
    done();
  });
});
