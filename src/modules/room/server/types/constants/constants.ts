export enum ROOM_EVENTS {
  ACTION = 'action',
}

export enum PARTICIPANT_ROLES {
  DEFAULT,
  PARTICIPANT,
  OWNER,
}
