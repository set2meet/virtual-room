/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as Router from 'koa-router';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { IServerMetaObjects } from '../../../ioc/server/types/interfaces';
import { HttpStatusCode } from '../../../types/httpStatusCode';
import { Next } from 'koa';
import { createRoomManager } from './utils/RoomManager/RoomManager';
import { getRoomOwner } from './utils/getRoomOwner/getRoomOwner';
import {
  IApiRoomParticipant,
  IApiUpdateRoomAction,
  IApiUpdateRoomParticipantsAction,
  IApiUpdateRoomSettingsAction,
} from '../common/types/interfaces';
import roomActionCreators from '../common/redux/actionCreators/actionCreators';
import { v4 as guid } from 'uuid';
import { IRoomReduxState } from '../../../ioc/common/ioc-interfaces';
import { CValidator } from './validators/validator';

export default (router: Router, appMetaObjects: IServerMetaObjects) => {
  const RoomManager = createRoomManager(appMetaObjects.redis.pubClient);
  const RoomsValidator = new CValidator();

  const serverActionTypes = serverEntityProvider.getActionTypes();

  /**
   * Check rights to access realm management API
   */
  router.use('/rooms', async (ctx, next: Next) => {
    const { roles } = (ctx as any).req.user;
    const validate = RoomsValidator.validate(ctx.request);

    if (!validate) {
      ctx.status = HttpStatusCode.BAD_REQUEST;
      return ctx;
    }

    if (!RoomManager.userHasRole(roles)) {
      ctx.status = HttpStatusCode.UNAVAILABLE_FOR_LEGAL_REASONS;
      return ctx;
    }
    return next();
  });

  router.get('/rooms', async (ctx) => {
    ctx.body = await RoomManager.getRoomsData();
  });

  router.post('/rooms', async (ctx) => {
    const { settings, participants } = (ctx.request as any).body;
    const roomId = guid();
    const ownerId = getRoomOwner(participants);

    const room: object = await RoomManager.getRoomData(roomId);

    if (!room) {
      const body: any = (ctx.request as any).body;

      if (!ownerId || !settings || !participants) {
        ctx.status = HttpStatusCode.BAD_REQUEST;
        return ctx;
      }

      const payload = {
        ownerId,
        roomId,
        isTemporary: body?.settings?.isTemporary || true,
        settings: body?.settings || {},
        participants,
      };

      await new Promise((resolve) => {
        appMetaObjects.dispatchAction(roomActionCreators.apiCreateRoom(payload), async () => {
          ctx.body = await RoomManager.getRoomData(roomId);
          resolve();
        });
      });
    } else {
      ctx.status = HttpStatusCode.NOT_FOUND;
    }
  });

  /**
   * Get room data for specific room
   */
  router.get('/rooms/:roomId', async (ctx) => {
    const { roomId } = ctx.params;

    const room: IRoomReduxState = await RoomManager.getRoomData(roomId);

    if (room) {
      ctx.body = room;
    } else {
      ctx.status = HttpStatusCode.NOT_FOUND;
    }
  });

  /**
   * Creates room with passed parameters
   */
  router.post('/rooms/:roomId', async (ctx) => {
    const { roomId } = ctx.params;
    const { settings, participants } = (ctx.request as any).body;

    const ownerId = getRoomOwner(participants);
    const room: object = await RoomManager.getRoomData(roomId);

    if (!room) {
      if (!ownerId || !settings || !participants) {
        ctx.status = HttpStatusCode.BAD_REQUEST;
        return ctx;
      }

      const payload = {
        ownerId,
        roomId,
        isTemporary: settings?.isTemporary || true,
        settings: settings || {},
        participants,
      };

      await new Promise((resolve) => {
        appMetaObjects.dispatchAction(roomActionCreators.apiCreateRoom(payload), async () => {
          ctx.body = await RoomManager.getRoomData(roomId);
          resolve();
        });
      });
    } else {
      ctx.status = HttpStatusCode.NOT_FOUND;
    }
  });

  /**
   * Update room state
   */
  router.put('/rooms/:roomId', async (ctx) => {
    const { roomId } = ctx.params;
    const { roles } = (ctx as any).req.user;

    const room: IRoomReduxState = await RoomManager.getRoomData(roomId);

    if (!room) {
      ctx.status = HttpStatusCode.NOT_FOUND;
    } else {
      const { settings, participants } = (ctx.request as any).body;
      const ownerId = getRoomOwner(participants);

      if (!roles || !settings || !participants) {
        ctx.status = HttpStatusCode.BAD_REQUEST;
        return ctx;
      }

      const payload: IApiUpdateRoomAction = {
        ownerId,
        roomId,
        isTemporary: settings?.isTemporary || true,
        settings,
        participants,
      };

      await new Promise((resolve) => {
        appMetaObjects.dispatchAction(roomActionCreators.apiUpdateRoom(payload), async () => {
          ctx.body = await RoomManager.getRoomData(roomId);
          resolve();
        });
      });
    }
  });

  /**
   * Deleting room
   */
  router.delete('/rooms/:roomId', async (ctx) => {
    const { roomId } = ctx.params;

    const room: IRoomReduxState = await RoomManager.getRoomData(roomId);

    if (!room) {
      ctx.status = HttpStatusCode.NOT_FOUND;
      return ctx;
    } else {
      const action = {
        type: serverActionTypes.ROOM_DELETE,
        roomId,
      };

      await new Promise((resolve) => {
        appMetaObjects.dispatchAction(action, async () => {
          await RoomManager.deleteRoomData(roomId);
          ctx.status = HttpStatusCode.OK;
          resolve();
        });
      });
    }
  });

  /**
   * Get room settings
   */
  router.get('/rooms/:roomId/settings', async (ctx) => {
    const { roomId } = ctx.params;

    const room: IRoomReduxState = await RoomManager.getRoomData(roomId);

    if (room) {
      ctx.body = room.settings;
      ctx.body = room.settings;
    } else {
      ctx.status = HttpStatusCode.NOT_FOUND;
    }
  });

  /**
   * Update room settings
   */
  router.put('/rooms/:roomId/settings', async (ctx) => {
    const { roomId } = ctx.params;
    const { settings } = (ctx.request as any).body;

    const payload: IApiUpdateRoomSettingsAction = {
      settings,
      roomId,
    };

    await new Promise((resolve) => {
      appMetaObjects.dispatchAction(roomActionCreators.apiUpdateRoomSettings(payload), async () => {
        ctx.body = await RoomManager.getRoomData(roomId);
        resolve();
      });
    });
  });

  /**
   * Get room participants
   */
  router.get('/rooms/:roomId/participants', async (ctx) => {
    const { roomId } = ctx.params;

    const room: IRoomReduxState = await RoomManager.getRoomData(roomId);

    if (room) {
      ctx.body = room.participants;
    } else {
      ctx.status = HttpStatusCode.NOT_FOUND;
    }
  });

  /**
   * Update room participants
   */
  router.put('/rooms/:roomId/participants', async (ctx) => {
    const { roomId } = ctx.params;
    const { participants } = (ctx.request as any).body;

    const payload: IApiUpdateRoomParticipantsAction = {
      participants,
      roomId,
    };

    await new Promise((resolve) => {
      appMetaObjects.dispatchAction(roomActionCreators.apiUpdateRoomParticipants(payload), async () => {
        ctx.body = await RoomManager.getRoomData(roomId);
        resolve();
      });
    });
  });

  /**
   * Add room participant
   */
  router.post('/rooms/:roomId/participants', async (ctx) => {
    const { roomId } = ctx.params;
    const { participants } = ctx.request.body;

    const roomData = await RoomManager.getRoomData(roomId);

    const _participants = roomData.participants;

    if (Array.isArray(participants) && participants.length > 0) {
      participants.forEach((participant: IApiRoomParticipant) => {
        _participants.push(participant);
      });
    }

    const action = {
      type: serverActionTypes.API_ROOM_UPDATE_PARTICIPANTS,
      participants: _participants,
      roomId,
    };

    await new Promise((resolve) => {
      appMetaObjects.dispatchAction(action, async () => {
        ctx.body = await RoomManager.getRoomData(roomId);
        resolve();
      });
    });
  });

  /**
   * Delete room participant
   */
  router.delete('/rooms/:roomId/participants/:participantId', async (ctx) => {
    const { roomId, participantId } = ctx.params;
    const { participants } = (ctx.request as any).body;

    const roomData = await RoomManager.getRoomData(roomId);
    let _participants = roomData.participants;

    if (Array.isArray(participants) && participants.length > 0) {
      _participants = _participants.filter((participant: IApiRoomParticipant) => participant?.id !== participantId);
    }

    const payload: IApiUpdateRoomParticipantsAction = {
      roomId,
      participants: _participants,
    };

    await new Promise((resolve) => {
      appMetaObjects.dispatchAction(roomActionCreators.apiUpdateRoomParticipants(payload), async () => {
        ctx.body = await RoomManager.getRoomData(roomId);
        resolve();
      });
    });
  });
};
