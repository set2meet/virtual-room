/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';
import _pick from 'lodash/pick';
import { IServerMiddleware, IServerMiddlewareContext } from '../../../ioc/common/ioc-interfaces';
import roomActionTypes from '../common/redux/types/RoomAction';
import roomStorageClient from './roomStorageClient';
import { Reducer } from 'redux';
import _get from 'lodash/get';
import _set from 'lodash/set';
import _unset from 'lodash/unset';
import _isEmpty from 'lodash/isEmpty';
import ROOM_JOIN_ERROR from '../common/types/constants/RoomJoinError';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import ScheduleTasks from './utils/scheduleTasks/scheduleTasks';
import { createRoomIfNotExists } from './utils/createRoomIfNotExists/createRoomIfNotExists';
import { sendActionsByTargets } from './utils/sendActionsByTargets/sendActionByTargets';
import { setSuspendRecordingValue } from './utils/setSuspendRecordingValue/setSuspendRecordingValue';
import { joinRoomError } from './utils/joinRoomError/joinRoomError';
import { isRoomFull } from './utils/isRoomFull/isRoomFull';
import { userColorFor } from './utils/userColorFor/userColorFor';
import { scheduleClosingEmptySession } from './utils/scheduleClosingEmptySession/scheduleClosingEmptySession';
import { IRoomInternalAction } from '../common/types/interfaces';
import { RecordingAction } from '../../../ioc/server/ioc-constants';
import { getRedisRoomId } from './utils/getRedisRoomId/getRedisRoomId';

export let roomReducer: Reducer = null;
export let storeKey: string = '';
export let scheduleTasks: ScheduleTasks = null;

// hash map to track user connections
// forbid to connect user to single room twice
const roomConnections = {};

type TMiddlewareEnum = Record<string, (context: IServerMiddlewareContext) => void>;

let cachedMiddlewareEnum: TMiddlewareEnum;

const getMiddlewareEnum = (): TMiddlewareEnum => {
  if (!cachedMiddlewareEnum) {
    cachedMiddlewareEnum = {
      [roomActionTypes.ROOM_CREATE]: async ({ action, clientSocket, appMetaObjects }) => {
        action.roomId = await createRoomIfNotExists(action, appMetaObjects);
      },
      [roomActionTypes.API_ROOM_CREATE]: async ({ action, clientSocket, appMetaObjects }) => {
        action.roomId = await createRoomIfNotExists(action, appMetaObjects, true);
      },

      [roomActionTypes.API_ROOM_UPDATE]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await appMetaObjects.redis.pubClient.get(getRedisRoomId(action.roomId));
        if (room) {
          await roomStorageClient.updateRoomAPI(
            {
              id: action.roomId,
              ownerId: action.ownerId,
              isTemporary: action.settings?.isTemporary || true,
              settings: action.settings,
              participants: action.participants,
            },
            appMetaObjects
          );
        }
      },
      [roomActionTypes.API_ROOM_UPDATE_SETTINGS]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await appMetaObjects.redis.pubClient.get(getRedisRoomId(action.roomId));
        if (room) {
          await roomStorageClient.updateRoomSettingsAPI(
            {
              id: action.roomId,
              settings: action.settings,
            },
            appMetaObjects
          );
        }
      },
      [roomActionTypes.API_ROOM_UPDATE_PARTICIPANTS]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await appMetaObjects.redis.pubClient.get(getRedisRoomId(action.roomId));
        if (room) {
          await roomStorageClient.updateRoomParticipantsAPI(
            {
              id: action.roomId,
              participants: action.participants,
            },
            appMetaObjects
          );
        }
      },
      [roomActionTypes.ROOM_CREATE_JOIN]: async ({ action, clientSocket, appMetaObjects }) => {
        await createRoomIfNotExists(action, appMetaObjects);

        action.type = roomActionTypes.ROOM_JOIN;
        action.meta.target = { users: [action.userId] };
      },
      [roomActionTypes.ROOM_GET_INFO]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await roomStorageClient.getRoom(action.roomId, appMetaObjects, roomReducer);
        const { fields } = action;

        if (room) {
          action.roomState = _pick(await room.getState(), fields);
        } else {
          action.roomState = {};
          joinRoomError(clientSocket, ROOM_JOIN_ERROR.WRONG_ID);
        }

        action.type = roomActionTypes.ROOM_SET_INFO;
        action.meta.target = { users: [action.meta.user.id] };

        delete action.fields;
      },
      [roomActionTypes.ROOM_JOIN]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await roomStorageClient.getRoom(action.roomId, appMetaObjects, roomReducer);

        if (!room) {
          return joinRoomError(clientSocket, ROOM_JOIN_ERROR.WRONG_ID);
        }

        // check: double entrance
        const storedSessionId = _get(roomConnections, `${action.roomId}.${action.userId}`);

        if (storedSessionId && storedSessionId !== action.sessionId) {
          return joinRoomError(clientSocket, ROOM_JOIN_ERROR.ALREADY_JOINED);
        }

        // check: full room
        const currRoomState = await room.getState();

        if (isRoomFull(action.userId, currRoomState)) {
          return joinRoomError(clientSocket, ROOM_JOIN_ERROR.IS_FULL);
        }

        // enter the room
        await (() => {
          return new Promise((resolve) => {
            clientSocket.join(`rooms/${action.roomId}`, resolve);
          });
        })();

        clientSocket.emit('action', {
          action: {
            type: roomActionTypes.SET_ROOM,
            roomState: await room.getState(),
          },
        });

        _set(roomConnections, `${action.roomId}.${action.userId}`, action.sessionId);

        await room.sendAction({
          type: roomActionTypes.USER_JOIN_ROOM,
          userColor: await userColorFor(room, action.userId),
          userId: action.userId,
          roomId: action.roomId,
        });

        // If some1 joined - reset expire timer
        // await roomStorageClient.setRoomExpireTime(action.roomId, -1, appMetaObjects);

        clientSocket.on('disconnect', async () => {
          _unset(roomConnections, `${action.roomId}.${action.userId}`);

          appMetaObjects.dispatchAction(
            {
              type: roomActionTypes.ROOM_LEAVE,
              userId: action.userId,
              roomId: action.roomId,
            },
            _noop
          );
        });

        // check scheduled tasks
        const { sessionId } = currRoomState.webrtcService;

        if (sessionId) {
          // if user enter to the session, and session scheduled to close
          // find this task and remove it from scheduling
          const scheduledTask = scheduleTasks.findTask({ sessionId });

          if (scheduledTask) {
            scheduleTasks.del(scheduledTask.id);
          }
        }
      },
      [RecordingAction.ROOM_RECORDING_RECORD_UPDATED]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await roomStorageClient.getRoom(action.record.roomId, appMetaObjects, roomReducer);

        if (room) {
          await room.sendAction(action);
        }
      },
      [RecordingAction.ROOM_RECORDING_RECORD_REMOVED]: async ({ action, clientSocket, appMetaObjects }) => {
        const room = await roomStorageClient.getRoom(action.record.roomId, appMetaObjects, roomReducer);

        if (room) {
          await room.sendAction(action);
        }
      },
      [RecordingAction.ROOM_RECORDING_SUSPENDED]: async ({ action, clientSocket, appMetaObjects }) => {
        return setSuspendRecordingValue(action.recordId, appMetaObjects, true);
      },
      [RecordingAction.ROOM_RECORDING_RESUMED]: async ({ action, clientSocket, appMetaObjects }) => {
        return setSuspendRecordingValue(action.recordId, appMetaObjects, false);
      },
      [roomActionTypes.ROOM_LEAVE]: async ({ action, clientSocket, appMetaObjects }) => {
        const { roomId, userId } = action;
        const room = await roomStorageClient.getRoom(roomId, appMetaObjects, roomReducer);

        if (!room) {
          return;
        }

        const { recording, state: roomState } = await room.getState();
        const { isRecording, recId: recordId } = recording;
        const isHost = roomState.ownerId === userId;

        if (isHost && isRecording) {
          await room.sendAction({ type: serverEntityProvider.getActionTypes().RECORDING_SUSPEND, recordId });
        }

        if (clientSocket) {
          clientSocket.emit('action', { action });
        }

        await room.sendAction({ type: roomActionTypes.USER_LEFT_ROOM, roomId, userId });

        await new Promise<void>((resolve) => {
          if (clientSocket) {
            clientSocket.removeAllListeners('disconnect');
            clientSocket.leave(`rooms/${action.roomId}`, resolve);
          } else {
            resolve();
          }
        });

        _unset(roomConnections, `${action.roomId}.${action.userId}`);

        await scheduleClosingEmptySession(appMetaObjects, roomId, userId, action.meta);
      },
      [roomActionTypes.ROOM_DELETE]: async ({ action, clientSocket, appMetaObjects }) => {
        const { roomId } = action;
        const room = await roomStorageClient.getRoom(roomId, appMetaObjects, roomReducer);

        if (!room) {
          return;
        }

        // todo::put here check if recording in process then prohibit deleting
        // const { recording, state: roomState } = await room.getState();
        // const { isRecording, recId: recordId } = recording;

        const connectedUsersMap = roomConnections[roomId];

        if (!_isEmpty(connectedUsersMap)) {
          const connectedUsers = Object.keys(connectedUsersMap);

          if (!_isEmpty(connectedUsers)) {
            await room.sendAction({ type: roomActionTypes.ROOM_DELETE, roomId });
          }
        }
        _unset(roomConnections, `${action.roomId}`);
      },
    };
  }

  return cachedMiddlewareEnum;
};

const roomServerMiddleware: IServerMiddleware = async (data, next) => {
  const { action, appMetaObjects } = data;
  const { logger } = appMetaObjects;
  const { type } = action;
  const middlewareEnum = getMiddlewareEnum();

  if (scheduleTasks === null) {
    scheduleTasks = new ScheduleTasks(appMetaObjects.redis.pubClient);
    scheduleTasks.onTask((task) => {
      logger.roomServer.info(`Room scheduler: run action ${task.action.type}`);
      data.appMetaObjects.dispatchAction(task.action, () => scheduleTasks.del(task.id));
    });
  }

  if (middlewareEnum[type]) {
    await middlewareEnum[type](data);
  }

  await sendActionsByTargets(action as IRoomInternalAction, appMetaObjects);

  next();
};

export default (roomStoreKey: string, AppRoomReducer: Reducer) => {
  storeKey = roomStoreKey;
  roomReducer = AppRoomReducer;

  return roomServerMiddleware;
};
