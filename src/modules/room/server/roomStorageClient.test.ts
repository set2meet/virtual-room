/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import roomStorageClient from './roomStorageClient';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { Reducer } from 'redux';
import { IServerMetaObjects, SocketIOAdapter, IServerLogger } from '../../../ioc/server/types/interfaces';
import Redis from 'ioredis-mock';
import IORedis from 'ioredis';
import setupIoCServerForTest from '../../../test/utils/ioc/testIoCServer';
import { mock } from 'jest-mock-extended';
import { getRedisRoomId } from './utils/getRedisRoomId/getRedisRoomId';

beforeAll(setupIoCServerForTest());

const sendAction = async (action: any, appMetaObjects: IServerMetaObjects, roomId: string, roomReducer: Reducer) => {
  const room = await roomStorageClient.getRoom(roomId, appMetaObjects, roomReducer);

  if (room) {
    await room.sendAction(action);
  }
};

const initAppMetaObjects = (): IServerMetaObjects => {
  const logger = mock<IServerLogger>();
  const socketAdapter: SocketIOAdapter = {
    emit: jest.fn(),
    getRoomClientSocket: jest.fn(),
    getRoomParticipants: jest.fn().mockImplementation((query) => Promise.resolve(query)),
  };
  const config = serverEntityProvider.getConfig();

  return {
    socketAdapter,
    logger,
    redis: {
      pubClient: new Redis() as IORedis.Redis,
      subClient: new Redis() as IORedis.Redis,
    },
    dispatchAction: null,
    config,
  };
};

describe('roomStorageClient', () => {
  const roomId = '123';
  let appMetaObjects: IServerMetaObjects = null;

  beforeAll(async () => {
    // ioc should be ready before this call
    appMetaObjects = initAppMetaObjects();
    const roomReducer = serverEntityProvider.getAppReducers().room;

    await roomStorageClient.createRoom(
      {
        id: '123',
        isTemporary: true,
        userId: 'user123',
      },
      appMetaObjects,
      roomReducer
    );
  });

  const webrtcService = {
    sessionId: '123',
    serviceName: '@PEER_TO_PEER',
  };
  const webrtcServiceNull = {
    sessionId: null,
    serviceName: null,
  };

  const setFakeSessionAction = { type: 'WEBRTC_SESSION_STARTED', ...webrtcService };
  const setNullSessionAction = { type: 'WEBRTC_SESSION_STARTED', ...webrtcServiceNull };

  it('should work synchronously', async (done) => {
    const roomReducer = serverEntityProvider.getAppReducers().room;

    await sendAction(setNullSessionAction, appMetaObjects, '123', roomReducer);
    await sendAction(setNullSessionAction, appMetaObjects, '123', roomReducer);
    await sendAction(setNullSessionAction, appMetaObjects, '123', roomReducer);
    await sendAction(setNullSessionAction, appMetaObjects, '123', roomReducer);
    await sendAction(setFakeSessionAction, appMetaObjects, '123', roomReducer);

    const roomState = await appMetaObjects.redis.pubClient.get(getRedisRoomId(roomId));

    expect(JSON.parse(roomState)).toEqual(
      expect.objectContaining({
        webrtcService,
      })
    );
    done();
  });

  it('check updateState function', async (done) => {
    const roomReducer = serverEntityProvider.getAppReducers().room;

    const room1 = await roomStorageClient.getRoom('123', appMetaObjects, roomReducer);
    const room2 = await roomStorageClient.getRoom('123', appMetaObjects, roomReducer);
    const updateState1 = jest.spyOn(room1, 'updateState');
    const updateState2 = jest.spyOn(room2, 'updateState');

    await room1.sendAction(setFakeSessionAction);
    await room2.sendAction(setNullSessionAction);

    expect(updateState1).toBeCalledTimes(1);
    expect(updateState1).toBeCalledWith(
      expect.objectContaining({
        webrtcService,
      })
    );
    expect(updateState2).toBeCalledTimes(1);
    expect(updateState2).toBeCalledWith(
      expect.objectContaining({
        webrtcService: webrtcServiceNull,
      })
    );

    const roomState = await appMetaObjects.redis.pubClient.get(getRedisRoomId(roomId));

    expect(JSON.parse(roomState)).toEqual(
      expect.objectContaining({
        webrtcService: webrtcServiceNull,
      })
    );

    done();
  });
});
