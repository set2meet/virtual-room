import Joi from 'joi';

export const participantsSchema = Joi.array()
  .items(
    Joi.object({
      id: Joi.string().required(),
      displayName: Joi.string().optional(),
      role: Joi.number().required(),
    }).required()
  )
  .required()
  .description('participants array');
