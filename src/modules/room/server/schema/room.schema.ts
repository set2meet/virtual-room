import Joi from 'joi';
import { participantsSchema } from './participants.schema';
import { settingsSchema } from './settings.schema';

export const headersValidationSchema = Joi.object({
  headers: Joi.object({
    token: Joi.string().required(),
  }),
});

export const Room = {
  getRoomsSchema: Joi.object({
    filter: Joi.string().optional().description('filter to retrieve specific data'),
    query: Joi.string().optional().description('query params'),
  }),
  createRoomsSchema: Joi.object({
    settings: settingsSchema,
    participants: participantsSchema,
  }),
};

export const RoomCommon = {
  createRoomSchema: Joi.object({
    settings: settingsSchema,
    participants: participantsSchema,
  }),
  getRoomSchema: Joi.object({}),

  updateRoomSchema: Joi.object({
    settings: settingsSchema,
    participants: participantsSchema,
  }),

  deleteRoomSchema: Joi.object({}),
};
export const RoomNarrow = {
  getSettingsSchema: Joi.object({}),

  updateSettingsSchema: Joi.object({
    settings: settingsSchema,
  }),

  getParticipantsSchema: Joi.object({}),

  updateParticipantsSchema: Joi.object({
    participants: participantsSchema,
  }),
};
