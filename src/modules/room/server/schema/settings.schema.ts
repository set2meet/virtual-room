import Joi from 'joi';

export const settingsSchema = Joi.object().required().description('settings object');
