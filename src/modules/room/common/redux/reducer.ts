/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import roomActionTypes from './types/RoomAction';
import { Action, AnyAction, Reducer } from 'redux';
import _without from 'lodash/without';
import { IRoomReduxState } from '../../../../ioc/common/ioc-interfaces';
import { IApiUpdateRoomAction } from '../types/interfaces';

const INITIAL_STATE: IRoomReduxState = {
  id: '',
  participants: [],
  ownerId: null,
  isTemporary: false,
  currentTool: '',
  error: {
    code: NaN,
  },
  userColors: {},
};

const reducers: Record<string, Reducer<IRoomReduxState>> = {};

reducers[roomActionTypes.SET_ROOM_OWNER] = (state: IRoomReduxState, action: AnyAction) => {
  return {
    ...state,
    ownerId: action.id,
  };
};

reducers[roomActionTypes.USER_JOIN_ROOM] = (state: IRoomReduxState, action: AnyAction) => {
  const { userId, userColor } = action;
  const participants = state.participants.slice(0);
  const userNotInTheRoom = state.participants.indexOf(userId) === -1;

  if (userNotInTheRoom) {
    participants.push(userId);
  }

  return {
    ...state,
    participants,
    userColors: {
      ...state.userColors,
      [userId]: userColor,
    },
  };
};

reducers[roomActionTypes.ROOM_UPDATE] = (state: IRoomReduxState, action: Action<string> & IApiUpdateRoomAction) => {
  const { settings } = action;

  return {
    ...state,
    ...settings,
  };
};

reducers[roomActionTypes.ROOM_DELETE] = () => {
  return INITIAL_STATE;
};

reducers[roomActionTypes.ROOM_LEAVE] = () => {
  return INITIAL_STATE;
};

reducers[roomActionTypes.USER_LEFT_ROOM] = (state: IRoomReduxState, action: AnyAction) => {
  return {
    ...state,
    participants: _without(state.participants, action.userId),
  };
};

reducers[roomActionTypes.ROOM_CANNOT_JOIN] = (state: IRoomReduxState, action: AnyAction) => {
  return {
    ...state,
    id: null,
    error: {
      code: action.errorCode,
    },
  };
};

reducers[roomActionTypes.ROOM_SET_CURRENT_TOOL] = (state: IRoomReduxState, action: AnyAction) => {
  return {
    ...state,
    currentTool: action.tool,
  };
};

export default (state = INITIAL_STATE, action: AnyAction): object => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
