/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { Action, ActionCreator, AnyAction } from 'redux';
import {
  IApiCreateRoomAction,
  IApiUpdateRoomAction,
  IApiUpdateRoomParticipantsAction,
  IApiUpdateRoomSettingsAction,
} from '../../types/interfaces';

export interface IRoomActionCreators {
  createJoinRoom: ActionCreator<AnyAction>;
  joinRoom: ActionCreator<AnyAction>;
  leaveRoom: ActionCreator<AnyAction>;
  setCurrentTool: ActionCreator<AnyAction>;
  getRoomInfo: ActionCreator<AnyAction>;
  deleteRoom: ActionCreator<AnyAction>;
  updateRoom: ActionCreator<AnyAction>;
  apiCreateRoom: ActionCreator<Action<string> & IApiCreateRoomAction>;
  apiUpdateRoom: ActionCreator<Action<string> & IApiUpdateRoomAction>;
  apiUpdateRoomSettings: ActionCreator<Action<string> & IApiUpdateRoomSettingsAction>;
  apiUpdateRoomParticipants: ActionCreator<Action<string> & IApiUpdateRoomParticipantsAction>;
}
