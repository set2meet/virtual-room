/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

enum RoomAction {
  API_ROOM_CREATE = 'ROOM:API:ROOM_CREATE',
  API_ROOM_UPDATE = 'ROOM:API:ROOM_UPDATE',
  API_ROOM_UPDATE_SETTINGS = 'ROOM:API:ROOM_UPDATE_SETTINGS',
  API_ROOM_UPDATE_PARTICIPANTS = 'ROOM:API:ROOM_UPDATE_PARTICIPANTS',
  ROOM_CREATE = 'ROOM:ROOM_CREATE',
  ROOM_CREATE_JOIN = 'ROOM:ROOM_CREATE_JOIN',
  ROOM_JOIN = 'ROOM:ROOM_JOIN',
  ROOM_JOIN_SUCCESS = 'ROOM:ROOM_JOIN_SUCCESS',
  ROOM_DELETE = 'ROOM:ROOM_DELETE',
  ROOM_UPDATE = 'ROOM:ROOM_UPDATE',
  ROOM_GET_INFO = 'ROOM:GET_INFO',
  ROOM_SET_INFO = 'ROOM:SET_INFO',
  ROOM_LEAVE = 'ROOM:ROOM_LEAVE',
  SET_ROOM = 'ROOM:SET_ROOM',
  USER_LEFT_ROOM = 'ROOM:USER_LEFT_ROOM',
  USER_JOIN_ROOM = 'ROOM:USER_JOIN_ROOM',
  SET_ROOM_OWNER = 'ROOM:SET_ROOM_OWNER',
  ROOM_CANNOT_JOIN = 'ROOM:ROOM_CANNOT_JOIN',
  ROOM_SET_CURRENT_TOOL = 'ROOM:ROOM_SET_CURRENT_TOOL',
}

export default RoomAction;

export type IRoomActionTypes = typeof RoomAction;
