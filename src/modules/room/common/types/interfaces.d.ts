export interface IRoomInternalAction {
  type: string;
  level?: string;
  data?: {
    moduleName: string;
    message: string;
  };
  meta: {
    target?: any;
    user?: {
      id?: string;
      name?: string;
      realmName?: string;
    };
  };
  session?: string | null;
  sender?: string;
  hostname?: string;
  roomId?: string;
  time?: string;
  browserId?: string;
  isHost?: boolean;
  clientSocketId?: string;
}

export interface IApiRoomParticipant {
  role?: string | number;
  id: string;
  displayName: string;
}

// tslint:disable-next-line:no-empty-interface
export interface IApiRoomSettings {}

interface IApiCommonAction {
  roomId: string;
}

export interface IApiCreateRoomAction extends IApiCommonAction {
  ownerId: string;
  isTemporary?: boolean;
  settings?: IApiRoomSettings;
  participants?: IApiRoomParticipant[];
}

export interface IApiUpdateRoomAction extends IApiCommonAction {
  ownerId: string;
  isTemporary?: boolean;
  settings?: IApiRoomSettings;
  participants?: IApiRoomParticipant[];
}

export interface IApiUpdateRoomSettingsAction extends IApiCommonAction {
  settings: IApiRoomSettings;
}

export interface IApiUpdateRoomParticipantsAction extends IApiCommonAction {
  participants: IApiRoomParticipant[];
}
