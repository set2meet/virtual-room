/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import actionCreators from '../common/redux/actionCreators/actionCreators';
import { IVRModule } from '../../../ioc/client/types/interfaces';
import { IRoomActionCreators } from '../common/redux/actionCreators/IRoomActionCreators';
import RoomAction, { IRoomActionTypes } from '../common/redux/types/RoomAction';

interface IRoom extends IVRModule<IRoomActionCreators> {
  actionTypes: IRoomActionTypes;
  actionCreators: IRoomActionCreators;
}

const roomClientModule: IRoom = {
  actionCreators,
  actionTypes: RoomAction,
};

export default roomClientModule;
