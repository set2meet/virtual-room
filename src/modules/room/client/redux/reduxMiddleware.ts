/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import { AnyAction, Dispatch } from 'redux';
import { push } from 'connected-react-router';
import _get from 'lodash/get';
import roomActionTypes from '../../common/redux/types/RoomAction';
import Socket = SocketIOClient.Socket;

const roomMiddlewareEnum: IAppReduxEnumMiddleware = {
  [roomActionTypes.ROOM_LEAVE](store, next, action) {
    next(action);
    store.dispatch(
      push({
        pathname: `/`,
      })
    );
  },
  [roomActionTypes.USER_LEFT_ROOM](store, next, action) {
    // do nothing if we closed another tab with this room
    if (_get(action, 'meta.user.id') !== action.userId) {
      next(action);
    }
  },
};

const roomMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  if (roomMiddlewareEnum[action.type]) {
    return roomMiddlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default roomMiddleware;
