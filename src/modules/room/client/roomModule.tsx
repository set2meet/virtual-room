/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import Socket = SocketIOClient.Socket;
import reducer from '../common/redux/reducer';
import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import reduxMiddleware from './redux/reduxMiddleware';

const createRoomModule: TS2MModuleFactory = async (socket: Socket, stateKey: string = 'room'): Promise<IS2MModule> => {
  return {
    rootComponent: null,
    reduxModules: [
      {
        id: stateKey,
        reducersToMerge: {
          [stateKey]: {
            state: reducer,
            // we use fake reducer here in order to be able to get the state
            // from the server before initializing the module
            recording: (state) => state || null,
            presentations: (state) => state || null,
            graph2d: (state) => state || null,
            chat: (state) => state || null,
            webphone: (state) => state || null,
            whiteboard: (state) => state || null,
            webrtcService: (state) => state || null,
          },
        },
        middlewares: [reduxMiddleware(stateKey)(socket)],
      },
    ],
  };
};

export default createRoomModule;
