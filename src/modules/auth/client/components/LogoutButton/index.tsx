import _noop from 'lodash/noop';
import * as React from 'react';
import { connect } from 'react-redux';
import authActionCreators from '../../redux/actionCreators';
import { ActionCreator, AnyAction } from 'redux';
import { IAuthButtonLogout } from './IAuthButtonLogout';

interface ILogoutButtonDispatchProps {
  logout: ActionCreator<AnyAction>;
}

export class LogoutButton extends React.Component<IAuthButtonLogout & ILogoutButtonDispatchProps> {
  public static defaultProps = {
    onClick: _noop,
  };

  private _doLogout = () => {
    this.props.onClick();
    this.props.logout();
  };

  public render() {
    const HighOrderComponent = this.props.component;

    if (!HighOrderComponent) {
      return null;
    }

    return (
      <HighOrderComponent title="logout" onClick={this._doLogout}>
        {this.props.children}
      </HighOrderComponent>
    );
  }
}

export default connect(null, {
  logout: authActionCreators.logout,
})(LogoutButton);
