import React from 'react';
import SwitchStrategyComponent from './SwitchStrategy';
import { action } from '@storybook/addon-actions';

export default {
  title: 'auth/SwitchStrategy',
  component: SwitchStrategyComponent,
};

export const Default = () => (
  <SwitchStrategyComponent onClick={action('onClick')}>Sign in with EPAM SSO</SwitchStrategyComponent>
);
