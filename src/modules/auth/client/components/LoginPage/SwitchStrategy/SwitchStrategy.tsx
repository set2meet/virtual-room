import React from 'react';
import styled from 'styled-components';

const SwitchStrategy = styled.div`
  button {
    color: #39c2d7 !important;
    text-decoration: underline;
  }

  button:hover {
    color: #39c2d7 !important;
  }
`;

const SwitchStrategyComponent = (props: {
  children: React.ReactNode | string;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}) => {
  return (
    <SwitchStrategy className="text-center">
      <button type="button" className="btn btn-link" onClick={props.onClick}>
        {props.children}
      </button>
    </SwitchStrategy>
  );
};

export default SwitchStrategyComponent;
