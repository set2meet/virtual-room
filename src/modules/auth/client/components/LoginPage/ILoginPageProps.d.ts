/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export interface ILoginPageProps {
  showGuestScreen: boolean;
  reconnectGuest: boolean; // reconnect user with guest token
  authParams: any; // TODO:: type here
  authSwitcher?: any;
  serviceParams?: any;
  keycloak?: any; // TODO:: type here
  auth?: any; // redux state already
  showNotification?: any; // redux state already
}
