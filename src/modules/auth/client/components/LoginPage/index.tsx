import React, { KeyboardEvent } from 'react';
import { FormControl, FormGroup } from 'react-bootstrap';
import styled, { css } from 'styled-components';
import { connect } from 'react-redux';
import authActionCreators from '../../redux/actionCreators';
import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import SwitchStrategyComponent from './SwitchStrategy/SwitchStrategy';
import { RouterState } from 'connected-react-router';
import { IAuthReduxState } from '../../redux/types/IAuthReduxState';
import { ILoginPageProps } from './ILoginPageProps';
import { IAuthActionCreators } from '../../redux/types/IAuthActionCreators';

interface ILoginStateProps {
  errorMessage: string;
  router: RouterState;
  auth: IAuthReduxState;
  remoteServiceUrl?: any;
  authSwitcher?: any;
}

/**
 * Extending for saving flexibility of dispatched props.
 */
export interface ILoginDispatchProps
  extends Pick<
    IAuthActionCreators,
    'loginWithOauth2' | 'loginChangeParams' | 'login' | 'loginWithToken' | 'loginGuest'
  > {}

const StyledLogin = styled.div`
  ${({ theme }: IRequiredTheme) => css`
    form {
      font-size: 22px;
      margin: 0 auto;
      width: 400px;
    }

    .login-error {
      color: ${theme.fontErrorColor};
      font-size: 16px;
      height: 40px;
      text-align: center;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;

      .glyphicon-alert {
        font-size: 12px;
        margin-right: 10px;
      }
    }

    .change-guest-mode {
      margin-top: 90px;
      font-size: 18px;
      text-align: center;
    }
  `};
`;

class LoginPage extends React.Component<ILoginPageProps & ILoginDispatchProps & ILoginStateProps> {
  public state = {
    password: '',
    username: '',
    realm: 'Set2Meet',
    loading: false,
    guestMode: false,
  };

  constructor(props: ILoginPageProps & ILoginDispatchProps & ILoginStateProps) {
    super(props);
  }

  public componentDidMount(): void {
    this.fakeGuestWatcher();
    this.ssoLoginWatcher();
  }

  /**
   * When we redirected back from SSO we have a state inside get params.
   * If we had all 3 params, then, it was attempt to login.
   * For this moment i dont know need we this or not, but it should be handled after we init keycloak adapter
   * Then adapter will remove it from url
   */
  public ssoLoginWatcher = (): void => {
    const hash: URL = new URL(window.location.toString().replace('#', '?'));
    const state = hash.searchParams.get('state');
    const sessionState = hash.searchParams.get('session_state');
    const code = hash.searchParams.get('code');

    if (state && sessionState && code) {
      this.loginSSO();
    }
  };

  public fakeGuestWatcher = (): void => {
    const available = true;

    if (available) {
      const url = new URL(`${window.location}`);

      const params = new URLSearchParams(url.search);
      const mode = params.get('mode');
      const guestName = params.get('guest');

      if (guestName && mode === 'test') {
        this.props.loginGuest({ name: guestName, fakeGuest: true });
      }

      if (mode === 'reset') {
        clientEntityProvider.getUserManager().logout();
      }
    }
  };

  public render() {
    const { Button, WaitingPage } = clientEntityProvider.getComponentProvider().UI;
    const { strategy, status } = this.props.auth;

    // TODO::move all renders to stateless component!
    if (this.state.loading || status === 'fetch') {
      return <WaitingPage text="Logging in..." />;
    }
    if (!strategy) {
      return <WaitingPage text="Initializing..." />;
    }

    return (
      <StyledLogin>
        <div className="text-center">
          <h1>Welcome!</h1>
        </div>
        <div>
          <form onKeyPress={this.handleKeyPress} className="large">
            <FormGroup controlId="username">
              <FormControl
                type="text"
                name="username"
                placeholder="Name"
                onChange={this.updateField}
                value={this.state.username}
              />
            </FormGroup>
            {!this.state.guestMode && (
              <FormGroup controlId="password">
                <FormControl
                  autoComplete="s2m-vr-password"
                  name="password"
                  type="password"
                  placeholder="Password"
                  onChange={this.updateField}
                  value={this.state.password}
                />
              </FormGroup>
            )}
            {!this.state.guestMode && false && (
              <FormGroup controlId="realm">
                <FormControl
                  name="realm"
                  type="test"
                  placeholder="Realm"
                  onChange={this.updateField}
                  value={this.state.realm}
                />
              </FormGroup>
            )}

            <div className="login-error" title={this.props.errorMessage}>
              {this.props.errorMessage.trim() && [
                <span key="login-error" className="glyphicon glyphicon-alert" />,
                this.props.errorMessage,
              ]}
            </div>

            <Button bsSize="large" disabled={this.isLoginDisabled()} onClick={this.login}>
              Enter
            </Button>

            {!this.state.guestMode && (
              <SwitchStrategyComponent onClick={this.loginSSO}>Sign in with EPAM SSO</SwitchStrategyComponent>
            )}

            {this.renderBottomText()}
          </form>
        </div>
      </StyledLogin>
    );
  }

  private updateField = (ev: any) => {
    this.setState({
      [ev.target.name]: ev.target.value,
      error: '',
    });
  };

  private handleKeyPress = (event: KeyboardEvent) => {
    if (event.key === 'Enter' && !this.isLoginDisabled()) {
      this.login();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  private login = () => {
    const { username, password, guestMode } = this.state;

    if (guestMode) {
      this.loginGuest();
    } else {
      this.props.login({ username, password });
    }
  };

  private loginSSO = (): void => {
    this.setState({ loading: false });
    this.props.loginWithOauth2();
  };

  private loginGuest = () => {
    const { username } = this.state;

    this.props.loginGuest({ name: username });
  };

  private isLoginDisabled = () => {
    if (this.state.guestMode) {
      return this.state.username.replace(/[\s-]*/g, '') === '';
    } else {
      return !this.state.username || !this.state.password || !this.state.realm;
    }
  };

  private toggleGuestMode = () => {
    this.setState({
      guestMode: !this.state.guestMode,
    });
  };

  private renderBottomText = () => {
    if (!this.props.showGuestScreen) {
      return null;
    }

    return (
      <div className="change-guest-mode">
        {this.state.guestMode && (
          <span>
            Already have an account? <a onClick={this.toggleGuestMode}>Log in</a>
          </span>
        )}
        {!this.state.guestMode && (
          <span>
            Don't have an account? <a onClick={this.toggleGuestMode}>Enter as Guest</a>
          </span>
        )}
      </div>
    );
  };
}

export default connect<ILoginStateProps, Partial<ILoginDispatchProps>>(
  (state: IStoreState) => {
    return {
      router: state.router,
      errorMessage: state.auth?.error || '',
      auth: state.auth,
    };
  },
  {
    login: authActionCreators.login,
    loginWithToken: authActionCreators.loginWithToken,
    loginWithOauth2: authActionCreators.loginWithOauth2,
    loginChangeParams: authActionCreators.loginChangeParams,
    loginGuest: authActionCreators.loginGuest,
    // showNotification: appActions.showNotification,
  }
)(LoginPage);
