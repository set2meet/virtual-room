import React from 'react';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { boolean, text } from '@storybook/addon-knobs';
import { withIoCFactory } from '../../../../../test/utils/utils';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { LoginStrategies } from '../../../../../ioc/client/providers/AuthProvider/types/constants';
import { ILoginPageProps } from './ILoginPageProps';
import { reduxMockEnhancer } from '../../../../../test/utils/reduxMockEnhancer';
import { getStorybookIoCInitConfig } from '../../../../../../.storybook/common/utils/ioc/storybookInitConfig';

const SuspendedLoginPage = withIoCFactory<ILoginPageProps>(() =>
  clientEntityProvider.getComponentProvider().resolveComponentSuspended(ComponentRegistryKey.Auth_LoginPage)
);

const decorators = [connectDecorator()];

export const changeStrategyAction = (strategy: LoginStrategies | '') => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();
  return {
    name: strategy.toUpperCase() || 'empty',
    action: appActionCreators.loginChangeParams({ strategy }),
  };
};

export const setLoginErrorAction = (error: string = 'error') => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();
  return {
    name: 'setError',
    action: appActionCreators.loginError(error),
  };
};

const authParamsGroupId = 'authParams';

export default {
  title: 'auth/LoginPage',
  component: SuspendedLoginPage,
  excludeStories: /.*Action$/,
  decorators,
  parameters: {
    redux: {
      enable: true,
    },
  },
};

const Default = () => {
  const auth = {
    realm: text('realm', 'Set2Meet', authParamsGroupId),
    allowChangeAuthMethods: boolean('allowChangeAuthMethods', false, authParamsGroupId),
    allowFakeUsers: boolean('allowFakeUsers', true, authParamsGroupId),
  };

  return (
    <SuspendedLoginPage
      authParams={auth}
      reconnectGuest={boolean('reconnectGuest', false)}
      showGuestScreen={boolean('showGuestScreen', false)}
    />
  );
};

const defaultStorySettings = {
  parameters: {
    redux: {
      actionsFactory: () => [
        changeStrategyAction(''),
        changeStrategyAction(LoginStrategies.STRATEGY_FORM),
        setLoginErrorAction(),
      ],
    },
  },
};

export const Initializing = () => <Default />;

Initializing.story = {
  parameters: {
    IoCInitializationOptions: {
      storeEnhancerFactory: () => [reduxMockEnhancer, ...getStorybookIoCInitConfig().storeEnhancerFactory()],
    },
  },
};

export const Form = (props) => {
  props.dispatch(changeStrategyAction(LoginStrategies.STRATEGY_FORM).action);
  return <Default />;
};

Form.story = defaultStorySettings;

export const FormWithHeader = (props) => {
  props.dispatch(changeStrategyAction(LoginStrategies.STRATEGY_FORM).action);
  return <Default />;
};

FormWithHeader.story = {
  ...defaultStorySettings,
  parameters: {
    ...defaultStorySettings.parameters,
    themeDecorator: {
      includeHeader: true,
    },
  },
};

export const FormWithError = (props) => {
  props.dispatch(changeStrategyAction(LoginStrategies.STRATEGY_FORM).action);
  props.dispatch(setLoginErrorAction(text('errorMessage', 'error')).action);
  return <Default />;
};

FormWithError.story = defaultStorySettings;

export const FormWithGuestMode = (props) => {
  const auth = {
    realm: text('realm', 'Set2Meet', authParamsGroupId),
    allowChangeAuthMethods: boolean('allowChangeAuthMethods', false, authParamsGroupId),
    allowFakeUsers: boolean('allowFakeUsers', true, authParamsGroupId),
  };
  props.dispatch(changeStrategyAction(LoginStrategies.STRATEGY_FORM).action);

  return (
    <SuspendedLoginPage
      authParams={auth}
      reconnectGuest={boolean('reconnectGuest', true)}
      showGuestScreen={boolean('showGuestScreen', true)}
    />
  );
};

FormWithGuestMode.story = defaultStorySettings;
