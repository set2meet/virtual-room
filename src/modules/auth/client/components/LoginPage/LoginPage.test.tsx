import { changeStrategyAction } from './LoginPage.stories';
import { mountStory, setupStorybookForTest } from '../../../../../test/utils/storybook';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { enzymeCleanup } from '../../../../../test/utils/utils';
import { IStoreExt, reduxMockEnhancer } from '../../../../../test/utils/reduxMockEnhancer';
import { getTestIoCInitConfig } from '../../../../../test/utils/ioc/config/testInitConfig';
import { IClientEntityProviderInternals } from '../../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { LoginStrategies } from '../../../../../ioc/client/providers/AuthProvider/types/constants';
import { TStore } from '../../../../../ioc/client/types/interfaces';

beforeAll(setupStorybookForTest());
afterEach(enzymeCleanup);

describe('<LoginPage />', () => {
  it('should render spinner', async (done) => {
    const wrapper = await mountStory('auth/LoginPage', 'Initializing');

    expect(wrapper.find('WaitingPage')).toHaveProp('text', 'Initializing...');
    done();
  });

  it('should render sso strategy', async (done) => {
    const storyRoot = await mountStory('auth/LoginPage', 'Form');
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const wrapper = storyRoot.find('LoginPage');

    expect(wrapper.prop('auth')).toEqual(
      expect.objectContaining({
        strategy: LoginStrategies.STRATEGY_FORM,
      })
    );
    await store.dispatch(changeStrategyAction(LoginStrategies.STRATEGY_SSO).action);
    expect(wrapper.update().find('LoginPage').prop('auth')).toEqual(
      expect.objectContaining({
        strategy: LoginStrategies.STRATEGY_SSO,
      })
    );
    done();
  });

  describe('check form story', () => {
    it("Shouldn't change state on mock store", async (done) => {
      const storyRoot = await mountStory('auth/LoginPage', 'Form', true, {
        storeEnhancerFactory: () => [reduxMockEnhancer, ...getTestIoCInitConfig().storeEnhancerFactory()],
      });

      const wrapper = storyRoot.find('LoginPage');

      expect(wrapper.update().find('LoginPage').prop('auth')).toEqual(
        expect.objectContaining({
          strategy: '',
        })
      );
      done();
    });
    it('Should change state on real store', async (done) => {
      const storyRoot = await mountStory('auth/LoginPage', 'Form', true);

      expect(storyRoot.find('LoginPage').prop('auth')).toEqual(
        expect.objectContaining({
          strategy: LoginStrategies.STRATEGY_FORM,
        })
      );
      done();
    });
  });

  it('should set guest mode', async (done) => {
    const wrapper = await mountStory('auth/LoginPage', 'Form With Guest Mode');

    expect(wrapper.find('LoginPage').state()).toEqual(
      expect.objectContaining({
        guestMode: false,
      })
    );
    wrapper.find('.change-guest-mode').find('a').simulate('click');
    wrapper.update();
    expect(wrapper.find('LoginPage').state()).toEqual(
      expect.objectContaining({
        guestMode: true,
      })
    );
    done();
  });

  it('should dispatch login action with provided data', async (done) => {
    const wrapper = await mountStory('auth/LoginPage', 'Form', true);
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    wrapper.find('input[name="username"]').getDOMNode<HTMLInputElement>().value = 'test';
    wrapper.find('input[name="username"]').simulate('change');
    wrapper.find('input[name="password"]').getDOMNode<HTMLInputElement>().value = 'password';
    wrapper.find('input[name="password"]').simulate('change');

    expect(wrapper.find('LoginPage').state()).toEqual(
      expect.objectContaining({
        username: 'test',
        password: 'password',
      })
    );
    wrapper.find('Button').find('button').simulate('click');
    expect((store as TStore & IStoreExt).getActions()).toContainEqual(
      appActionCreators.login({
        password: 'password',
        username: 'test',
      })
    );
    done();
  });
  it('fixate fetching state', async (done) => {
    const random = jest.spyOn(Math, 'random').mockImplementation(() => 1);
    const wrapper = await mountStory('auth/LoginPage', 'Form', true);

    wrapper
      .find('LoginPage')
      .setState({
        loading: true,
      })
      .update();
    expect(wrapper.find('WaitingPage')).toHaveProp('text', 'Logging in...');
    expect(wrapper.find('LoginPage')).toMatchSnapshot();
    random.mockRestore();
    done();
  });
});
