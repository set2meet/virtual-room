import React, { KeyboardEvent, SyntheticEvent } from 'react';
import { Form, FormGroup, FormControl, Col, ControlLabel, HelpBlock, Row, FormGroupProps } from 'react-bootstrap';
import styled from 'styled-components';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IButtonProps } from '../../../../../ioc/client/types/interfaces';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';

export interface IFeedback {
  field?: string;
  value?: string;
  validationState?: FormGroupProps['validationState'];
}

interface IChangePasswordProps {
  feedback: IFeedback;
  changePassword: (oldPassword: string, newPassword: string) => void;
  resetState: () => void;
}

interface IChangePasswordState {
  oldPassword: string;
  newPassword: string;
  newPassword2: string;
  feedback: IFeedback;
}

const StyledChangePasswordForm = styled.div`
  width: 550px;
  padding-top: 30px;
  margin-left: 15px;

  .row {
    margin: 0;
  }
`;

const StyledButtonContainer = styled(Col)`
  padding-left: 0;
`;

// tslint:disable-next-line
const columns = [4, 5, 3]; // bootstrap columns

/**
 * UI & local feedback
 */
class ChangePassword extends React.Component<IChangePasswordProps> {
  public state: IChangePasswordState = {
    oldPassword: '',
    newPassword: '',
    newPassword2: '',
    feedback: {},
  };

  private handleKeyPress = (event: KeyboardEvent<Form>) => {
    if (event.key === 'Enter' && this.isChangeEnabled()) {
      this.changePassword();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  private updateField = (ev: SyntheticEvent<FormControl>) => {
    const event = ev.target as any; // no typescript definition for ev.target = MouseEvent

    this.props.resetState();
    this.setState({
      [event.name]: event.value,
      feedback: {},
    });
  };

  private changePassword = () => {
    const { oldPassword, newPassword, newPassword2 } = this.state;

    if (newPassword !== newPassword2) {
      this.setState({
        feedback: {
          field: 'newPassword2',
          value: 'Passwords do not match',
          validationState: 'error',
        },
      });
      // tslint:disable-next-line
    } else if (newPassword.length < 8) {
      this.setState({
        feedback: {
          field: 'newPassword',
          value: 'At least 8 characters required',
          validationState: 'error',
        },
      });
    } else if (oldPassword === newPassword) {
      this.setState({
        feedback: {
          field: 'newPassword',
          value: 'New password must not be the same as the old one',
          validationState: 'error',
        },
      });
    } else {
      this.setState({ feedback: {} });
      this.props.changePassword(this.state.oldPassword, this.state.newPassword);
    }
  };

  private isChangeEnabled = () => {
    return this.state.oldPassword.length > 0 && this.state.newPassword.length > 0 && this.state.newPassword2.length > 0;
  };

  private renderFormControl = (
    { title, field }: { title: string; field: string },
    feedbacks: Record<string, IFeedback>
  ) => {
    const formAttrs: Partial<FormGroupProps> = {
      controlId: `form_changePassword__${field}`,
      validationState: null,
    };

    const fieldFeedback = feedbacks[field] as IFeedback;

    if (fieldFeedback) {
      formAttrs.validationState = fieldFeedback.validationState;
    }

    return (
      <FormGroup {...formAttrs} key={field}>
        <Col componentClass={ControlLabel} sm={columns[0]}>
          {title}
        </Col>
        <Col sm={columns[1]}>
          <FormControl
            autoComplete="off"
            name={field}
            type="password"
            onChange={this.updateField}
            value={(this.state as any)[field]}
          />
        </Col>
        <Col sm={columns[2]}>{fieldFeedback && <HelpBlock>{fieldFeedback.value}</HelpBlock>}</Col>
      </FormGroup>
    );
  };

  public render() {
    const feedbacks = {
      [this.state.feedback.field]: this.state.feedback,
      [this.props.feedback.field]: this.props.feedback,
    };

    const formGroups = [
      {
        title: 'Old password',
        field: 'oldPassword',
      },
      {
        title: 'New password',
        field: 'newPassword',
      },
      {
        title: 'Confirm new password',
        field: 'newPassword2',
      },
    ];
    const Button = clientEntityProvider.resolveComponentSuspended<IButtonProps>(ComponentRegistryKey.Button);
    const StyledButton = styled(Button)`
      width: 100%;
    `;

    return (
      <StyledChangePasswordForm>
        <Form horizontal={true} onKeyPress={this.handleKeyPress} bsClass="settingsForm">
          {formGroups.map((formGroup) => this.renderFormControl(formGroup, feedbacks))}
          <Row>
            <StyledButtonContainer smOffset={columns[0]} sm={columns[1]}>
              <StyledButton disabled={!this.isChangeEnabled()} onClick={this.changePassword}>
                CHANGE PASSWORD
              </StyledButton>
            </StyledButtonContainer>
          </Row>
        </Form>
      </StyledChangePasswordForm>
    );
  }
}

export default ChangePassword;
