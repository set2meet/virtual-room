import React from 'react';
import ChangePassword, { IFeedback } from './ChangePassword';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import { IChangePasswordContainerProps } from './IChangePasswordProps';

interface IChangePasswordState {
  feedback: IFeedback;
}

interface IChangePasswordStateProps {
  username: string;
  realm: string;
}

const INITIAL_STATE: IChangePasswordState = {
  feedback: {},
};

/**
 * Communication with server & server feedback here
 */
class ChangePasswordContainer extends React.Component<IChangePasswordContainerProps & IChangePasswordStateProps> {
  public state: IChangePasswordState = INITIAL_STATE;

  private resetState = () => {
    this.setState({ feedback: {} });
  };

  private changePassword = (oldPassword: string, newPassword: string) => {
    const { realm, username, changePasswordUrl } = this.props;

    fetch(changePasswordUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({
        username,
        realm,
        oldPassword,
        newPassword,
      }),
    })
      .then((resp) => {
        // tslint:disable-next-line
        if (resp.status === 401) {
          this.setState({
            feedback: {
              field: 'oldPassword',
              value: 'Invalid old password',
              validationState: 'error',
            },
          });
          // tslint:disable-next-line
        } else if (resp.status >= 400) {
          this.setState({
            feedback: {
              field: 'oldPassword',
              value: 'Changing password failed',
              validationState: 'error',
            },
          });
        } else {
          this.setState({
            feedback: {
              field: 'oldPassword',
              value: 'You password has been changed successfully',
              validationState: 'success',
            },
          });
        }
      })
      .catch(() => {
        this.setState({
          feedback: {
            field: 'oldPassword',
            value: 'Error changing password',
            validationState: 'error',
          },
        });
      });
  };

  public render() {
    return (
      <ChangePassword
        changePassword={this.changePassword}
        resetState={this.resetState}
        feedback={this.state.feedback}
      />
    );
  }
}

export default connect((state, ownProps: IChangePasswordContainerProps) => ({
  username: _get(state, `${ownProps.statePath}.user.id`),
  realm: _get(state, `${ownProps.statePath}.tokens.realm`),
}))(ChangePasswordContainer);
