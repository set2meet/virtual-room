/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { ComponentClass } from 'react';
import { IAuthButtonLogout } from './LogoutButton/IAuthButtonLogout';
import { IChangePasswordContainerProps } from './ChangePassword/IChangePasswordProps';
import { ILoginPageProps } from './LoginPage/ILoginPageProps';

export interface IAuthComponents {
  ChangePassword: ComponentClass<IChangePasswordContainerProps>;
  LoginPage: ComponentClass<ILoginPageProps>;
  LogoutButton: ComponentClass<IAuthButtonLogout>;
}
