/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import ui from './components';
import actionCreators from './redux/actionCreators';
import { authStorageKeys } from './storage/storageKeys';
import { IVRModule } from '../../../ioc/client/types/interfaces';
import { IAuthStorageKeys } from './storage/IAuthStorageKeys';
import { IAuthComponents } from './components/IAuthComponents';
import { IAuthActionCreators } from './redux/types/IAuthActionCreators';
import { AuthAction, IAuthActionTypes } from '../common/redux/AuthAction';

interface IAuth extends IVRModule<IAuthActionCreators> {
  actionTypes: IAuthActionTypes;
  actionCreators: IAuthActionCreators;
  storageKeys: IAuthStorageKeys;
  ui: IAuthComponents;
}

const authClientModule: IAuth = {
  actionTypes: AuthAction,
  actionCreators,
  ui,
  storageKeys: authStorageKeys,
};

export default authClientModule;
