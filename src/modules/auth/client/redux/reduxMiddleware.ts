/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import authActions from './actionCreators';
import { AnyAction, Dispatch, Store } from 'redux';
import _set from 'lodash/set';
import Socket = SocketIOClient.Socket;
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { AuthAction } from '../../common/redux/AuthAction';

let authMiddlewareEnumCache: IAppReduxEnumMiddleware = null;

/**
 * @TODO:
 * This is getter just a temp solution to workaround load order issues by wrapping the import-level factory code into
 * function call with cache. We should refactor this whole weird 'enum' logic to less tricky and more clean solution later
 */
const getAuthMiddlewareEnum = () => {
  if (!authMiddlewareEnumCache) {
    authMiddlewareEnumCache = {
      [AuthAction.AUTH_MODULE_INIT](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();

        UserManager.getAuthFromStorage().then((exists: boolean) => {
          if (exists) {
            const user = UserManager.getUser();

            if (user) {
              UserManager.checkSSO().then((resp: any) => {
                store.dispatch(authActions.loginWithToken());
              });
            }
          } else {
            store.dispatch(authActions.loginInitParams({ strategy: UserManager.AuthProvider.getStrategies()[0] })); // just returns 'form'
          }
        });
        next(action);
      },
      [AuthAction.LOGIN](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();

        UserManager.loginForm(action.payload)
          .then((authorized: boolean) => {
            if (authorized) {
              const userWithTokens = UserManager.getFullUser();
              store.dispatch(authActions.loginSuccess(userWithTokens));
            }
          })
          .catch((e: Error) => {
            console.log(e.toString());
            store.dispatch(authActions.loginError('The username or password you entered is incorrect'));
          });
        next(action);
      },
      [AuthAction.LOGIN_WITH_TOKEN](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();

        UserManager.loginWithToken()
          .then((authorized: boolean) => {
            if (authorized) {
              const userWithTokens = UserManager.getFullUser();
              store.dispatch(authActions.loginSuccess(userWithTokens));
            } else {
              store.dispatch(authActions.loginError('Session expired'));
              store.dispatch(authActions.logout());
              store.dispatch(authActions.loginInitParams({ strategy: UserManager.AuthProvider.getStrategies()[0] }));
            }
          })
          .catch((e: Error) => {
            console.log(e.toString());
            store.dispatch(authActions.loginError('The username or password you entered is incorrect'));
          });
        next(action);
      },
      [AuthAction.LOGIN_GUEST](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();

        UserManager.loginAsGuest(action.payload.name, action.payload.fakeGuest)
          .then((userWithTokens) => {
            if (userWithTokens) {
              store.dispatch(authActions.loginSuccess(userWithTokens));
            }
          })
          .catch((e: Error) => {
            console.log(e.toString());
            store.dispatch(authActions.loginError('Can`t login as guest'));
          });

        next(action);
      },

      [AuthAction.LOGIN_WITH_OAUTH2](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();

        UserManager.loginSSO()
          .then((authenticated: boolean) => {
            if (authenticated) {
              const userWithTokens = UserManager.getFullUser();
              store.dispatch(authActions.loginSuccess(userWithTokens));
            }
          })
          .catch((e: Error) => {
            console.log(e.toString());
            store.dispatch(authActions.loginError('The username or password you entered is incorrect'));
          });

        next(action);
      },
      [AuthAction.LOGIN_SUCCESS](store, next, action) {
        // create app socket connection
        store.dispatch(clientEntityProvider.getAppActionCreators().connect(action.payload.tokens.accessToken));
        next(action);
      },
      [AuthAction.LOGIN_INIT_PARAMS](store, next, action) {
        next(action);
      },
      [AuthAction.LOGOUT](store, next, action) {
        const UserManager = clientEntityProvider.getUserManager();
        UserManager.logout();
        store.dispatch(authActions.loginInitParams({ strategy: UserManager.AuthProvider.getStrategies()[0] }));
        next(action);
      },
    };
  }

  return authMiddlewareEnumCache;
};

const authMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => (
  next: Dispatch<AnyAction>
) => async (action: AnyAction) => {
  if (clientEntityProvider.getUtils().isReduxDynamicModulesAction(action)) {
    next(action);
    return;
  }
  const authMiddlewareEnum = getAuthMiddlewareEnum();

  if (authMiddlewareEnum[action.type]) {
    return authMiddlewareEnum[action.type](store, next, action, socket, storeKey);
  } else {
    _set(action, 'meta.user', store.getState()[storeKey].user);

    next(action);
  }
};

export default authMiddleware;
