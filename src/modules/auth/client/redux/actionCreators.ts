/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IAuthActionCreators } from './types/IAuthActionCreators';
import { AuthAction } from '../../common/redux/AuthAction';

const authActionCreators: IAuthActionCreators = {
  initAuthModule: () => {
    return {
      type: AuthAction.AUTH_MODULE_INIT,
    };
  },
  login: (payload: object) => {
    return {
      type: AuthAction.LOGIN,
      payload,
    };
  },
  loginInitParams: (payload: object) => {
    return {
      type: AuthAction.LOGIN_INIT_PARAMS,
      payload,
    };
  },
  loginChangeParams: (payload: any) => {
    return {
      type: AuthAction.LOGIN_CHANGE_PARAMS,
      payload,
    };
  },
  loginWithOauth2: (payload: object) => {
    return {
      type: AuthAction.LOGIN_WITH_OAUTH2,
      payload,
    };
  },
  loginGuest: (payload: object) => {
    return {
      type: AuthAction.LOGIN_GUEST,
      payload,
    };
  },
  loginWithToken: () => {
    return {
      type: AuthAction.LOGIN_WITH_TOKEN,
    };
  },
  loginSuccess: (payload: any) => {
    return {
      type: AuthAction.LOGIN_SUCCESS,
      payload,
    };
  },
  loginError: (error: string) => {
    return {
      type: AuthAction.LOGIN_ERROR,
      error,
    };
  },
  logout: () => {
    return {
      type: AuthAction.LOGOUT,
    };
  },
};

export default authActionCreators;
