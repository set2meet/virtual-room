/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { TRoomUser, TAuthTokens } from '../../../../../ioc/common/ioc-interfaces';
import { LoginStrategies } from '../../../../../ioc/client/providers/AuthProvider/types/constants';

export interface IAuthReduxState {
  user: TRoomUser;
  tokens: TAuthTokens;
  error: string;
  /**
   * @deprecated should use tokens?.strategy
   */
  strategy: LoginStrategies | '';
  status: 'initialized' | 'fetch' | 'success' | 'failed';
}
