/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, Reducer } from 'redux';
import { LoginStrategies } from '../../../../ioc/client/providers/AuthProvider/types/constants';
import { IAuthReduxState } from './types/IAuthReduxState';
import { AuthAction } from '../../common/redux/AuthAction';

const DEFAULT_DISPLAY_NAME = 'Unknown';

const INITIAL_STATE: IAuthReduxState = {
  user: null,
  tokens: null,
  error: '',
  strategy: '',
  status: 'initialized',
};

const reducers: Record<string, Reducer<IAuthReduxState>> = {};

reducers[AuthAction.LOGIN] = (state, { payload }) => {
  return {
    ...state,
    status: 'fetch',
  };
};

reducers[AuthAction.LOGIN_INIT_PARAMS] = (state, { payload }) => {
  return {
    ...state,
    ...payload,
  };
};

reducers[AuthAction.LOGIN_CHANGE_PARAMS] = (state, { payload }) => {
  return {
    ...state,
    ...payload,
  };
};

reducers[AuthAction.LOGIN_WITH_OAUTH2] = (state, { payload }) => {
  return {
    ...state,
    ...payload,
  };
};

reducers[AuthAction.LOGIN_SUCCESS] = (state, { payload }) => {
  if (!payload.user.displayName) {
    payload.user.displayName = DEFAULT_DISPLAY_NAME;
  }

  return {
    ...state,
    ...payload,
    status: 'success',
  };
};

reducers[AuthAction.LOGIN_ERROR] = (state, { error }) => {
  return {
    ...state,
    error,
    status: 'failed',
  };
};

reducers[AuthAction.LOGOUT] = (state) => {
  return {
    ...INITIAL_STATE,
    strategy: LoginStrategies.STRATEGY_FORM,
  };
};

export default (state = INITIAL_STATE, action: AnyAction): IAuthReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
