/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export interface IAuthRoutes {
  login: string;
  loginForm: string;
  validate: string;
  validateToken: string;
  token: string;
}

/**
 * Type user define decoded JWT token
 * For decode/encode you can user jwt.io
 */
export type TUserAccessToken = {
  jti?: string;
  exp?: number;
  nbf?: number;
  iat?: number;
  iss?: string;
  aud?: string;
  sub?: string;
  typ?: string;
  azp?: string;
  nonce?: string;
  auth_time?: number;
  session_state?: string;
  acr?: number;
  'allowed-origins?': any;
  realm_access?: any;
  resource_access?: any;
  name?: string;
  preferred_username?: string;
  given_name?: string;
  family_name?: string;
  email?: string;
  picture?: string;
};

export type TAuthError = string;
