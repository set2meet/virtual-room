/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import passport from 'koa-passport';
import * as Router from 'koa-router';
import { IAuthRoutes, TAuthError, TUserAccessToken } from './index';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { CURRENT_STRATEGY, LogErrorCode } from '../../../ioc/server/ioc-constants';
import { HttpStatusCode } from '../../../types/httpStatusCode';
import api, { GrantType, parseToken } from '@s2m/resource-management';
import { getRealmSettings } from './getRealmSettings';
import { getTokenForLoginWithForm } from './getTokenForLoginWithForm';
import { IAuthConfig } from '../../../ioc/common/types/config/IAuthConfig';

/**
 * Routes described
 */
const authVariables: IAuthRoutes = {
  login: '/login',
  loginForm: '/login-form',
  validate: '/validate',
  validateToken: '/validate-token',
  token: '/token',
  // changePassword: '/change-password',
  // TODO:: add change-password functionality later for non-sso clients
};

export default (router: Router) => {
  /**
   * Handles token validation
   * Validating expire period and right decode by RSA256 public key
   * In case with browser public flow tokens already stored at client
   * So here we just validate it at server and let Koa know about authenticated users
   * Maybe, in future cases we may need some private routes
   * If this route falls to 401 -> we get new one in /token route
   */
  router.post(authVariables.validate, async (ctx, next) => {
    /**
     * authenticate callback props is two returned values from done() function which is used in passport.use strategy below;
     */
    await passport.authenticate(
      CURRENT_STRATEGY,
      { session: false },
      (error: TAuthError, user: TUserAccessToken): void => {
        if (user) {
          const { name, given_name, family_name, email, sub, picture } = user;

          ctx.body = { name, preferred_username: sub, given_name, family_name, email, sub, picture };
        } else {
          ctx.throw(HttpStatusCode.UNAUTHORIZED);
          ctx.body = 'Error while authentication';
        }
      }
    )(ctx, next);
  });

  /**
   * Validating login after page reload
   * Data which returns DOESNT validated at client. We look only on server response code.
   * It is cuz we use local/session storage and retrieving data from it after page been refreshed.
   * @link syncBackend works with this route
   * @return 400 as Bad Request, as authenticated above in strategy. So you could get Bad Request
   * only if you passing wrong params with right token.
   */
  router.post(authVariables.validateToken, async (ctx: any, next) => {
    const { tokens } = ctx.request.body;

    if (tokens?.strategy === 'guest') {
      ctx.body = { tokens };

      await next();
      return;
    }

    try {
      const user = await api().fetchUserInfo(tokens.accessToken);

      ctx.body = { user, tokens };
    } catch (e) {
      ctx.throw(HttpStatusCode.BAD_REQUEST, e);
    }

    await next();
  });

  /**
   * Login route for 'FORM' method only.
   */
  router.post(authVariables.login, async (ctx, next) => {
    const { username, password, realm } = (ctx.request as any).body;

    try {
      const tokens = await getTokenForLoginWithForm({ username, password }, realm);
      const user = await api().fetchUserInfo(tokens.accessToken);

      ctx.body = { user, tokens };
    } catch (e) {
      ctx.throw(HttpStatusCode.BAD_REQUEST, e);
    }

    await next();
  });

  /**
   * Route used to retrieve new tokens pair
   */
  router.post(authVariables.token, async (ctx, next) => {
    const {
      tokens: { refreshToken, strategy },
    } = (ctx.request as any).body;
    const parsedToken = parseToken(refreshToken);
    const client = parsedToken.payload.azp;
    const realm = parsedToken.realmName;
    const baseUrl = parsedToken.baseUrl;
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;

    const settings = getRealmSettings(authConfig, realm, strategy);

    try {
      const token = await api({
        baseUrl,
        realmName: realm,
      }).refreshToken(
        {
          clientId: client,
          clientSecret: settings?.client_secret || '',
          grantType: GrantType.REFRESH_TOKEN,
        },
        refreshToken
      );

      const newTokens = {
        refresh_token: token.refreshToken,
        access_token: token.accessToken,
      };

      ctx.status = HttpStatusCode.CREATED;
      ctx.body = newTokens;
    } catch (e) {
      serverEntityProvider.getLogger().auth.error(LogErrorCode.MODULE, e.toString());
      ctx.status = HttpStatusCode.BAD_REQUEST;
    }

    await next();
  });
};
