/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import findKey from 'lodash/findKey';
import { IAuthConfig } from '../../../ioc/common/types/config/IAuthConfig';
type TSettings = { url: string; clientId: string; client_secret?: string; resource?: string };
/**
 * Getting local settings for realm
 * @param authConfigs
 * @param realm
 * @param strategy
 */
export const getRealmSettings = (authConfigs: IAuthConfig, realm: string, strategy?: string): TSettings => {
  const realmlist = { ...authConfigs.realmlist };
  let realmSettings;
  const authKey = findKey(realmlist, realm);
  /*
  Check if there is strategy provided
  If not - get the related to realm name
  That's created for multi-realm and for mono pass the strategy
  Because realms in different strategies named the same and you might get
  wrong realm settings
   */
  if (strategy) {
    realmSettings = realmlist[strategy][realm];
  } else {
    realmSettings = realmlist[authKey];
  }

  if (!realmSettings) {
    throw new Error('Do you configure realms in configs? Or do you trying get them properly?');
  }

  return realmSettings;
};
