/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import querystring from 'querystring';
import rp from 'request-promise';
import jwt from 'jsonwebtoken';
import getUserTokens from './getUserTokens';

interface IRealmOpts {
  client_id: string;
  url: string;
  client_secret: string;
}

interface IAuthOpts {
  credentials: {
    // for admin API interface
    username: string;
    password: string;
  };
  refreshSecretKey: string;
  secretKey: string;
  sso: Record<string, IRealmOpts>;
}

// get admin token for working with admin rest api
const getAdminAccessToken = async (url: string, credentials: { username: string; password: string }) => {
  const getTokenOpts = {
    body: querystring.stringify({
      client_id: 'admin-cli',
      grant_type: 'password',
      password: credentials.password,
      username: credentials.username,
    }),
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
    uri: `${url}/auth/realms/master/protocol/openid-connect/token`,
  };

  const tokens = await rp(getTokenOpts).then((body: string) => {
    return JSON.parse(body);
  });
  return tokens.access_token;
};

export default async (
  data: {
    realm: string;
    username: string;
    oldPassword: string;
    newPassword: string;
  },
  opts: IAuthOpts
) => {
  const { realm, username, newPassword, oldPassword } = data;

  if (!opts.sso[realm]) {
    throw new Error('Invalid Realm');
  }
  if (!newPassword || !oldPassword) {
    throw new Error('No passwords provided');
  }

  const { client_id, client_secret, url } = opts.sso[data.realm];
  let userId = '';

  // get user tokens = validate password is correct & get userID from token
  try {
    const { access_token } = await getUserTokens({
      url,
      realm,
      client_id,
      client_secret,
      username,
      password: oldPassword,
    });
    const decodedToken = jwt.decode(access_token) as any;

    userId = decodedToken.sub; // usedId here
  } catch (e) {
    throw new Error('Incorrect password');
  }

  // token to work with Admin API
  const APIAccessToken = await getAdminAccessToken(url, opts.credentials);

  const options = {
    body: {
      value: newPassword,
      type: 'password',
      temporary: false,
    },
    headers: {
      Authorization: `Bearer ${APIAccessToken}`,
      'Content-Type': 'application/json',
    },
    json: true,
    method: 'PUT',
    uri: `${url}/auth/admin/realms/${data.realm}/users/${userId}/reset-password`,
  };

  // change password request promise
  return rp(options);
};
