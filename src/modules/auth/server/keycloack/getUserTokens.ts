/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import querystring from 'querystring';
import rp from 'request-promise';

interface IGetUserTokensOpts {
  url: string;
  realm: string;
  client_id: string;
  client_secret: string;
  username: string;
  password: string;
}

// get JWT tokens by login/password
export default async ({ url, realm, client_id, client_secret, username, password }: IGetUserTokensOpts) => {
  try {
    const options = {
      body: querystring.stringify({
        client_id,
        client_secret,
        grant_type: 'password',
        password,
        username,
      }),
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      uri: `${url}/auth/realms/${realm}/protocol/openid-connect/token`,
    };

    return await rp(options).then((body: string) => {
      return JSON.parse(body);
    });
  } catch (e) {
    throw new Error('Invalid credentials');
  }
};
