/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/**
 * Public key for checking valid JWT;
 * Can be obtained in keycloak realm settings;
 * @type {string}
 */
const publicKey: string =
  '-----BEGIN PUBLIC KEY-----\n' +
  'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnVNZmxFUWAYFHiUlegMV\n' +
  'idSXVvr5w1a5MVFFCjBi5uGei+5ZFXiIl8uI79eV78BvX9CRF+bhx4C9GdYY2NsM\n' +
  'KQEzwHGFBGmNCpmNi+WU2lzlFXxTIuZ7bu2XaJ8oUx0/lmjRQfKVgGAu1J0riWxD\n' +
  'J1BHuLmvQl65f2r+MvAsb7D9fuEEPWfOEjQBCCCPwBRPtOdkuXrfkMlbXKhDKHnr\n' +
  'zokyBuRhfznbsgmQP3oGtrtw4g3+3Bc5dv+xIpzR7EriaKoElqqn/v8opSgmJWvc\n' +
  'Ix4w0S+tvfqB6xd9r7+bWuQax07SNRmIBpVfMEsEAX9GrlzdtHCPxGGSJHiCAGZ1\n' +
  'AwIDAQAB\n' +
  '-----END PUBLIC KEY-----\n';

export default publicKey;
