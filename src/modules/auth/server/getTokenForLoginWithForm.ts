/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import api, { GrantType, TToken } from '@s2m/resource-management';
import { getRealmSettings } from './getRealmSettings';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { LoginStrategies } from '../../../ioc/client/providers/AuthProvider/types/constants';
import { IAuthConfig } from '../../../ioc/common/types/config/IAuthConfig';
/**
 *
 * Getting token from current keycloak instance
 * Current realm means default in config
 * WARNING: client_id = realm. At least it's temporary solution;
 * Only used for 'form' strategy
 * @param username
 * @param password
 * @param realm
 * @param strategy
 * @deprecated
 * TODO::Move to resource-management lib
 */
export const getTokenForLoginWithForm = async ({ username, password }: any, realm?: string): Promise<TToken> => {
  const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
  const settings = getRealmSettings(authConfig, realm, LoginStrategies.STRATEGY_FORM);
  serverEntityProvider.getLogger().auth.info('Get token for user', {
    username,
    realm,
    clientId: settings.clientId,
    clientSecret: settings.client_secret,
    grantType: GrantType.PASSWORD,
  });

  return await api({
    baseUrl: settings.url,
    realmName: realm || authConfig.realm,
  }).fetchToken(
    {
      clientId: settings.resource || settings.clientId,
      clientSecret: settings.client_secret,
      grantType: GrantType.PASSWORD,
    },
    username,
    password
  );
};
