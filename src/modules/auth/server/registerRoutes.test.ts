/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { getRealmSettings } from './getRealmSettings';
import { LoginStrategies } from '../../../ioc/client/providers/AuthProvider/types/constants';
import { IAuthConfig } from '../../../ioc/common/types/config/IAuthConfig';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import setupIoCServerForTest from '../../../test/utils/ioc/testIoCServer';

enum cFields {
  realmlist = 'realmlist',
  allowFakeUsers = 'allowFakeUsers',
  allowChangeAuthMethods = 'allowChangeAuthMethods',
}

beforeAll(
  setupIoCServerForTest({
    configFactory: () =>
      Promise.resolve({
        notepad: {
          apiKey: '***',
          authDomain: '***',
          databaseURL: '***',
          projectId: '***',
          storageBucket: '***',
          messagingSenderId: '***',
        },
        codeEditor: {
          apiKey: '***',
          authDomain: '***',
          databaseURL: '***',
          projectId: '***',
          storageBucket: '***',
          messagingSenderId: '***',
        },
        auth: {
          strategy: 'sso',
          realm: 'Epam',
          localUrl: '',
          allowChangeAuthMethods: true,
          allowFakeUsers: true,
          realmlist: {
            form: {
              Set2Meet: {
                url: 'https://dev.set2meet.com/auth',
                sslRequired: 'none',
                resource: 'Set2Meet',
                publicClient: true,
                confidentialPort: 0,
                client_secret: 'dontshowthissecrettoanybody',
              },
            },
            sso: {
              Set2Meet: {
                realm: 'Set2Meet',
                url: 'https://dev.set2meet.com/auth',
                clientId: 'UNSAFE_set2meet-client',
                publicClient: true,
              },
            },
          },
        },
      }),
  })
);

describe('Check config is contain required auth fields', () => {
  it('Check the config', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;

    expect(authConfig).toHaveProperty(cFields.allowChangeAuthMethods);
    expect(authConfig).toHaveProperty(cFields.allowFakeUsers);
    expect(authConfig).toHaveProperty(cFields.realmlist);
    done();
  });
});

describe('Check getSettings getting right configs', () => {
  it('Check the SSO config', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
    const realRealm = 'Set2Meet';
    const realmSettings = getRealmSettings(authConfig, realRealm, LoginStrategies.STRATEGY_SSO);

    const realConfigSSO = {
      realm: 'Set2Meet',
      url: 'https://dev.set2meet.com/auth',
      clientId: 'UNSAFE_set2meet-client',
      publicClient: true,
    };

    expect(realmSettings).toMatchObject(realConfigSSO);

    done();
  });

  it('Check the form config', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
    const realRealm = 'Set2Meet';
    const realmSettings = getRealmSettings(authConfig, realRealm, LoginStrategies.STRATEGY_FORM);

    const realConfigForm = {
      url: 'https://dev.set2meet.com/auth',
      sslRequired: 'none',
      resource: 'Set2Meet',
      publicClient: true,
      confidentialPort: 0,
      client_secret: 'dontshowthissecrettoanybody',
    };

    expect(realmSettings).toMatchObject(realConfigForm);

    done();
  });
});

describe('Check getSettings getting right configs', () => {
  it('Check the wrong type config while form get sso', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
    const realRealm = 'Set2Meet';
    const realmSettings = getRealmSettings(authConfig, realRealm, LoginStrategies.STRATEGY_SSO);

    const realConfigForm = {
      url: 'https://dev.set2meet.com/auth',
      sslRequired: 'none',
      resource: 'Set2Meet',
      publicClient: true,
      confidentialPort: 0,
      client_secret: 'dontshowthissecrettoanybody',
    };

    expect(realmSettings).not.toEqual(realConfigForm);

    done();
  });

  it('Check the wrong type config while sso get form', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;

    const realRealm = 'Set2Meet';
    const realmSettings = getRealmSettings(authConfig, realRealm, LoginStrategies.STRATEGY_FORM);

    const realConfigSSO = {
      realm: 'Set2Meet',
      url: 'https://dev.set2meet.com/auth',
      clientId: 'UNSAFE_set2meet-client',
      publicClient: true,
    };

    expect(realmSettings).not.toEqual(realConfigSSO);

    done();
  });

  it('Check the wrong type config with non existing realm with strategy form', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
    const nonRealRealm = 'Set2Fleet';

    expect(() => getRealmSettings(authConfig, nonRealRealm, LoginStrategies.STRATEGY_FORM)).toThrow();

    done();
  });

  it('Check the wrong type config with non existing realm with strategy sso', (done) => {
    const authConfig: IAuthConfig = serverEntityProvider.getConfig().auth;
    const nonRealRealm = 'Set2Greed';

    expect(() => getRealmSettings(authConfig, nonRealRealm, LoginStrategies.STRATEGY_SSO)).toThrow();

    done();
  });
});
