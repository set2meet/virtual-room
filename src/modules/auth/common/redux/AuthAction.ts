/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export enum AuthAction {
  AUTH_MODULE_INIT = 'AUTH:INIT_MODULE',
  LOGIN = 'AUTH:LOGIN',
  LOGIN_WITH_TOKEN = 'AUTH:LOGIN_WITH_TOKEN',
  LOGIN_GUEST = 'AUTH:LOGIN_GUEST',
  LOGIN_WITH_OAUTH2 = 'AUTH:LOGIN_WITH_OAUTH2',
  LOGIN_ERROR = 'AUTH:LOGIN_ERROR',
  LOGIN_SUCCESS = 'AUTH:LOGIN_SUCCESS',
  LOGIN_INIT_PARAMS = 'AUTH:LOGIN_INIT_PARAMS',
  LOGIN_CHANGE_PARAMS = 'AUTH:LOGIN_CHANGE_PARAMS',
  LOGOUT = 'AUTH:LOGOUT',
}

export type IAuthActionTypes = typeof AuthAction;
