/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export enum Graph2dAction {
  EQUATIONS_UPDATE = 'GRAPH2D:EQUATIONS_UPDATE',
  UPDATE_MODE_ANGLE = 'GRAPH2D:EQUATION_REMOVE',
  UPDATE_MODE_GRIDLINES = 'GRAPH2D:UPDATE_MODE_GRIDLINES',
  UPDATE_MODE_PRECISION = 'GRAPH2D:UPDATE_MODE_PRECISION',
}
