/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import reducer, { INITIAL_STATE } from './roomReducer';
import { AnyAction } from 'redux';
import actionCreators from './actionCreators';

describe('graph2d reducer', () => {
  const action: AnyAction = { type: '@@INIT' };
  const ownAction: AnyAction = actionCreators.updateEquationsList([{}]);

  it('should use INITIAL_STATE for undefined and null incoming state', () => {
    expect(reducer(undefined, action)).toEqual(INITIAL_STATE);
    expect(reducer(null, action)).toEqual(INITIAL_STATE);
    expect(reducer(undefined, ownAction)).toEqual({
      ...INITIAL_STATE,
      equationsList: ownAction.equations,
    });
    expect(reducer(null, ownAction)).toEqual({
      ...INITIAL_STATE,
      equationsList: ownAction.equations,
    });
  });
});
