/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { IGraph2D } from '../types';
import ReduxStateRoom = IGraph2D.ReduxStateRoom;
import { Graph2dAction } from './types/Graph2dAction';

export const INITIAL_STATE: ReduxStateRoom = {
  angleMode: 'radians',
  gridlinesMode: 'normal',
  precisionMode: 1,
  equationsList: [],
};

const reducers = {
  [Graph2dAction.EQUATIONS_UPDATE](state: ReduxStateRoom, action: AnyAction) {
    return {
      ...state,
      equationsList: action.equations,
    };
  },
  [Graph2dAction.UPDATE_MODE_ANGLE](state: ReduxStateRoom, action: AnyAction) {
    return {
      ...state,
      angleMode: action.value,
    };
  },
  [Graph2dAction.UPDATE_MODE_GRIDLINES](state: ReduxStateRoom, action: AnyAction) {
    return {
      ...state,
      gridlinesMode: action.value,
    };
  },
  [Graph2dAction.UPDATE_MODE_PRECISION](state: ReduxStateRoom, action: AnyAction) {
    return {
      ...state,
      precisionMode: action.value,
    };
  },
};

export default (state: ReduxStateRoom | null, action: AnyAction): ReduxStateRoom => {
  state = state || INITIAL_STATE;
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
