/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IGraph2D } from '../types';
import { Graph2dAction } from './types/Graph2dAction';
import { IGraph2dActionCreators } from './types/IGraph2dActionCreators';

const actionCreators: IGraph2dActionCreators = {
  updateEquationsList: (equations: IGraph2D.EquationRecord) => ({
    type: Graph2dAction.EQUATIONS_UPDATE,
    equations,
  }),
  updateModeAngle: (value: IGraph2D.AngleMode) => ({
    type: Graph2dAction.UPDATE_MODE_ANGLE,
    value,
  }),
  updateModeGridlines: (value: IGraph2D.GridlinesMode) => ({
    type: Graph2dAction.UPDATE_MODE_GRIDLINES,
    value,
  }),
  updateModePrecision: (value: IGraph2D.PrecisionMode) => ({
    type: Graph2dAction.UPDATE_MODE_PRECISION,
    value,
  }),
};

export default actionCreators;
