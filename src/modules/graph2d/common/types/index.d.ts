/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export namespace IGraph2D {
  export type Point = {
    x: number;
    y: number;
  };

  export type Line = {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
  };

  export type AngleMode = 'degrees' | 'radians' | 'gradians';

  export type GridlinesMode = 'normal' | 'less' | 'off';

  // tslint:disable-next-line:no-magic-numbers
  export type PrecisionMode = 0.1 | 0.5 | 1 | 5;

  export type Tool =
    | 'pointer'
    | 'trace'
    | 'vertex'
    | 'root'
    | 'intersect'
    | 'derivative'
    | 'zoombox'
    | 'zoomin'
    | 'zoomout';

  export type EquationRecord = {
    expression: string;
    color: string;
  };

  export type ReduxStateRoom = {
    angleMode: AngleMode;
    gridlinesMode: GridlinesMode;
    precisionMode: PrecisionMode;
    equationsList: EquationRecord[];
  };

  export type IModuleProps = {
    roomId?: string;
    isOwner: boolean;
  };
}
