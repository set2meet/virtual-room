/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, Dispatch, Store } from 'redux';
import { IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import Socket = SocketIOClient.Socket;
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { Graph2dAction } from '../../common/redux/types/Graph2dAction';

const middlewareEnum: Record<string, 1> = Object.keys(Graph2dAction)
  .map((key: string) => Graph2dAction[key])
  .reduce((map, value) => {
    map[value] = 1;
    return map;
  }, {});

const middleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  const appActionCreators = clientEntityProvider.getActionCreators();

  if (middlewareEnum[action.type] && !action.executed) {
    action.executed = true;

    return store.dispatch(appActionCreators.sendRoomAction(action));
  }

  return next(action);
};

export default middleware;
