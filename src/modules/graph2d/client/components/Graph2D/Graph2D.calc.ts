/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import math from 'mathjs';
import { IGraph2D } from '../../../common/types';

const getVariablesInExpression = (expr: any): any[] => {
  const obj: any = {};

  expr.traverse((node: any) => {
    if (node.type === 'SymbolNode' && (math as any)[node.name] === undefined) {
      obj[node.name] = true;
    }
  });

  return Object.keys(obj).sort();
};

type VarsData = Record<string, number>;

const getDefaultVariablesReducer = (data: VarsData, name: string) => {
  data[name] = 1;
  return data;
};

// machine epsilon - the maximum expected floating point error
const MACHINE_EPS: number = (function calcEps() {
  let temp1 = 1.0,
    temp2,
    mchEps; // tslint:disable-line

  do {
    mchEps = temp1;
    temp1 /= 2;
    temp2 = 1.0 + temp1;
  } while (temp2 > 1.0);

  return mchEps;
})();

export default class Calc {
  public static isValidEquation = (equation: string): boolean => {
    if (equation === '') {
      return true;
    }

    try {
      const exp = math.parse(equation);
      const vars = getVariablesInExpression(exp);

      // allow only 1 variable, and it should not be called 'y'
      if (vars.length > 1 || vars.indexOf('y') > -1) {
        return false;
      }

      // try evaluate with default argument
      const answ = exp.eval(vars.reduce(getDefaultVariablesReducer, {}));

      // result may be number or Complex (object),
      // if function - there undefined function in expression
      return typeof answ !== 'function';
    } catch (err) {
      return false;
    }
  };

  private angles: IGraph2D.AngleMode;
  private loopcounter: number = 0;

  constructor(angleMode: IGraph2D.AngleMode) {
    this.angles = angleMode;

    // make trigonometric replacments for mathjs functions
    const replacements: any = {};

    ['sin', 'cos', 'tan', 'sec', 'cot', 'csc'].forEach((name) => {
      // tslint:disable-next-line
      const fn = (math as any)[name]; // the original function

      replacements[name] = (x: number) => {
        return fn(this.convertAngles(x));
      };
    });

    ['asin', 'acos', 'atan', 'atan2'].forEach((name) => {
      // tslint:disable-next-line
      const fn = (math as any)[name]; // the original function

      replacements[name] = (x: number) => {
        return this.convertRadians(fn(x));
      };
    });

    // import all replacements into mathjs, override existing trigonometric functions
    math.import(replacements, { override: true });
  }

  public roundFloat(val: number): number {
    return Math.round(val * 100000000000) / 100000000000; // tslint:disable-line
  }

  public variablesInExpression(expr: any): any[] {
    return getVariablesInExpression(expr);
  }

  public convertAngles(value: number): number {
    if (this.angles === 'degrees') {
      return value * (Math.PI / 180); // tslint:disable-line
    } else if (this.angles === 'gradians') {
      return value * (Math.PI / 200); // tslint:disable-line
    }

    return value;
  }

  public convertRadians(value: number): number {
    if (this.angles === 'degrees') {
      return (value * 180) / Math.PI; // tslint:disable-line
    } else if (this.angles === 'gradians') {
      return (value * 200) / Math.PI; // tslint:disable-line
    }

    return value;
  }

  // terribly inaccurate
  public getVertex(f: any, start: number, end: number, precision: number): number {
    this.loopcounter++;

    if (Math.abs(end - start) <= precision) {
      this.loopcounter = 0;
      return (end + start) / 2;
    }

    // tslint:disable-next-line
    if (this.loopcounter > 200) {
      this.loopcounter = 0;
      return NaN;
    }

    const interval = (end - start) / 40; // tslint:disable-line
    let xval = start - interval;
    const startanswer = f(xval);
    let prevanswer = startanswer;
    let answer;

    for (xval = start; xval <= end; xval += interval) {
      xval = this.roundFloat(xval);
      answer = f(xval);

      if ((prevanswer > startanswer && answer < prevanswer) || (prevanswer < startanswer && answer > prevanswer)) {
        return this.getVertex(f, xval - 2 * interval, xval, precision);
      }

      prevanswer = answer;
    }

    this.loopcounter = 0;

    return NaN;
  }

  public getDerivative(f: any, xval: number) {
    const eps = MACHINE_EPS;
    /*
     * This is a brute force method of calculating derivatives, using
     * Newton's difference quotient (except without a limit)
     *
     * The derivative of a function f and point x can be approximated by
     * taking the slope of the secant from x to x+h, provided that h is sufficently
     * small. However, if h is too small, then floating point errors may result.
     *
     * This algorithm is an effective 100-point stencil in one dimension for
     * calculating the derivative of any real function y=equation.
     */
    let ddx = 0;

    // the suitable value for h is given at http://www.nrbook.com/a/bookcpdf/c5-7.pdf to be sqrt(eps) * x
    const x = xval;
    const h = Math.sqrt(eps) * (x > 1 || x < -1 ? x : 1);
    const answerx = f(x);

    // tslint:disable-next-line
    for (let i = 1; i <= 50; i++) {
      const diff = h * i;
      const inverseDiff = 1 / diff;

      // h is positive
      xval = x + diff;
      let answer = f(xval);

      ddx += (answer - answerx) * inverseDiff;

      // h is negative
      xval = x - diff;
      answer = f(xval);
      ddx += (answerx - answer) * inverseDiff;
    }

    return ddx / 100;
  }

  // uses Newton's method to find the root of the equation.
  // accurate enough for these purposes.
  public getRoot(equation: any, guess: number, range: number = 5, shifted: boolean = false): number {
    const eps = MACHINE_EPS;
    const expr = math.parse(equation);
    const vars = this.variablesInExpression(expr);

    // newton's method becomes very inaccurate if the root is too close to zero,
    // therefore we just whift everything over a few units.
    // tslint:disable-next-line
    if (guess > -0.1 && guess < 0.1 && shifted !== true) {
      let replacedEquation = equation;

      if (vars.length > 0) {
        const v = vars[0];

        replacedEquation = replacedEquation.replace(new RegExp('\\b' + v + '\\b', 'g'), '(' + v + '+5)');
      }

      // tslint:disable-next-line
      const answer = this.getRoot(replacedEquation, guess - 5, range, true);

      if (!isNaN(answer)) {
        return answer + 5; // tslint:disable-line
      }

      return NaN;
    }

    const center = guess;
    let prev = guess;
    let j = 0;

    const code = expr.compile();
    const variables = this.variablesInExpression(expr);
    const f = (x: number): number => {
      const scope: any = {};

      variables.forEach((name: string) => {
        scope[name] = x;
      });

      return code.eval(scope);
    };

    while (prev > center - range && prev < center + range && j < 100) {
      const xval = prev;
      const answer = f(xval);

      if (answer > -eps && answer < eps) {
        return prev;
      }

      const derivative = this.getDerivative(f, xval);

      if (!isFinite(derivative)) {
        break;
      }

      prev = prev - answer / derivative;
      j++;
    }

    // tslint:disable-next-line
    if (j >= 100) {
      return prev;
    }

    return NaN;
  }

  // uses Newton's method for finding the intersection of the two equations. Actually very simple.
  public getIntersection(equation1: any, equation2: any, guess: any, range: any): number {
    return this.getRoot('(' + equation1 + ') - (' + equation2 + ')', guess, range);
  }

  public makeFunction(equation: string): any {
    const expr = math.parse(equation);
    const code = expr.compile();
    const variables = this.variablesInExpression(expr);

    return (x: number): number => {
      const scope: any = {};

      variables.forEach((name: string) => {
        scope[name] = x;
      });

      return code.eval(scope);
    };
  }
}
