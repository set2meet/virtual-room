/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IGraph2D } from '../../../common/types';
import Graph2DCalc from './Graph2D.calc';
import Line = IGraph2D.Line;
import Point = IGraph2D.Point;
import AngleMode = IGraph2D.AngleMode;
import GridlinesMode = IGraph2D.GridlinesMode;
import EquationRecord = IGraph2D.EquationRecord;
import PrecisionMode = IGraph2D.PrecisionMode;
import { colors } from '../common';

const ZOOM_STEP = 0.1;
const THICKNESS = 3;

const floatFix = (num: number) => {
  // rounds floating points
  return Math.round(num * 10000000) / 10000000; // tslint:disable-line
};

const getBbox = (node: HTMLElement): ClientRect | DOMRect => {
  return node.getBoundingClientRect();
};

export default class Graph2DRender {
  private background: string = colors.base;
  private graph: HTMLCanvasElement;
  private calc: Graph2DCalc;
  private ctx: CanvasRenderingContext2D;
  private width: number = NaN;
  private height: number = NaN;
  private gridlines: GridlinesMode;
  private maxgridlines: Point = { x: 13, y: 13 };
  private charHeight: number = 8;
  private startDrag: Point = { x: 0, y: 0 };
  private prevDrag: Point = { x: 0, y: 0 };
  private startCoord: Line = { x1: 0, y1: 0, x2: 0, y2: 0 };
  private currCoord: Line = { x1: -5, y1: -5, x2: 5, y2: 5 };
  private mousebutton: 0 | 1 = 0;
  private canvasX: number = NaN;
  private canvasY: number = NaN;
  private calccache: Record<string, any> = {};
  private quality: PrecisionMode;
  private zoomFactor: number = 0.1;
  private fillareapath: any[];
  private equationsList: EquationRecord[] = [];
  private equationInx: number = 0;
  private xgridscale: number;
  private ygridscale: number;

  constructor(
    node: HTMLCanvasElement,
    gridlines: GridlinesMode = 'normal',
    angleMode: AngleMode = 'radians',
    precisionMode: PrecisionMode = 1
  ) {
    this.graph = node;
    this.gridlines = gridlines;
    this.quality = precisionMode;
    this.calc = new Graph2DCalc(angleMode);
    this.ctx = this.graph.getContext('2d');

    this.initCanvas();
  }

  public setEquations(equations: EquationRecord[]) {
    this.equationsList = equations;
    this.draw();
  }

  public setCurrentEquation(value: number) {
    this.equationInx = value;
  }

  public setAngleMode(value: AngleMode) {
    this.calc = new Graph2DCalc(value);
    this.draw();
  }

  public setGridlines(value: GridlinesMode) {
    this.gridlines = value;
    this.draw();
  }

  public setQuality(value: PrecisionMode) {
    this.quality = value;
    this.draw();
  }

  private initCanvas() {
    const container = this.graph.parentNode as HTMLElement;
    const wBbox = getBbox(container);

    this.width = wBbox.width;
    this.height = wBbox.height;

    this.resizeGraph(wBbox.width, wBbox.height);
    // tslint:disable-next-line
    this.currCoord = { x1: -5 * (this.width / this.height), y1: -5, x2: 5 * (this.width / this.height), y2: 5 };
    this.startCoord = this.copyCoord(this.currCoord);
  }

  private resizeGraph(width: number, height: number) {
    const oldheight = this.height;
    const oldwidth = this.width;

    // Resize the elements
    this.graph.height = height;
    this.graph.width = width;
    this.height = height;
    this.width = width;

    // Compute the new boundaries of the graph
    this.currCoord.x1 *= width / oldwidth;
    this.currCoord.x2 *= width / oldwidth;
    this.currCoord.y1 *= height / oldheight;
    this.currCoord.y2 *= height / oldheight;
    this.startCoord = this.copyCoord(this.currCoord);

    // Compute how many grid lines to show
    this.maxgridlines.x = 0.015 * width; // tslint:disable-line
    this.maxgridlines.y = 0.015 * height; // tslint:disable-line
    this.draw();
  }

  public resetZoom() {
    // tslint:disable-next-line
    this.currCoord = { x1: -5 * (this.width / this.height), y1: -5, x2: 5 * (this.width / this.height), y2: 5 };
    this.startCoord = this.copyCoord(this.currCoord);
    this.draw();
  }

  public resize = function () {
    const wBbox = getBbox(this.graph.parentNode);

    this.resizeGraph(wBbox.width, wBbox.height);
  };

  private arbRound(value: number, roundTo: number): number {
    return Math.round(value / roundTo) * roundTo;
  }

  private arbFloor(value: number, roundTo: number) {
    return Math.floor(value / roundTo) * roundTo;
  }

  private copyCoord(coord: Line): Line {
    return { ...coord };
  }

  public clearScreen() {
    this.ctx.fillStyle = this.background;
    this.ctx.fillRect(0, 0, this.width, this.height);
  }

  private getEquation(inx: number): string {
    const eqRecord: EquationRecord = this.equationsList[inx];

    return eqRecord && eqRecord.expression;
  }

  private getColor(inx: number): string {
    const eqRecord: EquationRecord = this.equationsList[inx];

    return eqRecord ? eqRecord.color : '#000000';
  }

  // tslint:disable:cyclomatic-complexity
  public drawEquation(equation: any, color: string, thickness: number) {
    if (!(equation && Graph2DCalc.isValidEquation(equation))) {
      return;
    }

    const { ctx } = this;
    const { x1, y1 } = this.currCoord;
    const scale = this.getScale();

    if (!this.calccache[equation]) {
      this.calccache[equation] = {};
    }

    const oldLineWidth = ctx.lineWidth;

    ctx.strokeStyle = color;

    if (thickness) {
      ctx.lineWidth = thickness;
    }

    ctx.beginPath();

    // we don't want to draw lines that go off the screen too much,
    // so we keep track of how many times we've had
    // to go off the screen here
    let lineExists = 0;
    let lastpoint = 0;

    this.fillareapath = [0, this.height - -y1 * scale.y];

    // loop through each pixel

    const inverseQuality = 1.0 / this.quality;
    const inverseScaleX = 1.0 / scale.x;
    const maxxval = this.width + inverseQuality;
    const f = this.calc.makeFunction(equation);

    for (let i = 0; i < maxxval; i += inverseQuality) {
      const xval = i * inverseScaleX + x1; // calculate the x-value for a given pixel
      const yval = f(xval);
      const ypos = this.height - (yval - y1) * scale.y;

      // The line is on the screen, or pretty close to it
      if (ypos >= this.height * -1 && ypos <= this.height * 2) {
        if (lineExists > 1) {
          ctx.beginPath();
        }

        if (!isNaN(lastpoint) && ((lastpoint > 0 && yval < 0) || (lastpoint < 0 && yval > 0))) {
          ctx.moveTo(i, ypos);
        } else {
          ctx.lineTo(i, ypos);
        }

        lineExists = 0;
        lastpoint = NaN;
      }
      // The line is off the screen
      else if (lineExists <= 1) {
        ctx.lineTo(i, ypos);
        lastpoint = yval;
        ctx.stroke();
        lineExists++;
      }

      this.fillareapath.push([i, ypos]);
    }

    this.fillareapath.push([maxxval, this.height - -y1 * scale.y]);
    ctx.stroke();

    // restore line width
    ctx.lineWidth = oldLineWidth;
  }
  // tslint:enable

  public drawFillArea() {
    if (this.fillareapath.length < 1) {
      return;
    }

    const { ctx } = this;
    const len = this.fillareapath.length;

    ctx.beginPath();
    ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';

    for (let i = 0; i < len; i++) {
      if (i === 0) {
        this.ctx.lineTo(this.fillareapath[i][0], this.fillareapath[i][1]);
      } else {
        this.ctx.lineTo(this.fillareapath[i][0], this.fillareapath[i][1]);
      }
    }

    ctx.fill();
  }

  // Draws an arbritrary straight line from (x1, y1) to (x2, y2)
  public drawLine(x1: number, y1: number, x2: number, y2: number, color: string = '#000000', thickness?: number) {
    const { ctx } = this;

    ctx.strokeStyle = color;
    ctx.beginPath();

    const start = this.getCoord(x1, y1);
    const end = this.getCoord(x2, y2);

    ctx.moveTo(start.x, start.y);
    ctx.lineTo(end.x, end.y);

    const oldLineWidth = ctx.lineWidth;

    if (thickness) {
      ctx.lineWidth = thickness;
    }

    ctx.stroke();
    ctx.lineWidth = oldLineWidth;
  }

  // Draws an arbritrary label on the graph, given the numeric values (rather than the pixel values)
  public drawLabel(xval: number, yval: number, text: string, color: string = '#000000') {
    const { ctx } = this;
    const labelCoord = this.getCoord(xval, yval);
    let xpos = labelCoord.x;
    let ypos = labelCoord.y;

    ctx.font = '12pt "open sans"';
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(xpos, ypos);

    /* tslint:disable */
    if (ypos - 4 < this.charHeight) {
      ypos += this.charHeight * 2;
    }

    const textwidth = this.ctx.measureText(text).width;

    if (xpos - 4 < textwidth) {
      xpos += textwidth + 3;
    }

    ctx.fillText(text, xpos - 3, ypos - 3);
    /* tslint:enable */
  }

  public drawDot(xval: number, yval: number, color: string = '#000000', radius: number = 4) {
    const coord = this.getCoord(xval, yval);
    const { ctx } = this;

    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(coord.x, coord.y, radius, 0, Math.PI * 2, false);
    ctx.fill();
  }

  // Draws thge vertex of an equation (i.e. when it changes direction)
  public drawVertex(equation: string, color: string, x: number) {
    if (!(equation && Graph2DCalc.isValidEquation(equation))) {
      return;
    }

    const f = this.calc.makeFunction(equation);
    const scale = this.getScale();
    const xpos = x / scale.x + this.currCoord.x1;
    const matchingDist = 20 / scale.x; //tslint:disable-line
    let answer = this.calc.getVertex(f, xpos - matchingDist, xpos + matchingDist, 0.0000001); //tslint:disable-line
    let tries = 0;

    while (isNaN(answer)) {
      tries++;

      /* tslint:disable */
      if (tries > 5) {
        return;
      }

      answer = this.calc.getVertex(
        f,
        xpos - matchingDist - Math.random() / 100,
        xpos + matchingDist + Math.random() / 100,
        0.0000001
      );
      /* tslint:enable */
    }

    const xval = this.calc.roundFloat(answer);
    let yval = f(xval);

    // tslint:disable-next-line
    yval = this.calc.roundFloat(this.arbRound(yval, 0.0000001));

    this.drawDot(xval, yval, color, 4); // tslint:disable-line

    // draw label text
    // tslint:disable-next-line
    this.drawLabel(xval, yval, this.calc.roundFloat(this.arbRound(xval, 0.0000001)) + ', ' + yval, '#000000');
  }

  // Draws the root of an equation (i.e. where x=0)
  public drawRoot(equation: any, color: string, x: number) {
    if (!(equation && Graph2DCalc.isValidEquation(equation))) {
      return;
    }

    const scale = this.getScale();
    const xpos = x / scale.x + this.currCoord.x1;
    // Calculate the root (within 50 pixels)
    let answer = this.calc.getRoot(equation, xpos, 50 / scale.x); // tslint:disable-line

    if (isNaN(answer)) {
      return;
    }

    answer = floatFix(answer);

    const xval = this.calc.roundFloat(answer);
    const yval = 0;

    // tslint:disable-next-line
    this.drawDot(xval, yval, color, 4); // draw the dot

    // draw label text
    this.drawLabel(xval, yval, this.calc.roundFloat(this.arbRound(xval, 0.00000001)) + ', ' + yval); // tslint:disable-line
  }

  // Draws the intersection of an equation and the nearest equation to the mouse pointer
  public drawIntersect(equation1: any, color: string, x: number) {
    if (!(equation1 && Graph2DCalc.isValidEquation(equation1))) {
      return;
    }

    const scale = this.getScale();
    const xpos = x / scale.x + this.currCoord.x1;
    let equation;
    let answer = NaN;
    const total = this.equationsList.length;

    for (let i = 0, eq; i < total; i++) {
      eq = this.getEquation(i);

      if (eq === equation1) {
        continue;
      }

      // tslint:disable-next-line
      let tempanswer = this.calc.getIntersection(equation1, eq, xpos, 50 / scale.x);

      if (isNaN(tempanswer)) {
        continue;
      }

      tempanswer = floatFix(tempanswer);

      if (!isNaN(tempanswer) && (isNaN(answer) || Math.abs(xpos - answer) > Math.abs(xpos - tempanswer))) {
        answer = tempanswer;
        equation = equation1;
      }
    }

    if (isNaN(answer)) {
      return;
    }

    const xval = this.calc.roundFloat(answer);
    const f = this.calc.makeFunction(equation);
    const yval = f(xval);

    // Draw dot
    this.drawDot(xval, yval, color, 4); // tslint:disable-line

    // Draw label text
    this.drawLabel(xval, yval, floatFix(xval) + ', ' + floatFix(yval), color);
  }

  public drawDerivative(equation: any, color: string, x: number) {
    if (!(equation && Graph2DCalc.isValidEquation(equation))) {
      return;
    }

    const { ctx } = this;
    const f = this.calc.makeFunction(equation);
    const scale = this.getScale();
    const xpos = this.calc.roundFloat(this.arbRound(x / scale.x + this.currCoord.x1, this.xgridscale / 100));

    // Do the actual calculation.
    const slope = floatFix(this.calc.getDerivative(f, xpos));
    let xval = xpos;
    let yval = f(xval);

    yval = this.calc.roundFloat(this.arbRound(yval, 0.0000001)); // tslint:disable-line

    const pos = this.getCoord(xval, yval);

    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(pos.x, pos.y, 4, 0, Math.PI * 2, false); // tslint:disable-line
    ctx.fill();

    // Draw derivative lines of exactly 2*xgridscale long
    const xdist = (this.xgridscale * 2) / Math.sqrt(Math.pow(slope, 2) + 1);
    const ydist = xdist * slope;
    let linestart: Point = { x: xval - xdist, y: yval - ydist };
    let lineend: Point = { x: xval + xdist, y: yval + ydist };

    ctx.beginPath();
    ctx.strokeStyle = '#000000';
    linestart = this.getCoord(linestart.x, linestart.y);
    lineend = this.getCoord(lineend.x, lineend.y);
    ctx.moveTo(linestart.x, linestart.y);
    ctx.lineTo(lineend.x, lineend.y);
    ctx.stroke();

    // Draw label text
    ctx.font = '10pt "open sans"';
    ctx.fillStyle = '#000000';

    /* tslint:disable */
    const text = 'x=' + xval + ', d/dx=' + slope;
    const xval2 = xval; // find out whether to put label above or below dot

    xval -= this.xgridscale / 5;
    const answer2 = f(xval);

    xval += this.xgridscale / 10;
    const answer3 = f(x);

    if (pos.y - 4 < this.charHeight || answer2 > answer3) {
      pos.y += this.charHeight + 3;
    }

    const textwidth = ctx.measureText(text).width;

    if (pos.x - 4 < textwidth) {
      pos.x += textwidth + 3;
    }

    ctx.fillText(text, pos.x - 4, pos.y - 4);
    /* tslint:enable */
  }

  // Draws the trace on an equation
  // xpos is the pixel value of x, not the numerical value
  public drawTrace(equation: any, color: string, x: number) {
    if (!(equation && Graph2DCalc.isValidEquation(equation))) {
      return;
    }

    const f = this.calc.makeFunction(equation);
    const xval = floatFix(this.arbRound(x, this.xgridscale / 100));
    const yval = floatFix(f(xval)); // evaluate the equation
    const ypos = this.getCoord(xval, yval).y;

    this.ctx.strokeStyle = color;

    // Draw the lines if the y-value is on the screen
    if (ypos <= this.height && ypos >= 0) {
      // Draw a line from the point to the x-axis
      this.drawLine(xval, yval, xval, 0, '#999');

      // Draw line from point to the y-axis
      this.drawLine(xval, yval, 0, yval, '#999');

      // draw label text
      this.drawLabel(xval, yval, xval + ', ' + yval, '#000000');
    }

    // Draw dot
    this.drawDot(xval, yval, color, 4); // tslint:disable-line
  }

  public drawGrid() {
    this.clearScreen();

    const { ctx } = this;
    const x1 = this.currCoord.x1;
    const x2 = this.currCoord.x2;
    const y1 = this.currCoord.y1;
    const y2 = this.currCoord.y2;

    const xrange = x2 - x1;
    const yrange = y2 - y1;

    /* tslint:disable */
    // Calculate the scale of the gridlines
    let i, c;

    for (i = 0.000000000001, c = 0; xrange / i > this.maxgridlines.x - 1; c++) {
      // Alternating between 2, 5 and 10
      if (c % 3 === 1) {
        i *= 2.5;
      } else {
        i *= 2;
      }

      // Ensure we don't get into an infinite loop
      if (c > 10000) {
        break;
      }
    }
    /* tslint:enable */

    this.xgridscale = i;

    // Do the same for the y-axis
    /* tslint:disable */
    for (i = 0.000000000001, c = 0; yrange / i > this.maxgridlines.y - 1; c++) {
      if (c % 3 === 1) {
        i *= 2.5;
      } else {
        i *= 2;
      }

      // Ensure we don't get into an infinite loop
      if (c > 10000) {
        break;
      }
    }
    /* tslint:enable */

    this.ygridscale = i;

    ctx.font = '10pt "open sans"';
    ctx.textAlign = 'center';

    // tslint:disable-next-line
    let xaxis = NaN,
      yaxis = NaN,
      xpos,
      ypos;

    // currx is the current gridline being drawn, as a numerical value (not a pixel value)
    let currx = this.arbFloor(x1, this.xgridscale); // set it to before the lowest x-value on the screen
    let curry = this.arbFloor(y1, this.ygridscale);
    // tslint:disable-next-line
    let xmainaxis = this.charHeight * 1.5; // the next two variables are the axis on which text is going to be placed
    let ymainaxis = -1;

    currx = floatFix(currx);
    curry = floatFix(curry);

    // y=0 appears on the screen - move the text to follow
    if (y2 >= 0 && y1 <= 0) {
      // tslint:disable-next-line
      xmainaxis = this.height - ((0 - y1) / (y2 - y1)) * this.height + this.charHeight * 1.5;
    }
    // the smallest value of y is below the screen - the x-axis labels get pushed to the bottom of the screen
    else if (y1 > 0) {
      xmainaxis = this.height - 5; // tslint:disable-line
    }

    // the x-axis labels have to be a certain distance from the bottom of the screen
    if (xmainaxis > this.height - this.charHeight / 2) {
      xmainaxis = this.height - 5; // tslint:disable-line
    }

    // do the same as above with the y-axis

    // y-axis in the middle of the screen
    if (x2 >= 0 && x1 <= 0) {
      ymainaxis = ((0 - x1) / (x2 - x1)) * this.width - 2;
    }
    // y-axis on the right side of the screen
    else if (x2 < 0) {
      ymainaxis = this.width - 6; // tslint:disable-line
    }

    if (ymainaxis < ctx.measureText(curry.toString()).width + 1) {
      ymainaxis = -1;
    }

    // VERTICAL LINES

    // tslint:disable-next-line
    for (let i = 0; i < this.maxgridlines.x; i++) {
      xpos = ((currx - x1) / (x2 - x1)) * this.width; // position of the line (in pixels)

      // make sure it is on the screen
      // tslint:disable-next-line
      if (xpos - 0.5 > this.width + 1 || xpos < 0) {
        currx += this.xgridscale;
        continue;
      }

      currx = floatFix(currx);

      if (currx === 0) {
        xaxis = xpos;
      }

      // tslint:disable-next-line
      if (
        this.gridlines === 'normal' ||
        (this.gridlines === 'less' && this.calc.roundFloat(currx) % this.calc.roundFloat(this.xgridscale * 2) === 0)
      ) {
        ctx.fillStyle = 'rgb(190,190,190)';
        ctx.fillRect(xpos - 0.5, 0, 1, this.height); // tslint:disable-line
      }

      ctx.fillStyle = 'rgb(0,0,0)';

      // Draw label
      if (currx !== 0) {
        const xtextwidth = ctx.measureText(currx.toString()).width;

        // tslint:disable-next-line
        if (xpos + xtextwidth * 0.5 > this.width) {
          // cannot overflow the screen
          xpos = this.width - xtextwidth * 0.5 + 1; // tslint:disable-line
        }
        // tslint:disable-next-line
        else if (xpos - xtextwidth * 0.5 < 0) {
          xpos = xtextwidth * 0.5 + 1; // tslint:disable-line
        }

        ctx.fillText(currx.toString(), xpos, xmainaxis);
      }

      currx += this.xgridscale;
    }

    ctx.textAlign = 'right';

    // HORIZONTAL LINES

    // tslint:disable-next-line
    for (let i = 0; i < this.maxgridlines.y; i++) {
      // position of the line (in pixels)
      ypos = this.height - ((curry - y1) / (y2 - y1)) * this.height;

      // make sure it is on the screen
      // tslint:disable-next-line
      if (ypos - 0.5 > this.height + 1 || ypos < 0) {
        curry += this.ygridscale;
        continue;
      }

      curry = floatFix(curry);

      if (curry === 0) {
        yaxis = ypos;
      }

      // tslint:disable-next-line
      if (
        this.gridlines === 'normal' ||
        (this.gridlines === 'less' && this.calc.roundFloat(curry) % this.calc.roundFloat(this.ygridscale * 2) === 0)
      ) {
        ctx.fillStyle = 'rgb(190,190,190)';
        ctx.fillRect(0, ypos - 0.5, this.width, 1); // tslint:disable-line
      }

      ctx.fillStyle = 'rgb(0,0,0)';

      // Draw label
      if (curry !== 0) {
        const ytextwidth = ctx.measureText(curry.toString()).width;

        if (ypos + this.charHeight / 2 > this.height) {
          // cannot overflow the screen
          ypos = this.height - this.charHeight / 2 - 1;
        }
        /* tslint:disable */
        if (ypos - 4 < 0) {
          ypos = 4;
        }
        /* tslint:enable */

        let xaxispos = ymainaxis;

        if (ymainaxis === -1) {
          xaxispos = ytextwidth + 1;
        }

        // tslint:disable-next-line
        ctx.fillText(curry.toString(), xaxispos, ypos + 3);
      }

      curry += this.ygridscale;
    }

    // Draw the axis
    if (xaxis) {
      // tslint:disable-next-line
      ctx.fillRect(xaxis - 0.5, 0, 1, this.height);
    }
    if (yaxis) {
      // tslint:disable-next-line
      ctx.fillRect(0, yaxis - 0.5, this.width, 1);
    }
  }

  // Get the pixel coordinates of a value
  public getCoord(x: number, y: number): Point {
    const xpos = ((x - this.currCoord.x1) / (this.currCoord.x2 - this.currCoord.x1)) * this.width;
    const ypos = this.height - ((y - this.currCoord.y1) / (this.currCoord.y2 - this.currCoord.y1)) * this.height;

    return { x: xpos, y: ypos };
  }

  // Get the (numerical) position of a (pixel) coordinate
  public getValue(x: number, y: number): Point {
    const scale = this.getScale();
    const xpos = x / scale.x + this.currCoord.x1;
    const ypos = (this.height - y) / scale.y + this.currCoord.y1;

    return { x: xpos, y: ypos };
  }

  // Zoom to a box, the inputs are pixel coordinates
  public doZoomBox(x1: number, y1: number, x2: number, y2: number) {
    if (x1 === x2 || y1 === y2) {
      return;
    }

    const coord1 = this.getValue(x1, y1);
    const coord2 = this.getValue(x2, y2);

    if (x1 > x2) {
      this.currCoord.x1 = coord2.x;
      this.currCoord.x2 = coord1.x;
    } else {
      this.currCoord.x1 = coord1.x;
      this.currCoord.x2 = coord2.x;
    }

    if (y2 > y1) {
      this.currCoord.y1 = coord2.y;
      this.currCoord.y2 = coord1.y;
    } else {
      this.currCoord.y1 = coord1.y;
      this.currCoord.y2 = coord2.y;
    }

    this.startCoord = this.copyCoord(this.currCoord);
    this.draw();
  }

  public drawEquations() {
    this.equationsList.forEach((rec: EquationRecord) => {
      this.drawEquation(rec.expression, rec.color, THICKNESS);
    });
  }

  public draw() {
    this.drawGrid();
    this.drawEquations();
  }

  // Gets the scale (pixels per unit)
  public getScale(): Point {
    return {
      x: this.width / (this.startCoord.x2 - this.startCoord.x1),
      y: this.height / (this.startCoord.y2 - this.startCoord.y1),
    };
  }

  // Get the range of values on the screen
  public getRange(): Point {
    return {
      x: Math.abs(this.startCoord.x2 - this.startCoord.x1),
      y: Math.abs(this.startCoord.y2 - this.startCoord.y1),
    };
  }

  public checkMove(event: any, currtool: IGraph2D.Tool) {
    const bbox = this.graph.getBoundingClientRect();

    this.canvasX = bbox.left;
    this.canvasY = bbox.top;

    const currEq = this.equationInx;
    const x = event.pageX - this.canvasX;
    const y = event.pageY - this.canvasY;

    if (x === this.prevDrag.x && y === this.prevDrag.y) {
      return;
    }

    const scale = this.getScale();

    if (this.mousebutton === 1) {
      if (currtool === 'zoombox') {
        this.draw();
        this.ctx.strokeStyle = 'rgb(150,150,150)';
        this.ctx.strokeRect(this.startDrag.x, this.startDrag.y, x - this.startDrag.x, y - this.startDrag.y);
      }
      // Click and grag
      else {
        this.currCoord.x1 = this.startCoord.x1 - (x - this.startDrag.x) / scale.x;
        this.currCoord.x2 = this.startCoord.x2 - (x - this.startDrag.x) / scale.x;
        this.currCoord.y1 = this.startCoord.y1 + (y - this.startDrag.y) / scale.y;
        this.currCoord.y2 = this.startCoord.y2 + (y - this.startDrag.y) / scale.y;
        this.draw();
      }
    } else if (currtool === 'trace') {
      this.draw();
      this.drawTrace(this.getEquation(currEq), this.getColor(currEq), x / scale.x + this.currCoord.x1);
    } else if (currtool === 'vertex') {
      this.draw();
      this.drawVertex(this.getEquation(currEq), this.getColor(currEq), x);
    } else if (currtool === 'root') {
      this.draw();
      this.drawRoot(this.getEquation(currEq), this.getColor(currEq), x);
    } else if (currtool === 'intersect') {
      this.draw();
      this.drawIntersect(this.getEquation(currEq), this.getColor(currEq), x);
    } else if (currtool === 'derivative') {
      this.draw();
      this.drawDerivative(this.getEquation(currEq), this.getColor(currEq), x);
    }

    this.prevDrag = { x, y };
  }

  public mouseDown(event: any, currtool: IGraph2D.Tool) {
    document.body.style.cursor = 'hand';

    if (this.mousebutton === 0) {
      const bbox = this.graph.getBoundingClientRect();

      this.startDrag.x = event.pageX - bbox.left;
      this.startDrag.y = event.pageY - bbox.top;
      this.startCoord = this.copyCoord(this.currCoord);
    }

    this.mousebutton = 1;
  }

  public mouseUp(event: any, currtool: IGraph2D.Tool) {
    document.body.style.cursor = 'auto';

    /* tslint:disable */
    if (currtool === 'zoombox' && this.mousebutton === 1) {
      this.doZoomBox(this.startDrag.x, this.startDrag.y, event.pageX - this.canvasX, event.pageY - this.canvasY);
    }
    if (currtool === 'zoomin') {
      if (
        Math.abs(event.pageX - this.canvasX - this.startDrag.x) +
          Math.abs(event.pageY - this.canvasY - this.startDrag.y) <
        5
      ) {
        this.zoom(+ZOOM_STEP, event);
      }
    }
    if (currtool === 'zoomout') {
      if (
        Math.abs(event.pageX - this.canvasX - this.startDrag.x) +
          Math.abs(event.pageY - this.canvasY - this.startDrag.y) <
        5
      ) {
        this.zoom(-ZOOM_STEP, event);
      }
    }
    /* tslint:enable */
    this.mousebutton = 0;
    this.startCoord = this.copyCoord(this.currCoord);
  }

  public mouseWheel(event: any) {
    if (event.deltaY > 0) {
      this.zoom(this.zoomFactor, event);
    } else {
      this.zoom(-this.zoomFactor, event);
    }
  }

  public setWindow(x1: number, x2: number, y1: number, y2: number) {
    this.currCoord.x1 = x1;
    this.currCoord.x2 = x2;
    this.currCoord.y1 = y1;
    this.currCoord.y2 = y2;
    this.startCoord = this.copyCoord(this.currCoord);
    this.draw();
  }

  public zoom(scale: number, event: any) {
    const range = this.getRange();

    if (event) {
      const mousex = event.pageX - this.canvasX;
      const mousey = event.pageY - this.canvasY;
      // if we divide the screen into two halves based on the position of the mouse, this is the top half
      const mousetop = 1 - mousey / this.height;
      // as above, but the left hald
      const mouseleft = mousex / this.width;

      this.currCoord.x1 += range.x * scale * mouseleft;
      this.currCoord.y1 += range.y * scale * mousetop;
      this.currCoord.x2 -= range.x * scale * (1 - mouseleft);
      this.currCoord.y2 -= range.y * scale * (1 - mousetop);
    } else {
      this.currCoord.x1 += range.x * scale;
      this.currCoord.y1 += range.y * scale;
      this.currCoord.x2 -= range.x * scale;
      this.currCoord.y2 -= range.y * scale;
    }

    this.startCoord = this.copyCoord(this.currCoord);
    this.draw();
  }
}
