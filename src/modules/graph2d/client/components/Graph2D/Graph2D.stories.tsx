import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { TDynamicModuleWrapperProps } from '../../../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleRegistryKey } from '../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { boolean } from '@storybook/addon-knobs';
import React from 'react';
import { withIoCFactory } from '../../../../../test/utils/utils';
import Graph2D from './Graph2D';
import { DispatchProp } from 'react-redux';
import actionCreators from '../../../common/redux/actionCreators';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { TGraph2dModuleInitOptions } from '../../types/TGraph2dModuleInitOptions';
import { TOnMockClientActionCallback } from '../../../../../test/utils/serverSocket';
import { Graph2dAction } from '../../../common/redux/types/Graph2dAction';
import { action } from '@storybook/addon-actions';

const Graph2DModule = withIoCFactory(() =>
  clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps, TGraph2dModuleInitOptions>(
    ModuleRegistryKey.Graph2D,
    {
      skipRootMount: true,
    }
  )
);

const onActionCallback: TOnMockClientActionCallback = (reduxAction, serverSocket) => {
  if (Object.values(Graph2dAction).includes(reduxAction.type)) {
    action('socket event')(reduxAction);
    serverSocket.emit('action', {
      action: reduxAction,
    });
  }
};

export default {
  title: 'graph2d/Graph2D',
  component: Graph2D,
  parameters: {
    redux: {
      enable: true,
    },
    serverSocket: {
      onActionCallback,
    },
  },
};

export const Default = () => (
  <Graph2DModule>
    <Graph2D isOwner={boolean('isOwner', false)} />
  </Graph2DModule>
);

export const Owner = () => (
  <Graph2DModule>
    <Graph2D isOwner={boolean('isOwner', true)} />
  </Graph2DModule>
);

export const WithEquations = (props: DispatchProp) => {
  const equation1 = {
    expression: 'x^2',
    color: '#d32f2f',
    isValid: true,
  };
  const equation2 = {
    expression: 'x^',
    color: '#2f53d3',
    isValid: false,
  };
  const equation3 = {
    expression: '-x^5',
    color: '#2f53d3',
    isValid: true,
  };
  const ReduxWrapper = ({ dispatch }: DispatchProp) => {
    const reduxAction = actionCreators.updateEquationsList([equation1, equation2, equation3]);

    dispatch(reduxAction);
    return <Graph2D isOwner={boolean('isOwner', true)} />;
  };

  return (
    <Graph2DModule>
      <ReduxWrapper {...props} />
    </Graph2DModule>
  );
};

WithEquations.story = {
  decorators: [connectDecorator()],
};
