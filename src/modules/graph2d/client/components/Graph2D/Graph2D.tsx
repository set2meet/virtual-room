import { IGraph2D } from '../../../common/types';
import React, { MouseEvent } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import EquationsTool from '../EquationsTool';
import { styledContainer, styledToolBox, IStyledToolBoxButton, styledToolBoxButton } from './Graph2D.style';
import Graph2DRender from './Graph2D.render';
import Graph2DTool = IGraph2D.Tool;

const Container = styled.div`
  ${styledContainer};
`;
const ToolBox = styled.div`
  ${styledToolBox};
`;
const ToolBoxButton = styled.button<IStyledToolBoxButton>`
  ${styledToolBoxButton};
`;

interface IGraph2DState {
  currentTool: Graph2DTool;
  currentEqInx: number;
}

import IGraph2DOwnProps = IGraph2D.IModuleProps;
import IGraph2DStateProps = IGraph2D.ReduxStateRoom;

type IGraph2DProps = IGraph2DOwnProps & IGraph2DStateProps;

const BASIC_TOOLS: Array<Graph2DTool | 'separator'> = [
  'pointer',
  'separator',
  'trace',
  'vertex',
  'root',
  'intersect',
  'derivative',
  'separator',
  'zoombox',
  'zoomin',
  'zoomout',
];

export class Graph2D extends React.Component<IGraph2DProps, IGraph2DState> {
  private _renderer: any;
  private _canvas: HTMLCanvasElement;

  private _rendererZoom = (evt: MouseWheelEvent) => {
    this._renderer.mouseWheel(evt);
    return false;
  };

  private _rendererResize = () => {
    this._renderer.resize();
  };

  private _rendererOnMouseDown = (evt: MouseEvent<HTMLCanvasElement>) => {
    this._renderer.mouseDown(evt, this.state.currentTool);
  };

  private _rendererOnMouseMove = (evt: MouseEvent<HTMLCanvasElement>) => {
    this._renderer.checkMove(evt, this.state.currentTool);
  };

  private _rendererOnMouseUp = (evt: MouseEvent<HTMLCanvasElement>) => {
    this._renderer.mouseUp(evt, this.state.currentTool);
  };

  private _updateListeners(method: 'addEventListener' | 'removeEventListener') {
    (window as any)[method]('resize', this._rendererResize, false);
    (this._canvas as any)[method]('mousewheel', this._rendererZoom, false);
    (this._canvas as any)[method]('mousedown', this._rendererOnMouseDown, false);
    (this._canvas as any)[method]('mousemove', this._rendererOnMouseMove, false);
    (this._canvas as any)[method]('mouseup', this._rendererOnMouseUp, false);
  }

  private _rendererInit = (node: HTMLCanvasElement) => {
    if (node) {
      this._canvas = node;
      this._renderer = new Graph2DRender(node);
      this._updateListeners('addEventListener');

      this._renderer.setAngleMode(this.props.angleMode);
      this._renderer.setGridlines(this.props.gridlinesMode);
      this._renderer.setQuality(this.props.precisionMode);
      this._renderer.setEquations(this.props.equationsList);
      this._renderer.setCurrentEquation(this.state.currentEqInx);
    }
  };

  private _changeCurrentTool = (evt: MouseEvent<HTMLButtonElement>) => {
    this.setState({
      currentTool: (evt.target as HTMLButtonElement).name as Graph2DTool,
    });
  };

  private _selectEquation = (inx: number) => {
    this.setState({ currentEqInx: inx });
    this._renderer.setCurrentEquation(inx);
  };

  constructor(props: IGraph2DProps) {
    super(props);

    this.state = {
      currentTool: 'pointer',
      currentEqInx: 0,
    };
  }

  public componentDidUpdate(prevProps: IGraph2DProps) {
    const { angleMode, gridlinesMode, precisionMode, equationsList } = this.props;

    if (prevProps.angleMode !== angleMode) {
      this._renderer.setAngleMode(angleMode);
    }

    if (prevProps.gridlinesMode !== gridlinesMode) {
      this._renderer.setGridlines(gridlinesMode);
    }

    if (prevProps.precisionMode !== precisionMode) {
      this._renderer.setQuality(precisionMode);
    }

    if (prevProps.equationsList !== equationsList) {
      this._renderer.setEquations(equationsList);
    }
  }

  public componentWillUnmount() {
    this._updateListeners('removeEventListener');
  }

  public render() {
    return (
      <Container>
        <canvas ref={this._rendererInit} />
        <EquationsTool
          isEditMode={this.props.isOwner}
          equationInx={this.state.currentEqInx}
          onEquationSelect={this._selectEquation}
        />
        <ToolBox>
          <div>
            {BASIC_TOOLS.map((tool, inx) => {
              if (tool === 'separator') {
                return <hr key={inx} />;
              }

              return (
                <ToolBoxButton
                  key={inx}
                  name={tool}
                  onClick={this._changeCurrentTool}
                  selectedTool={tool === this.state.currentTool}
                />
              );
            })}
          </div>
        </ToolBox>
      </Container>
    );
  }
}

export default connect<IGraph2DStateProps, {}>((state: any) => ({
  ...state.room.graph2d,
}))(Graph2D);
