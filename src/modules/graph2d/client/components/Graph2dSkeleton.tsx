import React from 'react';
import Skeleton from 'react-loading-skeleton';

const Graph2dSkeleton = () => <Skeleton width={'100%'} height={'100%'} />;

export default Graph2dSkeleton;
