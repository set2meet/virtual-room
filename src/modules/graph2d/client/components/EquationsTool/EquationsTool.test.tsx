import { mountStory, setupStorybookForTest } from '../../../../../test/utils/storybook';
import { enzymeCleanup } from '../../../../../test/utils/utils';

beforeAll(setupStorybookForTest());
afterEach(enzymeCleanup);

describe('<EquationsTool />', () => {
  it('fixate opened state', async (done) => {
    const wrapper = await mountStory('graph2d/Graph2D', 'WithEquations', true);

    wrapper.find('EquationsTool').setState({
      isOpened: true,
    });
    wrapper.update();
    expect(wrapper.find('EquationsTool').state()).toEqual(
      expect.objectContaining({
        isOpened: true,
      })
    );
    expect(wrapper.find('EquationsTool')).toMatchSnapshot();
    wrapper.find('EquationsTool').find({ isOpened: true }).last().simulate('click').update();
    expect(wrapper.find('EquationsTool').state()).toEqual(
      expect.objectContaining({
        isOpened: false,
      })
    );
    done();
  });
});
