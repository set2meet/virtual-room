/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import icons from './EquationsTool.icons.svg';
import { colors } from '../common';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const toolPos = 14;
const headerSize = 34;
const toolWidth = 300;
const iconsSize = '34px 146px';

export interface IStyledContainer extends IRequiredTheme {
  isOpened: boolean;
}

export interface IStyledHeader extends IRequiredTheme {
  isOpened: boolean;
}

export const styledContainer = () => `
  margin: 0;
  border: 0;
  padding: 0;
  text-align: left;
  top: ${toolPos}px;
  left: ${toolPos}px;
  position: absolute;
  display: inline-block;
  box-shadow: 0 1px 4px 2px rgba(0, 0, 0, 0.1);
`;

export const styledHeader = ({ isOpened }: IStyledHeader) => `
  cursor: pointer;
  height: ${headerSize}px;
  background-color: ${colors.text};
  width: ${isOpened ? toolWidth : headerSize}px;

  &:before {
    content: ' ';
    display: inline-block;
    width: ${headerSize}px;
    height: ${headerSize}px;
    background-image: url(${icons});
    background-repeat: no-repeat;
    background-size: ${iconsSize};
    background-position: 0 ${isOpened ? -headerSize : 0}px;
  }
`;

export const styledContent = () => `
  padding: 14px;
  color: ${colors.text};
  background-color: #f8f8f8;

  > hr {
    margin: 20px 0;
    border-color: ${colors.text};
  }
`;

export const styledConfigMode = () => `
  width: 100%;
  margin-top: 14px;
  user-select: none;

  > div:first-child {
    width: 70px;
    font-size: 14px;
    font-weight: bold;
    display: inline-block;
  }

  > div:last-child {
    width: calc(100% - 70px);
    font-size: 14px;
    font-weight: bold;
    display: inline-block;
  }
`;

export interface IConfigModeButton extends IRequiredTheme {
  isSelected: boolean;
}

const defaultButtonMargin = 6;

export const styledConfigModeButton = ({ theme, isSelected }: IConfigModeButton) => `
  cursor: pointer;
  height: 22px;
  border-radius: 2px;
  margin-right: ${defaultButtonMargin}px;
  font-weight: normal;
  border: 0;
  padding: 0 8px;
  font-size: 14px;
  outline: none;
  color: ${isSelected ? theme.primaryTextColor : '#575757'};
  background-color: ${isSelected ? theme.primaryColor : colors.base};
`;

export const styledToolControls = () => `
  display: inline-flex;
  justify-content: flex-end;
  align-items: center;
  margin-top: 20px;
  width: 100%;

  > * {
    margin-left: ${defaultButtonMargin}px;
  }
`;

export const styledButtonEquationAdd = () => `
  cursor: pointer;
  outline: none;
  width: 26px;
  height: 26px;
  border-radius: 2px;
  border: solid 1px #ededed;
  background-color: #ffffff;
  color: ${colors.text};
  font-size: 16px;
`;

export const styledButtonEquationEvaluate = ({ theme }: IRequiredTheme) => `
  cursor: pointer;
  outline: none;
  width: 82px;
  height: 26px;
  border-radius: 2px;
  border: 0;
  color: ${theme.primaryTextColor};
  background-color: ${theme.primaryColor};
`;

export const styledEquationsList = () => `
  width: 100%;
`;

const fnPrefixWidth = 26;
const fnButtonDelSize = 26;
const fnDefaultMargin = 8;
const fnColorWidth = 6;

export interface IEquationRecord {
  isEditMode: boolean;
  isSelected: boolean;
}

export const styledEquationRecord = ({ isEditMode, isSelected }: IEquationRecord) => `
  margin-bottom: 10px;

  cursor: ${isEditMode ? 'default' : 'pointer'};

  &:before {
    content: 'y =';
    display: inline-block;
    width: ${fnPrefixWidth}px;
    font-size: 14px;
    font-weight: bold;
    color: ${colors.text};
    letter-spacing: 1.5px;
  }

  > input {
    padding: 0;
    outline: none;
    width: ${fnButtonDelSize}px;
    height: ${fnButtonDelSize}px;
    border-radius: 2px;
    border: solid 1px #ededed;
    background-color: #ffffff;
    box-shadow: none !important;
    vertical-align: middle;
  }

  > input[type="text"] {
    padding: 0 6px;
    border-color: ${isSelected ? colors.text : '#ededed'};
    width: calc(100% - ${fnPrefixWidth + fnButtonDelSize + fnColorWidth + 2 * fnDefaultMargin}px);

    &:disabled {
      background-color: rgba(0, 0, 0, 0);
    }

    &.invalid {
      background-color: rgba(255, 0, 0, 0.1);
    }
  }

  > input[type="button"] {
    user-select: none;
    color: rgba(0, 0, 0, 0);
    background-image: url(${icons});
    background-repeat: no-repeat;
    background-size: ${iconsSize};
    background-position: 0 -${isEditMode ? '68' : isSelected ? '120' : '94'}px;
    margin-left: ${fnDefaultMargin}px;

    &:disabled {
      opacity: 0.5;
    }
  }
`;

export interface IEquationColor {
  color: string;
}

export const styledEquationColor = ({ color }: IEquationColor) => `
  margin-left: ${fnDefaultMargin}px;  
  display: inline-block;
  padding: 0;
  width: ${fnColorWidth}px;
  height: ${fnButtonDelSize - 2}px;
  background-color: ${color};
  vertical-align: middle;
`;
