import { IGraph2D } from '../../../common/types';
import React, { MouseEvent, ChangeEvent, FocusEvent } from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction } from 'redux';
import actionCreators from '../../../common/redux/actionCreators';
import styled from 'styled-components';
import Calc from '../Graph2D/Graph2D.calc';
import {
  IStyledHeader,
  styledContainer,
  styledHeader,
  styledContent,
  styledConfigMode,
  IConfigModeButton,
  styledConfigModeButton,
  styledToolControls,
  styledButtonEquationAdd,
  styledButtonEquationEvaluate,
  styledEquationsList,
  styledEquationRecord,
  IEquationRecord,
  IEquationColor,
  styledEquationColor,
} from './EquationsTool.style';
import AngleMode = IGraph2D.AngleMode;
import GridlinesMode = IGraph2D.GridlinesMode;
import EquationRecord = IGraph2D.EquationRecord;

const MAX_EQUATIONS = 5;
const EQUATION_COLORS = ['#d32f2f', '#0277bd', '#00c853', '#ff5722', '#7e57c2'];

const angleData: Record<string, AngleMode> = {
  Deg: 'degrees',
  Rad: 'radians',
  Grad: 'gradians',
};
const angleButtons = Object.keys(angleData);

const gridlinesData: Record<string, GridlinesMode> = {
  Normal: 'normal',
  Less: 'less',
  Off: 'off',
};
const gridlinesButtons = Object.keys(gridlinesData);

/*
const precisionData: Record<string, PrecisionMode> = {
  Low: 0.1,
  Med: 0.5,
  High: 1,
  Ultra: 5,
};
const precisionButtons = Object.keys(precisionData);
*/

const Container = styled.div`
  ${styledContainer};
`;
const ToolHeader = styled.div<IStyledHeader>`
  ${styledHeader};
`;
const ToolContent = styled.div`
  ${styledContent};
`;
const ConfigMode = styled.div`
  ${styledConfigMode};
`;
const ConfigModeButton = styled.button<IConfigModeButton>`
  ${styledConfigModeButton};
`;
const ToolControls = styled.div`
  ${styledToolControls};
`;
const ButtonAdd = styled.button`
  ${styledButtonEquationAdd};
`;
const ButtonEvaluate = styled.button`
  ${styledButtonEquationEvaluate};
`;
const EquationsList = styled.div`
  ${styledEquationsList};
`;
const EquationItem = styled.div<IEquationRecord>`
  ${styledEquationRecord};
`;
const EquationColor = styled.div<IEquationColor>`
  ${styledEquationColor};
`;

type EquationExpression = EquationRecord & {
  isValid: boolean;
};

interface IEquationsToolOwnProps {
  isEditMode: boolean;
  equationInx: number;
  onEquationSelect: (inx: number) => void;
}

interface IEquationsToolState {
  isOpened: boolean;
  equations: EquationExpression[];
}

import IEquationsToolStateProps = IGraph2D.ReduxStateRoom;

interface IEquationsToolDispatchProps {
  updateModeAngle: ActionCreator<AnyAction>;
  updateModeGridlines: ActionCreator<AnyAction>;
  updateModePrecision: ActionCreator<AnyAction>;
  updateEquationsList: ActionCreator<AnyAction>;
}

type IEquationsToolProps = IEquationsToolOwnProps & IEquationsToolStateProps & IEquationsToolDispatchProps;

export class EquationsTool extends React.Component<IEquationsToolProps, IEquationsToolState> {
  private _toggleVisibility = () => {
    this.setState({
      isOpened: !this.state.isOpened,
    });
  };

  private _changeAngleMode = (evt: MouseEvent<HTMLButtonElement>) => {
    const value = angleData[(evt.target as HTMLButtonElement).name];

    if (value !== this.props.angleMode) {
      this.props.updateModeAngle(value);
    }
  };

  private _changeGridlinesMode = (evt: MouseEvent<HTMLButtonElement>) => {
    const value = gridlinesData[(evt.target as HTMLButtonElement).name];

    if (value !== this.props.gridlinesMode) {
      this.props.updateModeGridlines(value);
    }
  };

  /*private _changePrecisionMode = (evt: MouseEvent<HTMLButtonElement>) => {
    const value = precisionData[(evt.target as HTMLButtonElement).name];

    if (value !== this.props.precisionMode) {
      this.props.updateModePrecision(value);
    }
  };*/

  private _equationAdd = () => {
    const { equations } = this.state;
    const usedColors: Record<string, 1> = equations.reduce((colors: Record<string, 1>, eq) => {
      colors[eq.color] = 1;

      return colors;
    }, {});
    const color = EQUATION_COLORS.filter((value: string) => !usedColors[value])[0];

    this.setState({ equations: [...equations, { expression: '', color, isValid: true }] });
  };

  private _equationDel = (evt: MouseEvent<HTMLInputElement>) => {
    const inx: number = parseInt((evt.target as HTMLInputElement).name, 10);
    const equations = this.state.equations.slice(0);
    const originList = this.props.equationsList;

    equations.splice(inx, 1);

    // if equation existed in origin list, remove it
    if (originList[inx]) {
      originList.splice(inx, 1);
      this.props.updateEquationsList(originList);
    }

    this.setState({ equations });
  };

  private _equationUpdate = (evt: ChangeEvent<HTMLInputElement>) => {
    const inx: number = parseInt((evt.target as HTMLInputElement).name, 10);
    const equations = this.state.equations.slice(0);
    const equation = equations[inx];
    const eqExpression = evt.target.value;

    equation.expression = eqExpression;
    equation.isValid = Calc.isValidEquation(eqExpression);

    this.setState({ equations });
  };

  private _equationSelect = (evt: FocusEvent<HTMLInputElement> | MouseEvent<HTMLInputElement>) => {
    const inx: number = parseInt((evt.target as HTMLInputElement).name, 10);

    this.props.onEquationSelect(inx);
  };

  private _equationsEvaluate = () => {
    this.props.updateEquationsList(this.state.equations);
  };

  private _getEquationsList(): EquationExpression[] {
    const { equationsList } = this.props;

    if (equationsList.length === 0) {
      return [{ expression: '', color: EQUATION_COLORS[0], isValid: true }];
    }

    const list = equationsList.map((eq: EquationRecord) => ({
      ...eq,
      isValid: Calc.isValidEquation(eq.expression),
    }));

    if (this.props.isEditMode) {
      return list;
    }

    return list.filter((eq) => eq.isValid);
  }

  private _updateEquationsList() {
    this.setState({ equations: this._getEquationsList() });
  }

  constructor(props: IEquationsToolProps) {
    super(props);

    this.state = {
      isOpened: false,
      equations: this._getEquationsList(),
    };
  }

  public componentDidUpdate(prevProps: IEquationsToolProps) {
    if (prevProps.equationsList !== this.props.equationsList) {
      this._updateEquationsList();
    }
  }

  private _renderControls() {
    const { equations } = this.state;
    const total = equations.length;

    return (
      <React.Fragment>
        <ToolControls>
          {total < MAX_EQUATIONS && <ButtonAdd onClick={this._equationAdd}>+</ButtonAdd>}
          <ButtonEvaluate onClick={this._equationsEvaluate}>EVALUATE</ButtonEvaluate>
        </ToolControls>
        <hr />
        <ConfigMode>
          <div>Angle</div>
          <div>
            {angleButtons.map((name) => (
              <ConfigModeButton
                key={name}
                name={name}
                onClick={this._changeAngleMode}
                isSelected={angleData[name] === this.props.angleMode}
              >
                {name}
              </ConfigModeButton>
            ))}
          </div>
        </ConfigMode>
        <ConfigMode>
          <div>Gridlines</div>
          <div>
            {gridlinesButtons.map((name) => (
              <ConfigModeButton
                key={name}
                name={name}
                onClick={this._changeGridlinesMode}
                isSelected={gridlinesData[name] === this.props.gridlinesMode}
              >
                {name}
              </ConfigModeButton>
            ))}
          </div>
        </ConfigMode>
        {/*<ConfigMode>
          <div>Precision</div>
          <div>
            {precisionButtons.map((name) => (
              <ConfigModeButton
                key={name}
                name={name}
                onClick={this._changePrecisionMode}
                isSelected={precisionData[name] === this.props.precisionMode}
              >
                {name}
              </ConfigModeButton>
            ))}
          </div>
        </ConfigMode>*/}
      </React.Fragment>
    );
  }

  private _renderTools() {
    const { isEditMode, equationInx } = this.props;
    const { equations } = this.state;
    const total = equations.length;
    const onClickButton = isEditMode ? this._equationDel : this._equationSelect;

    return (
      <ToolContent>
        <EquationsList>
          {equations.map((eq: EquationExpression, inx: number) => (
            <EquationItem key={inx} isEditMode={isEditMode} isSelected={inx === equationInx}>
              <input
                type="text"
                autoComplete="off"
                value={eq.expression}
                name={inx.toString()}
                disabled={!isEditMode}
                onChange={this._equationUpdate}
                onFocus={this._equationSelect}
                className={eq.isValid ? '' : 'invalid'}
              />
              <input type="button" name={inx.toString()} onClick={onClickButton} disabled={total === 1} />
              <EquationColor color={eq.color} />
            </EquationItem>
          ))}
        </EquationsList>
        {isEditMode && this._renderControls()}
      </ToolContent>
    );
  }

  public render() {
    const { isOpened } = this.state;

    return (
      <Container>
        <ToolHeader isOpened={isOpened} onClick={this._toggleVisibility} />
        {isOpened && this._renderTools()}
      </Container>
    );
  }
}

export default connect<IEquationsToolStateProps, IEquationsToolDispatchProps>(
  (state: any) => ({
    ...state.room.graph2d,
  }),
  {
    ...actionCreators,
  }
)(EquationsTool);
