/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import roomReducer from '../common/redux/roomReducer';
import clientReduxMiddleware from '../client/redux/clientMiddleware';
import { TGraph2dModuleInitOptions } from './types/TGraph2dModuleInitOptions';
import Graph2D from './components/Graph2D';

const createGraph2dModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'graph2d',
  moduleInitOptions?: TGraph2dModuleInitOptions
): Promise<IS2MModule> => {
  return {
    rootComponent: moduleInitOptions?.skipRootMount ? null : Graph2D,
    reduxModules: [
      {
        id: stateKey,
        reducersToMerge: {
          room: {
            [stateKey]: roomReducer,
          },
        },
        middlewares: [clientReduxMiddleware(stateKey)(socket)],
      },
    ],
  };
};

export default createGraph2dModule;
