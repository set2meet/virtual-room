/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import 'react-toastify/dist/ReactToastify.min.css';
import { css } from 'styled-components';
import { ITheme } from '../../../../ioc/client/types/interfaces';

interface IProps {
  theme: ITheme;
}

export const styledNotificationContainer = (props: IProps) => css`
  &.Toastify__toast-container {
    position: absolute;

    > .Toastify__toast--success {
      background: ${props.theme.primaryColor};
    }

    & > .Toastify__toast.Toastify__toast--default {
      color: ${props.theme.primaryTextColor};
      background: ${props.theme.primaryColor};

      a {
        color: ${props.theme.backgroundColorSecondary};
      }

      p.no-wrap {
        white-space: nowrap;
      }
    }
  }
`;
