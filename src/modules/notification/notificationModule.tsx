/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { toastsReducer } from 'react-toastify-redux';
import { NotificationContainer } from './components/Notification/Notification';

const createNotificationModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'toasts'
): Promise<IS2MModule> => {
  return {
    rootComponent: NotificationContainer,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: toastsReducer,
        },
      },
    ],
  };
};

export default createNotificationModule;
