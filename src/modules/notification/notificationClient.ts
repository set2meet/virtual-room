/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import actionCreators from './redux/actionCreators';
import { INotificationActionTypes, NotificationAction as actionTypes } from './redux/types/NotificationAction';
import { IVRModule } from '../../ioc/client/types/interfaces';
import { INotificationActionCreators } from './redux/types/INotificationActionCreators';

interface INotification extends IVRModule<INotificationActionCreators> {
  actionTypes: INotificationActionTypes;
  actionCreators: INotificationActionCreators;
}

const notificationClientModule: INotification = {
  actionCreators,
  actionTypes,
};

export default notificationClientModule;
