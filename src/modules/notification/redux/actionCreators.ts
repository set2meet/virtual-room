/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { dismiss, error, message, success, warning } from 'react-toastify-redux';
import { INotificationActionCreators } from './types/INotificationActionCreators';
import { INotification } from './types/INotification';
import { ToastOptions } from 'react-toastify';

const notificationActionCreators: INotificationActionCreators = {
  showNotification: (notification: INotification) => {
    const { type, content, width, ...options } = notification;
    let show;

    // can't create mapper - just use simple if-else construction
    if (type === 'success') {
      show = success;
    } else if (type === 'error') {
      show = error;
    } else if (type === 'warning') {
      show = warning;
    } else {
      show = message;
    }

    // mutate width to style, if existed
    (options as ToastOptions).className = `custom-width-${width ? width : 'normal'}`;
    // here I am cheating - in fact the content is not a string
    // the source module has incorrect definitions
    return show(content as string, options);
  },
  removeNotification: (id: string) => {
    return dismiss(id);
  },
};

export default notificationActionCreators;
