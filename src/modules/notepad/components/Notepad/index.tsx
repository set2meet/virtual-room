import * as React from 'react';
import { INotepadProps } from './INotepadProps';
import styled from 'styled-components';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import templateString from './template.html';
import template from 'lodash/template';

const StyledIFrame = styled.iframe`
  border: 0;
  width: 100%;
  height: 100%;
`;

// TODO
// assets/modules/Notepad/editor.html has config to connect to FireBase (for collaborating)
// rewrite to local server (huge task) or change config for each customer
export class Notepad extends React.Component<INotepadProps> {
  public shouldComponentUpdate(nextProps: Readonly<INotepadProps>, nextState: Readonly<{}>, nextContext: any): boolean {
    return false;
  }

  public render() {
    const source = template(templateString)({
      ...clientEntityProvider.getConfig().notepad,
      roomId: this.props.roomId,
    });

    return <StyledIFrame srcDoc={source} />;
  }
}

export default Notepad;
