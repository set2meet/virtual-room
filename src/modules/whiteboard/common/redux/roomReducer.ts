/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import _cloneDeep from 'lodash/cloneDeep';
import { IWhiteboardRoomReduxState } from './types/IWhiteboardRoomReduxState';
import { WhiteboardAction } from './types/constants/WhiteboardAction';

const INITIAL_STATE: IWhiteboardRoomReduxState = {
  shapes: [],
  selected: [],
};

const reducers = {
  [WhiteboardAction.CLEAR_ALL]() {
    return _cloneDeep(INITIAL_STATE);
  },
  [WhiteboardAction.UPDATE_ROOM_STATE](state: IWhiteboardRoomReduxState, action: AnyAction) {
    return Object.assign({}, state, action.state);
  },
};

export default (state: IWhiteboardRoomReduxState | null, action: AnyAction): IWhiteboardRoomReduxState => {
  state = state || _cloneDeep(INITIAL_STATE);
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
