/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export enum WhiteboardAction {
  CLEAR_ALL = 'WHITEBOARD:CLEAR_ALL',
  UPDATE_ROOM_STATE = 'WHITEBOARD:UPDATE_ROOM_STATE',
  UPDATE_SHARED_STATE = 'WHITEBOARD:UPDATE_SHARED_STATE',
  SELECT_TOOL = 'WHITEBOARD:SELECT_TOOL',
  SELECT_COLOR = 'WHITEBOARD:SELECT_COLOR',
  WHITEBOARD_INIT = 'WHITEBOARD:INIT',
}
