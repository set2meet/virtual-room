/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { Socket } from 'socket.io';
import { IServerMetaObjects } from '../../../ioc/server/types/interfaces';
import { WHITEBOARD_SOCKET_MESSAGE } from '../common/types/socketMessages';

export default (clientSocket: Socket, appMetaObjects: IServerMetaObjects) => {
  clientSocket.on(WHITEBOARD_SOCKET_MESSAGE, async (data: any) => {
    // here no message type - just send message to all in the room
    // appMetaObjects.socketIO.to(`rooms/${data.roomId}`).emit(WHITEBOARD_SOCKET_MESSAGE, data);
    appMetaObjects.socketAdapter.emit({
      target: { roomId: data.roomId },
      event: WHITEBOARD_SOCKET_MESSAGE,
      payload: data,
    });
  });
};
