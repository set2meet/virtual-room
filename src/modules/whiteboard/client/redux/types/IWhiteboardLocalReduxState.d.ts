/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export interface IWhiteboardLocalReduxState {
  local: {
    color: string;
    selectedTool: string;
  };
  shared: Record<
    string,
    {
      // userId:state
      mouseTracker?: any; // current drawing figure
      selected?: {
        // current selected figure
        id: string;
        transform: any;
      };
    }
  >;
}
