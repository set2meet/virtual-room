/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { IWhiteboardRoomReduxState } from '../../common/redux/types/IWhiteboardRoomReduxState';
import _find from 'lodash/find';
import { IWhiteboardLocalReduxState } from './types/IWhiteboardLocalReduxState';
import { WhiteboardAction } from '../../common/redux/types/constants/WhiteboardAction';
import { WhiteboardTool } from '../types/WhiteboardTool';

export const INITIAL_STATE: IWhiteboardLocalReduxState = {
  local: {
    selectedTool: WhiteboardTool.LINE,
    color: 'black',
  },
  shared: {},
};

const reducers = {
  [WhiteboardAction.CLEAR_ALL](state: IWhiteboardLocalReduxState, action: AnyAction) {
    state.shared = {};

    return {
      ...state,
    };
  },
  [WhiteboardAction.SELECT_TOOL](state: IWhiteboardLocalReduxState, action: AnyAction) {
    const key = action.meta.user.id;

    state.shared[key] = {};

    return {
      ...state,
      local: {
        ...state.local,
        selectedTool: action.tool,
      },
    };
  },
  [WhiteboardAction.SELECT_COLOR](state: IWhiteboardLocalReduxState, { color }: AnyAction) {
    return {
      ...state,
      local: {
        ...state.local,
        color,
      },
    };
  },
  [WhiteboardAction.UPDATE_SHARED_STATE](state: IWhiteboardLocalReduxState, { sharedState }: AnyAction) {
    // merge
    return {
      ...state,
      shared: {
        ...state.shared,
        ...sharedState,
      },
    };
  },
  [WhiteboardAction.UPDATE_ROOM_STATE](state: IWhiteboardLocalReduxState, action: AnyAction) {
    // after user finish draw shape, it moves to roomState and we need to remove it from localStore after roomState update
    const newRoomState = action.state as IWhiteboardRoomReduxState;
    const userKey = action.meta.sender;
    const localSharedState = state.shared[userKey] as any;

    // if user send roomStateUpdate, remove his sharedState, because currently it means, that user stop drawing/dragging figures
    if (localSharedState) {
      ['mouseTracker', 'selected'].forEach((key) => {
        const localShape = (state.shared[userKey] as any)[key];

        if (localShape) {
          const isShapeExistInRoom = _find(newRoomState.shapes, { id: localShape.id });

          if (isShapeExistInRoom) {
            (state.shared[userKey] as any)[key] = null;
          }
        }
      });
    }

    return {
      ...state,
    };
  },
};

export default (state = INITIAL_STATE, action: AnyAction): IWhiteboardLocalReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
