/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { AnyAction, Dispatch, Store } from 'redux';
import actionCreators from '../../common/redux/actionCreators';
import { WHITEBOARD_SOCKET_MESSAGE } from '../../common/types/socketMessages';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;
import _throttle from 'lodash/throttle';
import { v4 as guid } from 'uuid';
import { THROTTLE_TIMEOUT } from '../../common/redux/types/constants/whiteboardReduxConstants';
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import { WhiteboardAction } from '../../common/redux/types/constants/WhiteboardAction';
import { WhiteboardTool } from '../types/WhiteboardTool';

const myDeviceGuid = guid().toString();
let socketEventsInitiated: boolean = false;

const whiteboardMiddlewareEnum: IAppReduxEnumMiddleware = {
  [WhiteboardAction.SELECT_TOOL](store: any, next, action, socket, storeKey) {
    if (action.tool === WhiteboardTool.ERASER) {
      store.dispatch(clientEntityProvider.appActionCreators.sendRoomAction(actionCreators.clearWhiteboard()));
    } else {
      next(action);
    }
  },
  [WhiteboardAction.UPDATE_SHARED_STATE](store: any, next, action, socket, storeKey) {
    next(action);

    if (action.stopPropagation) {
      return;
    }

    // todo: make roomId as first parameter in actions,
    // or set roomId into local storage as property
    const { roomId, ...sharedState } = action.sharedState;

    socket.emit(WHITEBOARD_SOCKET_MESSAGE, {
      roomId,
      author: myDeviceGuid,
      payload: sharedState,
    });
  },
  [WhiteboardAction.WHITEBOARD_INIT](store: any, next, action, socket, storeKey) {
    next(action);

    if (socketEventsInitiated) {
      return;
    }

    socketEventsInitiated = true;

    socket.emit(WHITEBOARD_SOCKET_MESSAGE, {
      author: myDeviceGuid,
    });

    socket.on(
      WHITEBOARD_SOCKET_MESSAGE,
      _throttle((data: any) => {
        // update state here, data is json string!
        if (data.author === myDeviceGuid) {
          return;
        }

        // second parameter 'true' as stop propagation
        store.dispatch(actionCreators.updateSharedState(data.payload, true));
      }, THROTTLE_TIMEOUT)
    );
  },
};

const whiteboardMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  if (whiteboardMiddlewareEnum[action.type]) {
    return whiteboardMiddlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default whiteboardMiddleware;
