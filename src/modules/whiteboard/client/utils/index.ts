/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { WhiteboardTool } from '../types/WhiteboardTool';

export function pointInsideRect(point: any, rect: any) {
  const left = rect.left || rect.x;
  const top = rect.top || rect.y;

  return point.x >= left && point.x <= left + rect.width && point.y >= top && point.y <= top + rect.height;
}

export function getShapeRect(shape: any) {
  const end = shape.path.length - 1;

  if (shape.type === WhiteboardTool.ELLIPSE) {
    const halfWidth = Math.abs(shape.path[0].x - shape.path[end].x);
    const halfHeight = Math.abs(shape.path[0].y - shape.path[end].y);

    return {
      x: shape.path[0].x - halfWidth,
      y: shape.path[0].y - halfHeight,
      width: 2 * halfWidth,
      height: 2 * halfHeight,
    };
  } else if (shape.type === WhiteboardTool.PEN) {
    const minX = Math.min(...shape.path.map((p: any) => p.x));
    const maxX = Math.max(...shape.path.map((p: any) => p.x));
    const minY = Math.min(...shape.path.map((p: any) => p.y));
    const maxY = Math.max(...shape.path.map((p: any) => p.y));

    return {
      x: minX,
      y: minY,
      width: maxX - minX,
      height: maxY - minY,
    };
  } else {
    return {
      x: Math.min(shape.path[0].x, shape.path[end].x),
      y: Math.min(shape.path[0].y, shape.path[end].y),
      width: Math.abs(shape.path[0].x - shape.path[end].x),
      height: Math.abs(shape.path[0].y - shape.path[end].y),
    };
  }
}
