import React from 'react';

export default class Line extends React.PureComponent<any> {
  public prepareData() {
    const d = [
      `M ${this.props.path[0].x} ${this.props.path[0].y}`,
      `L ${this.props.path[this.props.path.length - 1].x} ${this.props.path[this.props.path.length - 1].y}`,
    ];

    return d.join(' ');
  }

  public render() {
    const d = this.prepareData();

    return <path d={d} stroke={this.props.color} strokeWidth={this.props.strokeWidth} fill="none" />;
  }
}
