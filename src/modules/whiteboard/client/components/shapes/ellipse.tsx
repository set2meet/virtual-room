import React from 'react';

export default class Ellipse extends React.PureComponent<any> {
  public prepareData() {
    const ellipse = {
      cx: this.props.path[0].x,
      cy: this.props.path[0].y,
      rx: Math.abs(this.props.path[this.props.path.length - 1].x - this.props.path[0].x),
      ry: Math.abs(this.props.path[this.props.path.length - 1].y - this.props.path[0].y),
    };

    return ellipse;
  }

  public render() {
    const ellipseProps = this.prepareData();

    // @ts-ignore
    return (
      <ellipse
        cx={ellipseProps.cx}
        cy={ellipseProps.cy}
        rx={ellipseProps.rx}
        ry={ellipseProps.ry}
        stroke={this.props.color}
        strokeWidth={this.props.strokeWidth}
        fill="none"
      />
    );
  }
}
