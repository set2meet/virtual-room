import React from 'react';

export default class Pen extends React.PureComponent<any> {
  public prepareData() {
    const d = [`M ${this.props.path[0].x} ${this.props.path[0].y}`];

    const collector = this.props.path.map((point: any) => {
      const xNext = point.x;
      const yNext = point.y;
      return `L ${xNext} ${yNext}`;
    });

    return d.concat(collector).join(' ');
  }

  public render() {
    const d = this.prepareData();

    return <path d={d} stroke={this.props.color} strokeWidth={this.props.strokeWidth} fill="none" />;
  }
}
