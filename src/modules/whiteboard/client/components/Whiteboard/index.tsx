/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import React from 'react';
import WhiteBoard from './whiteBoard';
import Tools from '../Tools';
import styled from 'styled-components';
import backgroundImg from './mm.png';
import { IWhiteboardProps } from './IWhiteboardProps';
import { connect } from 'react-redux';
import { IWhiteboardLocalReduxState } from '../../redux/types/IWhiteboardLocalReduxState';
import { IWhiteboardRoomReduxState } from '../../../common/redux/types/IWhiteboardRoomReduxState';
import _get from 'lodash/get';

const StyledDiv = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  position: relative;

  & {
    #whiteBoard {
      border: 1px;
      position: relative;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
    }
    #selection {
      fill: aliceblue;
      fill-opacity: 0.5;
      stroke: blue;
      stroke-dasharray: 5 10;
    }
  }
`;

const SquareDiv = styled.div`
  display: flex;
  width: 110vh;
  height: 100%;
  background-color: #ebebeb;
  background-image: url(${backgroundImg});
  background-size: 106px 106px;
`;

interface IWhiteboardStateProps {
  localState: IWhiteboardLocalReduxState;
  roomState: IWhiteboardRoomReduxState;
}

class Whiteboard extends React.Component<IWhiteboardProps & IWhiteboardStateProps> {
  public render() {
    return (
      <StyledDiv>
        <SquareDiv>
          <WhiteBoard
            localState={this.props.localState}
            roomState={this.props.roomState}
            userId={this.props.userId}
            roomId={this.props.roomId}
            userColor={this.props.userColor}
          />
          <Tools localState={this.props.localState} isModerator={this.props.isModerator} />
        </SquareDiv>
      </StyledDiv>
    );
  }
}

export default connect<IWhiteboardStateProps, null, IWhiteboardProps>((state: any, ownProps: IWhiteboardProps) => {
  return {
    localState: _get(state, ownProps.localStatePath, {}),
    roomState: _get(state, ownProps.statePath, {}),
  };
})(Whiteboard);
