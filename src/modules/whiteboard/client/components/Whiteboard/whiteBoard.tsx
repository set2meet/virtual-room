/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import React from 'react';
import Selection from './selection';
import { getShapeRect, pointInsideRect } from '../../utils';
import { connect } from 'react-redux';
import { IWhiteboardRoomReduxState } from '../../../common/redux/types/IWhiteboardRoomReduxState';
import actionCreators from '../../../common/redux/actionCreators';
import whiteboardSelectors from '../../redux/selectors';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { v4 as guid } from 'uuid';
import { WhiteboardTool } from '../../types/WhiteboardTool';
import _findLast from 'lodash/findLast';
import Line from '../shapes/line';
import Rect from '../shapes/rect';
import Ellipse from '../shapes/ellipse';
import Pen from '../shapes/pen';
import { IWhiteboardLocalReduxState } from '../../redux/types/IWhiteboardLocalReduxState';
import _throttle from 'lodash/throttle';
import { THROTTLE_TIMEOUT } from '../../../common/redux/types/constants/whiteboardReduxConstants';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IAppActionCreators } from '../../../../../ioc/common/ioc-interfaces';

export const mapTools: any = {};

mapTools[WhiteboardTool.LINE] = Line;
mapTools[WhiteboardTool.RECT] = Rect;
mapTools[WhiteboardTool.ELLIPSE] = Ellipse;
mapTools[WhiteboardTool.PEN] = Pen;

const STROKE_WIDTH = 3;

interface IWhiteboardProps {
  localState: IWhiteboardLocalReduxState;
  roomState: IWhiteboardRoomReduxState;
  userId: string;
  roomId: string;
  userColor: string;
}

interface IStateWhiteboardProps {
  selectedTool: string;
  color: string;
}

interface IDispatchWhiteboardProps {
  init: () => void;
  updateSharedState: ActionCreator<AnyAction>;
  updateRoomState: ActionCreator<AnyAction>;
  sendRoomAction: ActionCreator<AnyAction>;
}

class WhiteBoard extends React.Component<IWhiteboardProps & IStateWhiteboardProps & IDispatchWhiteboardProps> {
  private startPoint: { x: number; y: number };
  private rect: ClientRect;
  private _svg: SVGElement;

  constructor(props: any) {
    super(props);

    this.props.init();
    this.startPoint = null;
  }

  public componentDidMount() {
    this.manageEventHandlers('addEventListener');
    this.onResize();
  }

  public componentWillUnmount(): void {
    this.manageEventHandlers('removeEventListener');
  }

  public manageEventHandlers(action: string) {
    (document as any)[action]('mousedown', this.mouseDown);
    (document as any)[action]('mousemove', this.mouseMove);
    (document as any)[action]('mouseup', this.mouseUp);
    (this._svg as any)[action]('mouseenter', this.onResize);
  }

  private _updateSharedState(sharedState: any) {
    this.props.updateSharedState({
      roomId: this.props.roomId,
      [this.props.userId]: sharedState,
    });
  }

  private onResize = () => {
    if (this._svg) {
      this.rect = this._svg.getBoundingClientRect();
    }
  };

  private mousePos = (e: any) => {
    const round = 2;

    return {
      x: round * Math.round(e.clientX / round) - this.rect.left,
      y: round * Math.round(e.clientY / round) - this.rect.top,
    };
  };

  private mouseDown = (e: any) => {
    this.onResize();

    if (pointInsideRect({ x: e.clientX, y: e.clientY }, this.rect)) {
      this.startPoint = this.mousePos(e);

      if (this.props.selectedTool === WhiteboardTool.POINTER) {
        this._selectShape(this.mousePos(e));
      } else {
        const sharedState = { ...this.props.localState.shared[this.props.userId] };

        sharedState.mouseTracker = {
          id: guid().toString(),
          type: this.props.selectedTool,
          path: [this.mousePos(e)],
          color: this.props.color,
        };

        this._updateSharedState(sharedState);
      }
    }
  };

  private mouseMove = (e: any) => {
    if (this.startPoint) {
      const sharedState = { ...this.props.localState.shared[this.props.userId] };

      if (sharedState.mouseTracker) {
        sharedState.mouseTracker.path = [...sharedState.mouseTracker.path, this.mousePos(e)];
        this._updateSharedState(sharedState);
      }
    }
  };

  private mouseUp = (e: any) => {
    const sharedState = { ...this.props.localState.shared[this.props.userId] };

    if (sharedState.mouseTracker || sharedState.selected) {
      const newRoomState = { ...this.props.roomState };
      const lastPos = this.mousePos(e);

      if (sharedState.mouseTracker) {
        sharedState.mouseTracker.path = [...sharedState.mouseTracker.path, lastPos];

        newRoomState.shapes.push(Object.assign({}, sharedState.mouseTracker));
      }

      if (sharedState.selected) {
        const movedShape = _findLast(newRoomState.shapes, (shape) => shape.id === sharedState.selected.id);

        if (movedShape && this.startPoint) {
          movedShape.path = movedShape.path.map(({ x, y }: any) => ({
            x: x + lastPos.x - this.startPoint.x,
            y: y + lastPos.y - this.startPoint.y,
          }));
        }
      }

      this.props.sendRoomAction(actionCreators.updateRoomState(newRoomState));
    }

    this.startPoint = null;
  };

  private onMove = (e: any) => {
    const sharedState = { ...this.props.localState.shared[this.props.userId] };

    if (this.startPoint && sharedState.selected) {
      const mousePos = this.mousePos(e);

      sharedState.selected.transform = {
        x: mousePos.x - this.startPoint.x,
        y: mousePos.y - this.startPoint.y,
      };

      this._updateSharedState(sharedState);
    }
  };

  private _selectShape(position: any) {
    const data = this.props.roomState;
    const selectedShape = _findLast(data.shapes, (shape) => {
      return pointInsideRect(position, getShapeRect(shape));
    });
    const sharedState = { ...this.props.localState.shared[this.props.userId] };

    if (selectedShape) {
      sharedState.selected = {
        id: selectedShape.id,
        transform: {
          x: 0,
          y: 0,
        },
      };
    } else {
      sharedState.selected = null;
    }

    this._updateSharedState(sharedState);
  }

  // returns null of shape not selected by user, otherwise return selected params
  private shapeSelected: (id: string) => any = (id) => {
    const sharedState = this.props.localState.shared;

    const foundUserState = _findLast(sharedState, (userSharedState) => {
      return userSharedState.selected && id === userSharedState.selected.id;
    });

    return foundUserState ? foundUserState.selected : null;
  };

  public render() {
    const data = this.props.roomState;
    const sharedState = this.props.localState.shared;

    const selections: any = [];
    const shapes = data.shapes.map((shape: any) => {
      if (!shape.id) {
        // for backward compatibility
        return null;
      }
      let path = shape.path;
      const shapeSelectedParams = this.shapeSelected(shape.id);

      if (shapeSelectedParams) {
        const move = shapeSelectedParams.transform;
        const selectedRect = getShapeRect(shape);

        if (move) {
          selectedRect.x += move.x;
          selectedRect.y += move.y;

          path = path.map(({ x, y }: any) => ({
            x: x + move.x,
            y: y + move.y,
          }));
        }
        selections.push(<Selection key={`sel_${shape.id}`} rect={selectedRect} />);
      }

      const ShapeClass = mapTools[shape.type];

      if (!ShapeClass) {
        console.error(`no react view for figure ${shape}`);
      }

      return ShapeClass ? (
        <ShapeClass key={shape.id} path={path} color={shape.color} strokeWidth={STROKE_WIDTH} />
      ) : null;
    });

    const currentDrawingShapes = (
      <React.Fragment>
        {Object.keys(sharedState).map((key) => {
          const userFigure = sharedState[key];
          let figure = null;

          if (userFigure && userFigure.mouseTracker && userFigure.mouseTracker.type) {
            const MouseTrackerClass = mapTools[userFigure.mouseTracker.type];

            if (MouseTrackerClass) {
              figure = (
                <MouseTrackerClass
                  key={userFigure.mouseTracker.id}
                  color={userFigure.mouseTracker.color}
                  strokeWidth={STROKE_WIDTH}
                  path={userFigure.mouseTracker.path}
                />
              );
            }
          }

          return figure;
        })}
      </React.Fragment>
    );

    return (
      <svg id="whiteBoard" ref={(canvas) => (this._svg = canvas)} onMouseMove={this.onMove}>
        {shapes}
        {currentDrawingShapes}
        {...selections}
      </svg>
    );
  }
}

export default connect<IStateWhiteboardProps, IDispatchWhiteboardProps>(
  (state, ownProps: IWhiteboardProps) => ({
    selectedTool: whiteboardSelectors.getSelectedTool(ownProps.localState),
    color: ownProps.userColor,
  }),
  (dispatch) => {
    const appActionCreators: IAppActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      // throttle user events
      updateSharedState: _throttle(bindActionCreators(actionCreators.updateSharedState, dispatch), THROTTLE_TIMEOUT),
      updateRoomState: bindActionCreators(actionCreators.updateRoomState, dispatch),
      sendRoomAction: bindActionCreators(appActionCreators.sendRoomAction, dispatch),
      init: bindActionCreators(actionCreators.initWhiteboard, dispatch),
    };
  }
)(WhiteBoard) as any;
