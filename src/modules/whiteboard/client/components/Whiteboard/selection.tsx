/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import React from 'react';

export default class Selection extends React.Component<any> {
  public render() {
    const { rect } = this.props;

    return <rect id="selection" x={rect.x} y={rect.y} width={rect.width} height={rect.height} />;
  }
}
