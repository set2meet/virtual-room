/* tslint:disable */
import React from 'react';
import { connect } from 'react-redux';
import whiteboardActionCreators from '../../common/redux/actionCreators';
import { ActionCreator, AnyAction } from 'redux';

const COLORS = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'black'];

export interface IColorPickerProps {
  color: string;
}

interface IColorPickerDispatchProps {
  selectColor: ActionCreator<AnyAction>;
}

class ColorPicker extends React.Component<IColorPickerProps & IColorPickerDispatchProps> {
  handleClick(index: number) {
    return () => {
      this.props.selectColor(COLORS[index]);
    };
  }

  public render() {
    const colors = COLORS.map((color: string, i: number) => {
      const style = {
        display: 'flex',
        backgroundColor: color,
        width: 20,
        height: 20,
        margin: 1,
      };
      const borderStyle = {
        display: 'flex',
        border: this.props.color === color ? '1px solid' : '1px transparent',
        margin: 1,
      };

      return (
        <div key={i} style={borderStyle} onClick={this.handleClick(i).bind(this)}>
          <div style={style} />
        </div>
      );
    });

    return <div> {colors} </div>;
  }
}

export default connect<null, IColorPickerDispatchProps>(null, {
  selectColor: whiteboardActionCreators.selectColor,
})(ColorPicker);
