/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import toolBoxIcons from './Tools.icons.svg';
import { TOOLS_DATA, ToolItem, STUFF_SEPARATOR } from './Tools.data';

const toolBoxWidth = 48;
const toolBoxMargin = 8;
const toolBoxPadding = 10;
const toolBoxInnerPadding = 8;
const toolBoxItemSize = toolBoxWidth - 2 * toolBoxInnerPadding;

export const styledToolsContainer = () => `
  top: 0;
  left: 0;
  height: 100%;
  width: ${toolBoxWidth + 2 * toolBoxPadding}px;
  padding: ${toolBoxPadding}px;
  position: absolute;
  display: inline-flex;
  align-items: center;
  z-index: 3;

  > div {
    width: ${toolBoxWidth}px;
    display: inline-block;
    padding: ${toolBoxInnerPadding}px;
    background-color: #fff;
    box-shadow: 0 1px 4px 2px rgba(0, 0, 0, 0.1);

    > hr {
      border-color: #a7a7a7;

      &:not(:last-child) {
        margin: ${toolBoxMargin}px 0 ${toolBoxMargin * 2}px 0;
      }
    }
  }
`;

export interface IStyledToolBoxButton {
  selectedTool: boolean;
}

const toolBoxSelectedButtonDelta = 2;

const styledToolBoxButtonSelected = `
  opacity: 1;

  &:before {
    content: ' ';
    position: absolute;
    top: -${toolBoxSelectedButtonDelta}px;
    left: -${toolBoxSelectedButtonDelta}px;
    border-radius: 4px;
    width: ${toolBoxItemSize + 2 * toolBoxSelectedButtonDelta}px;
    height: ${toolBoxItemSize + 2 * toolBoxSelectedButtonDelta}px;
    background-color: rgba(0, 0, 0, 0.1);
  }
`;

const toolBoxIconSize = 32;

const styledToolBoxButtonIcons = TOOLS_DATA.filter((rec) => rec.id !== STUFF_SEPARATOR).reduce(
  (css: string, tool: ToolItem, inx: number) => {
    return (
      css +
      `
      &[name="${tool.id}"] {
        background-position: 0 -${inx * toolBoxIconSize}px;
      }
    `
    );
  },
  ''
);

export const styledToolBoxButton = ({ selectedTool }: IStyledToolBoxButton) => `
  background-image: url(${toolBoxIcons});
  background-repeat: no-repeat;
  background-position: 0 0;
  background-size: 32px 288px;
  opacity: 0.6;
  background-color: transparent;
  position: relative;

  outline: none;
  margin: 0;
  border: 0;
  padding: 0;
  border-radius: 4px;
  width: ${toolBoxItemSize}px;
  height: ${toolBoxItemSize}px;
  cursor: pointer;

  &:not(:last-child) {
    margin-bottom: ${toolBoxMargin}px;
  }

  ${styledToolBoxButtonIcons};
  ${selectedTool ? styledToolBoxButtonSelected : ''};
`;
