import React, { MouseEvent } from 'react';
import { connect } from 'react-redux';
import whiteboardSelectors from '../../redux/selectors';
import actionCreators from '../../../common/redux/actionCreators';
import { ActionCreator, AnyAction } from 'redux';
import { styledToolsContainer, IStyledToolBoxButton, styledToolBoxButton } from './Tools.style';
import styled from 'styled-components';
import { TOOLS_DATA, ToolItem, STUFF_SEPARATOR } from './Tools.data';
import { IWhiteboardLocalReduxState } from '../../redux/types/IWhiteboardLocalReduxState';

interface ITools {
  isModerator: boolean;
  localState: IWhiteboardLocalReduxState;
}

const ToolsContainer = styled.div`
  ${styledToolsContainer};
`;
const ToolButton = styled.button<IStyledToolBoxButton>`
  ${styledToolBoxButton};
`;

interface IStateToolsProps {
  selectedTool: string;
  color: string;
}

interface IDispatchToolsProps {
  selectTool: ActionCreator<AnyAction>;
}

class Tools extends React.Component<ITools & IStateToolsProps & IDispatchToolsProps> {
  private _changeTool = (evt: MouseEvent<HTMLButtonElement>) => {
    const toolId = (evt.target as HTMLButtonElement).name;

    this.props.selectTool(toolId);
  };

  public render() {
    const { selectedTool, isModerator = false } = this.props;

    return (
      <ToolsContainer>
        <div>
          {TOOLS_DATA.map(({ id, isModeratorOnly }: ToolItem, inx: number) => {
            if (isModeratorOnly && isModeratorOnly !== isModerator) {
              return null;
            }

            if (id === STUFF_SEPARATOR) {
              return <hr key={inx} />;
            }

            return <ToolButton key={id} name={id} onClick={this._changeTool} selectedTool={selectedTool === id} />;
          })}
        </div>
      </ToolsContainer>
    );
  }
}

export default connect<IStateToolsProps, IDispatchToolsProps, ITools>(
  (state, ownProps: ITools) => ({
    selectedTool: whiteboardSelectors.getSelectedTool(ownProps.localState),
    color: whiteboardSelectors.getSelectedColor(ownProps.localState),
  }),
  {
    selectTool: actionCreators.selectTool,
  }
)(Tools);
