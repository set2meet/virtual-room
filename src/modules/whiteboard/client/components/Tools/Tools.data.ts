/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { WhiteboardTool } from '../../types/WhiteboardTool';

export type ToolItem = {
  id: string;
  name?: string;
  isModeratorOnly?: boolean;
};

export const STUFF_SEPARATOR = 'separator';

export const TOOLS_DATA: ToolItem[] = [
  { id: WhiteboardTool.POINTER, name: 'cursor' },
  { id: STUFF_SEPARATOR },
  { id: WhiteboardTool.PEN, name: 'pen' },
  { id: WhiteboardTool.RECT, name: 'rect' },
  { id: WhiteboardTool.ELLIPSE, name: 'ellipse' },
  { id: WhiteboardTool.LINE, name: 'line' },
  { id: WhiteboardTool.ERASER, name: 'eraser', isModeratorOnly: true },
];
