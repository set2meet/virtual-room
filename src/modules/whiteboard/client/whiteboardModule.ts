/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import Whiteboard from './components/Whiteboard';
import whiteboardLocalReducer from './redux/localReducer';
import clientWhiteboardMiddleware from './redux/clientMiddleware';
import roomReducer from '../common/redux/roomReducer';

const createWhiteboardModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'whiteboard'
): Promise<IS2MModule> => {
  return {
    rootComponent: Whiteboard,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: whiteboardLocalReducer,
        },
        reducersToMerge: {
          room: {
            [stateKey]: roomReducer,
          },
        },
        middlewares: [clientWhiteboardMiddleware(stateKey)(socket)],
      },
    ],
  };
};

export default createWhiteboardModule;
