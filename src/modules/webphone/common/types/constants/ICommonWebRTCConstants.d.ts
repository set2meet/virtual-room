/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { CONNECTION_STATUS, SERVICE_ACTIONS, SERVICE_EVENTS } from './webrtc';

export interface ICommonWebRTCConstants {
  CONNECTION_STATUS: typeof CONNECTION_STATUS;
  SERVICE_ACTIONS: typeof SERVICE_ACTIONS;
  SERVICE_EVENTS: typeof SERVICE_EVENTS;
  PARTICIPANT_UPDATE_AUDIO_LEVEL_TIME: number;
}
