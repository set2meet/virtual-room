/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { SERVICE_ACTIONS } from '../types/constants/webrtc';
import { IWebrtcParticipantInfo, IWebrtcPublisherInfo } from '../types/IWebrtcParticipantInfo';
import { IWebrtcMediaStreamTrackData } from '../types/webrtcMediaTypes';
import { IWebPhoneUserSetupTab } from '../../client/components/UserSetup/UserSetup.types';
import { WebrtcServiceType } from '../../../../ioc/common/ioc-constants';
import { WebphoneAction } from './types/WebphoneAction';
import {
  IWebrtcServiceAccessParams,
  IWebrtcTestCallConfig,
  IScreenSharingSupportInfo,
  UserMediaErrorName,
} from '../../../../ioc/common/ioc-interfaces';

export const actionCreators = {
  webphoneConnect: () => ({
    type: WebphoneAction.WEBPHONE_CONNECT,
  }),
  webphoneDisconnect: (reason: string) => ({
    type: WebphoneAction.WEBPHONE_DISCONNECT,
    reason,
  }),
  // module init action
  initWebphoneModule: () => ({
    type: WebphoneAction.WEBPHONE_MODULE_INIT,
  }),
  changeWebrtcService: (serviceName: WebrtcServiceType) => ({
    type: WebphoneAction.CHANGE_WEBRTC_SERVICE,
    serviceName,
  }),
  webrtcServiceWasChanged: () => ({
    type: WebphoneAction.WEBRTC_SERVICE_WAS_CHANGED,
  }),
  webrtcServiceSetAccessParams: (params?: IWebrtcServiceAccessParams) => ({
    type: WebphoneAction.SET_WEBRTC_ACCESS_PARAMS,
    params,
  }),
  initWebrtc: () => ({
    type: WebphoneAction.INIT_WEBRTC,
  }),
  startMeeting: () => ({
    type: WebphoneAction.START_MEETING,
  }),
  endMeeting: () => ({
    type: WebphoneAction.END_MEETING,
  }),
  sessionStart: (serviceName: string) => ({
    type: SERVICE_ACTIONS.START_SESSION,
    serviceName,
  }),
  sessionEnd: (serviceName: string, sessionId: string) => ({
    type: SERVICE_ACTIONS.END_SESSION,
    timeSessionClosed: Date.now(),
    serviceName,
    sessionId,
  }),
  sessionConnect: () => ({
    type: WebphoneAction.SESSION_CONNECT,
  }),
  sessionDisconnect: (sessionId: string) => ({
    type: WebphoneAction.SESSION_DISCONNECT,
    sessionId,
  }),
  sessionClosed: (sessionId: string) => ({
    type: WebphoneAction.SESSION_CLOSED,
    sessionId,
  }),
  sessionConnectedSusscess: () => ({
    type: WebphoneAction.SESSION_CONNECTED_SUCCESS,
  }),
  connectionUpdateStatus: (status: string | symbol) => ({
    type: WebphoneAction.CONNECTION_UPDATE_STATUS,
    status,
  }),
  publisherUpdate: (publisher: Partial<IWebrtcPublisherInfo>) => ({
    type: WebphoneAction.PUBLISHER_UPDATE,
    publisher,
  }),
  participantToggleTalking: (userId: string, isTalking: boolean) => ({
    type: WebphoneAction.PARTICIPANT_TOGGLE_TALKING,
    userId,
    isTalking,
  }),
  updateParticipantInfo: (participant: IWebrtcParticipantInfo) => ({
    type: WebphoneAction.PARTICIPANT_UPDATE_INFO,
    participant,
  }),
  toggleAudioStream: (isMuted?: boolean) => ({
    type: WebphoneAction.TOGGLE_AUDIO_STREAM,
    isMuted,
  }),
  toggleVideoStream: (isMuted?: boolean) => ({
    type: WebphoneAction.TOGGLE_VIDEO_STREAM,
    isMuted,
  }),
  baseScreenSetLoudestUser: (userId: string) => ({
    type: WebphoneAction.BASE_SCREEN_SET_LOUDEST_USER,
    userId,
  }),
  baseScreenSetSelectedUser: (userId: string) => ({
    type: WebphoneAction.BASE_SCREEN_SET_SELECTED_USER,
    userId,
  }),
  baseScreenDelSelectedUser: () => ({
    type: WebphoneAction.BASE_SCREEN_DEL_SELECTED_USER,
  }),
  baseScreenSetFullscreenMode: (status: boolean) => ({
    type: WebphoneAction.BASE_SCREEN_SET_FULLSCREEN_MODE,
    status,
  }),
  screenSharingUpdateCapabilities: (screenSharingSupport: IScreenSharingSupportInfo) => ({
    type: WebphoneAction.SCREEN_SHARING_CAPABILITIES_UPDATE,
    screenSharingSupport,
  }),
  screenSharingSetError: (errName: UserMediaErrorName) => ({
    type: WebphoneAction.SCREEN_SHARING_SET_ERROR,
    errName,
  }),
  screenSharingSetAvaliability: (value: boolean) => ({
    type: WebphoneAction.SCREEN_SHARING_SET_AVALIABILITY,
    value,
  }),
  toggleScreenSharing: () => ({
    type: WebphoneAction.TOGGLE_SCREEN_SHARING,
  }),
  webphoneUserSetupShow: (tab: IWebPhoneUserSetupTab) => ({
    type: WebphoneAction.WEBPHONE_USER_SETUP_SHOW,
    tab,
  }),
  webphoneUserSetupComplete: () => ({
    type: WebphoneAction.WEBPHONE_USER_SETUP_COMPLETE,
  }),
  webphoneSetStreamFromCamera: (stream: MediaStream) => ({
    type: WebphoneAction.WEBPHONE_SET_STREAM_FROM_CAMERA,
    stream,
  }),
  webphoneRequestStreamFromCamera: () => ({
    type: WebphoneAction.WEBPHONE_REQUEST_STREAM_FROM_CAMERA,
  }),
  webphoneSetStreamFromMicrophone: (stream: MediaStream) => ({
    type: WebphoneAction.WEBPHONE_SET_STREAM_FROM_MICROPHONE,
    stream,
  }),
  webphoneRequestStreamFromMicrophone: () => ({
    type: WebphoneAction.WEBPHONE_REQUEST_STREAM_FROM_MICROPHONE,
  }),
  webphoneUserSetupSetDontShowAgain: (value: boolean) => ({
    type: WebphoneAction.WEBPHONE_USER_SETUP_SET_DONT_SHOW_AGAIN,
    value,
  }),
  webphoneUserSetupToggleDisable: (value: boolean) => ({
    type: WebphoneAction.WEBPHONE_USER_SETUP_TOGGLE_DISABLE,
    value,
  }),
  webphoneUserSetupTestCallToggleProgress: (value: boolean) => ({
    type: WebphoneAction.WEBPHONE_USER_SETUP_TEST_CALL_TOGGLE_PROGRESS,
    value,
  }),
  webphoneSetConnectionQuality: (value: number) => ({
    type: WebphoneAction.WEBPHONE_SET_CONNECTION_QUALITY,
    value,
  }),
  webphoneTestCallStart: (config: IWebrtcTestCallConfig) => ({
    type: WebphoneAction.WEBPHONE_TEST_CALL_START,
    config,
  }),
  webphoneTestCallStop: (byUser: boolean = false) => ({
    type: WebphoneAction.WEBPHONE_TEST_CALL_STOP,
    byUser,
  }),
  webphoneUserLeftCall: (userId: string) => ({
    type: WebphoneAction.WEBPHONE_USER_LEFT_CALL,
    userId,
  }),
  updateParticipantMediaStreamTrack: (userId: string, tracks: IWebrtcMediaStreamTrackData[]) => ({
    type: WebphoneAction.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK,
    userId,
    tracks,
  }),
};

export type TWebphoneActionCreators = typeof actionCreators;
