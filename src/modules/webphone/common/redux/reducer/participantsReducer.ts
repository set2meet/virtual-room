/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { TWebPhoneParticipantsInfo } from '../types/TWebPhoneParticipantsInfo';
import { WebphoneAction } from '../types/WebphoneAction';

interface IWebPhoneParticipantsReduxReducer {
  [key: string]: (state: TWebPhoneParticipantsInfo, action: AnyAction) => TWebPhoneParticipantsInfo;
}

const INITIAL_STATE: TWebPhoneParticipantsInfo = {};

const reducers: IWebPhoneParticipantsReduxReducer = {
  [WebphoneAction.WEBPHONE_DISCONNECT](state: TWebPhoneParticipantsInfo, action: AnyAction) {
    return INITIAL_STATE;
  },
  [WebphoneAction.WEBPHONE_USER_LEFT_CALL](state: TWebPhoneParticipantsInfo, action: AnyAction) {
    const newState = { ...state };

    delete newState[action.userId];

    return newState;
  },
  [WebphoneAction.PARTICIPANT_UPDATE_INFO](state: TWebPhoneParticipantsInfo, action: AnyAction) {
    const userId = action.participant.userId;

    return {
      ...state,
      [userId]: {
        ...state[userId],
        ...action.participant,
      },
    };
  },
};

export default (state: TWebPhoneParticipantsInfo | null, action: AnyAction): TWebPhoneParticipantsInfo => {
  state = state || INITIAL_STATE;
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
