/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import reducer from './participantsReducer';
import { AnyAction } from 'redux';
import { WebphoneAction } from '../types/WebphoneAction';

describe('webphone participants reducer', () => {
  const action: AnyAction = { type: '@@INIT' };
  const ownAction: AnyAction = {
    type: WebphoneAction.PARTICIPANT_UPDATE_INFO,
    participant: {
      userId: '123',
    },
  };

  it('should use INITIAL_STATE for undefined and null incoming state', () => {
    expect(reducer(undefined, action)).toEqual({});
    expect(reducer(null, action)).toEqual({});
    expect(reducer({}, ownAction)).toEqual({
      [ownAction.participant.userId]: ownAction.participant,
    });
    expect(reducer(undefined, ownAction)).toEqual({
      [ownAction.participant.userId]: ownAction.participant,
    });
    expect(reducer(null, ownAction)).toEqual({
      [ownAction.participant.userId]: ownAction.participant,
    });
  });
});
