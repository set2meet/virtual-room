/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import { IWebrtcMediaStreamTrackKind, MediaElement } from '../../../common/types/webrtcMediaTypes';
import { IWebPhoneUserSetupState } from '../../components/UserSetup/UserSetup.types';
import { IWebrtcServiceAccessParams, IScreenSharingSupportInfo } from '../../../../../ioc/common/ioc-interfaces';
import { CONNECTION_STATUS } from '../../../common/types/constants/webrtc';
import { IWebrtcPublisherInfo } from '../../../common/types/IWebrtcParticipantInfo';

export interface IWebPhoneConnectionQualityState {
  score: number; // mean opinion score
}

export interface IScreenSharingSupportData {
  toggleControlAvailable: boolean;
}

interface IWebPhoneBaseScreenInfo {
  loudestUser: string | null;
  selectedUser: string | null;
  autoSelectLoudestUser: boolean;
}

export interface IWebPhoneReduxState {
  userSetup: IWebPhoneUserSetupState;
  connectionQuality: IWebPhoneConnectionQualityState;
  webrtcServiceName?: WebrtcServiceType;
  accessParams?: IWebrtcServiceAccessParams;
  isTalking: Record<string, boolean>;
  publisher: IWebrtcPublisherInfo;
  connectionStatus: CONNECTION_STATUS;
  isConnected: boolean;
  baseScreen: IWebPhoneBaseScreenInfo;
  screenSharing: IScreenSharingSupportInfo & IScreenSharingSupportData;
  mediaElements: Record<IWebrtcMediaStreamTrackKind, Record<string, MediaElement>>;
}
