/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
type AnyFunction = (...args: any[]) => any;

// merge two argument's arrays into one
const merge = (args1: any[] = [], args2: any[] = []): any[] => {
  const len = Math.max(args1.length, args2.length);
  const result = new Array(len);

  for (let i = 0; i < len; i++) {
    result[i] = args2[i] || args1[i];
  }

  return result;
};

// return wrapper that debounce with merged argumens
export default function debounceWithMergeArguments(func: AnyFunction, ms: number) {
  let debounce: AnyFunction;
  let wrapper: AnyFunction;
  let savedArgs: any[];
  let timeout: number;

  debounce = () => {
    const args = savedArgs.slice(0);

    timeout = null;
    savedArgs.length = 0;
    func.apply(null, args);
  };

  wrapper = (...args: any[]) => {
    clearTimeout(timeout);
    savedArgs = merge(savedArgs, args);
    timeout = window.setTimeout(debounce, ms);
  };

  return wrapper;
}
