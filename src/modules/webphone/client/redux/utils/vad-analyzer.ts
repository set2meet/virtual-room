/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { PARTICIPANT_UPDATE_AUDIO_LEVEL_TIME } from '../../../common/types/constants/webrtc';

const AUDIO_LEVEL_AVERAGE_ACCURACY = 6; // decimal places
const ACCURACY_1 = Math.pow(10, +AUDIO_LEVEL_AVERAGE_ACCURACY);
const ACCURACY_2 = Math.pow(10, -AUDIO_LEVEL_AVERAGE_ACCURACY);
const PARTICIPANT_CHECK_AVERAGE_TIME = 2000; // 2s;
const PARTICIPANT_CHECK_AVERAGE_MAX_TICKS = Math.round(
  PARTICIPANT_CHECK_AVERAGE_TIME / PARTICIPANT_UPDATE_AUDIO_LEVEL_TIME
);

// store current audio level of all participants
const talkingAudioLevel: {
  loudestSpeaker: {
    userId: string;
    averageLevel: number;
  } | null;
  audioLevels: Record<
    string,
    {
      summaryLevel: number;
      average: number;
      levels: number[];
    }
  >;
} = {
  loudestSpeaker: null,
  audioLevels: {},
};

const updateAverageAudioLevel = (userId: string, audioLevel: number) => {
  let userData = talkingAudioLevel.audioLevels[userId];

  if (!userData) {
    talkingAudioLevel.audioLevels[userId] = userData = {
      levels: [],
      average: 0,
      summaryLevel: 0,
    };
  }

  if (userData.levels.length === PARTICIPANT_CHECK_AVERAGE_MAX_TICKS) {
    userData.summaryLevel -= userData.levels.shift();
  }

  userData.levels.push(audioLevel);
  userData.summaryLevel += audioLevel;
  userData.average = Math.round((userData.summaryLevel / userData.levels.length) * ACCURACY_1) * ACCURACY_2;

  return userData.average;
};

export const updateTalkingAudioLevel = (userId: string, audioLevel: number, callback: (userId: string) => void) => {
  let { loudestSpeaker } = talkingAudioLevel;

  // if user just start talking
  if (!talkingAudioLevel.loudestSpeaker) {
    talkingAudioLevel.loudestSpeaker = {
      averageLevel: audioLevel,
      userId,
    };

    callback(userId);
  }

  const averageLevel = updateAverageAudioLevel(userId, audioLevel);

  if (!loudestSpeaker || (loudestSpeaker.userId !== userId && loudestSpeaker.averageLevel < averageLevel)) {
    talkingAudioLevel.loudestSpeaker = loudestSpeaker = {
      averageLevel,
      userId,
    };

    callback(userId);
  } else if (loudestSpeaker.userId === userId) {
    loudestSpeaker.averageLevel = averageLevel;
  }
};

export const participantStopTalking = (userId: string) => {
  // user stop talking by mute
  delete talkingAudioLevel.audioLevels[userId];

  // if this user the loudest speaker - remove one from the base screen
  const { loudestSpeaker } = talkingAudioLevel;

  if (loudestSpeaker && loudestSpeaker.userId === userId) {
    talkingAudioLevel.loudestSpeaker = null;
  }
};
