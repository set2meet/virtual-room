/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { MediaElement } from '../../../common/types/webrtcMediaTypes';

const patchVideoForSafari = (element: HTMLVideoElement) => {
  const runVideo = () => {
    setTimeout(() => {
      if (element.paused) {
        element.pause();
        setTimeout(() => element.play(), 10);
      }
    }, 100);
  };

  element.addEventListener('suspend', runVideo);
  element.addEventListener('pause', runVideo);
};

const createStream = (track: MediaStreamTrack): MediaStream => {
  const stream = new MediaStream();

  stream.addTrack(track);

  return stream;
};

export const createMediaElement = (track: MediaStreamTrack) => {
  if (track) {
    const element = document.createElement(track.kind) as MediaElement;

    element.autoplay = true;
    element.srcObject = createStream(track);

    if (track.kind === 'video') {
      patchVideoForSafari(element as HTMLVideoElement);
    }

    return element;
  }

  return null;
};
