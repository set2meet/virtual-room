/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _get from 'lodash/get';
import { IWebPhoneReduxState } from '../types/IWebPhoneReduxState';
import { TMiddlewareAPI } from '../../../../../ioc/common/ioc-interfaces';

export default (store: TMiddlewareAPI, keyStore: string): IWebPhoneReduxState => {
  return _get(store.getState(), keyStore);
};
