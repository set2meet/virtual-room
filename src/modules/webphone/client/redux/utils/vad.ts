/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const noiseVolume = 14;
const voiceLevelMax = 100;
const talkingTimeStart = 400;
const talkingTimeStop = 1200;
const minAnalyzerTimeout = 400;
const silenceMaxTime = minAnalyzerTimeout * 3; // tslint:disable-line
const maxLogData = 30;

type NOOP = () => {};
type VADCallback = (id: string) => void;
type VADCallbackLevel = (id: string, level: number) => void;

type VADOptions = {
  fftSize: number;
};

type VADActivity = {
  timestamp: number;
  isTalking: boolean;
};

export type VADCallbacks = {
  voiceStop: NOOP | VADCallback;
  voiceStart: NOOP | VADCallback;
  voiceLevel: NOOP | VADCallbackLevel;
  voiceSilence: NOOP | VADCallback;
};

type VADInputOptions = VADCallbacks & {
  userId: string;
  track: MediaStreamTrack;
  isMuted?: boolean;
};

const VADCache: Record<
  string,
  {
    vad: VAD;
    trackId: string;
  }
> = {};

const clearVAD = (vad: VAD) => {
  const cached = VADCache[vad.userId];

  if (cached) {
    delete VADCache[vad.userId];
  }
};

const getTrackId = (track: MediaStreamTrack) => track.id.substr(0, 7) + '...'; // tslint:disable-line

export class VAD {
  private _id: string;
  private _track: MediaStreamTrack;
  private _stream: MediaStream;
  private _source: MediaStreamAudioSourceNode;
  private _analyser: AnalyserNode;

  private _cbVoiceLevel: VADInputOptions['voiceLevel'] = _noop;
  private _cbVoiceStart: VADInputOptions['voiceStart'] = _noop;
  private _cbVoiceStop: VADInputOptions['voiceStop'] = _noop;
  private _cbVoiceSilence: VADInputOptions['voiceSilence'] = _noop;

  private _va: VADActivity = null;
  private _isMuted: boolean = false;
  private _isAnalyzing: boolean = false;
  private _lastDateAnalize: number = -Infinity;

  private _isSilenced: boolean = false;
  private _silencedTimeStart: number;

  private logger = clientEntityProvider.getLogger().webPhone;

  private _options: VADOptions = {
    fftSize: 512,
  };

  private _logData: Array<string | number> = [];

  constructor(options: VADInputOptions) {
    this._id = options.userId;
    this._track = options.track;
    this._isMuted = options.isMuted;
    this._cbVoiceLevel = options.voiceLevel;
    this._cbVoiceStart = options.voiceStart;
    this._cbVoiceStop = options.voiceStop;
    this._cbVoiceSilence = options.voiceSilence;
    this._init();
  }

  public destroy() {
    clearVAD(this);
    this._pushLog();
    this._disconnect();
    this._isAnalyzing = false;
  }

  public get userId() {
    return this._id;
  }

  public set isMuted(value: boolean) {
    value ? this._onMute() : this._onUnmute();
  }

  private _pushLog() {
    this.logger.info(`vad sequence user ${this._id} track ${getTrackId(this._track)}: ${this._logData.join(', ')}`);
    this._logData.length = 0;
  }

  private set _log(value: number | string) {
    if (this._logData.push(value) === maxLogData) {
      this._pushLog();
    }
  }

  private _init() {
    this._stream = new MediaStream([this._track]);

    this._track.addEventListener('ended', () => {
      this.destroy();
    });

    clientEntityProvider.webAudioContext.resume().then(() => {
      this._connect();
      setTimeout(this._monitor, 1);
    });
  }

  private _connect() {
    const audioContext = clientEntityProvider.getWebAudioContext();

    this._source = audioContext.createMediaStreamSource(this._stream);
    this._analyser = audioContext.createAnalyser();
    this._analyser.fftSize = this._options.fftSize;

    this._source.connect(this._analyser);

    this._isAnalyzing = true;
  }

  private _disconnect() {
    this._analyser.disconnect();
    this._source.disconnect();
    this._onMute();
  }

  private _nextTick() {
    setTimeout(() => requestAnimationFrame(this._monitor), 0);
  }

  private _monitor = () => {
    if (!this._isAnalyzing) {
      return;
    }

    if (this._track.readyState === 'ended') {
      this.destroy();

      return;
    }

    this._nextTick();

    const now = Date.now();

    if (now - this._lastDateAnalize < minAnalyzerTimeout) {
      return;
    }

    if (this._isMuted || !this._track.enabled) {
      this._va = null;

      return;
    }

    const array = new Uint8Array(this._analyser.frequencyBinCount);
    const length = array.length;
    let total = 0;

    this._analyser.getByteFrequencyData(array);

    for (let i = 0; i < length; i++) {
      total += array[i];
    }

    if (total === 0) {
      if (!this._isSilenced) {
        this._isSilenced = true;
        this._silencedTimeStart = now;
      } else if (now - this._silencedTimeStart > silenceMaxTime) {
        this._cbVoiceSilence(this._id);
      }
    } else if (this._isSilenced) {
      this._isSilenced = false;
      this._silencedTimeStart = NaN;
    }

    let level = Math.round(total / length);

    level = Math.min(level, voiceLevelMax);

    this._log = level;
    this._analize(level);
    this._lastDateAnalize = now;
  };

  private _analize(level: number) {
    const isTalkingNow = level > noiseVolume;
    const now = Date.now();

    if (isTalkingNow) {
      if (!this._va) {
        this._va = {
          timestamp: now,
          isTalking: false,
        };
      } else if (this._va.isTalking) {
        this._va.timestamp = now;
      } else if (now - this._va.timestamp > talkingTimeStart) {
        this._va.timestamp = now;
        this._va.isTalking = true;

        this._onVoiceStart();
      }
    } else if (this._va && now - this._va.timestamp > talkingTimeStop) {
      if (this._va.isTalking) {
        this._onVoiceStop();
      }

      this._va = null;
    }

    this._onVoiceUpdate(level);
  }

  private _onVoiceUpdate(level: number) {
    if (this._va && this._va.isTalking) {
      this._cbVoiceLevel(this._id, level);
    }
  }

  private _onVoiceStart() {
    this._cbVoiceStart(this._id);
  }

  private _onVoiceStop() {
    this._cbVoiceStop(this._id);
  }

  private _onUnmute = () => {
    this._log = 'unmuted';
    this._isMuted = false;
  };

  private _onMute = () => {
    this._log = 'muted';
    this._isMuted = true;

    if (this._va && this._va.isTalking) {
      this._onVoiceStop();
    }
  };
}

export const initVAD = (options: VADInputOptions): VAD => {
  const { userId, track } = options;

  let cached = VADCache[userId];

  if (cached) {
    if (cached.trackId === track.id) {
      return cached.vad;
    }
    if (cached.vad) {
      cached.vad.destroy();
    }
  }

  cached = VADCache[userId] = {
    trackId: track.id,
    vad: new VAD(options),
  };

  return cached.vad;
};

export const destroyVAD = (userId: string) => {
  const cached = VADCache[userId];

  if (cached) {
    cached.vad.destroy();
    delete VADCache[userId];
  }
};

export const updateVAD = (userId: string, isMuted: boolean) => {
  const cached = VADCache[userId];

  if (cached) {
    cached.vad.isMuted = isMuted;
  }
};

// development only
if ((module as any).hot) {
  (window as any).getVAD = () => {
    console.log('------- VAD list ------');
    Object.keys(VADCache).forEach((userId: string) => {
      const cached = VADCache[userId];

      console.log(`${userId} (${cached.trackId})`);
    });
    console.log('====================\n');
  };
}
