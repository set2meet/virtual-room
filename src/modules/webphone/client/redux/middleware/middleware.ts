/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { actionCreators } from '../../../common/redux/actionCreators';
import { CONNECTION_STATUS, SERVICE_ACTIONS, SERVICE_EVENTS } from '../../../common/types/constants/webrtc';
import { IWebrtcPublisherInfo } from '../../../common/types/IWebrtcParticipantInfo';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  IWebrtcService,
  UMSEvent,
  IScreenSharingSupportInfo,
  TLoggerClientModule,
} from '../../../../../ioc/client/types/interfaces';
import {
  IAppReduxEnumMiddleware,
  IModuleReduxMiddleware,
  IWebrtcTestCallConfig,
  TWebRTCServices,
  TMiddlewareAPI,
} from '../../../../../ioc/common/ioc-interfaces';
import { webrtcConstants, DisconnectReason, WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import { AnyAction, Dispatch } from 'redux';
import Socket = SocketIOClient.Socket;
import { participantStopTalking } from '../utils/vad-analyzer';
import webLocalStorage from '../../services/local-storage';
import debounceWithMergedArguments from '../utils/debounce-with-merge';
import getModuleState from '../utils/getModuleState';
import trackWatcherMiddleware, { trackFullState } from './mediaTrackWatcher';
import mediaDevicesWatcher from './mediaDevicesWatcher';
import { initVadInspector, middlewareEnum as vadMiddlewareEnum } from './vadInspector';
import { IWebPhoneReduxState } from '../types/IWebPhoneReduxState';
import { WebphoneAction } from '../../../common/redux/types/WebphoneAction';
import getCurrentUserAsParticipant from '../selectors/getCurrentUserAsParticipant';
import { LogErrorCode } from '../../../../../ioc/client/ioc-constants';

const WAIT_TIME_OF_NEW_DEVICES = 200; // ms

let testCallConfig: IWebrtcTestCallConfig;
let desktopStreamId: string = null;
let userMediaWasNotInitiated: boolean = true;

const getLogger = (): TLoggerClientModule => {
  return clientEntityProvider.getLogger().webPhone;
};

const getVideoInputStream = (store: TMiddlewareAPI, storeKey: string) => {
  const ums = clientEntityProvider.getUserMediaService();

  return (
    ums
      // get stream from default camera
      .getStreamFromDefaultInputVideo()
      // update webphone camera stream
      .then((streamId: string) => {
        store.dispatch(actionCreators.webphoneSetStreamFromCamera(ums.getStreamById(streamId)));

        return streamId;
      })
      .catch(() => {
        getLogger().error(LogErrorCode.WP_MID, 'cam: catch error by getting stream, abort');
        // remove webphone camera stream
        store.dispatch(actionCreators.webphoneSetStreamFromCamera(null));
        // toggle webphone camera button to muted state
        store.dispatch(actionCreators.toggleVideoStream(true));
      })
  );
};

const getAudioInputStream = (store: TMiddlewareAPI, storeKey: string) => {
  const ums = clientEntityProvider.getUserMediaService();

  return (
    ums
      // get stream from default microphone
      .getStreamFromDefaultInputAudio()
      // update webphone microphone stream
      .then((streamId: string) => {
        store.dispatch(actionCreators.webphoneSetStreamFromMicrophone(ums.getStreamById(streamId)));

        return streamId;
      })
      .catch(() => {
        getLogger().error(LogErrorCode.WP_MID, 'mic: catch error by getting stream, abort');
        // remove webphone microphone stream
        store.dispatch(actionCreators.webphoneSetStreamFromMicrophone(null));
        // toggle webphone microphone button to muted state
        store.dispatch(actionCreators.toggleAudioStream(true));
      })
  );
};

const cloneAudioStream = (store: TMiddlewareAPI, storeKey: string): Promise<string | void> => {
  const { userSetup } = getModuleState(store, storeKey);
  let audioStreamPromise: Promise<string | void>;
  const ums = clientEntityProvider.getUserMediaService();
  const stream = userSetup.microphoneStream;

  if (stream) {
    // clone stream
    const streamId = ums.cloneStream(stream.id);

    getLogger().debug('mic: clone', {
      audioTrackId: ums.getStreamById(streamId).getAudioTracks()[0].id,
    });

    // create 'fake' promise for video stream track
    audioStreamPromise = Promise.resolve(streamId);

    // close previous stream
    ums.closeStreamFromAudioDevice(stream.id);

    // update webphone stream data
    store.dispatch(actionCreators.webphoneSetStreamFromMicrophone(ums.getStreamById(streamId)));
  } else {
    audioStreamPromise = Promise.resolve();
  }

  return audioStreamPromise;
};

const cloneVideoStream = (store: TMiddlewareAPI, storeKey: string): Promise<string | void> => {
  const { userSetup } = getModuleState(store, storeKey);
  let videoStreamPromise: Promise<string | void>;
  const ums = clientEntityProvider.getUserMediaService();
  const stream = userSetup.cameraStream;

  if (stream) {
    // clone stream
    const streamId = ums.cloneStream(stream.id);

    getLogger().debug('cam: clone', {
      videoTrackId: ums.getStreamById(streamId).getVideoTracks()[0].id,
    });

    // create 'fake' promise for video stream track
    videoStreamPromise = Promise.resolve(streamId);

    // close previous stream
    ums.closeStreamFromVideoDevice(stream.id);

    // update webphone stream data
    store.dispatch(actionCreators.webphoneSetStreamFromCamera(ums.getStreamById(streamId)));
  } else {
    videoStreamPromise = new Promise((res) => {
      res(null);
    });
  }

  return videoStreamPromise;
};

const updatePublisherDevicesInfo = (store: TMiddlewareAPI, storeKey: string): void => {
  const { inputVideoDeviceError, inputAudioDeviceError } = store.getState().media.settings;
  const { isConnected, publisher } = getModuleState(store, storeKey);
  const currentUser = store.getState().auth.user;
  const userMediaService = clientEntityProvider.getUserMediaService();
  const appActionCreators = clientEntityProvider.getAppActionCreators();
  const { defaultDevices } = userMediaService;
  const publisherInfo: IWebrtcPublisherInfo = {
    ...publisher,
    hasAudioInputDevice: !!defaultDevices.inputAudio && !inputAudioDeviceError,
    hasVideoInputDevice: !!defaultDevices.inputVideo && !inputVideoDeviceError,
  };
  const participant = {
    ...publisherInfo,
    userId: currentUser?.id,
    userDisplayName: currentUser?.displayName,
    picture: currentUser?.picture,
  };
  // update publisher data in self model
  store.dispatch(actionCreators.publisherUpdate(publisherInfo));

  // send info about self to others participants
  if (isConnected) {
    store.dispatch(appActionCreators.sendRoomAction(actionCreators.updateParticipantInfo(participant)));
  }
};

const closeInputAudioStream = (store: TMiddlewareAPI, storeKey: string): void => {
  const ums = clientEntityProvider.getUserMediaService();
  const state = getModuleState(store, storeKey);
  const stream = state.userSetup.microphoneStream;

  if (!stream) {
    return;
  }

  getLogger().info('mic: close');

  // close stream correctly
  ums.closeStreamFromAudioDevice(stream.id);
  // remove stream from state
  store.dispatch(actionCreators.webphoneSetStreamFromMicrophone(null));
};

const closeInputVideoStream = (store: TMiddlewareAPI, storeKey: string): void => {
  const ums = clientEntityProvider.getUserMediaService();
  const state = getModuleState(store, storeKey);
  const stream = state.userSetup.cameraStream;

  if (!stream) {
    return;
  }

  getLogger().info('cam: close');

  // close stream correctly
  ums.closeStreamFromVideoDevice(stream.id);
  // remove stream from state
  store.dispatch(actionCreators.webphoneSetStreamFromCamera(null));
};

type TUpdateWebrtcStreams = (
  store: TMiddlewareAPI,
  storeKey: string,
  webrtcServices: TWebRTCServices,
  updateAudioStream: boolean,
  updateVideoStream: boolean
) => void;

// tslint:disable cyclomatic-complexity
const updateWebrtcStreams: TUpdateWebrtcStreams = debounceWithMergedArguments(
  (
    store: TMiddlewareAPI,
    storeKey: string,
    webrtcServices: TWebRTCServices,
    updateAudioStream: boolean = false,
    updateVideoStream: boolean = false
  ): void => {
    const ums = clientEntityProvider.getUserMediaService();
    let audioStreamPromise: Promise<string | void>;
    let videoStreamPromise: Promise<string | void>;

    if (
      !ums ||
      // device was unplugged! - nothing to do right now
      (updateVideoStream && !ums.defaultInputVideo) ||
      (updateAudioStream && !ums.defaultInputAudio)
    ) {
      return;
    }

    const { publisher, userSetup, isConnected, webrtcServiceName } = getModuleState(store, storeKey);

    // if we are on the user setup page
    if (!userSetup.isComplete && userSetup.currentTab) {
      if (updateAudioStream) {
        getAudioInputStream(store, storeKey);
      }
      if (updateVideoStream) {
        getVideoInputStream(store, storeKey);
      }

      return;
    }

    // if no active session - exit
    if (!isConnected) {
      // user has unmuted camera - restore it on base screen
      if (!publisher.isVideoStreamMuted) {
        getVideoInputStream(store, storeKey);
      }

      return;
    }

    // if audio input device was updated & if mic not muted right now
    if (updateAudioStream) {
      audioStreamPromise = getAudioInputStream(store, storeKey);
    }

    // if video input device was updated & if camera not muted right now
    if (updateVideoStream) {
      videoStreamPromise = getVideoInputStream(store, storeKey);
    }

    // if video stream existed - clone it and close old stream
    if (updateAudioStream && !updateVideoStream) {
      videoStreamPromise = cloneVideoStream(store, storeKey);
    }

    // if audio stream existed - clone it and close old stream
    if (updateVideoStream && !updateAudioStream) {
      audioStreamPromise = cloneAudioStream(store, storeKey);
    }
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    Promise.all([audioStreamPromise, videoStreamPromise]).then((streamIds) => {
      const aTrack = ums.getAudioStreamTrack(streamIds[0] || null);
      const vTrack = ums.getVideoStreamTrack(streamIds[1] || null);

      webrtcService.updatePublishStreams(aTrack || null, vTrack || null);
    });
  },
  WAIT_TIME_OF_NEW_DEVICES
);
// tslint:enable

const defaultInputAudioDeviceChanged = (
  store: TMiddlewareAPI,
  keyStore: string,
  webrtcServices: TWebRTCServices
): void => {
  // remove current stream from microphone
  closeInputAudioStream(store, keyStore);

  // update publisher state
  updatePublisherDevicesInfo(store, keyStore);

  // total update streams
  updateWebrtcStreams(store, keyStore, webrtcServices, true, false);
};

const defaultInputVideoDeviceChanged = (
  store: TMiddlewareAPI,
  keyStore: string,
  webrtcServices: TWebRTCServices
): void => {
  // remove current stream from camera
  closeInputVideoStream(store, keyStore);

  // update publisher state
  updatePublisherDevicesInfo(store, keyStore);

  // total update streams
  updateWebrtcStreams(store, keyStore, webrtcServices, false, true);
};

const initUserMediaServiceEvents = (store: TMiddlewareAPI, keyStore: string, webrtcServices: TWebRTCServices): void => {
  const ums = clientEntityProvider.getUserMediaService();

  // the function must be executed only once
  userMediaWasNotInitiated = false;

  // subscribe on change default devices
  updatePublisherDevicesInfo(store, keyStore);

  ums.subscribe('mediaDevicesChange', (evt: UMSEvent.IMediaDevicesChange) => {
    if (evt.default) {
      if (evt.default.inputAudio) {
        defaultInputAudioDeviceChanged(store, keyStore, webrtcServices);
      }
      if (evt.default.inputVideo) {
        defaultInputVideoDeviceChanged(store, keyStore, webrtcServices);
      }
    }
    if (evt.unplugged) {
      // remove links to streams from default devices
      if (evt.unplugged.inputAudio) {
        closeInputAudioStream(store, keyStore);
      }
      if (evt.unplugged.inputVideo) {
        closeInputVideoStream(store, keyStore);
      }
    }
  });

  ums.subscribe('mediaAccessError', () => {
    setTimeout(() => updatePublisherDevicesInfo(store, keyStore), 1);
  });

  ums.subscribe('mediaAccessAllowed', () => {
    setTimeout(() => updatePublisherDevicesInfo(store, keyStore), 1);
  });

  // subscribe on change default audio/video stream
  ums.subscribe('defaultInputAudioDeviceChanged', () => {
    setTimeout(() => defaultInputAudioDeviceChanged(store, keyStore, webrtcServices), 1);
  });

  ums.subscribe('defaultInputVideoDeviceChanged', () => {
    setTimeout(() => defaultInputVideoDeviceChanged(store, keyStore, webrtcServices), 1);
  });

  // update screen sharing support info
  const sss = ums.screenSharingService;

  store.dispatch(
    actionCreators.screenSharingUpdateCapabilities({
      supported: sss.isSupported,
      isBlocked: sss.isBlocked,
    })
  );

  sss.addEventListener('screenSharingStatusUpdate', (screenSharingCapability: IScreenSharingSupportInfo) => {
    store.dispatch(actionCreators.screenSharingUpdateCapabilities(screenSharingCapability));
  });
};

const syncUserStateWithLocalStorage = (store: TMiddlewareAPI, keyStore: string): void => {
  const state: IWebPhoneReduxState = getModuleState(store, keyStore);
  const publisher = { ...state.publisher };
  const { enableMicOnSessionStart } = clientEntityProvider.getConfig().clientConfig;

  // get local storage webphone state
  const lsIsMicMuted = webLocalStorage.getMicState();

  // sync microphone muted flag state
  if (lsIsMicMuted === undefined) {
    // if user enter the room - enable mic by config value
    if (enableMicOnSessionStart) {
      store.dispatch(actionCreators.toggleAudioStream(false));
    } else {
      // in other cases just update local storage saved value
      webLocalStorage.setMicState(publisher.isAudioStreamMuted);
    }
  } else {
    store.dispatch(actionCreators.toggleAudioStream(lsIsMicMuted));
  }
};

const turnOnMicrophoneForHoster = (store: TMiddlewareAPI): void => {
  const { enableMicOnSessionStart } = clientEntityProvider.getConfig().clientConfig;

  if (enableMicOnSessionStart) {
    store.dispatch(actionCreators.toggleAudioStream(false));
  }
};

const subscribeOnWebrtcServiceEvents = (store: TMiddlewareAPI, webrtc: IWebrtcService): void => {
  webrtc.subscribe(SERVICE_EVENTS.SESSION_CONNECT_ERROR, () => {
    // todo: correct action to show error or other one
    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.NONE));
  });

  webrtc.subscribe(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS, () => {
    store.dispatch(actionCreators.sessionConnectedSusscess());
  });

  webrtc.subscribe(SERVICE_EVENTS.SESSION_DISCONNECTED, () => {
    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.NONE));
  });

  webrtc.subscribe(SERVICE_EVENTS.SESSION_CONNECTION_LOST, () => {
    getLogger().info('Connection lost (message from webrtc)');
    store.dispatch(actionCreators.webphoneDisconnect(DisconnectReason.CONNECTION_LOST));
  });

  webrtc.subscribe(SERVICE_EVENTS.SESSION_CONNECTION_RESTORED, () => {
    store.dispatch(actionCreators.webphoneConnect());
  });

  webrtc.subscribe(SERVICE_EVENTS.PARTICIPANT_START_TALKING, (userId: string) => {
    store.dispatch(actionCreators.participantToggleTalking(userId, true));
  });

  webrtc.subscribe(SERVICE_EVENTS.PARTICIPANT_END_TALKING, (userId: string) => {
    store.dispatch(actionCreators.participantToggleTalking(userId, false));
  });

  webrtc.subscribe(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, ({ userId, tracks }) => {
    store.dispatch(actionCreators.updateParticipantMediaStreamTrack(userId, tracks));
  });
};

const unsubscribeOnWebrtcServiceEvents = (webrtc?: IWebrtcService): void => {
  if (webrtc) {
    webrtc.unsubscribe();
  }
};

const getWebrtcParams = (
  store: TMiddlewareAPI,
  storeKey: string,
  webrtcServices: TWebRTCServices,
  temporary: boolean = false
) => {
  const gState = store.getState();
  const appActionCreators = clientEntityProvider.getAppActionCreators();
  const state: IWebPhoneReduxState = getModuleState(store, storeKey);
  const webrtc: IWebrtcService = clientEntityProvider
    .getUtils()
    .getWebrtcServiceByName(state.webrtcServiceName, webrtcServices);
  // TODO remove hardlink ot user and session
  // may be webrtcService.sessionId
  const sessionId = temporary ? null : gState.room.webrtcService.sessionId;

  if (!(webrtc && (sessionId || temporary))) {
    return;
  }

  store.dispatch(
    appActionCreators.sendRoomAction({
      type: SERVICE_ACTIONS.GET_PARAMS,
      serviceName: webrtc.name,
      user: gState.auth.user,
      sessionId,
      temporary,
    })
  );
};

const connectIfActiveSessionExisted = (
  store: TMiddlewareAPI,
  storeKey: string,
  webrtcServices: TWebRTCServices,
  logError: boolean = false
) => {
  // get new access params for current webrtc service
  const roomState = store.getState().room;
  const sId = roomState.webrtcService.sessionId;
  const rId = roomState.state.id;

  if (sId && rId) {
    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.CONNECTING));
    getWebrtcParams(store, storeKey, webrtcServices);
  } else if (logError) {
    const errMsg = `Error: can not connect to session becouse not found [${sId ? '' : ` ${sId}`}${
      rId ? '' : ` ${rId}`
    }]`;

    console.error(new Error(errMsg));
    getLogger().error(LogErrorCode.WP_MID, errMsg);
  }
};

const addVideoFromCamera = (store: TMiddlewareAPI, storeKey: string): void => {
  const { userSetup } = getModuleState(store, storeKey);
  const userId = store.getState().auth.user?.id;
  const streamId = userSetup.cameraStream && userSetup.cameraStream.id;
  const ums = clientEntityProvider.getUserMediaService();
  store.dispatch(
    actionCreators.updateParticipantMediaStreamTrack(userId, [
      { kind: 'cam', track: ums.getVideoStreamTrack(streamId) },
    ])
  );
};

const delVideoFromCamera = (store: TMiddlewareAPI, userId: string): void => {
  store.dispatch(actionCreators.updateParticipantMediaStreamTrack(userId, [{ kind: 'cam', track: null }]));
};

const updateWebRTCStreamData = (state: IWebPhoneReduxState, webrtcService: IWebrtcService, userId: string) => {
  const { userSetup } = state;
  const aStreamId = userSetup.microphoneStream && userSetup.microphoneStream.id;
  const vStreamId = userSetup.cameraStream && userSetup.cameraStream.id;
  const ums = clientEntityProvider.getUserMediaService();

  getLogger().debug(`update webrtc streams:
    audio track ${trackFullState(ums.getAudioStreamTrack(aStreamId))},
    video track ${trackFullState(ums.getVideoStreamTrack(vStreamId))}
  `);

  webrtcService.publisherUpdateStream(userId, ums.getAudioStreamTrack(aStreamId), ums.getVideoStreamTrack(vStreamId));
};

const toggleVideoOffline = (store: TMiddlewareAPI, storeKey: string) => {
  const state: IWebPhoneReduxState = getModuleState(store, storeKey);
  const {
    userSetup,
    publisher: { isVideoStreamMuted },
  } = state;
  const stream = userSetup.cameraStream;
  const userId = store.getState().auth.user?.id;

  if (isVideoStreamMuted) {
    delVideoFromCamera(store, userId);
    closeInputVideoStream(store, storeKey);
  } else {
    if (stream) {
      addVideoFromCamera(store, storeKey);
    } else {
      getVideoInputStream(store, storeKey).then(() => {
        addVideoFromCamera(store, storeKey);
      });
    }
  }
};

const toggleAudioOffline = (store: TMiddlewareAPI, storeKey: string) => {
  const state = getModuleState(store, storeKey);
  const { publisher, userSetup } = state;
  const stream = userSetup.microphoneStream;
  const { isAudioStreamMuted } = publisher;

  if (isAudioStreamMuted) {
    closeInputAudioStream(store, storeKey);
  } else {
    if (!stream) {
      getAudioInputStream(store, storeKey);
    }
  }
};

const startPublishStream = (store: TMiddlewareAPI, storeKey: string, webrtcServices: TWebRTCServices): void => {
  const ums = clientEntityProvider.getUserMediaService();
  const state = getModuleState(store, storeKey);
  const { webrtcServiceName, userSetup } = state;
  const { cameraStream, microphoneStream } = userSetup;
  const streamPromises = [];

  getLogger().info('try publish streams', {
    cameraStream: !!cameraStream,
    microphoneStream: !!microphoneStream,
  });

  const publish = () => {
    const newState = getModuleState(store, storeKey);
    const {
      publisher: { isSharingScreen },
    } = newState;
    const userId = store.getState().auth.user?.id;
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    updateWebRTCStreamData(newState, webrtcService, userId);

    // restore desktop stream, if existed
    if (isSharingScreen && desktopStreamId) {
      const track = ums.getVideoStreamTrack(desktopStreamId);

      webrtcService.publishDesktop(userId, track);

      store.dispatch(
        actionCreators.updateParticipantMediaStreamTrack(userId, [
          {
            track,
            kind: 'desktop',
          },
        ])
      );
    }
  };

  if (!cameraStream && ums.defaultInputVideo) {
    streamPromises.push(getVideoInputStream(store, storeKey));
  }

  if (!microphoneStream && ums.defaultInputAudio) {
    streamPromises.push(getAudioInputStream(store, storeKey));
  }

  Promise.all(streamPromises).then(publish);
};

const toggleOffScreenSharing = (store: TMiddlewareAPI, storeKey: string): void => {
  const { isSharingScreen } = getModuleState(store, storeKey).publisher;

  if (isSharingScreen) {
    store.dispatch(actionCreators.toggleScreenSharing());
  }
};

const toggleOffCameraAndMicrophone = (store: TMiddlewareAPI, storeKey: string): void => {
  closeInputAudioStream(store, storeKey);
  closeInputVideoStream(store, storeKey);

  store.dispatch(actionCreators.toggleAudioStream(true));
  store.dispatch(actionCreators.toggleVideoStream(true));
};

const toggleOffStreams = (store: TMiddlewareAPI, storeKey: string): void => {
  toggleOffScreenSharing(store, storeKey);
  toggleOffCameraAndMicrophone(store, storeKey);
};

const cloneDefaultStreams = (store: TMiddlewareAPI, storeKey: string) => {
  const { userSetup } = getModuleState(store, storeKey);
  const aStream = userSetup.microphoneStream;
  const vStream = userSetup.cameraStream;
  const ums = clientEntityProvider.getUserMediaService();

  // clone audio track
  if (aStream) {
    getLogger().info('mic: clone default');
    store.dispatch(actionCreators.webphoneSetStreamFromMicrophone(ums.getStreamById(ums.cloneStream(aStream.id))));
  }

  // clone video track
  if (vStream) {
    getLogger().info('cam: clone default');
    store.dispatch(actionCreators.webphoneSetStreamFromCamera(ums.getStreamById(ums.cloneStream(vStream.id))));
  }

  // clone desktop stream
  desktopStreamId = ums.cloneStream(desktopStreamId);
};

const syncUserSetupParamsWithLocalStorage = (store: TMiddlewareAPI): void => {
  const dontShowAgain = webLocalStorage.getUserSetupDontShowAgainState();

  store.dispatch(actionCreators.webphoneUserSetupSetDontShowAgain(dontShowAgain));
};

const attachEventsToWebrtcServices = (store: TMiddlewareAPI, webRTCServices: TWebRTCServices) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  Object.values(webRTCServices).forEach((service) => {
    service.on(webrtcConstants.SERVICE_EVENTS.USER_STREAM_ADDED, (userId: string, stream: MediaStream) => {
      store.dispatch(appActionCreators.userStreamAdded(stream));
    });

    service.on(webrtcConstants.SERVICE_EVENTS.USER_STREAM_REMOVED, (stream: MediaStream) => {
      store.dispatch(appActionCreators.userStreamRemoved(stream));
    });
  });
};

const reconnectAfterLostConnection = (store: TMiddlewareAPI, storeKey: string, webrtcServices: TWebRTCServices) => {
  const { webrtcServiceName, accessParams } = getModuleState(store, storeKey);

  if (!accessParams) {
    // unexpected error
    // lets try get new webrtc access params
    // getWebrtcParams(store, storeKey);
    getLogger().info('Error: not found webrtc access params');
    connectIfActiveSessionExisted(store, storeKey, webrtcServices, true);

    return;
  }

  const { apiKey, sessionId, token } = accessParams;
  const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

  // call reconnect in webrtc service
  webrtcService.reconnect(apiKey, sessionId, token);
};

const testCallStop = (store: TMiddlewareAPI, score: number): void => {
  // if testCallConfig variable exists - test call still in progress
  //
  // sometimes user click 'Cancel' button in the middle of
  //   generated opentok error and called 'testCallStop' event by this error
  //   so, 'testCallStop' can be called twice at "the same time"
  // to avoid this case - check the existance of testCallConfig variable
  //   as proof that test call has not yet been completed
  // if no testCallConfig - no test call, means there is nothing to stop
  if (!testCallConfig) {
    return;
  }

  // clear test call config
  testCallConfig = null;

  store.dispatch(actionCreators.webphoneSetConnectionQuality(score));
  store.dispatch(actionCreators.webphoneUserSetupTestCallToggleProgress(false));
};

function getWebrtcServiceByName(serviceName: WebrtcServiceType, services: TWebRTCServices): IWebrtcService {
  return clientEntityProvider.getUtils().getWebrtcServiceByName(serviceName, services);
}

const middlewareEnum: IAppReduxEnumMiddleware<TWebRTCServices> = {
  [WebphoneAction.START_MEETING](store, next, action, socket, storeKey) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { webrtcServiceName } = state;
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    if (webrtcServiceName) {
      store.dispatch(appActionCreators.sendRoomAction(actionCreators.sessionStart(webrtcServiceName)));
    }

    turnOnMicrophoneForHoster(store);

    // continue redux actions
    next(action);
  },
  [WebphoneAction.END_MEETING](store, next, action, socket, storeKey) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { webrtcServiceName } = state;
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    if (webrtcServiceName) {
      store.dispatch(
        appActionCreators.sendRoomAction(actionCreators.sessionEnd(webrtcServiceName, state.accessParams.sessionId))
      );
    }

    // continue redux actions
    next(action);
  },
  [WebphoneAction.WEBPHONE_CONNECT](store, next, action, socket, storeKey, webrtcServices) {
    const gState = store.getState();

    // if connection restored but we were thrown out of the room
    if (!gState.room.state.id) {
      return;
    }

    const state = getModuleState(store, storeKey);
    const participant = getCurrentUserAsParticipant(store.getState());
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    if (state.connectionStatus === CONNECTION_STATUS.CONNECTING) {
      // There are may be call from app if network was broken totally
      return;
    }

    next(action);

    if (state.connectionStatus === CONNECTION_STATUS.NONE) {
      connectIfActiveSessionExisted(store, storeKey, webrtcServices);
      syncUserStateWithLocalStorage(store, storeKey);
      store.dispatch(actionCreators.updateParticipantInfo({ ...participant }));
    } else if (state.connectionStatus === CONNECTION_STATUS.CONNECTION_LOST) {
      store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.CONNECTING));
      reconnectAfterLostConnection(store, storeKey, webrtcServices);
      store.dispatch(appActionCreators.sendRoomAction(actionCreators.updateParticipantInfo({ ...participant })));
    } else if (state.connectionStatus === CONNECTION_STATUS.ESTABLISHED) {
      // USED IN OPENTOK: There is could be possible bug, when connection is not lost,
      // but at this moment it were near it. So user leave from webphone, but doesnt comes back
      store.dispatch(appActionCreators.sendRoomAction(actionCreators.updateParticipantInfo({ ...participant })));
    }
  },
  [WebphoneAction.WEBPHONE_DISCONNECT](store, next, action, socket, storeKey) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);

    getLogger().info('Disconnect', {
      actionReason: action.reason,
      connectionStatus: state.connectionStatus as string,
    });

    // webrtc connection lost
    if (action.reason === DisconnectReason.CONNECTION_LOST) {
      // it may be a duplicate signal in the event of a network outage
      if (state.connectionStatus !== CONNECTION_STATUS.CONNECTION_LOST) {
        // clone streams (on timeout webrtc can close it unilaterally
        cloneDefaultStreams(store, storeKey);
        // update connection status to 'connection lost'
        store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.CONNECTION_LOST));
      }

      return;
    }

    if (state.isConnected) {
      const { sessionId } = store.getState().room.webrtcService;

      store.dispatch(actionCreators.sessionDisconnect(sessionId));
    } else {
      toggleOffStreams(store, storeKey);
    }

    next(action);
  },
  [WebphoneAction.PARTICIPANT_UPDATE_INFO](store, next, action) {
    next(action);

    const { participant } = action;

    if (participant.isAudioStreamMuted) {
      participantStopTalking(participant.userId);
    }
  },
  [WebphoneAction.WEBRTC_SERVICE_WAS_CHANGED](store, next, action, socket, storeKey) {
    next(action);
    toggleOffStreams(store, storeKey);
  },
  [WebphoneAction.CHANGE_WEBRTC_SERVICE](store, next, action, socket, storeKey, webrtcServices) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const currWebrtc: IWebrtcService = clientEntityProvider
      .getUtils()
      .getWebrtcServiceByName(state.webrtcServiceName, webrtcServices);
    const nextWebrtc: IWebrtcService = clientEntityProvider
      .getUtils()
      .getWebrtcServiceByName(action.serviceName, webrtcServices);
    const appActionCreators = clientEntityProvider.getAppActionCreators();
    const selectors = clientEntityProvider.getSelectors();

    if (!nextWebrtc || (currWebrtc && currWebrtc.name === nextWebrtc.name)) {
      return;
    }

    // save link to the new service
    next(action);
    // unsubscribe from events in the current service
    unsubscribeOnWebrtcServiceEvents(currWebrtc);
    // subscribe on events in the new webrtc service
    subscribeOnWebrtcServiceEvents(store, nextWebrtc);

    const isOwner = selectors.appSelectors.isOwner(store.getState());

    if (isOwner) {
      // send event that webrtc service was changed
      store.dispatch(
        appActionCreators.sendRoomAction({
          type: SERVICE_ACTIONS.CHANGE_SERVICE,
          serviceName: nextWebrtc.name,
        })
      );
    }

    store.dispatch(actionCreators.webrtcServiceWasChanged());
  },
  [SERVICE_ACTIONS.SESSION_STARTED](store, next, action, socket, storeKey, webrtcServices) {
    next(action);
    // get new access params for current webrtc service
    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.CONNECTING));
    getWebrtcParams(store, storeKey, webrtcServices);
  },
  [SERVICE_ACTIONS.SESSION_ENDED](store, next, action, socket, storeKey) {
    getLogger().info('Session disconnected');
    next(action);
    store.dispatch(actionCreators.sessionDisconnect(action.sessionId));
  },
  [SERVICE_ACTIONS.SET_PARAMS](store, next, action, socket, storeKey, webrtcServices) {
    const { params } = action;

    // check the sessionId correct by existanse of token
    if (!params.token) {
      const state = store.getState();
      const selfState = getModuleState(store, storeKey);
      const serviceName = selfState.webrtcServiceName;
      const isOwner = state.room.state.ownerId === state.auth.user?.id;

      if (isOwner) {
        // stop session with incorrect sessionId
        store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.DISCONNECTING));
        store.dispatch(
          clientEntityProvider
            .getAppActionCreators()
            .sendRoomAction(actionCreators.sessionEnd(serviceName, params.sessionId))
        );
      } else {
        // stop connection
        store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.NONE));
      }
    }

    // TODO combine this to one action (SET_PARAMS -> SESSION_CONNECT?)
    if (action.temporary) {
      const { webrtcServiceName } = getModuleState(store, storeKey);
      const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

      webrtcService
        .testCallStart(action.params, testCallConfig, () => {
          // any error during the test call
          testCallStop(store, 0);
        })
        .then(() => {
          store.dispatch(actionCreators.webphoneUserSetupTestCallToggleProgress(true));
        })
        .catch(() => {
          // any error on starting the call
          testCallStop(store, 0);
        });

      return;
    }
    store.dispatch(actionCreators.webrtcServiceSetAccessParams(action.params));
    store.dispatch(actionCreators.sessionConnect());
  },
  [WebphoneAction.WEBPHONE_MODULE_INIT](store, next, action, socket, storeKey, webrtcServices) {
    syncUserSetupParamsWithLocalStorage(store);
    attachEventsToWebrtcServices(store, webrtcServices);

    next(action);
  },
  [WebphoneAction.SESSION_CONNECT](store, next, action, socket, storeKey, webrtcServices) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { accessParams, webrtcServiceName } = state;
    const { apiKey, sessionId, token } = accessParams;

    // if (state.connectionStatus !== CONNECTION_STATUS.NONE) {
    // todo: find and fix it!
    window.console.log('[WP Connect] Connect to the session', sessionId);
    // }
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    webrtcService.connect(apiKey, sessionId, token);

    // store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.CONNECTING));
  },
  [WebphoneAction.SESSION_DISCONNECT](store, next, action, socket, storeKey, webrtcServices) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { webrtcServiceName } = state;
    const { sessionId } = action;
    const userId = store.getState().auth.user?.id;

    getLogger().info('Session disconnect');

    store.dispatch(actionCreators.baseScreenDelSelectedUser());
    store.dispatch(actionCreators.sessionClosed(sessionId));
    closeInputAudioStream(store, storeKey);
    closeInputVideoStream(store, storeKey);
    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.DISCONNECTING));
    store.dispatch(
      actionCreators.updateParticipantMediaStreamTrack(userId, [
        { kind: 'mic', track: null },
        { kind: 'cam', track: null },
        { kind: 'desktop', track: null },
      ])
    );
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    webrtcService.disconnect();

    next(action);
  },
  [WebphoneAction.SESSION_CONNECTED_SUCCESS](store, next, action, socket, storeKey, webrtcServices) {
    const participant = getCurrentUserAsParticipant(store.getState());
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    getLogger().info('Session connected');

    startPublishStream(store, storeKey, webrtcServices);

    store.dispatch(actionCreators.connectionUpdateStatus(CONNECTION_STATUS.ESTABLISHED));
    store.dispatch(appActionCreators.sendRoomAction(actionCreators.updateParticipantInfo(participant)));
  },
  [WebphoneAction.CONNECTION_UPDATE_STATUS](store, next, action, socket, storeKey) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);

    if (state.connectionStatus === CONNECTION_STATUS.DISCONNECTING && action.status === CONNECTION_STATUS.NONE) {
      // on session close: stop all streams and mute devices
      toggleOffStreams(store, storeKey);
    }

    if (state.connectionStatus === CONNECTION_STATUS.CONNECTION_LOST && action.status === CONNECTION_STATUS.NONE) {
      // nothing todo, session is closed by timeout
      return;
    }

    next(action);
  },
  [WebphoneAction.BASE_SCREEN_SET_SELECTED_USER](store, next, action, socket, storeKey) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { selectedUser } = state.baseScreen;
    const { userId } = action;

    // if try select the same user as selected: unselect one
    if (selectedUser === userId) {
      store.dispatch(actionCreators.baseScreenDelSelectedUser());
    }
    // otherwise: continue action as is
    else {
      next(action);
    }
  },
  [WebphoneAction.TOGGLE_AUDIO_STREAM](store, next, action, socket, storeKey, webrtcServices) {
    const isMuted = action.isMuted;
    const { publisher, userSetup, webrtcServiceName, connectionStatus } = getModuleState(store, storeKey);
    const participant = getCurrentUserAsParticipant(store.getState());
    let isAudioStreamMuted: boolean = isMuted === undefined ? !publisher.isAudioStreamMuted : isMuted;

    if (!publisher.hasAudioInputDevice && !isAudioStreamMuted) {
      isAudioStreamMuted = true;
    }

    if (isAudioStreamMuted === publisher.isAudioStreamMuted) {
      return;
    }
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    if (webrtcService) {
      webrtcService.toggleAudioStream(isAudioStreamMuted);
    }

    webLocalStorage.setMicState(isAudioStreamMuted);

    next({
      type: action.type,
      value: isAudioStreamMuted,
    });

    const appActionCreators = clientEntityProvider.getAppActionCreators();

    store.dispatch(
      appActionCreators.sendRoomAction(
        actionCreators.updateParticipantInfo({
          ...participant,
          isAudioStreamMuted,
        })
      )
    );

    if (connectionStatus === CONNECTION_STATUS.NONE) {
      toggleAudioOffline(store, storeKey);
    } else if (!isAudioStreamMuted && !userSetup.microphoneStream) {
      updateWebrtcStreams(store, storeKey, webrtcServices, true, false);
    }
  },
  [WebphoneAction.TOGGLE_VIDEO_STREAM](store, next, action, socket, storeKey, webrtcServices) {
    const isMuted = action.isMuted;
    const { publisher, userSetup, webrtcServiceName, connectionStatus } = getModuleState(store, storeKey);
    const participant = getCurrentUserAsParticipant(store.getState());
    let isVideoStreamMuted: boolean = isMuted === undefined ? !publisher.isVideoStreamMuted : isMuted;

    if (!publisher.hasVideoInputDevice && !isVideoStreamMuted) {
      isVideoStreamMuted = true;
    }

    if (isVideoStreamMuted === publisher.isVideoStreamMuted) {
      return;
    }
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    if (webrtcService) {
      webrtcService.toggleVideoStream(isVideoStreamMuted);
    }

    next({
      type: action.type,
      value: isVideoStreamMuted,
    });

    const appActionCreators = clientEntityProvider.getAppActionCreators();

    store.dispatch(
      appActionCreators.sendRoomAction(
        actionCreators.updateParticipantInfo({
          ...participant,
          isVideoStreamMuted,
        })
      )
    );

    if (connectionStatus === CONNECTION_STATUS.NONE) {
      toggleVideoOffline(store, storeKey);
    } else if (!isVideoStreamMuted && !userSetup.cameraStream) {
      updateWebrtcStreams(store, storeKey, webrtcServices, false, true);
    }
  },
  [WebphoneAction.TOGGLE_SCREEN_SHARING](store, next, action, socket, storeKey, webrtcServices) {
    const state: IWebPhoneReduxState = getModuleState(store, storeKey);
    const { publisher, webrtcServiceName } = state;
    const { isSharingScreen } = publisher;
    const userId = store.getState().auth.user?.id;
    const ums = clientEntityProvider.getUserMediaService();

    store.dispatch(actionCreators.screenSharingSetAvaliability(false));

    const offSreenSharing = () => {
      if (desktopStreamId) {
        toggleOffScreenSharing(store, storeKey);
      }
    };
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

    if (isSharingScreen) {
      if (desktopStreamId) {
        const streamId = desktopStreamId;

        desktopStreamId = null;
        ums.closeStreamFromVideoDevice(streamId);
      }

      next(action);
      webrtcService.unpublishDesktop();

      store.dispatch(actionCreators.updateParticipantMediaStreamTrack(userId, [{ track: null, kind: 'desktop' }]));
      store.dispatch(actionCreators.screenSharingSetAvaliability(true));
    } else {
      // disable toggle functionality for second click

      ums
        .getStreamFromDesktop()
        .then((streamId: string) => {
          desktopStreamId = streamId;

          const track = ums.getVideoStreamTrack(streamId);

          webrtcService.publishDesktop(userId, track);
          track.addEventListener('ended', offSreenSharing);
          store.dispatch(actionCreators.updateParticipantMediaStreamTrack(userId, [{ track, kind: 'desktop' }]));

          next(action);
        })
        .then(() => {
          // enable toggle functionality
          store.dispatch(actionCreators.screenSharingSetAvaliability(true));
        });
    }
  },
  [WebphoneAction.WEBPHONE_REQUEST_STREAM_FROM_CAMERA](store, next, action, socket, storeKey) {
    const state = getModuleState(store, storeKey);

    if (!state.userSetup.cameraStream) {
      getVideoInputStream(store, storeKey);
    }

    if (!state.userSetup.cameraStream) {
      getLogger().info('cam: request stream start');
    } else {
      getLogger().info('cam: request stream missed (because existed)');
    }

    next(action);
  },
  [WebphoneAction.WEBPHONE_REQUEST_STREAM_FROM_MICROPHONE](store, next, action, socket, storeKey) {
    const state = getModuleState(store, storeKey);

    if (!state.userSetup.microphoneStream) {
      getAudioInputStream(store, storeKey);
    }

    if (!state.userSetup.microphoneStream) {
      getLogger().info('mic: request stream start');
    } else {
      getLogger().info('mic: request stream missed (because existed)');
    }

    next(action);
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_SET_DONT_SHOW_AGAIN](store, next, action, socket, storeKey) {
    webLocalStorage.setUserSetupDontShowAgainState(action.value);
    next(action);
  },

  [WebphoneAction.WEBPHONE_TEST_CALL_START](store, next, action, socket, storeKey, webrtcServices) {
    next(action);
    testCallConfig = action.config;
    getWebrtcParams(store, storeKey, webrtcServices, true);
  },
  [WebphoneAction.WEBPHONE_TEST_CALL_STOP](store, next, action, socket, storeKey, webrtcServices) {
    next(action);

    // stop test call and get connection quality score
    const { webrtcServiceName } = getModuleState(store, storeKey);
    const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);
    const score = webrtcService.testCallStop();

    testCallStop(store, action.byUser ? NaN : score);
  },
  [WebphoneAction.WEBPHONE_USER_LEFT_CALL](store, next, action, socket, storeKey, webrtcServices) {
    const { webrtcServiceName } = getModuleState(store, storeKey);
    const { userId } = action;
    const publisherUserId = store.getState().auth.user?.id;

    if (action.userId === publisherUserId) {
      const webrtcService = getWebrtcServiceByName(webrtcServiceName, webrtcServices);

      webrtcService.disconnect();
    }

    // resetting stream and toggle off
    store.dispatch(actionCreators.updateParticipantMediaStreamTrack(userId, [{ track: null, kind: 'desktop' }]));
    store.dispatch(actionCreators.screenSharingSetAvaliability(true));

    next(action);
  },
};

const clientMiddleware: IModuleReduxMiddleware<TWebRTCServices> = (storeKey: string) => (
  socket: Socket,
  webrtcServices: TWebRTCServices
) => (store: TMiddlewareAPI) => (next: Dispatch<AnyAction>) => (action: AnyAction) => {
  if (clientEntityProvider.getUtils().isReduxDynamicModulesAction(action)) {
    next(action);
    return;
  }
  let nextExecuted: boolean = false;
  let middlewareExecuted: boolean = false;

  // original "next" can be executed only once!
  const next2: any = (newAction: AnyAction) => {
    if (!nextExecuted) {
      next(newAction);
      nextExecuted = true;
    }
  };

  initVadInspector(store);

  if (socket) {
    mediaDevicesWatcher(socket);
  }

  if (userMediaWasNotInitiated) {
    initUserMediaServiceEvents(store, storeKey, webrtcServices);
  }

  if (middlewareEnum[action.type]) {
    middlewareExecuted = true;
    middlewareEnum[action.type](store, next2, action, socket, storeKey, webrtcServices);
  }

  if (vadMiddlewareEnum[action.type]) {
    middlewareExecuted = true;
    vadMiddlewareEnum[action.type](store, next2, action, socket, storeKey);
  }

  if (trackWatcherMiddleware[action.type]) {
    middlewareExecuted = true;
    trackWatcherMiddleware[action.type](store, next2, action, socket, storeKey);
  }

  if (!middlewareExecuted) {
    next(action);
  }
};

export default clientMiddleware;
