/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;
import { IScreenSharingSupportInfo, TLoggerClientModule } from '../../../../../ioc/client/types/interfaces';
import { LogErrorCode } from '../../../../../ioc/client/ioc-constants';

const DEVICE_ID_LENGTH = 7;
let notInitialized: boolean = true;

const getLogger = (): TLoggerClientModule => {
  return clientEntityProvider.getLogger().webPhone;
};

const logDevice = (type: 'video' | 'audio', title: string, device: MediaDeviceInfo) => {
  const devId = device ? device.deviceId.substr(0, DEVICE_ID_LENGTH) + '...' : null;
  const devLabel = device ? ` (${device.label})` : '';

  getLogger().info(`default device ${type}: (${title}) ${devId}${devLabel}`);
};

const createWatcher = () => {
  const ums = clientEntityProvider.getUserMediaService();
  const { inputAudio, inputVideo } = ums.defaultDevices;

  logDevice('audio', 'start', inputAudio);
  logDevice('video', 'start', inputVideo);

  ums.subscribe('mediaDevicesChange', (evt: any) => {
    if (evt.default) {
      if (evt.default.inputAudio) {
        logDevice('audio', 'changed to', ums.defaultDevices.inputAudio);
      }
      if (evt.default.inputVideo) {
        logDevice('video', 'changed to', ums.defaultDevices.inputVideo);
      }
    }
    if (evt.unplugged) {
      // remove links to streams from default devices
      if (evt.unplugged.inputAudio) {
        const device = evt.unplugged.inputAudio as MediaDeviceInfo;
        getLogger().info(
          `audio device was unplugged: ${device.deviceId.substr(0, DEVICE_ID_LENGTH)}... (${device.label})`
        );
      }
      if (evt.unplugged.inputVideo) {
        const device = evt.unplugged.inputVideo as MediaDeviceInfo;
        getLogger().info(
          `video device was unplugged: ${device.deviceId.substr(0, DEVICE_ID_LENGTH)}... (${device.label})`
        );
      }
    }
  });

  ums.subscribe('mediaAccessError', (evt: any) => {
    const devices = [
      {
        kind: 'audio',
        id: evt.inputAudioDeviceId,
      },
      {
        kind: 'video',
        id: evt.inputVideoDeviceId,
      },
    ]
      .filter((d) => !!d.id)
      .map((d) => `${d.kind} (${d.id})`);

    getLogger().error(LogErrorCode.MODULE, `media access error: ${evt.name}, [${devices.join(', ')}]`);
  });

  ums.subscribe('mediaAccessAllowed', (evt: any) => {
    getLogger().info(
      `media access allowed for: ${Object.keys(evt)
        .filter((key) => evt[key])
        .join(', ')}`
    );
  });

  // subscribe on change default audio/video stream
  ums.subscribe('defaultInputAudioDeviceChanged', () => {
    logDevice('audio', 'changed to', ums.defaultDevices.inputAudio);
  });

  ums.subscribe('defaultInputVideoDeviceChanged', () => {
    logDevice('video', 'changed to', ums.defaultDevices.inputVideo);
  });

  // update screen sharing support info
  const sss = ums.screenSharingService;

  getLogger().info('screen sharing capabilities: (start)', {
    isSupported: sss.isSupported,
    isBlocked: sss.isBlocked,
  });

  sss.addEventListener('screenSharingStatusUpdate', (screenSharingCapability: IScreenSharingSupportInfo) => {
    getLogger().info('screen sharing capabilities: (updated)', {
      isSupported: sss.isSupported,
      isBlocked: sss.isBlocked,
      errorName: screenSharingCapability.errorName,
    });
  });
};

export default (socket: Socket) => {
  if (notInitialized) {
    notInitialized = false;
    socket.on('authorized', createWatcher);
  }
};
