/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import middleware from './middleware';
import setupIoCClientForTest from '../../../../../test/utils/ioc/testIoCClient';
import { TWebRTCServices, IWebrtcService } from '../../../../../ioc/common/ioc-interfaces';
import { mock } from 'jest-mock-extended';
import { actionCreators } from '../../../common/redux/actionCreators';
import { webrtcConstants } from '../../../../../ioc/common/ioc-constants';
import { TStore, IStoreState } from '../../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IWebrtcParticipantInfo } from '../../../common/types/IWebrtcParticipantInfo';

/* tslint:disable:no-magic-numbers */

beforeAll(setupIoCClientForTest());

describe('webphone middleware', () => {
  let webphoneMiddleware = null;

  const webrtcServices: TWebRTCServices = {
    webrtc: mock<IWebrtcService>(),
  };

  const next = jest.fn();
  const store = mock<TStore>();

  beforeEach(() => {
    webphoneMiddleware = middleware('storeKey1')(null, webrtcServices)(store)(next);
  });

  test('events to webrtcServices must be added on init', () => {
    const state = mock<IStoreState>({ media: {} });
    const stream = 'stream1';
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    store.getState.mockReturnValue(state);

    webphoneMiddleware(actionCreators.initWebphoneModule());

    const onMock = webrtcServices.webrtc.on as jest.Mock;
    const [userStreamAddedEventName, funcUserStreamAdded] = onMock.mock.calls[0];
    const [userStreamRemovedEventName, funcUserStreamRemoved] = onMock.mock.calls[1];

    expect(userStreamAddedEventName).toBe(webrtcConstants.SERVICE_EVENTS.USER_STREAM_ADDED);
    expect(userStreamRemovedEventName).toBe(webrtcConstants.SERVICE_EVENTS.USER_STREAM_REMOVED);

    funcUserStreamAdded('userId1', stream);
    funcUserStreamRemoved(stream);

    expect(store.dispatch).toHaveBeenNthCalledWith(
      1,
      actionCreators.publisherUpdate({
        hasAudioInputDevice: false,
        hasVideoInputDevice: false,
      } as IWebrtcParticipantInfo)
    );
    expect(store.dispatch).toHaveBeenNthCalledWith(
      2,
      actionCreators.screenSharingUpdateCapabilities({
        supported: false,
        isBlocked: false,
      })
    );
    expect(store.dispatch).toHaveBeenNthCalledWith(3, actionCreators.webphoneUserSetupSetDontShowAgain(false));
    expect(store.dispatch).toHaveBeenNthCalledWith(4, appActionCreators.userStreamAdded(stream));
    expect(store.dispatch).toHaveBeenNthCalledWith(5, appActionCreators.userStreamRemoved(stream));
  });
});
