import _noop from 'lodash/noop';
import getModuleState from '../utils/getModuleState';
import { IWebrtcMediaStreamTrackData } from '../../../common/types/webrtcMediaTypes';
import { IWebrtcParticipantInfo } from '../../../common/types/IWebrtcParticipantInfo';
import { IAppReduxEnumMiddleware, TMiddlewareAPI } from '../../../../../ioc/common/ioc-interfaces';
import { actionCreators } from '../../../common/redux/actionCreators';
import { initVAD, destroyVAD, VADCallbacks, updateVAD } from '../utils/vad';
import { updateTalkingAudioLevel } from '../utils/vad-analyzer';
import { WebphoneAction } from '../../../common/redux/types/WebphoneAction';

const filterMicTrackData = (tracks: IWebrtcMediaStreamTrackData[]): IWebrtcMediaStreamTrackData => {
  return tracks.filter((t) => t && t.kind === 'mic')[0];
};

const vadCallbacks: VADCallbacks = {
  voiceSilence: _noop, // what to do if sefl audio silenced?
  voiceStart: _noop,
  voiceLevel: _noop,
  voiceStop: _noop,
};

export const middlewareEnum: IAppReduxEnumMiddleware = {
  [WebphoneAction.SESSION_CONNECTED_SUCCESS](store, next, action, socket, storeKey) {
    next(action);

    const {
      userSetup: { microphoneStream },
      publisher: { isAudioStreamMuted },
    } = getModuleState(store, storeKey);
    const userId = store.getState().auth.user?.id;

    if (microphoneStream) {
      initVAD({
        userId,
        isMuted: isAudioStreamMuted,
        track: (microphoneStream as MediaStream).getAudioTracks()[0],
        ...vadCallbacks,
      });
    }
  },
  [WebphoneAction.WEBPHONE_SET_STREAM_FROM_MICROPHONE](store, next, action, socket, storeKey) {
    next(action);

    const {
      isConnected,
      publisher: { isAudioStreamMuted },
    } = getModuleState(store, storeKey);
    const userId = store.getState().auth.user?.id;
    const { stream } = action;

    if (!isConnected || !stream) {
      destroyVAD(userId);
      return;
    }

    initVAD({
      userId,
      isMuted: isAudioStreamMuted,
      track: (stream as MediaStream).getAudioTracks()[0],
      ...vadCallbacks,
    });
  },
  [WebphoneAction.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK](store, next, action, socket, storeKey) {
    next(action);

    const data = filterMicTrackData(action.tracks);

    if (!data) {
      return;
    }

    const { userId } = action;

    if (!data.track) {
      destroyVAD(userId);
      return;
    }

    // todo: do not get data from unknown store (*.getLocalState() & *.getRoomState())
    const userInfo = store.getState().room.webphone[userId] as IWebrtcParticipantInfo;
    const isAudioStreamMuted = userInfo ? userInfo.isAudioStreamMuted : true;

    initVAD({
      userId: action.userId,
      isMuted: isAudioStreamMuted,
      track: data.track,
      ...vadCallbacks,
    });
  },
  [WebphoneAction.PARTICIPANT_UPDATE_INFO](store, next, action) {
    next(action);

    const { userId, isAudioStreamMuted } = action.participant as IWebrtcParticipantInfo;

    updateVAD(userId, isAudioStreamMuted);
  },
};

let inspectorInitialized: boolean = false;

export const initVadInspector = (store: TMiddlewareAPI) => {
  if (inspectorInitialized) {
    return;
  }

  inspectorInitialized = true;

  const setLoudestUser = (userId: string) => {
    store.dispatch(actionCreators.baseScreenSetLoudestUser(userId));
  };

  vadCallbacks.voiceStart = (userId: string) => {
    store.dispatch(actionCreators.participantToggleTalking(userId, true));
  };

  vadCallbacks.voiceStop = (userId: string) => {
    store.dispatch(actionCreators.participantToggleTalking(userId, false));
  };

  vadCallbacks.voiceLevel = (userId, audioLevel) => {
    updateTalkingAudioLevel(userId, audioLevel, setLoudestUser);
  };
};
