/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import getModuleState from '../utils/getModuleState';
import { IAppReduxEnumMiddleware } from '../../../../../ioc/common/ioc-interfaces';
import { TLoggerClientModule } from '../../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IWebrtcMediaStreamTrackData, IWebrtcMediaStreamTrackKind } from '../../../common/types/webrtcMediaTypes';
import { WebphoneAction } from '../../../common/redux/types/WebphoneAction';

type TrackFrom = 'basic' | 'participant';
type TrackKind = IWebrtcMediaStreamTrackKind;
type TrackInfo = {
  readyState: MediaStreamTrack['readyState'];
  enabled: MediaStreamTrack['enabled'];
  muted: MediaStreamTrack['muted'];
  id: MediaStreamTrack['id'];
};
type TrackHistory = TrackInfo & {
  interval: number;
};

const trackTimeInterval = 2000;

const trackHistory: Record<string, TrackHistory> = {};

const getLogger = (): TLoggerClientModule => {
  return clientEntityProvider.getLogger().webPhone;
};

export const trackFullState = (track: TrackInfo) => {
  return track ? `${track.id} ${track.readyState} ${track.enabled} ${track.muted}` : '';
};

const _tsId = (userId: string, kind: TrackKind) => `${userId}-${kind}`;

const logAndWatchTrack = (userId: string, kind: TrackKind, from: TrackFrom, track: MediaStreamTrack) => {
  const target = from + (from === 'participant' ? ` (${userId})` : '');
  const ts = (trackHistory[_tsId(userId, kind)] = {
    readyState: track.readyState,
    enabled: track.enabled,
    muted: track.muted,
    id: track.id,
    interval: window.setInterval(() => {
      if (!(track.readyState === ts.readyState && track.enabled === ts.enabled && track.muted === ts.muted)) {
        getLogger().info(`${kind}: ${target} track change props`, {
          trackState: trackFullState(track),
        });
        ts.readyState = track.readyState;
        ts.enabled = track.enabled;
        ts.muted = track.muted;
      }
    }, trackTimeInterval),
  });

  track.addEventListener('ended', () => {
    window.clearInterval(ts.interval);
    getLogger().info(`${kind}: ${target} track ended`, {
      trackState: trackFullState(track),
    });
  });

  getLogger().info(`${kind}: set ${target} track`, {
    trackState: trackFullState(track),
  });
};

const endLogAndWatchTrack = (userId: string, kind: TrackKind, from: TrackFrom) => {
  const target = from + (from === 'participant' ? ` (${userId})` : '');
  const tsId = _tsId(userId, kind);
  const ts = trackHistory[tsId];

  trackHistory[tsId] = null;
  getLogger().info(`${kind}: remove ${target} track ${ts ? trackFullState(ts) : '(already removed)'}`);
};

const middlewareEnum: IAppReduxEnumMiddleware = {
  [WebphoneAction.WEBPHONE_SET_STREAM_FROM_CAMERA](store, next, action, socket, storeKey) {
    const kind = 'cam';
    const from = 'basic';
    const state = getModuleState(store, storeKey);
    const publisherUserId = store.getState().auth.user?.id;

    if (action.stream) {
      logAndWatchTrack(publisherUserId, kind, from, action.stream.getVideoTracks()[0]);
    } else if (state.userSetup.cameraStream) {
      endLogAndWatchTrack(publisherUserId, kind, from);
    } else {
      getLogger().info(`${kind}: close ${from} track`);
    }

    next(action);
  },
  [WebphoneAction.WEBPHONE_SET_STREAM_FROM_MICROPHONE](store, next, action, socket, storeKey) {
    const kind = 'mic';
    const from = 'basic';
    const state = getModuleState(store, storeKey);
    const publisherUserId = store.getState().auth.user?.id;

    if (action.stream) {
      logAndWatchTrack(publisherUserId, kind, from, action.stream.getAudioTracks()[0]);
    } else if (state.userSetup.microphoneStream) {
      endLogAndWatchTrack(publisherUserId, kind, from);
    } else {
      getLogger().info(`${kind}: close ${from} track`);
    }

    next(action);
  },
  [WebphoneAction.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK](store, next, action, socket, storeKey) {
    next(action);

    // add track's watcher for participants, exclude tracks from publisher
    const { tracks, userId } = action;
    const from = 'participant';
    const publisherUserId = store.getState().auth.user?.id;

    if (userId === publisherUserId) {
      return;
    }

    (tracks as IWebrtcMediaStreamTrackData[]).forEach((data) => {
      if (data.track) {
        logAndWatchTrack(userId, data.kind, from, data.track);
      } else {
        endLogAndWatchTrack(userId, data.kind, from);
      }
    });
  },
};

export default middlewareEnum;
