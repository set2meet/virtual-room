/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { CONNECTION_STATUS } from '../../common/types/constants/webrtc';
import { IWebrtcMediaStreamTrackData } from '../../common/types/webrtcMediaTypes';
import { AnyAction } from 'redux';
import { createMediaElement } from './utils/html5-media';
import { DisconnectReason } from '../../../../ioc/common/ioc-constants';
import { IWebPhoneReduxState } from './types/IWebPhoneReduxState';
import { WebphoneAction } from '../../common/redux/types/WebphoneAction';

interface IWebPhoneReduxReducer {
  [key: string]: (state: IWebPhoneReduxState, action: AnyAction) => IWebPhoneReduxState;
}

const INITIAL_STATE_PUBLISHER = {
  isSharingScreen: false,
  isAudioStreamMuted: true,
  isVideoStreamMuted: true,
};

const INITIAL_STATE: IWebPhoneReduxState = {
  webrtcServiceName: null,
  accessParams: null,
  connectionStatus: CONNECTION_STATUS.NONE,
  isConnected: false,
  publisher: {
    hasAudioInputDevice: false,
    hasVideoInputDevice: false,
    ...INITIAL_STATE_PUBLISHER,
  },
  isTalking: {},
  baseScreen: {
    loudestUser: null,
    selectedUser: null,
    autoSelectLoudestUser: true,
  },
  screenSharing: {
    supported: false,
    errorName: null,
    isBlocked: false,
    toggleControlAvailable: true,
  },
  userSetup: {
    isComplete: false,
    isDisabled: false,
    dontShowAgain: false,
    currentTab: null,
    cameraStream: null,
    microphoneStream: null,
    testCallInProgress: false,
  },
  connectionQuality: {
    score: NaN,
  },
  mediaElements: {
    mic: {},
    cam: {},
    desktop: {},
  },
};

const reducers: IWebPhoneReduxReducer = {
  [WebphoneAction.CHANGE_WEBRTC_SERVICE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      webrtcServiceName: action.serviceName,
      connectionQuality: {
        ...INITIAL_STATE.connectionQuality,
      },
    };
  },
  [WebphoneAction.SET_WEBRTC_ACCESS_PARAMS](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      accessParams: action.params,
    };
  },
  [WebphoneAction.CONNECTION_UPDATE_STATUS](state: IWebPhoneReduxState, action: AnyAction) {
    const status: CONNECTION_STATUS = action.status;

    return {
      ...state,
      connectionStatus: status,
      isConnected: status === CONNECTION_STATUS.ESTABLISHED,
    };
  },
  [WebphoneAction.SESSION_CLOSED](state: IWebPhoneReduxState) {
    return {
      ...state,
      participants: [],
      baseScreen: {
        ...INITIAL_STATE.baseScreen,
      },
    };
  },
  [WebphoneAction.PUBLISHER_UPDATE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      publisher: {
        ...state.publisher,
        ...action.publisher,
      },
    };
  },
  [WebphoneAction.BASE_SCREEN_SET_LOUDEST_USER](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      baseScreen: {
        ...state.baseScreen,
        loudestUser: action.userId,
      },
    };
  },
  [WebphoneAction.BASE_SCREEN_SET_SELECTED_USER](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      baseScreen: {
        ...state.baseScreen,
        selectedUser: action.userId,
        autoSelectLoudestUser: false,
      },
    };
  },
  [WebphoneAction.BASE_SCREEN_DEL_SELECTED_USER](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      baseScreen: {
        ...state.baseScreen,
        selectedUser: null,
        autoSelectLoudestUser: true,
      },
    };
  },
  [WebphoneAction.TOGGLE_SCREEN_SHARING](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      publisher: {
        ...state.publisher,
        isSharingScreen: !state.publisher.isSharingScreen,
      },
    };
  },
  [WebphoneAction.SCREEN_SHARING_CAPABILITIES_UPDATE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      screenSharing: {
        ...state.screenSharing,
        ...action.screenSharingSupport,
      },
    };
  },
  [WebphoneAction.SCREEN_SHARING_SET_ERROR](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      screenSharing: {
        ...state.screenSharing,
        errorName: action.errName,
      },
    };
  },
  [WebphoneAction.SCREEN_SHARING_SET_AVALIABILITY](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      screenSharing: {
        ...state.screenSharing,
        toggleControlAvailable: action.value,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_LEFT_CALL](state: IWebPhoneReduxState, action: AnyAction) {
    const { userId } = action;
    const baseScreen = { ...state.baseScreen };
    const { loudestUser, selectedUser } = state.baseScreen;

    if (loudestUser === userId) {
      baseScreen.loudestUser = null;
    }
    if (selectedUser === userId) {
      baseScreen.selectedUser = null;
    }

    return {
      ...state,
      baseScreen,
    };
  },
  [WebphoneAction.WEBPHONE_DISCONNECT](state: IWebPhoneReduxState, action: AnyAction) {
    // clear state only on correct exit
    if (action.reason === DisconnectReason.EXIT) {
      return {
        ...state,
        accessParams: null,
        publisher: {
          ...state.publisher,
          ...INITIAL_STATE_PUBLISHER,
        },
        isTalking: INITIAL_STATE.isTalking,
        baseScreen: INITIAL_STATE.baseScreen,
        mediaElements: INITIAL_STATE.mediaElements,
      };
    }

    return state;
  },
  [WebphoneAction.PARTICIPANT_TOGGLE_TALKING](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      isTalking: {
        ...state.isTalking,
        [action.userId]: action.isTalking,
      },
    };
  },
  [WebphoneAction.TOGGLE_AUDIO_STREAM](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      publisher: {
        ...state.publisher,
        isAudioStreamMuted: action.value,
      },
    };
  },
  [WebphoneAction.TOGGLE_VIDEO_STREAM](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      publisher: {
        ...state.publisher,
        isVideoStreamMuted: action.value,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_SHOW](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        currentTab: action.tab,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_COMPLETE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        isComplete: true,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_TOGGLE_DISABLE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        isDisabled: action.value,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_TEST_CALL_TOGGLE_PROGRESS](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        testCallInProgress: action.value,
      },
    };
  },
  [WebphoneAction.WEBPHONE_SET_STREAM_FROM_CAMERA](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        cameraStream: action.stream,
      },
    };
  },
  [WebphoneAction.WEBPHONE_SET_STREAM_FROM_MICROPHONE](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        microphoneStream: action.stream,
      },
    };
  },
  [WebphoneAction.WEBPHONE_USER_SETUP_SET_DONT_SHOW_AGAIN](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      userSetup: {
        ...state.userSetup,
        dontShowAgain: action.value,
      },
    };
  },
  [WebphoneAction.WEBPHONE_SET_CONNECTION_QUALITY](state: IWebPhoneReduxState, action: AnyAction) {
    return {
      ...state,
      connectionQuality: {
        score: action.value,
      },
    };
  },
  [WebphoneAction.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK](state: IWebPhoneReduxState, action: AnyAction) {
    const { mic, cam, desktop } = state.mediaElements;
    const { userId, tracks } = action;
    const mediaElements = {
      mic: { ...mic },
      cam: { ...cam },
      desktop: { ...desktop },
    };

    tracks.forEach((td: IWebrtcMediaStreamTrackData) => {
      mediaElements[td.kind][userId] = createMediaElement(td.track);
    });

    return {
      ...state,
      mediaElements,
    };
  },
};

export default (state: any, action: any): any => {
  if (reducers.hasOwnProperty(action.type)) {
    const stateN: IWebPhoneReduxState = reducers[action.type](state, action);

    return stateN;
  }

  return state || INITIAL_STATE;
};
