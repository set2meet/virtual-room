/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TGetCurrentUserAsParticipant } from './IWebphoneSelectors';

const getCurrentUserAsParticipant: TGetCurrentUserAsParticipant = (appState) => {
  const publisher = appState.webphone.publisher;
  const currentUser = appState.auth?.user;

  return {
    ...publisher,
    userId: currentUser?.id,
    userDisplayName: currentUser?.displayName,
    picture: currentUser?.picture,
  };
};

export default getCurrentUserAsParticipant;
