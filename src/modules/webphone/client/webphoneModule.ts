/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleError } from '../../../ioc/client/providers/ModuleProvider/types/ModuleError';
import reducer from './redux/webphoneReducer';
import reduxMiddleware from './redux/middleware/middleware';
import { actionCreators } from '../common/redux/actionCreators';
import webphoneParticipantsReducer from '../common/redux/reducer/participantsReducer';
import { TWebphoneModuleInitOptions } from './types/TWebphoneModuleInitOptions';

const createWebphoneModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'webphone',
  moduleInitOptions?: TWebphoneModuleInitOptions
): Promise<IS2MModule> => {
  if (!moduleInitOptions || !(moduleInitOptions.webRTCServicesFactory || moduleInitOptions.webRTCServices)) {
    throw new Error(ModuleError.REQUIRE_INITIAL_OPTIONS);
  }
  const webrtcServices = moduleInitOptions.webRTCServices
    ? await moduleInitOptions.webRTCServices
    : (await moduleInitOptions.webRTCServicesFactory())();

  return {
    rootComponent: null,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: reducer,
        },
        reducersToMerge: {
          room: {
            [stateKey]: webphoneParticipantsReducer,
          },
        },
        middlewares: [reduxMiddleware(stateKey)(socket, webrtcServices)],
        initialActions: [actionCreators.initWebphoneModule()],
      },
    ],
  };
};

export default createWebphoneModule;
