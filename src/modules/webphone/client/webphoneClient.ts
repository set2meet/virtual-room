/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as ui from './components';
import { actionCreators, TWebphoneActionCreators } from '../common/redux/actionCreators';
import { IUIWebPhone } from './components/IUIWebPhone';
import { IWebphoneStorageKeys } from './services/IWebphoneStorageKeys';
import { webphoneStorageKeys } from './services/storageKeys';
import { TWebphoneActionTypes, WebphoneAction } from '../common/redux/types/WebphoneAction';
import { IVRModule } from '../../../ioc/client/types/interfaces';
import getCurrentUserAsParticipant from './redux/selectors/getCurrentUserAsParticipant';
import { IWebphoneSelectors } from './redux/selectors/IWebphoneSelectors';

interface IWebphone extends IVRModule<TWebphoneActionCreators> {
  actionTypes: TWebphoneActionTypes;
  storageKeys: IWebphoneStorageKeys;
  ui: IUIWebPhone;
  selectors: IWebphoneSelectors;
  actionCreators: TWebphoneActionCreators;
}

export const webphoneClientModule: IWebphone = {
  actionTypes: WebphoneAction,
  actionCreators,
  storageKeys: webphoneStorageKeys,
  ui,
  selectors: {
    getCurrentUserAsParticipant,
  },
};
