/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const BASE_SCREEN_USER_PANEL_INFO_HEIGHT = 40;

export interface IBSFSButton {
  status: boolean;
}

export interface IBSMediaContainer extends IRequiredTheme {
  isFullScreenMode: boolean;
  hasAnyStream: boolean;
}

export interface IBSBottomPanel extends IRequiredTheme {
  isHidden: boolean;
}

export const styledBaseScreenContainer = () => `
  width: 100%;
  height: 100%;
  position: relative;

  > div {
    width: 100%;
    height: 100%;
    background-color: transparent;
  }
`;

const topMargin = '25px';
const centerBlock = `
  left: 50%;
  transform: translate(-50%, 0) !important;
`;

export const styleBaseScreenMediaContainer = ({ theme }: IRequiredTheme) => {
  const icons = clientEntityProvider.getIcons();

  return `
    width: 100%;
    position: relative;
    margin-top: ${topMargin};
    height: calc(100% - ${topMargin});
  
    > .default {
      top: 0;
      left: 0;
      margin: 0;
      border: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      position: absolute;
      color: #fff;
      font-size: 2em;
      letter-spacing: 1px;
      display: inline-flex;
      align-items: center;
      justify-content: center;
  
      > span {
        text-align: center;
  
        &:before {
          font-size: 200px;
          color: #ccc;
          content: ${icons.code.user};
          font-family: ${icons.fontFamily};
        }
      }
    }
  `;
};

const getMediaWidthStyle = (isFullScreenMode: boolean, hasAnyStream: boolean): string => {
  if (isFullScreenMode) {
    return hasAnyStream ? '' : 'width: 100%;';
  } else {
    return 'max-width: 100%;';
  }
};

export const styledMediaContainer = ({ isFullScreenMode, hasAnyStream }: IBSMediaContainer) => `
  margin: 0;
  border: 0;
  padding: 0;
  height: 100%;

  > .media {
    margin: 0;
    border: 0;
    padding: 0;
    height: 100%;
    display: inline-block;
    position: absolute;
    ${getMediaWidthStyle(isFullScreenMode, hasAnyStream)};
    ${centerBlock};

    > video.minimal-view {
      opacity: 0;
      width: 1px;
      height: auto;
      position: absolute;
      left: 0;
      top: 0;
    }

    > canvas.base {
      position: relative;
      ${centerBlock};
    }

    > canvas.thumb {
      top: 0;
      right: 0;
      position: absolute;
    }

    > .camera-thumb {
      z-index: 3;
      position: absolute;
      right: 0;
      width: 160px;
      height: 120px;
      bottom: ${BASE_SCREEN_USER_PANEL_INFO_HEIGHT}px;

      > video {
        height: 100%;
        width: auto;
        position: relative;
        left: 50%;
        box-shadow: 0 0 2px 2px #c3c3c3;
        transform: translate(-50%, 0) !important;
      }
    }
  }
`;

export const styledBotomPanel = ({ isHidden, theme }: IBSBottomPanel) => {
  const hidden = isHidden
    ? `
    &:not(:hover) {
      opacity: 0;
      transition: opacity 1s ease-in;
    }
  `
    : '';

  return `
    z-index: 2;
    border: 0;
    margin: 0;
    padding: 0 20px;
    text-align: center;
    height: ${BASE_SCREEN_USER_PANEL_INFO_HEIGHT}px;
    line-height: ${BASE_SCREEN_USER_PANEL_INFO_HEIGHT}px;
    position: absolute;
    bottom: 0;
    width: 100%;
    white-space: nowrap;
    font-size: 22px;
    font-weight: bold;
    color: ${theme.primaryColor};
    white-space: nowrap;
    background: ${theme.backgroundColorThird};

    &:hover {
      opacity: 1;
    }

    ${hidden};
  `;
};

export const styledFullscreenButton = ({ status }: IBSFSButton) => `
  background: none;  
  position: static;
  bottom: auto;
  right: auto;
  float: right;
  box-shadow: none;

  &:before {
    color: #ccc;
  }
`;

export const styledUserName = () => `
  display: inline-block;
  width: calc(100% - 40px);
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: left;
`;

const deviceControlsWidth = 260;

export const styledDeviceControls = () => `
  position: absolute;
  width: ${deviceControlsWidth}px;
  left: calc(50% - ${deviceControlsWidth / 2}px);
  bottom: calc(${BASE_SCREEN_USER_PANEL_INFO_HEIGHT}px + 10px);

  > * {
    margin: 0 10px;
  }
`;
