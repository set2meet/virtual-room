import { withIoCFactory } from '../../../../../test/utils/utils';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import React from 'react';
import { DispatchProp } from 'react-redux';
import { getUserWithTokens } from '../../../../../test/mocks/auth';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { setRoom } from '../../../../../test/utils/actions';
import { boolean, text } from '@storybook/addon-knobs';
import { getFakeVideoDevicesInfo } from '../../../../../test/mocks/media';
import withStoryRootIndicator from '../../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { IWebPhoneBaseScreenProps } from './IBaseScreenProps';

const BaseScreen = withIoCFactory<IWebPhoneBaseScreenProps>(() =>
  clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.WebPhone_BaseScreen)
);

export default {
  title: 'webphone/BaseScreen',
  component: BaseScreen,
  parameters: {
    redux: {
      enable: true,
    },
  },
  decorators: [
    connectDecorator(),
    withStoryRootIndicator,
    withModule(ModuleRegistryKey.Fullscreen),
    withModule(ModuleRegistryKey.Webphone),
    withModule(ModuleRegistryKey.RoomModule),
  ],
};

export const Default = (props: DispatchProp) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', 'room123');
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  setRoom(props, roomId, userId);

  const fullscreenButton = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.Fullscreen_Button);
  const inputVideos = getFakeVideoDevicesInfo(1);

  props.dispatch(appActionCreators.updateDevices(inputVideos));
  props.dispatch(appActionCreators.changeMediaSettingsInputVideoStreamError(null));
  props.dispatch(appActionCreators.webphoneRequestStreamFromCamera());
  props.dispatch(
    appActionCreators.publisherUpdate({
      hasVideoInputDevice: true,
    })
  );

  return (
    <BaseScreen
      roomId={roomId}
      autoStartSession={false}
      fullscreenStatus={boolean('fullscreenStatus', true)}
      fullscreenButton={fullscreenButton}
    />
  );
};
