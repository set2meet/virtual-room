import * as React from 'react';
import { connect } from 'react-redux';
import ButtonCamera from '../ButtonCamera';
import ButtonMicrophone from '../ButtonMicrophone';
import ButtonScreenSharing from '../ButtonScreenSharing';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import styled, { withTheme, StyledComponent } from 'styled-components';
import {
  IBSBottomPanel,
  IBSMediaContainer,
  styledBotomPanel,
  styleBaseScreenMediaContainer,
  styledBaseScreenContainer,
  styledFullscreenButton,
  styledDeviceControls,
  styledUserName,
  styledMediaContainer,
} from './BaseScreen.style';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import { IWebrtcParticipantInfo } from '../../../common/types/IWebrtcParticipantInfo';
import { IRequiredTheme, IStoreState } from '../../../../../ioc/client/types/interfaces';
import { IWebPhoneBaseScreenProps } from './IBaseScreenProps';

const EXPLORE_MOUSE_IDLE_TIME = 1000; // 1s
const EXPLORE_MOUSE_NODES = 10; // length
const EXPLORE_MOUSE_MIN_Y = screen.height / 2;
const THUMB_BOX_HEIGHT = 120;
const USER_AVATAR_SIZE = '200px';
const BaseScreenContainer = withTheme(
  styled.div`
    ${styledBaseScreenContainer};
  `
);
const BaseScreenMediaContainer = withTheme(styled.div<IRequiredTheme>`
  ${styleBaseScreenMediaContainer};
`);
const MediaContainer = styled.div<IBSMediaContainer>`
  ${styledMediaContainer};
`;
const DevicesControls = styled.div`
  ${styledDeviceControls};
`;
const UserName = styled.div`
  ${styledUserName};
`;
const BottomPanel = styled.div<IBSBottomPanel>`
  ${styledBotomPanel};
`;

type MouseDirection = 1 | 0;

interface IMousePosition {
  clientX: number;
  clientY: number;
  timestamp: number;
  direction: MouseDirection;
}

interface IBaseScreenStateProps {
  isMeetingStarted: boolean;
  isConnected: boolean;
  hasCamera: boolean;
  hasDesktop: boolean;

  videoCamera?: HTMLVideoElement;
  videoDesktop?: HTMLVideoElement;
  baseScreenUser: IWebrtcParticipantInfo;
  thisUserId: string;
  thisUserColor?: string;
}

interface IBaseScreenDispatchProps {
  startMeeting: ActionCreator<AnyAction>;
  toggleAudioStream: ActionCreator<AnyAction>;
}

interface IBaseScreenState {
  showInfo: boolean;
}

type IBaseScreenProps = IBaseScreenStateProps & IBaseScreenDispatchProps & IWebPhoneBaseScreenProps;

export class BaseScreen extends React.Component<IBaseScreenProps, IBaseScreenState> {
  public static defaultProps = {
    autoStartSession: false,
  };

  public constructor(props: IBaseScreenProps) {
    super(props);

    this.state = {
      showInfo: false,
    };

    this.FullscreenToggleButton =
      props.fullscreenButton &&
      styled(props.fullscreenButton)`
        ${styledFullscreenButton};
      `;
  }

  private FullscreenToggleButton: StyledComponent<React.ComponentClass, any>;
  private reDrawBase: boolean = false;
  private reDrawThumb: boolean = false;
  private nodeMedia: HTMLElement = null;
  private nodeCanvasBase: HTMLCanvasElement = null;
  private canvas2DContext: CanvasRenderingContext2D = null;
  private nodeCanvasThumb: HTMLCanvasElement = null;
  private canvasTargetBase: HTMLVideoElement = null;
  private canvasTargetThumb: HTMLVideoElement = null;
  private baseScreenCameraVideo: HTMLVideoElement = null;
  private _exploreMousePositionNodes: IMousePosition[] = [];
  private _exploreMousePositionDirectionLength: number = 0;
  private _exploreMousePositionTimeout: any = 0;

  private _exploreMousePosition = (evt: MouseEvent) => {
    if (!this.props.fullscreenStatus) {
      return;
    }

    const { clientX, clientY } = evt;

    if (clientY < EXPLORE_MOUSE_MIN_Y) {
      return;
    }

    clearTimeout(this._exploreMousePositionTimeout);

    const nodes = this._exploreMousePositionNodes;
    const totalNodes = nodes.length;
    const lastNode = nodes[totalNodes - 1];

    if (totalNodes === EXPLORE_MOUSE_NODES) {
      this._exploreMousePositionDirectionLength -= nodes.shift().direction;
    }

    const nextNode: IMousePosition = {
      clientX,
      clientY,
      timestamp: Date.now(),
      direction: lastNode ? (clientY - lastNode.clientY > 0 ? 1 : 0) : 0,
    };

    nodes.push(nextNode);
    this._exploreMousePositionDirectionLength += nextNode.direction;

    // if mouse goes down - show user info panel
    const mouseGoesDown = this._exploreMousePositionDirectionLength === EXPLORE_MOUSE_NODES;

    if (this.state.showInfo !== mouseGoesDown) {
      this.setState({ showInfo: mouseGoesDown });
    }

    // check mouse not moving
    this._exploreMousePositionTimeout = setTimeout(() => {
      this.setState({ showInfo: false });
    }, EXPLORE_MOUSE_IDLE_TIME);
  };

  private _refMedia = (node: HTMLElement) => {
    if (node && node !== this.nodeMedia) {
      this.nodeMedia = node;
    }
  };

  private _refCanvasBase = (node: HTMLCanvasElement) => {
    if (node && node !== this.nodeCanvasBase) {
      this.nodeCanvasBase = node;
      this.canvas2DContext = node.getContext('2d', { alpha: false });
    }
  };

  private _refCanvasThumb = (node: HTMLCanvasElement) => {
    if (node && node !== this.nodeCanvasThumb) {
      this.nodeCanvasThumb = node;
    }
  };

  private _initHiddenPanel = (node: HTMLDivElement) => {
    if (node) {
      node.addEventListener('mousemove', this._exploreMousePosition);
    }
  };

  private _scheduleDrawBase() {
    if (this.reDrawBase) {
      setTimeout(() => requestAnimationFrame(this.reDrawVideoBase), 0);
    }
  }

  private reDrawVideoBase = () => {
    this._scheduleDrawBase();

    const video = this.canvasTargetBase;

    if (!(video && this.nodeMedia && this.nodeCanvasBase)) {
      return;
    }

    const origWidth = video.videoWidth;
    const origHeight = video.videoHeight;

    if (!(origWidth && origHeight)) {
      return;
    }

    const baseHeight = Math.round(this.nodeMedia.getBoundingClientRect().height);
    const baseWidth = Math.round((baseHeight * origWidth) / origHeight);
    const { width, height } = this.nodeCanvasBase;

    this.canvas2DContext.clearRect(0, 0, width, height);

    if (width !== baseWidth) {
      this.nodeCanvasBase.width = baseWidth;
    }
    if (height !== baseHeight) {
      this.nodeCanvasBase.height = baseHeight;
    }

    this.canvas2DContext.drawImage(video, 0, 0, baseWidth, baseHeight);
  };

  private _scheduleDrawThumb() {
    if (this.reDrawThumb) {
      setTimeout(() => requestAnimationFrame(this.reDrawVideoThumb), 0);
    }
  }

  private reDrawVideoThumb = () => {
    this._scheduleDrawThumb();

    const video = this.canvasTargetThumb;

    if (!(video && this.nodeMedia && this.nodeCanvasThumb)) {
      return;
    }

    const origWidth = video.videoWidth;
    const origHeight = video.videoHeight;

    if (!(origWidth && origHeight)) {
      return;
    }

    const baseHeight = THUMB_BOX_HEIGHT;
    const baseWidth = Math.round((baseHeight * origWidth) / origHeight);

    if (this.nodeCanvasThumb.width !== baseWidth) {
      this.nodeCanvasThumb.width = baseWidth;
    }
    if (this.nodeCanvasThumb.height !== baseHeight) {
      this.nodeCanvasThumb.height = baseHeight;
    }

    const ctx = this.nodeCanvasThumb.getContext('2d');

    ctx.clearRect(0, 0, baseWidth, baseHeight);
    ctx.drawImage(video, 0, 0, baseWidth, baseHeight);
  };

  private _resumeVideoOnPause(video: HTMLVideoElement): void {
    if (!video) {
      return;
    }

    video.addEventListener('pause', () => {
      video.play();
    });
  }

  private startDrawVideoBase() {
    if (!this.reDrawBase) {
      this.reDrawBase = true;
      this.reDrawVideoBase();
    }
  }

  private stopDrawVideoBase() {
    this.reDrawBase = false;
    this.nodeCanvasBase.width = 0;
    this.nodeCanvasBase.height = 0;
  }

  private startDrawVideoThumb() {
    if (!this.reDrawThumb) {
      this.reDrawThumb = true;
      this.reDrawVideoThumb();
    }
  }

  private stopDrawVideoThumb() {
    this.reDrawThumb = false;
    this.nodeCanvasThumb.width = 0;
    this.nodeCanvasThumb.height = 0;
  }

  private renderAvatar() {
    const user = this.props.baseScreenUser;
    const userColor = this.props.thisUserColor;

    if (!user) {
      return null;
    }

    const { UserAvatar } = clientEntityProvider.getComponentProvider().UI;

    return (
      <div className="default">
        <UserAvatar
          size={USER_AVATAR_SIZE}
          userColor={userColor}
          displayName={user.userDisplayName}
          src={user.picture}
        />
      </div>
    );
  }

  private renderMediaInfo(hasAnyStream: boolean) {
    const user = this.props.baseScreenUser;
    const { fullscreenStatus } = this.props;
    const isHidden = fullscreenStatus && !this.state.showInfo;
    const FullScreenToggleButton = this.FullscreenToggleButton;

    if (fullscreenStatus) {
      return (
        <BottomPanel isHidden={isHidden}>
          <UserName>{user && user.userDisplayName}</UserName>
          <DevicesControls>
            <ButtonMicrophone />
            <ButtonCamera />
            <ButtonScreenSharing />
          </DevicesControls>
          <FullScreenToggleButton />
        </BottomPanel>
      );
    } else if (hasAnyStream) {
      return (
        <BottomPanel isHidden={isHidden}>
          <UserName>{user && user.userDisplayName}</UserName>
          <FullScreenToggleButton />
        </BottomPanel>
      );
    }

    return null;
  }

  private renderMedia() {
    const user = this.props.baseScreenUser;
    const { hasDesktop, hasCamera, fullscreenStatus } = this.props;
    const videoCameraUnmuted = user && !user.isVideoStreamMuted;
    const hasAnyStream = (hasCamera && videoCameraUnmuted) || hasDesktop;
    const canvasBaseStyle = {
      display: hasAnyStream ? 'inline-block' : 'none',
    };
    const canvasThumbStyle = {
      display: hasCamera && videoCameraUnmuted && hasDesktop ? 'inline-block' : 'none',
    };

    return (
      <MediaContainer isFullScreenMode={fullscreenStatus} hasAnyStream={hasAnyStream}>
        <div className="media" ref={this._refMedia}>
          {this.renderMediaInfo(hasAnyStream)}
          <canvas ref={this._refCanvasBase} style={canvasBaseStyle} className="base" />
          <canvas ref={this._refCanvasThumb} style={canvasThumbStyle} className="thumb" />
        </div>
      </MediaContainer>
    );
  }

  private _patchBrowserRendering() {
    const { videoCamera, isConnected } = this.props;

    if (videoCamera) {
      videoCamera.oncanplaythrough = () => {
        videoCamera.play();
        videoCamera.oncanplaythrough = null;
      };
    }

    const existedVideo = this.baseScreenCameraVideo;
    const newVideo = existedVideo !== videoCamera;

    if (!isConnected && videoCamera && newVideo) {
      if (existedVideo && existedVideo.parentNode) {
        existedVideo.parentNode.removeChild(existedVideo);
      }

      videoCamera.className = 'minimal-view';
      videoCamera.style.display = 'inline-block';

      this.nodeMedia.appendChild(videoCamera);
    }

    this.baseScreenCameraVideo = videoCamera;
  }

  private _showBaseScreenUser() {
    const user = this.props.baseScreenUser;
    const { videoCamera, videoDesktop } = this.props;
    const isVideoCamUnmuted = user && !user.isVideoStreamMuted;

    this.canvasTargetBase = videoDesktop || (isVideoCamUnmuted && videoCamera);
    this.canvasTargetThumb = videoDesktop && isVideoCamUnmuted && videoCamera;

    this._patchBrowserRendering();

    if (this.canvasTargetBase) {
      this.startDrawVideoBase();
    } else {
      this.stopDrawVideoBase();
    }

    if (this.canvasTargetThumb) {
      this.startDrawVideoThumb();
    } else {
      this.stopDrawVideoThumb();
    }
  }

  public componentDidMount() {
    if (this.props.autoStartSession && !this.props.isMeetingStarted) {
      this.props.startMeeting();
    }
    if (this.props.videoCamera) {
      this.props.videoCamera.play();
    }
    if (this.props.videoDesktop) {
      this.props.videoDesktop.play();
    }
    this._showBaseScreenUser();
  }

  public componentDidUpdate(prevProps: IBaseScreenProps) {
    this._showBaseScreenUser();
    this._resumeVideoOnPause(this.props.videoCamera);
    this._resumeVideoOnPause(this.props.videoDesktop);
  }

  public componentWillUnmount() {
    this.stopDrawVideoBase();
    this.stopDrawVideoThumb();
  }

  public render() {
    return (
      <BaseScreenContainer>
        <div ref={this._initHiddenPanel}>
          <BaseScreenMediaContainer>
            {this.renderAvatar()}
            {this.renderMedia()}
          </BaseScreenMediaContainer>
        </div>
      </BaseScreenContainer>
    );
  }
}

export default connect<IBaseScreenStateProps, IBaseScreenDispatchProps>(
  (state: IStoreState) => {
    // TODO move ownerID to webphone props, hardrcoded key!
    // TODO move userColor to webphone props?, hardrcoded key!
    // TODO move isMeetingStarted to webphone props?, hardrcoded key!
    const { ownerId } = state.room.state;
    const { baseScreen, mediaElements, isConnected } = state.webphone;
    const baseScreenUserId = baseScreen.selectedUser || baseScreen.loudestUser || ownerId;
    const currentThisUserId = isConnected ? undefined : state.auth.user.id;
    const userId = baseScreenUserId || currentThisUserId;
    const baseScreenUser = state.room.webphone[userId];
    const videoDesktop = mediaElements.desktop[userId] as HTMLVideoElement;
    const videoCamera = mediaElements.cam[userId] as HTMLVideoElement;
    const hasDesktop = !!videoDesktop;
    const hasCamera = !!videoCamera && baseScreenUser && baseScreenUser.hasVideoInputDevice;
    const userColors = state?.room?.state?.userColors;
    const userColor = userId && userColors ? userColors[userId] : undefined;

    return {
      isConnected,
      baseScreenUser,
      videoDesktop,
      videoCamera,
      hasDesktop,
      hasCamera,
      isMeetingStarted: !!state?.room?.webrtcService?.sessionId,
      thisUserColor: userColor,
      thisUserId: state.auth.user.id,
    };
  },
  {
    toggleAudioStream: actionCreators.toggleAudioStream,
    startMeeting: actionCreators.startMeeting,
  }
)(BaseScreen);
