/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { css, keyframes } from 'styled-components';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme, ITheme } from '../../../../../ioc/client/types/interfaces';

export const VIDEO_BOX_WIDTH = 170;
export const VIDEO_BOX_HEIGHT = 120;

const MARGIN = '10px';
const BTTN_SIZE = '20px';
const THUMB_WIDTH = VIDEO_BOX_WIDTH + 'px';
const THUMB_BORDER_RADIUS = '2px';
const THUMB_PANEL_HEIGHT = '24px';
const THUMB_VIDEO_HEIGHT = VIDEO_BOX_HEIGHT + 'px';
const THUMB_PANEL_BG_COLOR = '#575757';
const THUMB_SELECTED_BORDER_WIDTH = '1px';
const THUMB_SELECTED_BORDER_STYLE = 'solid';
const THUMB_SELECTED_BORDER = `border: ${THUMB_SELECTED_BORDER_WIDTH} ${THUMB_SELECTED_BORDER_STYLE}`;
const THUMB_SELECTED_BORDER_ANIMATION_TIME = '1s infinite';

const keyframesRotating = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export interface IParticipantContainer {
  theme: ITheme;
}

export interface IBottomPanel {
  theme: ITheme;
  hasVideo: boolean;
  userColor: string;
}

export interface IUserMenu {
  theme: ITheme;
}

export interface IUserMediaStatus {
  theme: ITheme;
}

export interface IUserMediaContainer extends IRequiredTheme {
  isHorizontal: boolean;
}

const keyframesBorderColor = (colorA: string, colorB: string) => keyframes`
  0%  { border-color: ${colorA}; }
  20% { border-color: ${colorA}; }
  25% { border-color: ${colorB}; }
  70% { border-color: ${colorB}; }
  75% { border-color: ${colorA}; }
`;

const borderWithAnimation = (props: IParticipantContainer) => css`
  border-color: ${props.theme.primaryColor};
  border-width: ${THUMB_SELECTED_BORDER_WIDTH};
  border-style: ${THUMB_SELECTED_BORDER_STYLE};
  animation: ${keyframesBorderColor(props.theme.primaryColor, props.theme.selectedElementBorderColorPrimary)}
    ${THUMB_SELECTED_BORDER_ANIMATION_TIME};
`;

const getSelectionBorderSize = (size: string): string => {
  return `calc(${size} + 2 * ${THUMB_SELECTED_BORDER_WIDTH})`;
};

const stypedstyledParticipantContainerBorder = ({ theme }: IRequiredTheme) => {
  const { primaryColor, selectedElementBorderColorPrimary } = theme;

  return css`
    &:before {
      content: '';
      position: absolute;
      display: inline-block;
      border-radius: ${THUMB_BORDER_RADIUS};
      width: ${getSelectionBorderSize(THUMB_WIDTH)};
      height: ${getSelectionBorderSize(THUMB_PANEL_HEIGHT)};
      top: -${THUMB_SELECTED_BORDER_WIDTH};
      left: -${THUMB_SELECTED_BORDER_WIDTH};
    }

    &.box-max:before {
      height: ${getSelectionBorderSize(THUMB_VIDEO_HEIGHT)};
    }

    &.main-talking:before {
      ${css`
        ${borderWithAnimation};
      `};
    }

    &.auto-talking:before {
      ${THUMB_SELECTED_BORDER} ${primaryColor};
    }

    &.main-silent:before {
      ${THUMB_SELECTED_BORDER} ${selectedElementBorderColorPrimary};
    }
  `;
};

export const styledParticipantContainer = (props: IParticipantContainer) => css`
  position: relative;
  display: inline-block;
  z-index: 1;
  background: rgba(255, 255, 255, 0);
  border-radius: ${THUMB_BORDER_RADIUS};
  width: ${THUMB_WIDTH};
  height: ${THUMB_PANEL_HEIGHT};
  margin: 1px 1px ${MARGIN} 1px;

  &.box-max {
    height: ${THUMB_VIDEO_HEIGHT};
  }

  ${stypedstyledParticipantContainerBorder(props)};
`;

interface IUserStatusIcon extends IRequiredTheme {
  userColor: string;
}

const userColorSize = 7;
const userColorMargin = 4;
const pulseIconSize = 6;

const keyframesPulseRing = keyframes`
  0% { transform: scale(.33); }
  80%, 100% { opacity: 0; }
`;

const keyframesPulseDot = keyframes`
  0% { transform: scale(.8); }
  50% { transform: scale(1); }
  100% { transform: scale(.8); }
`;

const styledUserStatusIconConnected = ({ theme, userColor }: IUserStatusIcon) => `
  &:before {
    content: ' ';
    font-size: 1;
    display: inline-block;
    border-radius: 50%;
    border: 1px solid ${theme.primaryTextColor};
    box-sizing: content-box;
    width: ${pulseIconSize}px;
    height: ${pulseIconSize}px;
    vertical-align: baseline;
    background-color: ${userColor};
    margin-right: ${userColorMargin}px;
  }
`;

const styledUserStatusIconWaiting = ({ theme }: IRequiredTheme) => css`
  &:before {
    content: '';
    position: relative;
    display: inline-block;
    width: ${pulseIconSize * 2}px;
    height: ${pulseIconSize * 2}px;
    box-sizing: border-box;
    border-radius: 50%;
    background-color: white;
    left: -2px;
    animation: ${keyframesPulseRing} 1.25s cubic-bezier(0.215, 0.61, 0.355, 1) infinite;
  }

  &:after {
    content: '';
    position: absolute;
    left: calc(50% - ${pulseIconSize / 2}px - 2px);
    top: calc(50% - ${pulseIconSize / 2}px);
    display: inline-block;
    width: ${pulseIconSize}px;
    height: ${pulseIconSize}px;
    background-color: #c3c3c3;
    border-radius: 50%;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.3);
    animation: ${keyframesPulseDot} 1.25s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite;
  }
`;

export const styledBottomPanel = ({ theme, hasVideo, userColor }: IBottomPanel) => css`
  margin: 0;
  z-index: 10;
  position: absolute;
  display: flex;
  left: 0;
  bottom: 0;
  width: 100%;
  padding: 0 4px;
  overflow: hidden;
  align-items: center;
  justify-content: space-between;
  height: ${THUMB_PANEL_HEIGHT};
  background: ${THUMB_PANEL_BG_COLOR};
  border-bottom-left-radius: ${THUMB_BORDER_RADIUS};
  border-bottom-right-radius: ${THUMB_BORDER_RADIUS};
  ${!hasVideo ? `border-top-left-radius: ${THUMB_BORDER_RADIUS}` : ''};
  ${!hasVideo ? `border-top-right-radius: ${THUMB_BORDER_RADIUS}` : ''};

  > span {
    position: relative;
    display: inline-flex;
    width: ${2 * pulseIconSize}px;
    height: 100%;
    align-items: center;
    justify-content: center;

    ${userColor ? styledUserStatusIconConnected({ theme, userColor }) : styledUserStatusIconWaiting({ theme })};
  }

  > div:nth-child(2) {
    width: calc(100% - ${userColorSize + userColorMargin}px);
    display: inline-block;
    text-align: left;
    font-size: 12px;
    white-space: nowrap;
    overflow: hidden;
    user-select: none;
    text-overflow: ellipsis;
    color: ${userColor ? theme.primaryTextColor : '#c3c3c3'};
  }

  > div:nth-child(3) {
    text-align: right;
    white-space: nowrap;
  }
`;

const videoBoxSize: Record<string, string> = {
  true: `
    max-height: 100%;
    width: auto;
    top: 0;
    left: 50%;
    transform: translate(-50%, 0);
  `,
  false: `
    max-width: 100%;
    width: auto;
    top: 50%;
    left: 0;
    transform: translate(0, -50%);
  `,
  undefined: `
    max-width: 100%;
    width: auto;
  `,
};

export const styledUserMediaContainer = ({ isHorizontal, theme }: IUserMediaContainer) => css`
  > .media {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    z-index: 2;
    left: 0;
    top: 0;
    position: absolute;
    background: ${theme.backgroundColor};

    &:before {
      content: '';
      display: inline-block;
      width: 40px;
      height: 40px;
      background-size: 100% 100%, 50% 50%, 50% 50%, 50% 50%;
      background-repeat: no-repeat;
      background-image: linear-gradient(${theme.backgroundColor}, ${theme.backgroundColor}),
        linear-gradient(30deg, ${theme.primaryColor} 100%, lightgrey 30%),
        linear-gradient(120deg, ${theme.primaryColor} 100%, lightgrey 30%),
        linear-gradient(300deg, ${theme.primaryColor} 100%, lightgrey 30%);
      background-position: center center, left top, right top, left bottom, right bottom;
      background-origin: content-box, border-box, border-box, border-box;
      background-clip: content-box, border-box, border-box, border-box;
      border-radius: 50%;
      border: 4px solid transparent;
      position: relative;
      top: -10px;
      animation: ${keyframesRotating} 2s linear infinite;
    }

    > video {
      position: absolute;
      ${videoBoxSize[isHorizontal.toString()]};
    }
  }
`;

export const styledUserMediaStatus = (props: IUserMediaStatus) => {
  const icons = clientEntityProvider.getIcons();

  return `
    width: ${BTTN_SIZE};
    height: ${BTTN_SIZE};
    line-height: ${BTTN_SIZE};
    text-align: center;
    outline: none;
    border: 0;
    margin: 0;
    padding: 0;
    background: none;
    position: relative;
    display: inline-block;
    color: ${props.theme.primaryColor};
  
    &:before {
      font-size: 14px;
      font-family: ${icons.fontFamily};
    }
  
    &.cam {
      &:before {
        content: ${icons.code.videoCamera};
      }
  
      &.muted:before {
        color: ${props.theme.primaryTextColor};
        content: ${icons.code.videoCameraMuted};
      }
    }
  
    &.mic {
      &:before {
        content: ${icons.code.microphone};
      }
  
      &.muted:before {
        color: ${props.theme.primaryTextColor};
        content: ${icons.code.microphoneMuted};
      }
    }
  `;
};

export const styledUserMenu = (props: IUserMenu) => {
  const icons = clientEntityProvider.getIcons();

  return `
    width: calc(${BTTN_SIZE} / 2);
    height: ${BTTN_SIZE};
    line-height: ${BTTN_SIZE};
    text-align: center;
    outline: none;
    border: 0;
    margin: 0;
    padding: 0;
    background: none;
    position: relative;
    color: ${props.theme.primaryTextColor};
  
    &:before {
      font-size: 12px;
      font-family: ${icons.fontFamily};
      content: ${icons.code.menuDotted};
    }
  
    &:disabled {
      color: #676767;
    }
  `;
};
