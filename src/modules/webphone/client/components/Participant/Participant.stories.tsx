import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import Participant from './Participant';
import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import React from 'react';

export default {
  title: 'webphone/Participant',
  component: Participant,
  decorators: [connectDecorator()],
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const Muted = () => {
  const userMediaInfo = {
    userId: '1234',
    userDisplayName: 'Pavel',
    isSharingScreen: boolean('isSharingScreen', false),
    isAudioStreamMuted: boolean('isAudioStreamMuted', true),
    isVideoStreamMuted: boolean('isVideoStreamMuted', true),
    hasAudioInputDevice: boolean('hasAudioInputDevice', true),
    hasVideoInputDevice: boolean('hasVideoInputDevice', true),
  };
  return (
    <Participant
      onSelect={action('onSelect')}
      userColor={'#ceff00'}
      isReady={boolean('isReady', true)}
      isTalking={boolean('isTalking', false)}
      isAutoSelectParticipant={boolean('isAutoSelectParticipant', false)}
      isBaseScreenUser={boolean('isBaseScreenUser', false)}
      userMediaInfo={userMediaInfo}
    />
  );
};

export const Talking = () => {
  const userMediaInfo = {
    userId: '1234',
    userDisplayName: 'Pavel',
    isSharingScreen: boolean('isSharingScreen', false),
    isAudioStreamMuted: boolean('isAudioStreamMuted', false),
    isVideoStreamMuted: boolean('isVideoStreamMuted', false),
    hasAudioInputDevice: boolean('hasAudioInputDevice', true),
    hasVideoInputDevice: boolean('hasVideoInputDevice', true),
  };
  return (
    <Participant
      onSelect={action('onSelect')}
      userColor={'#ceff00'}
      isReady={boolean('isReady', true)}
      isTalking={boolean('isTalking', true)}
      isAutoSelectParticipant={boolean('isAutoSelectParticipant', false)}
      isBaseScreenUser={boolean('isBaseScreenUser', false)}
      userMediaInfo={userMediaInfo}
    />
  );
};

export const NotReady = () => {
  const userMediaInfo = {
    userId: '1234',
    userDisplayName: 'Pavel',
    isSharingScreen: boolean('isSharingScreen', false),
    isAudioStreamMuted: boolean('isAudioStreamMuted', false),
    isVideoStreamMuted: boolean('isVideoStreamMuted', false),
    hasAudioInputDevice: boolean('hasAudioInputDevice', true),
    hasVideoInputDevice: boolean('hasVideoInputDevice', true),
  };
  return (
    <Participant
      onSelect={action('onSelect')}
      userColor={'#ceff00'}
      isReady={boolean('isReady', false)}
      isTalking={boolean('isTalking', true)}
      isAutoSelectParticipant={boolean('isAutoSelectParticipant', false)}
      isBaseScreenUser={boolean('isBaseScreenUser', false)}
      userMediaInfo={userMediaInfo}
    />
  );
};
