import * as React from 'react';
import { LogEvent } from 'src/ioc/client/ioc-constants';
import styled from 'styled-components';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IWebrtcParticipantInfo } from '../../../common/types/IWebrtcParticipantInfo';

import {
  IUserMenu,
  styledUserMenu,
  IBottomPanel,
  styledBottomPanel,
  styledUserMediaStatus,
  IUserMediaContainer,
  styledUserMediaContainer,
  IParticipantContainer,
  styledParticipantContainer,
  VIDEO_BOX_WIDTH,
  VIDEO_BOX_HEIGHT,
} from './Participant.style';

const VIDEO_BOX_RATIO = VIDEO_BOX_WIDTH / VIDEO_BOX_HEIGHT;

const UserMenu = styled.button<IUserMenu>`
  ${styledUserMenu};
`;
const BottomPanel = styled.div<IBottomPanel>`
  ${styledBottomPanel};
`;
const UserMediaStatus = styled.div`
  ${styledUserMediaStatus};
`;
const UserMediaContainer = styled.div<IUserMediaContainer>`
  ${styledUserMediaContainer};
`;
const ParticipantsContainer = styled.div<IParticipantContainer>`
  ${styledParticipantContainer};
`;

interface IParticipantState {
  isHorizontalVideo: boolean;
}

interface IMediaElements {
  elementMic?: HTMLAudioElement;
  elementCam?: HTMLVideoElement;
  elementDsk?: HTMLVideoElement;
}

interface IParticipantProps extends IMediaElements {
  onSelect: (userId: string, freezeUser: boolean) => void;
  userColor: string;
  isReady: boolean;
  isTalking: boolean;
  isAutoSelectParticipant: boolean;
  isBaseScreenUser: boolean;
  userMediaInfo: IWebrtcParticipantInfo;
}

const propsDiffs = (props1: IMediaElements, props2: IMediaElements, prop: keyof IMediaElements): boolean => {
  return props1[prop] !== props2[prop];
};

const getVideoElement = (props: IMediaElements): HTMLVideoElement => {
  return props.elementDsk || props.elementCam;
};

const logMediaUpdate = (userId: string, oldTag: HTMLElement, newTag: HTMLElement) => {
  const tag = oldTag || newTag;

  if (!tag) {
    return;
  }

  const tagType = tag ? tag.nodeName.toLocaleLowerCase() : null;
  const oldTrack = oldTag ? (oldTag as any).srcObject.getTracks()[0].id : null;
  const newTrack = newTag ? (newTag as any).srcObject.getTracks()[0].id : null;

  clientEntityProvider.getLogger().webPhone.report(LogEvent.WpViewParticipant, {
    userId,
    tagType,
    oldTrack,
    newTrack,
    message: 'Participant updated tag',
  });
};

export class Participant extends React.PureComponent<IParticipantProps, IParticipantState> {
  // private _timeStartVideoCheck: number;
  private _scheduledAnimationFrame: number;
  private _mediaTagContainer: HTMLDivElement;
  private _requiredUpdateVideoPosition: boolean = false;

  private _setMediaContainerRef = (node: HTMLDivElement) => {
    this._mediaTagContainer = node;

    if (node) {
      const prevProps = {};

      this._updateAudioContent(prevProps);
      this._updateVideoContent(prevProps);
    }
  };

  private _updateMediaContent(oldTag: HTMLElement, newTag: HTMLElement) {
    const container = this._mediaTagContainer;

    if (container) {
      if (oldTag && oldTag.parentNode === container) {
        container.removeChild(oldTag);
      }
      if (newTag) {
        container.appendChild(newTag);
      }
      logMediaUpdate(this.props.userMediaInfo.userId, oldTag, newTag);
    }
  }

  private _updateAudioContent(prevProps: IMediaElements) {
    const oldTag = prevProps.elementMic;
    const newTag = this.props.elementMic;

    this._updateMediaContent(oldTag, newTag);

    if (newTag) {
      setTimeout(() => newTag.play(), 1);
    }
  }

  private _updateVideoContent(prevProps: IMediaElements) {
    const oldVideoTag = getVideoElement(prevProps);
    const newVideoTag = getVideoElement(this.props);

    this._updateMediaContent(oldVideoTag, newVideoTag);

    if (newVideoTag) {
      if (this._isVideoVisible(this.props)) {
        this._tryUpdateVideoSize();
      } else {
        this._requiredUpdateVideoPosition = true;
      }
    }
  }

  private _tryUpdateVideoSize() {
    this._scheduledAnimationFrame = window.requestAnimationFrame(this._updateVideoSize);
  }

  private _stopUpdateVideoSize() {
    window.cancelAnimationFrame(this._scheduledAnimationFrame);
  }

  private _isVideoVisible(props: IParticipantProps): boolean {
    const { userMediaInfo, elementCam, elementDsk } = props;

    return !!elementDsk || (!!elementCam && !userMediaInfo.isVideoStreamMuted);
  }

  private _updateVideoSize = () => {
    const tag = getVideoElement(this.props);

    const videoWidth = (tag && tag.videoWidth) || null;
    const videoHeight = (tag && tag.videoHeight) || null;

    // const { videoWidth, videoHeight } = tag;

    if (!(videoWidth + videoHeight)) {
      /*if (isNaN(this._timeStartVideoCheck)) {
        this._timeStartVideoCheck = Date.now();
      }*/
      this._tryUpdateVideoSize();
    } else {
      tag.pause();
      setTimeout(
        () =>
          tag.play().catch(() => {
            console.log(`
          [WebPhone] can't play video [
            user name: ${this.props.userMediaInfo.userDisplayName}
            user id: ${this.props.userMediaInfo.userId}
          ]
        `);
          }),
        1
      );
      // this._timeStartVideoCheck = NaN;
      this.setState({ isHorizontalVideo: VIDEO_BOX_RATIO < videoWidth / videoHeight });
    }
  };

  private onSelect = () => {
    this.props.onSelect(this.props.userMediaInfo.userId, true);
  };

  public constructor(props: IParticipantProps) {
    super(props);

    this.state = {
      isHorizontalVideo: false,
    };
  }

  public componentDidUpdate(prevProps: IParticipantProps) {
    const micUpdated = propsDiffs(this.props, prevProps, 'elementMic');
    const camUpdated = propsDiffs(this.props, prevProps, 'elementCam');
    const dskUpdated = propsDiffs(this.props, prevProps, 'elementDsk');

    if (micUpdated) {
      this._updateAudioContent(prevProps);
    }

    if (camUpdated || dskUpdated) {
      this._updateVideoContent(prevProps);
    }

    const tag = getVideoElement(this.props);
    const oldVisibility = this._isVideoVisible(prevProps);
    const newVisibility = this._isVideoVisible(this.props);
    const visibilityUpdated = oldVisibility !== newVisibility;
    const anyTrackUpdated = micUpdated || camUpdated || dskUpdated;

    if (tag && visibilityUpdated) {
      if (!anyTrackUpdated && newVisibility && this._requiredUpdateVideoPosition) {
        this._requiredUpdateVideoPosition = false;
        this._tryUpdateVideoSize();
      }
    }
  }

  public componentWillUnmount() {
    this._stopUpdateVideoSize();
  }

  // tslint:disable:cyclomatic-complexity
  public render() {
    const { userMediaInfo: umi, elementCam, elementDsk, isReady } = this.props;
    const { isTalking, isBaseScreenUser, isAutoSelectParticipant, userColor } = this.props;
    const hasMicDevice = umi.hasAudioInputDevice;
    const hasCamDevice = umi.hasVideoInputDevice;
    const isMicMuted = umi.isAudioStreamMuted ? 'muted' : '';
    const isCamMuted = umi.isVideoStreamMuted ? 'muted' : '';
    const hasDsk = !!elementDsk;
    const hasVisibleCam = hasCamDevice && !isCamMuted && !!elementCam;
    const hasAnyVideo = hasVisibleCam || hasDsk;
    const classNameBoxColor =
      (isBaseScreenUser && !isAutoSelectParticipant ? 'main' : 'auto') + '-' + (isTalking ? 'talking' : 'silent');
    const classNameBoxSize = hasAnyVideo ? 'box-max' : 'box-min';
    const className = isReady ? classNameBoxColor + ' ' + classNameBoxSize : '';

    return (
      <ParticipantsContainer className={className} onClick={this.onSelect}>
        <UserMediaContainer isHorizontal={this.state.isHorizontalVideo}>
          <div className="media" ref={this._setMediaContainerRef} />
        </UserMediaContainer>
        <BottomPanel hasVideo={hasAnyVideo} userColor={isReady ? userColor : null}>
          <span />
          <div>{umi.userDisplayName}</div>
          {isReady && (
            <div>
              {hasMicDevice && <UserMediaStatus className={`mic ${isMicMuted}`} />}
              {hasCamDevice && <UserMediaStatus className={`cam ${isCamMuted}`} />}
              <UserMenu disabled={true} />
            </div>
          )}
        </BottomPanel>
      </ParticipantsContainer>
    );
  }
  // tslint:enable
}

export default Participant;
