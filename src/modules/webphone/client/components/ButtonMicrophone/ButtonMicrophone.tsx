import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { UserMediaErrorName } from '../../../../../ioc/common/ioc-interfaces';

const iconMap: {
  [key: string]: string;
} = {
  false: 'microphone',
  true: 'microphoneMuted',
};

interface IButtonMicrophoneStateProps {
  disabled?: boolean;
  noInputDevice: boolean;
  isAudioStreamMuted: boolean;
  audioDeviceStatus: UserMediaErrorName;
}

interface IButtonMicrophoneDispatchProps {
  toggleAudioStream: ActionCreator<AnyAction>;
}

const possibleSettings: { [P in UserMediaErrorName]?: string } = {
  NotReadableError: 'Microphone is used by another application',
  NotAllowedError: 'You need to allow using your microphone first',
};

type IButtonMicrophoneProps = IButtonMicrophoneStateProps & IButtonMicrophoneDispatchProps;

export class ButtonMicrophone extends React.Component<IButtonMicrophoneProps> {
  private _onClick = () => {
    this.props.toggleAudioStream();
  };

  private _deviceTooltip = (errName: UserMediaErrorName): string => {
    return possibleSettings[errName] || 'Select device in settings';
  };

  public render() {
    const ButtonContainer = clientEntityProvider.getComponentProvider().UI.RoundButton;
    const { isAudioStreamMuted, noInputDevice, audioDeviceStatus } = this.props;
    const icon = iconMap[String(isAudioStreamMuted)];
    const disabled = noInputDevice && audioDeviceStatus !== null;
    const active = !isAudioStreamMuted && !disabled;
    const tooltip = disabled
      ? this._deviceTooltip(audioDeviceStatus)
      : active
      ? 'Mute your microphone'
      : 'Unmute your microphone';

    return (
      <ButtonContainer tooltip={tooltip} icon={icon} active={active} disabled={disabled} onClick={this._onClick} />
    );
  }
}

export default connect<IButtonMicrophoneStateProps, IButtonMicrophoneDispatchProps>(
  (state: any) => ({
    noInputDevice: !state.webphone.publisher.hasAudioInputDevice,
    isAudioStreamMuted: state.webphone.publisher.isAudioStreamMuted,
    audioDeviceStatus: state.media.settings.inputAudioDeviceError,
  }),
  {
    toggleAudioStream: actionCreators.toggleAudioStream,
  }
)(ButtonMicrophone);
