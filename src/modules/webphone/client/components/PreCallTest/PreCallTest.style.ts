/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export const styledContainer = () => `
  width: 100%;
  height: 100%;
  padding: 30px;

  .pre-call-test-delimiter {
    border: none;
    height: 36px;
    padding: 0;
    margin: 0;
  }
`;

export const styledTitle = () => `
  font-weight: bold;
  margin-bottom: 10px;
`;

export const styledDescription = () => `
  font-weight: normal;
  margin-bottom: 10px;
  padding-right: 60px;
  font-size: 13px;
`;
