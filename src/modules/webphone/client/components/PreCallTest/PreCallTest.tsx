import * as React from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import { actionCreators } from '../../../common/redux/actionCreators';
import Checkbox from '../UserSetup/Checkbox';
import Connection from '../UserSetup/Connection';
import styled from 'styled-components';
import { IUIWebPhoneUserSetup, IWebPhoneUserSetupState } from '../UserSetup/UserSetup.types';
import { styledContainer, styledDescription, styledTitle } from './PreCallTest.style';
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';

interface IPreCallTestDispatchProps {
  setDontShowAgain: (value: boolean) => void;
}

const PreTestCallContainer = styled.div`
  ${styledContainer};
`;
const Description = styled.div`
  ${styledDescription};
`;
const Title = styled.div`
  ${styledTitle};
`;

type IWebPhonePreCallTestStateProps = IWebPhoneUserSetupState & {
  isP2PService: boolean;
  connectionQualityScore: number;
};

export type IPreCallTestProps = IUIWebPhoneUserSetup & IWebPhonePreCallTestStateProps & IPreCallTestDispatchProps;

export class PreCallTest extends React.Component<IPreCallTestProps> {
  public get descriptionPart2_v1() {
    return (
      <Description>
        It is essential to ensure your web browser's connectivity to our servers to participate in a meeting, for this
        is used to establish the media connection
      </Description>
    );
  }

  public get descriptionPart2_v2() {
    return (
      <Description>
        It is essential to have a stable network when participating in a meeting, for this affects the audio and video
        quality, as well as the overall experience
      </Description>
    );
  }

  public get description() {
    return (
      <React.Fragment>
        <Title>Pre-call test display</Title>
        <Description>
          Pre-call test implies a step-by-step audio and video check followed by connection quality test prior to
          entering the meeting
        </Description>
        <Checkbox
          value={this.props.dontShowAgain}
          onChange={this.props.setDontShowAgain}
          label="Always skip my pre-call test on this device"
        />
        <hr className="pre-call-test-delimiter" />
        <Title>Connection quality test</Title>
        {this.props.isP2PService ? this.descriptionPart2_v1 : this.descriptionPart2_v2}
      </React.Fragment>
    );
  }

  public render() {
    return (
      <PreTestCallContainer>
        <Connection description={this.description} />
      </PreTestCallContainer>
    );
  }
}

export default connect<IWebPhonePreCallTestStateProps, IPreCallTestDispatchProps>(
  (state: IStoreState) => {
    const { userSetup, connectionQuality } = state.webphone;

    return {
      ...userSetup,
      connectionQualityScore: connectionQuality.score,
      isP2PService: state.webphone.webrtcServiceName === WebrtcServiceType.P2P,
    };
  },
  {
    setDontShowAgain: actionCreators.webphoneUserSetupSetDontShowAgain,
  }
)(PreCallTest);
