/* tslint:disable:no-magic-numbers */
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { SkeletonProps } from 'react-loading-skeleton';
import { FallbackRegistryKey } from '../../../../../ioc/client/providers/FallbackProvider/types/FallbackRegistryKey';
import React from 'react';
import styled, { css } from 'styled-components';

export const styledParticipantsContainer = () => css`
  padding-top: 9px;
  width: 100%;
  align-items: center;
  display: inline-flex;
  flex-direction: column;
  justify-content: flex-start;
  > * {
    margin-bottom: 10px;
  }
`;

const ParticipantsContainer = styled.div`
  ${styledParticipantsContainer};
`;

const ParticipantsSkeleton = ({ participantsCount = 3 }) => {
  const DarkSkeleton = clientEntityProvider.resolveFallback<SkeletonProps>(FallbackRegistryKey.DarkSkeleton);
  return (
    <ParticipantsContainer>
      {[...Array(participantsCount)].map((_, i) => (
        <DarkSkeleton width={170} height={24} key={i} />
      ))}
    </ParticipantsContainer>
  );
};

export default ParticipantsSkeleton;
