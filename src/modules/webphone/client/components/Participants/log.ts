/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

let lastParticipantsInfo: string;
let lastParticipantsList: string;

const getLogger = () => clientEntityProvider.getLogger().webPhone;

export const logInfo = (message: string) => {
  if (message !== lastParticipantsInfo) {
    lastParticipantsInfo = message;
    getLogger().info(`participants info found in the room: [${message}]`);
  }
};

export const logList = (message: string) => {
  if (message !== lastParticipantsList) {
    lastParticipantsList = message;
    getLogger().info(`participants list found in the room: [${message}]`);
  }
};
