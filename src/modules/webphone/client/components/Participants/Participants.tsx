import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import * as React from 'react';
import { connect } from 'react-redux';
import Participant from '../Participant';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import styled, { withTheme } from 'styled-components';
import ScrollbarArea from 'react-custom-scrollbars';
import { styledNoMeetingBody, styledNoMeetingContainer, styledParticipantsContainer } from './Participants.style';
import { logInfo, logList } from './log';
import { IWebPhoneReduxState } from '../../redux/types/IWebPhoneReduxState';
import { TWebPhoneParticipantsInfo } from '../../../common/redux/types/TWebPhoneParticipantsInfo';

const ParticipantsContainer = withTheme(
  styled.div`
    ${styledParticipantsContainer};
  `
);
const NoMeetingContainer = withTheme(
  styled.div`
    ${styledNoMeetingContainer};
  `
);
const NoMeetingBody = withTheme(
  styled.div`
    ${styledNoMeetingBody};
  `
);

const SpinnerContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const getStreamFromElement = (element: HTMLAudioElement | HTMLVideoElement): MediaStream => {
  return element ? (element.srcObject as MediaStream) : new MediaStream();
};

const hasAudioTrack = (element: HTMLAudioElement | HTMLVideoElement): boolean => {
  return getStreamFromElement(element).getAudioTracks().length !== 0;
};

const hasVideoTrack = (element: HTMLAudioElement | HTMLVideoElement): boolean => {
  return getStreamFromElement(element).getVideoTracks().length !== 0;
};

interface IParticipantsProps {
  show: boolean;
  curretUserId: string;
  isTalkingUser: Record<string, boolean>;
  baseScreenUserId: string;
  isConnected: boolean;
  sessionId: string;
  isAutoSelectParticipant: boolean;
  participantsId: string[];
  participantsInfo: TWebPhoneParticipantsInfo;
  baseScreenSetSelectedUser: ActionCreator<AnyAction>;
  mediaElements: IWebPhoneReduxState['mediaElements'];
  userColors: Record<string, string>;
}

export class Participants extends React.Component<IParticipantsProps> {
  private renderNoMeeting = () => {
    return (
      <NoMeetingContainer>
        <NoMeetingBody>
          <div>No meeting in progress</div>
        </NoMeetingBody>
      </NoMeetingContainer>
    );
  };

  public render() {
    const { Spinner } = clientEntityProvider.getComponentProvider().UI;

    if (!this.props.isConnected) {
      if (this.props.sessionId) {
        return (
          <SpinnerContainer>
            <Spinner />
          </SpinnerContainer>
        );
      } else {
        return this.renderNoMeeting();
      }
    }

    const {
      curretUserId,
      isTalkingUser,
      participantsId,
      participantsInfo,
      baseScreenUserId,
      baseScreenSetSelectedUser,
      isAutoSelectParticipant,
      userColors,
      mediaElements,
    } = this.props;

    return (
      <ScrollbarArea autoHide={true}>
        <ParticipantsContainer>
          {participantsId.map((userId: string) => {
            const umi = participantsInfo[userId];
            const self = userId === curretUserId;
            const elementMic = mediaElements.mic[userId] as HTMLAudioElement;
            const elementCam = mediaElements.cam[userId] as HTMLVideoElement;
            const elementDsk = mediaElements.desktop[userId] as HTMLVideoElement;

            const isReady = self
              ? true
              : umi &&
                umi.hasAudioInputDevice === hasAudioTrack(elementMic) &&
                umi.hasVideoInputDevice === hasVideoTrack(elementCam);

            return (
              <Participant
                key={userId}
                userColor={userColors[userId]}
                isTalking={isTalkingUser[userId]}
                isBaseScreenUser={userId === baseScreenUserId}
                onSelect={baseScreenSetSelectedUser}
                userMediaInfo={participantsInfo[userId]}
                isAutoSelectParticipant={isAutoSelectParticipant}
                elementMic={elementMic}
                elementCam={elementCam}
                elementDsk={elementDsk}
                isReady={isReady}
              />
            );
          })}
        </ParticipantsContainer>
      </ScrollbarArea>
    );
  }
}

export default connect<any, any, any>(
  (state: any) => {
    const { baseScreen, isConnected, mediaElements } = state.webphone;
    const participantsInfo: TWebPhoneParticipantsInfo = state.room.webphone;
    const baseScreenUserId = baseScreen && baseScreen.selectedUser;
    const isTalkingUser = state.webphone.isTalking || {};
    const hostUserId = state.room.state.ownerId;
    const sessionId = state.room.webrtcService.sessionId;
    const curretUserId = state.auth.user.id;
    const currentUser = participantsInfo[curretUserId];
    const hostUser = participantsInfo[hostUserId];
    const topUserId: Record<string, number> = {};
    const participantsId: string[] = state.room.state.participants
      // remove users without data info until info appear about one
      .filter((id: string) => participantsInfo[id] !== undefined);
    const isAutoSelectParticipant = baseScreen.autoSelectLoudestUser;

    if (currentUser) {
      topUserId[currentUser.userDisplayName] = 2;
    }
    if (hostUser) {
      topUserId[hostUser.userDisplayName] = 1;
    }

    logList(state.room.state.participants.join(', '));
    logInfo(Object.keys(participantsInfo).join(', '));

    const hasActiveVideoFromDesktop = (id: string): boolean => participantsInfo[id] && !!mediaElements.desktop[id];
    const hasActiveVideoFromCamera = (id: string): boolean =>
      !!mediaElements.cam[id] && participantsInfo[id] && !participantsInfo[id].isVideoStreamMuted;

    const sortByDisplayName = (a: string, b: string): number => {
      if (topUserId[a] && topUserId[b]) {
        return topUserId[a] - topUserId[b];
      }
      if (topUserId[a] && !topUserId[b]) {
        return -1;
      }
      if (!topUserId[a] && topUserId[b]) {
        return 1;
      }
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
      return 0;
    };

    if (isConnected && participantsId.length > 1) {
      // - first show users with active video streaming
      // - then each group (with/out video streaming) sorts by:
      //  -- first is host user
      //  -- second is current user
      //  -- the others sorts by their display name
      participantsId.sort((idA: string, idB: string) => {
        const aDisplayName = participantsInfo[idA].userDisplayName;
        const bDisplayName = participantsInfo[idB].userDisplayName;
        const aHasVideoDesktop = hasActiveVideoFromDesktop(idA);
        const bHasVideoDesktop = hasActiveVideoFromDesktop(idB);
        const aHasVideoCamera = hasActiveVideoFromCamera(idA);
        const bHasVideoCamera = hasActiveVideoFromCamera(idB);
        const aHasVideo = aHasVideoCamera || aHasVideoDesktop;
        const bHasVideo = bHasVideoCamera || bHasVideoDesktop;

        if (aHasVideo === bHasVideo) {
          if (aHasVideoDesktop === bHasVideoDesktop) {
            return sortByDisplayName(aDisplayName, bDisplayName);
          } else if (!aHasVideoDesktop && !bHasVideoDesktop) {
            return -1;
          } else {
            return 1;
          }
        } else if (aHasVideo && !bHasVideo) {
          return -1;
        } else {
          return 1;
        }
      });
    }
    const userColors = clientEntityProvider.getSelectors().appSelectors.getUserColors(state);

    return {
      userColors,
      curretUserId,
      isConnected,
      participantsId,
      participantsInfo,
      isTalkingUser,
      sessionId,
      baseScreenUserId,
      isAutoSelectParticipant,
      mediaElements,
    };
  },
  {
    baseScreenSetSelectedUser: actionCreators.baseScreenSetSelectedUser,
  }
)(Participants);
