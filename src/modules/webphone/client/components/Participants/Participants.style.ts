/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ITheme } from '../../../../../ioc/client/types/interfaces';
import { css } from 'styled-components';

const noMeetingsHeaderHeight = '60px';

interface IDefaultStyled {
  theme: ITheme;
}

export const styledParticipantsContainer = (props: IDefaultStyled) => css`
  padding-top: 15px;
  width: 100%;
  position: relative;
  align-items: center;
  display: inline-flex;
  flex-direction: column;
  justify-content: flex-start;
  background-color: ${props.theme.backgroundColorSecondary};
`;

export const styledNoMeetingContainer = () => css`
  height: 100%;
  width: 100%;
`;

export const styledNoMeetingBody = (props: IDefaultStyled) => {
  const icons = clientEntityProvider.getIcons();

  return css`
    width: 100%;
    height: calc(100% - ${noMeetingsHeaderHeight});
    align-items: center;
    display: inline-flex;
    justify-content: center;
    color: ${props.theme.primaryTextColor};

    > div:before {
      font-family: ${icons.fontFamily};
      content: ${icons.code.usersStroked};
      font-size: 72px;
      display: block;
      width: 100%;
      text-align: center;
    }
  `;
};
