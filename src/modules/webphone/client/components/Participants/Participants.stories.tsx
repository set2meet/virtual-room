import {
  STORYBOOK_SOCKET_AUTHORIZED_ACTION,
  TOnMockClientActionCallback,
} from '../../../../../test/utils/serverSocket';
import { action } from '@storybook/addon-actions';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import storyRootIndicator from '../../../../../../.storybook/decorators/storyRootIndicator';
import { withIoCFactory } from '../../../../../test/utils/utils';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import React from 'react';
import { boolean, color, text } from '@storybook/addon-knobs';
import { getUserWithTokens } from '../../../../../test/mocks/auth';
import { setRoom } from '../../../../../test/utils/actions';
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import withModule from '../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

const Participants = withIoCFactory<{}>(() =>
  clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.WebPhone_Participants)
);

const onActionCallback: TOnMockClientActionCallback = (reduxAction, serverSocket) => {
  action('sendSocketEvent')(reduxAction);
};

export default {
  title: 'webphone/Participants',
  component: Participants,
  decorators: [
    connectDecorator(),
    storyRootIndicator,
    withModule(ModuleRegistryKey.Webrtc),
    withModule(ModuleRegistryKey.Webphone),
    withModule(ModuleRegistryKey.RoomModule),
  ],
  parameters: {
    redux: {
      enable: true,
    },
    serverSocket: {
      onActionCallback,
    },
  },
};

export const Default = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', 'room123');
  const isOwner = boolean('isOwner', true);
  const ownerId = isOwner ? userId : 'other123';
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  setRoom(props, roomId, ownerId);
  return <Participants />;
};

export const Connecting = (props) => {
  const userId = '123';
  const roomId = 'room123';
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  setRoom(props, roomId, userId);
  props.dispatch({
    type: clientEntityProvider.getConstants().webrtcConstants.SERVICE_ACTIONS.SESSION_STARTED,
    sessionId: '1235',
    serviceName: WebrtcServiceType.P2P,
  });

  return <Participants />;
};

export const WithParticipants = (props) => {
  const userId = '123';
  const roomId = 'room123';
  const actionCreators = clientEntityProvider.getAppActionCreators();
  const appActionTypes = clientEntityProvider.getAppActionTypes();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  setRoom(props, roomId, userId);
  props.dispatch({
    type: appActionTypes.USER_JOIN_ROOM,
    userColor: color('userColor', '#000000'),
    userId,
    roomId,
  });
  props.dispatch(
    clientEntityProvider.getAppActionCreators().updateParticipantInfo({
      userId: '1234',
      userDisplayName: 'Pavel',
      isSharingScreen: true,
      isAudioStreamMuted: true,
      isVideoStreamMuted: true,
      hasAudioInputDevice: true,
      hasVideoInputDevice: true,
    })
  );
  props.dispatch({
    type: appActionTypes.USER_JOIN_ROOM,
    userColor: color('user2Color', '#1924d7'),
    userId: '1234',
    roomId,
  });
  props.dispatch(
    actionCreators.connectionUpdateStatus(
      clientEntityProvider.getConstants().webrtcConstants.CONNECTION_STATUS.ESTABLISHED
    )
  );

  return <Participants />;
};

export const Skeleton = () => {
  const ParticipantsSkeleton = clientEntityProvider.resolveFallback(ComponentRegistryKey.WebPhone_Participants);

  return <ParticipantsSkeleton />;
};
