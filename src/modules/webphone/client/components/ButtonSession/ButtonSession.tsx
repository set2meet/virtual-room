import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { CONNECTION_STATUS } from '../../../common/types/constants/webrtc';

const BUTTON_STATE: {
  [key: string]: {
    title: string;
    icon: string;
    style: string;
  };
} = {
  [CONNECTION_STATUS.NONE]: { title: 'START MEETING', icon: 'phoneTube', style: 'default' },
  [CONNECTION_STATUS.CONNECTING]: { title: '...CONNECTING', icon: 'phoneTube', style: 'default' },
  [CONNECTION_STATUS.ESTABLISHED]: { title: 'END MEETING', icon: 'endCall', style: 'warning' },
  [CONNECTION_STATUS.DISCONNECTING]: { title: '...CLOSING', icon: 'close2', style: 'warning' },
  [CONNECTION_STATUS.CONNECTION_LOST]: { title: '...', icon: 'close2', style: 'warning' },
};

interface IButtonSessionStateProps {
  status: string;
  disabled?: boolean;
  sessionStarted: boolean;
}

interface IButtonSessionDispatchProps {
  endMeeting: ActionCreator<AnyAction>;
  startMeeting: ActionCreator<AnyAction>;
}

export type IButtonSessionProps = IButtonSessionStateProps & IButtonSessionDispatchProps;

export class ButtonSession extends React.Component<IButtonSessionProps> {
  public static defaultProps = {
    disabled: false,
  };

  private toggleSession = () => {
    if (this.props.sessionStarted) {
      this.props.endMeeting();
    } else {
      this.props.startMeeting();
    }
  };

  private get isDisabled(): boolean {
    const { CONNECTING, DISCONNECTING } = CONNECTION_STATUS;

    return this.props.disabled || this.props.status === CONNECTING || this.props.status === DISCONNECTING;
  }

  public render() {
    const status = this.props.status;
    const bttn = BUTTON_STATE[status];
    const { Button } = clientEntityProvider.getComponentProvider().UI;

    return (
      <Button
        icon={bttn.icon}
        title={bttn.title}
        bsStyle={bttn.style}
        disabled={this.isDisabled}
        onClick={this.toggleSession}
      />
    );
  }
}

export default connect<IButtonSessionStateProps, IButtonSessionDispatchProps>(
  (state: any) => ({
    status: state.webphone.connectionStatus,
    sessionStarted: Boolean(state.room.webrtcService.sessionId),
  }),
  {
    endMeeting: actionCreators.endMeeting,
    startMeeting: actionCreators.startMeeting,
  }
)(ButtonSession);
