import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IScreenSharingSupportData } from '../../redux/types/IWebPhoneReduxState';
import { IScreenSharingSupportInfo } from '../../../../../ioc/client/types/interfaces';
import { detect } from 'detect-browser';
import { IButtonScreenSharingProps } from './IButtonScreenSharingProps';

interface IButtonScreenSharingStateProps {
  isSharingScreen: boolean;
  screenSharing: IScreenSharingSupportInfo & IScreenSharingSupportData;
}

interface IButtonScreenSharingDispatchProps {
  toggleScreenSharing: ActionCreator<AnyAction>;
}

type IButtonScreenSharingAllProps = IButtonScreenSharingStateProps &
  IButtonScreenSharingDispatchProps &
  IButtonScreenSharingProps;

export class ButtonScreenSharing extends React.Component<IButtonScreenSharingAllProps> {
  private _onClick = () => {
    this.props.toggleScreenSharing();
  };

  private get _buttonTooltip(): string {
    const { supported, toggleControlAvailable, errorName } = this.props.screenSharing;
    const disabled = !supported || !!errorName;
    const active = this.props.isSharingScreen;
    const browser: any = detect();
    const isSafari: boolean = browser.name && browser.name === 'safari';

    if (errorName) {
      if (errorName === 'NotAllowedError') {
        return 'You have denied access to the use of screen sharing. Refresh page to get access this functionality';
      } else {
        if (isSafari) {
          return 'Use Chrome or Firefox on your macOS device to share your screen';
        }
        return 'You have some problems with screen sharing functionality. Please, contact with tech support';
      }
    } else if (!toggleControlAvailable) {
      return 'In process...';
    }
    if (disabled) {
      return 'Share Screen';
    } else {
      return active ? 'Stop Share' : 'Share Screen';
    }
  }

  public render() {
    const { supported, errorName } = this.props.screenSharing;
    const active = this.props.isSharingScreen;
    const disabled = !supported || !!errorName;

    const ButtonContainer = this.props.container || clientEntityProvider.getComponentProvider().UI.RoundButton;

    return (
      <ButtonContainer
        tooltip={this._buttonTooltip}
        icon="desktop"
        active={active}
        disabled={disabled}
        onClick={this._onClick}
      />
    );
  }
}

export default connect<IButtonScreenSharingStateProps, IButtonScreenSharingDispatchProps>(
  (state: any) => ({
    screenSharing: state.webphone.screenSharing,
    isSharingScreen: state.webphone.publisher.isSharingScreen,
  }),
  (dispatch: any) => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      toggleScreenSharing: bindActionCreators(actionCreators.toggleScreenSharing, dispatch),
      // TODO: remove this actions
      // Call action 'START_SHARING_SCREEN_ERRRO' with text for notification?
      // Catch action in APP and show/hide notification normally from the app middleware
      showNotification: bindActionCreators(appActionCreators.showNotification, dispatch),
      removeNotification: bindActionCreators(appActionCreators.removeNotification, dispatch),
    };
  }
)(ButtonScreenSharing);
