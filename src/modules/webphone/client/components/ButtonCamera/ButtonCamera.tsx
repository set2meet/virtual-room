import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { UserMediaErrorName } from '../../../../../ioc/common/ioc-interfaces';

const iconMap: {
  [key: string]: string;
} = {
  false: 'videoCamera',
  true: 'videoCameraMuted',
};

interface IButtonCameraStateProps {
  disabled?: boolean;
  noInputDevice: boolean;
  isVideoStreamMuted: boolean;
  videoDeviceStatus: UserMediaErrorName;
}

interface IButtonCameraDispatchProps {
  toggleVideoStream: ActionCreator<AnyAction>;
}

type IButtonCameraProps = IButtonCameraStateProps & IButtonCameraDispatchProps;

const possibleSettings: { [P in UserMediaErrorName]?: string } = {
  NotReadableError: 'Camera is used by another application',
  NotAllowedError: 'You need to allow using your camera first',
};

export class ButtonCamera extends React.Component<IButtonCameraProps> {
  private _onClick = () => {
    this.props.toggleVideoStream();
  };

  private _deviceTooltip = (errName: UserMediaErrorName): string => {
    return possibleSettings[errName] || 'Select device in settings';
  };

  public render() {
    const ButtonContainer = clientEntityProvider.getComponentProvider().UI.RoundButton;
    const { isVideoStreamMuted, noInputDevice, videoDeviceStatus } = this.props;
    const icon = iconMap[String(isVideoStreamMuted)];

    const disabled = noInputDevice && videoDeviceStatus !== null;
    const active = !isVideoStreamMuted && !disabled;

    const tooltip = disabled ? this._deviceTooltip(videoDeviceStatus) : active ? 'Stop Video' : 'Start Video';

    return (
      <ButtonContainer tooltip={tooltip} icon={icon} active={active} disabled={disabled} onClick={this._onClick} />
    );
  }
}

export default connect<IButtonCameraStateProps, IButtonCameraDispatchProps>(
  (state: any) => ({
    noInputDevice: !state.webphone.publisher.hasVideoInputDevice,
    isVideoStreamMuted: state.webphone.publisher.isVideoStreamMuted,
    videoDeviceStatus: state.media.settings.inputVideoDeviceError,
  }),
  {
    toggleVideoStream: actionCreators.toggleVideoStream,
  }
)(ButtonCamera);
