import React from 'react';
import Camera from './Camera';
import withStoryRootIndicator from '../../../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

export default {
  title: 'webphone/UserSetup/Camera',
  component: Camera,
  parameters: {
    redux: {
      enable: true,
    },
  },
  decorators: [withStoryRootIndicator, withModule(ModuleRegistryKey.Webphone)],
};

export const Default = () => <Camera />;
