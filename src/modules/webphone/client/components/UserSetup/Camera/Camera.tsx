import * as React from 'react';
import { connect } from 'react-redux';
import { actionCreators } from '../../../../common/redux/actionCreators';
import {
  Row,
  RowDevice,
  RowHeader,
  RowDescription,
  RowDeviceLabel,
  RowDeviceChoice,
  RowDeviceExample,
} from '../common.style';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export interface ISetupCameraStateProps {
  stream?: MediaStream;
}

export interface ISetupCameraDispatchProps {
  requestStreamFromCamera: () => void;
}

export type ISetupCameraProps = ISetupCameraStateProps & ISetupCameraDispatchProps;

export class SetupCamera extends React.Component<ISetupCameraProps> {
  public componentDidMount() {
    this.props.requestStreamFromCamera();
  }

  public render() {
    const { MediaDeviceSelectVideoInput, MediaDeviceTestCamera } = clientEntityProvider.getComponentProvider().Media;

    return (
      <Row>
        <RowHeader>3. Video</RowHeader>
        <RowDescription>Move something in view of the camera to check that it is working.</RowDescription>
        <RowDevice>
          <RowDeviceLabel>Camera</RowDeviceLabel>
          <RowDeviceExample>
            <MediaDeviceTestCamera stream={this.props.stream} />
          </RowDeviceExample>
          <RowDeviceChoice>
            <MediaDeviceSelectVideoInput requestPermissions={this.props.requestStreamFromCamera} />
          </RowDeviceChoice>
        </RowDevice>
      </Row>
    );
  }
}

export default connect<ISetupCameraStateProps, ISetupCameraDispatchProps>(
  (state: any) => ({
    stream: state.webphone.userSetup.cameraStream,
  }),
  {
    requestStreamFromCamera: actionCreators.webphoneRequestStreamFromCamera,
  }
)(SetupCamera);
