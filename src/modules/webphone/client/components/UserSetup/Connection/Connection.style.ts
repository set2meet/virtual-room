/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/* tslint:disable:no-magic-numbers */
import { css, keyframes } from 'styled-components';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';

const listMargin = 8;
const listPadding = 24;
const segmentColorDefault = '#9f9f9f';
const segmentColorUnstable = '#ffd54f';
const segmentColorVeryBad = '#fc5a1b';

export interface IStyledContainer {
  hasExternalContainer?: boolean;
}

export interface IStyledProgress extends IRequiredTheme {
  progress: boolean;
  duration: number;
}

export interface IStyledResults extends IRequiredTheme {
  score: number;
}

export const styledContainer = ({ hasExternalContainer }: IStyledContainer) => `
  width: ${hasExternalContainer ? '100%' : '652px'};
  height: 312px;
  padding: 0;

  .bttn-run-test-splitter {
    border: none;
    padding: 0;
    margin: 0;

    &.v1 {
      height: 36px;
    }

    &.v2 {
      height: 24px;
    }
  }
`;

export const styledInto = ({ theme }: IRequiredTheme) => `
  > p {
    margin: 10px 0;
    font-size: 14px;
  }

  > label {
    font-size: 14px;
    color: ${theme.primaryColor};
  }

  > ul {
    list-style-type: none;
    margin: ${listMargin}px 0;
    padding-left: ${listPadding}px;

    > li {
      font-size: 14px;
      position: relative;
      margin: 8px 0;
      
      &:before {
        position: absolute;
        left: -${listPadding}px;
        top: 6px;
        content: '•';
        font-size: 26px;
        line-height: 10px;
        vertical-align: middle;
        color: ${theme.primaryColor};
      }
    }
  }
`;

export const styledIntroCheckbox = () => `
  &.shifted-down {
    margin: 32px 0 0 0;  
  }
`;

export const styledProgress = ({ theme, progress, duration }: IStyledProgress) => `
  display: inline-block;
  margin-bottom: 10px;
  width: 342px;

  > .title {
    width: 100%;
    height: 32px;
    font-size: 14px;
    align-items: center;
    display: inline-flex;
    justify-content: space-between;
    color: ${theme.primaryTextColor}
  }

  > .progress {
    height: 6px;
    width: 100%;
    border-radius: 0;
    position: relative;
    background-color: ${theme.buttonOffColor};

    &:after {
      top: 0;
      left: 0;
      height: 6px;
      width: ${progress ? 100 : 0}%;
      content: ' ';
      position: absolute;
      transition: width ${duration}ms linear;
      display: inline-block;
      background-color: ${theme.primaryColor};
    }
  }
`;

const segment1 = (primaryColor: string, score: number) => {
  if (score >= 5) {
    return primaryColor;
  }

  return segmentColorDefault;
};

const segment2 = (primaryColor: string, score: number) => {
  if (score >= 4) {
    return primaryColor;
  }

  return segmentColorDefault;
};

const segment3 = (primaryColor: string, score: number) => {
  if (score >= 4) {
    return primaryColor;
  } else if (score >= 3) {
    return segmentColorUnstable;
  }

  return segmentColorDefault;
};

const segment4 = (primaryColor: string, score: number) => {
  if (score >= 4) {
    return primaryColor;
  } else if (score >= 2) {
    return segmentColorUnstable;
  }

  return segmentColorDefault;
};

const segment5 = (primaryColor: string, score: number) => {
  if (score >= 4) {
    return primaryColor;
  } else if (score >= 2) {
    return segmentColorUnstable;
  } else if (score === 1) {
    return segmentColorVeryBad;
  }

  return segmentColorDefault;
};

export const styledResults = ({ theme, score }: IStyledResults) => `
  position: relative;

  > label {
    display: block;
    font-size: 14px;
    color: ${theme.primaryColor};
  }

  > h1 {
    width: 275px;
    font-size: 32px; 
    font-weight: bold;
    display: inline-block;
    color: ${theme.primaryTextColor};
    margin: 20px 0 10px 0;
  }

  > .descr {
    color: #faf9f5;
    font-size: 12px;
    height: 40px;
    width: 216px;
    opacity: 0.8;
  }

  > .score-value {
    top: 100px;
    right: 80px;
    width: 200px;
    position: absolute;
    text-align: center;

    > span:first-child {
      font-size: 36px;
      font-weight: bold;
      letter-spacing: 2px;
      color: ${theme.primaryTextColor};
    }

    > span:last-child {
      font-size: 22px;
      color: ${segmentColorDefault};
    }
  }

  > svg {
    top: 0;
    right: 80px;
    position: absolute;

    use:nth-child(1) {
      fill: ${segment1(theme.primaryColor, score)};
    }

    use:nth-child(2) {
      fill: ${segment2(theme.primaryColor, score)};
    }

    use:nth-child(3) {
      fill: ${segment3(theme.primaryColor, score)};
    }

    use:nth-child(4) {
      fill: ${segment4(theme.primaryColor, score)};
    }

    use:nth-child(5) {
      fill: ${segment5(theme.primaryColor, score)};
    }
  }
`;

export const styledRunTestContainer = ({ theme }: IRequiredTheme) => `
  display: flex;
  width: 100%;
  align-items: center;
  vertical-align: center;

  > span {
    margin-left: 16px;
    font-size: 14px;
    color: ${theme.buttonWarningColor};
  }
`;

export const styledRTCOptionBlock = () => `
  width: 100%;
  margin-top: 12px;
`;

const commonOptionsBlock = () => `
  font-size: 14px;
  padding: 10px 15px;
`;

export enum TestResult {
  failed = '0',
  passed = '1',
  inProgress = '3',
}

export interface IRTCOptionBlockTitle extends IRequiredTheme {
  result: TestResult;
}

const spinnerForRTCOptionBlockTitleIcon = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const styledRTCOptionBlockTitleIcon = ({ theme, result }: IRTCOptionBlockTitle) => {
  const icons = clientEntityProvider.getIcons();

  if (result === TestResult.inProgress) {
    return css`
      font-size: 10px;
      margin: auto;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
      position: relative;
      animation: ${spinnerForRTCOptionBlockTitleIcon} 1.4s infinite linear;
      transform: translateZ(0);
      transform-origin: 10px 10px 0;

      &:before {
        width: 50%;
        height: 50%;
        background: #ffffff;
        border-radius: 100% 0 0 0;
        position: absolute;
        top: 0;
        left: 0;
        content: '';
      }
      &:after {
        background: ${theme.primaryColor};
        width: 75%;
        height: 75%;
        border-radius: 50%;
        content: '';
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
      }
    `;
  } else if (result === TestResult.failed) {
    return `
      width: 20px;
      height: 20px;
      position: relative;
      top: -4px;
      left: 10px;

      &:before {
        font-size: 20px;
        font-weight: bold;
        color: ${theme.primaryTextColor};
        content: '!';
        font-family: Arial;
      }
    `;
  }

  return `
    width: 20px;
    height: 20px;
    position: relative;
    top: -6px;

    &:before {
      font-size: 24px;
      color: ${theme.primaryTextColor};
      content: ${icons.code.checkMark};
      font-family: ${icons.fontFamily};
    }
  `;
};

export const styledRTCOptionBlockTitle = ({ theme, result }: IRTCOptionBlockTitle) => css`
  ${commonOptionsBlock()};
  color: ${theme.primaryTextColor};
  background-color: ${result === TestResult.failed ? theme.buttonWarningColor : theme.primaryColor};

  > span {
    float: right;
    ${styledRTCOptionBlockTitleIcon({ theme, result })};
  }
`;

export const styledRTCOptionBlockDescription = ({ theme }: IRequiredTheme) => `
  ${commonOptionsBlock()};
  color: ${theme.primaryTextColor};
  background-color: ${theme.backgroundColorThird};
`;
