import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  styledContainer,
  IStyledContainer,
  styledInto,
  styledResults,
  IStyledResults,
  styledRunTestContainer,
  styledRTCOptionBlock,
  styledRTCOptionBlockTitle,
  styledRTCOptionBlockDescription,
  TestResult,
  IRTCOptionBlockTitle,
} from './Connection.style';
import { actionCreators } from '../../../../common/redux/actionCreators';
import { IWebrtcTestCallConfig } from '../../../../../../ioc/client/types/interfaces';

const TEST_TIMEOUT = 7000;

const ConnectionContainer = styled.div<IStyledContainer>`
  ${styledContainer};
`;
const ConnectionTestIntro = styled.div`
  ${styledInto};
`;
const OptionsBlock = styled.div`
  ${styledRTCOptionBlock};
`;
const OptionsBlockTitle = styled.div<IRTCOptionBlockTitle>`
  ${styledRTCOptionBlockTitle};
`;
const OptionsBlockDescr = styled.div`
  ${styledRTCOptionBlockDescription};
`;
const ConnectionTestResults = styled.div<IStyledResults>`
  ${styledResults};
`;
const RunTestContainer = styled.div`
  ${styledRunTestContainer};
`;

const BANDWIDTH_URL = 'http://speedtest.net';

const RESUL_IN_PROGRESS = `${TestResult.inProgress}${TestResult.inProgress}`;

export interface ITestRTCConnectionState {
  result: number;
  progress: boolean;
}

export interface ITestRTCConnectionOwnProps {
  description?: React.ReactNode;
}

export interface ITestRTCConnectionDispatchProps {
  userSetupToggleDisable: (value: boolean) => void;
  setConnectionQuality: (value: number) => void;
  testStart: (config: IWebrtcTestCallConfig) => void;
  testStop: (byUser: boolean) => void;
}

export type ITestRTCConnectionProps = ITestRTCConnectionOwnProps & ITestRTCConnectionDispatchProps & {};

export class SetupConnection extends React.Component<
  ITestRTCConnectionOwnProps & ITestRTCConnectionProps,
  ITestRTCConnectionState
> {
  public static Title = '';

  private _testTimeout: number = NaN;
  private _hasExtrenalContainer: boolean = false;

  private _stopByTimeout = () => {
    this._showResults(0);
  };

  private _showResults = (result: number) => {
    this._testStop();

    this.setState({
      result,
      progress: false,
    });
  };

  private _testStart = () => {
    this.props.testStart({
      audioOnly: true,
      duration: 1,
      interval: 1,
      callback: this._showResults,
    });
    this.setState({
      result: NaN,
      progress: true,
    });
    this.props.setConnectionQuality(NaN);
    this.props.userSetupToggleDisable(true);
    this._testTimeout = window.setTimeout(this._stopByTimeout, TEST_TIMEOUT);
  };

  private _testStop = (byUser: boolean = false) => {
    window.clearTimeout(this._testTimeout);
    this.props.userSetupToggleDisable(false);
    this.setState({ progress: false });
    this.props.testStop(byUser);
  };

  private _getResults(score: number) {
    const results = (isNaN(score) ? RESUL_IN_PROGRESS : score.toString(2)).split('');

    if (results.length === 1) {
      results.unshift('0');
    }

    return {
      udp: results[0] as TestResult,
      tcp: results[1] as TestResult,
    };
  }

  private _optionDescrText(result: TestResult, type: string): string {
    if (result === TestResult.inProgress) {
      return `Verifies ${type.toUpperCase()} connectivity from your browser to Set2Meet TURN servers.`;
    }

    if (result === TestResult.failed) {
      return `
        Could not establish ${type.toUpperCase()} connection to Set2Meet TURN servers.
      `;
    }

    return `${type.toUpperCase()} enabled.`;
  }

  constructor(props: ITestRTCConnectionProps) {
    super(props);

    this.state = {
      result: NaN,
      progress: false,
    };

    this._hasExtrenalContainer = !!this.props.description;
  }

  public componentWillUnmount() {
    if (this.state.progress) {
      this._testStop(true);
    }
  }

  private _renderRunTestButton(title: string) {
    const { Button } = clientEntityProvider.getComponentProvider().UI;

    return (
      <RunTestContainer>
        <Button title={title} bsStyle="info" onClick={this._testStart} disabled={this.state.progress} />
      </RunTestContainer>
    );
  }

  private get _description() {
    return (
      <>
        <ConnectionTestIntro>
          <label>Set2Meet WebRTC Diagnostics</label>
          <p>Check your browser and network environment to ensure you can use Set2Meet Virtual Room.</p>
          <ul>
            <li>Make sure you are on the specific network you intend to test.</li>
            <li>Make sure that the right camera and mic are selected.</li>
            <li>
              Run a bandwidth test at{' '}
              <a href={BANDWIDTH_URL} target="_blank">
                {BANDWIDTH_URL}
              </a>
              . Make sure that the bandwidth is over 350kbps upload and download.
            </li>
          </ul>
        </ConnectionTestIntro>
      </>
    );
  }

  private _renderResults() {
    const { result } = this.state;
    const { udp, tcp } = this._getResults(result);

    return (
      <ConnectionContainer hasExternalContainer={this._hasExtrenalContainer}>
        <ConnectionTestResults score={0}>
          <label>Your network test result</label>
          <OptionsBlock>
            <OptionsBlockTitle result={udp}>
              TURN UDP Connectivity
              <span />
            </OptionsBlockTitle>
            <OptionsBlockDescr>{this._optionDescrText(udp, 'udp')}</OptionsBlockDescr>
          </OptionsBlock>
          <OptionsBlock>
            <OptionsBlockTitle result={tcp}>
              TURN TCP Connectivity
              <span />
            </OptionsBlockTitle>
            <OptionsBlockDescr>{this._optionDescrText(tcp, 'tcp')}</OptionsBlockDescr>
          </OptionsBlock>
          <hr className="bttn-run-test-splitter v2" />
          {this._renderRunTestButton('TEST AGAIN')}
        </ConnectionTestResults>
      </ConnectionContainer>
    );
  }

  private _renderInto() {
    return (
      <ConnectionContainer hasExternalContainer={this._hasExtrenalContainer}>
        {this.props.description || this._description}
        <hr className="bttn-run-test-splitter v1" />
        {this._renderRunTestButton('RUN TEST')}
      </ConnectionContainer>
    );
  }

  public render() {
    const result = !isNaN(this.state.result);

    if (result || this.state.progress) {
      return this._renderResults();
    }

    return this._renderInto();
  }
}

export default connect<ITestRTCConnectionOwnProps, ITestRTCConnectionDispatchProps, ITestRTCConnectionOwnProps>(
  (state: any, ownProps: ITestRTCConnectionOwnProps) => ({
    ...ownProps,
  }),
  {
    userSetupToggleDisable: actionCreators.webphoneUserSetupToggleDisable,
    setConnectionQuality: actionCreators.webphoneSetConnectionQuality,
    testStart: actionCreators.webphoneTestCallStart,
    testStop: actionCreators.webphoneTestCallStop,
  }
)(SetupConnection);
