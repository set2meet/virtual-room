import * as React from 'react';
import { connect } from 'react-redux';
import MSConnection from './MSConnection';
import RTCConnection from './RTCConnection';
import { IStoreState } from '../../../../../../ioc/client/types/interfaces';

export interface ISetupConnectionOwnProps {
  description?: React.ReactNode;
}

export interface ISetupConnectionStateProps {
  ownProps: ISetupConnectionOwnProps;
  webrtcServiceName: string;
}

export type ISetupConnectionProps = ISetupConnectionStateProps & {} & {};

export class SetupConnection extends React.Component<ISetupConnectionOwnProps & ISetupConnectionProps> {
  public static Title = '';

  public render() {
    const TestConnection = this.props.webrtcServiceName === '@PEER_TO_PEER' ? RTCConnection : MSConnection;

    return <TestConnection {...this.props.ownProps} />;
  }
}

export default connect<ISetupConnectionStateProps, {}, ISetupConnectionOwnProps>(
  (state: IStoreState, ownProps: ISetupConnectionOwnProps) => ({
    ownProps,
    webrtcServiceName: state.webphone.webrtcServiceName,
  })
)(SetupConnection);
