import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Checkbox, { IWebPhoneUserSetupCheckboxProps } from '../Checkbox';
import TestStream, { AV } from './utils/TestStream';
import {
  styledContainer,
  IStyledContainer,
  styledInto,
  styledIntroCheckbox,
  styledProgress,
  IStyledProgress,
  styledResults,
  IStyledResults,
  styledRunTestContainer,
} from './Connection.style';
import { actionCreators } from '../../../../common/redux/actionCreators';
import { IWebrtcTestCallConfig } from '../../../../../../ioc/client/types/interfaces';
import { IWebPhoneConnectionQualityState } from '../../../redux/types/IWebPhoneReduxState';

const ConnectionContainer = styled.div<IStyledContainer>`
  ${styledContainer};
`;
const ConnectionTestIntro = styled.div`
  ${styledInto};
`;
const ConnectionTestProgress = styled.div<IStyledProgress>`
  ${styledProgress};
`;
const ConnectionTestResults = styled.div<IStyledResults>`
  ${styledResults};
`;
const CheckboxAudioOnly = styled(Checkbox)<IWebPhoneUserSetupCheckboxProps>`
  ${styledIntroCheckbox};
`;
const RunTestContainer = styled.div`
  ${styledRunTestContainer};
`;

const TEST_CALL_CONFIG = {
  interval: 1000,
  audioOnlyDuration: 10000,
  videoAndAudioDuration: 20000,
};
const MAX_PROGRESS_STEPS = 100;
const BANDWIDTH_URL = 'http://speedtest.net';
const DEFAULT_DESCR = 'We recommend switching off your video or trying a different network';
const METER_RADIUS_1 = 88;
const METER_RADIUS_2 = 76;
// tslint:disable-next-line:no-magic-numbers
const METER_ANGLE = Math.PI / 5 - 0.02;
const METER_PATH = `
  M ${METER_RADIUS_2} ${METER_RADIUS_1}
  L ${METER_RADIUS_1} ${METER_RADIUS_1}
  A ${METER_RADIUS_1} ${METER_RADIUS_1} 1 0 0 ${METER_RADIUS_1 * Math.cos(METER_ANGLE)} ${
  METER_RADIUS_1 * (1 - Math.sin(METER_ANGLE))
}
  L ${METER_RADIUS_2 * Math.cos(METER_ANGLE)} ${METER_RADIUS_1 - METER_RADIUS_2 * Math.sin(METER_ANGLE)}
  A ${METER_RADIUS_2} ${METER_RADIUS_2} 0 0 1 ${METER_RADIUS_2} ${METER_RADIUS_1}
`.replace(/\n/g, '');

const ResultsTitle: Record<number, string> = {
  0: 'No Connection',
  1: 'Low and unstable connection',
  2: 'Fast yet unstable connection',
  3: 'Stable yet slow connection',
  4: 'Fine connection quality',
  5: 'Perfect connection quality',
};

const ResultsDescription: Record<number, string> = {
  0: 'Please check your internet connection and try again',
  1: DEFAULT_DESCR,
  2: DEFAULT_DESCR,
  3: DEFAULT_DESCR,
  4: '',
  5: '',
};

export interface ISetupConnectionState {
  showIntroAnyWay: boolean;
  hasAudioDevice: boolean;
  hasVideoDevice: boolean;
  audioOnly: boolean;
  progress: boolean;
}
export interface ISetupConnectionOwnProps {
  description?: React.ReactNode;
}
export interface ISetupConnectionStateProps extends IWebPhoneConnectionQualityState {
  testCallInProgress: boolean;
}
export interface ISetupConnectionDispatchProps {
  userSetupToggleDisable: (value: boolean) => void;
  setConnectionQuality: (value: number) => void;
  testStart: (config: IWebrtcTestCallConfig) => void;
  testStop: (byUser: boolean) => void;
}
export type ISetupConnectionProps = ISetupConnectionStateProps & ISetupConnectionDispatchProps & {};

export class SetupConnection extends React.Component<
  ISetupConnectionOwnProps & ISetupConnectionProps,
  ISetupConnectionState
> {
  public static Title = '';

  private _timer: number = NaN;
  private _config: IWebrtcTestCallConfig;
  private _hasExtrenalContainer: boolean = false;
  private _testStream: TestStream = new TestStream();
  private _progressSpan: HTMLSpanElement = null;

  private _setRefProgress = (ref?: HTMLSpanElement) => {
    if (ref && !this._progressSpan) {
      this._progressSpan = ref;
    }
  };

  private _toggleAudioOnly = () => {
    this.setState({ audioOnly: !this.state.audioOnly });
  };

  private _processTestProgress = () => {
    const interval = Math.round(this._config.duration / MAX_PROGRESS_STEPS);
    let step = 0;

    this._timer = window.setInterval(() => {
      if (step++ === MAX_PROGRESS_STEPS) {
        return this._testStop();
      }

      if (this.props.testCallInProgress && this._progressSpan) {
        this._progressSpan.textContent = step.toFixed(0) + '%';
      } else {
        this._progressSpan.textContent = '';
      }
    }, interval);
  };

  private _testStart = () => {
    const { audioOnly } = this.state;
    const { interval, audioOnlyDuration, videoAndAudioDuration } = TEST_CALL_CONFIG;
    const duration = audioOnly ? audioOnlyDuration : videoAndAudioDuration;

    this._config = {
      audioOnly,
      interval,
      duration,
    };

    this._testStream
      .start()
      .then(({ audioTrack, videoTrack }) => {
        this.props.testStart({
          ...this._config,
          audioTrack,
          videoTrack,
        });
      })
      .catch(() => {
        this._testStop(true);
      });

    this.setState({
      progress: true,
      showIntroAnyWay: false,
    });
    this.props.setConnectionQuality(NaN);
    this.props.userSetupToggleDisable(true);
  };

  private _testCancel = () => {
    this._testStop(true);
  };

  private _testStop = (byUser: boolean = false) => {
    this.props.userSetupToggleDisable(false);
    window.clearInterval(this._timer);
    this.setState({ progress: false });
    this.props.testStop(byUser);
    this._testStream.stop();
  };

  private _catchedError() {
    this.props.userSetupToggleDisable(false);
    window.clearInterval(this._timer);
    this.setState({ progress: false });
    this._testStream.stop();
  }

  private _updateDevicesState = (devices: AV) => {
    this.setState({
      hasAudioDevice: devices.audio,
      hasVideoDevice: devices.video,
    });
  };

  private get cantRunTest() {
    return !(this.state.hasAudioDevice || this.state.hasVideoDevice);
  }

  constructor(props: ISetupConnectionProps) {
    super(props);

    this.state = {
      showIntroAnyWay: !!this.props.description,
      hasAudioDevice: false,
      hasVideoDevice: false,
      audioOnly: false,
      progress: false,
    };

    this._hasExtrenalContainer = !!this.props.description;
    this._testStream.ready().then(this._updateDevicesState);
    this._testStream.onChangeDevices(this._updateDevicesState);
  }

  public componentDidUpdate(prevProps: ISetupConnectionProps) {
    if (isNaN(prevProps.score) && this.props.score === 0) {
      this._catchedError();
    }
    if (!prevProps.testCallInProgress && this.props.testCallInProgress) {
      this._processTestProgress();
    }
  }

  public componentWillUnmount() {
    if (this.state.progress) {
      this._testStop(true);
    }
  }

  private _renderRunTestButton(title: string) {
    const { Button } = clientEntityProvider.getComponentProvider().UI;

    return (
      <RunTestContainer>
        <Button title={title} bsStyle="info" onClick={this._testStart} disabled={this.cantRunTest} />
        {this.cantRunTest && <span>No devices found to run test</span>}
      </RunTestContainer>
    );
  }

  private get _description() {
    return (
      <React.Fragment>
        <ConnectionTestIntro>
          <label>Before it starts</label>
          <ul>
            <li>Make sure you are on the specific network you intend to test.</li>
            <li>Make sure that the right camera and mic are selected.</li>
            <li>
              Run a bandwidth test at{' '}
              <a href={BANDWIDTH_URL} target="_blank">
                {BANDWIDTH_URL}
              </a>
              . Make sure that the bandwidth is over 350kbps upload and download.
            </li>
          </ul>
        </ConnectionTestIntro>
      </React.Fragment>
    );
  }

  private _renderCheckBoxAudioOnly(shiftedDown: boolean = true) {
    const { hasAudioDevice, hasVideoDevice } = this.state;
    const hasAll = hasAudioDevice && hasVideoDevice;
    const hasAny = hasAudioDevice || hasVideoDevice;
    const noOne = !hasAny;
    const value = hasAll ? this.state.audioOnly : noOne || hasAudioDevice ? false : true;
    const className = shiftedDown ? 'shifted-down' : '';

    // disabled if only one stream avaliable
    return (
      <CheckboxAudioOnly
        value={value}
        label="Audio only"
        className={className}
        disabled={!hasAll && hasAny}
        onChange={this._toggleAudioOnly}
      />
    );
  }

  private _renderTestProgress() {
    const { testCallInProgress } = this.props;
    const { Button } = clientEntityProvider.getComponentProvider().UI;

    return (
      <ConnectionContainer hasExternalContainer={this._hasExtrenalContainer}>
        <ConnectionTestProgress progress={testCallInProgress} duration={this._config.duration}>
          <div className="title">
            <span>{testCallInProgress ? 'Checking...' : 'Starting the call...'}</span>
            <span ref={this._setRefProgress} />
          </div>
          <div className="progress" />
        </ConnectionTestProgress>
        <br />
        <Button title="CANCEL" bsStyle="info" style={{ width: '100px' }} onClick={this._testCancel} />
      </ConnectionContainer>
    );
  }

  private _renderResults() {
    const { score } = this.props;

    return (
      <ConnectionContainer hasExternalContainer={this._hasExtrenalContainer}>
        <ConnectionTestResults score={score}>
          <label>Your test results</label>
          <h1>{ResultsTitle[score]}</h1>
          <div className="descr">{ResultsDescription[score]}</div>
          <div className="score-value">
            <span>{score}</span>
            <span>{'/5'}</span>
          </div>
          <svg
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            width="200px"
            height="200px"
            xmlnsXlink="http://www.w3.org/1999/xlink"
          >
            <defs>
              <path id="-segment" stroke="none" d={METER_PATH} />
            </defs>
            <g transform="translate(100, 50)">
              <use x="0" y="0" xlinkHref="#-segment" />
              <use x="0" y="0" xlinkHref="#-segment" transform="rotate(-36, 0, 88)" />
              <use x="0" y="0" xlinkHref="#-segment" transform="rotate(-72, 0, 88)" />
              <use x="0" y="0" xlinkHref="#-segment" transform="rotate(-108, 0, 88)" />
              <use x="0" y="0" xlinkHref="#-segment" transform="rotate(-144, 0, 88)" />
            </g>
          </svg>
          {this._renderCheckBoxAudioOnly()}
          <hr className="bttn-run-test-splitter v2" />
          {this._renderRunTestButton('TEST AGAIN')}
        </ConnectionTestResults>
      </ConnectionContainer>
    );
  }

  private _renderInto() {
    return (
      <ConnectionContainer hasExternalContainer={this._hasExtrenalContainer}>
        {this.props.description || this._description}
        {this._renderCheckBoxAudioOnly(!this._hasExtrenalContainer)}
        <hr className="bttn-run-test-splitter v1" />
        {this._renderRunTestButton('RUN TEST')}
      </ConnectionContainer>
    );
  }

  public render() {
    const calculatedMOS = !isNaN(this.props.score);
    const checkProgress = this.state.progress;
    const { showIntroAnyWay } = this.state;

    if (checkProgress) {
      return this._renderTestProgress();
    }

    if (calculatedMOS && !showIntroAnyWay) {
      return this._renderResults();
    }

    return this._renderInto();
  }
}

export default connect<ISetupConnectionStateProps, ISetupConnectionDispatchProps, ISetupConnectionOwnProps>(
  (state: any, ownProps: ISetupConnectionOwnProps) => ({
    ...ownProps,
    ...state.webphone.connectionQuality,
    testCallInProgress: state.webphone.userSetup.testCallInProgress,
  }),
  {
    userSetupToggleDisable: actionCreators.webphoneUserSetupToggleDisable,
    setConnectionQuality: actionCreators.webphoneSetConnectionQuality,
    testStart: actionCreators.webphoneTestCallStart,
    testStop: actionCreators.webphoneTestCallStop,
  }
)(SetupConnection);
