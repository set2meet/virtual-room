/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

/**
 * This is a virtual audio stream
 * created via a web audio context
 * for testing the network,
 * regardless of the existence of real the device
 */

type OscillatorSettings = {
  modGain: number;
  modFreq: number;
  oscFreq: number;
};

class AudioNoise {
  private osc: OscillatorNode;
  private mod: OscillatorNode;
  private modGain: GainNode;
  private settings: OscillatorSettings;
  private dest: any;

  constructor(modFreq: number = 2, modGain: number = 40, oscFreq: number = 600) {
    this.settings = { modFreq, modGain, oscFreq };
  }

  public start() {
    const { modFreq, modGain, oscFreq } = this.settings;
    const ctx = clientEntityProvider.getWebAudioContext();

    this.dest = ctx.createMediaStreamDestination();

    this.mod = ctx.createOscillator();
    this.mod.frequency.value = modFreq;

    this.modGain = ctx.createGain();
    this.modGain.gain.value = modGain;

    this.osc = ctx.createOscillator();
    this.osc.frequency.value = oscFreq;

    this.mod.connect(this.modGain);
    this.modGain.connect(this.osc.frequency);
    this.osc.connect(this.dest as any);

    this.osc.start(0);
    this.mod.start(0);
  }

  public stop() {
    this.osc.stop(0);
    this.mod.stop(0);
    this.osc = null;
    this.mod = null;
    this.modGain = null;
  }

  public getAudioStreamTrack(): MediaStreamTrack {
    return this.dest.stream.getAudioTracks()[0];
  }
}

export default AudioNoise;
