/**
 * This is a virtual video stream
 * created via a canvas 2d context
 * with animated diagonal gradient
 * for testing the network,
 * regardless of the existence of real the device
 */

type RGBColor = {
  r: number;
  g: number;
  b: number;
};
type ColorStop = {
  pos: number;
  colors: RGBColor[];
  currColor: string; // rgb(#r, #g, #b)
};
type GradientSettings = {
  stopAColor: RGBColor[];
  stopBColor: RGBColor[];
};
type AnimationSettings = {
  duration: number;
  interval: number;
  stepUnit: number;
  currUnit: number;
};

// linear color interpolation
const lerp = (start: number, end: number, val: number): number => {
  return (1 - val) * start + val * end;
};

class CanvasGradient {
  private ctx: CanvasRenderingContext2D;
  private width: number;
  private height: number;
  private currentStop: number;
  private colorStops: ColorStop[];
  // tslint:disable
  private grSettings: GradientSettings = {
    stopAColor: [
      { r: 9, g: 117, b: 190 }, // blue
      { r: 59, g: 160, b: 89 }, // green
      { r: 230, g: 192, b: 39 }, // yellow
      { r: 238, g: 30, b: 77 }, // red
    ],
    stopBColor: [
      { r: 205, g: 24, b: 75 }, // pink
      { r: 33, g: 98, b: 155 }, // blue
      { r: 64, g: 149, b: 69 }, // green
      { r: 228, g: 171, b: 33 }, // yellow
    ],
  };
  private animSettings: AnimationSettings = {
    duration: 2000, // ms
    interval: 20,
    stepUnit: 1.0,
    currUnit: 0.0,
  };
  // tslint:enable

  constructor(context: CanvasRenderingContext2D, width: number, height: number) {
    this.ctx = context;
    this.width = width;
    this.height = height;
    this.colorStops = [];
    this.currentStop = 0;
    this.addStop(0, this.grSettings.stopAColor);
    this.addStop(1, this.grSettings.stopBColor);
  }

  public addStop(pos: number, colors: RGBColor[]) {
    this.colorStops.push({ pos, colors, currColor: null });
  }

  public calcStopColors() {
    const anim = this.animSettings;
    const steps = anim.duration / anim.interval;
    const stepU = anim.stepUnit / steps;
    const posLen = this.colorStops.length;
    const stopsLen = this.colorStops[0].colors.length - 1;
    const { currUnit } = anim;

    // cycle through all stops in gradient
    for (let i = 0; i < posLen; i++) {
      // tslint:disable-next-line:one-variable-per-declaration
      let endColor, r, g, b;
      const stop = this.colorStops[i];
      const startColor = stop.colors[this.currentStop]; // get stop 1 color

      // get stop 2 color, go to first if at last stop

      if (this.currentStop < stopsLen) {
        endColor = stop.colors[this.currentStop + 1];
      } else {
        endColor = stop.colors[0];
      }

      // interpolate both stop 1&2 colors to get new color based on animaiton unit
      r = Math.floor(lerp(startColor.r, endColor.r, currUnit));
      g = Math.floor(lerp(startColor.g, endColor.g, currUnit));
      b = Math.floor(lerp(startColor.b, endColor.b, currUnit));

      stop.currColor = `rgb(${r},${g},${b})`;
    }

    // update current stop and animation units if interpolaiton is complete
    if (currUnit >= 1.0) {
      anim.currUnit = 0;

      if (this.currentStop < stopsLen) {
        this.currentStop++;
      } else {
        this.currentStop = 0;
      }
    }

    // increment animation unit
    anim.currUnit += stepU;
  }

  public reDraw() {
    const gr = this.ctx.createLinearGradient(0, this.width, this.height, 0);
    const posLen = this.colorStops.length;

    for (let i = 0; i < posLen; i++) {
      const stop = this.colorStops[i];
      const pos = stop.pos;
      const color = stop.currColor;

      gr.addColorStop(pos, color);
    }

    this.ctx.clearRect(0, 0, this.width, this.height);
    this.ctx.fillStyle = gr;
    this.ctx.fillRect(0, 0, this.width, this.height);
  }
}

class VideoNoise {
  private canvas: HTMLCanvasElement = document.createElement('canvas');
  private ctx: CanvasRenderingContext2D = this.canvas.getContext('2d');
  private isEnabled: boolean = false;
  private gradient: CanvasGradient;
  private fps: number;
  private lastTimeDraw = 0;
  private defaultTimeout = 50;

  constructor(fps: number = 24, width: number = 640, height: number = 480) {
    this.fps = fps;
    this.canvas.width = width;
    this.canvas.height = height;
    this.gradient = new CanvasGradient(this.ctx, width, height);
  }

  private nextFrame = () => {
    setTimeout(() => requestAnimationFrame(this.animate), 0);
  };

  private animate = (): void => {
    if (this.isEnabled) {
      const now = Date.now();

      if (now - this.lastTimeDraw < this.defaultTimeout) {
        return this.nextFrame();
      }

      this.gradient.calcStopColors();
      this.gradient.reDraw();
      this.nextFrame();
      this.lastTimeDraw = now;
    }
  };

  public start(): void {
    this.isEnabled = true;
    this.animate();
  }

  public stop(): void {
    this.isEnabled = false;
  }

  public getVideoStreamTrack(): MediaStreamTrack {
    const stream: MediaStream = (this.canvas as any).captureStream(this.fps);

    return stream.getVideoTracks()[0];
  }
}

export default VideoNoise;
