/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _debounce from 'lodash/debounce';
import AudioNoise from './NoiseAudio';
import VideoNoise from './NoiseVideo';
import { detect } from 'detect-browser';

const browser: any = detect();
const DEVICES_TIMEOUT = 500;
const DEFAULT_CONSTRAINTS = { audio: true, video: true };
const AudioContext = (window as any).AudioContext || (window as any).webkitAudioContext;
const isSupportNativeStreamCreation =
  // safari crashes on virtual video stream with canvas
  // crash not constantly but often, just block it
  browser.name !== 'safari' &&
  !!(HTMLCanvasElement as any).prototype.captureStream &&
  !!AudioContext.prototype.createMediaStreamDestination;
const stopTrack = (track: MediaStreamTrack) => track.stop();

export type AV = {
  audio: boolean;
  video: boolean;
};
export type AVTracks = {
  audioTrack?: MediaStreamTrack;
  videoTrack?: MediaStreamTrack;
};
export type AVListener = (devices: AV) => void;

const getAV: () => Promise<AV> = () =>
  navigator.mediaDevices
    .enumerateDevices()
    .then((devices) => {
      return devices.reduce(
        (result: AV, item: MediaDeviceInfo) => {
          return {
            audio: result.audio || item.kind === 'audioinput',
            video: result.video || item.kind === 'videoinput',
          };
        },
        { audio: false, video: false }
      );
    })
    .catch((err) => {
      return { audio: false, video: false };
    });

export default class TestStream {
  private _listeners: AVListener[] = [];
  private _audioNoise: AudioNoise;
  private _videoNoise: VideoNoise;
  private _stream: MediaStream;
  public ready: () => Promise<AV>;

  constructor() {
    if (isSupportNativeStreamCreation) {
      this._audioNoise = new AudioNoise();
      this._videoNoise = new VideoNoise();

      this.ready = () => Promise.resolve(DEFAULT_CONSTRAINTS);
    } else {
      this.ready = () => getAV();

      // in this case - there can be changed existance of devices
      navigator.mediaDevices.addEventListener(
        'devicechange',
        _debounce(() => {
          getAV().then((devices) => this._listeners.forEach((fn) => fn(devices)));
        }, DEVICES_TIMEOUT)
      );
    }
  }

  public start(): Promise<AVTracks> {
    if (isSupportNativeStreamCreation) {
      this._audioNoise.start();
      this._videoNoise.start();

      return Promise.resolve({
        audioTrack: this._audioNoise.getAudioStreamTrack(),
        videoTrack: this._videoNoise.getVideoStreamTrack(),
      });
    }

    return navigator.mediaDevices.getUserMedia(DEFAULT_CONSTRAINTS).then((stream) => {
      this._stream = stream;

      return {
        audioTrack: stream.getAudioTracks()[0] || null,
        videoTrack: stream.getVideoTracks()[0] || null,
      };
    });
  }

  public stop() {
    if (isSupportNativeStreamCreation) {
      this._audioNoise.stop();
      this._videoNoise.stop();

      return;
    }

    this._stream.getTracks().forEach(stopTrack);
    this._stream = null;
  }

  public onChangeDevices(fn: AVListener) {
    if (!isSupportNativeStreamCreation) {
      this._listeners.push(fn);
    }
  }
}
