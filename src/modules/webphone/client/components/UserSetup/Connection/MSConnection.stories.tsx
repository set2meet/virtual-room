import React from 'react';
import MSConnection from './MSConnection';
import { setConnectionQualityAction, TestCallToggleProgressAction } from '../UserSetup.stories';
import { text } from '@storybook/addon-knobs';
import withStoryRootIndicator from '../../../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

export default {
  title: 'webphone/UserSetup/MSConnection',
  component: MSConnection,
  excludeStories: /.*Action$/,
  parameters: {
    redux: {
      enable: true,
      actionsFactory: () => [
        // tslint:disable-next-line:no-magic-numbers
        setConnectionQualityAction(0),
        // tslint:disable-next-line:no-magic-numbers
        setConnectionQualityAction(2),
        // tslint:disable-next-line:no-magic-numbers
        setConnectionQualityAction(4),
        TestCallToggleProgressAction(true),
        TestCallToggleProgressAction(false),
      ],
    },
  },
  decorators: [withStoryRootIndicator, withModule(ModuleRegistryKey.Webphone)],
};

export const Default = () => <MSConnection />;

export const WithCustomDescription = () => {
  return <MSConnection description={<>{text('Description', 'Custom description')}</>} />;
};
