import React from 'react';
import RTCConnection from './RTCConnection';
import { setConnectionQualityAction } from '../UserSetup.stories';
import { text } from '@storybook/addon-knobs';
import withStoryRootIndicator from '../../../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

export default {
  title: 'webphone/UserSetup/RTCConnection',
  component: RTCConnection,
  excludeStories: /.*Action$/,
  parameters: {
    redux: {
      enable: true,
      // tslint:disable-next-line:no-magic-numbers
      actionsFactory: () => [setConnectionQualityAction(0), setConnectionQualityAction(2)],
    },
  },
  decorators: [withStoryRootIndicator, withModule(ModuleRegistryKey.Webphone)],
};

export const Default = () => <RTCConnection />;

export const WithCustomDescription = () => {
  return <RTCConnection description={<>{text('Description', 'Custom description')}</>} />;
};
