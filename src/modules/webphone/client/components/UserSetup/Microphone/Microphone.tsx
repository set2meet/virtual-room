import * as React from 'react';
import { connect } from 'react-redux';
import { actionCreators } from '../../../../common/redux/actionCreators';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  Row,
  RowDevice,
  RowHeader,
  RowDescription,
  RowDeviceLabel,
  RowDeviceChoice,
  RowDeviceExample,
} from '../common.style';

export interface ISetupMicrophoneStateProps {
  stream?: MediaStream;
}

export interface ISetupMicrophoneDispatchProps {
  requestStreamFromMicrophone: () => void;
}

export type ISetupMicrophoneProps = ISetupMicrophoneStateProps & ISetupMicrophoneDispatchProps;

export class SetupMicrophone extends React.Component<ISetupMicrophoneProps> {
  public componentDidMount() {
    this.props.requestStreamFromMicrophone();
  }

  public render() {
    const {
      MediaDeviceSelectAudioInput,
      MediaDeviceTestMicrophone,
    } = clientEntityProvider.getComponentProvider().Media;

    return (
      <Row>
        <RowHeader>2. Microphone</RowHeader>
        <RowDescription>Say something in the microphone to check that it is working.</RowDescription>
        <RowDevice>
          <RowDeviceLabel>Microphone</RowDeviceLabel>
          <RowDeviceExample>
            <MediaDeviceTestMicrophone stream={this.props.stream} />
          </RowDeviceExample>
          <RowDeviceChoice>
            <MediaDeviceSelectAudioInput requestPermissions={this.props.requestStreamFromMicrophone} />
          </RowDeviceChoice>
        </RowDevice>
      </Row>
    );
  }
}

export default connect<ISetupMicrophoneStateProps, ISetupMicrophoneDispatchProps>(
  (state: any) => ({
    stream: state.webphone.userSetup.microphoneStream,
  }),
  {
    requestStreamFromMicrophone: actionCreators.webphoneRequestStreamFromMicrophone,
  }
)(SetupMicrophone);
