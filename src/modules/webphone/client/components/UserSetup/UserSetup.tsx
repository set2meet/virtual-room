import * as React from 'react';
import { connect } from 'react-redux';
import { actionCreators } from '../../../common/redux/actionCreators';
import Devices from './Devices';
import Connection from './Connection';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Checkbox from './Checkbox';
import styled from 'styled-components';
import {
  styledUserSetupContainer,
  styledUserSetupContent,
  styledUserSetupHeader,
  styledUserSetupFooter,
  styledSkipButton,
  styledButtonChangeTab,
} from './UserSetup.style';
import { IUIWebPhoneUserSetup, IWebPhoneUserSetupState, IWebPhoneUserSetupTab } from './UserSetup.types';

interface ITab {
  Title?: string;
}

const UserSetupContainer = styled.div`
  ${styledUserSetupContainer};
`;
const UserSetupContent = styled.div`
  ${styledUserSetupContent};
`;
const UserSetupHeader = styled.div`
  ${styledUserSetupHeader};
`;
const UserSetupFooter = styled.div`
  ${styledUserSetupFooter};
`;

const UserSetupTab: Record<IWebPhoneUserSetupTab, ITab & React.ComponentClass<{}>> = {
  Devices,
  Connection,
};

const UserSetupBackButtonTitle: Record<IWebPhoneUserSetupTab, { title: string; className: string }> = {
  Devices: {
    className: 'colored',
    title: 'TEST CONNECTION QUALITY',
  },
  Connection: {
    className: 'simple',
    title: 'BACK TO DEVICE SETUP',
  },
};
const DefaultTab: IWebPhoneUserSetupTab = 'Devices';

interface IUserSetupDispatchProps {
  show: (tab: IWebPhoneUserSetupTab) => void;
  complete: () => void;
  setDontShowAgain: (value: boolean) => void;
}

export type IWebPhoneUserSetupStateProps = IWebPhoneUserSetupState & {
  connectionQualityScore: number;
};

export type IUserSetupState = {
  dontShowAgain: boolean;
  userChangeDontShowAgainState: boolean;
};

export type IUserSetupProps = IUIWebPhoneUserSetup & IWebPhoneUserSetupStateProps & IUserSetupDispatchProps;

export class UserSetup extends React.Component<IUserSetupProps, IUserSetupState> {
  private _isSelfContained = () => {
    return this.props.selfContained !== false;
  };

  private _showNextTab = () => {
    const { currentTab, show } = this.props;

    show(currentTab === 'Devices' ? 'Connection' : 'Devices');
  };

  private _onComplete = () => {
    this.props.complete();
  };

  private _onSkip = () => {
    this.props.complete();
  };

  private _tabProps = {};

  private _setDontShowAgain = (value: boolean) => {
    this.setState({
      dontShowAgain: value,
      userChangeDontShowAgainState: true,
    });
  };

  constructor(props: IUserSetupProps) {
    super(props);

    this.state = {
      dontShowAgain: this.props.dontShowAgain,
      userChangeDontShowAgainState: false,
    };
  }

  public componentDidMount() {
    if (!this.props.currentTab) {
      this.props.show(DefaultTab);
    }
  }

  public componentWillUnmount() {
    // this method called even if props.dontShowAgain = true,
    // to avoid change dontShowAgain on false
    // check the user really change this prop
    if (this.state.userChangeDontShowAgainState) {
      this.props.setDontShowAgain(this.state.dontShowAgain);
    }
  }

  public render() {
    const { Button, Spinner } = clientEntityProvider.getComponentProvider().UI;
    const { currentTab } = this.props;

    if (!currentTab) {
      return (
        <UserSetupContainer>
          <Spinner />
        </UserSetupContainer>
      );
    }

    const CurrentTab = UserSetupTab[currentTab];
    const selfContained = this._isSelfContained();
    const hasBttnSkip = !selfContained;
    const tab = UserSetupBackButtonTitle[currentTab];
    const ButtonSkip = styled(Button)`
      ${styledSkipButton};
    `;
    const ButtonChangeTab = styled(Button)`
      ${styledButtonChangeTab};
    `;

    return (
      <UserSetupContainer>
        <UserSetupContent>
          <UserSetupHeader>
            <h1>{CurrentTab.Title}</h1>
            {hasBttnSkip && <ButtonSkip bsStyle="link" title="Skip user setup" onClick={this._onSkip} />}
          </UserSetupHeader>
          <CurrentTab {...this._tabProps} />
          {!selfContained && (
            <UserSetupFooter>
              {
                <Checkbox
                  value={this.state.dontShowAgain}
                  onChange={this._setDontShowAgain}
                  label="Always skip my user setup on this device"
                />
              }
              <ButtonChangeTab
                bsStyle="info"
                className={tab.className}
                disabled={this.props.isDisabled}
                title={tab.title}
                onClick={this._showNextTab}
              />
              {this.props.connectionQualityScore ? (
                // show finish button if score existed and more than 0
                <Button icon="checkMark" title="FINISH SETUP" onClick={this._onComplete} />
              ) : null}
            </UserSetupFooter>
          )}
        </UserSetupContent>
      </UserSetupContainer>
    );
  }
}

export default connect<IWebPhoneUserSetupStateProps, IUserSetupDispatchProps>(
  (state: any) => {
    const { userSetup, connectionQuality } = state.webphone;

    return {
      ...userSetup,
      connectionQualityScore: connectionQuality.score,
    };
  },
  {
    show: actionCreators.webphoneUserSetupShow,
    complete: actionCreators.webphoneUserSetupComplete,
    setDontShowAgain: actionCreators.webphoneUserSetupSetDontShowAgain,
  }
)(UserSetup);
