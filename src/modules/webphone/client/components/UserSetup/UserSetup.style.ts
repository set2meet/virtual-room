/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const skipButtonWidth = 110;
const footerMarginBetween = 20;
const hrStyle = '1px solid #575757';

export const styledUserSetupContainer = () => `
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const styledUserSetupHeader = () => `
  padding-bottom: 6px;

  > h1 {
    color: #fff;
    margin: 0;
    font-size: 18px;
    font-weight: bold;
    display: inline-block;
    width: calc(100% - ${skipButtonWidth}px);
  }

  > button {
    height: 20px;
    width: ${skipButtonWidth}px;
  }
`;

const footerPaddingV = 18;

export const styledUserSetupContent = ({ theme }: IRequiredTheme) => `
  border-top: 2px solid ${theme.primaryColor};
  padding: 20px 28px ${footerPaddingV}px 28px;
  background-color: ${theme.backgroundColorSecondary};
`;

export const styledUserSetupFooter = () => `
  display: flex;  
  text-align: center;
  border-top: ${hrStyle};
  justify-content: space-between;
  padding-top: ${footerPaddingV}px;

  > * {
    margin: 0 ${footerMarginBetween / 2}px;

    &:first-child {
      margin-left: 0;
    }

    &:last-child {
      margin-right: 0;
    }
  }

  > button {
    padding: 0 8px;
  }
`;

export const styledSkipButton = ({ theme }: IRequiredTheme) => `
  color: ${theme.primaryColor} !important;
`;

export const styledButtonChangeTab = ({ theme }: IRequiredTheme) => `
  &.btn.btn-info.simple {
    border-color: ${theme.primaryTextColor};
    background-color: transparent;

    > span {
      color: ${theme.primaryTextColor};
    }

    &:disabled:hover {
      border-color: ${theme.primaryTextColor};
      background-color: transparent;
    }

    &:not(:disabled) {
      background-color: transparent;
      border-color: ${theme.primaryTextColor};
      &:before {
        color: ${theme.primaryTextColor};
      }
  
      &:hover {
        background-color: transparent;
        border-color: ${theme.primaryTextColor};
        &:before {
          color: ${theme.primaryTextColor};
        }

        > span {
          color: ${theme.primaryTextColor};
        }
      }
  
      &:active {
        background-color: transparent;
        border-color: ${theme.primaryTextColor};
        &:before {
          color: ${theme.primaryTextColor};
        }

        > span {
          color: ${theme.primaryTextColor};
        }
      }
    }

    &:disabled {
      &,
      &:hover,
      &:active {
        opacity: 1;
        background-color: transparent;
        color: ${theme.buttonDisabledColor};
        border-color: ${theme.buttonDisabledColor};
      }

      &:before {
        color: ${theme.buttonDisabledColor};
      }

      > span {
        color: ${theme.buttonDisabledColor};
      }
    }
  }
`;
