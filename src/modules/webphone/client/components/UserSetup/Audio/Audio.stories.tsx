import React from 'react';
import SetupAudio from './Audio';

export default {
  title: 'webphone/UserSetup/Audio',
  component: SetupAudio,
};

export const Default = () => <SetupAudio />;
