import * as React from 'react';
import { Row, RowDevice, RowHeader, RowDescription, RowDeviceLabel, RowDeviceExample } from '../common.style';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export class SetupAudio extends React.Component<{}> {
  public render() {
    const { MediaDeviceAudioOutputExample } = clientEntityProvider.getComponentProvider().Media;

    return (
      <Row>
        <RowHeader>1. Audio</RowHeader>
        <RowDescription>
          Click on the volume icon to listen to the sound. If it doesn't work, check the sound settings of your
          computer.
        </RowDescription>
        <RowDevice>
          <RowDeviceLabel>Speakers</RowDeviceLabel>
          <RowDeviceExample>
            <MediaDeviceAudioOutputExample mode="user-setup" />
          </RowDeviceExample>
        </RowDevice>
      </Row>
    );
  }
}

export default SetupAudio;
