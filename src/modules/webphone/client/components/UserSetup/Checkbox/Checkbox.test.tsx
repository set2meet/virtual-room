import React from 'react';
import { enzymeCleanup, mountWithTheme } from '../../../../../../test/utils/utils';
import UserSetupCheckbox from './index';
import setupIoCClientForTest from '../../../../../../test/utils/ioc/testIoCClient';

beforeAll(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('<Checkbox />', () => {
  const onChangeMock = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should change value', () => {
    const wrapper = mountWithTheme()(<UserSetupCheckbox label="label" value={false} onChange={onChangeMock} />);

    wrapper.find('label').simulate('click');
    expect(onChangeMock).toBeCalledWith(true);
  });

  it('shouldn\'t change value if disabled', () => {
    const wrapper = mountWithTheme()(
      <UserSetupCheckbox label="label" value={false} onChange={onChangeMock} disabled={true} />
    );

    wrapper.find('label').simulate('click');
    expect(onChangeMock).toBeCalledTimes(0);
  });
});
