/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';
import { css } from 'styled-components';

export interface ICheckboxUIProps extends IRequiredTheme {
  checked: boolean;
  disabled: boolean;
}

export const styledCheckbox = ({ checked, disabled, theme }: ICheckboxUIProps) => {
  const icons = clientEntityProvider.getIcons();

  return css`
    display: inline-flex;
    align-items: center;

    > label {
      cursor: ${disabled ? 'default' : 'pointer'};
      opacity: ${disabled ? '0.6' : '1'};
      user-select: none;
      font-weight: normal;
      margin: 0;
      white-space: nowrap;
      font-size: 12px;

      &:before {
        font-size: 15px;
        margin-right: 6px;
        vertical-align: middle;
        position: relative;
        top: -1px;
        color: ${theme.primaryColor};
        font-family: ${icons.fontFamily};
        content: ${checked ? icons.code.checkboxOn : icons.code.checkboxOff};
      }
    }
  `;
};
