import React from 'react';
import { boolean, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import UserSetupCheckbox from './index';

export default {
  title: 'webphone/UserSetup/Checkbox',
  component: UserSetupCheckbox,
};

const DefaultComponent = (props) => (
  <UserSetupCheckbox
    label={text('label', 'label')}
    value={boolean('value', false)}
    onChange={action('change')}
    {...props}
  />
);

export const Default = () => <DefaultComponent />;

export const Disabled = () => <DefaultComponent value={boolean('value', true)} disabled={boolean('disabled', true)} />;
