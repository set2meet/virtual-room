import styled from 'styled-components';

const rowHeight = 30;
const rowDeviceLabelWidth = 100;
const rowDeviceChoiceWidth = 210;
const rowDeviceChoiceMarginRight = 30;
const rowDeviceExampleWidth = `calc(100% - ${rowDeviceLabelWidth +
  rowDeviceChoiceWidth +
  rowDeviceChoiceMarginRight}px)`;

const row = (width: string) => `
  display: inline-flex;
  width: ${width};
  vertical-align: top;
  min-height: ${rowHeight}px;
  justify-content: flex-start;
  align-items: center;
`;

const styledRow = () => `
  &:first-child {
    margin-top: 22px;
  }

  &:last-child {
    margin-bottom: 28px;
  }
`;

const styledRowHeader = () => `
  color: #2bce72;
`;

const styledRowDescription = () => `
  color: #faf9f5;
  font-size: 12px;
  margin: 8px 0;
`;

const styledRowDevice = () => `
  width: 100%;
`;

const styledRowDeviceLabel = () => `
  ${row(rowDeviceLabelWidth + 'px')};
`;

const styledRowDeviceChoice = () => `
  height: ${rowHeight};
  ${row(rowDeviceChoiceWidth + 'px')};
`;

const styledRowDeviceExample = () => `
  ${row(rowDeviceExampleWidth)};
  position: relative;
  margin-right: ${rowDeviceChoiceMarginRight}px;
`;

export const Row = styled.div`
  ${styledRow};
`;
export const RowHeader = styled.div`
  ${styledRowHeader};
`;
export const RowDescription = styled.div`
  ${styledRowDescription};
`;
export const RowDevice = styled.div`
  ${styledRowDevice};
`;
export const RowDeviceLabel = styled.div`
  ${styledRowDeviceLabel};
`;
export const RowDeviceChoice = styled.div`
  ${styledRowDeviceChoice};
`;
export const RowDeviceExample = styled.div`
  ${styledRowDeviceExample};
`;
