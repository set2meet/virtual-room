import * as React from 'react';
import styled from 'styled-components';
import TabAudio from '../Audio';
import TabCamera from '../Camera';
import TabMicrophone from '../Microphone';
import { styledDevicesContainer } from './Devices.style';

const DevicesContainer = styled.div`
  ${styledDevicesContainer};
`;

export type ISetupDevicesProps = {};

export class SetupDevices extends React.Component<ISetupDevicesProps> {
  public static Title = 'User Setup';

  public render() {
    return (
      <DevicesContainer>
        <TabAudio />
        <hr />
        <TabMicrophone />
        <hr />
        <TabCamera />
      </DevicesContainer>
    );
  }
}

export default SetupDevices;
