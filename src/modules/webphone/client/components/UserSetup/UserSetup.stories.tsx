import React from 'react';
import { boolean, select } from '@storybook/addon-knobs';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { getFakeVideoDevicesInfo } from '../../../../../test/mocks/media';
import { withIoCFactory } from '../../../../../test/utils/utils';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { reduxMockEnhancer } from '../../../../../test/utils/reduxMockEnhancer';
import { getStorybookIoCInitConfig } from '../../../../../../.storybook/common/utils/ioc/storybookInitConfig';
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import { AnyAction } from 'redux';
import withStoryRootIndicator from '../../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { IUIWebPhoneUserSetup, IWebPhoneUserSetupTab } from './UserSetup.types';

const SuspendedUserSetup = withIoCFactory<IUIWebPhoneUserSetup>(() =>
  clientEntityProvider.getComponentProvider().resolveComponentSuspended(ComponentRegistryKey.WebPhone_UserSetup)
);

const changeWebRtcServiceAction = (serviceName: WebrtcServiceType): AnyAction => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  return appActionCreators.changeWebrtcService(serviceName);
};

export const setConnectionQualityAction = (value: number) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  return {
    name: `Set Connection Quality to ${value}`,
    action: appActionCreators.webphoneSetConnectionQuality(value),
  };
};

export const TestCallToggleProgressAction = (value: boolean) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  return {
    name: `TestCallToggleProgress: ${value} `,
    action: appActionCreators.webphoneUserSetupTestCallToggleProgress(value),
  };
};

export const changeTabAction = (tab: IWebPhoneUserSetupTab) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  return {
    name: `Tab: ${tab || 'default'}`,
    action: appActionCreators.webphoneUserSetupShow(tab),
  };
};

export default {
  title: 'webphone/UserSetup',
  component: SuspendedUserSetup,
  excludeStories: /.*Action$/,
  parameters: {
    redux: {
      enable: true,
      actionsFactory: () => [
        changeTabAction('Devices'),
        changeTabAction('Connection'),
        changeTabAction(null),
        // tslint:disable-next-line:no-magic-numbers
        setConnectionQualityAction(0),
        // tslint:disable-next-line:no-magic-numbers
        setConnectionQualityAction(2),
      ],
    },
  },
  decorators: [withStoryRootIndicator, withModule(ModuleRegistryKey.Webphone)],
};

export const Default = () => <SuspendedUserSetup />;

export const Initializing = () => {
  return <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;
};

Initializing.story = {
  parameters: {
    IoCInitializationOptions: {
      storeEnhancerFactory: () => [reduxMockEnhancer, ...getStorybookIoCInitConfig().storeEnhancerFactory()],
    },
  },
};

export const WithQualityTest = () => <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;

export const WithFakeDevices = (props) => {
  const actionCreators = clientEntityProvider.getAppActionCreators();
  const inputVideos = getFakeVideoDevicesInfo(2);
  props.dispatch(actionCreators.setDefaultDevices(inputVideos[0]));
  props.dispatch(actionCreators.updateDevices(inputVideos));
  return <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;
};

WithFakeDevices.story = {
  decorators: [connectDecorator()],
};

export const WithFinishButton = (props) => {
  props.dispatch(setConnectionQualityAction(2).action);
  return <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;
};

WithFinishButton.story = {
  decorators: [connectDecorator()],
};

export const P2PConnectionTab = (props) => {
  props.dispatch(changeWebRtcServiceAction(WebrtcServiceType.P2P));

  const tabs: IWebPhoneUserSetupTab[] = ['Devices', 'Connection'];
  props.dispatch(changeTabAction(select('Tab', tabs, 'Connection')).action);
  return <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;
};

P2PConnectionTab.story = {
  decorators: [connectDecorator()],
};

export const OpentokConnectionTab = (props) => {
  props.dispatch(changeWebRtcServiceAction(WebrtcServiceType.OPENTOK));

  const tabs: IWebPhoneUserSetupTab[] = ['Devices', 'Connection'];
  props.dispatch(changeTabAction(select('Tab', tabs, 'Connection')).action);
  return <SuspendedUserSetup selfContained={boolean('selfContained', false)} />;
};

OpentokConnectionTab.story = {
  decorators: [connectDecorator()],
};
