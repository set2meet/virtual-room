/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IModuleStorageKey } from '../../../../ioc/common/ioc-interfaces';

export interface ILocalStorageWebPhoneRoomData {
  isMicMuted?: boolean;
}

export interface ILocalStorageWebPhoneUserData {
  dontShowAgain?: boolean;
}

export interface IWebphoneStorageKeys extends Record<string, IModuleStorageKey<any>> {
  WEBPHONE_ROOM_DATA_KEY: IModuleStorageKey<ILocalStorageWebPhoneRoomData>;
  WEBPHONE_USER_DATA_KEY: IModuleStorageKey<ILocalStorageWebPhoneUserData>;
}
