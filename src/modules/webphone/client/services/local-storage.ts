/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ILocalStorageWebPhoneRoomData, ILocalStorageWebPhoneUserData } from './IWebphoneStorageKeys';

const getUserState = (): ILocalStorageWebPhoneUserData => {
  const WEBPHONE_USER_DATA_KEY = clientEntityProvider.getStorageKeys().WEBPHONE_USER_DATA_KEY;
  return clientEntityProvider.getStorageManager().get<ILocalStorageWebPhoneUserData>(WEBPHONE_USER_DATA_KEY, {});
};

const getRoomState = (): ILocalStorageWebPhoneRoomData => {
  const WEBPHONE_ROOM_DATA_KEY = clientEntityProvider.getStorageKeys().WEBPHONE_ROOM_DATA_KEY;
  return clientEntityProvider.getStorageManager().get<ILocalStorageWebPhoneRoomData>(WEBPHONE_ROOM_DATA_KEY, {});
};

const setUserState = (state: ILocalStorageWebPhoneUserData): void => {
  const WEBPHONE_USER_DATA_KEY = clientEntityProvider.getStorageKeys().WEBPHONE_USER_DATA_KEY;
  clientEntityProvider.getStorageManager().set<ILocalStorageWebPhoneUserData>(WEBPHONE_USER_DATA_KEY, state);
};

const setRoomState = (state: ILocalStorageWebPhoneRoomData): void => {
  const WEBPHONE_ROOM_DATA_KEY = clientEntityProvider.getStorageKeys().WEBPHONE_ROOM_DATA_KEY;
  clientEntityProvider.getStorageManager().set<ILocalStorageWebPhoneRoomData>(WEBPHONE_ROOM_DATA_KEY, state);
};

const getRoomField = (name: keyof ILocalStorageWebPhoneRoomData) => {
  return getRoomState()[name];
};

const getUserField = (name: keyof ILocalStorageWebPhoneUserData) => {
  return getUserState()[name];
};

const setRoomField = (name: keyof ILocalStorageWebPhoneRoomData, value: any) => {
  const state: ILocalStorageWebPhoneRoomData = getRoomState();

  state[name] = value;

  setRoomState(state);
};

const setUserField = (name: keyof ILocalStorageWebPhoneUserData, value: any) => {
  const state: ILocalStorageWebPhoneUserData = getUserState();

  state[name] = value;

  setUserState(state);
};

export interface ILocalStorageAPI {
  setMicState: (value: boolean) => void;
  getMicState: () => boolean;
  getUserSetupDontShowAgainState: () => boolean;
  setUserSetupDontShowAgainState: (value: boolean) => void;
}

const localStorageAPI: ILocalStorageAPI = {
  setMicState: (value) => {
    setRoomField('isMicMuted', value);
  },
  getMicState: () => {
    return getRoomField('isMicMuted') === true;
  },
  setUserSetupDontShowAgainState: (value) => {
    setUserField('dontShowAgain', value);
  },
  getUserSetupDontShowAgainState: () => {
    return getUserField('dontShowAgain') === true;
  },
};

export default localStorageAPI;
