/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { Storages } from '../../../../ioc/client/providers/StorageManager/storages/Storages';
import { IWebphoneStorageKeys } from './IWebphoneStorageKeys';

const VR_WEBPHONE_LS_KEY = 'webphone';

export const webphoneStorageKeys: IWebphoneStorageKeys = {
  WEBPHONE_ROOM_DATA_KEY: {
    key: VR_WEBPHONE_LS_KEY,
    storage: Storages.LOCAL_STORAGE,
    postfix: (state) => {
      return state?.room?.state?.id || '';
    },
  },
  WEBPHONE_USER_DATA_KEY: {
    key: VR_WEBPHONE_LS_KEY,
    storage: Storages.LOCAL_STORAGE,
  },
};
