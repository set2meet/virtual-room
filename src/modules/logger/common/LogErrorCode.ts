export enum LogErrorCode {
  SERVER = 'E_SERVER',
  MODULE = 'E_MODULE',
  WP_MID = 'E_WP_MID',
}
