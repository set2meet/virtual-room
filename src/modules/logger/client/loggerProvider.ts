/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { S2MServices, ClientLogger } from '@s2m/logger';
import { LogErrorCode } from '../common/LogErrorCode';
import { LogModuleClient } from './types/LogModuleClient';
import { IClientLogger } from './types/types';
import Socket = SocketIOClient.Socket;

const loggerProvider = async (socket: Socket): Promise<IClientLogger> => {
  const clientLogger = new ClientLogger<LogModuleClient, LogErrorCode>(S2MServices.VirtualRoomClient);
  clientLogger.startToSync({
    clientFactory: () => socket,
  });
  const webrtcOpentokLogger = clientLogger.createBoundChild(LogModuleClient.WebrtcOpentok);
  const webrtcP2pLogger = clientLogger.createBoundChild(LogModuleClient.WebrtcP2P);
  const webPhoneLogger = clientLogger.createBoundChild(LogModuleClient.WebPhone);
  const recordingCaptureLogger = clientLogger.createBoundChild(LogModuleClient.RecordingCapture);

  return {
    client: clientLogger,
    webrtcOpentok: webrtcOpentokLogger,
    webrtcP2p: webrtcP2pLogger,
    webPhone: webPhoneLogger,
    recordingCapture: recordingCaptureLogger,
  };
};

export default loggerProvider;
