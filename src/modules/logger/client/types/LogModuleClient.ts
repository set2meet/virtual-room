export enum LogModuleClient {
  WebrtcOpentok = 'WebrtcOpentok',
  WebrtcP2P = 'WebrtcP2P',
  WebPhone = 'WebPhone',
  RecordingCapture = 'Recording',
}
