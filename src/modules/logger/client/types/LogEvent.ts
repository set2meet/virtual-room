export enum LogEvent {
  WsP2p = 'WS_P2P',
  WsOpentok = 'WS_OPENTOK',
  WpViewParticipant = 'WP_VIEW_PARTICIPANT',
  WsP2pNegotiator = 'WS_P2P_negotiator',
}
