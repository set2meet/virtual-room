/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ILogger } from '@s2m/logger';
import { LogErrorCode } from '../../common/LogErrorCode';
import { LogModuleClient } from './LogModuleClient';

export type TLoggerClientModule = ILogger<LogModuleClient, LogErrorCode>;

export interface IClientLogger {
  client: TLoggerClientModule;
  webrtcOpentok: TLoggerClientModule;
  webrtcP2p: TLoggerClientModule;
  webPhone: TLoggerClientModule;
  recordingCapture: TLoggerClientModule;
}
