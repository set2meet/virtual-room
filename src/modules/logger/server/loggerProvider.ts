/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { S2MServices, ServerLogger } from '@s2m/logger';
import { LogErrorCode } from '../common/LogErrorCode';
import { LogModuleServer } from './types/LogModuleServer';
import { IServerLogger } from './types/types';

const loggerProvider = async (): Promise<IServerLogger> => {
  const serverLogger = new ServerLogger<LogModuleServer, LogErrorCode>(S2MServices.VirtualRoomServer);
  const roomServerLogger = serverLogger.createBoundChild(LogModuleServer.RoomServer);
  const authLogger = serverLogger.createBoundChild(LogModuleServer.Auth);
  const loggerLogger = serverLogger.createBoundChild(LogModuleServer.Logger);
  const webrtcOpentokLogger = serverLogger.createBoundChild(LogModuleServer.WebrtcOpentok);
  const webrtcP2pLogger = serverLogger.createBoundChild(LogModuleServer.WebrtcP2P);
  const feedbackLogger = serverLogger.createBoundChild(LogModuleServer.Feedback);

  return {
    server: serverLogger,
    roomServer: roomServerLogger,
    auth: authLogger,
    logger: loggerLogger,
    webrtcOpentok: webrtcOpentokLogger,
    webrtcP2p: webrtcP2pLogger,
    feedback: feedbackLogger,
  };
};

export default loggerProvider;
