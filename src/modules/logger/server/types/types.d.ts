/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ILogger, ServerLogger } from '@s2m/logger';
import { LogErrorCode } from '../../common/LogErrorCode';
import { LogModuleServer } from './LogModuleServer';

export type TLoggerServerModule = ILogger<LogModuleServer, LogErrorCode>;

export interface IServerLogger {
  server: ServerLogger<LogModuleServer, LogErrorCode>;
  roomServer: TLoggerServerModule;
  auth: TLoggerServerModule;
  logger: TLoggerServerModule;
  webrtcOpentok: TLoggerServerModule;
  webrtcP2p: TLoggerServerModule;
  feedback: TLoggerServerModule;
}
