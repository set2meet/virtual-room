export enum LogModuleServer {
  ResourceManagement = 'ResourceManagement',
  RoomServer = 'RoomServer',
  Auth = 'Auth',
  Logger = 'Logger',
  WebrtcOpentok = 'WebrtcOpentok',
  WebrtcP2P = 'WebrtcP2P',
  Feedback = 'Feedback',
}
