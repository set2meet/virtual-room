/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _find from 'lodash/find';
import { AnyAction } from 'redux';
import {
  IMediaReduxState,
  UserMediaErrorName,
  IUserMediaDevicesSettings,
  IUserUnpluggedMediaState,
  IUserMediDeviceType,
} from '../types/mediaTypes';
import auxiliaryErrors from '../types/constants/auxiliaryErrors';
import { MediaAction } from './types/MediaAction';

type TMediaAction = Record<string, any> & {
  type: MediaAction;
};

type MediaReducer = (state: IMediaReduxState, action: TMediaAction) => IMediaReduxState;

type IMediaReduxReducer = { [key in MediaAction]?: MediaReducer };

const INITIAL_STATE: IMediaReduxState = {
  devices: {
    inputVideo: null,
    inputAudio: null,
    outputAudio: null,
  },
  default: {
    inputVideo: null,
    inputAudio: null,
    outputAudio: null,
  },
  settings: {
    outputAudioLevel: 1,
    videoExample: null,
    microphoneVolume: null,
    inputVideoDeviceError: 'NotSet',
    inputAudioDeviceError: 'NotSet',
  },
  unplugged: {
    inputAudio: null,
    inputVideo: null,
  },
};

const removeInputAudioUnplugged = (settings: IUserMediaDevicesSettings, unplugged: IUserUnpluggedMediaState) => {
  unplugged.inputAudio = null;
  settings.inputAudioDeviceError = null;
};

const removeInputVideoUnplugged = (settings: IUserMediaDevicesSettings, unplugged: IUserUnpluggedMediaState) => {
  unplugged.inputVideo = null;
  settings.inputVideoDeviceError = null;
};

const reducersEnum: IMediaReduxReducer = {
  [MediaAction.UPDATE_DEVICES](state: IMediaReduxState, action: AnyAction) {
    const { devices } = action;
    const dia = state.default.inputAudio;
    const div = state.default.inputVideo;
    const defaultDevices = { ...state.default };
    const inputAudio = dia && (_find(devices.inputAudio, { deviceId: dia.deviceId }) as MediaDeviceInfo);
    const inputVideo = div && (_find(devices.inputVideo, { deviceId: div.deviceId }) as MediaDeviceInfo);

    /**
     * In FF labels gets after getting  access rights,
     *  so, on each update - check default device label
     */

    if (inputAudio && inputAudio.label !== dia.label) {
      defaultDevices.inputAudio = inputAudio;
    }

    if (inputVideo && inputVideo.label !== div.label) {
      defaultDevices.inputVideo = inputVideo;
    }

    return {
      ...state,
      devices,
      default: defaultDevices,
    };
  },
  [MediaAction.ENABLE_MEDIA_DEVICE](state: IMediaReduxState, action: AnyAction) {
    const settings = { ...state.settings };

    if (action.audio && auxiliaryErrors[settings.inputAudioDeviceError]) {
      settings.inputAudioDeviceError = null;
    }

    if (action.video && auxiliaryErrors[settings.inputVideoDeviceError]) {
      settings.inputVideoDeviceError = null;
    }

    return {
      ...state,
      settings,
    };
  },
  [MediaAction.DEFAULT_DEVICES_UPDATE_INFO](state: IMediaReduxState, action: AnyAction) {
    const defaultDevices = state.default;
    const { inputAudio, inputVideo } = action;

    if (inputAudio) {
      defaultDevices.inputAudio = inputAudio;
    }

    if (inputVideo) {
      defaultDevices.inputVideo = inputVideo;
    }

    return {
      ...state,
      default: defaultDevices,
    };
  },
  [MediaAction.SET_DEFAULT_DEVICES](state: IMediaReduxState, action: AnyAction) {
    const { inputVideo, inputAudio, outputAudio } = action;
    const defaultDevices = { ...state.default };

    if (inputVideo !== undefined) {
      defaultDevices.inputVideo = inputVideo;
    }

    if (inputAudio !== undefined) {
      defaultDevices.inputAudio = inputAudio;
    }

    if (outputAudio !== undefined) {
      defaultDevices.outputAudio = outputAudio;
    }

    return {
      ...state,
      default: defaultDevices,
    };
  },
  [MediaAction.SET_UNPLUGGED_DEVICES](state: IMediaReduxState, action: AnyAction) {
    const error: UserMediaErrorName = 'DeviceUnplugged';
    const { inputAudio, inputVideo } = action;
    const unplugged = { ...state.unplugged };
    const settings = { ...state.settings };

    if (inputAudio) {
      settings.inputAudioDeviceError = error;
      settings.microphoneVolume = null;
      unplugged.inputAudio = inputAudio;
    } else if (inputAudio === null) {
      removeInputAudioUnplugged(settings, unplugged);
    }

    if (inputVideo) {
      settings.inputVideoDeviceError = error;
      settings.videoExample = null;
      unplugged.inputVideo = inputVideo;
    } else if (inputVideo === null) {
      removeInputVideoUnplugged(settings, unplugged);
    }

    return {
      ...state,
      unplugged,
      settings,
    };
  },
  [MediaAction.IGNORE_UNPLUGGED_DEVICES](state: IMediaReduxState, action: AnyAction) {
    const type: IUserMediDeviceType = action.deviceType;
    const unplugged = { ...state.unplugged };
    const settings = { ...state.settings };

    if (type === 'inputAudio') {
      removeInputAudioUnplugged(settings, unplugged);
    } else if (type === 'inputVideo') {
      removeInputVideoUnplugged(settings, unplugged);
    } else {
      removeInputAudioUnplugged(settings, unplugged);
      removeInputVideoUnplugged(settings, unplugged);
    }

    return {
      ...state,
      unplugged,
      settings,
    };
  },
  [MediaAction.MEDIA_SETTINGS_CHANGE_OUTPUT_AUDIO_LEVEL](state: IMediaReduxState, action: AnyAction) {
    return {
      ...state,
      settings: {
        ...state.settings,
        outputAudioLevel: action.level,
      },
    };
  },
  [MediaAction.MEDIA_SETTINGS_CHANGE_VIDEO_EXAMPLE](state: IMediaReduxState, action: AnyAction) {
    return {
      ...state,
      settings: {
        ...state.settings,
        videoExample: action.video,
      },
    };
  },
  [MediaAction.MEDIA_SETTINGS_CHANGE_MICROPHONE_METER](state: IMediaReduxState, action: AnyAction) {
    return {
      ...state,
      settings: {
        ...state.settings,
        microphoneVolume: action.node,
      },
    };
  },
  [MediaAction.MEDIA_SETTINGS_CHANGE_GET_INPUT_VIDEO_ERROR](state: IMediaReduxState, action: AnyAction) {
    return {
      ...state,
      settings: {
        ...state.settings,
        inputVideoDeviceError: action.errorName,
      },
    };
  },
  [MediaAction.MEDIA_SETTINGS_CHANGE_GET_INPUT_AUDIO_ERROR](state: IMediaReduxState, action: AnyAction) {
    return {
      ...state,
      settings: {
        ...state.settings,
        inputAudioDeviceError: action.errorName,
      },
    };
  },
};

export default (state: IMediaReduxState, action: TMediaAction): IMediaReduxState => {
  const reducer = reducersEnum[action.type];

  return reducer ? reducer(state, action) : state || INITIAL_STATE;
};
