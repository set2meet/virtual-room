/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IMediaActionCreators, IUserMediDeviceType, UserMediaErrorName } from '../types/mediaTypes';
import { MediaAction } from './types/MediaAction';

export const actionCreators: IMediaActionCreators = {
  initDevices: () => ({
    type: MediaAction.INIT_DEVICES,
  }),
  enableDeviceAccess: (audio: boolean, video: boolean) => ({
    type: MediaAction.ENABLE_MEDIA_DEVICE,
    audio,
    video,
  }),
  updateDevices: (
    inputVideo: MediaDeviceInfo[] = [],
    inputAudio: MediaDeviceInfo[] = [],
    outputAudio: MediaDeviceInfo[] = []
  ) => ({
    type: MediaAction.UPDATE_DEVICES,
    devices: {
      inputVideo,
      inputAudio,
      outputAudio,
    },
  }),
  setDefaultDevices: (inputVideo?: MediaDeviceInfo, inputAudio?: MediaDeviceInfo, outputAudio?: MediaDeviceInfo) => ({
    type: MediaAction.SET_DEFAULT_DEVICES,
    inputVideo,
    inputAudio,
    outputAudio,
  }),
  setDefaultInputVideo: (device: MediaDeviceInfo) => ({
    type: MediaAction.SET_DEFAULT_INPUT_VIDEO,
    device,
  }),
  setDefaultInputAudio: (device: MediaDeviceInfo) => ({
    type: MediaAction.SET_DEFAULT_INPUT_AUDIO,
    device,
  }),
  defaultDevicesUpdateInfo: (inputAudio?: MediaDeviceInfo, inputVideo?: MediaDeviceInfo) => ({
    type: MediaAction.DEFAULT_DEVICES_UPDATE_INFO,
    inputAudio,
    inputVideo,
  }),
  defaultDevicesWasUpdated: (
    inputAudio?: MediaDeviceInfo,
    inputVideo?: MediaDeviceInfo,
    audioStreamTrack?: MediaStreamTrack,
    videoStreamTrack?: MediaStreamTrack
  ) => ({
    type: MediaAction.DEFAULT_DEVICES_WAS_UPDATED,
    inputAudio,
    inputVideo,
    audioStreamTrack,
    videoStreamTrack,
  }),
  setUnpluggedDevices: (inputAudio?: MediaDeviceInfo, inputVideo?: MediaDeviceInfo) => ({
    type: MediaAction.SET_UNPLUGGED_DEVICES,
    inputAudio,
    inputVideo,
  }),
  ignoreUnpluggedDevices: (deviceType?: IUserMediDeviceType) => ({
    type: MediaAction.IGNORE_UNPLUGGED_DEVICES,
    deviceType,
  }),
  replaceUnpluggedDevice: (device: MediaDeviceInfo) => ({
    type: MediaAction.REPLACE_UNPLUGGED_DEVICE,
    device,
  }),
  changeMediaSettingsOutputAudioLevel: (level: number) => ({
    type: MediaAction.MEDIA_SETTINGS_CHANGE_OUTPUT_AUDIO_LEVEL,
    level,
  }),
  changeMediaSettingsVideoExample: (video: HTMLVideoElement) => ({
    type: MediaAction.MEDIA_SETTINGS_CHANGE_VIDEO_EXAMPLE,
    video,
  }),
  changeMediaSettingsMicrophoneMeter: (node: HTMLElement) => ({
    type: MediaAction.MEDIA_SETTINGS_CHANGE_MICROPHONE_METER,
    node,
  }),
  changeMediaSettingsInputVideoStreamError: (errorName: UserMediaErrorName) => ({
    type: MediaAction.MEDIA_SETTINGS_CHANGE_GET_INPUT_VIDEO_ERROR,
    errorName,
  }),
  changeMediaSettingsInputAudioStreamError: (errorName: UserMediaErrorName) => ({
    type: MediaAction.MEDIA_SETTINGS_CHANGE_GET_INPUT_AUDIO_ERROR,
    errorName,
  }),
  requestAudioPermissions: () => ({
    type: MediaAction.MEDIA_SETTINGS_REQUEST_AUDIO_PERMISSIONS,
  }),
  requestVideoPermissions: () => ({
    type: MediaAction.MEDIA_SETTINGS_REQUEST_VIDEO_PERMISSIONS,
  }),
  closeMediaSettingsAudioStream: () => ({
    type: MediaAction.MEDIA_SETTINGS_CLOSE_AUDIO_STREAM,
  }),
  closeMediaSettingsVideoStream: () => ({
    type: MediaAction.MEDIA_SETTINGS_CLOSE_VIDEO_STREAM,
  }),
  getMediaSettingsAudioStream: () => ({
    type: MediaAction.MEDIA_SETTINGS_GET_AUDIO_STREAM,
  }),
  getMediaSettingsVideoStream: () => ({
    type: MediaAction.MEDIA_SETTINGS_GET_VIDEO_STREAM,
  }),
  autoSelectDefaultDevices: () => ({
    type: MediaAction.AUTO_SELECT_DEFAULT_DEVICES,
  }),
};
