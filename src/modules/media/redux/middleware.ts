/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import _get from 'lodash/get';
import _find from 'lodash/find';
import { actionCreators } from './actionCreators';
import Socket = SocketIOClient.Socket;
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware, TMiddlewareAPI } from '../../../ioc/common/ioc-interfaces';
import { AnyAction, Dispatch } from 'redux';
import { DefaultDevice } from '../services/types/DefaultDevice';
import { UMSEvent } from '../services/types/IUMSEvent';
import { IMediaReduxState, UserMediaErrorName, IUserMediDeviceType } from '../types/mediaTypes';
import auxiliaryErrors from '../types/constants/auxiliaryErrors';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { MediaAction } from './types/MediaAction';

let settingsInputVideoStreamId: string = null;
let settingsInputAudioStreamId: string = null;

const getModuleState = (store: TMiddlewareAPI, keyStore: string): IMediaReduxState => {
  return _get(store.getState(), keyStore);
};

const updateDevicesList = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();
  const cd = userMediaService.connectedDevices;

  store.dispatch(actionCreators.updateDevices(cd.inputVideo, cd.inputAudio, cd.outputAudio));
};

const settingsCloseInputVideoStream = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();

  store.dispatch(actionCreators.changeMediaSettingsVideoExample(null));
  userMediaService.closeStreamFromVideoDevice(settingsInputVideoStreamId);
  settingsInputVideoStreamId = null;
};

const settingsGetInputVideoStream = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();

  if (!userMediaService.defaultInputVideo) {
    return;
  }

  // get input stream from camera and build html5 video tag to show stream
  userMediaService
    .getStreamFromDefaultInputVideo()
    // on stream success: create html5 video tag
    .then((streamId: string) => {
      settingsInputVideoStreamId = streamId;

      store.dispatch(
        actionCreators.changeMediaSettingsVideoExample(
          userMediaService.getHTMLMediaElementForStream(settingsInputVideoStreamId)
        )
      );
    })
    // on stream failure: set stream error
    .catch((errName: UserMediaErrorName) => {
      store.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(errName));
    });
};

/**
 * This is only used when settings pop-up closed to nullify settings
 * @param store
 */
const settingsCloseInputAudioStream = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();

  store.dispatch(actionCreators.changeMediaSettingsMicrophoneMeter(null));
  userMediaService.closeStreamFromAudioDevice(settingsInputAudioStreamId);
  settingsInputAudioStreamId = null;
};

const settingsGetInputAudioStream = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();

  if (!userMediaService.defaultInputAudio) {
    return;
  }

  // get input stream from microphone and microphone volume analizer
  userMediaService
    .getStreamFromDefaultInputAudio()
    // on stream success: create mic volume meter
    .then((streamId: string) => {
      settingsInputAudioStreamId = streamId;

      store.dispatch(
        actionCreators.changeMediaSettingsMicrophoneMeter(userMediaService.getMicrophoneVolumeMeter(streamId))
      );
    });
};

const clearInputAudioError = (store: TMiddlewareAPI) => {
  store.dispatch(actionCreators.changeMediaSettingsInputAudioStreamError(null));
};

const clearInputVideoError = (store: TMiddlewareAPI) => {
  store.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(null));
};

const setDefaultDevices = (store: TMiddlewareAPI) => {
  const userMediaService = clientEntityProvider.getUserMediaService();

  const { inputAudio, inputVideo, outputAudio } = userMediaService.defaultDevices;

  store.dispatch(actionCreators.setDefaultDevices(inputVideo, inputAudio, outputAudio));

  // if default devices is unknown - set errors about it
  const errName = 'UnknownDefaultDevice';

  if (inputAudio && inputAudio.deviceId === DefaultDevice.deviceId) {
    store.dispatch(actionCreators.changeMediaSettingsInputAudioStreamError(errName));
  }
  if (inputVideo && inputVideo.deviceId === DefaultDevice.deviceId) {
    store.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(errName));
  }
};

const tryReconnectUnplugedDevices = (
  store: TMiddlewareAPI,
  storeKey: string,
  connected: Record<IUserMediDeviceType, MediaDeviceInfo[]>
) => {
  const state = getModuleState(store, storeKey);
  const inputVideoDevices = connected.inputVideo;
  const inputAudioDevices = connected.inputAudio;
  const unpluggedInputVideo = state.unplugged.inputVideo;
  const unpluggedInputAudio = state.unplugged.inputAudio;

  const unpluggedVideoNowConnected =
    unpluggedInputVideo &&
    (_find(inputVideoDevices, { deviceId: unpluggedInputVideo.deviceId }) ||
      _find(inputVideoDevices, { label: unpluggedInputVideo.label }));

  const unpluggedAudioNowConnected =
    unpluggedInputAudio &&
    (_find(inputAudioDevices, { deviceId: unpluggedInputAudio.deviceId }) ||
      _find(inputAudioDevices, { label: unpluggedInputAudio.label }));

  if (unpluggedVideoNowConnected) {
    store.dispatch(actionCreators.setUnpluggedDevices(undefined, null));
    store.dispatch(actionCreators.setDefaultInputVideo(unpluggedVideoNowConnected));
  }

  if (unpluggedAudioNowConnected) {
    store.dispatch(actionCreators.setUnpluggedDevices(null, undefined));
    store.dispatch(actionCreators.setDefaultInputAudio(unpluggedAudioNowConnected));
  }
};

const replaceUnpluggedInputAudioDevice = (store: TMiddlewareAPI, storeKey: string, device: MediaDeviceInfo) => {
  store.dispatch(actionCreators.setUnpluggedDevices(null, undefined));
  store.dispatch(actionCreators.setDefaultInputAudio(device));
};

const replaceUnpluggedInputVideoDevice = (store: TMiddlewareAPI, storeKey: string, device: MediaDeviceInfo) => {
  store.dispatch(actionCreators.setUnpluggedDevices(undefined, null));
  store.dispatch(actionCreators.setDefaultInputVideo(device));
};

const replaceUnpluggedDevice = (store: TMiddlewareAPI, storeKey: string, device: MediaDeviceInfo) => {
  if (device.kind === 'audioinput') {
    replaceUnpluggedInputAudioDevice(store, storeKey, device);
  }
  if (device.kind === 'videoinput') {
    replaceUnpluggedInputVideoDevice(store, storeKey, device);
  }
};

const ignoreUnpluggedDevices = (store: TMiddlewareAPI, storeKey: string, type: IUserMediDeviceType) => {
  const state = getModuleState(store, storeKey);
  const { inputAudio, inputVideo } = state.unplugged;

  if (type === 'inputAudio' || (!type && inputAudio)) {
    store.dispatch(actionCreators.setDefaultInputAudio(null));
  }

  if (type === 'inputVideo' || (!type && inputVideo)) {
    store.dispatch(actionCreators.setDefaultInputVideo(null));
  }
};

const showWarningAboutUnpluggedDevices = (
  store: TMiddlewareAPI,
  unplugged: Record<IUserMediDeviceType, MediaDeviceInfo>
) => {
  const { inputAudio, inputVideo } = unplugged;

  if (inputAudio || inputVideo) {
    store.dispatch(actionCreators.setUnpluggedDevices(inputAudio, inputVideo));
  }
};

const updateDefaultDevicesInfo = (store: TMiddlewareAPI, devices: Record<IUserMediDeviceType, MediaDeviceInfo>) => {
  const { inputAudio, inputVideo } = devices;

  store.dispatch(actionCreators.defaultDevicesUpdateInfo(inputAudio, inputVideo));
};

let middlewareEnumCache: IAppReduxEnumMiddleware = null;
const getMiddlewareEnum = (): IAppReduxEnumMiddleware => {
  if (!middlewareEnumCache) {
    middlewareEnumCache = {
      [MediaAction.INIT_DEVICES](store, next, action, socket, storeKey) {
        const userMediaService = clientEntityProvider.getUserMediaService();

        next(action);

        setDefaultDevices(store);
        updateDevicesList(store);

        userMediaService.subscribe('mediaAccessError', (error: UMSEvent.IMediaDeviceError) => {
          const { inputAudio, inputVideo } = getModuleState(store, storeKey).default;
          const { inputAudioDeviceId, inputVideoDeviceId, name } = error;

          if (inputAudio && inputAudio.deviceId === inputAudioDeviceId) {
            store.dispatch(actionCreators.changeMediaSettingsInputAudioStreamError(name));
          }
          if (inputVideo && inputVideo.deviceId === inputVideoDeviceId) {
            store.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(name));
          }
        });

        userMediaService.subscribe('mediaAccessAllowed', ({ audio, video }: UMSEvent.IMediaDeviceAllowed) => {
          const { inputAudioDeviceError, inputVideoDeviceError } = getModuleState(store, storeKey).settings;

          if ((audio && auxiliaryErrors[inputAudioDeviceError]) || (video && auxiliaryErrors[inputVideoDeviceError])) {
            store.dispatch(actionCreators.enableDeviceAccess(audio, video));
          }
        });

        userMediaService.subscribe('mediaDevicesChange', (event: UMSEvent.IMediaDevicesChange) => {
          const { connected, unplugged, updatedDefaultDevices } = event;

          updateDevicesList(store);

          if (event.default) {
            setDefaultDevices(store);
          }

          if (unplugged) {
            showWarningAboutUnpluggedDevices(store, unplugged);
          }

          if (connected) {
            tryReconnectUnplugedDevices(store, storeKey, connected);
          }

          if (updatedDefaultDevices) {
            updateDefaultDevicesInfo(store, updatedDefaultDevices);
          }
        });
      },
      [MediaAction.SET_DEFAULT_INPUT_AUDIO](store, next, action, socket, storeKey) {
        const userMediaService = clientEntityProvider.getUserMediaService();
        const { device } = action;

        clearInputAudioError(store);

        // set new input audio device
        userMediaService.defaultInputAudio = device;
        store.dispatch(actionCreators.setUnpluggedDevices(null, undefined));
        store.dispatch(actionCreators.setDefaultDevices(undefined, device, undefined));
      },
      [MediaAction.SET_DEFAULT_INPUT_VIDEO](store, next, action, socket, storeKey) {
        const userMediaService = clientEntityProvider.getUserMediaService();
        const { device } = action;

        clearInputVideoError(store);

        // set new input video device
        userMediaService.defaultInputVideo = device;
        store.dispatch(actionCreators.setUnpluggedDevices(undefined, null));
        store.dispatch(actionCreators.setDefaultDevices(device, undefined, undefined));
      },
      [MediaAction.MEDIA_SETTINGS_CLOSE_AUDIO_STREAM](store, next, action) {
        settingsCloseInputAudioStream(store);
        next(action);
      },
      [MediaAction.MEDIA_SETTINGS_CLOSE_VIDEO_STREAM](store, next, action) {
        settingsCloseInputVideoStream(store);
        next(action);
      },
      [MediaAction.MEDIA_SETTINGS_GET_AUDIO_STREAM](store, next, action, socket, storeKey) {
        next(action);

        setTimeout(() => {
          settingsCloseInputAudioStream(store);
          settingsGetInputAudioStream(store);
        }, 1);
      },
      [MediaAction.MEDIA_SETTINGS_GET_VIDEO_STREAM](store, next, action, socket, storeKey) {
        next(action);

        setTimeout(() => {
          settingsCloseInputVideoStream(store);
          settingsGetInputVideoStream(store);
        }, 1);
      },
      [MediaAction.REPLACE_UNPLUGGED_DEVICE](store, next, action, socket, storeKey) {
        next(action);
        replaceUnpluggedDevice(store, storeKey, action.device);
      },
      [MediaAction.IGNORE_UNPLUGGED_DEVICES](store, next, action, socket, storeKey) {
        // here very important sequence of actions
        ignoreUnpluggedDevices(store, storeKey, action.deviceType);
        next(action);
      },
      [MediaAction.AUTO_SELECT_DEFAULT_DEVICES](store, next, action, socket, storeKey) {
        const userMediaService = clientEntityProvider.getUserMediaService();
        next(action);
        store.dispatch(actionCreators.setUnpluggedDevices(null, null));
        userMediaService.autoSelectDefaultDevices();
      },
      [MediaAction.MEDIA_SETTINGS_REQUEST_AUDIO_PERMISSIONS](store, next, action, socket, storeKey) {
        next(action);
        settingsGetInputAudioStream(store);
      },
      [MediaAction.MEDIA_SETTINGS_REQUEST_VIDEO_PERMISSIONS](store, next, action, socket, storeKey) {
        next(action);
        settingsGetInputVideoStream(store);
      },
    };
  }
  return middlewareEnumCache;
};

const clientMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: TMiddlewareAPI) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  const middlewareEnum = getMiddlewareEnum();
  if (middlewareEnum[action.type]) {
    return middlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default clientMiddleware;
