/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';

type PromiseFunc = () => Promise<any>;

class PromisesQueue extends Array {
  private _status: 'none' | 'busy' = 'none';

  constructor() {
    super();
  }

  private _runNextNode = () => {
    if (this._status === 'busy') {
      return;
    }

    const node = this.shift();

    if (node) {
      this._status = 'busy';
      node();
    }
  };

  private _onNodeEnd = () => {
    this._status = 'none';
    this._runNextNode();
  };

  private _makeQueueNode(func: PromiseFunc) {
    return () => func().catch(_noop).then(this._onNodeEnd);
  }

  public add(promise: PromiseFunc) {
    this.push(this._makeQueueNode(promise));
    this._runNextNode();
  }
}

export default PromisesQueue;
