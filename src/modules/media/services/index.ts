/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _get from 'lodash/get';
import _find from 'lodash/find';
import ls from './localStorage';
import EventEmitter from 'events';
import PromisesQueue from './promisesQueue';
import { ScreenSharingService } from './screenSharing';
import VolumeMeter from './userMedia/volumeMeter';
import { IScreenSharingSupportInfo, IUserMediDeviceType, UserMediaErrorName } from '../types/mediaTypes';
import { ExtendedMediaStreamConstraints } from './types/IExtendedMediaStreamConstraints';
import { DEVICES_MAP, getDefaultDevice, getDevices, getMediaStream, onDevicesChange } from './userMedia';
import { stopTrack } from './utils/mediaTrack';
import {
  IUserMediaDevicesCollection,
  IUserMediaService,
  IUserMediaServiceEventHandler,
  IUserMediaServiceEventName,
} from './types/IUserMediaService';
import { DefaultDevice } from './types/DefaultDevice';
import { IScreenSharingService } from './screenSharing/IScreenSharingService';

type InputDeviceType = 'inputAudio' | 'inputVideo';

/**
 * There are bug under win10/macOs - stream is captured
 * but closes very quickly, in less than time below
 * in case where device is used by other app
 */
const MEDIA_STREAM_TRACK_TIME_STOP = 300;

const waitStreamTrack = (track: MediaStreamTrack) =>
  new Promise((resolve, reject) => {
    if (!track) {
      return resolve();
    }

    track.onended = reject;

    const timer: number = window.setTimeout(() => {
      window.clearTimeout(timer);
      track.onended = null;
      resolve();
    }, MEDIA_STREAM_TRACK_TIME_STOP);
  });

/**
 * There are cached devices id error sometimes in Firefox
 *
 * Take steam through time of MEDIA_STREAM_TIMEOUT always
 * to avoid this error in any way
 */

export const MEDIA_STREAM_TIMEOUT = 500; // im ms

/**
 * There are 2 sets of error for FireFox and EDGE,
 *  see https://addpipe.com/blog/common-getusermedia-errors/
 *
 *  NotFoundError        DevicesNotFoundError
 *  NotReadableError     TrackStartError
 *  OverconstrainedError ConstraintNotSatisfiedError
 *  NotAllowedError      PermissionDeniedError
 */

const MEDIA_DEVICE_ERROR_MAP: Record<string, UserMediaErrorName> = {
  DevicesNotFoundError: 'NotFoundError',
  TrackStartError: 'NotReadableError',
  SourceUnavailableError: 'NotReadableError',
  PermissionDeniedError: 'NotAllowedError',
};

// Public interfaces (move to WebPhone?)
interface IUserMediaStreamInfo {
  deviceId: string;
  stream: MediaStream;
}
type IUserMediaDevicesInfo = Record<IUserMediDeviceType, MediaDeviceInfo>;

// patch video element: make it transparent on track ended (device was unplugged, for example)
const patchVideoTag = (video: HTMLVideoElement, stream: MediaStream): void => {
  if (!(video && stream)) {
    return;
  }

  const track = stream.getVideoTracks()[0];

  if (!track) {
    return;
  }

  track.addEventListener('ended', () => {
    video.srcObject = null;
  });
};

// Create video tag, hidden
const createVideoTag = (stream: MediaStream): HTMLVideoElement => {
  if (stream) {
    const video = document.createElement('video');

    video.volume = 0;
    video.autoplay = true;
    video.srcObject = stream;

    patchVideoTag(video, stream);

    return video;
  }

  return undefined;
};

// Get unplugged devices
const getUnpluggedDevices = (currDevices: MediaDeviceInfo[], prevDevices: MediaDeviceInfo[]): MediaDeviceInfo[] => {
  const table: Record<string, boolean> = {};

  currDevices.forEach((device) => {
    table[device.deviceId] = true;
  });

  return prevDevices.filter((device) => !table[device.deviceId]);
};

// Get new connected devices
const getConnectedDevices = (currDevices: MediaDeviceInfo[], prevDevices: MediaDeviceInfo[]): MediaDeviceInfo[] => {
  const prevDevicesId: Record<string, 1> = {};

  prevDevices.forEach((device) => {
    prevDevicesId[device.deviceId] = 1;
  });

  const newDevices = currDevices.filter((device) => {
    return prevDevicesId[device.deviceId] !== 1;
  });

  return newDevices.length ? newDevices : null;
};

// Save device data in local storage
const saveInStoreDefaultDevice = (device: MediaDeviceInfo): void => {
  if (device) {
    ls.setDefaultDevice(DEVICES_MAP[device.kind], { id: device.deviceId, label: device.label });
  }
};

// Save info about default devices in local storage
const saveInStoreDefaultDevices = (dd: IUserMediaDevicesInfo): void => {
  saveInStoreDefaultDevice(dd.inputVideo);
  saveInStoreDefaultDevice(dd.inputAudio);
  saveInStoreDefaultDevice(dd.outputAudio);
};

const getDefaultUnpluggedDevice = (device: MediaDeviceInfo, unplugged: MediaDeviceInfo[]): MediaDeviceInfo => {
  if (device) {
    return unplugged.filter((unpluggedDevice) => unpluggedDevice.deviceId === device.deviceId)[0];
  }

  return null;
};

const hasAnyDeviceType = (data: Record<InputDeviceType, MediaDeviceInfo | MediaDeviceInfo[]>): boolean => {
  return Boolean(data.inputVideo || data.inputAudio);
};

const deviceNotDescribed = (device: MediaDeviceInfo): boolean => {
  return !(device && device.deviceId !== DefaultDevice.deviceId && device.label !== '');
};

const deviceHasNoId = (device: MediaDeviceInfo): boolean => {
  return !(device && device.deviceId !== DefaultDevice.deviceId);
};

// todo 1: extends interface from webphone as requires
// todo 2: define events interface for each event name

export default class UserMediaService extends EventEmitter implements IUserMediaService {
  private _queue: PromisesQueue = new PromisesQueue();
  private _defaultDevices: IUserMediaDevicesInfo = {
    inputVideo: undefined,
    inputAudio: undefined,
    outputAudio: undefined,
  };
  private _connectedDevices: IUserMediaDevicesCollection = {
    inputVideo: [],
    inputAudio: [],
    outputAudio: [],
  };
  private _screenSharingCapabilty: IScreenSharingSupportInfo;
  private _currentInputStreams: Record<string, IUserMediaStreamInfo> = {};
  private _videoTagsCollection: Record<string, HTMLVideoElement> = {};
  private _microphoneVolumeMeterCollection: Record<string, VolumeMeter> = {};
  public defaultInputAudioStreamId: string = null;
  public defaultInputVideoStreamId: string = null;
  public screenSharingService: IScreenSharingService;

  private _emitMediaAccessError(constraint: ExtendedMediaStreamConstraints, name: string): void {
    const { video, audio } = constraint;
    const getDeviceId = (constraints: MediaTrackConstraints): string => {
      return constraints && constraints.deviceId ? _get(constraints, 'deviceId.exact', DefaultDevice.deviceId) : null;
    };

    this._fire('mediaAccessError', {
      name,
      inputAudioDeviceId: getDeviceId(audio as MediaTrackConstraints),
      inputVideoDeviceId: getDeviceId(video as MediaTrackConstraints),
    });
  }

  private _emitMediaAccessAllowed(constraint: ExtendedMediaStreamConstraints): void {
    this._fire('mediaAccessAllowed', {
      audio: !!constraint.audio,
      video: !!constraint.video,
    });
  }

  private _getDefaultDevice(type: IUserMediDeviceType): MediaDeviceInfo {
    const cd = this._connectedDevices;
    const lsdd = ls.getDefaultDevices();

    return getDefaultDevice(cd[type], lsdd[type]);
  }

  private _getNewDefaultDevices(prevDevices: IUserMediaDevicesCollection, type: IUserMediDeviceType): MediaDeviceInfo {
    const devices = prevDevices[type];
    const dd = this._defaultDevices;

    if (!(dd[type] && devices && devices.length)) {
      return (dd[type] = this._getDefaultDevice(type));
    }

    return null;
  }

  private _fire(eventName: IUserMediaServiceEventName, eventValue?: any): void {
    // this method is more convenient because it suggests you the name of the event
    this.emit(eventName, eventValue);
  }

  private _removeStreamFromCollection(streamId: string): void {
    const streamInfo = this._currentInputStreams[streamId];

    if (streamInfo) {
      streamInfo.stream.getTracks().forEach(stopTrack);
    }

    delete this._currentInputStreams[streamId];
  }

  private _storeStreamById = (deviceId: string, stream: MediaStream): string => {
    const { id } = stream;

    this._currentInputStreams[id] = { deviceId, stream };

    return id;
  };

  // TODO::do smth wit it
  // private _removeStreamByDeviceId(deviceId: string) {
  //   const pair = _find(this._currentInputStreams, { deviceId });
  //
  //   if (pair) {
  //     this._removeStreamFromCollection(pair.stream.id);
  //   }
  // }

  private _updateDevicesInfo(stream: MediaStream): Promise<void> {
    const { inputAudio, inputVideo } = this._defaultDevices;
    const checkAudioDevice = deviceNotDescribed(inputAudio);
    const checkVideoDevice = deviceNotDescribed(inputVideo);

    if (!(checkAudioDevice || checkAudioDevice)) {
      return Promise.resolve();
    }

    return new Promise((resolve) => {
      this.getDevicesList().then((devices: IUserMediaDevicesCollection) => {
        const updatedDefaultDevices: IUserMediaDevicesInfo = {
          inputAudio: null,
          inputVideo: null,
          outputAudio: null,
        };

        // in safari, first device info has no deviceId/label property
        // get its info and update default devices info by event
        if (checkAudioDevice) {
          let device: MediaDeviceInfo;
          const track = stream.getAudioTracks()[0];

          if (track) {
            device = _find(devices.inputAudio, { label: track.label });
          } else {
            device = this._getDefaultDevice('inputAudio');
          }

          updatedDefaultDevices.inputAudio = device;
          this._defaultDevices.inputAudio = device;
        }
        if (checkVideoDevice) {
          let device: MediaDeviceInfo;
          const track = stream.getVideoTracks()[0];

          if (track) {
            device = _find(devices.inputVideo, { label: track.label });
          } else {
            device = this._getDefaultDevice('inputVideo');
          }

          updatedDefaultDevices.inputVideo = device;
          this._defaultDevices.inputVideo = device;
        }

        this._fire('mediaDevicesChange', {
          devices,
          updatedDefaultDevices,
        });

        // resolve this action
        resolve();
      });
    });
  }

  private _getMediaStream = (constraint: ExtendedMediaStreamConstraints): Promise<MediaStream> => {
    return new Promise((success, error) => {
      const onError = (errName: UserMediaErrorName) => {
        this._emitMediaAccessError(constraint, errName);
        error(errName);
      };

      const onSuccess = (stream: MediaStream) => {
        this._emitMediaAccessAllowed(constraint);
        success(stream);
      };

      const patchStreamErrors = (stream: MediaStream) => {
        // if media track will closed within MEDIA_STREAM_TRACK_TIME_STOP
        // return NotReadableError error (device is busy by other app)
        Promise.all([waitStreamTrack(stream.getVideoTracks()[0]), waitStreamTrack(stream.getAudioTracks()[0])])
          .then(() => onSuccess(stream))
          .catch(() => onError('NotReadableError'));
      };

      const onStreamRecieved = (stream: MediaStream) => {
        this._updateDevicesInfo(stream).then(() => {
          patchStreamErrors(stream);
        });
      };

      this._queue.add(
        () =>
          new Promise((done) => {
            setTimeout(() => {
              getMediaStream(constraint)
                .then((stream) => {
                  onStreamRecieved(stream);
                })
                .catch((err: Error) => {
                  onError((MEDIA_DEVICE_ERROR_MAP[err.name] || err.name) as UserMediaErrorName);
                })
                .then(done);
            }, MEDIA_STREAM_TIMEOUT);
          })
      );
    });
  };

  private _closeStreamsFromDevice(type: IUserMediDeviceType): void {
    const device = this._defaultDevices[type];

    if (!device) {
      return;
    }

    const deviceId = device.deviceId;

    Object.keys(this._currentInputStreams).forEach((streamId) => {
      if (this._currentInputStreams[streamId].deviceId === deviceId) {
        this._removeStreamFromCollection(streamId);
      }
    });
  }

  constructor() {
    super();
    this.screenSharingService = new ScreenSharingService();

    // setup
    // 1. get real devices list
    // 2. define "default" devices
    this.getDevicesList().then((devices) => {
      this.autoSelectDefaultDevices();
      this._fire('mediaDevicesChange', { devices });
    });

    // native events
    // 1. subscribe on change devices event
    // 2. search only unplugged devices as collection
    onDevicesChange((devices: IUserMediaDevicesCollection) => {
      const { inputVideo, inputAudio } = devices;
      const prevDevices = this._connectedDevices;
      const dd = this._defaultDevices;

      this._connectedDevices = devices;

      // if was no devices - define default
      const ndd = {
        inputAudio: this._getNewDefaultDevices(prevDevices, 'inputAudio'),
        inputVideo: this._getNewDefaultDevices(prevDevices, 'inputVideo'),
      };

      // all input unplugged devices
      const ud = {
        inputVideo: getUnpluggedDevices(inputVideo, prevDevices.inputVideo),
        inputAudio: getUnpluggedDevices(inputAudio, prevDevices.inputAudio),
      };

      // find unplugged default input devices
      const udd = {
        inputVideo: getDefaultUnpluggedDevice(dd.inputVideo, ud.inputVideo),
        inputAudio: getDefaultUnpluggedDevice(dd.inputAudio, ud.inputAudio),
      };

      // find all new connected devices
      const ncd = {
        inputVideo: getConnectedDevices(inputVideo, prevDevices.inputVideo),
        inputAudio: getConnectedDevices(inputAudio, prevDevices.inputAudio),
      };

      if (udd.inputVideo) {
        this._closeStreamsFromDevice('inputVideo');
      }

      if (udd.inputAudio) {
        this._closeStreamsFromDevice('inputAudio');
      }

      this._fire('mediaDevicesChange', {
        default: hasAnyDeviceType(ndd) ? ndd : null,
        connected: hasAnyDeviceType(ncd) ? ncd : null,
        unplugged: hasAnyDeviceType(udd) ? udd : null,
      });
    });

    // the code below for debug only!
    // it will be removed automatically in prod build
    if ((module as any).hot) {
      /* tslint:disable */
      (window as any).md = this;
      (window as any).mdLogStreams = () => {
        const totalLogs: string[] = [];
        let atc = 0;
        let vtc = 0;

        Object.keys(this._currentInputStreams).forEach((id) => {
          const { stream, deviceId } = this._currentInputStreams[id];
          const at = stream.getAudioTracks()[0];
          const vt = stream.getVideoTracks()[0];
          const label = (at && at.label) || (vt && vt.label);

          atc += at ? 1 : 0;
          vtc += vt ? 1 : 0;
          totalLogs.push(
            `[stream info] stream ${id.substr(0, 7)}
            (${stream.active})-(${label})-(${deviceId.substr(0, 6)}),
            a: ${at && at.enabled ? '1' : '0'} |
            v: ${vt && vt.enabled ? '1' : '0'}`
          );
        });

        console.log(`[stream info] tracks total: audio (${atc}) | video (${vtc})`);
        console.log(totalLogs.join('\n'));
      };
      (window as any).mdCloseStreams = (streamId?: string) => {
        Object.keys(this._currentInputStreams).forEach((id) => {
          this._removeStreamFromCollection(id);
        });
        console.log('[stream info] all streams was removed');
        (window as any).mdLogStreams();
      };
      /* tslint:enable */
    }
  }

  public getVideoStreamTrack(streamId: string): MediaStreamTrack {
    const streamInfo = this._currentInputStreams[streamId];

    return streamInfo ? streamInfo.stream.getVideoTracks()[0] : null;
  }

  public getVideoStreamTrackByDeviceId(deviceId: string): MediaStreamTrack {
    const streamInfo = _find(this._currentInputStreams, { deviceId });

    return streamInfo ? streamInfo.stream.getVideoTracks()[0] : null;
  }

  public getTabVideoTrack(): MediaStreamTrack {
    return this.getVideoStreamTrackByDeviceId('tab');
  }

  public getAudioStreamTrack(streamId: string): MediaStreamTrack {
    const streamInfo = this._currentInputStreams[streamId];

    return streamInfo ? streamInfo.stream.getAudioTracks()[0] : null;
  }

  public get defaultDevices(): IUserMediaDevicesInfo {
    return this._defaultDevices;
  }

  public get connectedDevices(): IUserMediaDevicesCollection {
    return this._connectedDevices;
  }

  public get screenSharingCapability(): IScreenSharingSupportInfo {
    return this._screenSharingCapabilty;
  }

  public get defaultInputVideo(): MediaDeviceInfo {
    return this._defaultDevices.inputVideo;
  }

  public set defaultInputVideo(device: MediaDeviceInfo) {
    // close all video streams from current device
    this._closeStreamsFromDevice('inputVideo');
    // set new default device
    if (device) {
      saveInStoreDefaultDevice(device);
    }
    this._defaultDevices.inputVideo = device;
    this._fire('defaultInputVideoDeviceChanged');
  }

  public get defaultInputAudio(): MediaDeviceInfo {
    return this._defaultDevices.inputAudio;
  }

  public set defaultInputAudio(device: MediaDeviceInfo) {
    // close all audio streams from current device
    this._closeStreamsFromDevice('inputAudio');
    // set new default device
    if (device) {
      saveInStoreDefaultDevice(device);
    }
    this._defaultDevices.inputAudio = device;
    this._fire('defaultInputAudioDeviceChanged');
  }

  public get defaultOutputAudio(): MediaDeviceInfo {
    return this._defaultDevices.outputAudio;
  }

  public set defaultOutputAudio(device: MediaDeviceInfo) {
    if (device) {
      saveInStoreDefaultDevice(device);
    }
    this._defaultDevices.outputAudio = device;
    this._fire('defaultOutputAudioDeviceChanged');
  }

  public getHTMLMediaElementForStream(streamId: string): HTMLVideoElement {
    const streamInfo = this._currentInputStreams[streamId];

    if (!streamInfo) {
      return;
    }

    const element = createVideoTag(streamInfo.stream);

    this._videoTagsCollection[streamId] = element;

    return element;
  }

  public getMicrophoneVolumeMeter(streamId?: string): HTMLElement {
    const streamInfo = this._currentInputStreams[streamId];

    if (!streamInfo) {
      return null;
    }

    const volumeMeter = new VolumeMeter(streamInfo.stream);

    this._microphoneVolumeMeterCollection[streamId] = volumeMeter;

    return volumeMeter.node;
  }

  public subscribe(eventName: IUserMediaServiceEventName, eventHandler: IUserMediaServiceEventHandler): void {
    this.on(eventName, eventHandler);
  }

  public unsubscribe(eventName: IUserMediaServiceEventName, eventHandler: IUserMediaServiceEventHandler): void {
    this.removeListener(eventName, eventHandler);
  }

  public getStreamFromDesktop(): Promise<string> {
    return this.screenSharingService.getDisplayMedia().then((stream) => this._storeStreamById('desktop', stream));
  }

  public getStreamFromDefaultDevices(getAudio: boolean = true, getVideo: boolean = true): Promise<MediaStream> {
    const { inputAudio, inputVideo } = this._defaultDevices;
    const constraints: ExtendedMediaStreamConstraints = {};

    if (getAudio) {
      constraints.audio = {
        deviceId: deviceHasNoId(inputAudio) ? undefined : { exact: inputAudio.deviceId },
      };
    }
    if (getVideo) {
      constraints.video = {
        deviceId: deviceHasNoId(inputVideo) ? undefined : { exact: inputVideo.deviceId },
      };
    }

    return this._getMediaStream(constraints);
  }

  public getStreamFromDefaultInputVideo(): Promise<string> {
    const { inputVideo } = this._defaultDevices;

    return this.getStreamFromDefaultDevices(false, true).then((stream) =>
      this._storeStreamById(inputVideo.deviceId, stream)
    );
  }

  public getStreamFromDefaultInputAudio(): Promise<string> {
    const { inputAudio } = this._defaultDevices;

    return this.getStreamFromDefaultDevices(true, false).then((stream) =>
      this._storeStreamById(inputAudio.deviceId, stream)
    );
  }

  public closeStreamFromVideoDevice(streamId: string): void {
    const element = this._videoTagsCollection[streamId];

    if (element) {
      element.pause();
      delete this._videoTagsCollection[streamId];
    }

    this._removeStreamFromCollection(streamId);
  }

  public closeStreamFromAudioDevice(streamId: string): void {
    const element = this._microphoneVolumeMeterCollection[streamId];

    if (element) {
      element.stopAnalizeMicrophone();
      delete this._microphoneVolumeMeterCollection[streamId];
    }

    this._removeStreamFromCollection(streamId);
  }

  public getDevicesList(): Promise<IUserMediaDevicesCollection> {
    return new Promise((res) => {
      getDevices((devices: IUserMediaDevicesCollection) => {
        this._connectedDevices = devices;
        res(devices);
      });
    });
  }

  public cloneStream(streamId: string): string {
    const streamInfo = this._currentInputStreams[streamId];

    if (!streamInfo) {
      return;
    }

    const stream = streamInfo.stream.clone();
    const { id } = stream;

    this._currentInputStreams[id] = {
      stream,
      deviceId: streamInfo.deviceId,
    };

    return id;
  }

  public autoSelectDefaultDevices(): void {
    const dd = this._defaultDevices;

    dd.inputVideo = this._getDefaultDevice('inputVideo');
    dd.inputAudio = this._getDefaultDevice('inputAudio');
    dd.outputAudio = this._getDefaultDevice('outputAudio');

    saveInStoreDefaultDevices(dd);

    this._fire('mediaDevicesChange', { default: dd });
  }

  public getStreamById(streamId: string): MediaStream {
    const rec = this._currentInputStreams[streamId];

    return rec && rec.stream;
  }
}
