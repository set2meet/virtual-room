/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// Stop media stream track
export const stopTrack = (track: MediaStreamTrack): void => {
  if (track.readyState === 'live') {
    track.stop();
    // https://stackoverflow.com/questions/55953038/why-is-the-ended-event-not-firing-for-this-mediastreamtrack
    // Because ended is explicitly not fired when you call track.stop() yourself.
    // It only fires when a track ends for other reasons
    track.dispatchEvent(new Event('ended'));
  }
};
