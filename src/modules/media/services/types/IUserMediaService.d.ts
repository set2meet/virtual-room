/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IScreenSharingSupportInfo, IUserMediaDevicesInfo, IUserMediDeviceType } from '../../types/mediaTypes';
import { IScreenSharingService } from '../screenSharing/IScreenSharingService';

export type IUserMediaDevicesCollection = Record<IUserMediDeviceType, MediaDeviceInfo[]>;
export type IUserMediaServiceEventHandler = (evt: any) => void;

export type IUserMediaServiceEventName =
  | 'screenSharingStatusUpdate'
  | 'mediaDevicesChange'
  | 'mediaStreamChange'
  | 'mediaAccessError'
  | 'mediaAccessAllowed'
  | 'mediaStreamDevicesStopped'
  | 'defaultInputAudioDeviceChanged'
  | 'defaultInputVideoDeviceChanged'
  | 'defaultOutputAudioDeviceChanged';

export interface IUserMediaService {
  defaultInputAudioStreamId: string;
  defaultInputVideoStreamId: string;
  screenSharingService: IScreenSharingService;

  getVideoStreamTrack(streamId: string): MediaStreamTrack;
  getVideoStreamTrackByDeviceId(deviceId: string): MediaStreamTrack;
  getTabVideoTrack(): MediaStreamTrack;
  getAudioStreamTrack(streamId: string): MediaStreamTrack;

  defaultDevices: IUserMediaDevicesInfo;
  connectedDevices: IUserMediaDevicesCollection;
  screenSharingCapability: IScreenSharingSupportInfo;
  defaultInputVideo: MediaDeviceInfo;
  defaultInputAudio: MediaDeviceInfo;
  defaultOutputAudio: MediaDeviceInfo;

  getHTMLMediaElementForStream(streamId: string): HTMLVideoElement;
  getMicrophoneVolumeMeter(streamId?: string): HTMLElement;
  subscribe(eventName: IUserMediaServiceEventName, eventHandler: IUserMediaServiceEventHandler): void;
  unsubscribe(eventName: IUserMediaServiceEventName, eventHandler: IUserMediaServiceEventHandler): void;
  getStreamFromDesktop(): Promise<string>;
  getStreamFromDefaultDevices(getAudio: boolean, getVideo: boolean): Promise<MediaStream>;
  getStreamFromDefaultInputVideo(): Promise<string>;
  getStreamFromDefaultInputAudio(): Promise<string>;
  closeStreamFromVideoDevice(streamId: string): void;
  closeStreamFromAudioDevice(streamId: string): void;
  getDevicesList(): Promise<IUserMediaDevicesCollection>;
  cloneStream(streamId: string): string;
  autoSelectDefaultDevices(): void;
  getStreamById(streamId: string): MediaStream;
}
