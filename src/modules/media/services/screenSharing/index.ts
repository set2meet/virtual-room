/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import EventEmitter from 'events';
import { stopTrack } from '../utils/mediaTrack';
import { IScreenSharingService, ScreenSharingEventName } from './IScreenSharingService';

const md = navigator.mediaDevices as any;

const isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

// Edge dog-nail
// TODO::remove if we dont need support for old browsers (like Edge) anymore.
const oldNavigator = navigator as any;

if (!md.getDisplayMedia && typeof oldNavigator.getDisplayMedia !== 'undefined') {
  md.getDisplayMedia = () =>
    new Promise((resolve, reject) => {
      oldNavigator.getDisplayMedia().then(resolve).catch(reject);
    });
}

export class ScreenSharingService extends EventEmitter implements IScreenSharingService {
  private _streams: Record<string, MediaStream> = {};
  private _isSupported: boolean = !!md.getDisplayMedia;
  private _isBlockedByUser: boolean = false;

  public get isBlocked() {
    return this._isBlockedByUser;
  }

  public get isSupported() {
    return this._isSupported;
  }

  public getDisplayMedia(): Promise<MediaStream> {
    return new Promise((resolve, reject) => {
      if (!this._isSupported) {
        return reject('NotSupported');
      }
      if (this._isBlockedByUser) {
        return reject('NotAllowedError');
      }
      let streamCaptured: boolean = false;

      // @ts-ignore
      md.getDisplayMedia()
        .then((stream: MediaStream) => {
          streamCaptured = true;
          this._streams[stream.id] = stream;
          resolve(stream);
        })
        .catch((err: any) => {
          reject(err.name);

          let emitError: boolean = true;

          if (err.name === 'NotAllowedError') {
            if (isChrome) {
              emitError = false;
            }
            if (isFirefox) {
              this._isBlockedByUser = true;
            }
          }

          if (emitError) {
            this.emit('screenSharingStatusUpdate', {
              supported: this.isSupported,
              isBlocked: this.isBlocked,
              errorName: err.name,
            });
          }
        })
        .finally(() => {
          if (!streamCaptured) {
            reject('NotAllowedError');
          }
        });
    });
  }

  public stopStream(id: string) {
    const stream = this._streams[id];

    if (!stream) {
      return;
    }

    stream.getTracks().forEach(stopTrack);
    this._streams[id] = null;
  }

  public addEventListener(eventName: ScreenSharingEventName, eventListener: any) {
    this.on(eventName, eventListener);
  }

  public removeEventListener(eventName: ScreenSharingEventName, eventListener: any) {
    this.removeListener(eventName, eventListener);
  }

  public removeAllEventListeners() {
    this.removeAllListeners();
  }
}
