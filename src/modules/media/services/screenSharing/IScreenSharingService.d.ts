/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export type ScreenSharingEventName = 'screenSharingStatusUpdate';

export interface IScreenSharingService {
  isBlocked: boolean;
  isSupported: boolean;
  getDisplayMedia(): Promise<MediaStream>;
  stopStream(id: string): void;
  addEventListener(eventName: ScreenSharingEventName, eventListener: any): void;
  removeEventListener(eventName: ScreenSharingEventName, eventListener: any): void;
  removeAllEventListeners(): void;
}
