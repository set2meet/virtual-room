/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const LEVEL_POINT_MAX = 100;

const createElement = (name: string): HTMLElement => {
  return document.createElement(name);
};

export default class VolumeMeter {
  private _node: HTMLElement;
  private _meter: HTMLElement;
  private _isAnylizing: boolean;
  private _inputAnalizer: AnalyserNode;
  private _inputStreamNode: MediaStreamAudioSourceNode;

  constructor(stream: MediaStream) {
    this._node = createElement('div');
    this._meter = createElement('span');
    this._node.appendChild(this._meter);

    this.update(0);
    this.startAnalizeMicrophone(stream);
  }

  private _scheduleMicMeter() {
    setTimeout(() => requestAnimationFrame(this._showMicrophoneInputVolume), 0);
  }

  private _showMicrophoneInputVolume = () => {
    if (!this._isAnylizing) {
      return;
    }

    const array = new Uint8Array(this._inputAnalizer.frequencyBinCount);
    const length = array.length;
    let total = 0;

    this._inputAnalizer.getByteFrequencyData(array);

    for (let i = 0; i < length; i++) {
      total += array[i];
    }

    let level = Math.round(total / length);

    level = Math.min(level, LEVEL_POINT_MAX);

    if (this._meter) {
      this._meter.style.width = `${level}%`;
    }

    this._scheduleMicMeter();
  };

  public update(value: number) {
    this._meter.style.width = value + '%';
  }

  public get node(): HTMLElement {
    return this._node;
  }

  public stopAnalizeMicrophone(): void {
    this._isAnylizing = false;

    if (this._inputAnalizer) {
      this._inputAnalizer.disconnect();
    }

    if (this._inputStreamNode) {
      this._inputStreamNode.disconnect();
    }
  }

  public startAnalizeMicrophone(stream: MediaStream): void {
    const { webAudioContext } = clientEntityProvider;

    webAudioContext.resume().then(() => {
      this._isAnylizing = true;
      this._inputAnalizer = webAudioContext.createAnalyser();
      this._inputStreamNode = webAudioContext.createMediaStreamSource(stream);
      this._inputStreamNode.connect(this._inputAnalizer);

      setTimeout(this._showMicrophoneInputVolume, 1);
    });
  }
}
