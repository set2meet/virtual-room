/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _uniqBy from 'lodash/uniqBy';
import _debounce from 'lodash/debounce';
import { IUserMediaDevicesData, IUserMediDeviceType, UserMediaLocalStorage } from '../types/mediaTypes';
import LocalStorageDefaultDevice = UserMediaLocalStorage.DefaultDevice;
import { DefaultDevice } from './types/DefaultDevice';

const MD: MediaDevices = navigator.mediaDevices || ({} as MediaDevices); // safari 10.1 and older
const ON_DEVICE_CHANGE_TIME_INTERVAL = 500;

const DEFAULT_OUTPUT_AUDIO: MediaDeviceInfo = {
  groupId: '---',
  deviceId: '---',
  kind: 'audiooutput',
  label: 'Default speakers',
  toJSON: () => null,
};

export const DEVICES_MAP: Record<MediaDeviceKind, IUserMediDeviceType> = {
  videoinput: 'inputVideo',
  audioinput: 'inputAudio',
  audiooutput: 'outputAudio',
};

export class UserMediaDevicesData implements IUserMediaDevicesData {
  public inputVideo: MediaDeviceInfo[];
  public inputAudio: MediaDeviceInfo[];
  public outputAudio: MediaDeviceInfo[];

  constructor() {
    this.inputVideo = [];
    this.inputAudio = [];
    this.outputAudio = [];
  }
}

type UserMediaDeviceFilterProps = 'deviceId' | 'label';

const filterDefaultDevicesBy = (property: UserMediaDeviceFilterProps) => (device: MediaDeviceInfo) => {
  return device[property].toLowerCase().indexOf(DefaultDevice.label) !== -1;
};

const filterByIdAndLabel = (ldd: LocalStorageDefaultDevice) => (device: MediaDeviceInfo) => {
  return ldd ? device.deviceId === ldd.id : false;
};

const sortUserMediaDevices = (devices: MediaDeviceInfo[]): IUserMediaDevicesData => {
  return devices.reduce((sortedDevices: IUserMediaDevicesData, device: MediaDeviceInfo) => {
    sortedDevices[DEVICES_MAP[device.kind]].push(device);

    return sortedDevices;
  }, new UserMediaDevicesData());
};

const createDefaultDevice = (kind: MediaDeviceKind): MediaDeviceInfo => {
  return {
    get kind() {
      return kind;
    },
    get label() {
      return DefaultDevice.label;
    },
    get deviceId() {
      return DefaultDevice.deviceId;
    },
    get groupId() {
      return DefaultDevice.groupId;
    },
    toJSON() {
      return null;
    },
  };
};

const patchEmptyDevices = (devices: MediaDeviceInfo[]): void => {
  devices.forEach((od: MediaDeviceInfo, inx: number) => {
    // if original device had no id and label
    // replace on default device
    if (od.deviceId === '' && od.label === '') {
      devices.splice(inx, 1, createDefaultDevice(od.kind));
    }
  });
};

export const getDefaultDevice = (devices: MediaDeviceInfo[], lsDevice?: LocalStorageDefaultDevice): MediaDeviceInfo => {
  return (
    devices.filter(filterByIdAndLabel(lsDevice))[0] ||
    devices.filter(filterDefaultDevicesBy('deviceId'))[0] ||
    devices.filter(filterDefaultDevicesBy('deviceId'))[0] ||
    devices.filter(filterDefaultDevicesBy('label'))[0] ||
    devices[0] ||
    null
  );
};

type IUserDevicesCallback = (devices: IUserMediaDevicesData) => void;

export const getDevices = (callback: IUserDevicesCallback): void => {
  MD.enumerateDevices()
    .then((devices) => {
      const result = sortUserMediaDevices(devices);

      // patch output audio: all browsers lies about audio output
      result.outputAudio = [DEFAULT_OUTPUT_AUDIO];

      // remove devices with the same deviceId property
      result.inputAudio = _uniqBy(result.inputAudio, 'deviceId');
      result.inputVideo = _uniqBy(result.inputVideo, 'deviceId');

      // patch empty devices (from safari), replace on default device
      patchEmptyDevices(result.inputAudio);
      patchEmptyDevices(result.inputVideo);

      // return patched result
      callback(result);
    })
    .catch((err) => {
      callback({
        inputVideo: [],
        inputAudio: [],
        outputAudio: [],
        // todo: add errorMessage/errorType field
      });
    });
};

export const onDevicesChange = (callback: IUserDevicesCallback): void => {
  const onDeviceChange = _debounce(() => getDevices(callback), ON_DEVICE_CHANGE_TIME_INTERVAL);

  if (MD.addEventListener) {
    MD.addEventListener('devicechange', onDeviceChange);
  } else if (MD.ondevicechange) {
    MD.ondevicechange = onDeviceChange;
  }
};

export const getMediaStream = (constraints: MediaStreamConstraints): Promise<MediaStream> =>
  MD.getUserMedia(constraints);
