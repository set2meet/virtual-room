/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { UserMediaLocalStorage, IUserMediDeviceType } from '../types/mediaTypes';
import DefaultDevices = UserMediaLocalStorage.DefaultDevices;
import DefaultDevice = UserMediaLocalStorage.DefaultDevice;

const DEFAULT_DEVICE: DefaultDevice = {
  label: '-- default device ---',
  id: '--- default device id ---',
};

const saveData = (devices: DefaultDevices) => {
  const MEDIA_DEFAULT_DEVICES_KEY = clientEntityProvider.getStorageKeys().MEDIA_DEFAULT_DEVICES_KEY;
  clientEntityProvider.getStorageManager().set<DefaultDevices>(MEDIA_DEFAULT_DEVICES_KEY, devices);
};

const getDefaultDevices = (): DefaultDevices => {
  const MEDIA_DEFAULT_DEVICES_KEY = clientEntityProvider.getStorageKeys().MEDIA_DEFAULT_DEVICES_KEY;
  const devicesFromStorage = clientEntityProvider.getStorageManager().get<DefaultDevices>(MEDIA_DEFAULT_DEVICES_KEY);
  return {
    ...devicesFromStorage,
  };
};

const setDefaultDevice = (type: IUserMediDeviceType, device: DefaultDevice = DEFAULT_DEVICE): void => {
  const devicesFromStorage = getDefaultDevices();
  devicesFromStorage[type] = device;
  saveData(devicesFromStorage);
};

export default {
  setDefaultDevice,
  getDefaultDevices,
} as UserMediaLocalStorage.ILocalStorageService;
