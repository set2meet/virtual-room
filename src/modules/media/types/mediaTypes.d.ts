/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ComponentType } from 'react';
import { IModuleStorageKey } from '../../../ioc/common/ioc-interfaces';
import { ActionCreator, AnyAction } from 'redux';

/* common used types */

export type IUserMediDeviceType = 'inputVideo' | 'inputAudio' | 'outputAudio';

export type UserMediaErrorName =
  | 'AbortError'
  | 'NotAllowedError'
  | 'NotFoundError'
  | 'NotReadableError'
  | 'SecurityError'
  | 'DeviceUnplugged'
  | 'UnknownDefaultDevice'
  | 'NotSet';
/* redux */

export interface IUserMediaDevicesData {
  inputVideo: MediaDeviceInfo[];
  inputAudio: MediaDeviceInfo[];
  outputAudio: MediaDeviceInfo[];
}

export interface IUserMediaDevicesSettings {
  outputAudioLevel: number;
  videoExample: HTMLVideoElement | null;
  microphoneVolume: HTMLElement | null;
  inputVideoDeviceError: UserMediaErrorName;
  inputAudioDeviceError: UserMediaErrorName;
}

export interface IUserMediaDevicesInfo {
  inputVideo: MediaDeviceInfo;
  inputAudio: MediaDeviceInfo;
  outputAudio: MediaDeviceInfo;
}

export interface IUserUnpluggedMediaState {
  inputAudio: MediaDeviceInfo;
  inputVideo: MediaDeviceInfo;
}

export interface IMediaReduxState {
  devices: IUserMediaDevicesData;
  default: IUserMediaDevicesInfo;
  settings: IUserMediaDevicesSettings;
  unplugged: IUserUnpluggedMediaState;
}

/* screen sharing */

export interface IScreenSharingSupportInfo {
  supported: boolean;
  isBlocked?: boolean;
  errorName?: UserMediaErrorName;
}

export namespace UserMediaLocalStorage {
  export type DefaultDevice = {
    id: string;
    label: string;
  };

  export type DefaultDevices = Record<IUserMediDeviceType, DefaultDevice>;

  export interface ILocalStorageService {
    getDefaultDevices: () => DefaultDevices;
    setDefaultDevice: (type: IUserMediDeviceType, device: DefaultDevice) => void;
  }

  export interface IMediaStorageKeys extends Record<string, IModuleStorageKey<any>> {
    MEDIA_DEFAULT_DEVICES_KEY: IModuleStorageKey<DefaultDevices>;
  }
}

/* ui */

export interface IUIMediaDeviceSelectOwnProps {
  requestPermissions?: () => void;
  style?: React.StyleHTMLAttributes<HTMLElement>;
}

export interface IUIMediaDeviceTest {
  stream?: MediaStream;
}

export interface IUIMediaDeviceTestCamera extends IUIMediaDeviceTest {
  size?: { width: number; height: number };
}

export interface IUIMedia {
  MediaSettingsAudio: ComponentType<any>;
  MediaSettingsVideo: ComponentType<any>;
  UnpluggedDeviceWarning: ComponentType<any>;
  MediaDeviceSelectAudioInput: ComponentType<any>;
  MediaDeviceSelectVideoInput: ComponentType<any>;
  MediaDeviceAudioOutputExample: ComponentType<any>;
  MediaDeviceTestMicrophone: ComponentType<any>;
  MediaDeviceTestCamera: ComponentType<any>;
}

export interface IMediaActionCreators {
  initDevices: ActionCreator<AnyAction>;
  enableDeviceAccess: ActionCreator<AnyAction>;
  updateDevices: ActionCreator<AnyAction>;
  setDefaultDevices: ActionCreator<AnyAction>;
  setDefaultInputVideo: ActionCreator<AnyAction>;
  setDefaultInputAudio: ActionCreator<AnyAction>;
  defaultDevicesUpdateInfo: ActionCreator<AnyAction>;
  defaultDevicesWasUpdated: ActionCreator<AnyAction>;
  setUnpluggedDevices: ActionCreator<AnyAction>;
  ignoreUnpluggedDevices: ActionCreator<AnyAction>;
  replaceUnpluggedDevice: ActionCreator<AnyAction>;
  changeMediaSettingsOutputAudioLevel: ActionCreator<AnyAction>;
  changeMediaSettingsVideoExample: ActionCreator<AnyAction>;
  changeMediaSettingsMicrophoneMeter: ActionCreator<AnyAction>;
  changeMediaSettingsInputVideoStreamError: ActionCreator<AnyAction>;
  changeMediaSettingsInputAudioStreamError: ActionCreator<AnyAction>;
  requestAudioPermissions: ActionCreator<AnyAction>;
  requestVideoPermissions: ActionCreator<AnyAction>;
  closeMediaSettingsAudioStream: ActionCreator<AnyAction>;
  closeMediaSettingsVideoStream: ActionCreator<AnyAction>;
  getMediaSettingsAudioStream: ActionCreator<AnyAction>;
  getMediaSettingsVideoStream: ActionCreator<AnyAction>;
  autoSelectDefaultDevices: ActionCreator<AnyAction>;
}
