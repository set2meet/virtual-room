/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

const includeTrackStyle = () => `
  height: 2px;
  border-radius: 1px;
`;

export const styledAudioOutputLevelContainer = ({ theme }: IRequiredTheme) => `
  width: 100%;

  .input-range__track {
    &.input-range__track--active {
      ${includeTrackStyle()}
      background-color: ${theme.primaryColor};
    }

    &.input-range__track--background {
      ${includeTrackStyle()}
      margin-top: 0rem;
      position: relative;

      &:before {
        position: absolute;
        left: 0;
        top: -7px;
        height: 16px;
        content: '';
        display: inline-block;
        border: 0;
        width: 100%;
        background: transparent;
      }
    }
  }

  .input-range__slider {
    transition: none;
    margin-top: -0.55rem;
    background-color: #fff;
    border: 2px solid ${theme.primaryColor};
  }

  .input-range__label-container {
    display: none;
  }
`;
