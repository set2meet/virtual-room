import { styledAudioOutputLevelContainer } from './MediaSettingsAudioLevelOutput.style';
import styled, { withTheme } from 'styled-components';
import 'react-input-range/lib/css/index.css';
import React from 'react';
import InputRange from 'react-input-range';

const DEFAULT_VOLUME_STEP = 0.001;

const AudioLevelOutputContainer = withTheme(
  styled.div`
    ${styledAudioOutputLevelContainer};
  `
);

interface IMediaSettingsAudioLevelOutputProps {
  value: number;
  onChange: (value: number) => void;
}

class MediaSettingsAudioLevelOutput extends React.Component<IMediaSettingsAudioLevelOutputProps> {
  private _changeValue = (value: number) => {
    this.props.onChange(value);
  };

  public render() {
    return (
      <AudioLevelOutputContainer>
        <InputRange
          maxValue={1}
          minValue={0}
          step={DEFAULT_VOLUME_STEP}
          value={this.props.value}
          onChange={this._changeValue}
        />
      </AudioLevelOutputContainer>
    );
  }
}

export default MediaSettingsAudioLevelOutput;
