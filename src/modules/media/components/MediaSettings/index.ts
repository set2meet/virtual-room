/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { css } from 'styled-components';

const leftPanelWidth = '184px';
const blockLineHeight = '50px';
const blockPadding = '10px';
const blockWidth = '210px';

const includeBlockLineCommonStyle = () => css`
  padding: 0 ${blockPadding};
  display: inline-flex;
  align-items: center;
`;

export const styledContainer = () => css`
  border: 0;
  padding: 0;
  margin: 10px 0;
  width: 100%;
  height: calc(100% - 20px);
  color: #fff;
  vertical-align: top;
  display: inline-block;

  > hr {
    border: none;
    height: 25px;
    width: 100%;
    margin: 0;
    background: transparent;
    text-align: center;

    &:before {
      visibility: hidden;
      width: 70%;
      height: 2px;
      // possibly aint used see mockups but ill save it anyway 25.03.19 Ruslan
      //display: inline-block;
      //content: '';
      //position: relative;
      //top: 10px;
      //background: linear-gradient(to right, rgba(51,51,51,1) 0%, #575757 50%,rgba(51,51,51,1) 100%);
    }
  }
`;

export const styledBlock = () => css`
  border: 0;
  padding: 0;
  margin: 10px 0;
  height: ${blockLineHeight};

  > div:first-child {
    ${includeBlockLineCommonStyle()};
    width: ${leftPanelWidth};
    height: inherit;
    justify-content: flex-end;
    vertical-align: middle;
  }

  > div:last-child {
    ${includeBlockLineCommonStyle()};
    width: calc(100% - ${leftPanelWidth});
    height: 30px;
    justify-content: flex-start;
    vertical-align: middle;
    max-width: calc(${blockWidth} + 2 * ${blockPadding});
  }
`;

export const styledVideoExample = () => css`
  max-width: 100%;
  position: relative;
  height: calc(100% - ${blockLineHeight} - 2 * ${blockPadding});
  display: flex;
  align-items: center;
  justify-content: center;

  > video {
    max-height: 80%;
    display: inline-block;
    position: relative;
    z-index: 2;
  }

  > div {
    z-index: 1;
    position: absolute;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const styledInputStreamError = () => `

`;

const NO_CAMERA_AVATAR_SIZE = 60;
const VIDEO_EXAMPLE: {
  WIDTH: number;
  HEIGHT: number;
} = {
  WIDTH: 320,
  HEIGHT: 180,
};

export const styledEmptyElement = () => {
  const icons = clientEntityProvider.getIcons();
  return css`
    width: ${VIDEO_EXAMPLE.WIDTH}px;
    height: ${VIDEO_EXAMPLE.HEIGHT}px;
    background-color: #1f1f1f;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    border: 0;

    &:before {
      content: '';
      border: 0;
      margin: 0;
      padding: 0;
      position: absolute;
      top: calc(50% - ${NO_CAMERA_AVATAR_SIZE / 2}px);
      left: calc(50% - ${NO_CAMERA_AVATAR_SIZE / 2}px);
      border-radius: 50%;
      border: ${NO_CAMERA_AVATAR_SIZE / 2}px solid #686868;
      width: 0;
      height: 0;
      z-index: 0;
    }

    &:after {
      font-family: ${icons.fontFamily};
      color: ${(props) => props.theme.primaryTextColor};
      content: ${icons.code.user};
      font-size: 26px;
      z-index: 1;
      position: relative;
    }
  `;
};

export const styledNoMicrophone = () => css`
  width: 100%;
  height: 100%;
  background: transparent;
  display: inline-flex;
  align-items: center;

  &:after {
    content: '';
    height: 2px;
    display: inline-block;
    width: 100%;
    background-color: ${(props) => props.theme.primaryTextColor};
  }
`;
