import React from 'react';
import browser from '../../services/browser';
import { UserMediaErrorName } from '../../types/mediaTypes';

type OnRetryCallbackHandler = React.MouseEventHandler;

const defaultChangePermissionAction = (name: TemplateStringsArray): string =>
  ` Use the ${name} button in the address bar to fix this.`;

const DEVICE_USER_ACTION: Record<string, string> = {
  camera: 'see',
  microphone: 'hear',
};
const DEVICE_USER_CHANGE_PERMISSIONS: Record<string, string> = {
  chrome: defaultChangePermissionAction`camera`,
  firefox: defaultChangePermissionAction`camera`,
  edge: defaultChangePermissionAction`lock`,
  safari: '',
};

const onRetryLink = (onRetry: OnRetryCallbackHandler): React.ReactFragment => (
  <React.Fragment>
    <a onClick={onRetry}>click here</a>
  </React.Fragment>
);

const MEDIA_DEVICE_ERROR_TEXT: Record<
  UserMediaErrorName,
  (device: string, onRetry: OnRetryCallbackHandler) => React.ReactFragment
> = {
  AbortError: (device, onRetry) => (
    <React.Fragment>
      {`A problem occured. Please make sure your media device is on and`} {onRetryLink(onRetry)}
    </React.Fragment>
  ),
  NotAllowedError: (device) => (
    <React.Fragment>
      {`You have not granted permission to use your ${device}. `}
      {`You can still join the conference but others won't ${DEVICE_USER_ACTION[device]} you.`}
      {DEVICE_USER_CHANGE_PERMISSIONS[browser.name]}
    </React.Fragment>
  ),
  NotFoundError: () => (
    <React.Fragment>{`The device was not found. Please make sure it is plugged in.`}</React.Fragment>
  ),
  NotReadableError: (device, onRetry) => (
    <React.Fragment>
      {`This ${device} is currently used by another application. Please make sure it is released and `}
      {onRetryLink(onRetry)}
      {` or choose another device.`}
    </React.Fragment>
  ),
  SecurityError: (device, onRetry) => (
    <React.Fragment>
      {`A problem occured. Please make sure you have granted access to use the ${device} and`} {onRetryLink(onRetry)}
    </React.Fragment>
  ),
  DeviceUnplugged: (device) => (
    <React.Fragment>
      {`The device has been disabled. Please make sure it is plugged in, or select another`}
    </React.Fragment>
  ),
  UnknownDefaultDevice: () => <></>,
  NotSet: () => '',
};

export default MEDIA_DEVICE_ERROR_TEXT;
