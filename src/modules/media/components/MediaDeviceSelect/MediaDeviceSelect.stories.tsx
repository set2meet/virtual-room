import React from 'react';
import { getFakeVideoDevicesInfo } from '../../../../test/mocks/media';
import { text } from '@storybook/addon-knobs';
import { MediaDeviceSelect } from '../index';

export default {
  title: 'media/MediaDeviceSelect',
  component: MediaDeviceSelect,
};

export const Default = () => {
  const inputVideos = getFakeVideoDevicesInfo(2);

  return <MediaDeviceSelect title={text('title', 'title')} devices={inputVideos} />;
};

export const EmptyDevices = () => <MediaDeviceSelect title={text('title', 'title')} devices={[]} />;

export const UndefinedDevices = () => <MediaDeviceSelect title={text('title', 'title')} devices={null} />;

export const SecurityError = () => {
  const inputVideos = getFakeVideoDevicesInfo(2);

  return <MediaDeviceSelect title={text('title', 'title')} errorName="SecurityError" devices={inputVideos} />;
};

export const WithDefaultDevice = () => {
  // tslint:disable-next-line:no-magic-numbers
  const inputVideos = getFakeVideoDevicesInfo(3);
  // tslint:disable-next-line:no-magic-numbers
  const defaultDevice = inputVideos[3];

  return <MediaDeviceSelect title={text('title', 'title')} devices={inputVideos} default={defaultDevice} />;
};
