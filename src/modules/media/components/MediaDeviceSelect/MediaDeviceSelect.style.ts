/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { rgba } from 'polished';
import { IRequiredTheme, ITheme } from '../../../../ioc/client/types/interfaces';

const zIndex = 100;
const baseHeight = '30px';
const borderSize = '1px';
const defaultPadding = '0 6px';
const containerWidth = `calc(100% - 2 * ${borderSize})`;
const containerHeight = `calc(100% - 2 * ${borderSize})`;

const includeOverflowText = () => `
  width: 100%;
  overflow: hidden;
  user-select: none;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const getBorderColor = (theme: ITheme, hasError: boolean): string => {
  if (hasError) {
    return theme.buttonWarningColor;
  } else {
    return theme.primaryColor;
  }
};

export const styledTextContainer = () => `
  ${includeOverflowText()};
`;

export interface IStyledDropDownContainerProps extends IRequiredTheme {
  hasError?: boolean;
  disabled?: boolean;
}

export const styledDropDownContainer = ({
  theme,
  disabled = false,
  hasError = false,
}: IStyledDropDownContainerProps) => {
  const icons = clientEntityProvider.getIcons();
  return `
    padding: 0;
    display: inline-block;
    box-sizing: content-box;
    width: ${containerWidth};
    height: ${containerHeight};
    background: ${theme.backgroundColorSecondary};
    border: ${borderSize} solid ${getBorderColor(theme, hasError)};
    position: relative;
  
    &.empty {
      border: 1px solid #faf9f5;
      display: inline-flex;
      align-items: center;
      justify-content: flex-start;
      opacity: 0.6;
      user-select: none;
      pointer-events: none;
  
      > span {
        text-align: center;
        color: #faf9f5;
      }
    }
  
    > .dropdown {
      width: 100%;
      height: 100%;
  
      > button {
        &.btn[disabled] {
          opacity: 1;
        }
  
        padding: 0;
        width: 100%;
        height: 100%;
        border: none;
        text-align: left;
        display: inline-flex;
        align-items: center;
        background: transparent;
        color:  ${theme.primaryTextColor};
  
        &.dropdown-toggle,
        &.dropdown-toggle.btn-default:focus,
        &.dropdown-toggle.btn-default:hover {
          color:  ${theme.primaryTextColor};
          border: none;
          background-color: transparent;
        }
  
        > span:not(.caret) {
          width: 100%;
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;
          display: inline-block;
          padding: ${defaultPadding};
          color: ${theme.primaryTextColor};
        }
  
        > span.caret {
          margin-left: 0;
          border: 0;
          width: 24px;
          position: relative;
          display: ${disabled ? 'none' : 'inline-flex'};
          align-items: center;
  
          &:before {
            font-size: 26px;
            color: ${theme.primaryColor};
            content: ${icons.code.arrowDown};
            font-family: ${icons.fontFamily};
          }
        }
      }
  
      > .dropdown-menu {
        min-width: 100%;
        padding: 0;
        border-radius: 0;
        box-shadow: none;
        z-index: ${zIndex + 10};
        border: ${borderSize} solid ${theme.primaryColor};
        background-color: ${theme.backgroundColorSecondary};
  
        > li {
          height: ${baseHeight};
  
          > a {
            height: ${baseHeight};
            line-height: ${baseHeight};
            padding: ${defaultPadding};
            color: ${theme.primaryTextColor};
            display: inline-block;
            ${styledTextContainer()};
    
            &:hover {
              background-color: ${theme.primaryColor};
            }
          }
        }
      }
    }
  `;
};

export const styledDeviceError = ({ theme }: IRequiredTheme) => `
  position: absolute;
  z-index: ${zIndex};
  top: calc(100% + 6px);
  left: 0;
  width: 100%;
  padding: 6px;
  font-size: 10px;
  color: ${theme.buttonWarningColor};
  background-color: ${/* tslint:disable */ rgba(theme.buttonWarningColor, 0.2) /* tslint:enable */};
`;
