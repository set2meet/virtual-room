import _find from 'lodash/find';
import React, { SyntheticEvent } from 'react';
import styled from 'styled-components';
import MEDIA_DEVICE_ERROR_TEXT from './MediaDeviceErrorText';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import {
  styledTextContainer,
  styledDropDownContainer,
  styledDeviceError,
  IStyledDropDownContainerProps,
} from './MediaDeviceSelect.style';
import { UserMediaErrorName, IUIMediaDeviceSelectOwnProps } from '../../types/mediaTypes';

const DROPDOWN_ID = 'media-setup-dropdown-menu';
const StyledDeviceError = styled.div`
  ${styledDeviceError};
`;
const StyledTextContainer = styled.span`
  ${styledTextContainer};
`;
const StyledDropDownContainer = styled.div<IStyledDropDownContainerProps>`
  ${styledDropDownContainer};
`;

export interface IMediaDeviceSelectProps extends IUIMediaDeviceSelectOwnProps {
  theme?: any;
  errorName?: UserMediaErrorName;
  default?: MediaDeviceInfo;
  title: string;
  disabled?: boolean;
  devices: MediaDeviceInfo[];
  onChange?: (device: MediaDeviceInfo) => void;
}

interface IDeviceChoice {
  id: string;
  label: string;
}

interface IMediaDeviceSelectState {
  isOpened: boolean;
  devicesList: IDeviceChoice[];
  defaultDeviceName: string;
}

class MediaDeviceSelect extends React.Component<IMediaDeviceSelectProps, IMediaDeviceSelectState> {
  public static defaultProps = {
    disabled: false,
  };

  private _updateData = () => {
    this.setState(this._buildStateData());
  };

  private _toggleVisibility = () => {
    this.setState({ isOpened: !this.state.isOpened });
  };

  private _buildStateData = () => {
    const devices = this.props.devices || [];
    const devicesList = devices.map((device: MediaDeviceInfo, inx: number) => {
      return {
        id: device.deviceId,
        label: device.label || `${this.props.title} ${inx + 1}`,
      };
    });
    const defaultDeviceName = this._getDefaultDeviceName(devicesList);

    return {
      devicesList,
      defaultDeviceName,
    };
  };

  private _getDefaultDeviceName = (devicesList: IDeviceChoice[]): string => {
    const defaultDevice = this.props.default;
    const origDevices = this.props.devices || [];

    if (!defaultDevice) {
      return '';
    }

    if (defaultDevice.label) {
      return defaultDevice.label;
    }

    const inx = origDevices.map((item) => item.deviceId).indexOf(defaultDevice.deviceId);

    return inx > -1 ? devicesList[inx].label : defaultDevice.label || '...loading';
  };

  private _changeDefaultDevice = (deviceId: any) => {
    const { devices, onChange } = this.props;
    const defaulDevice = this.props.default;

    if (!onChange || (defaulDevice && defaulDevice.deviceId === deviceId)) {
      return;
    }

    onChange(_find(devices, { deviceId }) as MediaDeviceInfo);
  };

  private _retryGetUserMediaPermissions = (evt: SyntheticEvent) => {
    evt.preventDefault();
    this.props.requestPermissions();
  };

  private _renderDeviceError(title: string, errName: UserMediaErrorName) {
    if (errName === 'NotSet' || !errName) {
      return null;
    }

    return (
      <StyledDeviceError>
        {MEDIA_DEVICE_ERROR_TEXT[errName](title, this._retryGetUserMediaPermissions)}
      </StyledDeviceError>
    );
  }

  constructor(props: IMediaDeviceSelectProps) {
    super(props);

    this.state = {
      isOpened: false,
      ...this._buildStateData(),
    };
  }

  public componentDidUpdate(prevProps: IMediaDeviceSelectProps) {
    if (prevProps.devices !== this.props.devices || prevProps.default !== this.props.default) {
      this._updateData();
    }
  }

  public render() {
    const { devices, title, style } = this.props;
    let { disabled, errorName } = this.props;
    let { defaultDeviceName } = this.state;
    const defaultDevice = this.props.default;
    const noDevices = devices && devices.length === 0;
    const devicesNotDefined = !devices;

    if (noDevices && !errorName) {
      disabled = true;
      defaultDeviceName = '';
      errorName = 'NotFoundError';
    } else if (devicesNotDefined) {
      disabled = true;
      defaultDeviceName = '... Loading';
    } else if (errorName === 'UnknownDefaultDevice') {
      // ignore this error
      errorName = null;
    } else if (errorName === 'NotSet') {
      // ignore this error
      errorName = null;
    }

    const bttnProps =
      defaultDevice && defaultDevice.kind === 'audiooutput'
        ? {
            'data-tooltip': 'Default system speakers',
            'data-tooltip-pos': 'down',
          }
        : {};
    const defaultDeviceId = defaultDevice ? defaultDevice.deviceId : '-';

    return (
      <StyledDropDownContainer hasError={!!errorName} disabled={disabled} style={style}>
        <DropdownButton
          {...bttnProps}
          id={DROPDOWN_ID}
          disabled={disabled}
          title={
            <React.Fragment>
              <StyledTextContainer>{defaultDeviceName}</StyledTextContainer>
            </React.Fragment>
          }
          open={this.state.isOpened}
          onToggle={this._toggleVisibility}
          onSelect={this._changeDefaultDevice}
        >
          {this.state.devicesList.map((item: IDeviceChoice, inx) => (
            <MenuItem key={item.id} value={item.id} eventKey={item.id} active={item.id === defaultDeviceId}>
              <span>{item.label}</span>
            </MenuItem>
          ))}
        </DropdownButton>
        {this._renderDeviceError(title.toLowerCase(), errorName)}
      </StyledDropDownContainer>
    );
  }
}

export default MediaDeviceSelect;
