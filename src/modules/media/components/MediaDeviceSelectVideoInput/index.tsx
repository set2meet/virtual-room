import _noop from 'lodash/noop';
import * as React from 'react';
import { connect } from 'react-redux';
import { actionCreators } from '../../redux/actionCreators';
import { IUIMediaDeviceSelectOwnProps } from '../../types/mediaTypes';
import MediaDeviceSelect, { IMediaDeviceSelectProps } from '../MediaDeviceSelect';

export const MediaDeviceSelectVideoInput = (props: IMediaDeviceSelectProps) => <MediaDeviceSelect {...props} />;

export default connect<
  IMediaDeviceSelectProps,
  Pick<IMediaDeviceSelectProps, 'onChange' | 'requestPermissions'>,
  IUIMediaDeviceSelectOwnProps
>(
  (state: any) => ({
    title: 'Camera',
    default: state.media.default.inputVideo,
    devices: state.media.devices.inputVideo,
    errorName: state.media.settings.inputVideoDeviceError,
  }),
  (dispatch: any, ownProps: IUIMediaDeviceSelectOwnProps) => {
    const requestPermissionsOwn = ownProps.requestPermissions || _noop;
    const clearInputError = () => dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(null));

    return {
      requestPermissions: () => {
        clearInputError();
        requestPermissionsOwn();
      },
      onChange: (...args: any[]) => {
        dispatch(actionCreators.setDefaultInputVideo.apply(null, args));
      },
    };
  }
)(MediaDeviceSelectVideoInput);
