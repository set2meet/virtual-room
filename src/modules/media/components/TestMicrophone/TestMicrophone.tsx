import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IUIMediaDeviceTest } from '../../types/mediaTypes';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { styledContainer, IMicrophoneMeter, styledMicrophoneMeter } from './TestMicrophone.style';

const LEVEL_POINT_MAX = 100;
const TestDeviceContainer = styled.div`
  ${styledContainer};
`;
const MicrophoneMeter = styled.div<IMicrophoneMeter>`
  ${styledMicrophoneMeter};
`;

interface ITestMicrophoneStateProps {
  error: boolean;
}

export type ITestMicrophoneProps = IUIMediaDeviceTest & ITestMicrophoneStateProps;

export class TestMicrophone extends React.Component<ITestMicrophoneProps> {
  private _isAnylizing: boolean;
  private _inputAnalizer: AnalyserNode;
  private _inputStreamNode: MediaStreamAudioSourceNode;
  private _refMicVolume = React.createRef<HTMLSpanElement>();

  private get _volumeNode(): HTMLSpanElement {
    return this._refMicVolume.current;
  }

  private _scheduleMicMeter() {
    setTimeout(() => requestAnimationFrame(this._showMicrophoneInputVolume), 0);
  }

  private _showMicrophoneInputVolume = () => {
    if (!(this._isAnylizing && this._volumeNode)) {
      return;
    }

    const array = new Uint8Array(this._inputAnalizer.frequencyBinCount);
    const length = array.length;
    let total = 0;

    this._inputAnalizer.getByteFrequencyData(array);

    for (let i = 0; i < length; i++) {
      total += array[i];
    }

    let level = Math.round(total / length);

    level = Math.min(level, LEVEL_POINT_MAX);

    this._volumeNode.style.width = `${level}%`;

    this._scheduleMicMeter();
  };

  private _startAnalizeMicrophone(): void {
    const { webAudioContext } = clientEntityProvider;

    webAudioContext.resume().then(() => {
      const { stream } = this.props;
      const track = stream.getAudioTracks()[0];

      track.onended = () => this._stopAnalizeMicrophone();

      this._isAnylizing = true;
      this._inputAnalizer = webAudioContext.createAnalyser();
      this._inputStreamNode = webAudioContext.createMediaStreamSource(stream);
      this._inputStreamNode.connect(this._inputAnalizer);

      setTimeout(this._showMicrophoneInputVolume, 1);
    });
  }

  private _stopAnalizeMicrophone(): void {
    this._isAnylizing = false;

    if (this._inputAnalizer) {
      this._inputAnalizer.disconnect();
    }

    if (this._inputStreamNode) {
      this._inputStreamNode.disconnect();
    }

    this._inputAnalizer = null;
    this._inputStreamNode = null;

    if (this._volumeNode) {
      this._volumeNode.style.width = '';
    }
  }

  public componentDidMount() {
    if (this.props.stream) {
      this._startAnalizeMicrophone();
    }
  }

  public componentDidUpdate(prevProps: ITestMicrophoneProps) {
    const currStream = this.props.stream;
    const streamChanged = currStream !== prevProps.stream;

    if (currStream && streamChanged) {
      this._startAnalizeMicrophone();
    } else if (!currStream && streamChanged) {
      this._stopAnalizeMicrophone();
    }
  }

  public render() {
    const { stream, error } = this.props;
    const { UI } = clientEntityProvider.getComponentProvider();
    const loading = !error && !stream;

    return (
      <TestDeviceContainer>
        {loading ? (
          <UI.Spinner />
        ) : (
          <MicrophoneMeter empty={error}>
            <span ref={this._refMicVolume} />
          </MicrophoneMeter>
        )}
      </TestDeviceContainer>
    );
  }
}

export default connect<ITestMicrophoneStateProps>((state: any) => {
  const devices = state.media.devices.inputAudio;

  return {
    error: !!((devices && devices.length === 0) || state.media.settings.inputAudioDeviceError),
  };
})(TestMicrophone);
