/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

const microphoneIconSize = 20;
const noMicrophoneColor = '#565656';

export interface IMicrophoneMeter extends IRequiredTheme {
  empty?: boolean;
}

export const styledContainer = () => `
  height: 32px;
  width: 100%;

  > .spinner {
    transform-origin: 0 0 0;
    transform: scale(0.5, 0.5);
  }
`;

export const styledMicrophoneMeter = ({ theme, empty }: IMicrophoneMeter) => {
  const icons = clientEntityProvider.getIcons();

  return `
    width: 100%;
    height: 100%;
    align-items: center;
    display: inline-flex;
  
    &:before {
      width: ${microphoneIconSize}px;
      display: inline-block;
      height: 100%;
      margin-right: 10px;
      text-align: left;
      color: ${empty ? noMicrophoneColor : theme.primaryColor};
      content: ${icons.code.microphone};
      font-family: ${icons.fontFamily};
      font-size: 26px;
      position: relative;
      top: -2px;
      left: -6px;
    }
  
    > span {
      height: 5px;
      display: inline-block;
      background-color: ${empty ? noMicrophoneColor : theme.primaryColor};
      ${empty && `width: calc(100% - ${microphoneIconSize}px)`};
    }
  `;
};
