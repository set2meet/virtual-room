/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

export const VIDEO_SIZE = {
  WIDTH: 200,
  HEIGHT: 120,
};

const NO_CAMERA_AVATAR_SIZE = 60;

export const styledContainer = () => `
  width: ${VIDEO_SIZE.WIDTH}px;
  height: ${VIDEO_SIZE.HEIGHT}px;
  overflow: hidden;
  position: relative;
  display: inline-flex;
  align-items: center;
  justify-content: center;

  > video {
    top: 0;
    left: 0;
    z-index: 2;
    position: absolute;

    &.mode-v {
      width: auto;
      height: 100%;
      transform: translate(-50%, 0);
      left: 50%;
    }

    &.mode-h {
      width: 100%;
      height: auto;
      transform: translate(0, -50%);
      top: 50%;
    }
  }
`;

export const styledNoCameraStub = ({ theme }: IRequiredTheme) => {
  const icons = clientEntityProvider.getIcons();

  return `
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: #1f1f1f;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  
    &:before {
      content: '';
      border: 0;
      margin: 0;
      padding: 0;
      position: absolute;
      top: calc(50% - ${NO_CAMERA_AVATAR_SIZE / 2}px);
      left: calc(50% - ${NO_CAMERA_AVATAR_SIZE / 2}px);
      border-radius: 50%;
      border: ${NO_CAMERA_AVATAR_SIZE / 2}px solid #686868;
      width: 0;
      height: 0;
      z-index: 0;
    }
  
    &:after {
      font-family: ${icons.fontFamily};
      color: ${theme.primaryTextColor};
      content: ${icons.code.user};
      font-size: 26px;
      z-index: 1;
      position: relative;
    }
  `;
};
