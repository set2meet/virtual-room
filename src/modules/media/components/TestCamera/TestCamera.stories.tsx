import React from 'react';
import { connectDecorator } from '../../../../../.storybook/decorators/reduxDecorators';
import { getFakeVideoDevicesInfo } from '../../../../test/mocks/media';
import { actionCreators } from '../../redux/actionCreators';
import MediaDeviceTestCamera from './index';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IStoreState } from '../../../../ioc/client/types/interfaces';
import withStoryRootIndicator from '../../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

export default {
  title: 'media/TestCamera',
  component: MediaDeviceTestCamera,
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const Default = () => <MediaDeviceTestCamera />;

export const WithCameraStream = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  const inputVideos = getFakeVideoDevicesInfo(1);
  props.dispatch(actionCreators.updateDevices(inputVideos));
  props.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(null));
  props.dispatch(appActionCreators.webphoneRequestStreamFromCamera());
  return <MediaDeviceTestCamera stream={props.stream} />;
};

WithCameraStream.story = {
  decorators: [
    connectDecorator((state: IStoreState) => ({
      stream: state.webphone.userSetup.cameraStream,
    })),
    withStoryRootIndicator,
    withModule(ModuleRegistryKey.Webphone),
  ],
};

export const Loading = (props) => {
  const inputVideos = getFakeVideoDevicesInfo(1);
  props.dispatch(actionCreators.updateDevices(inputVideos));
  props.dispatch(actionCreators.changeMediaSettingsInputVideoStreamError(null));

  return <MediaDeviceTestCamera />;
};

Loading.story = {
  decorators: [connectDecorator()],
};
