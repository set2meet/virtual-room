import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IUIMediaDeviceTest } from '../../types/mediaTypes';
import { VIDEO_SIZE, styledContainer, styledNoCameraStub } from './TestCamera.style';

const VIDEO_SIZE_COEF = VIDEO_SIZE.WIDTH / VIDEO_SIZE.HEIGHT;
const TestDeviceContainer = styled.div`
  ${styledContainer};
`;
const NoCameraStub = styled.div`
  ${styledNoCameraStub};
`;

interface ITestCameraStateProps {
  error: boolean;
}

interface ITestCameraState {
  isStreamReady: boolean;
}

export type ITestCameraProps = IUIMediaDeviceTest & ITestCameraStateProps;

export class TestCamera extends React.Component<ITestCameraProps, ITestCameraState> {
  private _videoElement: HTMLVideoElement = null;

  private _refHtmlVideoElement = (node: HTMLVideoElement) => {
    if (node !== this._videoElement) {
      this._videoElement = node;
      this._attachVideoStream();
    }
  };

  private _attachVideoStream() {
    const { stream } = this.props;
    const video = this._videoElement;

    this.setState({ isStreamReady: false });

    if (!(video && stream)) {
      return;
    }

    const track = stream.getVideoTracks()[0];

    if (track) {
      track.onended = this._hideVideoStream;
    }

    video.onloadedmetadata = this._updateVideoElementMode;
    video.oncanplaythrough = this._showVideoStream;
    video.srcObject = stream;
  }

  private _updateVideoElementMode = () => {
    this.setState({ isStreamReady: true });

    const video = this._videoElement;

    if (!video) {
      return;
    }

    const vCoeff = video.videoWidth / video.videoHeight;

    video.className = vCoeff > VIDEO_SIZE_COEF ? 'mode-v' : 'mode-h';
  };

  private _updateVideoElementOpacity(opacity: '0' | '1') {
    if (this._videoElement) {
      this._videoElement.style.opacity = opacity;
    }
  }

  private _showVideoStream = () => {
    this._updateVideoElementOpacity('1');

    if (this._videoElement) {
      this._videoElement.play();
    }
  };

  private _hideVideoStream = () => {
    const video = this._videoElement;

    this._updateVideoElementOpacity('0');

    if (video) {
      video.onloadedmetadata = null;
      video.oncanplaythrough = null;
      video.className = '';
    }
  };

  constructor(props: ITestCameraProps) {
    super(props);

    this.state = {
      isStreamReady: false,
    };
  }

  public componentDidUpdate(prevProps: ITestCameraProps) {
    const currStream = this.props.stream;
    const streamChanged = currStream !== prevProps.stream;

    if (currStream && streamChanged) {
      this._attachVideoStream();
    }
    if (!currStream && streamChanged) {
      this._hideVideoStream();
    }
  }

  public componentWillUnmount() {
    const { stream } = this.props;
    const video = this._videoElement;

    if (stream) {
      stream.getVideoTracks()[0].onended = null;
    }

    if (video) {
      video.onloadedmetadata = null;
      video.oncanplaythrough = null;
    }
  }

  public render() {
    const { UI } = clientEntityProvider.getComponentProvider();
    const { stream, error } = this.props;
    const loading = !error && !(stream && this.state.isStreamReady);

    return (
      <TestDeviceContainer>
        {error && <NoCameraStub />}
        {loading && <UI.Spinner />}
        <video ref={this._refHtmlVideoElement} style={{ opacity: 0 }} autoPlay={true} />
      </TestDeviceContainer>
    );
  }
}

export default connect<ITestCameraStateProps>((state: any) => {
  const devices = state.media.devices.inputVideo;

  return {
    error: !!((devices && devices.length === 0) || state.media.settings.inputVideoDeviceError),
  };
})(TestCamera);
