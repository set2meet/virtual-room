/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { css } from 'styled-components';
import { IRequiredTheme, ITheme } from '../../../../ioc/client/types/interfaces';

const bttnPlaySize = '20px';

export type AudioOutputLevelSampleStyle = 'media-settings' | 'user-setup';

export interface IAudioOutputLevelSampleProps extends IRequiredTheme {
  mode: AudioOutputLevelSampleStyle;
}

export const styledAudioOutputLevelSample = ({ theme, mode }: IAudioOutputLevelSampleProps) => {
  const icons = clientEntityProvider.getIcons();

  // TODO::ref wtf is this component oh my god @Ruslan
  const modeStyleSpecification: Record<AudioOutputLevelSampleStyle, (theme: ITheme) => string> = {
    'media-settings': (specTheme: ITheme) => `
    > button {
      &:before {
        content: ${icons.code.play};
      }

      &.on:before {
        content: ${icons.code.pause};
      }
    }

    > div {
      height: 2px;
    }
  `,
    'user-setup': (specTheme: ITheme) => `
    display: inline-flex;
    align-items: center;

    > button {
      top: 1px;
      margin-right: 10px;
      display: inline-block;
      position: relative;

      &:before {
        content: ${icons.code.speaker};
      }

      &.on:before {
        color: ${specTheme.primaryColor};
      }
    }
  
    > div {
      top: 0;
      height: 5px;
      width: calc(100% - ${bttnPlaySize} - 10px);
    }
  `,
  };

  return css`
    width: 100%;

    > button {
      vertical-align: middle;
      position: relative;

      &:not(:disabled) {
        cursor: pointer;
      }

      border: 0;
      margin: 0;
      padding: 0;
      outline: none;
      background: transparent;
      width: ${bttnPlaySize};
      height: ${bttnPlaySize};

      &:before {
        position: absolute;
        top: -6px;
        left: -6px;
        font-size: 24px;
        font-family: ${icons.fontFamily};
        color: ${theme.primaryColor};
      }
    }

    > div {
      vertical-align: middle;
      display: inline-block;
      width: calc(100% - ${bttnPlaySize});
      background-color: #fff;
      border-radius: 1px;
      position: relative;
      top: 2px;

      > span {
        display: inline-block;
        height: inherit;
        border-radius: inherit;
        background-color: ${theme.primaryColor};

        //todo::ref possible refactor later

        position: absolute;
        top: 0px;
      }
    }

    ${modeStyleSpecification[mode](theme)};
  `;
};
