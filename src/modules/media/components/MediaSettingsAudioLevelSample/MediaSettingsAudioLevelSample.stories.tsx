import React from 'react';
import { select } from '@storybook/addon-knobs';
import { AudioOutputLevelSampleStyle } from './MediaSettingsAudioLevelSample.style';
import { MediaDeviceAudioOutputExample } from '../index';


export default {
  title: 'media/AudioLevelSample',
  component: MediaDeviceAudioOutputExample,
};

const modes: AudioOutputLevelSampleStyle[] = ['media-settings', 'user-setup'];

export const MediaSettingsMode = () => <MediaDeviceAudioOutputExample mode={select('mode', modes, 'media-settings')} />;

export const UserSetupMode = () => <MediaDeviceAudioOutputExample mode={select('mode', modes, 'user-setup')} />;
