import 'react-input-range/lib/css/index.css';
import React from 'react';
import styled, { withTheme } from 'styled-components';
import audioSampleBase64 from './test-audio-output-sample-mp3';
import {
  styledAudioOutputLevelSample,
  AudioOutputLevelSampleStyle,
  IAudioOutputLevelSampleProps,
} from './MediaSettingsAudioLevelSample.style';

const PERCANTAGE = 100;

const AudioOutputLevelContainer = withTheme(
  styled.div<IAudioOutputLevelSampleProps>`
    ${styledAudioOutputLevelSample};
  `
);

interface IMediaSettingsAudioLevelSampleProps {
  value?: number;
  mode?: AudioOutputLevelSampleStyle;
}

interface IMediaSettingsAudioLevelSampleState {
  canPlaySample: boolean;
  isPlaying: boolean;
}

class MediaSettingsAudioLevelSample extends React.Component<
  IMediaSettingsAudioLevelSampleProps,
  IMediaSettingsAudioLevelSampleState
> {
  public static defaultProps = {
    value: 1,
  };
  private _audioNode: HTMLAudioElement;
  private _trackNode: HTMLElement;

  private _setTrackNodeRef = (node: HTMLElement) => {
    if (node && this._trackNode !== node) {
      this._trackNode = node;
    }
  };

  private _updateTrackWidth = (value: number) => {
    if (this._trackNode) {
      this._trackNode.style.width = `${value}%`;
    }
  };

  private _updateAudioSampleData = () => {
    this.setState({ canPlaySample: true });
  };

  private _resetAudioSampleState = () => {
    this._updateTrackWidth(0);
    this.setState({ isPlaying: false });
  };

  private _updateAudioSampleTrack = () => {
    this._updateTrackWidth((this._audioNode.currentTime / this._audioNode.duration) * PERCANTAGE);
  };

  private _toggleAudioState = () => {
    if (this.state.isPlaying) {
      this._audioNode.pause();
      this.setState({ isPlaying: false });
    } else {
      this._audioNode.play();
      this.setState({ isPlaying: true });
    }
  };

  constructor(props: IMediaSettingsAudioLevelSampleProps) {
    super(props);

    this.state = {
      isPlaying: false,
      canPlaySample: false,
    };
  }

  public componentDidMount() {
    this._audioNode = new Audio(`data:audio/mpeg;base64,${audioSampleBase64}`);
    this._audioNode.autoplay = false;
    this._audioNode.volume = this.props.value;
    this._audioNode.addEventListener('ended', this._resetAudioSampleState, false);
    this._audioNode.addEventListener('timeupdate', this._updateAudioSampleTrack, false);
    this._audioNode.addEventListener('canplaythrough', this._updateAudioSampleData, false);
  }

  public componentDidUpdate() {
    this._audioNode.volume = this.props.value;
  }

  public componentWillUnmount() {
    this._audioNode.removeEventListener('ended', this._resetAudioSampleState, false);
    this._audioNode.removeEventListener('timeupdate', this._updateAudioSampleTrack, false);
    this._audioNode.removeEventListener('canplaythrough', this._updateAudioSampleData, false);
    this._audioNode = null;
  }

  public render() {
    return (
      <AudioOutputLevelContainer mode={this.props.mode || 'media-settings'}>
        <button
          className={this.state.isPlaying ? 'on' : 'off'}
          onClick={this._toggleAudioState}
          disabled={!this.state.canPlaySample}
        />
        <div>
          <span ref={this._setTrackNodeRef} />
        </div>
      </AudioOutputLevelContainer>
    );
  }
}

export default MediaSettingsAudioLevelSample;
