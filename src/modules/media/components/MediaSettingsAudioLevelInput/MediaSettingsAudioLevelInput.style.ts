/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { css } from 'styled-components';
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

export const styledAudioLevelInputContainer = ({ theme }: IRequiredTheme) => css`
  width: 100%;
  height: 0;
  display: inline-block;
  margin-top: -22px;

  > div {
    width: 100%;
    height: 2px;
    border-radius: 1px;
    background-color: #fff;
    vertical-align: middle;
    display: inline-block;

    > span {
      display: inline-block;
      height: inherit;
      width: 25%;
      background-color: ${theme.primaryColor};
      // possible refactor later TODO::ref
      position: relative;
      top: -13px;
    }
  }
`;
