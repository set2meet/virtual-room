import React from 'react';
import styled, { withTheme } from 'styled-components';
import { styledAudioLevelInputContainer } from './MediaSettingsAudioLevelInput.style';

const AudioLevelInputContainer = withTheme(
  styled.div`
    ${styledAudioLevelInputContainer};
  `
);

interface IMediaSettingsAudioLevelInputProps {
  defaultInput?: MediaDeviceInfo;
  volumeMeter?: HTMLElement;
}

class MediaSettingsAudioLevelInput extends React.Component<IMediaSettingsAudioLevelInputProps> {
  private _holder: HTMLElement;

  private _setRef = (node: HTMLElement) => {
    if (node && this._holder !== node) {
      this._holder = node;
      this._addVolumeMeter();
    }
  };

  private _addVolumeMeter = () => {
    const { volumeMeter } = this.props;

    if (this._holder && volumeMeter) {
      this._holder.appendChild(volumeMeter);
    }
  };

  private _delVolumeMeter = (volumeMeter?: HTMLElement) => {
    if (volumeMeter) {
      volumeMeter.parentNode.removeChild(volumeMeter);
    }
  };

  public componentDidUpdate(prevProps: IMediaSettingsAudioLevelInputProps) {
    const currVolumeMeter = this.props.volumeMeter;
    const prevVolumeMeter = prevProps.volumeMeter;

    if (prevVolumeMeter && !currVolumeMeter) {
      this._delVolumeMeter(prevVolumeMeter);
    } else if (!prevVolumeMeter && currVolumeMeter) {
      this._addVolumeMeter();
    } else if (prevVolumeMeter !== currVolumeMeter) {
      this._delVolumeMeter(prevVolumeMeter);
      this._addVolumeMeter();
    }
  }

  public render() {
    const classEmpty = this.props.defaultInput ? '' : 'no-device';

    return <AudioLevelInputContainer ref={this._setRef} className={classEmpty} />;
  }
}

export default MediaSettingsAudioLevelInput;
