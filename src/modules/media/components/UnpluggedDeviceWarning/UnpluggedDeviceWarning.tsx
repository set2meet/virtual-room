import React from 'react';
import { connect } from 'react-redux';
import { IUserMediDeviceType } from '../../types/mediaTypes';
import MediaDeviceSelect from '../MediaDeviceSelect';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../redux/actionCreators';
import styled from 'styled-components';
import {
  styledModal,
  styledRowHeader,
  styledRowSelect,
  styledRowConfirm,
  styledButtonConfirm,
} from './UnpluggedDeviceWarning.style';

const DEVICES_MAP: Record<MediaDeviceKind, IUserMediDeviceType> = {
  videoinput: 'inputVideo',
  audioinput: 'inputAudio',
  audiooutput: 'outputAudio',
};

const deviceName: Record<MediaDeviceKind, string> = {
  audiooutput: 'speakers',
  audioinput: 'microphone',
  videoinput: 'camera',
};

const ModalContent = styled.div`
  ${styledModal};
`;
const RowHeader = styled.div`
  ${styledRowHeader};
`;
const RowSelect = styled.div`
  ${styledRowSelect};
`;
const RowConfirm = styled.div`
  ${styledRowConfirm};
`;

interface IUnpluggedDeviceWarningOwnProps {
  onHide: () => void;
}

interface IUnpluggedDeviceWarningStateProps {
  isLocatedInTheRoom: boolean;
  isLocatedInTheHome: boolean;
  inputAudio: MediaDeviceInfo;
  inputVideo: MediaDeviceInfo;
  defaultDevices: Record<IUserMediDeviceType, MediaDeviceInfo[]>;
}

interface IUnpluggedDeviceWarningDispatchProps {
  ignoreUnpluggedDevices: ActionCreator<AnyAction>;
  replaceUnpluggedDevice: ActionCreator<AnyAction>;
  autoSelectDefaultDevices: ActionCreator<AnyAction>;
}

interface IUnpluggedDeviceWarningState {
  selectedDevice?: MediaDeviceInfo;
  connectedDevices: MediaDeviceInfo[];
}

type UnpluggedDeviceWarningProps = IUnpluggedDeviceWarningStateProps &
  IUnpluggedDeviceWarningDispatchProps &
  IUnpluggedDeviceWarningOwnProps;

export class UnpluggedDeviceWarning extends React.Component<UnpluggedDeviceWarningProps, IUnpluggedDeviceWarningState> {
  private get unpluggedDevice(): MediaDeviceInfo {
    return this.props.inputAudio || this.props.inputVideo;
  }

  private get connectedDevices(): MediaDeviceInfo[] {
    const device = this.unpluggedDevice;

    if (!device) {
      return this.props.defaultDevices.inputVideo;
    }

    return this.props.defaultDevices[DEVICES_MAP[device.kind]];
  }

  private updateListOfDevices() {
    this.setState({
      connectedDevices: this.connectedDevices,
    });
  }

  private confirmChangeDevice = () => {
    const { selectedDevice } = this.state;

    this.setState({ selectedDevice: null });
    this.props.replaceUnpluggedDevice(selectedDevice);
  };

  private changeDevice = (device: MediaDeviceInfo) => {
    this.setState({ selectedDevice: device });
  };

  private ignoreUnpluggedDevice = () => {
    this.props.ignoreUnpluggedDevices(DEVICES_MAP[this.unpluggedDevice.kind]);
  };

  constructor(props: UnpluggedDeviceWarningProps) {
    super(props);

    this.state = {
      selectedDevice: null,
      connectedDevices: this.connectedDevices,
    };
  }

  public componentDidUpdate(prevProps: UnpluggedDeviceWarningProps) {
    // uivd = unplugged input video device
    // uiad = unplugged input audio device
    const prevUIVD = prevProps.inputVideo;
    const prevUIAD = prevProps.inputAudio;
    const currUIVD = this.props.inputVideo;
    const currUIAD = this.props.inputAudio;
    const oldInputAudioDevices = prevProps.defaultDevices.inputAudio;
    const oldInputVideoDevices = prevProps.defaultDevices.inputVideo;
    const newInputAudioDevices = this.props.defaultDevices.inputAudio;
    const newInputVideoDevices = this.props.defaultDevices.inputVideo;

    if (!currUIAD && !currUIVD && (prevUIVD || prevUIAD)) {
      this.props.onHide();
    }

    if (oldInputAudioDevices !== newInputAudioDevices || oldInputVideoDevices !== newInputVideoDevices) {
      this.updateListOfDevices();
    }

    if (this.props.isLocatedInTheHome && ((currUIVD && currUIAD !== prevUIVD) || (currUIAD && currUIAD !== prevUIAD))) {
      this.props.autoSelectDefaultDevices();
    }
  }

  public render() {
    const device = this.unpluggedDevice;
    const deviceTitle: string = device && deviceName[device.kind];
    const { UI } = clientEntityProvider.getComponentProvider();
    const ButtonConfirm = styled(UI.Button)`
      ${styledButtonConfirm};
    `;

    return (
      <ModalContent>
        <RowHeader>
          <div>Your {deviceTitle} has been disconnected</div>
          <button onClick={this.ignoreUnpluggedDevice} />
        </RowHeader>
        <RowSelect>
          <div className="desc">Please, connect your device again or select another.</div>
          <div className="choice">
            <MediaDeviceSelect
              devices={this.connectedDevices || []}
              title={deviceTitle || '-'}
              default={this.state.selectedDevice}
              onChange={this.changeDevice}
            />
          </div>
        </RowSelect>
        <RowConfirm>
          <ButtonConfirm title="OK" disabled={!this.state.selectedDevice} onClick={this.confirmChangeDevice} />
        </RowConfirm>
      </ModalContent>
    );
  }
}

export default connect<IUnpluggedDeviceWarningStateProps, IUnpluggedDeviceWarningDispatchProps>(
  (state: any) => {
    const defaultDevices = state.media.devices;
    const { inputAudio, inputVideo } = state.media.unplugged;
    const isLocatedInTheRoom = !!state.room.state.id;
    const isLocatedInTheHome = state.router.location.pathname === '/';

    return {
      inputVideo,
      inputAudio,
      defaultDevices,
      isLocatedInTheHome,
      isLocatedInTheRoom,
    };
  },
  {
    ignoreUnpluggedDevices: actionCreators.ignoreUnpluggedDevices,
    replaceUnpluggedDevice: actionCreators.replaceUnpluggedDevice,
    autoSelectDefaultDevices: actionCreators.autoSelectDefaultDevices,
  }
)(UnpluggedDeviceWarning);
