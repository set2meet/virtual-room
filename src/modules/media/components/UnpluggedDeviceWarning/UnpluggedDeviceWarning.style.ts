/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export const styledModal = () => `
  margin: 0;
  padding: 15px;
`;

export const styledRowHeader = () => {
  const icons = clientEntityProvider.getIcons();

  return `
    display: flex;
    align-items: center;
    justify-content: flex-start;
    font-weight: bold;
    margin-bottom: 10px;
  
    > button {
      top: 14px;
      right: 14px;
      cursor: pointer;
      position: absolute;
      display: inline-block;
      width: 16px;
      height: 16px;
      border: none;
      background: transparent;
      outline: none;
    
      &:before {
        top: 0;
        left: 0;
        position: absolute;
        color: #fff;
        font-size: 16px;
        vertical-align: middle;
        display: inline-block;
        content: ${icons.code.close};
        font-family: ${icons.fontFamily};
      }
    }
  `;
};

export const styledRowSelect = () => `
  > div.desc {
    margin-top: 24px;
    text-align: center;
  }

  > div.choice {
    height: 32px;
    width: 100%;
    margin: 20px 0 50px 0;
    text-align: center;

    > div {
      width: 360px;
    }
  }
`;

export const styledRowConfirm = () => `
  margin-top: 10px;  
  text-align: center;
`;

export const styledButtonConfirm = () => `
  width: 140px;
`;
