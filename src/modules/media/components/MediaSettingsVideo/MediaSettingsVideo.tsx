import React from 'react';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { connect } from 'react-redux';
import { IUserMediaDevicesSettings, IUIMediaDeviceSelectOwnProps } from '../../types/mediaTypes';
import { styledContainer, styledBlock, styledVideoExample, styledEmptyElement } from '../MediaSettings';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../redux/actionCreators';
import SelectDeviceInputVideo from '../MediaDeviceSelectVideoInput';
import styled from 'styled-components';

const SelectInputVideo = SelectDeviceInputVideo as React.ComponentClass<IUIMediaDeviceSelectOwnProps>;

const TITLE_CAMERA = 'Camera';

const MediaSettingsContainer = styled.div`
  ${styledContainer};
`;
const MediaSettingsBlock = styled.div`
  ${styledBlock};
`;
const VideoExample = styled.div`
  ${styledVideoExample};
`;
const EmptyElement = styled.hr`
  ${styledEmptyElement};
`;

interface IMediaSettingsVideoStateProps {
  mediaSettings: IUserMediaDevicesSettings;
  inputDevices: MediaDeviceInfo[];
  defaultInput: MediaDeviceInfo;
}

interface IMediaSettingsVideoDispatchProps {
  getMediaSettingsVideoStream: ActionCreator<AnyAction>;
  closeMediaSettingsVideoStream: ActionCreator<AnyAction>;
  setDefaultInputVideo: ActionCreator<AnyAction>;
  requestVideoPermissions: ActionCreator<AnyAction>;
}

type IMediaSettingsVideoProps = IMediaSettingsVideoStateProps & IMediaSettingsVideoDispatchProps;

export class MediaSettingsVideo extends React.Component<IMediaSettingsVideoProps, {}> {
  private _mediaContainer: HTMLDivElement;

  private _showInputVideo = (video: HTMLVideoElement) => {
    const container = this._mediaContainer;

    if (container && video) {
      container.appendChild(video);
      video.play();
    }
  };

  private _hideInputVideo = (video: HTMLVideoElement) => {
    const container = video ? video.parentNode : null;

    if (container && video.parentNode) {
      video.volume = 0;
      container.removeChild(video);
    }
  };

  private _setMediaContainer = (node: HTMLDivElement) => {
    if (node && node !== this._mediaContainer) {
      this._mediaContainer = node;
    }
  };

  public componentDidMount() {
    const { videoExample } = this.props.mediaSettings;

    if (videoExample) {
      this._showInputVideo(videoExample);
    }

    this.props.getMediaSettingsVideoStream();
  }

  public componentDidUpdate(prevProps: IMediaSettingsVideoStateProps) {
    const oldVideoExample = prevProps.mediaSettings.videoExample;
    const newVideoExample = this.props.mediaSettings.videoExample;

    // re-attach video html tag
    if (oldVideoExample && !newVideoExample) {
      this._hideInputVideo(oldVideoExample);
    } else if (!oldVideoExample && newVideoExample) {
      this._showInputVideo(newVideoExample);
    } else if (oldVideoExample !== newVideoExample) {
      this._hideInputVideo(oldVideoExample);
      this._showInputVideo(newVideoExample);
    }

    const prevDevice = prevProps.defaultInput;
    const prevDeviceId = prevDevice ? prevDevice.deviceId : null;
    const currDevice = this.props.defaultInput;
    const currDeviceId = currDevice ? currDevice.deviceId : null;
    const prevError = prevProps.mediaSettings.inputVideoDeviceError;
    const currError = this.props.mediaSettings.inputVideoDeviceError;

    // if unplugged device was returned
    // or
    // new device selected as default
    if (
      (prevError === 'DeviceUnplugged' && !currError) ||
      (prevDeviceId !== currDeviceId &&
        currDeviceId &&
        // not just updated unknown device (fixes bug)
        prevError !== 'UnknownDefaultDevice' &&
        !currError)
    ) {
      this.props.getMediaSettingsVideoStream();
    }
  }

  public componentWillUnmount(): void {
    this.props.closeMediaSettingsVideoStream();
  }

  public render() {
    const { mediaSettings, inputDevices } = this.props;
    const { inputVideoDeviceError } = mediaSettings;
    const noDevices = inputDevices.length === 0;
    const { UI } = clientEntityProvider.getComponentProvider();

    return (
      <MediaSettingsContainer>
        <MediaSettingsBlock>
          <div>{TITLE_CAMERA}</div>
          <div>
            <SelectInputVideo requestPermissions={this.props.requestVideoPermissions} />
          </div>
        </MediaSettingsBlock>
        <VideoExample ref={this._setMediaContainer}>
          {inputVideoDeviceError || noDevices ? (
            <EmptyElement />
          ) : (
            <div>
              <UI.Spinner />
            </div>
          )}
        </VideoExample>
      </MediaSettingsContainer>
    );
  }
}

export default connect<IMediaSettingsVideoStateProps, IMediaSettingsVideoDispatchProps>(
  (state: any) => ({
    mediaSettings: state.media.settings,
    inputDevices: state.media.devices.inputVideo,
    defaultInput: state.media.default.inputVideo,
  }),
  {
    getMediaSettingsVideoStream: actionCreators.getMediaSettingsVideoStream,
    closeMediaSettingsVideoStream: actionCreators.closeMediaSettingsVideoStream,
    setDefaultInputVideo: actionCreators.setDefaultInputVideo,
    requestVideoPermissions: actionCreators.requestVideoPermissions,
  }
)(MediaSettingsVideo);
