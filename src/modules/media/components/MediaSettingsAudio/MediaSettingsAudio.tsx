import React from 'react';
import { connect } from 'react-redux';
import { styledContainer, styledBlock, styledNoMicrophone } from '../MediaSettings';
import MediaSettingsAudioLevelInput from '../MediaSettingsAudioLevelInput';
import MediaSettingsAudioLevelSample from '../MediaSettingsAudioLevelSample';
import SelectDeviceInputAudio from '../MediaDeviceSelectAudioInput';
import { ActionCreator, AnyAction } from 'redux';
import { actionCreators } from '../../redux/actionCreators';
import styled from 'styled-components';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IUserMediaDevicesSettings, IUIMediaDeviceSelectOwnProps } from '../../types/mediaTypes';

const SelectInputAudio = SelectDeviceInputAudio as React.ComponentClass<IUIMediaDeviceSelectOwnProps>;

const TITLE_MICROPHONE = 'Microphone';

const MediaSettingsContainer = styled.div`
  ${styledContainer};
`;
const MediaSettingsBlock = styled.div`
  ${styledBlock};
`;
const LoadingContainer = styled.div`
  width: 100%;
  height: 100%;
  text-align: center;

  > div {
    top: 0px;
    left: 8px;
    position: relative;
    transform-origin: 0 0 0;
    transform: scale(0.4, 0.4);
  }
`;

const DeviceNoMicrophone = styled.div`
  ${styledNoMicrophone};
`;

interface IMediaSettingsAudioStateProps {
  mediaSettings: IUserMediaDevicesSettings;
  inputDevices: MediaDeviceInfo[];
  defaultInput: MediaDeviceInfo;
  microphoneVolumeMeter?: HTMLElement;
}

interface IMediaSettingsAudioDispatchProps {
  getMediaSettingsAudioStream: ActionCreator<AnyAction>;
  closeMediaSettingsAudioStream: ActionCreator<AnyAction>;
  setDefaultInputAudio: ActionCreator<AnyAction>;
  requestAudioPermissions: ActionCreator<AnyAction>;
}

type IMediaSettingsAudioProps = IMediaSettingsAudioStateProps & IMediaSettingsAudioDispatchProps;

export class MediaSettingsAudio extends React.Component<IMediaSettingsAudioProps> {
  public componentDidMount(): void {
    this.props.getMediaSettingsAudioStream();
  }

  public componentDidUpdate(prevProps: IMediaSettingsAudioProps): void {
    const prevDevice = prevProps.defaultInput;
    const prevDeviceId = prevDevice ? prevDevice.deviceId : null;
    const currDevice = this.props.defaultInput;
    const currDeviceId = currDevice ? currDevice.deviceId : null;
    const prevError = prevProps.mediaSettings.inputAudioDeviceError;
    const currError = this.props.mediaSettings.inputAudioDeviceError;

    // if unplugged device was returned
    // or
    // new device selected as default
    if (
      (prevError === 'DeviceUnplugged' && !currError) ||
      (prevDeviceId !== currDeviceId &&
        currDeviceId &&
        // not just updated unknown device (fixes bug)
        prevError !== 'UnknownDefaultDevice' &&
        !currError)
    ) {
      this.props.getMediaSettingsAudioStream();
    }
  }

  public componentWillUnmount(): void {
    this.props.closeMediaSettingsAudioStream();
  }

  private _renderMicrophoneStuff() {
    const { UI } = clientEntityProvider.getComponentProvider();
    const { defaultInput, mediaSettings } = this.props;
    const { microphoneVolume, inputAudioDeviceError } = mediaSettings;

    if (inputAudioDeviceError) {
      return <DeviceNoMicrophone />;
    }

    if (defaultInput && !microphoneVolume) {
      return (
        <LoadingContainer>
          <UI.Spinner />
        </LoadingContainer>
      );
    }

    if (defaultInput && microphoneVolume) {
      return <MediaSettingsAudioLevelInput defaultInput={this.props.defaultInput} volumeMeter={microphoneVolume} />;
    }
  }

  public render() {
    return (
      <MediaSettingsContainer>
        <MediaSettingsBlock>
          <div>{TITLE_MICROPHONE}</div>
          <div>
            <SelectInputAudio requestPermissions={this.props.requestAudioPermissions} />
          </div>
        </MediaSettingsBlock>
        <MediaSettingsBlock>
          <div>Input level</div>
          <div>{this._renderMicrophoneStuff()}</div>
        </MediaSettingsBlock>
        <hr />
        <MediaSettingsBlock>
          <div>Speaker test</div>
          <div>
            <MediaSettingsAudioLevelSample />
          </div>
        </MediaSettingsBlock>
      </MediaSettingsContainer>
    );
  }
}

export default connect<IMediaSettingsAudioStateProps, IMediaSettingsAudioDispatchProps>(
  (state: any) => ({
    mediaSettings: state.media.settings,
    inputDevices: state.media.devices.inputAudio,
    defaultInput: state.media.default.inputAudio,
    microphoneVolumeMeter: state.media.settings.microphoneVolume,
  }),
  {
    getMediaSettingsAudioStream: actionCreators.getMediaSettingsAudioStream,
    closeMediaSettingsAudioStream: actionCreators.closeMediaSettingsAudioStream,
    setDefaultInputAudio: actionCreators.setDefaultInputAudio,
    requestAudioPermissions: actionCreators.requestAudioPermissions,
  }
)(MediaSettingsAudio);
