/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { CONNECTION_STATUS, DisconnectReason } from './constants';

export interface IConnectionConstants {
  CONNECTION_STATUS: typeof CONNECTION_STATUS;
  DisconnectReason: typeof DisconnectReason;
}
