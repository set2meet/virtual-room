/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

/*** Websocket Connection module ***/
import actionCreators from './redux/actionCreators';
import * as constants from './types/constants';
import { IVRModule } from '../../ioc/client/types/interfaces';
import { IConnectionConstants } from './types/IConnectionConstants';
import { IConnectionActionCreators } from './redux/types/IConnectionActionCreators';
import { ConnectionAction, IConnectionActionTypes } from './redux/types/ConnectionAction';

interface IConnection extends IVRModule<IConnectionActionCreators> {
  actionTypes: IConnectionActionTypes;
  actionCreators: IConnectionActionCreators;
  constants: IConnectionConstants;
}

const connectionClientModule: IConnection = {
  actionTypes: ConnectionAction,
  actionCreators,
  constants,
};

export default connectionClientModule;
