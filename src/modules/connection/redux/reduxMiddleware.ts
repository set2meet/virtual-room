/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../ioc/common/ioc-interfaces';
import { ConnectionAction } from './types/ConnectionAction';
import Socket = SocketIOClient.Socket;
import { AnyAction, Dispatch } from 'redux';
import actionCreators from './actionCreators';
import { DisconnectReason } from '../types/constants';
import { TStore } from '../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const createSocketListeners = (socket: Socket, store: TStore) => {
  const onDisconnect = () => {
    const disconnectReason = store.getState().connection.disconnectReason;
    store.dispatch(actionCreators.disconnected(disconnectReason || DisconnectReason.CONNECTION_LOST));
  };
  const onConnect = () => {
    store.dispatch(actionCreators.connected());
  };

  const onUnauthorized = () => {
    store.dispatch(actionCreators.disconnected(DisconnectReason.UNAUTHORIZED));
  };

  socket.on('disconnect', onDisconnect);
  socket.on('authorized', onConnect);
  socket.on('unauthorized', onUnauthorized);
};

export const connectionMiddlewareEnum: IAppReduxEnumMiddleware = {
  [ConnectionAction.CONNECT](store, next, action, socket) {
    socket.io.opts.query = { token: action.token };
    socket.connect();
    next(action);
  },
  [ConnectionAction.DISCONNECT](store, next, action, socket) {
    next(action);
    socket.disconnect();
  },
  [ConnectionAction.DISCONNECTED](store, next, action) {
    next(action);

    if (action.reason === DisconnectReason.UNAUTHORIZED) {
      store.dispatch(clientEntityProvider.getAppActionCreators().logout());
    }
  },
};

const connectionMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: TStore) => {
  createSocketListeners(socket, store);

  return (next: Dispatch<AnyAction>) => (action: AnyAction) => {
    if (connectionMiddlewareEnum[action.type]) {
      return connectionMiddlewareEnum[action.type](store, next, action, socket, storeKey);
    } else {
      next(action);
    }
  };
};

export default connectionMiddleware;
