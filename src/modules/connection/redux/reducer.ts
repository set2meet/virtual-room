/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, Reducer } from 'redux';
import { CONNECTION_STATUS } from '../types/constants';
import { ConnectionAction } from './types/ConnectionAction';
import { IConnectionReduxState } from './types/IConnectionReduxState';

const INITIAL_STATE: IConnectionReduxState = {
  status: CONNECTION_STATUS.DISCONNECTED,
  disconnectReason: null,
};

const reducers: Record<string, Reducer<IConnectionReduxState>> = {};

reducers[ConnectionAction.CONNECT] = (state) => {
  return {
    ...state,
    status: CONNECTION_STATUS.CONNECTING,
    disconnectReason: null,
  };
};

reducers[ConnectionAction.DISCONNECT] = (state, action) => {
  return {
    ...state,
    disconnectReason: action.reason,
  };
};

reducers[ConnectionAction.CONNECTED] = (state) => {
  return {
    ...state,
    status: CONNECTION_STATUS.CONNECTED,
  };
};

reducers[ConnectionAction.DISCONNECTED] = (state) => {
  return {
    ...state,
    status: CONNECTION_STATUS.DISCONNECTED,
  };
};

export default (state = INITIAL_STATE, action: AnyAction): IConnectionReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
