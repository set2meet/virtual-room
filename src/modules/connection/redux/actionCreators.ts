/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { DisconnectReason } from '../types/constants';
import { IConnectionActionCreators } from './types/IConnectionActionCreators';
import { ConnectionAction } from './types/ConnectionAction';

const connectionActionCreators: IConnectionActionCreators = {
  connect: (token: string) => {
    return {
      type: ConnectionAction.CONNECT,
      token,
    };
  },
  disconnect: (reason: DisconnectReason = DisconnectReason.EXIT) => {
    return {
      type: ConnectionAction.DISCONNECT,
      reason,
    };
  },
  connected: () => {
    return {
      type: ConnectionAction.CONNECTED,
    };
  },
  disconnected: (reason: DisconnectReason) => {
    return {
      type: ConnectionAction.DISCONNECTED,
      reason,
    };
  },
};

export default connectionActionCreators;
