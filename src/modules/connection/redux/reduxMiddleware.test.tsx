/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IModuleReduxMiddleware } from '../../../ioc/common/ioc-interfaces';
import { mock } from 'jest-mock-extended';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;
import { AnyAction, Dispatch, Store } from 'redux';
import setupIoCClientForTest from '../../../test/utils/ioc/testIoCClient';
import reduxMiddleware, { connectionMiddlewareEnum } from './reduxMiddleware';
import actionCreators from './actionCreators';
import { DisconnectReason } from '../types/constants';
import { loadAsyncTree } from '../../../test/utils/storybook';
import EnzymeMountTracker from '../../../test/utils/EnzymeMountTracker';
import { enzymeCleanup, InitModulesWrapper } from '../../../test/utils/utils';
import { IClientEntityProviderInternals } from '../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import React from 'react';
import { createMockServerSocket } from '../../../test/utils/serverSocket';
import { STORYBOOK_CONFIG_SERVICE } from '../../../../.storybook/common/utils/ioc/storybookInitConfig';

const createMiddlewareInvoker = (
  testMiddleware: IModuleReduxMiddleware,
  socket: Socket = mock<Socket>(),
  store: Store = mock<Store>()
): {
  next: Dispatch;
  store: Store;
  invoke: (action: AnyAction) => any;
} => {
  const next = jest.fn();
  const middleware = testMiddleware('')(socket)(store)(next);

  const invoke = (action) => {
    middleware(action);
  };

  return { store, next, invoke };
};

describe('connection module middleware', () => {
  createMockServerSocket(STORYBOOK_CONFIG_SERVICE);

  beforeEach(
    setupIoCClientForTest({
      storeEnhancerFactory: () => [],
    })
  );
  afterEach(async (done) => {
    await clientEntityProvider.destroy();
    enzymeCleanup();
    jest.restoreAllMocks();
    jest.clearAllMocks();
    done();
  });

  it('should dispatch action with custom disconnect reason', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));

    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const reason = '123';
    const receivedAction = actionCreators.disconnect(reason);
    const disconnected = jest.spyOn(actionCreators, 'disconnected');

    store.dispatch(receivedAction);
    await clientEntityProvider.getUtils().sleep(10);
    expect(disconnected).toHaveBeenCalledWith(reason);
    done();
  });
  it('should dispatch action with default disconnect reason', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));

    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const receivedAction = actionCreators.disconnect('');
    const disconnected = jest.spyOn(actionCreators, 'disconnected');

    store.dispatch(receivedAction);
    await clientEntityProvider.getUtils().sleep(10);
    expect(disconnected).toHaveBeenCalledWith(DisconnectReason.CONNECTION_LOST);
    done();
  });
  it('should dispatch action with default disconnect reason 2', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));

    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const receivedAction2 = actionCreators.disconnect();

    expect(receivedAction2.reason).toEqual(DisconnectReason.EXIT);
    const disconnected = jest.spyOn(actionCreators, 'disconnected');

    expect(disconnected).toHaveBeenCalledTimes(0);
    store.dispatch(receivedAction2);
    await clientEntityProvider.getUtils().sleep(10);
    expect(disconnected).toHaveBeenCalledWith(DisconnectReason.EXIT);
    done();
  });
  it('should call next on actions not from current middleware', async (done) => {
    const { next, invoke } = createMiddlewareInvoker(reduxMiddleware);
    const action = { type: '123' };

    expect(connectionMiddlewareEnum[action.type]).toBeUndefined();

    await invoke(action);
    expect(next).toHaveBeenNthCalledWith(1, action);
    done();
  });
});
