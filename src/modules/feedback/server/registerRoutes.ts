/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as Router from 'koa-router';
import nodemailer from 'nodemailer';
import { IServerMetaObjects } from '../../../ioc/server/types/interfaces';
import { TSmtpConfig } from './types/TSmtpConfig';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { LogErrorCode } from '../../../ioc/server/ioc-constants';

const createMailer = (configObj: TSmtpConfig) => {
  const {
    smtp: { host, port, secure, auth },
  } = configObj;

  return nodemailer.createTransport({
    host,
    port,
    secure,
    auth: {
      user: auth.user,
      pass: auth.pass,
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false,
    },
  });
};

export default (router: Router, appMetaObject: IServerMetaObjects) => {
  const logger = appMetaObject.logger.feedback;

  const feedbackConfig: TSmtpConfig = serverEntityProvider.getConfig().feedback;

  if (!feedbackConfig) {
    console.log('\nError: not found config for presentations converter service');
    process.exit(1);
  }

  router.post(`/${feedbackConfig.route}`, async (ctx, next) => {
    const { comment, selectedCategory, selectedSeverity, additionalData } = (ctx.request as any).body;
    const {
      mail: { params },
    } = feedbackConfig;

    enum HttpStatusCode {
      Conflict = 409,
      BadRequest = 400,
    }

    const sendMail = async () => {
      const transporter = createMailer(feedbackConfig);

      const data = {
        from: params.from,
        to: params.to,
        subject: params.subject,
        text: '',
        html: `<div><b>Category: </b>${selectedCategory}</div>
               <div><b>Severity: </b>${selectedSeverity}</div>
               <div><b>Comment: </b>${comment}</div>
               <div><b>Tech information: </b>${JSON.stringify(additionalData)}</div>
        `,
      };

      const verify = await transporter.verify();

      if (verify) {
        try {
          await transporter.sendMail(data);
          ctx.body = 'OK';
          logger.info('`Nodemailer: Message was sent!`');
        } catch (e) {
          ctx.status = HttpStatusCode.BadRequest;
          ctx.body = 'ERROR';
          logger.error(LogErrorCode.MODULE, 'Nodemailer: Error while sent mail: Check SMTP server or spam settings;', {
            error: e,
          });
        }
      } else {
        ctx.status = HttpStatusCode.Conflict;
        ctx.body = 'ERROR';
        logger.error(LogErrorCode.MODULE, `Nodemailer: Cant verify connect; Check SMTP server available or no`);
        throw new Error('Nodemailer: Cant verify connect; Check SMTP server available or no');
      }
    };

    await sendMail();
    await next();
  });
};
