/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// tslint:disable:no-magic-numbers
import styled, { css } from 'styled-components';
import { Button, DropdownButton, MenuItem, Row } from 'react-bootstrap';
import { styledButtonClose } from '../../../../app/components/SettingsPanel/settingsPanel.style';
import { Container } from './Containers';

const darkGray = '#575757';
const mainGreen = '#2bce72';
const defaultRowMargin = 1; // REM

export const FeedbackContainer = styled(Container)`
  width: 100%;
  padding: 14px 35px 20px 35px;
  border-top: 2px solid ${mainGreen};
`;

// font normalizer
const FN = styled.div`
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
`;

const mt = (rem: number) =>
  css`
    margin-top: ${rem}rem;
  `;

export const FeedbackDropdownContainer = styled.div`
  display: block;
  > .dropdown {
    display: block;
  }

  > .dropdown > .dropdown-menu {
    width: 100%;
    top: 47px;
    background-color: ${darkGray};
    border-radius: 2px;
    a {
      color: #fff;
      &:hover,
      &:focus {
        background: ${mainGreen};
      }
    }
  }

  .open > .dropdown-toggle.btn-default {
    color: #fff;
    border-size: 2px;
    border-color: ${mainGreen};
    background-color: ${darkGray};
  }
`;

export const FeedbackDropdownBtnWithDot = styled(DropdownButton)`
  display: block;
  text-align: left;
  width: 100%;
  background-color: ${darkGray};
  border-color: ${darkGray};
  color: #fff;
  padding-left: 20px;
  border-radius: 2px;

  ${mt(defaultRowMargin)};
  &:hover,
  &:active,
  &:focus,
  &:active:focus {
    color: #fff;
    border-color: ${darkGray};
    background-color: ${darkGray};
  }
  &:before {
    content: '•';
    position: absolute;
    margin-left: -12px;
    font-size: 2rem;
    top: -5px;
    left: 18px;
    color: ${(props) => props.color || '#fff'};
  }
  > .caret {
    float: right;
    margin-top: 8px;
  }
`;

export const FeedbackDropdownBtn = styled(FeedbackDropdownBtnWithDot)`
  padding-left: 10px;
  &:before {
    display: none;
  }
`;

export const FeedbackMenuItemStyled = styled(MenuItem)`
  > a:link {
    padding: 5.5px 10px };
  }
`;

export const FeedbackMenuItemStyledWithDot = styled(FeedbackMenuItemStyled)`
  &:before {
    position: relative;
    content: '•';
    left: 5px;
    top: -6px;
    height: 0px;
    width: 0;
    display: block;
    font-size: 2rem;
    color: ${(props) => props.style.color};
    
  }
  > a:link {
    padding: 5.5px 20px };
  }
`;

export const FeedbackTextarea = styled.textarea`
  width: 100%;
  max-width: 490px;
  height: 8.285em;
  background-color: ${darkGray};
  font-size: 14px;
  padding: 8px 12px;
  color: #e8e8e8;
  border: none;
  outline: none;

  ${mt(defaultRowMargin)};
`;

export const FeedbackRow = styled(Row)`
  margin-top: 20px;
`;

export const FeedbackFooter = styled(Row)`
  text-align: right;
  height: 55px;
  padding-top: 23px;

  > hr {
    border-top: 1px solid ${darkGray};
    padding: 0 15px;
  }
  > .col-md-12 {
    margin-top: 20px;
    text-align: right;
  }

  .btn {
    margin-left: 2.5rem;
  }
`;

export const ButtonClose = styled(Button)`
  ${styledButtonClose};
  top: -20px;
  right: -5px;
  z-index: 100;

  &:hover,
  &:active,
  &:focus,
  &:active:focus {
    background-color: transparent;
  }
`;

export const FeedbackLabel = styled(FN)`
  font-size: 14px;
`;

export const FeedbackHeader = styled(FN)`
  font-size: 18px;
`;
