import React from 'react';
import FeedbackPanel from './FeedbackPanel';
import { IStoreState } from '../../../../../ioc/client/types/interfaces';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { action } from '@storybook/addon-actions';

const mapStateToProps = (state: IStoreState) => ({
  user: '',
  room: state.room?.state,
  webrtc: state.room?.webrtcService,
});

export default {
  title: 'feedback/FeedbackPanel',
  component: FeedbackPanel,
  decorators: [connectDecorator(mapStateToProps)],
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const Default = (props) => {
  return <FeedbackPanel room={props.room} user={props.user} webrtc={props.webrtc} onHide={action('onHide')} />;
};
