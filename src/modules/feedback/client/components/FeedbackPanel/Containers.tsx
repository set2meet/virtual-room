import * as React from 'react';

// Bootstrap temporary wrapper until we update
export interface IContainer {
  className?: [];
  children: React.ReactElement[] | React.ReactElement | string;
  style?: Partial<React.CSSProperties>;
}

export const Container = ({ className, children, style }: IContainer) => {
  return (
    <div style={style} className={`container ${className}`}>
      {children}
    </div>
  );
};
