// tslint:disable:no-magic-numbers
// tslint:disable:jsx-no-lambda
import React, { ChangeEvent, FocusEvent, Component } from 'react';
import resolveUrl from 'resolve-url';
import { IFeedbackPanelProps, IFeedbackPanelState, TFeedbackAdditionalData } from '../../types/feedbackTypes';
import { CategoryStates, SeverityStates } from '../../types/constants';
import {
  ButtonClose,
  FeedbackContainer,
  FeedbackDropdownBtn,
  FeedbackDropdownBtnWithDot,
  FeedbackDropdownContainer,
  FeedbackFooter,
  FeedbackHeader,
  FeedbackLabel,
  FeedbackMenuItemStyled,
  FeedbackMenuItemStyledWithDot,
  FeedbackRow,
  FeedbackTextarea,
} from './FeedbackPanel.style';
import noop from 'lodash/noop';
import { Col } from 'react-bootstrap';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const FeedbackMenuItem = (props: any) => {
  if (!props.dot) {
    return <FeedbackMenuItemStyled onClick={(e: any) => props.handler(e, 2)}>{props.children}</FeedbackMenuItemStyled>;
  } else {
    return (
      <FeedbackMenuItemStyledWithDot
        style={{ color: props.color || 'white' }}
        onClick={(e: any) => props.handler(e, 1)}
      >
        {props.children}
      </FeedbackMenuItemStyledWithDot>
    );
  }
};

// const FeedbackMenuItems = (props: any) => {
//   const { items } = props;
// };

const ErrorContainer = (): React.ReactElement => {
  return (
    <FeedbackContainer>
      <FeedbackRow>
        <Col md={12} style={{ padding: '10px', textAlign: 'center' }}>
          Whoops, some error here!
        </Col>
      </FeedbackRow>
    </FeedbackContainer>
  );
};

class FeedbackPanel extends Component<IFeedbackPanelProps, IFeedbackPanelState> {
  public static SIZE = { width: 563 };

  constructor(props: IFeedbackPanelProps) {
    super(props);

    this.state = {
      error: null,
      severityColor: '#fff',
      selectedCategory: CategoryStates.Bug,
      selectedSeverity: SeverityStates.Mild,
      comment: 'Leave a comment',
      severityDropdownStatus: false,
      categoryDropdownStatus: false,
      beenSent: false,
      beenError: false,
      textareaInit: false,
    };
  }

  public componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    this.setState({ error: errorInfo });
  }

  public _toggleDropdown = (type: string) => {
    this._closeDropdowns();

    if (type === 'severity') {
      this.setState({ severityDropdownStatus: !this.state.severityDropdownStatus });
    } else {
      this.setState({ categoryDropdownStatus: !this.state.categoryDropdownStatus });
    }
  };

  public _click = (event: any, type: string) => {
    const color = event.target.parentNode.style.color;

    this._closeDropdowns();

    if (+type === 1) {
      this.setState({ selectedSeverity: event.target.innerText, severityColor: color });
    }
    if (+type === 2) {
      this.setState({ selectedCategory: event.target.innerText });
    }
  };

  public _changeHandler = (e: ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({ comment: e.target.value });
  };

  public _onFocusHandler = (e: FocusEvent<HTMLTextAreaElement>): void => {
    if (!this.state.textareaInit) {
      this.setState({ textareaInit: true });
      e.target.value = '';
    }
  };

  public _submit = () => {
    const { comment, selectedCategory, selectedSeverity } = this.state;
    const { user, webrtc, room } = this.props;
    const nvgt = navigator as any;
    const browser = clientEntityProvider.getBrowser().info;

    const additionalData: TFeedbackAdditionalData = {
      user: {
        userID: user.id,
        userDisplayName: user.displayName,
      },
      room: {
        roomID: room?.id,
        hostUsername: room?.ownerId,
        sessionID: webrtc?.sessionId,
        meetingMode: webrtc?.serviceName,
        date: Date.now(),
      },
      os: {
        name: browser.os,
        platform: browser.osFamily,
      },
      browser: {
        name: browser.name,
        version: browser.versionMajor,
      },
      resolution: [window.screen.availWidth, window.screen.availHeight],
      cpu: {
        cores: nvgt.hardwareConcurrency,
      },
    };

    const config = clientEntityProvider.getConfig();

    const route = resolveUrl(config.service.url, config.service.path);

    // TODO:: refactor a bit later when refactor notifys @Ruslan
    fetch(`${route}${config.feedback.route}`, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ comment, selectedCategory, selectedSeverity, additionalData }),
    })
      .then((response) => {
        if (response.status !== 200) {
          setTimeout(() => {
            this.props.onHide();
          }, 1500);
          this.setState({ beenError: true });
        }
        this.setState({ beenSent: true });
        setTimeout(() => {
          this.props.onHide();
        }, 2000);
        return response;
      })
      .catch((error) => {
        console.log(error); // TODO::add here logging when refactor notifys
      });
  };

  public render() {
    if (this.state.error) {
      return <ErrorContainer />;
    }
    const { UI } = clientEntityProvider.getComponentProvider();

    if (this.state.beenSent) {
      const modal = document.querySelector('.modal-backdrop');

      (modal as any).style.backgroundColor = 'transparent';
      setTimeout(() => {
        // adoviy kostyl TODO::refactor

        (modal as any).style.backgroundColor = '#000';
      }, 2000);

      // will b removed or refactored later
      return (
        <div
          style={{
            top: '-422px',
            left: '28%',
            position: 'fixed',
            width: '257px',
            color: 'white',
            background: '#2bce72',
            height: '37px',
            textAlign: 'center',
            fontSize: '14px',
            paddingTop: '8px',
          }}
        >
          Thank you for your feedback
        </div>
      );
    }

    if (this.state.beenError) {
      return (
        <div
          style={{
            top: '-422px',
            left: '28%',
            position: 'fixed',
            width: '257px',
            color: 'white',
            background: '#fc5a1b',
            height: '37px',
            textAlign: 'center',
            fontSize: '14px',
            paddingTop: '8px',
          }}
        >
          Unable connect to server
        </div>
      );
    }

    return (
      <FeedbackContainer>
        <FeedbackRow style={{ marginTop: '1.15rem' }}>
          <Col md={11}>
            <FeedbackHeader>Feedback</FeedbackHeader>
          </Col>
          <Col md={1}>
            <ButtonClose onClick={this.props.onHide} />
          </Col>
        </FeedbackRow>
        <FeedbackRow>
          <Col md={6}>
            <FeedbackLabel>Select your comment category</FeedbackLabel>
            <FeedbackDropdownContainer>
              <FeedbackDropdownBtn
                id="fb-d1"
                open={this.state.categoryDropdownStatus}
                onToggle={noop}
                title={<span>{this.state.selectedCategory}</span>}
                onClick={() => this._toggleDropdown('category')}
              >
                <FeedbackMenuItem handler={this._click}>Bug</FeedbackMenuItem>
                <FeedbackMenuItem handler={this._click}>New Feature</FeedbackMenuItem>
                <FeedbackMenuItem handler={this._click}>Other</FeedbackMenuItem>
              </FeedbackDropdownBtn>
            </FeedbackDropdownContainer>
          </Col>
          <Col md={6}>
            <FeedbackLabel>Select severity states</FeedbackLabel>
            <FeedbackDropdownContainer>
              <FeedbackDropdownBtnWithDot
                id="fb-d2"
                open={this.state.severityDropdownStatus}
                onToggle={noop}
                onClick={() => this._toggleDropdown('severity')}
                title={<span>{this.state.selectedSeverity}</span>}
                block={true}
                color={this.state.severityColor}
              >
                <FeedbackMenuItem dot={true} color={'#ffffff'} handler={this._click}>
                  Mild
                </FeedbackMenuItem>
                <FeedbackMenuItem dot={true} color={'#62c08b'} handler={this._click}>
                  Moderate
                </FeedbackMenuItem>
                <FeedbackMenuItem dot={true} color={'#ffd54f'} handler={this._click}>
                  Severe
                </FeedbackMenuItem>
                <FeedbackMenuItem dot={true} color={'#fc5a1b'} handler={this._click}>
                  Critical
                </FeedbackMenuItem>
              </FeedbackDropdownBtnWithDot>
            </FeedbackDropdownContainer>
          </Col>
        </FeedbackRow>
        <FeedbackRow style={{ marginTop: '1.8rem' }}>
          <Col md={12}>
            <FeedbackLabel>Describe your issue or share ideas</FeedbackLabel>
            <FeedbackTextarea
              defaultValue={this.state.comment}
              onChange={this._changeHandler}
              onFocus={this._onFocusHandler}
            />
          </Col>
        </FeedbackRow>
        <FeedbackFooter>
          <hr />
          <Col md={12}>
            <UI.Button title="CANCEL" bsStyle="secondary" onClick={this.props.onHide} />
            <UI.Button title="SUBMIT FEEDBACK" bsStyle="default" onClick={this._submit} />
          </Col>
        </FeedbackFooter>
      </FeedbackContainer>
    );
  }

  private _closeDropdowns = () => {
    this.setState({
      severityDropdownStatus: false,
      categoryDropdownStatus: false,
    });
  };
}

export default FeedbackPanel;
