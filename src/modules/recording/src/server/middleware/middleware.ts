/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import {
  IServerMiddleware,
  IServerMiddlewareContext,
  IServerMiddlewareNext,
} from '../../../../../ioc/server/types/interfaces';
import notifyUsers from '../../server/utils/notifyUsers';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { TRecordingMessage } from '../../../../../ioc/server/types/interfaces';

export const APP_SERVER_REDUX_INIT = '@APP_SERVER:REDUX:INIT';

const middlewareEnum: {
  [key: string]: IServerMiddleware;
} = {
  [APP_SERVER_REDUX_INIT]: async (context, next) => {
    const recordingSubPub = serverEntityProvider.getRecordingSubPub();

    await recordingSubPub.subscribeOnChannel((message: TRecordingMessage) => {
      void notifyUsers(context, message);
    });

    next();
  },
};

// TODO middleware must be in webphone and call api service methods
const middleware: IServerMiddleware = async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
  if (middlewareEnum[context.action.type]) {
    await middlewareEnum[context.action.type](context, next);
  } else {
    next();
  }
};

export default middleware;
