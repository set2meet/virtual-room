/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import serverMiddleware, { APP_SERVER_REDUX_INIT } from './middleware';
import { mock } from 'jest-mock-extended';
import notifyUsers from '../../server/utils/notifyUsers';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { IRecordingSubPub, IServerMiddlewareContext } from '../../../../../ioc/server/types/interfaces';

jest.mock('../utils/notifyUsers');

jest.mock('../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider');

describe('recording serverMiddleware', () => {
  beforeEach(async () => {
    jest.clearAllMocks();
  });

  test('next should be called', async () => {
    const context = mock<IServerMiddlewareContext>();
    const next = jest.fn();

    await serverMiddleware(context, next);

    expect(next).toBeCalled();
  });

  test('subscribe should be called on init', async () => {
    const recordingSubPub = mock<IRecordingSubPub>();
    const context = mock<IServerMiddlewareContext>();
    const next = jest.fn();

    context.action.type = APP_SERVER_REDUX_INIT;

    (serverEntityProvider.getRecordingSubPub as jest.Mock).mockReturnValue(recordingSubPub);

    (recordingSubPub.subscribeOnChannel as jest.Mock).mockImplementation((fn) => {
      fn();
    });

    await serverMiddleware(context, next);

    expect(recordingSubPub.subscribeOnChannel).toBeCalled();
    expect(notifyUsers).toBeCalled();
    expect(next).toBeCalled();
  });
});
