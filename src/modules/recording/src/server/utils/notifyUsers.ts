/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';
import RecordingAction from '../../common/redux/RecordingAction';
import { IServerMiddlewareContext as ISContext } from '../../../../../ioc/common/ioc-interfaces';
import { TRecordingMessage, RecordingMessageType } from '@s2m/recording-db';

const recordActionType = {
  [RecordingMessageType.RECORDING_UPDATED]: RecordingAction.ROOM_RECORDING_RECORD_UPDATED,
  [RecordingMessageType.RECORDING_REMOVED]: RecordingAction.ROOM_RECORDING_RECORD_REMOVED,
};

export default async (context: ISContext, recordingMessage: TRecordingMessage) => {
  const { dispatchAction } = context.appMetaObjects;
  const users = recordingMessage.participants;
  const record = recordingMessage.record;
  const action = {
    type: recordActionType[recordingMessage.type],
    meta: { target: { users } },
    record,
  };

  dispatchAction(action, _noop);
};
