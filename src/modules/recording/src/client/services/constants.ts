/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export enum CaptureServiceEvent {
  ConnectionSuccess = 'connection-success',
  ConnectionFailure = 'connection-failure',
  ConnectionDisconnected = 'connection-disconnected',
  RecordReady = 'record-ready',
  RecordStopped = 'record-stop',
  RecordBroken = 'record-broken',
  RecordStarted = 'record-started',
  RecordSuspended = 'record-suspended',
}

export enum RecordingServiceEvents {
  ConnectionSuccess = 'connection-success',
  ConnectionFailure = 'connection-failure',
  RecordingStarted = 'recording-started',
  RecordingStopped = 'recording-stopped',
  RecordingCanceled = 'recording-canceled',
  RecordingSuspended = 'recording-suspended',
  RecordingDisconnected = 'recording-disconnected',
  RecordingBroken = 'recording-broken',
  RecordingEnd = 'recording-end',
}

export enum CaptureEvents {
  RecordInitOffer = 'record-init-offer',
  RecordInitAnswer = 'record-init-answer',
  RecordStartOffer = 'record-start-offer',
  RecordStartAnswer = 'record-start-answer',
  RecordStopOffer = 'record-stop-offer',
  RecordStopAnswer = 'record-stop-answer',
  RecordSuspendOffer = 'record-suspend-offer',
  RecordSuspendAnswer = 'record-suspend-answer',
}
