/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { AxiosInstance } from 'axios';
import { IAppRecordingPreviewConfig, IAppConfigService } from '../../../../../../ioc/client/types/interfaces';
import { TRecordItem } from '@s2m/recording-db/lib/types';
import { IRecordingsClient } from './IRecordingsClient';

const buildBaseUrl = (config: IAppConfigService): string => {
  const { url, path } = config;

  return new URL(path, url).href.replace(/[/]$/, '');
};

export default class RecordingsClient implements IRecordingsClient {
  private readonly baseUrl: string;

  constructor(config: IAppRecordingPreviewConfig, private httpClient: AxiosInstance) {
    this.baseUrl = buildBaseUrl(config.service);
  }

  public addUserAsParticipant(recordId: string, userId: string): Promise<void> {
    return this.httpClient.put(`${this.baseUrl}/recordings/${recordId}/participant/add/${userId}`);
  }

  public createRecord(recordId: string, roomId: string): Promise<void> {
    return this.httpClient.put(`${this.baseUrl}/recordings/${recordId}/create`, {
      roomId,
    });
  }

  public stopRecord(recordId: string): Promise<void> {
    return this.httpClient.put(`${this.baseUrl}/recordings/${recordId}/stop`);
  }

  public async downloadRecord(recordSrc: string): Promise<string> {
    const resp = await this.httpClient.get(`${this.baseUrl}?uri=${recordSrc}`, {
      responseType: 'blob',
    });
    const type = resp.headers['content-type'];

    return URL.createObjectURL(new Blob([resp.data], { type }));
  }

  public deleteRecord(recordId: string): Promise<void> {
    return this.httpClient.delete(`${this.baseUrl}/recordings/${recordId}`);
  }

  public async fetchRecordings(userId: string): Promise<TRecordItem[]> {
    const resp = await this.httpClient.get(`${this.baseUrl}/users/${userId}/recordings`);

    return resp.data;
  }
}
