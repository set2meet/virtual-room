/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import RecordingsClient from './RecordingsClient';
import { IAppRecordingPreviewConfig } from '../../../../../../ioc/client/types/interfaces';
import { mock } from 'jest-mock-extended';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { HttpStatusCode } from '../../../../../../types/httpStatusCode';
import { TRecordItem } from '@s2m/recording-db/lib/types';

const mockAxios = new MockAdapter(axios);

describe('RecordingClient', () => {
  const buildConfig = (url: string, path: string): IAppRecordingPreviewConfig => ({
    service: {
      url,
      path,
    },
  });

  const config: IAppRecordingPreviewConfig = buildConfig('http://localhost', 'path');

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    mockAxios.reset();
  });

  test('addUserAsParticipant with difference configs', async () => {
    mockAxios.onPut('http://localhost/path/recordings/recordId1/participant/add/userId1').reply(HttpStatusCode.OK);

    const client1 = new RecordingsClient(buildConfig('http://localhost', '/path'), axios);
    const client2 = new RecordingsClient(buildConfig('http://localhost/', '/path'), axios);
    const client3 = new RecordingsClient(buildConfig('http://localhost/', '/path/'), axios);
    const client4 = new RecordingsClient(buildConfig('http://localhost', 'path/'), axios);

    await client1.addUserAsParticipant('recordId1', 'userId1');
    await client2.addUserAsParticipant('recordId1', 'userId1');
    await client3.addUserAsParticipant('recordId1', 'userId1');
    await client4.addUserAsParticipant('recordId1', 'userId1');
  });

  test('createRecord', async () => {
    mockAxios
      .onPut('http://localhost/path/recordings/recordId1/create', { roomId: 'roomId1' })
      .reply(HttpStatusCode.OK);

    const client = new RecordingsClient(config, axios);

    await client.createRecord('recordId1', 'roomId1');
  });

  test('stopRecord', async () => {
    mockAxios.onPut('http://localhost/path/recordings/recordId1/stop').reply(HttpStatusCode.OK);

    const client = new RecordingsClient(config, axios);

    await client.stopRecord('recordId1');
  });

  test('downloadRecord', async () => {
    mockAxios.onGet('http://localhost/path?uri=recordSrc').reply((axisConfig) => {
      if (axisConfig.responseType === 'blob') {
        return [HttpStatusCode.OK, 'fileData1', { 'content-type': 'mp4' }];
      }
    });

    (window.URL.createObjectURL as jest.Mock).mockReturnValue('objectURL1');

    const client = new RecordingsClient(config, axios);
    const blobUrl = await client.downloadRecord('recordSrc');

    expect(window.URL.createObjectURL).toBeCalledWith(expect.any(Blob));

    const blob = (window.URL.createObjectURL as jest.Mock).mock.calls[0][0];

    expect(blob.type).toBe('mp4');
    expect(blobUrl).toBe('objectURL1');
  });

  test('deleteRecord', async () => {
    mockAxios.onDelete('http://localhost/path/recordings/recordId1').reply(HttpStatusCode.OK);

    const client = new RecordingsClient(config, axios);

    await client.deleteRecord('recordId1');
  });

  test('fetchRecordings', async () => {
    const serverRecordsResp = [mock<TRecordItem>(), mock<TRecordItem>()];

    mockAxios.onGet('http://localhost/path/users/userId1/recordings').reply(HttpStatusCode.OK, serverRecordsResp);

    const client = new RecordingsClient(config, axios);
    const records = await client.fetchRecordings('userId1');

    expect(records.length).toBe(2);
  });
});
