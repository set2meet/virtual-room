/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// @ts-ignore
import { adapter as _ } from 'webrtc-adapter';

const RTCPeerConnection =
  // @ts-ignore
  window.RTCPeerConnection ||
  // @ts-ignore
  window.mozRTCPeerConnection ||
  // @ts-ignore
  window.webkitRTCPeerConnection;

const RTCSessionDescription =
  // @ts-ignore
  window.RTCSessionDescription ||
  // @ts-ignore
  window.mozRTCSessionDescription ||
  // @ts-ignore
  window.webkitRTCSessionDescription;

// todo: move code above to separate file

import { EventEmitter } from 'events';
import SocketIOClient from 'socket.io-client';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { CaptureServiceEvent, CaptureEvents } from '../constants';
import { IAppRecordingConfig } from '../../../../../../ioc/client/types/interfaces';
import { RecordingCaptureParams } from './types/TRecordingCaptureParams';
import { LogErrorCode } from '../../../../../../ioc/client/ioc-constants';

enum LogTitles {
  setLocalDescription = '[Recording] err set local description',
  offerError = '[Recording] offer error',
  sdpAnswerError = '[Recording] sdp answer error',
}

const RTCDefaultOffer = {
  offerToReceiveAudio: false,
  offerToReceiveVideo: false,
  voiceActivityDetection: false,
};

export default class RecordingCaptureClient extends EventEmitter {
  private recordId: string;
  private socket: SocketIOClient.Socket;
  private rtcConnection: RTCPeerConnection;

  private logger = clientEntityProvider.getLogger().recordingCapture;

  constructor(private config: IAppRecordingConfig) {
    super();
  }

  public connect() {
    const { RecordInitAnswer, RecordStartAnswer, RecordStopAnswer, RecordSuspendAnswer } = CaptureEvents;

    this.socket = SocketIOClient(this.config.service.url, {
      reconnectionDelayMax: 1000,
      autoConnect: false,
      timeout: 3000,
      ...this.config.service, // rewrite all*
      path: this.config.service.path + 'ws', // *except path
    });

    this.socket.on('error', this.handleSocketError);
    this.socket.on('connect', this.handleSocketConnected);
    this.socket.on('disconnect', this.handleSocketDisconnected);
    this.socket.on('connect_error', this.handleSocketConnectError);

    this.socket.on(RecordInitAnswer, this.handleRecordInitAnswer);
    this.socket.on(RecordStartAnswer, this.handleRecordStartAnswer);
    this.socket.on(RecordStopAnswer, this.handleRecordStopAnswer);
    this.socket.on(RecordSuspendAnswer, this.handleRecordSuspendAnswer);

    this.socket.connect();
  }

  private destroy() {
    this.recordId = null;

    if (this.rtcConnection) {
      this.rtcConnection.close();
      this.rtcConnection = null;
    }

    if (this.socket) {
      this.socket.removeAllListeners();
      this.socket.close();
      this.socket = null;
    }
  }

  public initRecord(recordId: string, params: RecordingCaptureParams) {
    this.socket.emit(CaptureEvents.RecordInitOffer, { recordId, ...params });
  }

  public startRecord(stream: MediaStream) {
    this.rtcConnection = new RTCPeerConnection(null);

    stream.getTracks().forEach((track) => {
      this.rtcConnection.addTrack(track, stream);
    });

    this.rtcConnection.createOffer(RTCDefaultOffer).then(this.rtcOfferCreateSuccess).catch(this.rtcOfferCreateFailure);
  }

  public stopRecord() {
    this.socket.emit(CaptureEvents.RecordStopOffer, {
      recordId: this.recordId,
    });
  }

  public suspendRecord() {
    this.socket.emit(CaptureEvents.RecordSuspendOffer, {
      recordId: this.recordId,
    });
  }

  private handleSocketError = (reason: string) => {
    this.logger.error(LogErrorCode.MODULE, `socket error: ${reason}`);
    this.destroy();
    this.emit(CaptureServiceEvent.ConnectionFailure, reason);
  };

  private handleSocketConnectError = () => {
    this.handleSocketError('Not available');
  };

  private handleSocketDisconnected = () => {
    this.emit(CaptureServiceEvent.ConnectionDisconnected);
  };

  private handleSocketConnected = () => {
    this.emit(CaptureServiceEvent.ConnectionSuccess);
  };

  private handleRecordInitAnswer = (payload: any) => {
    const { recordId, error } = payload;

    this.recordId = recordId;

    if (error) {
      this.logger.error(LogErrorCode.MODULE, error?.message, {
        title: LogTitles.sdpAnswerError,
      });

      this.destroy();
      this.emit(CaptureServiceEvent.RecordBroken, error);

      return;
    }

    this.emit(CaptureServiceEvent.RecordReady, this.recordId);
  };

  private handleRecordStartAnswer = (payload: any) => {
    const { error, answer } = payload;

    if (error) {
      this.logger.error(LogErrorCode.MODULE, error?.message, {
        title: LogTitles.sdpAnswerError,
      });
      this.emit(CaptureServiceEvent.RecordBroken, error);
      return;
    }

    if (!answer) {
      this.logger.error(LogErrorCode.MODULE, 'no answer', {
        title: LogTitles.sdpAnswerError,
      });
      this.emit(CaptureServiceEvent.RecordBroken, {});
      return;
    }

    const sdp = new RTCSessionDescription(payload.answer);

    this.rtcConnection
      .setRemoteDescription(sdp)
      .then(() => {
        this.emit(CaptureServiceEvent.RecordStarted);
      })
      .catch((err) => {
        this.logger.error(LogErrorCode.MODULE, err?.message, {
          title: LogTitles.offerError,
        });
        this.emit(CaptureServiceEvent.RecordBroken, err);
      });
  };

  private handleRecordStopAnswer = () => {
    this.destroy();
    this.emit(CaptureServiceEvent.RecordStopped);
  };

  private handleRecordSuspendAnswer = () => {
    this.destroy();
    this.emit(CaptureServiceEvent.RecordSuspended);
  };

  private rtcOfferCreateFailure = (err: Error) => {
    this.logger.error(LogErrorCode.MODULE, err?.message, {
      title: LogTitles.offerError,
    });
    this.emit(CaptureServiceEvent.RecordBroken, err);
  };

  private rtcOfferCreateSuccess = (offer: RTCSessionDescriptionInit) => {
    this.rtcConnection
      .setLocalDescription(offer)
      .then(() => {
        this.socket.emit(CaptureEvents.RecordStartOffer, {
          offer,
          recordId: this.recordId,
        });
      })
      .catch((err: Error) => {
        this.logger.error(LogErrorCode.MODULE, err?.message, {
          title: LogTitles.setLocalDescription,
        });
      });
  };
}
