/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { EventEmitter } from 'events';

export const mockConnect = jest.fn();
export const mockStopRecord = jest.fn();
export const mockSuspendRecord = jest.fn();
export const mockInitRecord = jest.fn();

let instance = null;

export const getLastInstance = () => instance;

export default class Mock extends EventEmitter {
  constructor() {
    super();
    instance = this;
  }

  public connect = mockConnect;
  public stopRecord = mockStopRecord;
  public suspendRecord = mockSuspendRecord;
  public initRecord = mockInitRecord;
}
