/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import RecordingClient from './RecordingCaptureClient';
import { CaptureEvents, CaptureServiceEvent } from '../constants';
import { EventEmitter } from 'events';
import { mock } from 'jest-mock-extended';
import { IAppRecordingConfig } from '../../../../../../ioc/client/types/interfaces';

class MockIOSocket extends EventEmitter {
  public connect = jest.fn();
  public subscribe = jest.fn();
  public close = jest.fn();
}

jest.mock('../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider', () => ({
  getLogger: jest.fn(() => {
    return {
      recordingCapture: {
        info: jest.fn(),
        error: jest.fn(),
      },
    };
  }),
}));

const mockIOSocket = new MockIOSocket();

jest.mock('socket.io-client', () => jest.fn(() => mockIOSocket));

describe('RecordingClient', () => {
  let recordingClient: RecordingClient = null;

  beforeEach(() => {
    const config = mock<IAppRecordingConfig>();

    recordingClient = new RecordingClient(config);
  });

  test('should connected', () => {
    recordingClient.connect();

    expect(mockIOSocket.eventNames().length).not.toBe(0);
    expect(mockIOSocket.connect).toBeCalled();
  });

  test('record init finished with error', (done) => {
    recordingClient.connect();

    recordingClient.on(CaptureServiceEvent.RecordBroken, (e) => {
      expect(e.message).toBe('some_error');

      done();
    });

    mockIOSocket.emit(CaptureEvents.RecordInitAnswer, { recordId: 'recordId1', error: new Error('some_error') });

    expect(mockIOSocket.close).toBeCalled();
    expect(mockIOSocket.eventNames().length).toBe(0);
  });

  test('record init started', (done) => {
    recordingClient.connect();

    recordingClient.on(CaptureServiceEvent.RecordReady, (recordId) => {
      expect(recordId).toBe('recordId1');

      done();
    });

    mockIOSocket.emit(CaptureEvents.RecordInitAnswer, { recordId: 'recordId1' });
  });
});
