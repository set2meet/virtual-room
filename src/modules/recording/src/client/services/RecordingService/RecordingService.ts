/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { EventEmitter } from 'events';
import MediaStreamMerger from '../MediaStreamMerger/MediaStreamMerger';
import MediaCaptureService from '../RecordingCaptureClient/RecordingCaptureClient';
import { CaptureServiceEvent, RecordingServiceEvents } from '../constants';
import { IAppRecordingConfig } from '../../../../../../ioc/client/types/interfaces';
import { RecordingCaptureParams } from '../RecordingCaptureClient/types/TRecordingCaptureParams';

export default class RecordingService extends EventEmitter {
  private mediaStreamTrack: MediaStreamTrack; // screen sharing video track
  private recordId: string;
  private params: RecordingCaptureParams;
  private streamMerger: MediaStreamMerger;
  private captureService: MediaCaptureService;

  constructor(config: IAppRecordingConfig, streams: { [streamId: string]: MediaStream }) {
    super();

    // create new capture service
    this.captureService = new MediaCaptureService(config);

    // create audio stream merger
    this.streamMerger = new MediaStreamMerger();

    Object.keys(streams)
      .map((key) => streams[key])
      .forEach((stream) => stream && this.streamMerger.addStream(stream));

    // subscribe to capture service
    this.subscribeForCaptureService();
  }

  public connect(recordId: string, params: RecordingCaptureParams) {
    this.recordId = recordId;
    this.params = params;
    this.captureService.connect();
  }

  public stop() {
    this.captureService.stopRecord();
  }

  public suspend() {
    this.captureService.suspendRecord();
  }

  private destroy() {
    if (this.mediaStreamTrack) {
      this.mediaStreamTrack.stop();
      this.mediaStreamTrack = null;
    }

    this.streamMerger.destroy();
    this.streamMerger = null;

    this.captureService.removeAllListeners();
    this.captureService = null;
  }

  public addStream = (stream: MediaStream) => {
    if (!stream) {
      return;
    }

    const track = stream.getAudioTracks()[0];

    if (!track) {
      return;
    }

    // add stream to merger
    this.streamMerger.addStream(stream);

    // patch: if track was closed ('ended') - remove stream
    track.addEventListener('ended', () => this.removeStream(stream));
  };

  public removeStream = (stream: MediaStream) => {
    // remove stream from merger
    this.streamMerger?.delStream(stream);
  };

  private get audioTrack(): Promise<MediaStreamTrack> {
    return Promise.resolve(this.streamMerger.audioTrack);
  }

  private get videoTrack(): Promise<MediaStreamTrack> {
    return (navigator.mediaDevices as any).getDisplayMedia().then((stream: MediaStream) => stream.getVideoTracks()[0]);
  }

  private subscribeForCaptureService() {
    const {
      RecordReady,
      RecordBroken,
      RecordStopped,
      RecordStarted,
      RecordSuspended,
      ConnectionSuccess,
      ConnectionFailure,
      ConnectionDisconnected,
    } = CaptureServiceEvent;

    this.captureService.on(ConnectionDisconnected, this.onDisconnected);
    this.captureService.on(ConnectionFailure, this.onConnectFailure);
    this.captureService.on(ConnectionSuccess, this.onConnectSuccess);

    this.captureService.on(RecordStarted, this.onRecordStarted);
    this.captureService.on(RecordStopped, this.onRecordStopped);
    this.captureService.on(RecordSuspended, this.onRecordSuspended);
    this.captureService.on(RecordBroken, this.onRecordBroken);
    this.captureService.on(RecordReady, this.onRecordReady);
  }

  private onConnectSuccess = () => {
    this.captureService.initRecord(this.recordId, this.params);
  };

  private onConnectFailure = (reason: string) => {
    this.emit(RecordingServiceEvents.ConnectionFailure, reason);
  };

  private onDisconnected = () => {
    this.emit('disconnected');
  };

  private onRecordStopped = () => {
    this.destroy();
    this.emit(RecordingServiceEvents.RecordingStopped, this.recordId);
  };

  private onRecordSuspended = () => {
    this.destroy();
    this.emit(RecordingServiceEvents.RecordingSuspended, this.recordId);
  };

  private onRecordStarted = () => {
    this.emit(RecordingServiceEvents.RecordingStarted, this.recordId);
  };

  private onRecordBroken = () => {
    this.destroy();
    this.emit(RecordingServiceEvents.RecordingBroken);
  };

  private onCancel = () => {
    this.emit(RecordingServiceEvents.RecordingCanceled);
  };

  private onRecordReady = (recordId: string) => {
    this.recordId = recordId;

    Promise.all([this.audioTrack, this.videoTrack])
      .then((tracks: MediaStreamTrack[]) => {
        this.mediaStreamTrack = tracks[1]; // save link to proper close it
        this.captureService.startRecord(new MediaStream(tracks));
      })
      .catch(this.onCancel);
  };
}
