/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export const mockAddStream = jest.fn();
export const mockDelStream = jest.fn();
export const mockDestroy = jest.fn();

let instance = null;

export const getLastInstance = () => instance;

export default class Mock {
  constructor() {
    instance = this;
  }

  public addStream = mockAddStream;
  public delStream = mockDelStream;
  public destroy = mockDestroy;
  public audioTrack = null;
}
