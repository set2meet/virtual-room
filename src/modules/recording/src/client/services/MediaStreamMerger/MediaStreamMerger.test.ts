/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import MediaStreamMerger from './MediaStreamMerger';
import { mock, mockDeep } from 'jest-mock-extended';

it('test MediaStreamMerger', async () => {
  const constantSourceNode = mock<ConstantSourceNode>();
  const audioContextPrototype = (window as any).AudioContext.prototype;
  const mediaStreamAudioDestination = mockDeep<MediaStreamAudioDestinationNode>();
  const mediaStreamTrack: MediaStreamTrack = mock<MediaStreamTrack>();
  const audioTracks = [mediaStreamTrack];

  mediaStreamAudioDestination.stream.getAudioTracks.calledWith().mockReturnValue(audioTracks);

  audioContextPrototype.createMediaStreamDestination = jest.fn(() => mediaStreamAudioDestination);

  const mediaStreamSource = {
    connect: jest.fn(),
    disconnect: jest.fn(),
  };

  audioContextPrototype.createMediaStreamSource = jest.fn(() => mediaStreamSource);
  audioContextPrototype.createConstantSource = jest.fn(() => constantSourceNode);

  const streamMerger: MediaStreamMerger = new MediaStreamMerger();

  expect(constantSourceNode.start).toBeCalled();

  const mediaStream: MediaStream = mock<MediaStream>();

  streamMerger.addStream(mediaStream);

  expect(mediaStreamSource.connect).toBeCalled();

  streamMerger.delStream(mediaStream);

  expect(mediaStreamSource.disconnect).toHaveBeenCalledTimes(1);

  streamMerger.addStream(mediaStream);

  streamMerger.destroy();

  expect(mediaStreamSource.disconnect).toHaveBeenCalledTimes(2);

  expect(mediaStreamTrack.stop).toBeCalled();
});
