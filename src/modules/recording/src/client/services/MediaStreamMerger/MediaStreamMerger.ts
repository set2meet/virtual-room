/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const AudioContext = (window as any).AudioContext || (window as any).webkitAudioContext;

type MergeRecord = {
  stream: MediaStream;
  source: MediaStreamAudioSourceNode;
  output: GainNode;
};

export default class MediaStreamMerger {
  private _ctx = new AudioContext();
  private _streams: Map<MediaStream, MergeRecord> = new Map();
  private _audioDestination = this._ctx.createMediaStreamDestination();

  constructor() {
    this._setupConstantNode();
    this._backgroundAudioHack();
  }

  private _setupConstantNode() {
    const constantAudioNode = this._ctx.createConstantSource();
    const constantGain = this._ctx.createGain(); // gain node prevents quality drop

    constantAudioNode.start();
    constantGain.gain.value = 0;

    constantAudioNode.connect(constantGain);
    constantGain.connect(this._audioDestination);
  }

  private _backgroundAudioHack() {
    // stop browser from throttling timers by playing almost-silent audio
    const source = this._ctx.createConstantSource();
    const gainNode = this._ctx.createGain();

    // required to prevent popping on start
    gainNode.gain.value = 0.001; // tslint:disable-line
    source.connect(gainNode);
    gainNode.connect(this._ctx.destination);
    source.start();
  }

  public get audioTrack(): MediaStreamTrack {
    return this._audioDestination.stream.getAudioTracks()[0];
  }

  public addStream(stream: MediaStream) {
    const source = this._ctx.createMediaStreamSource(stream);
    const output = this._ctx.createGain();

    output.gain.value = 1;
    source.connect(output);
    output.connect(this._audioDestination);

    this._streams.set(stream, { stream, source, output });
  }

  public delStream(stream: MediaStream) {
    if (!this._streams.has(stream)) {
      return;
    }

    const { output, source } = this._streams.get(stream);

    output.disconnect(this._audioDestination);
    source.disconnect(output);

    this._streams.delete(stream);
  }

  public destroy() {
    this._streams.forEach(({ stream }) => {
      this.delStream(stream);
    });

    this.audioTrack.stop();
    this._audioDestination = null;
  }
}
