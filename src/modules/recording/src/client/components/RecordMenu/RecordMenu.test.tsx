import { RecordMenu } from './RecordMenu';
import noop from 'lodash/noop';
import React from 'react';
import { TRecordItem, RecordItemStatus } from '@s2m/recording-db/lib/types';
import { mock } from 'jest-mock-extended';
import { mountWithTheme } from '../../../../../../test/utils/utils';
import { enzymeCleanup } from '../../../../../../test/utils/utils';
import setupIoCClientForTest from '../../../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import downloadRecording from './utils/downloadRecording';

beforeEach(setupIoCClientForTest());
afterEach(enzymeCleanup);

jest.mock('./utils/downloadRecording');

describe('<RecordMenu>', () => {
  it('RecordMenu not opened', async () => {
    const record = mock<TRecordItem>();
    const wrapper = mountWithTheme()(
      <RecordMenu
        isReady={true}
        record={record}
        isOwner={true}
        isOpened={false}
        onToggle={noop}
        onRemove={noop}
        showModal={jest.fn()}
        logout={jest.fn()}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('RecordMenu opened', async () => {
    const record = mock<TRecordItem>();
    const wrapper = mountWithTheme()(
      <RecordMenu
        isReady={true}
        record={record}
        isOwner={true}
        isOpened={true}
        onToggle={noop}
        onRemove={noop}
        showModal={jest.fn()}
        logout={jest.fn()}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should call onRemove', () => {
    const record = mock<TRecordItem>();

    record.id = '123';
    const onRemove = jest.fn();

    const wrapper = mountWithTheme()(
      <RecordMenu
        isReady={true}
        record={record}
        isOwner={true}
        isOpened={true}
        onToggle={noop}
        onRemove={onRemove}
        showModal={jest.fn()}
        logout={jest.fn()}
      />
    );

    wrapper.find('[icon="delete"]').simulate('click');
    expect(onRemove).toBeCalledTimes(1);
    expect(onRemove).toBeCalledWith(record.id);
  });

  it('should call onToggle', () => {
    const commonIcons = clientEntityProvider.getIcons();
    const record = mock<TRecordItem>();

    record.id = '123';
    const onToggle = jest.fn();
    const wrapper = mountWithTheme()(
      <RecordMenu
        isReady={true}
        record={record}
        isOwner={true}
        isOpened={true}
        onToggle={onToggle}
        onRemove={noop}
        showModal={jest.fn()}
        logout={jest.fn()}
      />
    );

    expect(wrapper.find('button').at(0)).toHaveStyleRule('content', `${commonIcons.code.menuDotted}`, {
      modifier: '&:before',
    });
    wrapper.find('button').at(0).simulate('click');
    expect(onToggle).toBeCalledTimes(1);
    expect(onToggle).toBeCalledWith(record.id);
  });

  describe('check download', () => {
    it('shouldn t download if record not available', () => {
      const record = mock<TRecordItem>();

      record.status = RecordItemStatus.failed;

      const wrapper = mountWithTheme()(
        <RecordMenu
          isReady={true}
          record={record}
          isOwner={true}
          isOpened={true}
          onToggle={noop}
          onRemove={noop}
          showModal={jest.fn()}
          logout={jest.fn()}
        />
      );

      wrapper.find('[icon="download"]').simulate('click');
      expect(downloadRecording).not.toBeCalled();
    });

    it('should download if record available', async (done) => {
      const record = mock<TRecordItem>();

      record.status = RecordItemStatus.available;
      record.src = 'src123';
      const responseUrl = 'http://localhost/url12345';

      Object.defineProperty(window.URL, 'createObjectURL', { value: () => responseUrl });

      const wrapper = mountWithTheme()(
        <RecordMenu
          isReady={true}
          record={record}
          isOwner={true}
          isOpened={true}
          onToggle={noop}
          onRemove={noop}
          showModal={jest.fn()}
          logout={jest.fn()}
        />
      );

      wrapper.find('[icon="download"]').simulate('click');

      expect(downloadRecording).toBeCalled();
      done();
    });
  });

  it('should copy to clipboard', () => {
    const record = mock<TRecordItem>();

    document.execCommand = jest.fn();
    const wrapper = mountWithTheme()(
      <RecordMenu
        isReady={true}
        record={record}
        isOwner={true}
        isOpened={true}
        onToggle={noop}
        onRemove={noop}
        showModal={jest.fn()}
        logout={jest.fn()}
      />
    );

    wrapper.find('[icon="copy"]').simulate('click');
    expect(document.execCommand).toHaveBeenCalledWith('copy');
    // @ts-ignore
    document.execCommand.mockReset();
    // @ts-ignore
    wrapper.setProps({ isReady: false });
    wrapper.find('[icon="copy"]').simulate('click');
    expect(document.execCommand).not.toHaveBeenCalledWith('copy');
  });
});
