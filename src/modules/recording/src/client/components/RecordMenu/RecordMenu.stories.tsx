import React from 'react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { getUserWithTokens } from '../../../../../../test/mocks/auth';
import { connectDecorator } from '../../../../../../../.storybook/decorators/reduxDecorators';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import RecordMenu from './RecordMenu';
import { buildRecordItem } from '../../../../../../test/mocks/recording';

export default {
  title: 'recording/RecordMenu',
  component: RecordMenu,
  decorators: [connectDecorator()],
  parameters: {
    redux: {
      enable: true,
    },
  },
};

const userWithTokens = getUserWithTokens();

const recordStatuses = ['processing', 'available', 'recording', 'error', 'removing', 'failed', 'random'];

const getRecord = () => {
  const index = 1;
  return buildRecordItem({
    id: `rec${index}`,
    src: `src${index}`,
    name: text('name', `Record ${index}`),
    status: select('status', recordStatuses, status),
  });
};

export const Default = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  const record = getRecord();

  return (
    <RecordMenu
      isReady={boolean('isReady', false)}
      record={record}
      isOwner={boolean('isOwner', false)}
      isOpened={boolean('isOpened', false)}
      onToggle={action('onToggle')}
      onRemove={action('onRemove')}
    />
  );
};

export const Ready = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  const record = getRecord();

  return (
    <RecordMenu
      isReady={boolean('isReady', true)}
      record={record}
      isOwner={boolean('isOwner', true)}
      isOpened={boolean('isOpened', false)}
      onToggle={action('onToggle')}
      onRemove={action('onRemove')}
    />
  );
};

export const OpenedAndReady = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  const record = getRecord();

  return (
    <RecordMenu
      isReady={boolean('isReady', true)}
      record={record}
      isOwner={boolean('isOwner', true)}
      isOpened={boolean('isOpened', true)}
      onToggle={action('onToggle')}
      onRemove={action('onRemove')}
    />
  );
};

export const OpenedAndNotReady = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  const record = getRecord();

  return (
    <RecordMenu
      isReady={boolean('isReady', false)}
      record={record}
      isOwner={boolean('isOwner', true)}
      isOpened={boolean('isOpened', true)}
      onToggle={action('onToggle')}
      onRemove={action('onRemove')}
    />
  );
};
