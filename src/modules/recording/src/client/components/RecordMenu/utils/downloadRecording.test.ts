/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TRecordItem } from '@s2m/recording-db/lib/types';
import downloadRecording from './downloadRecording';
import setupIoCClientForTest from '../../../../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

describe('downloadRecording', () => {
  beforeEach(async (done) => {
    jest.restoreAllMocks();
    jest.clearAllMocks();

    await setupIoCClientForTest()(done);
  });

  test('test1', async () => {
    const record = {
      src: 'src1',
      name: 'name1',
    } as Partial<TRecordItem>;

    const recordingClient = clientEntityProvider.getRecordingsClient();

    jest.spyOn(recordingClient, 'downloadRecord').mockResolvedValue('newUrl1');

    await downloadRecording(record as TRecordItem);

    expect(URL.revokeObjectURL).toBeCalledWith('newUrl1');
  });
});
