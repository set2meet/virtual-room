import React from 'react';
import styled from 'styled-components';
import {
  styledRecordMenuContainer,
  styledMenuItem,
  styledRecordMenuButton,
  IRecordMenuItem,
  IRecordMenuButton,
  IRecordMenuContainer,
} from './RecordMenu.style';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators, Dispatch } from 'redux';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import downloadRecording from './utils/downloadRecording';
import MessageModal from '../MessageModal/MessageModal';
import { IAppModalDialogDefinition } from '../../../../../../ioc/client/types/interfaces';
import { IStoreState } from '../../../../../../ioc/common/ioc-interfaces';
import { HttpClientError } from '../../../../../../ioc/client/ioc-constants';
import { RecordItemStatus, TRecordItem } from '@s2m/recording-db/lib/types';

const UserMenuContainer = styled.div<IRecordMenuContainer>`
  ${styledRecordMenuContainer};
`;

const MenuButton = styled.button<IRecordMenuButton>`
  ${styledRecordMenuButton};
`;

const MenuItem = styled.a<IRecordMenuItem>`
  ${styledMenuItem};
`;

interface IRecordMenuProps {
  isReady: boolean;
  record: TRecordItem;
  isOwner: boolean;
  isOpened: boolean;
  onToggle: (id: string) => void;
  onRemove: (id: string) => void;
}

interface IRecordMenuDispatchProps {
  showModal: (modal: IAppModalDialogDefinition) => void;
  logout: ActionCreator<AnyAction>;
}

export type RecordMenuProps = IRecordMenuProps & IRecordMenuDispatchProps;

export class RecordMenu extends React.Component<RecordMenuProps> {
  private remove = () => {
    this.props.onRemove(this.props.record.id);
  };

  private toggleVisibility = () => {
    this.props.onToggle(this.props.record.id);
  };

  private buildPreviewHref = () => {
    const {
      record: { src },
    } = this.props;

    return `${location.origin}/video/?${src}`;
  };

  private copyToClipboard = () => {
    const record = this.props.record;

    if (!this.props.isReady || record.status === RecordItemStatus.failed) {
      return;
    }

    const el = document.createElement('textarea');

    el.value = this.buildPreviewHref();
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };

  private handleDownloadRecordError = (e) => {
    const buttons = [];

    // TODO see httpClient.ts
    if (e.errorType === HttpClientError.session_expired) {
      buttons.push({
        title: 'Logout',
        onClick: () => {
          this.props.logout();
        },
      });
    } else {
      buttons.push({
        title: 'Ok',
      });
    }

    this.props.showModal({
      view: MessageModal,
      props: {
        message: e.response?.data?.message || e.message,
        caption: 'info',
        buttons,
      },
    });
  };

  private downloadRecord = async () => {
    const { record } = this.props;

    if (record.status !== RecordItemStatus.available) {
      return;
    }

    try {
      await downloadRecording(record);
    } catch (e) {
      this.handleDownloadRecordError(e);
    }
  };

  private renderButtons() {
    if (!this.props.isOpened) {
      return null;
    }

    const {
      record: { status },
      isOwner,
    } = this.props;
    const disabled = !this.props.isReady || status === 'failed';
    const href = disabled ? 'javascript:void(0)' : this.buildPreviewHref();

    return (
      <>
        <MenuItem disabled={disabled} href={href} target="_blank" icon="eye" />
        <MenuItem disabled={disabled} onClick={this.downloadRecord} icon="download" />
        <MenuItem disabled={disabled} onClick={this.copyToClipboard} icon="copy" />
        {isOwner && <MenuItem onClick={this.remove} icon="delete" />}
      </>
    );
  }

  public render() {
    const { isReady, isOpened } = this.props;

    return (
      <UserMenuContainer expanded={isOpened}>
        {this.renderButtons()}
        <MenuButton disabled={!isReady} active={isOpened} onClick={this.toggleVisibility} />
      </UserMenuContainer>
    );
  }
}

export default connect<{}, IRecordMenuDispatchProps>(
  (state: IStoreState, ownProps: IRecordMenuProps) => ({
    ...ownProps,
  }),
  (dispatch: Dispatch<AnyAction>) => {
    const appActionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        showModal: appActionCreators.showModalDialog,
        logout: appActionCreators.logout,
      },
      dispatch
    );
  }
)(RecordMenu);
