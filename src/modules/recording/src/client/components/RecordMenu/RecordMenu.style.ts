/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme, ITheme } from '../../../../../../ioc/client/types/interfaces';

export interface IRecordMenuContainer extends IRequiredTheme {
  expanded: boolean;
}

export const styledRecordMenuContainer = ({ theme, expanded }: IRecordMenuContainer) => `
  position: ${expanded ? 'absolute' : 'relative'};
  text-align: ${expanded ? 'right' : 'center'};
  padding: 0 ${expanded ? '10px' : 0};
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
  background: ${
    expanded
      ? 'linear-gradient(90deg, rgba(0,0,0,0.5) 0%, rgba(0,0,0,0.5) 60%, rgba(0,0,0,1) 83%, rgba(0,0,0,1) 100%)'
      : ''
  }
`;

export interface IRecordMenuButton extends IRequiredTheme {
  active: boolean;
}

export const styledRecordMenuButton = ({ theme, active }: IRecordMenuButton) => {
  const icons = clientEntityProvider.getIcons();

  return `
    display: inline-block;
    width: 20px;
    height: 100%;
    text-align: center;
    background: transparent;
    outline: none;
    padding: 0;
    margin: 0;
    border: 0;
    margin-left: ${active ? '48px' : 0};
    color: ${active ? theme.primaryColor : theme.primaryTextColor};
  
    &:before {
      font-size: 16px;
      content: ${icons.code.menuDotted};
      font-family: ${icons.fontFamily};
    }
  
    &:disabled {
      opacity: 0.2;
    }
  `;
};

export interface IRecordMenuItem extends IRequiredTheme {
  disabled?: boolean;
  icon: string;
}

const iconColorByName = (theme: ITheme, icon: string) => {
  if (icon === 'delete') {
    return theme.buttonWarningColor;
  }

  return theme.primaryTextColor;
};

const iconSizeByName = (theme: ITheme, icon: string) => {
  if (icon === 'delete') {
    return '18px';
  }

  return '16px';
};

export const styledMenuItem = ({ theme, icon, disabled = false }: IRecordMenuItem) => {
  const icons = clientEntityProvider.getIcons();

  return `
    display: inline-block;
    width: 32px;
    height: 32px;
    text-decoration: none;
  
    &:before {
      font-size: ${iconSizeByName(theme, icon)};
      content: ${(icons.code as any)[icon]};
      font-family: ${icons.fontFamily};
      color: ${iconColorByName(theme, icon)};
    }
  
    &:visited, &:hover, &:active, &:focus {
      outline: none;
      text-decoration: none;
      color: ${iconColorByName(theme, icon)};
    }
  
    &[disabled] {
      cursor: default;
  
      &:before {
        opacity: 0.3
      }
    }
  `;
};
