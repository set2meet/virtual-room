import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';
import { videoContainerStyle, videoPlayerPageStyle } from './VideoPlayer.style';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const VideoPlayerPage = styled.div`
  ${videoPlayerPageStyle}
`;
const VideoContainer = styled.div`
  ${videoContainerStyle}
`;

export interface IRecordingsContainerProps {
  url?: string;
}

interface IRecordingsContainerStateProps {
  recordSrc: string;
}

export type VideoPlayerContainerProps = IRecordingsContainerProps &
  RouteComponentProps<{}> &
  IRecordingsContainerStateProps;

export type VideoPlayerContainerState = {
  url?: string;
  message?: string;
};

interface IVideoProps {
  url: string;
}

class Video extends React.Component<IVideoProps> {
  public render() {
    const { url } = this.props;

    return <video src={url} controls={true} />;
  }
}

export class VideoPlayer extends React.Component<VideoPlayerContainerProps, VideoPlayerContainerState> {
  public state: VideoPlayerContainerState = {
    url: null,
    message: null,
  };

  private setBlobURL = (url) => {
    this.setState({ url });
  };

  public async componentDidMount() {
    const { recordSrc, url } = this.props;

    try {
      const recordingUrl = url || (await clientEntityProvider.getRecordingsClient().downloadRecord(recordSrc));

      this.setBlobURL(recordingUrl);
    } catch (e) {
      this.setState({
        message: e.response?.data?.message || e.message,
      });
    }
  }

  public componentDidUpdate(
    prevProps: Readonly<VideoPlayerContainerProps>,
    prevState: Readonly<VideoPlayerContainerState>,
    snapshot?: any
  ) {
    if (this.props.url !== prevProps.url) {
      this.setBlobURL(this.props.url);
    }
  }

  private renderVideo() {
    const { url, message } = this.state;

    if (url) {
      return <Video url={url} />;
    }

    return message;
  }

  public render() {
    return (
      <VideoPlayerPage>
        <VideoContainer>{this.renderVideo()}</VideoContainer>
      </VideoPlayerPage>
    );
  }
}

export default connect<IRecordingsContainerStateProps>((state: any, ownProps: RouteComponentProps<{}>) => ({
  recordSrc: location.search.substr(1),
  history: ownProps.history,
  location: ownProps.location,
}))(VideoPlayer);
