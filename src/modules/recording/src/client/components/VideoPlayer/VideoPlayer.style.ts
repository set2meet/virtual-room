/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { css } from 'styled-components';

export const videoPlayerPageStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1em;
  padding-bottom: 2em;
  height: 100%;
`;

export const videoContainerStyle = css`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;

  video {
    max-width: 100%;
    max-height: 100%;
  }
`;
