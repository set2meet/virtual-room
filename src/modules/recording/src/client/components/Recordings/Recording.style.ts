/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import backgroundImageNoRec from './norecording.png';
import { rgba } from 'polished';
import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';

const VER_MARGIN = 25;
const HOR_MARGIN = 20;
const BACK_LINK_HEIGHT = 44;
const PAGE_TITLE_HEIGHT = 50;
const STATUS_INDICATOR_SIZE = 7;
const IMAGE_NO_REC_WIDTH = 382;
const IMAGE_NO_REC_HEIGHT = 219;
const NO_REC_MESSAGE_PADDING_BOTTON = 150;

export const styledRecordingPage = ({ theme }: IRequiredTheme) => `
  display: block;
  color: ${theme.primaryTextColor};
  margin: 0px ${HOR_MARGIN}px ${VER_MARGIN}px ${HOR_MARGIN}px;
  height: calc(100% - 25px);
  overflow: hidden;
`;

export const styledBackPageLink = ({ theme }: IRequiredTheme) => {
  const icons = clientEntityProvider.getIcons();

  return `
    height: ${BACK_LINK_HEIGHT}px;
    width: 100%;
    display: flex;
    align-items: center;
    font-size: 16px;
  
    label {
      display: inline-block;
      height: ${BACK_LINK_HEIGHT}px;
      color: ${theme.primaryColor};
      cursor: pointer;
      margin: 0;
      line-height: ${BACK_LINK_HEIGHT}px;
  
      &:before {
        font-size: 26px;
        content: ${icons.code.arrowLeft};
        font-family: ${icons.fontFamily};
        vertical-align: middle;
      }
  
      > span {
        margin-left: 6px;
        vertical-align: middle;
        display: inline-block;
        height: 26px;
      }
    }
  `;
};

export const styledPageTitle = () => `
  height: ${PAGE_TITLE_HEIGHT}px;
  width: 100%;
  display: flex;
  align-items: center;
  font-size: 20px;
`;

export interface IStatusIndicatorProps {
  color: string;
}

export const styledStatusIndicator = ({ color }: IStatusIndicatorProps) => `
  white-space: nowrap;

  &:before {
    content: ' ';
    margin-right: 6px;
    font-size: 1px;
    border-radius: 50%;
    display: inline-block;
    width: ${STATUS_INDICATOR_SIZE}px;
    height: ${STATUS_INDICATOR_SIZE}px;
    background-color: ${color};
  }
`;

export const styledNoRecMessage = () => `
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  padding-bottom: ${NO_REC_MESSAGE_PADDING_BOTTON}px;
  align-items: center;
  justify-content: center;
`;

export const styledNoRecImage = () => `
  width: ${IMAGE_NO_REC_WIDTH}px;
  height: ${IMAGE_NO_REC_HEIGHT}px;
  background-image: url(${backgroundImageNoRec});
`;

export const styledNoRecLabelTitle = () => `
  width: 100%;
  font-family: Roboto;
  font-size: 20px;
  font-weight: normal;
  text-align: center;
  color: #ffffff;
  margin-top: 40px;
  display: block;
`;

export const styledNoRecLabelDescr = () => `
  width: 100%;
  font-family: Roboto;
  font-size: 20px;
  font-weight: normal;
  text-align: center;
  color: #ffffff;
`;

export const styledRecordsList = ({ theme }: IRequiredTheme) => `
  .cl-align-center {
    text-align: center;
    justify-content: center;
  }

  .ws-10 {
    word-spacing: 10px;
  }

  &.ReactTable {
    border: 0;
    height: calc(100% - ${BACK_LINK_HEIGHT}px);

    .rt-tbody {
      .rt-tr-group {
        height: 40px;
        max-height: 40px;
        position: relative;
        padding: 0;
        border-bottom: 1px solid ${theme.backgroundColorSecondary};

        &:last-child {
          border-bottom: 1px solid ${theme.backgroundColorSecondary};
        }
      }
    }
  }

  .rt-th {
    display: flex;
    align-items: center;
    vertical-align: middle;
    font-size: 14px;

    &:focus {
      outline: none;
    }

    > div {
      width: inherit;
      height: inherit;
    }
  }

  .rt-thead {
    height: 44px;
    background-color: ${theme.backgroundColorSecondary};
  }

  .rt-td {
    display: flex;
    align-items: center;
  }
`;

export const styledRecordRemoving = ({ theme }: IRequiredTheme) => `
  position: absolute;
  text-align: right;
  padding: 0 10px;
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
  background: ${rgba(theme.backgroundColor, 0.8) /* tslint:disable-line*/};
  cursor: not-allowed;
`;
