import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import Navigation from './components/Navigation';
import Loading from './components/Loading';
import EmptyList from './components/Empty';
import RecordsTable from './components/RecordsTable/RecordsTable';
import RecordModalRemove from '../RecordModalRemove/RecordModalRemove';
import { TRecordItem } from '@s2m/recording-db/lib/types';
import styled from 'styled-components';
import { styledRecordingPage } from './Recording.style';
import { IAppModalDialogDefinition, IStoreState } from '../../../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { bindActionCreators } from 'redux';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';

const RecordingPage = styled.div`
  ${styledRecordingPage};
`;

interface IRouterParams {
  userId?: string;
}

interface IRecordingsContainerStateProps {
  userId: string;
  routeUserId: string;
  records?: TRecordItem[];
}

interface IRecordingsContainerDispatchProps {
  fetchRecordings: () => any;
  showModal: (modal: IAppModalDialogDefinition) => any;
}

type RecordingsContainerProps = RouteComponentProps<IRouterParams> &
  IRecordingsContainerStateProps &
  IRecordingsContainerDispatchProps;

class RecordingsContainer extends React.Component<RecordingsContainerProps> {
  private _checkRouteValidness() {
    const { routeUserId, userId } = this.props;

    if (routeUserId !== userId) {
      this.props.history.replace(`/recordings/${userId}`);
    }
  }

  private _goHome = () => {
    this.props.history.push('/');
  };

  private _removeRecord = (id: string) => {
    const record = this.props.records.filter((rec) => rec.id === id)[0];

    this.props.showModal({
      view: RecordModalRemove,
      props: { record },
    });
  };

  public componentDidMount() {
    this._checkRouteValidness();

    this.props.fetchRecordings();
  }

  public render() {
    const { records, userId } = this.props;

    if (!records) {
      return <Loading />;
    }

    if (records.length === 0) {
      return (
        <RecordingPage>
          <Navigation goHomePage={this._goHome} />
          <EmptyList />
        </RecordingPage>
      );
    }

    return (
      <RecordingPage>
        <Navigation goHomePage={this._goHome} />
        <RecordsTable records={records} userId={userId} onRemove={this._removeRecord} />
      </RecordingPage>
    );
  }
}

export default connect<IRecordingsContainerStateProps, IRecordingsContainerDispatchProps>(
  (state: IStoreState, ownProps: RouteComponentProps<IRouterParams>) => ({
    routeUserId: ownProps.match.params.userId,
    userId: state.auth?.user?.id,
    history: ownProps.history,
    location: ownProps.location,
    records: state.recording.records,
  }),
  (dispatch) => {
    const appActionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        showModal: appActionCreators.showModalDialog,
        fetchRecordings: actionCreators.fetchRecordings,
      },
      dispatch
    );
  }
)(RecordingsContainer);
