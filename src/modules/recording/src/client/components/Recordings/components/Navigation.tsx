import React from 'react';
import styled from 'styled-components';
import { styledBackPageLink } from '../Recording.style';

type EmptyListProps = {
  goHomePage: () => void;
};

const BackPageLink = styled.div`
  ${styledBackPageLink};
`;

export default (({ goHomePage }: EmptyListProps) => {
  return (
    <BackPageLink>
      <label onClick={goHomePage}>Back to homepage</label>
    </BackPageLink>
  );
}) as React.StatelessComponent<EmptyListProps>;
