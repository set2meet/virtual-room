import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import styled from 'styled-components';
import { TRecordItem, RecordItemStatus } from '@s2m/recording-db/lib/types';
import RecordMenu from '../../../RecordMenu/RecordMenu';
import {
  IStatusIndicatorProps,
  styledStatusIndicator,
  styledRecordsList,
  styledRecordRemoving,
} from '../../Recording.style';
import dateFormat from 'dateformat';

interface IRecordsTableProps {
  userId: string;
  records: TRecordItem[];
}

const DATE_STR_FMT = 'yyyy-mm-dd HH:MM';

const RecordsList = styled(ReactTable)`
  ${styledRecordsList};
`;

const StatusIndicator = styled.div<IStatusIndicatorProps>`
  ${styledStatusIndicator};
`;

const RecordRemoving = styled.div`
  ${styledRecordRemoving};
`;

type TableCellRender<T> = {
  original: T;
};

const STATUS_COLOR: Record<string, string> = {
  processing: '#ffd54f',
  available: '#32CD32',
  recording: '#fff',
  error: '#fc5a1b',
  removing: '#575757',
  failed: '#ff0000',
};

interface IRecordsTableDispatchProps {
  onRemove: (id: string) => void;
}

type RTState = {
  openedMenuRecordId: string;
};

type RTProps = IRecordsTableProps & IRecordsTableDispatchProps;

export default class RecordsTable extends React.Component<RTProps, RTState> {
  constructor(props: RTProps) {
    super(props);
    this.state = { openedMenuRecordId: null };
  }

  private _removeRecordById = (id: string) => {
    this.props.onRemove(id);
  };

  private _toogleOpenedRecordMenu = (id: string) => {
    const recId = this.state.openedMenuRecordId;

    this.setState({
      openedMenuRecordId: recId === id ? null : id,
    });
  };

  private _fieldTitle = ({ original: rec }: TableCellRender<TRecordItem>) => {
    if (rec.src) {
      return (
        <a href={`/video/?${rec.src}`} target="_blank">
          {rec.name}
        </a>
      );
    }

    return rec.name;
  };

  private _fieldCreatedAt = ({ original: rec }: TableCellRender<TRecordItem>) => {
    (window as any).dateFormat = dateFormat;
    return dateFormat(rec.createdAt, DATE_STR_FMT);
  };

  private _fieldStatus = ({ original: rec }: TableCellRender<TRecordItem>) => {
    const { status } = rec;

    return <StatusIndicator color={STATUS_COLOR[status]}>{status}</StatusIndicator>;
  };

  private _fieldSize = ({ original: rec }: TableCellRender<TRecordItem>) => {
    if (rec.size && rec.resolution) {
      return `${rec.resolution} (${rec.size})`;
    }

    return '---';
  };

  private _fieldMenu = ({ original: rec }: TableCellRender<TRecordItem>) => {
    const isReady = rec.status === RecordItemStatus.available || rec.status === RecordItemStatus.failed;
    const isOwner = !!rec.meta?.isOwner;
    const isOpened = rec.id === this.state.openedMenuRecordId;

    if (rec.status === 'removing') {
      return <RecordRemoving />;
    }

    return (
      <RecordMenu
        record={rec}
        isReady={isReady}
        isOpened={isOpened}
        isOwner={isOwner}
        onRemove={this._removeRecordById}
        onToggle={this._toogleOpenedRecordMenu}
      />
    );
  };

  public render() {
    const { records } = this.props;

    return (
      <RecordsList
        data={records}
        columns={[
          {
            Header: 'Title',
            id: 'id',
            Cell: this._fieldTitle,
            sortable: false,
          },
          {
            Header: 'Created At',
            id: 'id',
            Cell: this._fieldCreatedAt,
            sortable: false,
            className: 'cl-align-center ws-10',
            width: 200,
          },
          {
            Header: 'Duration',
            accessor: 'duration',
            sortable: false,
            className: 'cl-align-center',
            width: 140,
          },
          {
            Header: 'Size',
            Cell: this._fieldSize,
            sortable: false,
            className: 'cl-align-center',
            width: 160,
          },
          {
            Header: 'Status',
            accessor: 'status',
            sortable: false,
            Cell: this._fieldStatus,
            width: 150,
          },
          {
            Header: '',
            sortable: false,
            Cell: this._fieldMenu,
            className: 'cl-align-center',
            width: 40,
          },
        ]}
        style={{
          width: '100%',
        }}
        resizable={false}
        showPagination={false}
        pageSize={records.length}
        className="-striped"
      />
    );
  }
}
