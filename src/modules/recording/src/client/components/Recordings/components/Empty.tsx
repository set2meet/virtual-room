import React from 'react';
import styled from 'styled-components';
import { styledNoRecMessage, styledNoRecImage, styledNoRecLabelTitle, styledNoRecLabelDescr } from '../Recording.style';

const NoRecMessage = styled.div`
  ${styledNoRecMessage};
`;
const NoRecImage = styled.div`
  ${styledNoRecImage};
`;
const LabelTitle = styled.div`
  ${styledNoRecLabelTitle};
`;
const LabelDescr = styled.div`
  ${styledNoRecLabelDescr};
`;

export default (() => {
  return (
    <NoRecMessage>
      <NoRecImage />
      <LabelTitle>The meetings that you record will appear here.</LabelTitle>
      <LabelDescr>To record a meeting, click on the REC button after the meeting starts.</LabelDescr>
    </NoRecMessage>
  );
}) as React.StatelessComponent;
