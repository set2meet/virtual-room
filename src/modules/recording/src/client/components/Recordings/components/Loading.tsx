import React from 'react';
import clientEntityProvider from '../../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export default (() => {
  const { WaitingPage } = clientEntityProvider.getComponentProvider().UI;

  return <WaitingPage text="Recordings list is loading..." />;
}) as React.StatelessComponent;
