import React from 'react';
import { shallowWithTheme } from '../../../../../../../../test/utils/utils';
import noop from 'lodash/noop';
import RecordsTable from './RecordsTable';
import setupIoCClientForTest from '../../../../../../../../test/utils/ioc/testIoCClient';
import { buildRecordItem } from '../../../../../../../../test/mocks/recording';
import { TRecordItem } from '@s2m/recording-db/lib/types';

beforeAll(setupIoCClientForTest());

describe('RecordsTable', () => {
  it('RecordsTable', () => {
    const userId = 'userId_1';
    const records: TRecordItem[] = [buildRecordItem({ id: 'rec1', src: 'src1' })];
    const component = shallowWithTheme()(<RecordsTable onRemove={noop} userId={userId} records={records} />);

    expect(component).toMatchSnapshot();
  });
});
