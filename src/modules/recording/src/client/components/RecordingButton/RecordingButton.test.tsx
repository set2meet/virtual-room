import { RecordingButton, IRecordingButtonProps } from './RecordingButton';
import noop from 'lodash/noop';
import React from 'react';
import { enzymeCleanup } from '../../../../../../test/utils/utils';
import setupIoCClientForTest from '../../../../../../test/utils/ioc/testIoCClient';
import EnzymeMountTracker from '../../../../../../test/utils/EnzymeMountTracker';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

Object.defineProperty(navigator, 'userAgent', {
  value:
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
  configurable: true,
  writable: true,
});

beforeEach(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('RecordingButton', () => {
  it('no recording for Owner', async () => {
    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={true}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={true}
        isRoomRecording={false}
        isRecording={false}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('recording for Owner', async () => {
    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={true}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={true}
        isRoomRecording={true}
        isRecording={true}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper.find('RecordingRestricted').length > 0).toBeFalsy();
    expect(wrapper).toMatchSnapshot();
  });

  it('no recording for non Owner', async () => {
    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={false}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={true}
        isRoomRecording={false}
        isRecording={false}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('recording for non Owner', async () => {
    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={false}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={true}
        isRoomRecording={true}
        isRecording={true}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('not supported by browser', async () => {
    await clientEntityProvider.destroy();
    Object.defineProperty(navigator, 'userAgent', {
      value: 'Mozilla/5.0 (win32) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0',
      configurable: true,
      writable: true,
    });
    await setupIoCClientForTest()();

    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={true}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={true}
        isRoomRecording={true}
        isRecording={false}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper.find('RecordingRestricted').length > 0).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
  it('functionality disabled', async () => {
    const wrapper = EnzymeMountTracker.mount<IRecordingButtonProps, {}>(
      <RecordingButton
        isOwner={true}
        tryStartRecording={false}
        tryStopRecording={false}
        isFunctionalityEnabled={false}
        isRoomRecording={true}
        isRecording={false}
        isSuspended={false}
        recordingStart={noop}
        recordingStop={noop}
        sessionId={'123'}
        isReconnecting={false}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
