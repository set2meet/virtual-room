import React from 'react';
import { connect } from 'react-redux';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import styled from 'styled-components';
import {
  IButtonContainerProps,
  styledButtonContainer,
  styledinnerCircleOffRecord,
  styledinnerCircleOnRecord,
  styledlabelRecStyle,
  styledmiddleCircle,
  styledouterCircle,
  styledRecordStatusOff,
  styledRecordStatusOn,
  styledSpinnerContainer,
} from './RecordingButton.style';
import { IRecordingButtonOwnProps } from './IRecordingButtonProps';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';
import { IStoreState } from '../../../../../../ioc/client/types/interfaces';

const TOOLTIP_FOR_GUEST_ON = {
  'data-tooltip': 'Recording in progress',
  'data-tooltip-pos': 'down',
};
const TOOLTIP_FOR_GUEST_WAITING = {
  'data-tooltip': 'No recording in progress',
  'data-tooltip-pos': 'down',
};
const TOOLTIP_REC_STATE_ON = {
  'data-tooltip': 'Stop Recording',
  'data-tooltip-pos': 'down',
};
const TOOLTIP_REC_STATE_OFF = {
  'data-tooltip': 'Start Recording',
  'data-tooltip-pos': 'down',
};
const TOOLTIP_REC_STATE_WAITING = {
  'data-tooltip': 'Starting recording',
  'data-tooltip-pos': 'down',
};

const TOOLTIP_REC_STATE_RESTRICTED = {
  'data-tooltip': 'Please use Google Chrome to record the meeting',
  'data-tooltip-pos': 'down',
};

const ButtonContainer = styled.div<IButtonContainerProps>`
  ${styledButtonContainer};
`;

const LabelRec = styled.label`
  ${styledlabelRecStyle};
`;

const OuterCircleComponent = styled(styledouterCircle)``;

const MiddleCircleComponent = styled.div`
  ${styledmiddleCircle};
`;

const InnerCircleOnRecordComponent = styled.div`
  ${styledinnerCircleOnRecord};
`;

const InnerCircleOffRecordComponent = styled.div`
  ${styledinnerCircleOffRecord};
`;

const RecordStatusOn = styledRecordStatusOn;

const RecordStatusOff = styled.div`
  ${styledRecordStatusOff};
`;

const SpinnerContainer = styled.div`
  ${styledSpinnerContainer};
`;

const RestrictedRecordingComponent = styled(ButtonContainer)`
  cursor: not-allowed;

  ${LabelRec} {
    color: #4b4b4b;
  }
  ${OuterCircleComponent} {
    background: #4b4b4b;
  }
  ${MiddleCircleComponent} {
    background: #333333;
  }
  ${InnerCircleOffRecordComponent} {
    background: #4b4b4b;
  }
`;

interface IRecordingButtonStateProps {
  isOwner: boolean;
  tryStartRecording: boolean;
  tryStopRecording: boolean;
  isFunctionalityEnabled: boolean;
  isRoomRecording: boolean;
  isSuspended: boolean;
  isRecording: boolean;
}

interface IRecordingButtonDispatchProps {
  recordingStart: () => void;
  recordingStop: () => void;
}

const RecordingRestricted = (props: { title: string }) => {
  return (
    <RestrictedRecordingComponent {...TOOLTIP_REC_STATE_RESTRICTED}>
      <LabelRec>{props.title}</LabelRec>
      <OuterCircleComponent>
        <MiddleCircleComponent>
          <InnerCircleOffRecordComponent />
        </MiddleCircleComponent>
      </OuterCircleComponent>
    </RestrictedRecordingComponent>
  );
};

export type IRecordingButtonProps = IRecordingButtonStateProps &
  IRecordingButtonDispatchProps &
  IRecordingButtonOwnProps;

export class RecordingButton extends React.Component<IRecordingButtonProps> {
  private _waitingChangeStatus: boolean = false;
  private _recordingStartTimeoutId: any = null;

  private get _buttonDisabled() {
    return !this.props.isOwner || this.props.tryStartRecording;
  }

  public componentDidMount() {
    this._checkStartedRecording();
  }

  public componentWillUnmount() {
    clearTimeout(this._recordingStartTimeoutId);
  }

  public componentDidUpdate(prevProps: IRecordingButtonProps) {
    this._checkStartedRecording();

    if (this.props.isRecording === this.props.isRoomRecording) {
      this._waitingChangeStatus = false;
    }
  }

  public render() {
    const { tryStartRecording, isRecording, isRoomRecording, isReconnecting, isSuspended } = this.props;
    const title = 'REC';

    if (!this.props.isOwner) {
      const isButtonOn = isRoomRecording && !isSuspended;
      const Icon = isButtonOn ? RecordStatusOn : RecordStatusOff;
      const tooltips = isButtonOn ? TOOLTIP_FOR_GUEST_ON : TOOLTIP_FOR_GUEST_WAITING;

      return (
        <ButtonContainer {...tooltips}>
          <LabelRec>{title}</LabelRec>
          <Icon />
        </ButtonContainer>
      );
    }

    if (!(this.props.isFunctionalityEnabled && this.props.sessionId)) {
      return null;
    }
    // Checks if it s chrome or not. Cuz recording works ONLY in chrome for now.
    const browser = clientEntityProvider.getBrowser().info;
    const isChrome = browser.name && browser.name === 'chrome';

    if (!isChrome) {
      return <RecordingRestricted title={title} />;
    }

    const StateIcon = isRecording ? InnerCircleOnRecordComponent : InnerCircleOffRecordComponent;
    const tooltipProps = tryStartRecording
      ? TOOLTIP_REC_STATE_WAITING
      : isRecording
      ? TOOLTIP_REC_STATE_ON
      : TOOLTIP_REC_STATE_OFF;
    const buttonProps = {
      ...tooltipProps,
      onClick: this._buttonRecClick,
      clickable: !this._buttonDisabled,
    };

    return (
      <ButtonContainer {...buttonProps}>
        <LabelRec>{title}</LabelRec>
        {tryStartRecording || isReconnecting ? (
          <SpinnerContainer />
        ) : (
          <OuterCircleComponent>
            <MiddleCircleComponent>
              <StateIcon />
            </MiddleCircleComponent>
          </OuterCircleComponent>
        )}
      </ButtonContainer>
    );
  }

  private _buttonRecClick = () => {
    if (this._buttonDisabled) {
      return;
    }

    if (this.props.isRecording) {
      this.props.recordingStop();
    } else {
      this.props.recordingStart();
    }

    this._waitingChangeStatus = true;
  };

  private _checkStartedRecording() {
    if (!this.props.isOwner) {
      return;
    }

    const { isRecording, isRoomRecording, tryStartRecording, tryStopRecording } = this.props;

    // there are an error on janus with black video and silence audio if
    // start records quick after joint to the room
    if (isRoomRecording && !isRecording && !tryStartRecording && !tryStopRecording && !this._waitingChangeStatus) {
      this._waitingChangeStatus = true;

      this._recordingStartTimeoutId = setTimeout(() => {
        this.props.recordingStart();
        this._waitingChangeStatus = false;
      }, 1000);
    }
  }
}

export default connect<IRecordingButtonStateProps, IRecordingButtonDispatchProps, IRecordingButtonOwnProps>(
  (state: IStoreState, ownProps: IRecordingButtonOwnProps) => ({
    ...ownProps,
    isRecording: state.recording.isRecording,
    isRoomRecording: state.room.recording.isRecording,
    isSuspended: state.room.recording.isSuspend,
    tryStartRecording: state.recording.tryStartRecording,
    tryStopRecording: state.recording.tryStopRecording,
    isFunctionalityEnabled: clientEntityProvider.getConfig().recording.enabled,
  }),
  {
    recordingStart: actionCreators.recordingStart,
    recordingStop: actionCreators.recordingStop,
  }
)(RecordingButton);
