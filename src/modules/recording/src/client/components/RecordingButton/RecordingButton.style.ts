/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import styled, { css, keyframes } from 'styled-components';
import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';

const paddingDiv = '0px 12px';
const colorLabelRec = '#C8C8C8';
const fontLabelRec = '14px Roboto';
const outerCircleHeight = '21px';
const outerCircleWidth = '21px';
const middleCircleHeight = '17px';
const middleCircleWidth = '17px';
const innerCircleHeight = '9px';
const innerCircleWidth = '9px';
const statusRecHeight = '12.1px';
const statusRecWidth = '12.1px';

export interface IButtonContainerProps extends IRequiredTheme {
  clickable?: boolean;
}

export const styledButtonContainer = ({ clickable = false, theme }: IButtonContainerProps) => `
  padding: ${paddingDiv};
  display: flex;
  flex-flow: row nowrap;
  position: relative;
  cursor: ${clickable ? 'pointer' : 'default'};

  > label {
    cursor: inherit;
  }

  &:before {
    cursor: default;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 1px;
    height: 34px;
    border: 1px;
    margin-right: 18px;
    margin-left: 5px;
    background-color: #686868;
  }
`;

export const styledlabelRecStyle = () => `
  font: ${fontLabelRec};
  color: ${colorLabelRec};
  display: flex;
  align-self: center;
  margin-right: 8px;
  margin-bottom: 0;
`;

export const styledouterCircle = styled.div`
  background: #a9a9a9;
  border-radius: 50%;
  height: ${outerCircleHeight};
  width: ${outerCircleWidth};
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: center;
  justify-content: center;
`;

export const styledmiddleCircle = css`
  background: #333333;
  border-radius: 50%;
  height: ${middleCircleHeight};
  width: ${middleCircleWidth};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const styledinnerCircleOnRecord = () => `
  background: #fc5a1b;
  border-radius: 50%;
  display: flex;
  height: ${innerCircleHeight};
  width: ${innerCircleWidth};
`;

export const styledinnerCircleOffRecord = () => `
  background: white;
  border-radius: 50%;
  display: flex;
  height: ${innerCircleHeight};
  width: ${innerCircleWidth};
`;

export const styledRecordStatusOn = styled.div`
  display: flex;
  align-self: center;
  background: #fc5a1b;
  border-radius: 50%;
  height: ${statusRecHeight};
  width: ${statusRecWidth};
`;

export const styledRecordStatusOnDisabled = styled(styledRecordStatusOn)`
  background: red;
`;

export const styledRecordStatusOff = () => `
  display: flex;
  align-self: center;
  background: #575757;
  border-radius: 50%;
  height: ${statusRecHeight};
  width: ${statusRecWidth};
`;

const keyframesRotating = keyframes`
0% {
  transform: rotate(0deg);
}
100% {
  transform: rotate(360deg);
}
`;

export const styledSpinnerContainer = ({ theme }: IRequiredTheme) => css`
  display: flex;
  align-self: center;
  height: 20px;
  width: 20px;
  border: 2px solid;
  border-radius: 50%;
  animation: ${keyframesRotating} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: ${theme.primaryColor} ${theme.primaryColor} ${theme.primaryColor} transparent;
`;
