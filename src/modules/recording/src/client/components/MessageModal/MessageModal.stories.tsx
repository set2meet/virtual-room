import React from 'react';
import { action } from '@storybook/addon-actions';
import MessageModal, { TButtonDesc } from './MessageModal';

export default {
  title: 'recording/MessageModal',
  component: MessageModal,
};

export const DefaultMessageBox = (props) => {
  const buttons: TButtonDesc[] = [
    {
      title: 'Ok',
      onClick: action('ok'),
    },
    {
      title: 'Cancel',
      onClick: action('cancel'),
    },
  ];

  return <MessageModal caption="some caption" message="some message" buttons={buttons} onHide={action('onHide')} />;
};
