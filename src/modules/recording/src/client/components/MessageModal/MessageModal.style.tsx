import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';
import { css } from 'styled-components';

const MARGIN = 18;

export const styledModalContainer = ({ theme }: IRequiredTheme) => css`
  border-top: 2px solid ${theme.primaryColor};
  padding: ${MARGIN}px;
  position: relative;
`;

export const styledControls = ({ theme }: IRequiredTheme) => css`
  text-align: right;
  padding-top: ${MARGIN}px;
  border-top: 1px solid ${theme.backgroundColorThird};

  > * {
    margin-left: 25px;
  }
`;

export const styledHeader = css`
  font-size: 18px;
`;

export const styledDescription = css`
  font-size: 14px;
  margin-top: 28px;
  margin-bottom: 44px;
`;
