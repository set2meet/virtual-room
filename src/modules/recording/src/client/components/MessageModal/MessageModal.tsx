import * as React from 'react';
import styled from 'styled-components';
import flow from 'lodash/flow';
import noop from 'lodash/noop';
import { styledModalContainer, styledHeader, styledDescription, styledControls } from './MessageModal.style';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { IButtonProps } from '../../../../../../ioc/client/types/interfaces';
import Skeleton from 'react-loading-skeleton';

const ModalContent = styled.div`
  ${styledModalContainer};
`;

const Header = styled.div`
  ${styledHeader};
`;

const Description = styled.div`
  ${styledDescription};
`;

const Controls = styled.div`
  ${styledControls};
`;

export type TButtonHandler = () => void;
export type TButtonDesc = {
  title: string;
  onClick?: TButtonHandler;
};

export type TMessageModalProps = {
  caption: string;
  message: string;
  buttons: TButtonDesc[];
  onHide: TButtonHandler;
};

export class MessageModal extends React.Component<TMessageModalProps> {
  public static SIZE = {
    width: 432,
    height: 230,
  };

  public render() {
    const { caption, message, buttons, onHide } = this.props;
    const Button = clientEntityProvider.resolveComponentSuspended<IButtonProps>(
      ComponentRegistryKey.Button,
      // tslint:disable-next-line:no-magic-numbers
      <Skeleton width={42} height={34} />
    );

    return (
      <ModalContent>
        <Header>{caption}</Header>
        <Description>{message}</Description>
        <Controls>
          {buttons &&
            buttons.map(({ title, onClick = noop }) => (
              <Button key={title} title={title} bsStyle="secondary" onClick={flow(onClick, onHide)} />
            ))}
        </Controls>
      </ModalContent>
    );
  }
}

export default MessageModal;
