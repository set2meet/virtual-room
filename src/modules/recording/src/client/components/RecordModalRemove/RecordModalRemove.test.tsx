import React from 'react';
import { enzymeCleanup, InitModulesWrapper } from '../../../../../../test/utils/utils';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import setupIoCClientForTest from '../../../../../../test/utils/ioc/testIoCClient';
import { loadAsyncTree } from '../../../../../../test/utils/storybook';
import EnzymeMountTracker from '../../../../../../test/utils/EnzymeMountTracker';
import { TStore } from '../../../../../../ioc/client/types/interfaces';
import { IStoreExt } from '../../../../../../test/utils/reduxMockEnhancer';
import { buildRecordItem } from '../../../../../../test/mocks/recording';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';
import RecordModalRemove from './RecordModalRemove';

beforeEach(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('<RecordModalRemove />', () => {
  const record = buildRecordItem({
    id: `rec9`,
    src: 'src9',
    name: 'With size',
    status: 'available',
    size: '123',
    resolution: '123',
  });
  const onHide = jest.fn();

  afterEach(() => {
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

    onHide.mockReset();
    (store as TStore & IStoreExt).clearActions();
  });

  it('should hide on cancel', async (done) => {
    const wrapper = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <RecordModalRemove onHide={onHide} record={record} />
        </InitModulesWrapper>
      )
    );

    wrapper.find('button[title="Cancel"]').simulate('click');
    expect(onHide).toBeCalledTimes(1);
    done();
  });

  it('should hide on close', async (done) => {
    const commonIcons = clientEntityProvider.getIcons();
    const wrapper = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <RecordModalRemove onHide={onHide} record={record} />
        </InitModulesWrapper>
      )
    );

    expect(wrapper.find('button').at(0)).toHaveStyleRule('content', `${commonIcons.code.close}`, {
      modifier: '&:before',
    });
    wrapper.find('button').at(0).simulate('click');
    expect(onHide).toBeCalledTimes(1);
    done();
  });

  it('should remove and hide', async (done) => {
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const wrapper = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <RecordModalRemove onHide={onHide} record={record} />
        </InitModulesWrapper>
      )
    );

    wrapper.find('button[title="Delete"]').simulate('click');
    expect((store as TStore & IStoreExt).getActions()).toContainEqual(
      expect.objectContaining(actionCreators.recordingRemoveRecord('rec9'))
    );
    expect(onHide).toBeCalledTimes(1);
    done();
  });
});
