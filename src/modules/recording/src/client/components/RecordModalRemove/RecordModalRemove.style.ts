/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IRequiredTheme } from '../../../../../../ioc/client/types/interfaces';

const MARGIN = 18;
const BTTN_CLOSE_SIZE = 16;

export const styledModalContainer = ({ theme }: IRequiredTheme) => `
  border-top: 2px solid ${theme.primaryColor};
  padding: ${MARGIN}px;
  position: relative;
`;

export const styledButtonClose = () => {
  const icons = clientEntityProvider.getIcons();

  return `
    top: ${MARGIN}px;
    right: ${MARGIN}px;
    cursor: pointer;
    position: absolute;
    display: inline-block;
    width: ${BTTN_CLOSE_SIZE}px;
    height: ${BTTN_CLOSE_SIZE}px;
    border: none;
    background: transparent;
    outline: none;
    padding: 0;
  
    &:before {
      color: #fff;
      font-size: 16px;
      width: ${BTTN_CLOSE_SIZE}px;
      height: ${BTTN_CLOSE_SIZE}px;
      line-height: ${BTTN_CLOSE_SIZE}px;
      vertical-align: middle;
      display: inline-block;
      content: ${icons.code.close};
      font-family: ${icons.fontFamily};
    }
  `;
};

export const styledControls = ({ theme }: IRequiredTheme) => `
  text-align: right;
  padding-top: ${MARGIN}px;
  border-top: 1px solid ${theme.backgroundColorThird};

  > * {
    margin-left: 25px;
  }
`;

export const styledSplitter = () => `

`;

export const styledHeader = () => `
  font-size: 18px;
`;

export const styledDescription = () => `
  font-size: 14px;
  margin-top: 28px;
  margin-bottom: 44px;
`;
