import React from 'react';
import { action } from '@storybook/addon-actions';
import { select, text } from '@storybook/addon-knobs';
import RecordModalRemove from './RecordModalRemove';
import { buildRecordItem } from '../../../../../../test/mocks/recording';

export default {
  title: 'recording/RecordModalRemove',
  component: RecordModalRemove,
  parameters: {
    redux: {
      enable: true,
    },
  },
};

const recordStatuses = ['processing', 'available', 'recording', 'error', 'removing', 'failed', 'random'];

export const Default = () => {
  const index = 1;
  const record = buildRecordItem({
    id: `rec${index}`,
    src: `src${index}`,
    name: text('name', `Record ${index}`, `Record ${index}`),
    status: select('status', recordStatuses, status, `Record ${index}`),
  });

  return <RecordModalRemove record={record} onHide={action('onHide')} />;
};
