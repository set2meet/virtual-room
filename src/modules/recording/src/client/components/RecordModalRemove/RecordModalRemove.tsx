import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { IStoreState } from '../../../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  styledModalContainer,
  styledButtonClose,
  styledHeader,
  styledDescription,
  styledControls,
} from './RecordModalRemove.style';
import { IRecordModalRemoveOwnProps } from './IRecordModalRemoveProps';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';

const ModalContent = styled.div`
  ${styledModalContainer};
`;

const ButtonClose = styled.button`
  ${styledButtonClose};
`;

const Header = styled.div`
  ${styledHeader};
`;

const Description = styled.div`
  ${styledDescription};
`;

const Controls = styled.div`
  ${styledControls};
`;

interface IRecordModalRemoveDispatchProps {
  removeRecord: (id: string) => any;
}

type IRecordModalRemoveProps = IRecordModalRemoveOwnProps & IRecordModalRemoveDispatchProps;

export class RecordModalRemove extends React.Component<IRecordModalRemoveProps> {
  public static SIZE = {
    width: 432,
    height: 230,
  };

  private _hide = () => {
    this.props.onHide();
  };

  private _remove = () => {
    this.props.removeRecord(this.props.record.id);
    this._hide();
  };

  public render() {
    const { Button } = clientEntityProvider.getComponentProvider().UI;

    return (
      <ModalContent>
        <ButtonClose onClick={this._hide} />
        <Header>Delete recording</Header>
        <Description>Do you want to delete this recording? The deleted recording cannot be restored</Description>
        <Controls>
          <Button title="Cancel" bsStyle="secondary" onClick={this._hide} />
          <Button title="Delete" bsStyle="warning" onClick={this._remove} />
        </Controls>
      </ModalContent>
    );
  }
}

export default connect<{}, IRecordModalRemoveDispatchProps, IRecordModalRemoveOwnProps>(
  (state: IStoreState, ownProps: IRecordModalRemoveOwnProps) => ownProps,
  {
    removeRecord: actionCreators.recordingRemoveRecord,
  }
)(RecordModalRemove);
