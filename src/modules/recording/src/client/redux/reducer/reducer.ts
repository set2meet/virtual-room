/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { TRecordItem } from '@s2m/recording-db/lib/types';
import RecordingAction from '../../../common/redux/RecordingAction';
import { IRecordingLocalReduxState } from './IRecordingLocalReduxState';

const INITIAL_LOCAL_STATE: IRecordingLocalReduxState = {
  records: null,
  tryStartRecording: false,
  tryStopRecording: false,
  isRecording: false,
};

const typingCreatedAt = (records: TRecordItem[]) => {
  records.forEach((rec) => {
    rec.createdAt = new Date(rec.createdAt);
  });

  return records;
};

const sortRecordsByCreatedDate = (records: TRecordItem[]) => {
  return records.sort((r1, r2) => (r2.createdAt > r1.createdAt ? 1 : -1));
};

interface IRecordingLocalReduxEnum {
  [key: string]: (state: IRecordingLocalReduxState, action: AnyAction) => IRecordingLocalReduxState;
}

const reducers: IRecordingLocalReduxEnum = {
  [RecordingAction.RECORDING_START]: (state, action) => ({
    ...state,
    tryStartRecording: true,
  }),
  [RecordingAction.RECORDING_STARTED]: (state, action) => ({
    ...state,
    isRecording: true,
    tryStartRecording: false,
  }),
  [RecordingAction.RECORDING_START_ERROR]: (state, action) => ({
    ...state,
    isRecording: false,
    tryStartRecording: false,
  }),
  [RecordingAction.RECORDING_SUSPEND]: (state, action) => ({
    ...state,
    isRecording: false,
  }),
  [RecordingAction.ROOM_RECORDING_STOPPED]: (state, action) => ({
    ...state,
    isRecording: false,
    tryStopRecording: false,
  }),
  [RecordingAction.RECORDING_CANCELED]: (state, action) => ({
    ...state,
    isRecording: false,
    tryStartRecording: false,
  }),
  [RecordingAction.ROOM_RECORDING_RECORD_REMOVED]: (state, action) => {
    const { record } = action;
    const { id } = record;

    return {
      ...state,
      records: state.records.filter((rec) => rec.id !== id),
    };
  },
  [RecordingAction.ROOM_RECORDING_RECORD_UPDATED]: (state, action) => {
    const { record } = action;
    const { id } = record;
    const records = state.records || [];

    typingCreatedAt([record]);

    return {
      ...state,
      records: records.map((rec) => {
        if (rec.id === id) {
          record.meta = rec.meta;

          return record;
        }

        return rec;
      }),
    };
  },
  [RecordingAction.ROOM_RECORDING_SET_RECORDS]: (state, action) => {
    const curRecords = state.records || [];
    const newRecords = action.records || [];
    // remove old records that appears in new set
    const ids = newRecords.reduce((res: any, rec: TRecordItem) => {
      return (res[rec.id] = 1), res;
    }, {});
    const records = curRecords.filter((rec: TRecordItem) => !ids[rec.id]);

    return {
      ...state,
      records: sortRecordsByCreatedDate(records.concat(typingCreatedAt(newRecords))),
    };
  },
};

type IRecordingLocalReduxReducer = (state: IRecordingLocalReduxState, action: AnyAction) => IRecordingLocalReduxState;

const localReducer: IRecordingLocalReduxReducer = (state: IRecordingLocalReduxState, action: AnyAction) => {
  const reducer = reducers[action.type];

  if (reducer) {
    return reducer(state, action);
  }

  return state || INITIAL_LOCAL_STATE;
};

export default localReducer;
