/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import localReducer from './reducer';
import RecordingAction from '../../../common/redux/RecordingAction';
import { TRecordItem } from '@s2m/recording-db/lib/types';

test('recording localReducer', () => {
  let state = localReducer(null, { type: 'init' });

  state = localReducer(state, { type: RecordingAction.RECORDING_START });

  expect(state).toEqual({
    records: null,
    isRecording: false,
    tryStartRecording: true,
    tryStopRecording: false,
  });

  state = localReducer(state, { type: RecordingAction.RECORDING_STARTED });

  expect(state).toEqual({
    records: null,
    isRecording: true,
    tryStartRecording: false,
    tryStopRecording: false,
  });

  state = localReducer(state, { type: RecordingAction.RECORDING_SUSPEND });

  expect(state).toEqual({
    records: null,
    tryStartRecording: false,
    tryStopRecording: false,
    isRecording: false,
  });
});

test('recording localReducer', () => {
  const records = ([
    {
      id: 'id1',
      name: 'name1',
      meta: {
        isOwner: true,
      },
    },
  ] as any) as TRecordItem[];

  let state = localReducer(null, { type: 'init' });

  state.records = records;
  state = localReducer(state, {
    type: RecordingAction.ROOM_RECORDING_RECORD_UPDATED,
    record: { id: 'id1', name: 'name2' },
  });

  const record = state.records[0];

  expect(record.id).toBe('id1');
  expect(record.name).toBe('name2');
  expect(record.meta.isOwner).toBeDefined();
});
