/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import {
  IAppReduxEnumMiddleware,
  IModuleReduxMiddleware,
  TWebRTCServices,
} from '../../../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { AnyAction, Dispatch } from 'redux';
import Socket = SocketIOClient.Socket;
import RecordingService from '../../services/RecordingService/RecordingService';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';
import RecordingAction from '../../../common/redux/RecordingAction';
import showErrorNotification from './utils/showErrorNotification';
import createRecordingService from './utils/createRecordingService';
import { RecordingServiceEvents } from '../../services/constants';

let recordingService: RecordingService = null;

const middlewareEnum: IAppReduxEnumMiddleware<TWebRTCServices> = {
  [RecordingAction.RECORDING_START](store, next, action, socket, storeKey, webrtcServices) {
    next(action);

    createRecordingService(store, webrtcServices).then((service) => {
      recordingService = service;

      recordingService.on(RecordingServiceEvents.RecordingEnd, () => {
        recordingService = null;
      });
    });
  },
  [RecordingAction.RECORDING_SUSPEND](store, next, action) {
    next(action);

    if (recordingService) {
      recordingService.suspend();
    }
  },
  [RecordingAction.RECORDING_STOP](store, next, action) {
    next(action);

    if (recordingService) {
      recordingService.stop();
    }
  },
  [RecordingAction.USER_STREAM_ADDED](store, next, action) {
    if (recordingService) {
      recordingService.addStream(action.stream);
    }

    next(action);
  },
  [RecordingAction.USER_STREAM_REMOVED](store, next, action) {
    if (recordingService) {
      recordingService.removeStream(action.stream);
    }

    next(action);
  },
  [RecordingAction.RECORDING_REMOVE_RECORD](store, next, action) {
    next(action);
    const recordId = action.recordId;

    clientEntityProvider
      .getRecordingsClient()
      .deleteRecord(recordId)
      .catch((e) => {
        store.dispatch(showErrorNotification(e));
      });
  },
  [RecordingAction.FETCH_RECORDINGS](store, next, action) {
    next(action);

    const state = store.getState();
    const userId = state.auth.user.id;

    clientEntityProvider
      .getRecordingsClient()
      .fetchRecordings(userId)
      .then((records) => {
        store.dispatch(actionCreators.roomRecordingSetRecords(records));
      })
      .catch((e) => {
        store.dispatch(showErrorNotification(e));
      });
  },
};

const middleware: IModuleReduxMiddleware<TWebRTCServices> = (storeKey: string) => (socket: Socket, webrtcServices) => (
  store
) => {
  return (next: Dispatch<AnyAction>) => (action: AnyAction) => {
    if (middlewareEnum[action.type]) {
      return middlewareEnum[action.type](store, next, action, socket, storeKey, webrtcServices);
    }

    return next(action);
  };
};

export default middleware;
