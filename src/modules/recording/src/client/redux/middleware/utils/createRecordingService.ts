/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import RecordingService from '../../../services/RecordingService/RecordingService';
import { RecordingServiceEvents } from '../../../services/constants';
import actionCreators from '../../../../common/redux/actionCreators/actionCreators';
import showErrorNotification from './showErrorNotification';
import { TMiddlewareAPI, TWebRTCServices } from '../../../../../../../ioc/common/ioc-interfaces';

const ErrorCode: Record<string, number> = {
  'Not available': 20,
  'Authentication error': 21,
  'Max connections exceeded': 22,
};

const createRecordingService = async (store: TMiddlewareAPI, services: TWebRTCServices): Promise<RecordingService> => {
  const state = store.getState();
  const { room } = state;
  const recId = room.recording.recId;
  const { serviceName } = room.webrtcService;
  const recordingServiceConfig = clientEntityProvider.getConfig().recording;
  const webrtcService = clientEntityProvider.getUtils().getWebrtcServiceByName(serviceName, services);
  const {
    ConnectionFailure,
    RecordingStarted,
    RecordingStopped,
    RecordingCanceled,
    RecordingSuspended,
    RecordingBroken,
    RecordingEnd,
  } = RecordingServiceEvents;

  const recordingService = new RecordingService(recordingServiceConfig, webrtcService.getUserStreams());

  recordingService.on(ConnectionFailure, (reason: string) => {
    store.dispatch(actionCreators.recordingStartError(ErrorCode[reason]));
  });

  recordingService.on(RecordingBroken, (reason: string) => {
    store.dispatch(actionCreators.recordingStartError(ErrorCode[reason]));
  });

  recordingService.on(RecordingStarted, (recordId: string) => {
    store.dispatch(actionCreators.recordingStarted(recordId));
    // if recId = undefined - this is new record
    if (recId !== recordId) {
      const roomId = state.room.state.id;

      clientEntityProvider
        .getRecordingsClient()
        .createRecord(recordId, roomId)
        .then(() => {
          store.dispatch(
            clientEntityProvider.getAppActionCreators().sendRoomAction(actionCreators.roomRecordingStarted(recordId))
          );
        })
        .catch((e) => {
          store.dispatch(showErrorNotification(e));
        });
    } else {
      store.dispatch(
        clientEntityProvider.getAppActionCreators().sendRoomAction(actionCreators.roomRecordingResumed(recordId))
      );
    }
  });

  recordingService.on(RecordingStopped, (recordId) => {
    recordingService.emit(RecordingEnd);

    clientEntityProvider
      .getRecordingsClient()
      .stopRecord(recordId)
      .then(() => {
        store.dispatch(
          clientEntityProvider.getAppActionCreators().sendRoomAction(actionCreators.roomRecordingStopped(recordId))
        );
      })
      .catch((e) => {
        store.dispatch(showErrorNotification(e));
      });
  });

  recordingService.on(RecordingSuspended, (recordId) => {
    recordingService.emit(RecordingEnd);

    store.dispatch(
      clientEntityProvider.getAppActionCreators().sendRoomAction(actionCreators.roomRecordingSuspended(recordId))
    );
  });

  recordingService.on(RecordingCanceled, () => {
    recordingService.emit(RecordingEnd);

    store.dispatch(actionCreators.recordingCanceled());
  });

  recordingService.connect(recId, {
    type: 'mp4',
  });

  return recordingService;
};

export default createRecordingService;
