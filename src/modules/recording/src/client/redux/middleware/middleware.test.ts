/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import middleware from './middleware';
import setupIoCClientForTest from '../../../../../../test/utils/ioc/testIoCClient';
import { mock } from 'jest-mock-extended';
import { TStore } from '../../../../../../ioc/client/types/interfaces';
import actionCreators from '../../../common/redux/actionCreators/actionCreators';
import Socket = SocketIOClient.Socket;
import { waitNextTick } from '../../../../../../test/utils/utils';
import createRecordingService from './utils/createRecordingService';
import RecordingService from '../../services/RecordingService/RecordingService';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import RecordingsClient from '../../services/RecordingsClient/RecordingsClient';
import { buildRecordItem } from '../../../../../../test/mocks/recording';
import { IStoreState } from '../../../../../../ioc/client/types/interfaces';
import showErrorNotification from './utils/showErrorNotification';

/* tslint:disable:no-magic-numbers */

const mockRecordingService = mock<RecordingService>();
const mockRecordingsClient = mock<RecordingsClient>();

jest.mock('./utils/createRecordingService');
jest.mock('./utils/showErrorNotification');

beforeAll(setupIoCClientForTest());

describe('recording client middleware', () => {
  let recordingClientMiddleware = null;

  const next = jest.fn();
  const store = mock<TStore>();
  const socket = mock<Socket>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    (createRecordingService as jest.Mock).mockResolvedValue(mockRecordingService);

    recordingClientMiddleware = middleware('storeKey1')(socket)(store)(next);
  });

  test('RECORDING_SUSPEND, RECORDING_STOP, USER_STREAM_ADDED, USER_STREAM_REMOVED when RecordingService is not defined', () => {
    expect(createRecordingService).not.toBeCalled();

    recordingClientMiddleware(actionCreators.userStreamAdded('stream1'));
    expect(mockRecordingService.addStream).not.toBeCalled();

    recordingClientMiddleware(actionCreators.userStreamRemoved('stream2'));
    expect(mockRecordingService.removeStream).not.toBeCalled();

    recordingClientMiddleware(actionCreators.recordingStop());
    expect(mockRecordingService.stop).not.toBeCalled();

    recordingClientMiddleware(actionCreators.recordingSuspend());
    expect(mockRecordingService.suspend).not.toBeCalled();

    expect(next).toBeCalledTimes(4);
  });

  test('RECORDING_SUSPEND, RECORDING_STOP, USER_STREAM_ADDED, USER_STREAM_REMOVED when RecordingService created', async () => {
    recordingClientMiddleware(actionCreators.recordingStart());

    expect(createRecordingService).toBeCalled();

    await waitNextTick();

    recordingClientMiddleware(actionCreators.userStreamAdded('stream1'));
    expect(mockRecordingService.addStream).toBeCalledWith('stream1');

    recordingClientMiddleware(actionCreators.userStreamRemoved('stream2'));
    expect(mockRecordingService.removeStream).toBeCalledWith('stream2');

    recordingClientMiddleware(actionCreators.recordingStop());
    expect(mockRecordingService.stop).toBeCalled();

    recordingClientMiddleware(actionCreators.recordingSuspend());
    expect(mockRecordingService.suspend).toBeCalled();

    expect(next).toBeCalledTimes(5);
  });

  test('next should be called for any action', async () => {
    const anyAnotherAction = { type: 'unknownAction' };

    recordingClientMiddleware(anyAnotherAction);

    expect(next).toBeCalledWith(anyAnotherAction);
  });

  describe('FETCH_RECORDINGS action', () => {
    const records = [buildRecordItem({ id: 'id1', src: 'src1' })];
    const state = {
      auth: {
        user: {
          id: 'userId1',
        },
      },
    };

    test('success path', async () => {
      jest.spyOn(clientEntityProvider, 'getRecordingsClient').mockReturnValue(mockRecordingsClient);

      store.getState.mockReturnValue(state as IStoreState);

      mockRecordingsClient.fetchRecordings.mockResolvedValue(records);

      recordingClientMiddleware(actionCreators.fetchRecordings());

      expect(next).toBeCalledWith(actionCreators.fetchRecordings());
      expect(mockRecordingsClient.fetchRecordings).toBeCalledWith('userId1');

      await waitNextTick();

      expect(store.dispatch).toBeCalledWith(actionCreators.roomRecordingSetRecords(records));
    });

    test('fail path', async () => {
      jest.spyOn(clientEntityProvider, 'getRecordingsClient').mockReturnValue(mockRecordingsClient);

      store.getState.mockReturnValue(state as IStoreState);

      mockRecordingsClient.fetchRecordings.mockRejectedValue('error');

      recordingClientMiddleware(actionCreators.fetchRecordings());

      expect(next).toBeCalledWith(actionCreators.fetchRecordings());
      expect(mockRecordingsClient.fetchRecordings).toBeCalledWith('userId1');

      await waitNextTick();

      expect(store.dispatch).not.toBeCalledWith(actionCreators.roomRecordingSetRecords(records));
      expect(store.dispatch).toBeCalled();
      expect(showErrorNotification).toBeCalled();
    });
  });

  describe('RECORDING_REMOVE_RECORD action', () => {
    test('success path', () => {
      jest.spyOn(clientEntityProvider, 'getRecordingsClient').mockReturnValue(mockRecordingsClient);
      mockRecordingsClient.deleteRecord.mockResolvedValueOnce(null);

      recordingClientMiddleware(actionCreators.recordingRemoveRecord('record1'));

      expect(next).toBeCalledWith(actionCreators.recordingRemoveRecord('record1'));
      expect(mockRecordingsClient.deleteRecord).toBeCalledWith('record1');
    });

    test('failed path', async () => {
      jest.spyOn(clientEntityProvider, 'getRecordingsClient').mockReturnValue(mockRecordingsClient);

      mockRecordingsClient.deleteRecord.mockRejectedValue('error1');

      recordingClientMiddleware(actionCreators.recordingRemoveRecord('record2'));

      expect(next).toBeCalledWith(actionCreators.recordingRemoveRecord('record2'));

      await waitNextTick();

      expect(showErrorNotification).toBeCalled();
    });
  });
});
