/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IRecordingActionCreators } from './IRecordingActionCreators';
import RecordingAction from '../RecordingAction';
import { TRecordItem } from '@s2m/recording-db/lib/types';

const actionCreators: IRecordingActionCreators = {
  roomRecordingSuspended: (recordId: string) => ({
    type: RecordingAction.ROOM_RECORDING_SUSPENDED,
    recordId,
  }),
  roomRecordingResumed: (recordId: string) => ({
    type: RecordingAction.ROOM_RECORDING_RESUMED,
    recordId,
  }),
  recordingStop: () => ({
    type: RecordingAction.RECORDING_STOP,
  }),
  recordingSuspend: () => ({
    type: RecordingAction.RECORDING_SUSPEND,
  }),
  roomRecordingSetRecords: (records: TRecordItem[]) => ({
    type: RecordingAction.ROOM_RECORDING_SET_RECORDS,
    records,
  }),
  fetchRecordings: () => ({
    type: RecordingAction.FETCH_RECORDINGS,
  }),
  recordingParticipantAdd: (recordId: string, userId: string) => ({
    type: RecordingAction.ROOM_RECORDING_PARTICIPANT_ADD,
    recordId,
    userId,
  }),
  recordingStart: () => ({
    type: RecordingAction.RECORDING_START,
  }),
  recordingStarted: (recordId: string) => ({
    type: RecordingAction.RECORDING_STARTED,
    recordId,
  }),
  recordingCanceled: () => ({
    type: RecordingAction.RECORDING_CANCELED,
  }),
  recordingStartError: (code: number) => ({
    type: RecordingAction.RECORDING_START_ERROR,
    code,
  }),
  recordingRemoveRecord: (recordId: string) => ({
    type: RecordingAction.RECORDING_REMOVE_RECORD,
    recordId,
  }),
  roomRecordingStarted: (recordId: string) => ({
    type: RecordingAction.ROOM_RECORDING_STARTED,
    recordId,
  }),
  roomRecordingStopped: (recordId: string) => ({
    type: RecordingAction.ROOM_RECORDING_STOPPED,
    recordId,
  }),
  userStreamAdded: (stream) => ({
    type: RecordingAction.USER_STREAM_ADDED,
    stream,
  }),
  userStreamRemoved: (stream) => ({
    type: RecordingAction.USER_STREAM_REMOVED,
    stream,
  }),
};

export default actionCreators;
