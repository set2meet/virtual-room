/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { Action } from 'redux';
import { TRecordItem } from '@s2m/recording-db/lib/types';

interface IActionWithRecordId extends Action<string> {
  recordId: string;
}

interface IActionWithRecordIdAndUserId extends Action<string> {
  userId: string;
  recordId: string;
}

interface IActionWithRecords extends Action<string> {
  records: TRecordItem[];
}

interface IActionRecordingError extends Action<string> {
  code: number;
}

export interface IRecordingActionCreators {
  roomRecordingResumed: (recId: string) => IActionWithRecordId;
  roomRecordingSuspended: (recId: string) => IActionWithRecordId;
  recordingStop: () => Action<string>;
  recordingSuspend: () => Action<string>;
  roomRecordingSetRecords: (records: TRecordItem[]) => IActionWithRecords;

  recordingParticipantAdd: (recordId: string, userId: string) => IActionWithRecordIdAndUserId;
  fetchRecordings: () => Action<string>;
  recordingStart: () => Action<string>;
  recordingStarted: (recordId: string) => IActionWithRecordId;
  recordingStartError: (code: number) => IActionRecordingError;
  recordingCanceled: () => Action<string>;
  recordingRemoveRecord: (recordId: string) => IActionWithRecordId;
  roomRecordingStarted: (recId: string) => IActionWithRecordId;
  roomRecordingStopped: (recId: string) => IActionWithRecordId;
  userStreamAdded: (stream) => Action<string>;
  userStreamRemoved: (stream) => Action<string>;
}
