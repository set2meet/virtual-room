/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import roomReducer from './reducer';
import actionCreators from '../../redux/actionCreators/actionCreators';
import RecordingAction from '../RecordingAction';

test('recording roomReducer', () => {
  let state = roomReducer(null, { type: 'init' });

  expect(state).toEqual({
    isRecording: false,
    isSuspend: false,
    recId: '',
  });

  state = roomReducer(state, actionCreators.roomRecordingStarted('rec1'));

  expect(state).toEqual({
    isRecording: true,
    isSuspend: false,
    recId: 'rec1',
  });

  state = roomReducer(state, actionCreators.roomRecordingStopped('rec1'));

  expect(state).toEqual({
    isRecording: false,
    isSuspend: false,
    recId: '',
  });

  state = roomReducer(state, { type: RecordingAction.ROOM_RECORDING_SET_SUSPEND_VALUE, value: true });

  expect(state).toEqual({
    isRecording: false,
    isSuspend: true,
    recId: '',
  });
});
