/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { IRecordingReduxState } from './IRecordingReduxState';
import RecordingAction from '../RecordingAction';

const INITIAL_STATE: IRecordingReduxState = {
  isRecording: false,
  isSuspend: false,
  recId: '',
};

interface IRecordingReduxEnum {
  [key: string]: (state: IRecordingReduxState, action: AnyAction) => IRecordingReduxState;
}

const reducers: IRecordingReduxEnum = {
  [RecordingAction.ROOM_RECORDING_STARTED]: (state: IRecordingReduxState, action: AnyAction) => ({
    ...state,
    isRecording: true,
    isSuspend: false,
    recId: action.recordId || state.recId || '',
  }),
  [RecordingAction.ROOM_RECORDING_STOPPED]: (state: IRecordingReduxState, action: AnyAction) => {
    return {
      ...state,
      isRecording: false,
      recId: '',
    };
  },
  [RecordingAction.ROOM_RECORDING_SET_SUSPEND_VALUE]: (state: IRecordingReduxState, action: AnyAction) => {
    return {
      ...state,
      isSuspend: action.value,
    };
  },
};

type IRecordingReduxReducer = (state: IRecordingReduxState | null, action: AnyAction) => IRecordingReduxState;

const roomReducer: IRecordingReduxReducer = (state, action) => {
  state = state || INITIAL_STATE;
  const reducer = reducers[action.type];

  if (reducer) {
    return reducer(state, action);
  }

  return state;
};

export default roomReducer;
