/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import reduxMiddleware from './src/client/redux/middleware/middleware';
import roomReducer from './src/common/redux/reducer/reducer';
import reducer from './src/client/redux/reducer/reducer';
import { TRecordingModuleInitOptions } from './TRecordingModuleInitOptions';

const createRecordingModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'recording',
  moduleInitOptions?: TRecordingModuleInitOptions
): Promise<IS2MModule> => {
  const webrtcServices =
    moduleInitOptions &&
    (moduleInitOptions.webRTCServices
      ? await moduleInitOptions.webRTCServices
      : (await moduleInitOptions.webRTCServicesFactory())());

  return {
    rootComponent: null,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: reducer,
        },
        reducersToMerge: {
          room: {
            [stateKey]: roomReducer,
          },
        },
        middlewares: [reduxMiddleware(stateKey)(socket, webrtcServices)],
      },
    ],
  };
};

export default createRecordingModule;
