/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import io from 'socket.io-client';
import resolveUrl from 'resolve-url';

const createClientSocket = (config) => {
  const remoteUrl = resolveUrl(config.service.url, config.service.path);

  const socket = io(remoteUrl, {
    path: config.service.path + 'ws',
    autoConnect: false,
    reconnectionDelayMax: 1000,
    timeout: 3000,
  });

  return socket;
};

export default createClientSocket;
