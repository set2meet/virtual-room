# How to edit icons

1. Open https://icomoon.io/app/#/projects
2. Click "Import Project"
3. Select file "icons.json" for import
4. After success import appears project "Virtual Room Common Icons"
5. Click "Load" button for this project
6. Now you can import (from svg)/remove new icons, build and download new woff file

If woff file was changed - download JSON project meta data of the project and commit it too
To download JSON project file:

1. Open https://icomoon.io/app/#/projects
2. Click "Download" button for this project
3. Copy JSON file and rename as icons.json
4. Commit one with new woff file

# Add to styles

1. Add to index.d.ts
