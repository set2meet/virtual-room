/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// @ts-ignore
import url from './icons.woff';
import { vrCommon } from '../../../common';
import ICommonIcons = vrCommon.client.UI.ICommonIcons;

const FONT_FAMILY_NAME = 'VRM Common Icons';
const e = (code: TemplateStringsArray): string => `"\\\e${code.raw[0]}"`;

export const injectIconsFontToGlobalStyle = `
  @font-face {
    font-family: ${FONT_FAMILY_NAME};
    src: url('${url}') format("woff");
  }
`;

const icons: ICommonIcons = {
  fontFamily: 'VRM Common Icons',
  code: {
    pencil: e`902`,
    trash: e`903`,
    userTemp: e`904`,
    arrowUp: e`905`,
    arrowRight: e`906`,
    arrowLeft: e`907`,
    arrowDown: e`908`,
    pause: e`909`,
    play: e`90a`,
    close: e`920`,
    close2: e`90f`,
    headset: e`91f`,
    hand: e`915`,
    handMuted: e`914`,
    videoCamera: e`91e`,
    videoCameraMuted: e`91d`,
    microphone: e`91c`,
    microphoneMuted: e`91b`,
    speaker: e`91a`,
    messages: e`919`,
    messagesStroked: e`900`,
    user: e`918`,
    users: e`917`,
    usersStroked: e`901`,
    desktop: e`916`,
    smile: e`913`,
    attach: e`912`,
    send: e`911`,
    link: e`90b`,
    exit: e`90d`,
    linkMirror: e`910`,
    closeRound: e`90f`,
    menuDotted: e`90e`,
    phoneTube: e`90c`,
    fullscreenOn: e`922`,
    fullscreenOff: e`921`,
    brush: e`923`,
    codeEditor: e`924`,
    asset: e`925`,
    notepad: e`926`,
    addParticipant: e`928`,
    unlock: e`927`,
    conference: e`929`,
    checkboxOn: e`92d`,
    checkboxOff: e`92c`,
    checkMark: e`92a`,
    speedometer: e`92b`,
    presentation: e`92f`,
    upload: e`92e`,
    graph2d: e`930`,
    endCall: e`931`,
    leave: e`932`,
    construct: e`933`,
    copy: e`934`,
    download: e`935`,
    eye: e`936`,
    delete: e`937`,
  },
};

export default icons;
