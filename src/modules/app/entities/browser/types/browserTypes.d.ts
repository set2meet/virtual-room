/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IModuleStorageKey } from '../../../../../ioc/client/providers/StorageManager/types';

export type RecommendedBrowser = 'firefox' | 'safari' | 'chrome' | 'edge' | 'chromium' | 'vivaldi';
export type RecommendedBrowsers = Record<
  RecommendedBrowser,
  {
    version: number;
    link: string;
  }
>;

export interface IBrowserInfo {
  os: string;
  name: string;
  /**
   * Each user can be connected to the app with several clients
   * So, to separate data about used user client - use browser uuid, stored on local storage
   */
  uuid: string;
  /**
   * local session id, to identify and keep id of window on refresh
   * so we always can join the same room after refresh
   */
  sessionId: string;
  osFamily: string;
  version: string;
  versionMajor: number;
  versionMinor: number;
}

export interface IBrowser {
  info: IBrowserInfo;
  supportWebRTC: boolean;
  recomendations: RecommendedBrowser[];
  supportedBrowsers: RecommendedBrowsers;
}

export interface IBrowserStorageKeys extends Record<string, IModuleStorageKey<any>> {
  BROWSER_UUID_KEY: IModuleStorageKey<string>;
  BROWSER_SESSION_ID: IModuleStorageKey<string>;
}
