/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { detect, BrowserInfo } from 'detect-browser';
import supportedBrowsers from './types/supportedBrowsers.json';
import getUuid from './uuid';
import { Storages } from '../../../../ioc/client/providers/StorageManager/storages/Storages';
import {
  IBrowser,
  IBrowserInfo,
  IBrowserStorageKeys,
  RecommendedBrowser,
  RecommendedBrowsers,
} from './types/browserTypes';

const recommendedBrowsers: RecommendedBrowsers = supportedBrowsers;

const recomendations: Record<string, RecommendedBrowser[]> = {
  Windows: ['chrome', 'firefox', 'edge'],
  Mac: ['safari', 'chrome', 'firefox'],
  Linux: ['chrome', 'chromium', 'firefox', 'vivaldi'],
  others: ['chrome', 'firefox'],
};

const isBrowserPassVersion = (name: RecommendedBrowser, browser: IBrowserInfo): boolean => {
  return !(browser.name === name && browser.versionMajor < recommendedBrowsers[name].version);
};

const isBrowserSupportMediaDevicesAPI = (md: MediaDevices): boolean => {
  return Boolean(md && md.getUserMedia && md.enumerateDevices);
};

export const browserStorageKeys: IBrowserStorageKeys = {
  BROWSER_UUID_KEY: {
    key: `browser:uuid-fingerprint`,
    storage: Storages.LOCAL_STORAGE,
  },
  BROWSER_SESSION_ID: {
    key: `browser:session-id`,
    storage: Storages.SESSION_STORAGE,
  },
};

const browserData = (): IBrowser => {
  const { BROWSER_UUID_KEY, BROWSER_SESSION_ID } = browserStorageKeys;

  const browserInfo: BrowserInfo = detect() as BrowserInfo;
  const browserVersion: number[] = browserInfo.version.split('.').map((v) => parseInt(v, 10));
  const browser: IBrowserInfo = {
    osFamily: browserInfo.os.split(' ')[0],
    os: browserInfo.os,
    name: browserInfo.name,
    uuid: getUuid(BROWSER_UUID_KEY),
    sessionId: getUuid(BROWSER_SESSION_ID),
    version: browserInfo.version,
    versionMajor: browserVersion[0],
    versionMinor: browserVersion[1],
  };
  const isBrowserSupportsWebRTC =
    isBrowserSupportMediaDevicesAPI(navigator.mediaDevices) &&
    isBrowserPassVersion('chromium', browser) &&
    isBrowserPassVersion('firefox', browser) &&
    isBrowserPassVersion('safari', browser) &&
    isBrowserPassVersion('chrome', browser) &&
    isBrowserPassVersion('edge', browser);

  return {
    info: browser,
    supportWebRTC: isBrowserSupportsWebRTC,
    recomendations: recomendations[browser.osFamily] || recomendations.others,
    supportedBrowsers,
  };
};

export default {
  browserData,
  browserStorageKeys,
};
