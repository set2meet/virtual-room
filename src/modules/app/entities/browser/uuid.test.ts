/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import uuid from './uuid';

beforeAll(setupIoCClientForTest());

describe('browser fingerprint', () => {
  beforeEach(() => {
    const storageManager = clientEntityProvider.getStorageManager();

    jest.resetAllMocks();
    jest.restoreAllMocks();
    storageManager.removeAll();
  });

  it('should get existing key from storage', () => {
    const storageManager = clientEntityProvider.getStorageManager();
    const BROWSER_UUID_KEY = clientEntityProvider.getStorageKeys().BROWSER_UUID_KEY;

    storageManager.set(BROWSER_UUID_KEY, '123');
    expect(uuid(BROWSER_UUID_KEY)).toBe('123');
  });

  it('should set key', () => {
    const storageManager = clientEntityProvider.getStorageManager();
    const BROWSER_UUID_KEY = clientEntityProvider.getStorageKeys().BROWSER_UUID_KEY;

    uuid(BROWSER_UUID_KEY);
    expect(storageManager.get(BROWSER_UUID_KEY, null, false)).not.toBeNull();
  });
});
