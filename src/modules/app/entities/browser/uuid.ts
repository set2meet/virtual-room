/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { v4 as guid } from 'uuid';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IModuleStorageKey } from '../../../../ioc/client/providers/StorageManager/types';

/**
 * Each user can be connected to the app with several clients
 * So, to separate data about used user client - use browser uuid, stored on local storage
 */

export default (moduleKey: IModuleStorageKey<string>): string => {
  const storageManager = clientEntityProvider.getStorageManager();
  let uuid: string = storageManager.get<string>(moduleKey, null, false);

  if (!uuid) {
    uuid = guid();
    storageManager.set(moduleKey, uuid);
  }

  return uuid;
};
