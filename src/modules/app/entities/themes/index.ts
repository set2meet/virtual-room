/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/* tslint:disable:no-magic-numbers */

import defaultTheme from './default';
// import oldBlack from './oldBlack';
import { darken, desaturate, lighten } from 'polished';
import _forEach from 'lodash/forEach';
import { ITheme } from './types/ITheme';

const extraProps: { [key in keyof ITheme]?: (T: ITheme) => string } = {
  buttonDefaultColorHover: (theme) => lighten(0.0804, desaturate(0.2273, theme.buttonDefaultColor)),
  buttonDefaultColorActive: (theme) => darken(0.198, desaturate(0.006, theme.buttonDefaultColor)),
  buttonWarningColorHover: (theme) => lighten(0.1235, desaturate(0.035, theme.buttonWarningColor)),
  buttonWarningColorActive: (theme) => darken(0.1, desaturate(0.167, theme.buttonWarningColor)),
  buttonOffColorHover: (theme) => lighten(0.0902, theme.buttonOffColor),
  buttonOffColorActive: (theme) => darken(0.3608, theme.buttonOffColor),
};

// add default hover/active colors for buttons if not declared
const populateSchemeWithExtraColors = (theme: ITheme) => {
  _forEach(extraProps, (val: (t: ITheme) => string, key: Exclude<'userAvatarColors', keyof ITheme>) => {
    theme[key] = theme[key] || val(theme);
  });
};

populateSchemeWithExtraColors(defaultTheme);

export { defaultTheme };
