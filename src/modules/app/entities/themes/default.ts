/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import logoShort from './assets/shortLogo.svg';
import logo from './assets/logo.svg';
import { ITheme } from './types/ITheme';
import customizePageLogo from './assets/customize.svg';
import myRoomPageLogo from './assets/my-room.svg';
import oneTimeRoomPageLogo from './assets/one-time-room.svg';
import { Breakpoints } from './types/breakpoints';

const defaultTheme: ITheme = {
  backgroundColor: '#1f1f1f', // main background color
  backgroundColorSecondary: '#333333',
  backgroundColorThird: '#575757',
  primaryTextColor: '#ffffff',
  primaryColor: '#2BCE72',
  fontColor: '#FAF9F5',
  fontErrorColor: '#C03713',
  h1Color: '#8C8C8C',
  buttonDefaultColor: '#2BCE72',
  buttonWarningColor: '#FC5A1B',
  buttonDisabledColor: '#686868',
  buttonSecondaryColor: 'transparent',
  buttonOffColor: '#E8E8E8',
  logo,
  logoShort,
  selectedElementBorderColorPrimary: '#45B6FC',
  userAvatarColors: [
    '#E9906B',
    '#6AD5CB',
    '#FF8E82',
    '#AEC5EB',
    '#78C0E0',
    '#AA6F63',
    '#B38CB4',
    '#EFBB7C',
    '#087E8B',
    '#FCBCB8',
    '#449DD1',
  ],
  customizePageLogo,
  myRoomPageLogo,
  oneTimeRoomPageLogo,
  breakpoints: {
    tablet: `${Breakpoints.MD}px`,
    desktop: `${Breakpoints.LG}px`,
  },
};

export default defaultTheme;
