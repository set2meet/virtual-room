/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { ITheme } from './types/ITheme';
import defaultTheme from './default';

const oldBlackTheme: ITheme = Object.assign(defaultTheme, {
  primaryColor: '#00bc8c',
  buttonDefaultColor: '#00bc8c',
  buttonWarningColor: '#F24333',
});

export default oldBlackTheme;
