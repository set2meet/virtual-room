/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export interface ITheme {
  backgroundColor: string;
  backgroundColorSecondary: string;
  backgroundColorThird: string;
  primaryTextColor: string;
  primaryColor: string;
  fontColor: string;
  fontErrorColor: string;
  h1Color: string;
  buttonDefaultColor: string;
  buttonDefaultColorHover?: string;
  buttonDefaultColorActive?: string;
  buttonWarningColor: string;
  buttonWarningColorHover?: string;
  buttonWarningColorActive?: string;
  buttonDisabledColor: string;
  buttonSecondaryColor: string;
  buttonOffColor: string;
  buttonOffColorHover?: string;
  buttonOffColorActive?: string;
  selectedElementBorderColorPrimary: string;
  logoShort: string;
  logo: string;
  userAvatarColors: string[];
  customizePageLogo: string;
  myRoomPageLogo: string;
  oneTimeRoomPageLogo: string;
  breakpoints: Record<string, string | number>;
}

export interface IRequiredTheme {
  theme: ITheme;
}

export interface IThemes {
  defaultTheme: ITheme;
  Competentum: ITheme;
}
