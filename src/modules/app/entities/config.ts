/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IClientConfig } from '../../../ioc/client/types/IClientConfig';
import resolveUrl from 'resolve-url';

export const buildConfig = async (): Promise<IClientConfig> => {
  const config: Partial<IClientConfig> = {};

  if ((module as any).hot) {
    // TODO:: possible this is creating conflicts on localhost debuggin!!!!!!!!!!!
    // local path to server
    config.service = {
      url: `https://${process.env.IP}:3040/`,
      path: '/',
    };
  } else {
    // config to remote service
    config.service = {
      url: `/`,
      path: '/api/v1/',
    };
  }

  return fetch(`${resolveUrl(config.service.url, config.service.path)}config`)
    .then((response) => response.json())
    .then((configFromServer) => ({
      ...configFromServer,
      ...config,
    }))
    .catch((e) => {
      throw new Error(`Can't request config file. Maybe you forget to run server. \n"${e.message}"`);
    });
};
