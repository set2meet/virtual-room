/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { createBrowserHistory } from 'history';
import { THistoryFactory } from './types/THistoryFactory';
import { BrowserHistoryBuildOptions } from 'history/createBrowserHistory';
import { THistoryLocationState } from './types/THistory';

const createHistory: THistoryFactory = (options?: BrowserHistoryBuildOptions) => {
  return createBrowserHistory<THistoryLocationState>(options);
};

export default createHistory;
