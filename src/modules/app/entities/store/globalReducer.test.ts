/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _cloneDeep from 'lodash/cloneDeep';
import globalReducer from './globalReducer';
import { Reducer } from 'redux';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';

beforeAll(setupIoCClientForTest());

describe('globalReducer', () => {
  const subReducer: Reducer = (state, action) => {
    return {
      ...state,
    };
  };

  it('shouldn\'t modify INITIAL_STATE with ROOM:SET_INFO action', () => {
    const appActionTypes = clientEntityProvider.getAppActionTypes();
    const reducer = globalReducer(subReducer, appActionTypes);
    // @ts-ignore
    const initialState = reducer({}, { type: '@@INIT' });
    const frozenInitialState = _cloneDeep(initialState);

    reducer(initialState, { type: appActionTypes.ROOM_SET_INFO, roomState: { state: { id: '123' } } });

    expect(initialState).toEqual(frozenInitialState);
  });
});
