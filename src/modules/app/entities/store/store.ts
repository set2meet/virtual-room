/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAPIAction } from '../../../../types/API-types';
import { createStore as createDynamicStore } from 'redux-dynamic-modules-core';
import { TStore } from './types/TStore';
import { TEnhancerFactory, TStoreFactory } from './types/TStoreFactory';
import Socket = SocketIOClient.Socket;
import { IStoreState, IAppActionCreators, IAppActionTypes } from '../../../../ioc/client/types/interfaces';
import { combineRoomReducers } from './globalReducer';

const initStore: TStoreFactory = (
  socket: Socket,
  appActionCreators: IAppActionCreators,
  actionTypes: Partial<IAppActionTypes>,
  storeEnhancerFactory: TEnhancerFactory
): Promise<TStore> => {
  const enhancers = storeEnhancerFactory && [...storeEnhancerFactory()];
  // redux devtools are handled by default (as far as per-module compose / combine logic)
  // @see https://github.com/microsoft/redux-dynamic-modules/blob/master/docs/reference/Devtools.md#redux-devtools-extension
  const store: TStore = createDynamicStore<IStoreState>({
    enhancers,
    advancedNestedCombineReducers: {
      room: combineRoomReducers(actionTypes),
    },
  });
  const dispatchActionFromSocket = (data: IAPIAction) => store.dispatch(data.action);

  socket.on('connect', () => {
    socket.on('action', dispatchActionFromSocket);
  });

  socket.on('disconnect', () => {
    socket.off('action', dispatchActionFromSocket);
  });

  window.onbeforeunload = () => {
    socket.close();
  };

  return Promise.resolve(store);
};

export default initStore;
