/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppActionCreators, IAppActionTypes } from '../../../../../ioc/common/ioc-interfaces';
import Socket = SocketIOClient.Socket;
import { StoreEnhancer } from 'redux';
import { TStore } from './TStore';

export type TEnhancerFactory<Ext extends {} = {}, StateExt extends {} = {}> = () => Array<StoreEnhancer<Ext, StateExt>>;

export type TStoreFactory = (
  socket: Socket,
  appActionCreators: IAppActionCreators,
  actionTypes: Partial<IAppActionTypes>,
  storeEnhancerFactory: TEnhancerFactory
) => Promise<TStore>;
