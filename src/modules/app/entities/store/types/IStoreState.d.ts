/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IApp } from '../../../types/app';
import { RouterState } from 'connected-react-router';
import { Toast } from 'react-toastify-redux';
import {
  ILocalPresentationsReduxState,
  IWebPhoneReduxState,
  IGlobalRoomReduxState,
  IRecordingLocalReduxState,
  IAuthReduxState,
  IConnectionReduxState,
  IFullscreenReduxState,
  IMediaReduxState,
} from '../../../../../ioc/common/ioc-interfaces';

export interface IStoreState {
  app: IApp;
  router: RouterState;
  media: IMediaReduxState;
  toasts: Toast[];
  room: IGlobalRoomReduxState;
  recording: IRecordingLocalReduxState;
  fullscreen: IFullscreenReduxState;
  connection: IConnectionReduxState;
  presentations: ILocalPresentationsReduxState;
  webphone: IWebPhoneReduxState;
  auth: IAuthReduxState;
}
