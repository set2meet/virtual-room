/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { APIAction } from '../APIAction';
import { IAPIActionCreators } from './IAPIActionCreators';

const apiActionCreators: IAPIActionCreators = {
  sendAction: (action: object, traceToLocal: boolean = true) => {
    return {
      type: APIAction.SEND,
      action,
      traceToLocal,
    };
  },
  sendRoomAction: (action: object) => {
    return {
      type: APIAction.ROOM_SEND,
      action,
    };
  },
};

export default apiActionCreators;
