/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import actionCreators from './actionCreators/actionCreators';
import { APIAction } from './APIAction';
import { IAPIActionCreators } from './actionCreators/IAPIActionCreators';
import { IVRModule } from '../../../../ioc/client/types/interfaces';

interface IAPI extends IVRModule<IAPIActionCreators> {
  actionCreators: IAPIActionCreators;
  actionTypes: typeof APIAction;
}

const apiClientModule: IAPI = {
  actionCreators,
  actionTypes: APIAction,
};

export default apiClientModule;
