/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _get from 'lodash/get';
import _set from 'lodash/set';
import _findIndex from 'lodash/findIndex';
import { APIAction } from './APIAction';
import { AnyAction, Dispatch, Store } from 'redux';
import Socket = SocketIOClient.Socket;
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  IAppReduxEnumMiddleware,
  IModuleReduxMiddleware,
  IStoreState,
  TActionMetadata,
  TStore,
} from '../../../../ioc/common/ioc-interfaces';

const queue: Record<string, AnyAction[]> = {
  [APIAction.SEND]: [],
  [APIAction.ROOM_SEND]: [],
};

const applyQueue = (name: string, socket: Socket) => {
  const q = queue[name];
  const len = q.length;

  for (let i = 0; i < len; i++) {
    socket.emit('action', q[i]);
  }

  // clear queue data
  q.length = 0;
};

const sendQueueActions = (socket: Socket) => {
  applyQueue(APIAction.SEND, socket);
  applyQueue(APIAction.ROOM_SEND, socket);
};

const addActionToQueue = (name: string, action: AnyAction) => {
  const q = queue[name];
  const { type } = action;
  const existedInx = _findIndex(q, { type });

  // if this action already puttet to the queue,
  // replace old action with new one
  // and place at the and of queue
  // as last correct action
  //
  if (existedInx > -1) {
    q.splice(existedInx, 1);
  }

  // save action data
  q.push(action);
};

const getActionMeta = (appState: IStoreState, action: any): TActionMetadata => {
  const { sessionId } = action;
  const userId = _get(appState, 'auth.user.id');
  const userName = _get(appState, 'auth.user.displayName');
  const realmName = _get(appState, 'auth.user.realmName');
  const ws = _get(appState, 'room.webrtcService');
  const session = ws?.sessionId ? ws : sessionId ? { sessionId } : null;
  const user =
    userId && userName
      ? {
          id: userId,
          name: userName,
          realmName,
        }
      : null;
  const browser = clientEntityProvider.getBrowser();

  return {
    user,
    session,
    sender: userId,
    hostname: location.hostname,
    roomId: _get(appState, 'room.state.id'),
    time: new Date().toString(),
    browserId: browser.info.uuid,
    isHost: _get(appState, 'room.state.ownerId') === userId,
  };
};

const apiMiddlewareEnum: IAppReduxEnumMiddleware = {
  [APIAction.SEND](store: TStore, next, action, socket) {
    const { traceToLocal } = action;
    const appState = store.getState();
    const userId = _get(appState, 'auth.user.id');

    delete action.traceToLocal;

    if (userId) {
      _set(action, 'action.meta', {
        ...getActionMeta(appState, action.action),
        ..._get(action, 'action.meta', {}),
      });
    }

    if (socket.connected) {
      socket.emit('action', action.action);
    } else {
      addActionToQueue(APIAction.SEND, action.action);
    }

    if (traceToLocal) {
      // send to local reducer
      next(action);
    }
  },
  [APIAction.ROOM_SEND](store: TStore, next, action, socket) {
    const appState = store.getState();

    _set(action, 'action.meta', {
      target: {
        roomId: _get(appState, 'room.state.id'),
      },
      ...getActionMeta(appState, action.action),
      ..._get(action, 'action.meta', {}),
    });

    if (socket.connected) {
      socket.emit('action', action.action);
    } else {
      addActionToQueue(APIAction.ROOM_SEND, action.action);
    }

    next(action);
  },
};

const apiMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => {
  socket.on('authorized', () => sendQueueActions(socket));

  return (store: Store) => (next: Dispatch<AnyAction>) => (action: AnyAction) => {
    if (apiMiddlewareEnum[action.type]) {
      return apiMiddlewareEnum[action.type](store, next, action, socket, storeKey);
    }

    return next(action);
  };
};

export default apiMiddleware;
