/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { Action } from 'redux';
import { IAppModalDialogDefinition } from '../../types/IAppModalDialog';

interface IChangeSettingsPanelTabAction extends Action<string> {
  tab: string;
}

interface IShowModalDialogAction extends Action<string> {
  modal: IAppModalDialogDefinition;
}

interface IGoToRecordingsAction extends Action<string> {
  userId: string;
}

interface IAppChangeWebRtcServiceAction extends Action<string> {
  inx: number;
}

export interface IAppModuleActionCreators {
  roomToggleLeftPanelVisibility: () => Action<string>;
  roomToggleRightPanelVisibility: () => Action<string>;
  changeSettingsPanelTab: (tab: string) => IChangeSettingsPanelTabAction;
  showModalDialog: (modal: IAppModalDialogDefinition) => IShowModalDialogAction;
  hideModalDialog: () => Action<string>;
  gotoRecordings: (userId: string) => IGoToRecordingsAction;
  appChangeWebrtcService: (inx: number) => IAppChangeWebRtcServiceAction;
}
