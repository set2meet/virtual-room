/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppModalDialogDefinition } from '../../../../ioc/client/types/interfaces';
import { AppAction } from '../types/AppAction';
import { IAppModuleActionCreators } from './IAppActionCreators';

export const appActionCreators: IAppModuleActionCreators = {
  roomToggleLeftPanelVisibility: () => ({
    type: AppAction.ROOM_TOGGLE_LEFT_PANEL_VISIBILITY,
  }),
  roomToggleRightPanelVisibility: () => ({
    type: AppAction.ROOM_TOGGLE_RIGHT_PANEL_VISIBILITY,
  }),
  changeSettingsPanelTab: (tab: string) => ({
    type: AppAction.SETTINGS_PANEL_CHANGE_TAB,
    tab,
  }),
  showModalDialog: (modal: IAppModalDialogDefinition) => ({
    type: AppAction.MODAL_DIALOG_SHOW,
    modal,
  }),
  hideModalDialog: () => ({
    type: AppAction.MODAL_DIALOG_HIDE,
  }),
  gotoRecordings: (userId: string) => ({
    type: AppAction.GOTO_RECORDINGS,
    userId,
  }),
  appChangeWebrtcService: (inx: number) => ({
    type: AppAction.WEBRTC_SERVICE_CHANGE,
    inx,
  }),
};
