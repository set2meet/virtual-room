/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Socket = SocketIOClient.Socket;
import { AnyAction, Dispatch, Store } from 'redux';
import _get from 'lodash/get';
import { push } from 'connected-react-router';
import { IClientConfig, IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import { DisconnectReason } from '../../../../ioc/common/ioc-constants';
import { createRecStopNotification, recordingStartErrorNotification } from '../../utils/utils';
import { isGuest, isOwner } from '../selectors';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IStoreState } from '../../../../ioc/client/types/interfaces';

const REC_STOP_NOTIFICATION_TIMEOUT = 3000;

let middlewareEnumNotBuilded: boolean = true;
let appMiddlewareEnum: IAppReduxEnumMiddleware<IClientConfig> = {};

const syncWebrtcServiceName = (store: any, action: AnyAction) => {
  const actionCreators = clientEntityProvider.getActionCreators();
  // before set room state,
  // sync settings webrtc service name with the same in the room
  // so, if room has webrtc service name - set it in settings as based
  const appState: IStoreState = store.getState();
  const roomWebrtcServiceName = _get(action, 'roomState.webrtcService.serviceName', null);

  if (!roomWebrtcServiceName) {
    // no service name if room ID was incorrect
    return;
  }

  const { services, selectedServiceInx, defaultServiceInx } = appState.app.webrtcConfig;
  const settingsWebrtcServiceName = services[selectedServiceInx].name;

  if (!roomWebrtcServiceName) {
    store.dispatch(actionCreators.appChangeWebrtcService(defaultServiceInx));
  } else if (roomWebrtcServiceName !== settingsWebrtcServiceName) {
    const roomServiceInx = services.reduce((num, item, inx) => {
      return item.name === roomWebrtcServiceName ? inx : num;
    }, NaN);

    store.dispatch(actionCreators.appChangeWebrtcService(roomServiceInx));
  }
};

const resetWebrtcServiceName = (store: any) => {
  const actionCreators = clientEntityProvider.getActionCreators();
  const { defaultServiceInx } = store.getState().app.webrtcConfig;

  store.dispatch(actionCreators.appChangeWebrtcService(defaultServiceInx));
};

const isRecording = (store: Store): boolean => {
  const state: IStoreState = store.getState();
  const isRecordingActive = state.recording?.isRecording;
  const recordId = state.room?.recording?.recId;

  return isRecordingActive && !!recordId;
};

const recordingRecordAddUser = (store: Store<IStoreState>): Promise<void> => {
  const state = store.getState();
  const isUserGuest = isGuest(state);
  const recordId = state.room?.recording?.recId;
  const userId = state.auth.user.id;

  if (isUserGuest || !recordId) {
    return;
  }

  return clientEntityProvider.getRecordingsClient().addUserAsParticipant(recordId, userId);
};

const initAppMiddlewareEnum = () => {
  const appActionTypes = clientEntityProvider.getAppActionTypes();
  const actionCreators = clientEntityProvider.getActionCreators();

  middlewareEnumNotBuilded = false;

  appMiddlewareEnum = {
    [appActionTypes.SESSION_CLOSED](store, next, action) {
      const { sessionId } = action;
      const state = store.getState();
      const appActionCreators = clientEntityProvider.getAppActionCreators();

      if (isOwner(state)) {
        // TODO this must be on server side
        // but don't know where this code must be executed (hint: on in chat and not in webphone)
        // we don't have mechanism to dispatch new actions on server-side

        store.dispatch(appActionCreators.sendRoomAction(appActionCreators.clearChat(sessionId)));
      } else if (state.auth.user) {
        const roomId = _get(state, 'room.state.id');

        store.dispatch(appActionCreators.sendAction(appActionCreators.leaveRoom(roomId, state.auth.user.id)));
      }

      next(action);
    },
    /**
     * Reset presentations ONLY when OWNER ends MEETING!
     */
    [appActionTypes.END_MEETING](store: Store, next, action) {
      const appActionCreators = clientEntityProvider.getAppActionCreators();
      store.dispatch(appActionCreators.sendRoomAction(appActionCreators.resetPresentationState()));

      if (isRecording(store)) {
        store.dispatch(appActionCreators.recordingStop());
      }

      next(action);
    },
    [appActionTypes.USER_JOIN_ROOM](store: Store<IStoreState>, next, action) {
      next(action);

      const state = store.getState();
      const userId = state.auth.user.id;
      const participant = clientEntityProvider.getSelectors().webphoneSelectors.getCurrentUserAsParticipant(state);
      const appActionCreators = clientEntityProvider.getAppActionCreators();

      // for self only
      if (userId !== action.userId) {
        return;
      }

      // required by socket sessions map as filter by roomId
      store.dispatch(
        clientEntityProvider.getAppActionCreators().sendRoomAction({
          type: clientEntityProvider.getAppActionTypes().ROOM_JOIN_SUCCESS,
          userId: action.userId,
          roomId: action.roomId,
        })
      );

      // patch recording
      recordingRecordAddUser(store);

      // create webphone connection
      store.dispatch(appActionCreators.webphoneConnect());

      // send other peers user info about me
      store.dispatch(appActionCreators.sendRoomAction(appActionCreators.updateParticipantInfo(participant)));
    },
    [appActionTypes.LOGOUT](store: Store, next, action) {
      const state = store.getState();
      const roomId = _get(state, 'room.state.id');
      const appActionCreators = clientEntityProvider.getAppActionCreators();

      if (isRecording(store)) {
        store.dispatch(appActionCreators.recordingSuspend());
      }

      if (roomId) {
        store.dispatch(appActionCreators.leaveRoom(roomId, action.userId));
      }

      // disconect from app socket
      store.dispatch(appActionCreators.disconnect());

      // disconnect webphone
      store.dispatch(appActionCreators.webphoneDisconnect(DisconnectReason.EXIT));

      next(action);
    },
    [appActionTypes.ROOM_LEAVE](store, next, action) {
      const state: IStoreState = store.getState();
      const appActionCreators = clientEntityProvider.getAppActionCreators();

      // disconnect webphone
      store.dispatch(appActionCreators.webphoneDisconnect(DisconnectReason.EXIT));

      if (isGuest(state)) {
        push({
          pathname: `/thanks/`,
        });
      } else {
        next(action);
        // this action mast be after webphone disconnect
        setTimeout(() => resetWebrtcServiceName(store), 1);
      }
    },
    [appActionTypes.USER_LEFT_ROOM](store, next, action) {
      store.dispatch(clientEntityProvider.getAppActionCreators().webphoneUserLeftCall(action.userId));
      next(action);
    },
    [appActionTypes.RECORDING_START_ERROR](store, next, action) {
      store.dispatch(
        clientEntityProvider.getAppActionCreators().showNotification(recordingStartErrorNotification(action.code))
      );
      next(action);
    },
    [appActionTypes.ROOM_RECORDING_STOPPED](store, next, action) {
      // update recording plugin
      const appState = store.getState();

      if (isGuest(appState)) {
        next(action);
        return;
      }

      const userId = appState.auth.user.id;
      const notification = createRecStopNotification(userId);
      const notifAction = clientEntityProvider.getAppActionCreators().showNotification(notification);
      setTimeout(() => {
        store.dispatch(notifAction);
      }, REC_STOP_NOTIFICATION_TIMEOUT);
      next(action);
    },
    [appActionTypes.ROOM_RECORDING_STARTED](store: Store<IStoreState>, next, action) {
      next(action);
      recordingRecordAddUser(store);
    },
    [appActionTypes.ROOM_SET_INFO](store, next, action) {
      next(action);
      syncWebrtcServiceName(store, action);
    },
    [appActionTypes.SET_ROOM](store, next, action) {
      syncWebrtcServiceName(store, action);
      next(action);
    },
    [appActionTypes.MODAL_DIALOG_HIDE](store, next, action) {
      const state = store.getState();
      // If closed media settings & if still existed unplugged devices
      // - dispatch event to ignore them
      const { inputAudio, inputVideo } = state.media?.unplugged || {};
      const isSettingsPanelOpened = state.app.view.modalDialog.view.Name === 'SettingsPanelModal';
      const hasAnyUnpluggedDevices = Boolean(inputAudio || inputVideo);

      if (isSettingsPanelOpened && hasAnyUnpluggedDevices) {
        store.dispatch(clientEntityProvider.getAppActionCreators().ignoreUnpluggedDevices());
      }

      next(action);
    },
    [appActionTypes.SET_UNPLUGGED_DEVICES](store, next, action, socket, storeKey, config) {
      next(action);

      const { inputAudio, inputVideo } = action;

      if (!(inputAudio || inputVideo)) {
        return;
      }

      // choose: show or not warning about unplugged devices
      const state: IStoreState = store.getState();
      // at this moment only one modal - MediaSettingsPanel
      // think about other dialogs: queue/importance of dialogs?
      const hasOpenedModalDialog = !!state.app.view.modalDialog;
      const path = state.router.location.pathname;
      const isOnHomePage = path === '/';
      const isOnTestPage = path === config.project.routes.userMediaCheck;
      const isUserSetupComplete = _get(state, 'webphone.userSetup.isComplete');
      const isOwnerUser = isOwner(state);
      const isOnUserSetup = isOwnerUser ? false : !isUserSetupComplete;
      const { Media } = clientEntityProvider.getComponentProvider();

      if (isOnHomePage || isOnTestPage || hasOpenedModalDialog || isOnUserSetup) {
        return;
      }

      store.dispatch(
        actionCreators.showModalDialog({
          view: Media.UnpluggedDeviceWarning,
          closeOnOutsideClick: false,
        })
      );
    },
    [appActionTypes.GOTO_RECORDINGS](store, next, action, socket, storeKey) {
      next(action);
      store.dispatch(
        push({
          pathname: `/recordings/${action.userId}`,
        })
      );
    },
    [appActionTypes.RECORDING_REMOVE_RECORD_SUCCESS](store, next, action) {
      const { auth } = store.getState();
      const { ownerId } = action;

      if (ownerId === auth.user.id) {
        store.dispatch(
          actionCreators.showNotification({
            content: `Recording was removed successfully`,
            autoClose: 4000,
          })
        );
      }

      next(action);
    },
    [appActionTypes.ROOM_JOIN](store, next, action) {
      next(action);
      store.dispatch(
        push({
          pathname: `/room/${action.roomId}`,
        })
      );
    },
  };
};

export const appMiddleware: IModuleReduxMiddleware<IClientConfig> = (storeKey: string) => (socket: Socket, config) => (
  store: Store<IStoreState>
) => (next: Dispatch<AnyAction>) => (action: AnyAction) => {
  if (config.logger.saveReduxActions) {
    // @todo clientEntityProvider.getLogger().client.info('redux', action);
  }

  if (middlewareEnumNotBuilded) {
    initAppMiddlewareEnum();
  }

  if (appMiddlewareEnum[action.type]) {
    return appMiddlewareEnum[action.type](store, next, action, socket, storeKey, config);
  }

  return next(action);
};
