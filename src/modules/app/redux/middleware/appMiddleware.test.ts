/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { appMiddleware } from './appMiddleware';
import { mock } from 'jest-mock-extended';
import Socket = SocketIOClient.Socket;
import { Store } from 'redux';
import { DisconnectReason } from '../../../../ioc/common/ioc-constants';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientConfig } from '../../../../ioc/common/ioc-interfaces';

beforeAll(setupIoCClientForTest());

describe('appMiddleware', () => {
  const socket = mock<Socket>();
  const store = mock<Store>();
  const config = mock<IClientConfig>();
  const next = jest.fn();

  const middleware = appMiddleware('')(socket, config)(store)(next);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('logout with recording', () => {
    store.getState.mockReturnValue({
      room: {
        recording: {
          recId: 'recordId_1',
        },
      },
      recording: {
        isRecording: true,
      },
    });

    const appActionCreators = clientEntityProvider.getAppActionCreators();

    middleware(appActionCreators.logout());

    expect(store.dispatch).toHaveBeenNthCalledWith(1, appActionCreators.recordingSuspend());
    expect(store.dispatch).toHaveBeenNthCalledWith(2, appActionCreators.disconnect());
    // tslint:disable-next-line:no-magic-numbers
    expect(store.dispatch).toHaveBeenNthCalledWith(3, appActionCreators.webphoneDisconnect(DisconnectReason.EXIT));

    expect(next).toBeCalled();
  });

  test('logout without recording', () => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    store.getState.mockReturnValue({
      room: {
        recording: {
          recId: '',
        },
      },
      recording: {
        isRecording: false,
      },
    });

    middleware(appActionCreators.logout());

    expect(store.dispatch).toHaveBeenNthCalledWith(1, appActionCreators.disconnect());
    expect(store.dispatch).toHaveBeenNthCalledWith(2, appActionCreators.webphoneDisconnect(DisconnectReason.EXIT));

    expect(next).toBeCalled();
  });

  test('end meeting with recording', () => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    store.getState.mockReturnValue({
      room: {
        recording: {
          recId: 'recordId_1',
        },
      },
      recording: {
        isRecording: true,
      },
    });

    middleware(appActionCreators.endMeeting());

    expect(store.dispatch).toHaveBeenNthCalledWith(
      1,
      appActionCreators.sendRoomAction(appActionCreators.resetPresentationState())
    );

    expect(store.dispatch).toHaveBeenNthCalledWith(2, appActionCreators.recordingStop());
  });
});
