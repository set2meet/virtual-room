/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IApp } from '../types/app';
import { Action, AnyAction, Reducer } from 'redux';
import { IClientConfig } from '../../../ioc/client/types/IClientConfig';
import { IAppActionTypes } from '../../../ioc/common/ioc-interfaces';
import createInitialState from './appState';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { WebrtcServiceType } from '../../../ioc/common/ioc-constants';

let cachedReducers: Record<string, Reducer<IApp>> = null;
const resolveReducers = (): Record<string, Reducer<IApp>> => {
  if (!cachedReducers) {
    const appActionTypes: IAppActionTypes = clientEntityProvider.getAppActionTypes();
    cachedReducers = {
      [appActionTypes.ROOM_TOGGLE_LEFT_PANEL_VISIBILITY](state: IApp) {
        return {
          ...state,
          view: {
            ...state.view,
            room: {
              ...state.view.room,
              isLeftPanelVisible: !state.view.room.isLeftPanelVisible,
            },
          },
        };
      },
      [appActionTypes.ROOM_TOGGLE_RIGHT_PANEL_VISIBILITY](state: IApp) {
        return {
          ...state,
          view: {
            ...state.view,
            room: {
              ...state.view.room,
              isRightPanelVisible: !state.view.room.isRightPanelVisible,
            },
          },
        };
      },
      ['WEBRTC_SESSION_ENDED'](state: IApp) {
        return {
          ...state,
          view: {
            ...state.view,
            room: {
              ...state.view.room,
              isSessionJustEnded: true,
            },
          },
        };
      },
      [appActionTypes.ROOM_LEAVE](state: IApp) {
        return {
          ...state,
          view: {
            ...state.view,
            room: {
              ...state.view.room,
              isSessionJustEnded: false,
            },
          },
        };
      },
      [appActionTypes.ROOM_JOIN](state: IApp) {
        return {
          ...state,
          view: {
            ...state.view,
            room: {
              ...state.view.room,
              isSessionJustEnded: false,
            },
          },
        };
      },
      [appActionTypes.SETTINGS_PANEL_CHANGE_TAB](state: IApp, action: AnyAction) {
        return {
          ...state,
          view: {
            ...state.view,
            settingsPanel: {
              ...state.view.settingsPanel,
              currentTab: action.tab,
            },
          },
        };
      },
      [appActionTypes.MODAL_DIALOG_SHOW](state: IApp, action: AnyAction) {
        return {
          ...state,
          view: {
            ...state.view,
            modalDialog: action.modal,
          },
        };
      },
      [appActionTypes.MODAL_DIALOG_HIDE](state: IApp, action: AnyAction) {
        return {
          ...state,
          view: {
            ...state.view,
            modalDialog: null,
          },
        };
      },
      [appActionTypes.WEBRTC_SERVICE_CHANGE](state: IApp, action: AnyAction) {
        return {
          ...state,
          webrtcConfig: {
            ...state.webrtcConfig,
            selectedServiceInx: action.inx,
          },
        };
      },
    };
  }
  return cachedReducers;
};

export default (config?: IClientConfig) => {
  let initState = createInitialState();

  if (config) {
    // modify initial state by incoming params
    initState = {
      ...initState,
      recordingConfig: config.recording,
    };

    const webrtcConfig = initState.webrtcConfig;
    const webrtcServices = webrtcConfig.services;

    // remove tokbox service if it isn't configured correctly (see README.md media service usage statement)
    if (!config.webrtc?.tokbox?.clientBundle) {
      const opentokIndex = webrtcServices.findIndex((service) => service.name === WebrtcServiceType.OPENTOK);

      webrtcServices.splice(opentokIndex, 1);
    }

    if (webrtcServices.length) {
      // redefine default webrtc service
      webrtcConfig.selectedServiceInx = webrtcConfig.defaultServiceInx = webrtcServices.findIndex(
        (service) => service.name === config.webrtc.default
      );
    } else {
      webrtcConfig.selectedServiceInx = webrtcConfig.defaultServiceInx = 0;
    }
  }

  // return common app reducer
  return (state: IApp = initState, action: Action) => {
    const reducers = resolveReducers();

    if (reducers[action.type]) {
      return reducers[action.type](state, action);
    }

    return state;
  };
};
