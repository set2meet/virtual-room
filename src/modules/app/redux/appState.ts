/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IApp } from '../types/app';
import { WebrtcServiceType } from '../../../ioc/common/ioc-constants';

const createInitialState = (): IApp => {
  // TODO: move to client config
  const webrtcServiceTitleMap: Record<string, string> = {
    opentok: 'TokBox',
    p2p: 'Peer-to-peer (no media server)',
  };
  const webrtcServiceNameMap = {
    opentok: WebrtcServiceType.OPENTOK,
    p2p: WebrtcServiceType.P2P,
  };

  const services = Object.keys(webrtcServiceTitleMap).map((key) => ({
    key,
    name: webrtcServiceNameMap[key],
    title: webrtcServiceTitleMap[key],
  }));

  return {
    recordingConfig: {
      enabled: false,
    },
    webrtcConfig: {
      services,
      defaultServiceInx: 0,
      selectedServiceInx: 0,
    },
    view: {
      room: {
        isLeftPanelVisible: true,
        isRightPanelVisible: true,
        isSessionJustEnded: false,
      },
      settingsPanel: {
        // @TODO: maybe we don't use it, at least it is missing inside IApp
        isVisible: false,
        currentTab: 'Audio',
      },
      modalDialog: null,
    },
  } as IApp;
};

export default createInitialState;
