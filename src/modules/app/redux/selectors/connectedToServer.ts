/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IStoreState } from '../../entities/store/types/IStoreState';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

// is app connected to server
// @param {IStoreState} state - app state
export default (state: IStoreState): boolean =>
  state?.connection?.status === clientEntityProvider.getConstants().connection.CONNECTION_STATUS.CONNECTED;
