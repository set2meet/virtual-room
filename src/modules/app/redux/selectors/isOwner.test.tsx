import { isOwner as isOwnerSelector } from './index';
import { getUserWithTokens } from '../../../../test/mocks/auth';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import { enzymeCleanup, InitModulesWrapper } from '../../../../test/utils/utils';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import React from 'react';

beforeAll(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('test isOwner selector', () => {
  const userId = 'user123';

  it('user is owner', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper withRoomModule={true} />));
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

    store.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(getUserWithTokens({ id: userId })));
    store.dispatch({ type: clientEntityProvider.getAppActionTypes().SET_ROOM_OWNER, id: userId });

    expect(isOwnerSelector(store.getState())).toEqual(true);
    done();
  });

  it('user is not owner', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper withRoomModule={true} />));
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

    store.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(getUserWithTokens({ id: userId })));
    store.dispatch({ type: clientEntityProvider.getAppActionTypes().SET_ROOM_OWNER, id: 'other123' });

    expect(isOwnerSelector(store.getState())).toEqual(false);
    done();
  });
});
