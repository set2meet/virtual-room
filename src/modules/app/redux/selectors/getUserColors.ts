/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { UserColors } from '../../types/app';
import { IStoreState } from '../../entities/store/types/IStoreState';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

let latestUserColors: any = null;
let latestUserColorsState: any = null;

export default (state: IStoreState): UserColors => {
  const userColors = state?.room?.state?.userColors;

  if (userColors !== latestUserColorsState) {
    latestUserColorsState = userColors;

    latestUserColors = {
      ...latestUserColorsState,
      default: clientEntityProvider.defaultTheme.buttonDisabledColor,
    };
  }

  return latestUserColors;
};
