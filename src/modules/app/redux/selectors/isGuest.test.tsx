import { getUserWithTokens } from '../../../../test/mocks/auth';
import isGuest from './isGuest';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { enzymeCleanup, InitModulesWrapper } from '../../../../test/utils/utils';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import React from 'react';

beforeAll(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('test isGuest selector', () => {
  const userId = 'user123';

  it('is not guest', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const userWithTokens = getUserWithTokens({ id: userId });

    store.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(userWithTokens));

    expect(isGuest(store.getState())).toEqual(false);
    done();
  });

  it('is guest', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const userWithTokens = getUserWithTokens({ guest: true });

    store.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(userWithTokens));

    expect(isGuest(store.getState())).toEqual(true);
    done();
  });
});
