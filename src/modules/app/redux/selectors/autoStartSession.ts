/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _get from 'lodash/get';
import { IStoreState } from '../../entities/store/types/IStoreState';
import isOwner from './isOwner';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

// is user owner of the room
// @param {IStoreState} state - app state
export default (state: IStoreState): boolean => {
  return (
    clientEntityProvider.getConfig().clientConfig.autoStartOnDemandSession &&
    isOwner(state) &&
    _get(state, 'room.state.id') !== _get(state, 'auth.user.roomId')
  ); // not permanent room
};
