import React from 'react';
import { getUserWithTokens } from '../../../../test/mocks/auth';
import getUser from './getUser';
import { mock } from 'jest-mock-extended';
import { IStoreState } from '../../entities/store/types/IStoreState';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import { IClientEntityProviderInternals } from '../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { enzymeCleanup, InitModulesWrapper } from '../../../../test/utils/utils';

beforeAll(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('test isUser selector', () => {
  const userId = 'user123';

  it('user exists', async (done) => {
    await loadAsyncTree(EnzymeMountTracker.mount(<InitModulesWrapper />));
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const userWithTokens = getUserWithTokens({ id: userId });

    store.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(userWithTokens));
    expect(getUser(store.getState())).toEqual(userWithTokens.user);
    done();
  });

  it('user not exists', () => {
    const state = mock<IStoreState>();

    expect(getUser(state)).toEqual(null);
  });
});
