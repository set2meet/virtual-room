/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import setupIoCClientForTest from '../../../test/utils/ioc/testIoCClient';
import createInitialState from './appState';
import { WebrtcServiceType } from '../../../ioc/common/ioc-constants';

beforeAll(setupIoCClientForTest());

describe('appState', () => {
  it('createInitialState', () => {
    expect(createInitialState()).toEqual(
      expect.objectContaining({
        webrtcConfig: {
          services: [
            {
              key: 'opentok',
              name: WebrtcServiceType.OPENTOK,
              title: 'TokBox',
            },
            {
              key: 'p2p',
              name: WebrtcServiceType.P2P,
              title: 'Peer-to-peer (no media server)',
            },
          ],
          defaultServiceInx: 0,
          selectedServiceInx: 0,
        },
      })
    );
  });
});
