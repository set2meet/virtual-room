/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export enum AppAction {
  ROOM_TOGGLE_LEFT_PANEL_VISIBILITY = 'VIEW:ROOM:TOGGLE_LEFT_PANEL_VISIBILITY',
  ROOM_TOGGLE_RIGHT_PANEL_VISIBILITY = 'VIEW:ROOM:TOGGLE_RIGHT_PANEL_VISIBILITY',
  SETTINGS_PANEL_CHANGE_TAB = 'VIEW:SETTINGS_PANEL_CHANGE_TAB',
  MODAL_DIALOG_SHOW = 'MODAL_DIALOG_SHOW',
  MODAL_DIALOG_HIDE = 'MODAL_DIALOG_HIDE',
  GOTO_RECORDINGS = 'GOTO_RECORDINGS',
  WEBRTC_SERVICE_CHANGE = 'APP:WEBRTC_SERVICE_CHANGE',
}

export type TAppActionTypes = typeof AppAction;
