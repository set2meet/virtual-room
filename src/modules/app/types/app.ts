/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppRecordingConfig } from '../../../ioc/client/types/IClientConfig';
import { IAppModalDialogDefinition } from './IAppModalDialog';
import { WebrtcServiceType } from '../../../ioc/common/ioc-constants';

export interface IAppConfigProject {
  realmPrefix: string;
  name: string;
  urls: {
    home: string;
  };
  routes: {
    userMediaCheck: string;
  };
  contacts: {
    support: string;
  };
  activeTools: string[];
  gui?: {
    disableRecordings?: boolean;
  };
}

export interface IAppSettingsPanel {
  currentTab: string;
}

export interface IAppWebRTCInfo {
  key: string;
  name: WebrtcServiceType;
  title: string;
}

export interface IAppWebRTCConfig {
  services: IAppWebRTCInfo[];
  defaultServiceInx: number; // index of IAppWebRTCInfo of default service
  selectedServiceInx: number; // index of IAppWebRTCInfo of current service
}

export interface IApp {
  recordingConfig: IAppRecordingConfig;
  webrtcConfig: IAppWebRTCConfig;
  view: {
    room: {
      isLeftPanelVisible: boolean;
      isRightPanelVisible: boolean;
      isSessionJustEnded: boolean;
    };
    settingsPanel: IAppSettingsPanel;
    modalDialog: IAppModalDialogDefinition;
  };
}

export type UserColors = Record<string, string> & {
  default: string;
};
