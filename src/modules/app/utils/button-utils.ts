/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { ITheme, IRequiredTheme } from '../../../ioc/client/types/interfaces';

interface IColorModel {
  normal: string;
  hover: string;
  pressed: string;
  disabled: string;
}

const setButtonColor = (color: string): string => `
  &:before {
    color: ${color};
  }

  border-color: ${color};
`;

const getColor = (active: boolean, theme: ITheme, state: string): string => {
  const prefix = active ? 'buttonDefaultColor' : 'buttonOffColor';
  const targetIndex = `${prefix}${state}`;

  return (theme as any)[targetIndex];
};

const getColorModel = (active: boolean, theme: ITheme): IColorModel => {
  return {
    normal: getColor(active, theme, ''),
    hover: getColor(active, theme, 'Hover'),
    pressed: getColor(active, theme, 'Active'),
    disabled: theme.buttonDisabledColor,
  };
};

export interface IButtonProps {
  theme: ITheme;
  active?: boolean;
}

export const includeColorModel = (props: IButtonProps & IRequiredTheme) => {
  const color = getColorModel(props.active, props.theme);

  return `
    ${setButtonColor(color.normal)};

    &:disabled {
      opacity: 0.5;
      cursor: default;      
    }

    &:not(:disabled) {
      &:hover {
        ${setButtonColor(color.hover)};
      }

      &:active {
        ${setButtonColor(color.pressed)};
      }
    }
  `;
};

export const includeSize = (size: string) => `
  margin: 0;
  border: 0;
  padding: 0;
  width: ${size};
  height: ${size};  
`;
