/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export default (str: string): number => {
  let chr;
  let i;
  let hash = 0;
  const len = str.length;

  if (len === 0) {
    return hash;
  }

  for (i = 0; i < len; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr; // tslint:disable-line
    hash |= 0; // tslint:disable-line
  }

  return hash;
};
