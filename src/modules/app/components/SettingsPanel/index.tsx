import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import resolveUrl from 'resolve-url';
import styled from 'styled-components';
import { styledButtonClose, styledMenuButton, styledModalContainer } from './settingsPanel.style';
import { IAppSettingsPanel } from '../../types/app';
import { IStoreState } from '../../../../ioc/common/ioc-interfaces';
import { isGuest as isGuestSelector, isOwner as isOwnerSelector } from '../../redux/selectors';
import SelectWebrtcService from '../SelectWebrtcService';
import NotificationSettings from '../NotificationSettings';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ModuleRegistryKey } from '../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { TDynamicModuleWrapperProps } from '../../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { TWebphoneModuleInitOptions } from '../../../../ioc/client/types/interfaces';
import { SkeletonProps } from 'react-loading-skeleton';
import { FallbackRegistryKey } from '../../../../ioc/client/providers/FallbackProvider/types/FallbackRegistryKey';

const ModalButtonTab = styled.button`
  ${styledMenuButton};
`;
const ButtonClose = styled.button`
  ${styledButtonClose};
`;
const ModalContent = styled.div`
  ${styledModalContainer};
`;

interface ITabInfo {
  title: string;
  icon: string;
  element: React.ComponentType;
}

interface ISettingsPanelOwnProps {
  onHide: () => void;
}

interface ISettingsPanelStateProps {
  settingsPanel: IAppSettingsPanel;
  remoteServiceUrl: string;
  showRoomTab: boolean;
  isGuest: boolean;
  authStrategy: string;
}

interface ISettingsPanelDispatchProps {
  changeSettingsPanelTab: ActionCreator<AnyAction>;
}

type ISettingsPanelModalProps = ISettingsPanelOwnProps & ISettingsPanelStateProps & ISettingsPanelDispatchProps;

export class SettingsPanelModal extends React.Component<ISettingsPanelModalProps> {
  public static Name = 'SettingsPanelModal';

  public static SIZE = {
    width: 796,
    height: 406,
  };

  private TABS: Record<string, ITabInfo>;

  constructor(props: ISettingsPanelModalProps) {
    super(props);

    const changePasswordUrl = resolveUrl(props.remoteServiceUrl, 'change-password');
    const { Media, Auth } = clientEntityProvider.getComponentProvider();
    const DarkSkeleton = clientEntityProvider.resolveFallback<SkeletonProps>(FallbackRegistryKey.DarkSkeleton);
    const ChatModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Chat, {
      actionCreators: clientEntityProvider.getAppActionCreators(),
    });

    this.TABS = {
      Audio: {
        title: 'Audio',
        icon: 'headset',
        element: () => <Media.MediaSettingsAudio />,
      },
      Video: {
        title: 'Video',
        icon: 'videoCamera',
        element: () => <Media.MediaSettingsVideo />,
      },
      ...(this.props.showRoomTab
        ? {
            Room: {
              title: 'Room',
              icon: 'construct',
              element: () => <SelectWebrtcService />,
            },
          }
        : {}),
      Notifications: {
        title: 'Notifications',
        icon: 'user',
        element: () => (
          <ChatModule fallback={<DarkSkeleton width={'100%'} height={'100px'} />}>
            <NotificationSettings />
          </ChatModule>
        ),
      },
      ...(props.isGuest || this.props.authStrategy !== 'any'
        ? {}
        : {
            // only permanent users can change password
            Password: {
              title: 'Security',
              icon: 'unlock',
              element: () => <Auth.ChangePassword statePath="auth" changePasswordUrl={changePasswordUrl} />,
            },
          }),
      PreCallTest: {
        title: 'Pre-call test',
        icon: 'speedometer',
        element: () => {
          const { WebPhone } = clientEntityProvider.getComponentProvider();
          const WebphoneModule = clientEntityProvider.resolveModuleSuspended<
            TDynamicModuleWrapperProps,
            TWebphoneModuleInitOptions
          >(ModuleRegistryKey.Webphone, { webRTCServices: clientEntityProvider.getWebRTCServices() }, null);
          const WebrtcModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Webrtc, null);
          const WebphoneSkeleton = <DarkSkeleton width={'100%'} height={'320px'} />;

          return (
            <WebphoneModule fallback={WebphoneSkeleton}>
              <WebrtcModule fallback={WebphoneSkeleton}>
                <WebPhone.PreCallTest />
              </WebrtcModule>
            </WebphoneModule>
          );
        },
      },
    };
  }

  private hide = () => {
    this.props.onHide();
  };

  private changeTabHandler = (evt: React.SyntheticEvent) => {
    const target = evt.target as HTMLButtonElement;

    this.changeTab(target.name);
  };

  private changeTab(tab: string) {
    if (this.props.settingsPanel.currentTab !== tab) {
      this.props.changeSettingsPanelTab(tab);
    }
  }

  public componentDidMount() {
    const md: IAppSettingsPanel = this.props.settingsPanel;

    if (!this.TABS[md.currentTab]) {
      this.changeTab(Object.keys(this.TABS)[0]);
    }
  }

  public render() {
    const md: IAppSettingsPanel = this.props.settingsPanel;
    const currTab = this.TABS[md.currentTab];
    const CurrentTab = currTab?.element;
    const DarkSkeleton = clientEntityProvider.resolveFallback<SkeletonProps>(FallbackRegistryKey.DarkSkeleton);
    const MediaModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Media);
    const MediaSkeleton = <DarkSkeleton width={'100%'} height={'320px'} />;

    return (
      <ModalContent>
        {<ButtonClose onClick={this.hide} />}
        <ul>
          {Object.keys(this.TABS).map((tabKey: string) => {
            const tab = this.TABS[tabKey];

            return (
              <li key={tab.title} className={tabKey === md.currentTab ? 'current' : ''}>
                <ModalButtonTab name={tabKey} icon={tab.icon} onClick={this.changeTabHandler}>
                  <span>{tab.title}</span>
                </ModalButtonTab>
              </li>
            );
          })}
        </ul>
        <div>
          <MediaModule fallback={MediaSkeleton}>
            <CurrentTab />
          </MediaModule>
        </div>
      </ModalContent>
    );
  }
}

export default connect<ISettingsPanelStateProps, ISettingsPanelDispatchProps>(
  (state: IStoreState) => {
    const { service } = clientEntityProvider.getConfig();
    const isGuest = isGuestSelector(state);
    const isOwner = isOwnerSelector(state);
    const userInTheRoom = !!state.room?.state?.id;
    const sessionIsActive = !!state.room?.webrtcService?.sessionId;

    return {
      authStrategy: state.auth.strategy,
      settingsPanel: state.app.view.settingsPanel,
      remoteServiceUrl: resolveUrl(service.url, service.path),
      isGuest,
      showRoomTab: userInTheRoom && (isOwner ? true : sessionIsActive),
    };
  },
  (dispatch) => {
    const actionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        changeSettingsPanelTab: actionCreators.changeSettingsPanelTab,
      },
      dispatch
    );
  }
)(SettingsPanelModal);
