import * as React from 'react';
import styled from 'styled-components';
import { styledCheckbox, ICheckboxUIProps } from './Checkbox.style';

export interface ICheckboxComponentProps {
  label: string;
  value: boolean;
  disabled?: boolean;
  className?: string;
  onChange: (value: boolean) => void;
}

const Checkbox = styled.div<ICheckboxUIProps>`
  ${styledCheckbox};
`;

const CheckboxComponent: React.FunctionComponent<ICheckboxComponentProps> = (props) => {
  const onChange = () => {
    if (!props.disabled) {
      props.onChange(!props.value);
    }
  };
  const disabled = props.disabled || false;

  return (
    <Checkbox checked={props.value} className={props.className} disabled={disabled}>
      <label onClick={onChange}>{props.label}</label>
    </Checkbox>
  );
};

export default CheckboxComponent;
