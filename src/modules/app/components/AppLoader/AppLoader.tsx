import { hot } from 'react-hot-loader';
import { TDynamicModuleWrapperProps } from '../../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleRegistryKey } from '../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { Provider } from 'react-redux';
import React, { Component, Context } from 'react';
import CompositionRoot from '../../../../ioc/client/components/CompositionRoot/CompositionRoot';
import {
  ClientEntityProviderContext,
  TClientEntityProviderContext,
} from '../../../../ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { IClientEntityProviderInternals } from '../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { TRouterModuleInitOptions } from '../../../../ioc/client/types/interfaces';
import { TAppModuleInitOptions } from '../../../../ioc/client/types/interfaces';
import { TStore } from '../../entities/store/types/TStore';
import AppSkeleton from './AppSkeleton';

class AppTree extends Component {
  public static contextType: Context<TClientEntityProviderContext> = ClientEntityProviderContext;

  public render() {
    const clientEntityProvider: IClientEntityProviderInternals = this.context.clientEntityProvider;

    if (!clientEntityProvider) {
      return AppSkeleton;
    }
    if (!clientEntityProvider.isInitialized) {
      throw new Error('Application tree was rendered outside CompositionRoot!');
    }

    const store: TStore = clientEntityProvider.getStore();
    const RouterModule = clientEntityProvider.resolveModuleSuspended<
      TDynamicModuleWrapperProps,
      TRouterModuleInitOptions
    >(ModuleRegistryKey.RouterModule, { history: clientEntityProvider.getHistory() }, AppSkeleton);
    const AppModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps, TAppModuleInitOptions>(
      ModuleRegistryKey.AppModule,
      {
        config: clientEntityProvider.getConfig(),
      },
      AppSkeleton
    );
    const ConnectionModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps>(
      ModuleRegistryKey.Connection,
      AppSkeleton
    );
    const AuthModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps>(
      ModuleRegistryKey.Auth,
      AppSkeleton
    );

    return (
      <Provider store={store}>
        <RouterModule>
          <ConnectionModule>
            <AuthModule>
              <AppModule />
            </AuthModule>
          </ConnectionModule>
        </RouterModule>
      </Provider>
    );
  }
}

const AppLoader = (): JSX.Element => (
  <CompositionRoot fallback={AppSkeleton}>
    <AppTree />
  </CompositionRoot>
);

export default hot(module)(AppLoader);
