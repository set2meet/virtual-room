import AppSkeleton from './AppSkeleton';

export default {
  title: 'app/AppSkeleton',
  component: AppSkeleton,
  parameters: {
    themeDecorator: {
      disable: true,
    },
  },
};

export const Default = () => AppSkeleton;
