import React from 'react';
import styled, { css } from 'styled-components';
import HeaderSkeleton from '../Header/HeaderSkeleton';

const Page = styled.div`
  ${css`
    height: 100%;
    background-color: rgb(31, 31, 31);
  `}
`;

const SkeletonsContainer = styled.div`
  ${css`
    height: 100%;
  `}
`;

const AppSkeleton = (
  <Page>
    <SkeletonsContainer>
      <HeaderSkeleton />
    </SkeletonsContainer>
  </Page>
);

export default AppSkeleton;
