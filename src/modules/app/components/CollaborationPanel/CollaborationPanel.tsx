import React from 'react';
import styled from 'styled-components';
import PRESENT_TOOLS, { ITools } from '../PRESENT_TOOLS';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import isOwner from '../../redux/selectors/isOwner';
import { IStoreState } from '../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const StyledCollaborationPanel = styled.div`
  text-align: center;
  padding-top: 10px;

  & > div {
    margin-bottom: 15px;
  }
`;

interface ICollaborationPanelDispatchProps {
  sendRoomAction: ActionCreator<AnyAction>;
}

interface ICollaborationPanelStateProps {
  isRoomOwner: boolean;
  currentTool: string;
  activeTools: string[];
}

export class CollaborationPanel extends React.Component<
  ICollaborationPanelDispatchProps & ICollaborationPanelStateProps
> {
  private setActiveTool = (tool: string) => {
    const appActions = clientEntityProvider.getActionCreators();

    if (this.props.currentTool === tool) {
      this.props.sendRoomAction(appActions.setCurrentTool(PRESENT_TOOLS.webphone.name));
    } else {
      this.props.sendRoomAction(appActions.setCurrentTool(tool));
    }
  };

  private generateToolTooltip = (tool: ITools[keyof ITools]) => {
    return `${tool.name} is currently ${this.props.currentTool === tool.name ? 'active ' : 'inactive'}`;
  };

  private renderTools = () => {
    return this.props.activeTools.map((key) => {
      const tool = PRESENT_TOOLS[key];

      if (!tool) {
        return null;
      }

      const currentTool = this.props.currentTool || PRESENT_TOOLS.webphone.name;
      const tooltip = {
        'data-tooltip': this.generateToolTooltip(tool),
        'data-tooltip-pos': 'right',
      };
      const { SimpleToggleButton } = clientEntityProvider.getComponentProvider().UI;

      return (
        <SimpleToggleButton
          disabled={!this.props.isRoomOwner}
          icon={tool.icon}
          key={tool.name}
          tooltip={tooltip}
          active={currentTool === tool.name}
          onClick={this.setActiveTool.bind(this, tool.name)}
        />
      );
    });
  };

  public render() {
    return <StyledCollaborationPanel>{this.renderTools()}</StyledCollaborationPanel>;
  }
}

export default connect<ICollaborationPanelStateProps, ICollaborationPanelDispatchProps>(
  (state: IStoreState) => {
    const { activeTools } = clientEntityProvider.getConfig().project;

    return {
      activeTools,
      isRoomOwner: isOwner(state),
      currentTool: state.room.state.currentTool || PRESENT_TOOLS.webphone.name,
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        sendRoomAction: appActions.sendRoomAction,
      },
      dispatch
    );
  }
)(CollaborationPanel);
