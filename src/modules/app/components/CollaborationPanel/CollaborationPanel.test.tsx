import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { ActionCreator } from 'redux';
import PRESENT_TOOLS from '../PRESENT_TOOLS';
import { CollaborationPanel } from './CollaborationPanel';
import React from 'react';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import { enzymeCleanup } from '../../../../test/utils/utils';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

beforeAll(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('<CollaborationPanel/>', () => {
  it('should change tool to default on active tool click', () => {
    const sendRoomAction = jest.fn();
    const mounted = EnzymeMountTracker.mount(
      <CollaborationPanel
        sendRoomAction={sendRoomAction as ActionCreator<any>}
        isRoomOwner={true}
        activeTools={Object.keys(PRESENT_TOOLS)}
        currentTool={PRESENT_TOOLS.codeEditor.name}
      />
    );

    // tslint:disable-next-line:no-magic-numbers
    expect(mounted.find('SimpleToggleButton').length).toBe(6);
    const activeTool = mounted.find({ active: true }).first();

    expect(activeTool.prop('icon')).toEqual(PRESENT_TOOLS.codeEditor.icon);

    activeTool.find('button').simulate('click');
    expect(sendRoomAction).toHaveBeenNthCalledWith(
      1,
      clientEntityProvider.getActionCreators().setCurrentTool(PRESENT_TOOLS.webphone.name)
    );
  });
  it('should change active tool', () => {
    const sendRoomAction = jest.fn();
    const mounted = EnzymeMountTracker.mount(
      <CollaborationPanel
        sendRoomAction={sendRoomAction as ActionCreator<any>}
        isRoomOwner={true}
        activeTools={Object.keys(PRESENT_TOOLS)}
        currentTool={PRESENT_TOOLS.codeEditor.name}
      />
    );

    const activeTool = mounted.find({ active: false }).first();

    expect(activeTool.prop('icon')).toEqual(PRESENT_TOOLS.whiteboard.icon);

    activeTool.find('button').simulate('click');
    expect(sendRoomAction).toHaveBeenNthCalledWith(
      1,
      clientEntityProvider.getActionCreators().setCurrentTool(PRESENT_TOOLS.whiteboard.name)
    );
  });
});
