import { CollaborationPanel } from './CollaborationPanel';
import React from 'react';
import PRESENT_TOOLS from '../PRESENT_TOOLS';
import { action } from '@storybook/addon-actions';
import { ActionCreator } from 'redux';
import { boolean } from '@storybook/addon-knobs';

export default {
  title: 'app/CollaborationPanel',
  component: CollaborationPanel,
};

export const Default = () => {
  return (
    <CollaborationPanel
      sendRoomAction={action('sendRoomAction') as ActionCreator<any>}
      isRoomOwner={boolean('isRoomOwner', false)}
      activeTools={[...Object.keys(PRESENT_TOOLS), 'testTool']}
      currentTool={PRESENT_TOOLS.webphone.name}
    />
  );
};

export const Owner = () => {
  return (
    <CollaborationPanel
      sendRoomAction={action('sendRoomAction') as ActionCreator<any>}
      isRoomOwner={boolean('isRoomOwner', true)}
      activeTools={Object.keys(PRESENT_TOOLS)}
      currentTool={PRESENT_TOOLS.codeEditor.name}
    />
  );
};
