import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import Skeleton from 'react-loading-skeleton';
import React, { ComponentType } from 'react';
import { withIoCFactory } from '../../../../test/utils/utils';
import { vrCommon } from '../../../common';
import IRoundButtonProps = vrCommon.client.UI.IRoundButtonProps;
import { action } from '@storybook/addon-actions';
import { boolean, number, text } from '@storybook/addon-knobs';

const buttonFactory = () =>
  clientEntityProvider.resolveComponentSuspended<IRoundButtonProps>(
    ComponentRegistryKey.RoundButton,
    // tslint:disable-next-line:no-magic-numbers
    <Skeleton width={100} height={34} />
  );

const SuspendedRoundButton: ComponentType<IRoundButtonProps> = withIoCFactory<IRoundButtonProps>(buttonFactory);

export default {
  title: 'common/RoundButton',
  component: SuspendedRoundButton,
  parameters: {
    themeDecorator: {
      centered: true,
    },
  },
};

export const Default = () => <SuspendedRoundButton icon="pencil" indicator={100} />;

export const Variants = () => {
  const icon = 'pencil';

  return (
    <>
      <SuspendedRoundButton
        icon={icon}
        indicator={number('indicator', 100)}
        onClick={action('onClick1')}
        active={boolean('active', true)}
        disabled={boolean('disabled', false)}
      />
      <SuspendedRoundButton indicator={1} icon={icon} onClick={action('onClick2')} active={true} disabled={true} />
      <SuspendedRoundButton icon={icon} onClick={action('onClick3')} active={false} disabled={false} />
      <SuspendedRoundButton
        // See @todo section in RoundButton.tsx
        tooltip={text('tooltip', 'tooltip') as any}
        icon={icon}
        onClick={action('onClick4')}
        active={false}
        disabled={true}
      />
    </>
  );
};
