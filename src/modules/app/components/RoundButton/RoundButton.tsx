import React from 'react';
import _noop from 'lodash/noop';
import { vrCommon } from '../../../common';
import styled from 'styled-components';
import { styledContainer } from './RoundButton.style';
import IRoundButtonProps = vrCommon.client.UI.IRoundButtonProps;
import SimpleToggleButton from '../SimpleToggleButton';
import Indicator from '../Indicator';

const ButtonContainer = styled.div`
  ${styledContainer};
`;

const StyledRoundButton = styled(SimpleToggleButton)`
  border-radius: 50%;
  border-width: 2px;
`;

class RoundButton extends React.Component<IRoundButtonProps> {
  public static defaultProps = {
    active: false,
    disabled: false,
    onClick: _noop,
    indicatorValue: 0,
    indicatorVisibility: false,
  };

  private _renderIndicator = () => {
    return this.props.indicator ? <Indicator value={this.props.indicator} /> : null;
  };

  public render() {
    /**
     * @todo make tooltip type compatible with ISimpleToggleButton
     */
    const props = this.props.tooltip
      ? {
          'data-tooltip': this.props.tooltip,
          'data-tooltip-pos': 'up',
        }
      : {};

    return (
      <ButtonContainer {...props}>
        <StyledRoundButton
          fontSize="28px"
          buttonSize="60px"
          icon={this.props.icon}
          active={this.props.active}
          disabled={this.props.disabled}
          onClick={this.props.onClick}
        />
        {this._renderIndicator()}
      </ButtonContainer>
    );
  }
}

export default RoundButton;
