import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { IStoreState } from '../../../../ioc/common/ioc-interfaces';
import { Tab, Tabs } from 'react-bootstrap';
import PaticipantControls from './ParticipantControls';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { isOwner } from '../../redux/selectors';
import { styledPanelContainer } from './Sidebar.style';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { IChatPanelProps, IIndicatorProps } from '../../../../ioc/client/types/interfaces';
import Skeleton from 'react-loading-skeleton';

const StyledSidebarPanel = styled.div`
  ${styledPanelContainer};
`;

const StyledTitleContainer = styled.span`
  position: relative;
`;

interface ISidebarPanelStateProps {
  isRoomOwner: boolean;
  numberOfParticipants: number;
  currentTool: string;
  numberOfUnreadMessages: number;
}

interface ISidebarPanelDispatchProps {
  setAllRead: ActionCreator<AnyAction>;
}

interface ISidebarPanelState {
  currentTab: number;
}

class SidebarPanel extends React.Component<ISidebarPanelStateProps & ISidebarPanelDispatchProps> {
  public state: ISidebarPanelState = {
    currentTab: 1,
  };

  private renderChatTabTitle = () => {
    const Indicator = clientEntityProvider.resolveComponentSuspended<IIndicatorProps>(
      ComponentRegistryKey.Indicator,
      <Skeleton width={'10px'} height={'10px'} />
    );

    return (
      <StyledTitleContainer>
        Chat
        {this.props.numberOfUnreadMessages > 0 && <Indicator value={this.props.numberOfUnreadMessages} />}
      </StyledTitleContainer>
    );
  };

  private changeTab = (val: any) => {
    this.setState({ currentTab: val });
  };

  public componentDidUpdate() {
    if (this.state.currentTab === 2) {
      this.props.setAllRead();
    }
  }

  public render() {
    const { isRoomOwner } = this.props;
    const Participants = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.WebPhone_Participants);
    const Panel = clientEntityProvider.resolveComponentSuspended<IChatPanelProps>(ComponentRegistryKey.Chat_PANEL);

    return (
      <StyledSidebarPanel>
        <Tabs activeKey={this.state.currentTab} onSelect={this.changeTab} id="s2m-right-sidebar">
          <Tab eventKey={1} title={`Participants (${this.props.numberOfParticipants})`}>
            {isRoomOwner && <PaticipantControls />}
            {isRoomOwner && <hr />}
            <Participants />
          </Tab>
          <Tab eventKey={2} title={this.renderChatTabTitle()}>
            <Panel statePath="room.chat" />
          </Tab>
        </Tabs>
      </StyledSidebarPanel>
    );
  }
}

export default connect<ISidebarPanelStateProps, ISidebarPanelDispatchProps>(
  (state: IStoreState) => {
    const selectors = clientEntityProvider.getSelectors();
    const { numberOfUnreadMessages } = selectors.chatSelectors;

    return {
      isRoomOwner: isOwner(state),
      numberOfParticipants: state.room.state.participants.length,
      currentTool: state.room.state.currentTool,
      numberOfUnreadMessages: numberOfUnreadMessages(state.room.chat),
    };
  },
  (dispatch: any) => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      setAllRead: bindActionCreators(appActionCreators.setAllRead, dispatch),
    };
  }
)(SidebarPanel);
