import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import isOwner from '../../../redux/selectors/isOwner';
import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import copy from 'copy-to-clipboard';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const StyledParticipantControls = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 5px;
`;

interface IParticipantControlsDispatchProps {
  showNotification: ActionCreator<AnyAction>;
}

interface IParticipantControlsStateProps {
  isRoomOwner: boolean;
}

class ParticipantControls extends React.Component<IParticipantControlsDispatchProps & IParticipantControlsStateProps> {
  private _copyRoomLink = () => {
    copy(window.location.href);
    this.props.showNotification({ content: 'Link copied!' });
  };

  public render() {
    const { SimpleToggleButton } = clientEntityProvider.getComponentProvider().UI;

    return (
      <StyledParticipantControls>
        <div data-tooltip="Copy Guest link" data-tooltip-pos="down">
          <SimpleToggleButton disabled={!this.props.isRoomOwner} icon="addParticipant" onClick={this._copyRoomLink} />
        </div>
        <div data-tooltip="Coming soon" data-tooltip-pos="down">
          <SimpleToggleButton disabled={true} icon="microphoneMuted" />
        </div>
        <div data-tooltip="Coming soon" data-tooltip-pos="down">
          <SimpleToggleButton disabled={true} icon="handMuted" />
        </div>
      </StyledParticipantControls>
    );
  }
}

export default connect<IParticipantControlsStateProps, IParticipantControlsDispatchProps>(
  (state: IStoreState) => {
    return {
      isRoomOwner: isOwner(state),
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        showNotification: appActions.showNotification,
      },
      dispatch
    );
  }
)(ParticipantControls);
