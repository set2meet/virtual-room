/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';
import { css } from 'styled-components';

const TABS_HEIGHT = '40px';

export const styledPanelContainer = ({ theme }: IRequiredTheme) => css`
  height: 100%;

  #s2m-right-sidebar {
    height: 100%;
  }

  .nav-tabs {
    border-bottom: 0;
    margin-bottom: 1px;
    display: flex;
    height: ${TABS_HEIGHT};

    li {
      width: 100%;
      text-align: center;

      a:focus {
        outline: 0;
      }

      a {
        background-color: ${theme.backgroundColorThird};
        color: ${theme.buttonOffColor};
        border-radius: 0;
        border: 0;
      }

      &.active a,
      &.hover a {
        border: 0;
        color: ${theme.fontColor};
        background-color: inherit;
      }
    }
  }

  .tab-content {
    height: calc(100% - ${TABS_HEIGHT});

    > div {
      height: 100%;
    }
  }
`;
