/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { css, keyframes } from 'styled-components';
import { ISpinnerProps } from './Spinner.types';

const keyframesRotating = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

/**
 * Margin relative to @size
 * @param theme
 * @param size
 */
export const styledSpinnerContainer = ({ theme, size = 'Lg' }: ISpinnerProps) => {
  let _size;

  if (size === 'Lg') {
    _size = '52px';
  } // how bout rem?
  if (size === 'Md') {
    _size = '40px';
  }
  if (size === 'Sm') {
    _size = '30px';
  }
  if (size === 'Fx') {
    _size = '100%';
  }

  return css`
    width: ${_size};
    height: ${_size};
    min-width: ${_size};
    position: relative;
    display: inline-block;
    color: red;

    & > div {
      box-sizing: border-box;
      display: block;
      position: absolute;
      width: 76%;
      height: 76%;
      margin: 12%;
      border: 4px solid;
      border-radius: 50%;
      animation: ${keyframesRotating} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
      border-color: ${theme.primaryColor} ${theme.primaryColor} ${theme.primaryColor} transparent;
    }
  `;
};
