import * as React from 'react';
import styled from 'styled-components';
import { styledSpinnerContainer } from './Spinner.style';
import { ISpinnerProps } from './Spinner.types';

const SpinnerContainer = styled.div<ISpinnerProps>`
  ${styledSpinnerContainer};
`;

export default class Spinner extends React.Component<ISpinnerProps> {
  public render() {
    return (
      <SpinnerContainer className="spinner" size={this.props.size}>
        <div />
      </SpinnerContainer>
    );
  }
}
