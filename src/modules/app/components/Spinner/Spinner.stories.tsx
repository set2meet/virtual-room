import React from 'react';
import { withIoCFactory } from '../../../../test/utils/utils';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { ISpinnerProps } from './Spinner.types';

const SuspendedSpinner = withIoCFactory<ISpinnerProps>(() =>
  clientEntityProvider.getComponentProvider().resolveComponentSuspended(ComponentRegistryKey.Spinner)
);

export default {
  title: 'common/Spinner',
  component: SuspendedSpinner,
};

export const Default = () => <SuspendedSpinner />;

export const AllSpinners = () => (
  <>
    <SuspendedSpinner size="Lg" />
    <SuspendedSpinner size="Md" />
    <SuspendedSpinner size="Sm" />
    <SuspendedSpinner size="Fx" />
  </>
);
