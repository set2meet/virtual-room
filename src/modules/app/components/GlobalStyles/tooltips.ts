/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/* tslint:disable */
import { ITheme } from '../../entities/themes/types/ITheme';

/* in styled-components long svg utf-8 not works, see https://github.com/styled-components/styled-components/issues/1957
 *  so transcode it to base64
 * */
const svgTop = (color: string) =>
  `<svg xmlns="http://www.w3.org/2000/svg" width="36px" height="12px"><path fill="${color}" transform="rotate(0)" d="M2.658,0.000 C-13.615,0.000 50.938,0.000 34.662,0.000 C28.662,0.000 23.035,12.002 18.660,12.002 C14.285,12.002 8.594,0.000 2.658,0.000 Z"/></svg>`;
const svgDown = (color: string) =>
  `<svg xmlns="http://www.w3.org/2000/svg" width="36px" height="12px"><path fill="${color}" transform="rotate(180 18,6)" d="M2.658,0.000 C-13.615,0.000 50.938,0.000 34.662,0.000 C28.662,0.000 23.035,12.002 18.660,12.002 C14.285,12.002 8.594,0.000 2.658,0.000 Z"/></svg>`;
const svgLeft = (color: string) =>
  `<svg xmlns="http://www.w3.org/2000/svg" width="12px" height="36px"><path fill="${color}" transform="rotate(-90 18,18)" d="M2.658,0.000 C-13.615,0.000 50.938,0.000 34.662,0.000 C28.662,0.000 23.035,12.002 18.660,12.002 C14.285,12.002 8.594,0.000 2.658,0.000 Z"/></svg>`;
const svgRight = (color: string) =>
  `<svg xmlns="http://www.w3.org/2000/svg" width="12px" height="36px"><path fill="${color}" transform="rotate(90 6,6)" d="M2.658,0.000 C-13.615,0.000 50.938,0.000 34.662,0.000 C28.662,0.000 23.035,12.002 18.660,12.002 C14.285,12.002 8.594,0.000 2.658,0.000 Z"/></svg>`;

export const injectTooltipsToGlobalStyle = (theme: ITheme) => {
  const encodedArrowTop = `no-repeat url('data:image/svg+xml;base64,${btoa(svgTop(theme.buttonOffColor))}')`;
  const encodedArrowDown = `no-repeat url('data:image/svg+xml;base64,${btoa(svgDown(theme.buttonOffColor))}')`;
  const encodedArrowLeft = `no-repeat url('data:image/svg+xml;base64,${btoa(svgLeft(theme.buttonOffColor))}')`;
  const encodedArrowRight = `no-repeat url('data:image/svg+xml;base64,${btoa(svgRight(theme.buttonOffColor))}')`;

  return `
  button[data-tooltip] {
    overflow: visible;
  }
  [data-tooltip] {
    position: relative;
    cursor: pointer;
  }

  [data-tooltip]:after {
    filter: alpha(opacity=0);
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    -moz-opacity: 0;
    -khtml-opacity: 0;
    opacity: 0;
    pointer-events: none;  
    font-family: sans-serif !important;
    font-weight: normal !important;
    font-style: normal !important;
    text-shadow: none !important;
    font-size: 12px !important;
    background: ${theme.buttonOffColor};
    border-radius: 4px;
    color: ${theme.backgroundColor};
    content: attr(data-tooltip);
    padding: .5em 1em;
    position: absolute;
    white-space: nowrap;
    z-index: 10;
    line-height: 18px;
  }
  
  [data-tooltip]:hover:after {
    -webkit-transition: all 0.18s ease-out 0.18s;
    -moz-transition: all 0.18s ease-out 0.18s;
    -ms-transition: all 0.18s ease-out 0.18s;
    -o-transition: all 0.18s ease-out 0.18s;
    transition: all 0.18s ease-out 0.18s;
  }

  [data-tooltip]:before {
    background: ${encodedArrowTop};
    background-size: 100% auto;
    width: 18px;
    height: 6px;
    filter: alpha(opacity=0);
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    -moz-opacity: 0;
    -khtml-opacity: 0;
    opacity: 0;
    pointer-events: none;   
    content: '';
    position: absolute;
    z-index: 10;
  }
  
  [data-tooltip]:hover:before {
    -webkit-transition: all 0.18s ease-out 0.18s;
    -moz-transition: all 0.18s ease-out 0.18s;
    -ms-transition: all 0.18s ease-out 0.18s;
    -o-transition: all 0.18s ease-out 0.18s;
    transition: all 0.18s ease-out 0.18s;
  }

  [data-tooltip]:hover:before, [data-tooltip]:hover:after, [data-tooltip][data-tooltip-visible]:before, [data-tooltip][data-tooltip-visible]:after {
    filter: alpha(opacity=100);
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    -moz-opacity: 1;
    -khtml-opacity: 1;
    opacity: 1;
    pointer-events: auto;
  }

  [data-tooltip].font-awesome:after {
    font-family: FontAwesome;
  }
  [data-tooltip][data-tooltip-break]:after {
    white-space: pre;
  }
  [data-tooltip][data-tooltip-blunt]:before, [data-tooltip][data-tooltip-blunt]:after {
    -webkit-transition: none;
    -moz-transition: none;
    -ms-transition: none;
    -o-transition: none;
    transition: none; }
  [data-tooltip][data-tooltip-pos="up"]:after {
    bottom: 100%;
    left: 50%;
    margin-bottom: 11px;
    -webkit-transform: translate(-50%, 10px);
    -moz-transform: translate(-50%, 10px);
    -ms-transform: translate(-50%, 10px);
    transform: translate(-50%, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up"]:before {
    bottom: 100%;
    left: 50%;
    margin-bottom: 5px;
    -webkit-transform: translate(-50%, 10px);
    -moz-transform: translate(-50%, 10px);
    -ms-transform: translate(-50%, 10px);
    transform: translate(-50%, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up"]:hover:after, [data-tooltip][data-tooltip-pos="up"][data-tooltip-visible]:after {
    -webkit-transform: translate(-50%, 0);
    -moz-transform: translate(-50%, 0);
    -ms-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
  }
  [data-tooltip][data-tooltip-pos="up"]:hover:before, [data-tooltip][data-tooltip-pos="up"][data-tooltip-visible]:before {
    -webkit-transform: translate(-50%, 0);
    -moz-transform: translate(-50%, 0);
    -ms-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
  }
  [data-tooltip][data-tooltip-pos="up-left"]:after {
    bottom: 100%;
    left: 0;
    margin-bottom: 11px;
    -webkit-transform: translate(0, 10px);
    -moz-transform: translate(0, 10px);
    -ms-transform: translate(0, 10px);
    transform: translate(0, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up-left"]:before {
    bottom: 100%;
    left: 5px;
    margin-bottom: 5px;
    -webkit-transform: translate(0, 10px);
    -moz-transform: translate(0, 10px);
    -ms-transform: translate(0, 10px);
    transform: translate(0, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up-left"]:hover:after, [data-tooltip][data-tooltip-pos="up-left"][data-tooltip-visible]:after {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos="up-left"]:hover:before, [data-tooltip][data-tooltip-pos="up-left"][data-tooltip-visible]:before {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos="up-right"]:after {
    bottom: 100%;
    right: 0;
    margin-bottom: 11px;
    -webkit-transform: translate(0, 10px);
    -moz-transform: translate(0, 10px);
    -ms-transform: translate(0, 10px);
    transform: translate(0, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up-right"]:before {
    bottom: 100%;
    right: 5px;
    margin-bottom: 5px;
    -webkit-transform: translate(0, 10px);
    -moz-transform: translate(0, 10px);
    -ms-transform: translate(0, 10px);
    transform: translate(0, 10px);
    -webkit-transform-origin: top;
    -moz-transform-origin: top;
    -ms-transform-origin: top;
    transform-origin: top;
  }
  [data-tooltip][data-tooltip-pos="up-right"]:hover:after, [data-tooltip][data-tooltip-pos="up-right"][data-tooltip-visible]:after {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos="up-right"]:hover:before, [data-tooltip][data-tooltip-pos="up-right"][data-tooltip-visible]:before {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos='down']:after {
    left: 50%;
    margin-top: 11px;
    top: 100%;
    -webkit-transform: translate(-50%, -10px);
    -moz-transform: translate(-50%, -10px);
    -ms-transform: translate(-50%, -10px);
    transform: translate(-50%, -10px);
  }
  [data-tooltip][data-tooltip-pos='down']:before {
    background: ${encodedArrowDown};
    background-size: 100% auto;
    width: 18px;
    height: 6px;
    left: 50%;
    margin-top: 5px;
    top: 100%;
    -webkit-transform: translate(-50%, -10px);
    -moz-transform: translate(-50%, -10px);
    -ms-transform: translate(-50%, -10px);
    transform: translate(-50%, -10px);
  }
  [data-tooltip][data-tooltip-pos='down']:hover:after, [data-tooltip][data-tooltip-pos='down'][data-tooltip-visible]:after {
    -webkit-transform: translate(-50%, 0);
    -moz-transform: translate(-50%, 0);
    -ms-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
  }
  [data-tooltip][data-tooltip-pos='down']:hover:before, [data-tooltip][data-tooltip-pos='down'][data-tooltip-visible]:before {
    -webkit-transform: translate(-50%, 0);
    -moz-transform: translate(-50%, 0);
    -ms-transform: translate(-50%, 0);
    transform: translate(-50%, 0);
  }
  [data-tooltip][data-tooltip-pos='down-left']:after {
    left: 0;
    margin-top: 11px;
    top: 100%;
    -webkit-transform: translate(0, -10px);
    -moz-transform: translate(0, -10px);
    -ms-transform: translate(0, -10px);
    transform: translate(0, -10px);
  }
  [data-tooltip][data-tooltip-pos='down-left']:before {
    background: ${encodedArrowDown};
    background-size: 100% auto;
    width: 18px;
    height: 6px;
    left: 5px;
    margin-top: 5px;
    top: 100%;
    -webkit-transform: translate(0, -10px);
    -moz-transform: translate(0, -10px);
    -ms-transform: translate(0, -10px);
    transform: translate(0, -10px);
  }
  [data-tooltip][data-tooltip-pos='down-left']:hover:after, [data-tooltip][data-tooltip-pos='down-left'][data-tooltip-visible]:after {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos='down-left']:hover:before, [data-tooltip][data-tooltip-pos='down-left'][data-tooltip-visible]:before {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos='down-right']:after {
    right: 0;
    margin-top: 11px;
    top: 100%;
    -webkit-transform: translate(0, -10px);
    -moz-transform: translate(0, -10px);
    -ms-transform: translate(0, -10px);
    transform: translate(0, -10px);
  }
  [data-tooltip][data-tooltip-pos='down-right']:before {
    background: ${encodedArrowDown};
    background-size: 100% auto;
    width: 18px;
    height: 6px;
    right: 5px;
    margin-top: 5px;
    top: 100%;
    -webkit-transform: translate(0, -10px);
    -moz-transform: translate(0, -10px);
    -ms-transform: translate(0, -10px);
    transform: translate(0, -10px);
  }
  [data-tooltip][data-tooltip-pos='down-right']:hover:after, [data-tooltip][data-tooltip-pos='down-right'][data-tooltip-visible]:after {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos='down-right']:hover:before, [data-tooltip][data-tooltip-pos='down-right'][data-tooltip-visible]:before {
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  [data-tooltip][data-tooltip-pos='left']:after {
    margin-right: 11px;
    right: 100%;
    top: 50%;
    -webkit-transform: translate(10px, -50%);
    -moz-transform: translate(10px, -50%);
    -ms-transform: translate(10px, -50%);
    transform: translate(10px, -50%);
  }
  [data-tooltip][data-tooltip-pos='left']:before {
    background: ${encodedArrowLeft};
    background-size: 100% auto;
    width: 6px;
    height: 18px;
    margin-right: 5px;
    right: 100%;
    top: 50%;
    -webkit-transform: translate(10px, -50%);
    -moz-transform: translate(10px, -50%);
    -ms-transform: translate(10px, -50%);
    transform: translate(10px, -50%);
  }
  [data-tooltip][data-tooltip-pos='left']:hover:after, [data-tooltip][data-tooltip-pos='left'][data-tooltip-visible]:after {
    -webkit-transform: translate(0, -50%);
    -moz-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
  }
  [data-tooltip][data-tooltip-pos='left']:hover:before, [data-tooltip][data-tooltip-pos='left'][data-tooltip-visible]:before {
    -webkit-transform: translate(0, -50%);
    -moz-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
  }
  [data-tooltip][data-tooltip-pos='right']:after {
    left: 100%;
    margin-left: 11px;
    top: 50%;
    -webkit-transform: translate(-10px, -50%);
    -moz-transform: translate(-10px, -50%);
    -ms-transform: translate(-10px, -50%);
    transform: translate(-10px, -50%);
  }
  [data-tooltip][data-tooltip-pos='right']:before {
    background: ${encodedArrowRight};
    background-size: 100% auto;
    width: 6px;
    height: 18px;
    left: 100%;
    margin-left: 5px;
    top: 50%;
    -webkit-transform: translate(-10px, -50%);
    -moz-transform: translate(-10px, -50%);
    -ms-transform: translate(-10px, -50%);
    transform: translate(-10px, -50%);
  }
  [data-tooltip][data-tooltip-pos='right']:hover:after, [data-tooltip][data-tooltip-pos='right'][data-tooltip-visible]:after {
    -webkit-transform: translate(0, -50%);
    -moz-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
  }
  [data-tooltip][data-tooltip-pos='right']:hover:before, [data-tooltip][data-tooltip-pos='right'][data-tooltip-visible]:before {
    -webkit-transform: translate(0, -50%);
    -moz-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
  }
  [data-tooltip][data-tooltip-length='small']:after {
    white-space: normal;
    width: 80px;
  }
  [data-tooltip][data-tooltip-length='medium']:after {
    white-space: normal;
    width: 150px;
  }
  [data-tooltip][data-tooltip-length='large']:after {
    white-space: normal;
    width: 260px;
  }
  [data-tooltip][data-tooltip-length='xlarge']:after {
    white-space: normal;
    width: 380px;
  }
    @media screen and (max-width: 768px) {
      [data-tooltip][data-tooltip-length='xlarge']:after {
        white-space: normal;
        width: 90vw;
      }
    }
  [data-tooltip][data-tooltip-length='fit']:after {
    white-space: normal;
    width: 100%;
  }
`;
};
