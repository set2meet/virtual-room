/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { injectIconsFontToGlobalStyle } from '../../entities/icons';
import { injectTooltipsToGlobalStyle } from './tooltips';
import { createGlobalStyle } from 'styled-components';
import { IRequiredTheme } from '../../entities/themes/types/ITheme';

const GlobalStyle = createGlobalStyle`${({ theme }: IRequiredTheme) => `  
  ${injectTooltipsToGlobalStyle(theme)};
  ${injectIconsFontToGlobalStyle};

  @font-face {
    font-family: RobotoLight;
    src: url(/fonts/Roboto-Light.ttf);
  }

  @font-face {
    font-family: Roboto;
    src: url(/fonts/Roboto-Regular.ttf);
  }

  input {
    -webkit-box-shadow: inset 0 0 0 50px ${theme.backgroundColor} !important;
  }

  html, body {
    font-size: 15px;
    font-family: Roboto;
    height: 100%;
  }

  h1 {
    margin: 120px 0 80px 0;
    font-family: RobotoLight;
    color: ${theme.h1Color};
  }
  
  hr {
    margin: 0;
    border-top-color: ${theme.backgroundColorThird};
  }
  
  .form-group {
    margin-bottom: 15px;
  }
  
  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    transition: background-color 5000s ease-in-out 0s;
    -webkit-text-fill-color: ${theme.fontColor} !important;
  }
  
  .form-control {
     background: inherit !important;
     border-radius: 0;     
     box-shadow: none !important;
     caret-color:  ${theme.buttonDefaultColor};
     color: ${theme.fontColor};     
  }

  .large {
    .form-control {
      height: 50px
      font-size: 22px;
      padding: 0;
          
      border: 0;
      border-bottom: 1px solid ${theme.h1Color};      
  
      &:focus {
        border-bottom: 2px solid ${theme.buttonDefaultColor};
        box-shadow: none;
      }
    }
  }

  a {
    color: ${theme.primaryColor};
    cursor: pointer;

    &:visited {
      color: #b781e1;
    }

    &:hover {
      color: ${theme.primaryColor};
    }
  }
  
  div.Toastify__progress-bar {
    background: ${theme.buttonOffColor};
  }
`}`;

export default GlobalStyle;
