import React, { useContext } from 'react';
import MediaQuery from 'react-responsive';
import { ThemeContext } from 'styled-components';
import { IBreakpointProps, ScreenSize } from './Breakpoint.types';

const Breakpoint: React.FunctionComponent<IBreakpointProps> = (props) => {
  const theme = useContext(ThemeContext);
  const mediaProps = props;

  switch (props.size) {
    case ScreenSize.MOBILE:
      mediaProps.maxWidth = theme.breakpoints.tablet - 1;
      break;
    case ScreenSize.TABLET:
      mediaProps.maxWidth = theme.breakpoints.desktop - 1;
      mediaProps.minWidth = theme.breakpoints.tablet;
      break;
    case ScreenSize.DESKTOP:
      mediaProps.minWidth = theme.breakpoints.desktop;
      break;
    default:
      mediaProps.minWidth = theme.breakpoints.tablet;
  }

  return <MediaQuery {...mediaProps}>{props.children}</MediaQuery>;
};

export default Breakpoint;
