import SimpleToggleButton from './index';
import React from 'react';
import { action } from '@storybook/addon-actions';
import { boolean, text } from '@storybook/addon-knobs';

export default {
  title: 'common/SimpleToggleButton',
  component: SimpleToggleButton,
  parameters: {
    themeDecorator: {
      centered: true,
    },
  },
};

export const Variants = () => {
  const icon = 'pencil';

  return (
    <>
      <SimpleToggleButton
        icon={icon}
        onClick={action('onClick1')}
        active={boolean('active', true)}
        disabled={boolean('disabled', false)}
      />
      <SimpleToggleButton icon={icon} onClick={action('onClick2')} active={true} disabled={true} />
      <SimpleToggleButton icon={icon} onClick={action('onClick3')} active={false} disabled={false} />
      <SimpleToggleButton icon={icon} onClick={action('onClick4')} active={false} disabled={true} />
    </>
  );
};

export const WithTooltip = () => {
  const tooltip = {
    'data-tooltip': text('tooltip', 'tooltip'),
    'data-tooltip-pos': 'right',
  };

  return <SimpleToggleButton icon="notepad" active={true} tooltip={tooltip} />;
};
