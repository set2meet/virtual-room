import * as React from 'react';
import styled, { css } from 'styled-components';
import { vrCommon } from '../../../common';
import ISimpleToggleButton = vrCommon.client.UI.ISimpleToggleButton;
import { ITheme } from '../../entities/themes/types/ITheme';
import icons from '../../entities/icons';
import { includeColorModel, includeSize } from '../../utils/button-utils';
import _omit from 'lodash/omit';

interface IStyleProps extends ISimpleToggleButton {
  theme: ITheme;
  value?: string;
}

export const styledButton = (props: IStyleProps) => css`
  ${includeSize(props.buttonSize)};

  outline: none;
  align-items: center;
  display: inline-flex;
  justify-content: center;
  border-style: solid;
  vertical-align: middle;
  background: ${props.theme.backgroundColorSecondary};

  &::before {
    display: inline-block;
    vertical-align: middle;
    font-family: ${icons.fontFamily};
    font-size: ${props.fontSize};
    content: ${(icons.code as any)[props.icon as any]}; // TODO workout type
  }

  ${includeColorModel(props)};
`;

const Button = styled.button`
  ${styledButton};
`;

/**
 * simple toggle button
 */
export class SimpleToggleButton extends React.Component<ISimpleToggleButton> {
  public static defaultProps = {
    fontSize: '22px',
    buttonSize: '40px',
  };

  public render() {
    return (
      <div {...this.props.tooltip}>
        <Button {..._omit(this.props, 'tooltip')} />
      </div>
    );
  }
}

export default SimpleToggleButton;
