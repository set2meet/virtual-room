import _get from 'lodash/get';
import React, { ComponentType } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { IRoomReduxState, IStoreState, IWebRTCServiceReduxState, TRoomUser } from '../../../../ioc/common/ioc-interfaces';
import UserMenu from '../UserMenu';
import SettingsPanel from '../SettingsPanel';

// TODO get from ioc list of headers
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { styledNavbar, styledNavbarCollapse } from './Header.style';
import { Location } from 'history';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import Skeleton from 'react-loading-skeleton';

interface IHeaderStateProps {
  location: Location | null;
  user: TRoomUser;
  roomState: IRoomReduxState;
  webrtc: IWebRTCServiceReduxState;
  userColor: string;
  isOnMeeting: boolean;
}

const headers: Record<string, ComponentType<IHeaderStateProps>> = {
  room: ({ roomState }: IHeaderStateProps) => {
    if (!roomState) {
      return null;
    }

    const RoomHeader = clientEntityProvider.resolveComponentSuspended(
      ComponentRegistryKey.Room_Header,
      <Skeleton width={'420px'} height={'30px'} />
    );
    return <RoomHeader />;
  },
};

const StyledNavbar = styled(Navbar)`
  ${styledNavbar};
`;
const StyledNavbarCollapse = styled(Navbar.Collapse)`
  ${styledNavbarCollapse};
`;

interface IHeaderDispatchProps {
  showModal?: ActionCreator<AnyAction>;
}

export class Header extends React.Component<IHeaderStateProps & IHeaderDispatchProps> {
  private getCurrentRoute = () => {
    const pathname = this.props.location.pathname;

    if (pathname === '/') {
      return 'dashboard';
    }

    if (pathname.indexOf('/room') === 0) {
      return 'room';
    }

    return undefined;
  };

  private _showSettingsPanel = () => {
    this.props.showModal({
      view: SettingsPanel,
    });
  };

  private _showFeedbackPanel = () => {
    const FeedbackPanel = clientEntityProvider.resolveComponentSuspended(
      ComponentRegistryKey.Feedback_Panel,
      <Skeleton width={'600px'} height={'450px'} />
    );

    this.props.showModal({
      view: FeedbackPanel,
      props: {
        width: 563,
        user: this.props.user,
        room: this.props.roomState,
        webrtc: this.props.webrtc,
      },
    });
  };

  public render() {
    const route = this.getCurrentRoute();
    const HeaderRoute = headers[route];

    return (
      <StyledNavbar>
        <Navbar.Header>
          <Navbar.Brand>
            <div className="s2m-logo" />
          </Navbar.Brand>
        </Navbar.Header>
        {this.props.user && (
          <StyledNavbarCollapse>
            {HeaderRoute && <HeaderRoute {...this.props} />}
            <Nav bsStyle="pills" pullRight={true}>
              <UserMenu
                user={this.props.user}
                userColor={this.props.userColor}
                isOnMeeting={this.props.isOnMeeting}
                showSettingsPanel={this._showSettingsPanel}
                showFeedbackPanel={this._showFeedbackPanel}
                avatarImage={this.props.user.picture}
              />
            </Nav>
          </StyledNavbarCollapse>
        )}
      </StyledNavbar>
    );
  }
}

export default connect<IHeaderStateProps, IHeaderDispatchProps>(
  (state: IStoreState) => {
    const { user } = state.auth;
    const room = state.room;
    const userColors = _get(state, 'room.state.userColors');
    const userColor = user && userColors ? userColors[user.id] : undefined;
    const { CONNECTION_STATUS } = clientEntityProvider.constants.webrtcConstants;

    return {
      user,
      roomState: room?.state,
      webrtc: room?.webrtcService,
      userColor,
      isOnMeeting: state.webphone?.connectionStatus !== CONNECTION_STATUS.NONE,
      location: state.router.location,
    };
  },
  (dispatch) => {
    const appActionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        showModal: appActionCreators.showModalDialog,
      },
      dispatch
    );
  }
)(Header);
