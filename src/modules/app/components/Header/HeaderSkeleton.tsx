import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import styled, { css } from 'styled-components';

const Container = styled.div`
  ${css`
    background-color: #333333;
    box-shadow: 0px 2px 8px 0 rgba(0, 0, 0, 0.4);
    height: 70px;
    width: 100%;
  `}
`;

const HeaderSkeleton = () => (
  <Container>
    {/* tslint:disable-next-line:no-magic-numbers */}
    <SkeletonTheme color={'#444'} highlightColor={'#333333'}>
      <div style={{ padding: '6px 15px', height: '100%' }}>
        <Skeleton width={100} height={'50px'} />
      </div>
    </SkeletonTheme>
  </Container>
);

export default HeaderSkeleton;
