/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const bttnSize = '16px';

export const divStyle = () => `
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background: #ffd54f;
  width: 600px;
  height: 50px;
`;

export const StyledText = () => `
  font-size: 20px;
  color: black;
`;

export const styledButtonClose = () => {
  const icons = clientEntityProvider.getIcons();

  return `
    margin: 0 3%;
    position: absolute;
      right: 0;
      height: 100%;
    // width: ${bttnSize};
    // height: ${bttnSize};
    
    border: none;
    background: transparent;
    outline: none;
    
    &:before {
      color: black;
      font-size: ${bttnSize};
      content: ${icons.code.close};
      font-family: ${icons.fontFamily};
    }
  `;
};
