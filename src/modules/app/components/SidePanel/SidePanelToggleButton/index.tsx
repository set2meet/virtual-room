import React from 'react';
import styled from 'styled-components';
import Indicator from '../../Indicator';

interface ISidePanelProps {
  open: boolean;
  position: 'left' | 'right';
}

interface ISidePanelToggleButtonProps extends ISidePanelProps {
  onClick: () => void;
  indicatorValue: number;
}

const getTransform = ({ open, position }: ISidePanelProps) => {
  let x = open ? '50%' : '100%';

  if (position === 'right') {
    x = '-' + x;
  }

  return `translate(${x}, -50%)`;
};

const getButtonPosition = (pos: ISidePanelProps['position']): string => {
  return pos === 'left' ? 'right: 0;' : 'left: 0;';
};

const StyledButtonContainer = styled.div`
  ${(props: ISidePanelProps) => `
  position: absolute;
  top: 50%;
  z-index: 1;
  transform: ${getTransform(props)};
  ${getButtonPosition(props.position)};
`};
`;

const StyledButton = styled.button`
  ${({ open, position }: ISidePanelProps) => `
  border: 0;
  padding: 3px 0;    
  border-radius: 0 3px 3px 0;
  outline:0;

  background: #575757;
  color: white;

  &:before {
    font-size: 20px;
    font-family: VRM Common Icons;
    content: '${open ? '\\e907' : '\\e906'}';   
  }

  transform: ${position === 'left' ? '' : 'rotate(180deg)'};
`};
`;

export class SidePanelToggleButton extends React.Component<ISidePanelToggleButtonProps> {
  public render() {
    return (
      <StyledButtonContainer open={this.props.open} position={this.props.position}>
        <StyledButton {...this.props} />
        {this.props.indicatorValue > 0 &&
          !this.props.open && <Indicator value={this.props.indicatorValue} position="left" />}
      </StyledButtonContainer>
    );
  }
}

export default SidePanelToggleButton;
