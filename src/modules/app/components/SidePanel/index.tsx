import React from 'react';
import styled from 'styled-components';
import { vrCommon } from '../../../common';
import ISidePanelProps = vrCommon.client.UI.ISidePanelProps;
import {
  ISidePanelContents,
  styledSidePanelContents,
  ISidePanelContainer,
  styledSidePanelContainer,
} from './SidePanel.styles';
import SidePanelToggleButton from './SidePanelToggleButton';

interface ISidePanelState {
  hasContent: boolean;
}

const TIME_SHIFT_BEFORE_REMOVE_CONTENT = 0.1; // ms

const SidePanelContents = styled.div<ISidePanelContents>`
  ${styledSidePanelContents};
`;
const SidePanelContainer = styled.div<ISidePanelContainer>`
  ${styledSidePanelContainer};
`;

export class SidePanel extends React.Component<ISidePanelProps, ISidePanelState> {
  private contents: React.ReactNode;

  private _showContents = () => {
    this.setState({ hasContent: true });
  };

  private _hideContents = () => {
    setTimeout(() => {
      this.setState({ hasContent: false });
    }, TIME_SHIFT_BEFORE_REMOVE_CONTENT * 1000);
  };

  constructor(props: ISidePanelProps) {
    super(props);

    this.state = {
      hasContent: this.props.open,
    };

    this.contents = (
      <SidePanelContents width={this.props.width} position={this.props.position}>
        {this.props.children}
      </SidePanelContents>
    );
  }

  public componentDidUpdate(prevProps: ISidePanelProps) {
    if (this.props.open !== prevProps.open) {
      if (this.props.open) {
        this._showContents();
      } else {
        this._hideContents();
      }
    }
  }

  public render() {
    return (
      <SidePanelContainer width={this.props.open ? this.props.width : 0}>
        <SidePanelToggleButton
          position={this.props.position}
          open={this.props.open}
          onClick={this.props.onToggle}
          indicatorValue={this.props.indicatorValue}
        />
        {this.state.hasContent && this.contents}
      </SidePanelContainer>
    );
  }
}

export default SidePanel;
