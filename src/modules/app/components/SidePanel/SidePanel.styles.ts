/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export interface ISidePanelContents {
  width: number;
  position: 'left' | 'right';
}

export interface ISidePanelContainer {
  width: number;
}

export const styledSidePanelContents = ({ width, position }: ISidePanelContents) => `
  top: 0;
  height: 100%;
  position: absolute;
  width: ${width}px;
  ${position === 'left' ? 'right' : 'left'}: 0;
`;

export const styledSidePanelContainer = ({ width }: ISidePanelContainer) => `  
  position: relative;
  height: 100%;
  width: ${width}px;  
`;
