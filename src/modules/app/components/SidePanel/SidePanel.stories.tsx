// tslint:disable:no-magic-numbers
import React, { ComponentType } from 'react';
import Skeleton from 'react-loading-skeleton';
import { vrCommon } from '../../../common';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { withIoCFactory } from '../../../../test/utils/utils';
import { action } from '@storybook/addon-actions';
import { boolean, number, radios, text } from '@storybook/addon-knobs';
import { connectDecorator } from '../../../../../.storybook/decorators/reduxDecorators';
import { setRoom, setUser } from '../../../../test/utils/actions';
import withStoryRootIndicator, {
  StoryRootIndicator,
} from '../../../../../.storybook/decorators/storyRootIndicator';
import ISidePanelProps = vrCommon.client.UI.ISidePanelProps;
import { ModuleRegistryKey } from '../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import withModule from '../../../../../.storybook/decorators/moduleDecorator';

const exampleWidth = 500;

// In order to workaround import order issues each ioc factory call should be wrapped inside render function
// make sure that props interface are exported separately from the component code to don't break the lazy-loading idea
const SuspendedSidePanel: ComponentType<ISidePanelProps> = withIoCFactory(() =>
  clientEntityProvider.resolveComponentSuspended<ISidePanelProps>(
    ComponentRegistryKey.SidePanel,
    <Skeleton width={exampleWidth} height={34} />
  )
);

export default {
  title: 'common/Side Panel',
  component: SuspendedSidePanel,
};

export const Default = () => (
  <SuspendedSidePanel
    position={radios('position', { left: 'left', right: 'right' }, 'left')}
    width={number('width', exampleWidth)}
    onToggle={action('onToggle')}
    open={boolean('open', false)}
    indicatorValue={number('indicatorValue', 0)}
  />
);

export const ClosedWithIndicator = () => (
  <SuspendedSidePanel
    position={radios('position', { left: 'left', right: 'right' }, 'left')}
    width={number('width', exampleWidth)}
    onToggle={action('onToggle')}
    open={boolean('open', false)}
    indicatorValue={number('indicatorValue', 30)}
  />
);

export const WithContent = () => (
  <SuspendedSidePanel
    position={radios('position', { left: 'left', right: 'right' }, 'left')}
    width={number('width', exampleWidth)}
    onToggle={action('onToggle')}
    open={boolean('open', true)}
  >
    <div>{text('content', '123')}</div>
  </SuspendedSidePanel>
);

export const WithContentRight = () => (
  <SuspendedSidePanel
    position={radios('position', { left: 'left', right: 'right' }, 'right')}
    width={number('width', exampleWidth)}
    onToggle={action('onToggle')}
    open={boolean('open', true)}
  >
    <div>{text('content', '123')}</div>
  </SuspendedSidePanel>
);

export const WithCollaborationPanel = () => {
  const CollaborationPanel = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.CollaborationPanel);

  return (
    <SuspendedSidePanel
      position={radios('position', { left: 'left', right: 'right' }, 'left')}
      width={number('width', 60)}
      open={boolean('open', true)}
      onToggle={action('onToggle')}
    >
      <CollaborationPanel />
    </SuspendedSidePanel>
  );
};

WithCollaborationPanel.story = {
  decorators: [withStoryRootIndicator, withModule(ModuleRegistryKey.RoomModule)],
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const WithSidebarPanel = (props) => {
  const SidebarPanel = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.SidebarPanel);
  const ChatModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Chat, {
    actionCreators: clientEntityProvider.getAppActionCreators(),
  });

  setUser(props, { id: 'user123', roomId: '123' });
  setRoom(props, '123', 'user123');

  return (
    <ChatModule>
      <StoryRootIndicator>
        <SuspendedSidePanel
          position={radios('position', { left: 'left', right: 'right' }, 'left')}
          width={number('width', exampleWidth)}
          open={boolean('open', true)}
          onToggle={action('onToggle')}
        >
          <SidebarPanel />
        </SuspendedSidePanel>
      </StoryRootIndicator>
    </ChatModule>
  );
};

WithSidebarPanel.story = {
  decorators: [
    connectDecorator(),
    withModule(ModuleRegistryKey.Webrtc),
    withModule(ModuleRegistryKey.Webphone),
    withModule(ModuleRegistryKey.RoomModule),
  ],
  parameters: {
    redux: {
      enable: true,
    },
  },
};
