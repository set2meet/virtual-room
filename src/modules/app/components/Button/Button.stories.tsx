import React, { ComponentType } from 'react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { mapButtonStyleByBsStyle } from './Button.style';
import Skeleton from 'react-loading-skeleton';
import { vrCommon } from '../../../common';
import IButtonProps = vrCommon.client.UI.IButtonProps;
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { withIoCFactory } from '../../../../test/utils/utils';

// make sure that props interface are exported separately from the component code to don't break the lazy-loading idea
const buttonFactory = () =>
  clientEntityProvider.resolveComponentSuspended<IButtonProps>(
    ComponentRegistryKey.Button,
    // tslint:disable-next-line:no-magic-numbers
    <Skeleton width={100} height={34} />
  );

// In order to workaround import order issues each ioc factory call should be wrapped inside render function
const SuspendedButton: ComponentType<IButtonProps> = withIoCFactory<IButtonProps>(buttonFactory);

export default {
  title: 'common/Button',
  component: SuspendedButton,
};

export const Default = () => <SuspendedButton />;

export const DefaultWithKnobs = () => {
  const commonIcons = clientEntityProvider.getIcons();
  return (
    <SuspendedButton
      disabled={boolean('Disabled', false)}
      title={text('title', 'Default button')}
      bsStyle={select('style', Object.keys(mapButtonStyleByBsStyle), 'default')}
      icon={select('icon', ['', ...Object.keys(commonIcons.code)], '')}
    />
  );
};

export const allButtons = () => {
  const commonIcons = clientEntityProvider.getIcons();

  return (
    <>
      <p>By variant: </p>
      {Object.keys(mapButtonStyleByBsStyle).map((style) => (
        <SuspendedButton disabled={boolean('Disabled', false)} bsStyle={style} title={text('title', 'Button')} key={style} />
      ))}
      <br />
      <p>By icon: </p>
      {Object.keys(commonIcons.code).map((code) => (
        <SuspendedButton disabled={boolean('Disabled', false)} icon={code} key={code} />
      ))}
      <p>By size: </p>
      <SuspendedButton disabled={boolean('Disabled', false)} title={text('title', 'Button')} bsSize="large" />
    </>
  );
};

export const Warning = () => (
  <SuspendedButton disabled={false} bsStyle="warning">
    Warning button
  </SuspendedButton>
);
