/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import icons from '../../entities/icons';
import { lighten, darken } from 'polished';
import { CommonIconsList, vrCommon } from '../../../common';
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';
import IButtonProps = vrCommon.client.UI.IButtonProps;
import { addStyle } from 'react-bootstrap/lib/utils/bootstrapUtils';
import { Button } from 'react-bootstrap';
import { css } from 'styled-components';

// Add custom variants
addStyle(Button, 'secondary');

const DARKEN_VAL = 0.1;
const LIGHTEN_VAL = 0.04;
const TRANSPARENT = 'transparent';

const includeIconIfExisted = (icon: CommonIconsList) => {
  if (!icon) {
    return '';
  }

  return css`
    &:before {
      vertical-align: middle;
      display: inline-block;
      font-family: ${icons.fontFamily};
      content: ${icons.code[icon]};
      font-size: 16px;
      margin-right: 8px;
      margin-bottom: 3px;
      margin-top: 3px;
      line-height: inherit;
    }
  `;
};

const mapButtonStyleByBsSizes: Record<string, string> = {
  large: `
    width: 400px;
    height: 50px;
    font-size: 20px;
  `,
  default: `
    width: auto;
    height: 34px;
    font-size: 14px;
  `,
};

const getButtonStyle = (
  backgroundColor: string | null,
  borderColor: string | null,
  borderWidth: number,
  textColor: string
) => css`
  border-width: ${borderWidth || 0}px;

  &:not(:disabled) {
    border-color: ${borderColor || TRANSPARENT};
    background-color: ${backgroundColor || TRANSPARENT};

    &:hover {
      border-color: ${borderColor ? lighten(LIGHTEN_VAL, borderColor) : TRANSPARENT};
      background-color: ${backgroundColor ? lighten(LIGHTEN_VAL, backgroundColor) : TRANSPARENT};
    }

    &:active {
      border-color: ${borderColor ? darken(DARKEN_VAL, borderColor) : TRANSPARENT};
      background-color: ${backgroundColor ? darken(LIGHTEN_VAL, backgroundColor) : TRANSPARENT};
    }
  }

  &:disabled {
    &,
    &:hover,
    &:active {
      color: ${textColor} !important;
      background-color: ${backgroundColor || TRANSPARENT};
    }
  }
`;

export const mapButtonStyleByBsStyle: any = {
  default: ({ theme }: IRequiredTheme) => css`
    border-width: 0;
    border-color: transparent;

    &:not(:disabled) {
      background-color: ${theme.buttonDefaultColor};

      &:hover {
        background-color: ${theme.buttonDefaultColorHover};
      }

      &:active {
        background-color: ${theme.buttonDefaultColorActive};
      }
    }

    &:disabled {
      &,
      &:hover,
      &:active {
        background-color: ${theme.buttonDisabledColor};
      }
    }

    &.btn-default[disabled]:focus {
      background-color: ${theme.buttonDisabledColor} !important;
    }
  `,
  warning: ({ theme }: IRequiredTheme) => css`
    border-width: 0px;
    border-color: transparent;

    &:not(:disabled) {
      background-color: ${theme.buttonWarningColor};

      &:hover {
        background-color: ${theme.buttonWarningColorHover};
      }

      &:active {
        background-color: ${theme.buttonWarningColorActive};
      }
    }

    &:disabled {
      &,
      &:hover,
      &:active {
        background-color: ${theme.buttonDisabledColor};
      }
    }
  `,
  info: ({ theme }: IRequiredTheme) => css`
    border-width: 2px;
    border-color: transparent;
    background-color: transparent;

    &.btn-info {
      border-color: ${theme.buttonDefaultColor};
      background-color: transparent;

      > span {
        color: ${theme.buttonDefaultColor};
      }

      &:disabled:hover {
        border-color: ${theme.buttonDefaultColor};
        background-color: transparent;
      }

      &:not(:disabled) {
        background-color: transparent;
        border-color: ${theme.buttonDefaultColor};
        &:before {
          color: ${theme.buttonDefaultColor};
        }

        &:hover {
          background-color: transparent;
          border-color: ${theme.buttonDefaultColorHover};
          &:before {
            color: ${theme.buttonDefaultColorHover};
          }

          > span {
            color: ${theme.buttonDefaultColorHover};
          }
        }

        &:active {
          background-color: transparent;
          border-color: ${theme.buttonDefaultColorActive};
          &:before {
            color: ${theme.buttonDefaultColorActive};
          }

          > span {
            color: ${theme.buttonDefaultColorActive};
          }
        }
      }

      &:disabled {
        &,
        &:hover,
        &:active {
          opacity: 1;
          background-color: transparent;
          color: ${theme.buttonDisabledColor};
          border-color: ${theme.buttonDisabledColor};
        }

        &:before {
          color: ${theme.buttonDisabledColor};
        }

        > span {
          color: ${theme.buttonDisabledColor};
        }
      }
    }
  `,
  link: ({ theme }: IRequiredTheme) => css`
    padding-left: 0;
    padding-right: 0;

    ${getButtonStyle(null, null, 0, theme.primaryTextColor)};
  `,
  secondary: ({ theme }: IRequiredTheme) => css`
    border-width: 2px;
    border-color: ${theme.buttonOffColor};
    color: ${theme.buttonOffColor} !important;

    &:not(:disabled) {
      background-color: ${theme.buttonSecondaryColor};

      &:hover,
      &:active {
        border-color: ${theme.primaryTextColor};
        color: ${theme.primaryTextColor} !important;
      }
    }

    &:disabled {
      &,
      &:hover,
      &:active {
        background-color: ${theme.buttonDisabledColor};
      }
    }
  `,
};

export const styledButton = (props: IButtonProps & IRequiredTheme) => css`
  ${mapButtonStyleByBsSizes[props.bsSize || 'default']};

  padding: 0 12px;
  transition: 0.2s;
  border-radius: 2px;
  color: ${props.theme.fontColor} !important;

  > span {
    vertical-align: middle;
  }

  &.btn {
    &,
    &:hover,
    &:focus,
    &:active,
    &:active:focus {
      outline: none;
      box-shadow: none !important;
      text-decoration: none !important;
    }
  }

  ${includeIconIfExisted(props.icon as CommonIconsList)};

  ${mapButtonStyleByBsStyle[props.bsStyle || 'default'](props)};
`;
