import React from 'react';
import { Button } from 'react-bootstrap';
import styled from 'styled-components';
import { styledButton } from './Button.style';

const StyledButton = styled(Button)`
  ${styledButton};
`;

class VRButton extends React.Component<any> {
  public render() {
    return (
      <StyledButton {...(this.props as any)}>
        {' '}
        {/* TODO fix styled component ts error*/}
        <span>{this.props.title}</span>
        {this.props.children}
      </StyledButton>
    );
  }
}

export default VRButton;
