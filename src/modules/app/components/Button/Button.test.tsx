import { loadAsyncTree, mountStory, setupStorybookForTest } from '../../../../test/utils/storybook';
import { enzymeCleanup } from '../../../../test/utils/utils';

beforeAll(setupStorybookForTest());
afterEach(enzymeCleanup);

describe('Button', () => {
  it('Button with text: check styles and lazy loading', async (done) => {
    const wrapper = await mountStory('common/Button', 'Default', false);

    expect(wrapper.find('LoadFallbackWrapper')).toExist();

    await loadAsyncTree(wrapper);

    expect(wrapper.find('VRButton')).toExist();
    expect(wrapper.find('VRButton')).toHaveStyleRule('padding', '0 12px');
    done();
  });
  it('Button with text: check text', async (done) => {
    const wrapper = await mountStory('common/Button', 'Warning');
    const button = wrapper.find('VRButton');

    expect(button.contains('Warning button')).toEqual(true);
    done();
  });
});
