/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import GlobalStyles from './GlobalStyles';
import Button from './Button';
import Breakpoint from './Breakpoint/Breakpoint';
import SimpleToggleButton from './SimpleToggleButton';
import Spinner from './Spinner';
import SidePanel from './SidePanel';
import RoundButton from './RoundButton';
import UserAvatar from './UserAvatar';
import { vrCommon } from '../../common';
import ICommonUI = vrCommon.client.UI.ICommonUI;
import WaitingPage from './WaitingPage';
import BrowserNotSupported from './BrowserNotSupported';
import Checkbox from './Checkbox';

const ui: ICommonUI = {
  GlobalStyles,
  Button,
  Breakpoint,
  SimpleToggleButton,
  Spinner,
  SidePanel,
  UserAvatar,
  RoundButton,
  WaitingPage,
  BrowserNotSupported,
  Checkbox,
};

export { ui };
