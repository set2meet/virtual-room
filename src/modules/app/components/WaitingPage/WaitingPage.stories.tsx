import React from 'react';
import { text } from '@storybook/addon-knobs';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { withIoCFactory } from '../../../../test/utils/utils';
import { vrCommon } from '../../../common';
import IWaitingPageProps = vrCommon.client.UI.IWaitingPageProps;

const WaitingPageSuspended = withIoCFactory<IWaitingPageProps>(() =>
  clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.WaitingPage)
);

export default {
  title: 'common/WaitingPage',
  component: WaitingPageSuspended,
};

export const Default = () => <WaitingPageSuspended text={text('text', 'Some default text')} />;
