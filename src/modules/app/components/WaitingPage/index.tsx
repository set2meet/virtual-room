import React from 'react';
import styled from 'styled-components';
import { vrCommon } from '../../../common';
import Spinner from '../Spinner';
import IWaitingPageProps = vrCommon.client.UI.IWaitingPageProps;
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

const StyledWaitingPage = styled.div`
  ${({ theme }: IRequiredTheme) => `
  margin-top: 200px;
  display: flex;
  align-items: center;
  flex-direction: column;

  .s2m-logo-short {
    width: 140px;
    height: 88px;
    margin-bottom: 50px;   
    background-image: url(${theme.logoShort});
    background-repeat: no-repeat;
    background-position: center bottom;
    background-size: contain;
  }

  .s2m-wp-text {
    display: flex;
    align-items: center;
    font-size: 22px;

    *:first-child {
      margin-right: 20px;
    }
  }
`};
`;

class WaitingPage extends React.Component<IWaitingPageProps> {
  public render() {
    return (
      <StyledWaitingPage>
        <div className="s2m-logo-short" />
        <div className="s2m-wp-text">
          <Spinner />
          {this.props.text}
        </div>
      </StyledWaitingPage>
    );
  }
}

export default WaitingPage;
