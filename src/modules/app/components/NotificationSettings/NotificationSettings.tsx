import * as React from 'react';
import styled from 'styled-components';

import { styledContainer } from './/NotificationSettings.style';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';

const NotificationSettingsContainer = styled.div`
  ${styledContainer};
`;

/**
 * UI
 */
export default class NotificationSettings extends React.Component<{}> {
  private _renderChatSoundSetup() {
    const NotificationSetup = clientEntityProvider.resolveComponentSuspended(
      ComponentRegistryKey.Chat_NotificationSetup
    );

    return <NotificationSetup />;
  }

  public render() {
    return <NotificationSettingsContainer>{this._renderChatSoundSetup()}</NotificationSettingsContainer>;
  }
}
