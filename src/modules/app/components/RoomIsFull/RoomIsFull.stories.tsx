import React, { ComponentType } from 'react';
import Skeleton from 'react-loading-skeleton';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { withIoCFactory } from '../../../../test/utils/utils';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

// In order to workaround import order issues each ioc factory call should be wrapped inside render function
const RoomIsFullSuspended: ComponentType = withIoCFactory(() =>
  // make sure that props interface are exported separately from the component code to don't break the lazy-loading idea
  clientEntityProvider.resolveComponentSuspended(
    ComponentRegistryKey.RoomIsFool,
    // tslint:disable-next-line:no-magic-numbers
    <Skeleton width={100} height={34} />
  )
);

export default {
  title: 'app/Room is full',
  component: RoomIsFullSuspended,
};

export const Default = () => <RoomIsFullSuspended />;
