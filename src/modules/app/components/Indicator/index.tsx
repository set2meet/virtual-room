import React from 'react';
import styled from 'styled-components';
import { IIndicatorProps, IStyledProps } from './Indicator.types';

const indicatorSize = 20;
const MAX_VALUE = 99;

const styledIndicator = (props: IStyledProps) => `
  display: inline-flex;
  position: absolute;  
  top: -${indicatorSize / 2}px;
  border: 0;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  width: ${indicatorSize}px;
  height: ${indicatorSize}px;
  background-color: ${props.theme.buttonWarningColor};

  &:before {
    vertical-align: middle;
    font-size: 9px;
    display: inline-block;
    height: ${indicatorSize}px;
    line-height: ${indicatorSize}px;
    color: ${props.theme.primaryTextColor};
    content: '${props.value > MAX_VALUE ? `${MAX_VALUE}+` : props.value}'; 
    font-weight: bold;
  }
  
   ${props.position === 'left' ? `left: -${indicatorSize / 2}px;` : `right: -${indicatorSize}px;`}
`;

const StyledIndicator = styled.span`
  ${(props: IStyledProps) => styledIndicator(props)};
`;

class Indicator extends React.Component<IIndicatorProps> {
  public static defaultProps = {
    position: 'right',
  };

  public render() {
    return <StyledIndicator {...this.props} />;
  }
}

export default Indicator;
