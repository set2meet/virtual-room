import React from 'react';
import styled from 'styled-components';
import {
  IBrowserSuggestionProps,
  styledContainer,
  styledLayout,
  styledTitle,
  styledDescription,
  styledBrowserSuggestionContainer,
  styledBrowserSuggestion,
  styledBrowserSuggestionLink,
} from './BrowserNotSupported.style';
import { vrCommon } from '../../../common';
import { RecommendedBrowser } from '../../../../ioc/client/types/interfaces';
import IBrowserNotSupportedOwnProps = vrCommon.client.UI.IBrowserNotSupportedOwnProps;
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

interface IBrowserSuggestion {
  name: RecommendedBrowser;
  title: string;
  version: string;
}

const PageContainer = styled.div`
  ${styledContainer};
`;
const PageLayout = styled.div`
  ${styledLayout};
`;
const Title = styled.div`
  ${styledTitle};
`;
const Description = styled.div`
  ${styledDescription};
`;
const SuggestionContainer = styled.div`
  ${styledBrowserSuggestionContainer};
`;
const Suggestion = styled.div`
  ${styledBrowserSuggestion};
`;

const SuggestionLink = styled.a<IBrowserSuggestionProps>`
  ${styledBrowserSuggestionLink};
`;

const capitalizeFirstLetter = (value: string): string => {
  return value.charAt(0).toUpperCase() + value.slice(1);
};

const browserSuggestion = (): IBrowserSuggestion[] => {
  const browser = clientEntityProvider.getBrowser();

  return browser.recomendations.map(
    (name: RecommendedBrowser): IBrowserSuggestion => {
      return {
        name,
        title: capitalizeFirstLetter(name),
        version: browser.supportedBrowsers[name].version + '+',
      };
    }
  );
};

class BrowserNotSupported extends React.Component<IBrowserNotSupportedOwnProps, {}> {
  public render() {
    const { projectName, techSupportEMail } = this.props;
    const emailSubject = `[${projectName}] problems`;
    const browser = clientEntityProvider.getBrowser();
    const EMAIL_BODY = encodeURIComponent(`Browser user agent: ${navigator.userAgent}`);

    return (
      <PageContainer>
        <PageLayout>
          <Title>Please upgrade your browser to use {projectName}</Title>
          <Description>
            We build {projectName} using the latest technology. This makes {projectName} faster and easier to use.
            <br />
            Unfortunately, your browser doesn't support those technologies. Download one of these great
            <br /> browsers and you'll be on your way:
          </Description>
          <SuggestionContainer>
            {browserSuggestion().map((item, inx) => (
              <Suggestion key={inx}>
                <SuggestionLink browser={item.name} target="_blank" href={browser.supportedBrowsers[item.name].link}>
                  <div>{item.title}</div>
                </SuggestionLink>
                <div>(Version {item.version})</div>
              </Suggestion>
            ))}
          </SuggestionContainer>
          {techSupportEMail && (
            <Description>
              Already upgraded but still having problems? Please{' '}
              <a href={`mailto:${techSupportEMail}?subject=${emailSubject}&body=${EMAIL_BODY}`}>contact us</a>
            </Description>
          )}
        </PageLayout>
      </PageContainer>
    );
  }
}

export default BrowserNotSupported;
