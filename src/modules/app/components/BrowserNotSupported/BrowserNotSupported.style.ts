/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { RecommendedBrowser, IRequiredTheme } from '../../../../ioc/client/types/interfaces';
import iconChromium from './icons/chromium.png';
import iconVivaldi from './icons/vivaldi.png';
import iconFirefox from './icons/firefox.png';
import iconChrome from './icons/chrome.png';
import iconSafari from './icons/safari.png';

import iconEdge from './icons/edge.png';

const iconsLinksMap: Record<RecommendedBrowser, string> = {
  chromium: iconChromium,
  vivaldi: iconVivaldi,
  firefox: iconFirefox,
  chrome: iconChrome,
  safari: iconSafari,
  edge: iconEdge,
};

export const styledContainer = () => `
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
  border: 0;
  display: flex;
  overflow: hidden;
  align-items: center;
  justify-content: center;
`;

export const styledLayout = () => `
  display: inline-block;
  max-width: 800px;
`;

export const styledTitle = ({ theme }: IRequiredTheme) => `
  color: ${theme.primaryTextColor};
  font-size: 38px;
  text-align: center;
  white-space: nowrap;
`;

export const styledDescription = ({ theme }: IRequiredTheme) => `
  color: ${theme.primaryTextColor};
  font-size: 18px;
  text-align: center;
  margin: 32px 0;
`;

export const styledBrowserSuggestionContainer = () => `
  text-align: center;
  padding: 20px 0;
`;

export interface IBrowserSuggestionProps extends IRequiredTheme {
  browser: RecommendedBrowser;
}

export const styledBrowserSuggestionLink = ({ theme, browser }: IBrowserSuggestionProps) => `
  display: inline-block;
    
 &:before {
    display: block;
    text-align: center;
    height: 70px;
    margin-bottom: 20px;
    content: url(${iconsLinksMap[browser]});
  } 
`;

export const styledBrowserSuggestion = ({ theme }: IRequiredTheme) => `
  display: inline-block;
  margin: 0 30px;
  height: 130px;
  
  > div {
    font-size: 14px;
  }

  > div:first-child {
    color: ${theme.primaryColor};
  }

  > div:last-child {
    opacity: 0.6;
    color: ${theme.primaryTextColor};
    cursor: default;
    text-decoration: none;
  }
`;
