import React from 'react';
import { text } from '@storybook/addon-knobs';
import { withIoCFactory } from '../../../../test/utils/utils';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { vrCommon } from '../../../common';
import IBrowserNotSupportedOwnProps = vrCommon.client.UI.IBrowserNotSupportedOwnProps;

const SuspendedBrowserNotSupported = withIoCFactory<IBrowserNotSupportedOwnProps>(() =>
  clientEntityProvider.getComponentProvider().resolveComponentSuspended(ComponentRegistryKey.BrowserNotSupported)
);

export default {
  title: 'common/BrowserNotSupported',
  component: SuspendedBrowserNotSupported,
};

export const Default = () => (
  <SuspendedBrowserNotSupported
    projectName={text('projectName', 'Set2Meet')}
    techSupportEMail={text('techSupportEMail', null)}
  />
);

export const WithTechSupportEmail = () => (
  <SuspendedBrowserNotSupported
    projectName={text('projectName', 'Set2Meet')}
    techSupportEMail={text('techSupportEMail', 'email')}
  />
);
