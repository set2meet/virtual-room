import React from 'react';
import styled, { withTheme } from 'styled-components';
import { vrCommon } from '../../../common';
import { defaultTheme } from '../../entities/themes';
import IUserAvatarProps = vrCommon.client.UI.IUserAvatarProps;

const DEFAULT_SIZE = '34px';
const DEFAULT_USER_COLOR = defaultTheme.buttonDisabledColor;

interface IStyledUserAvatarProps extends IUserAvatarProps {
  color: string;
  scale: number;
  abbr?: string;
  size: string;
}

const includTextAbbr = (props: IStyledUserAvatarProps) => {
  if (!props.abbr) {
    return '';
  }

  return `
    &:before {
      -webkit-user-select: none;  
      -moz-user-select: none;    
      -ms-user-select: none;      
      user-select: none;
      content: '${props.abbr}';
      font-size: 12px;
      transform-origin: 50% 50%;
      font-size: 12px;
      font-weight: bold;
      color: white;
      display: inline-flex;
      width: ${DEFAULT_SIZE};
      height: ${DEFAULT_SIZE};
      justify-content: center;
      align-items: center;
      transform: scale(${props.scale}, ${props.scale});
    }
  `;
};

const StyledAvatar = withTheme(
  styled.div`
    ${(props: IStyledUserAvatarProps) => `
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  overflow: hidden;
  width: ${props.size};
  height: ${props.size};
  background-color: ${props.color};

  img {
    height: 100%;
  }

  ${includTextAbbr(props)};
`};
  `
);

const getDisplayNameAbbr = (name: string = ''): string => {
  return name
    .split(/-|\s+/g)
    .reduce((previousValue, currentValue) => {
      if (currentValue === '') {
        return previousValue;
      }

      return previousValue + currentValue[0];
    }, '')
    .toUpperCase()
    .substr(0, 2);
};

const getAvatarColor = (color?: string): string => {
  return color || DEFAULT_USER_COLOR;
};

const calcScale = (size: string): number => {
  if (!size) {
    return 1;
  }

  return parseInt(size, 10) / parseInt(DEFAULT_SIZE, 10);
};

const UserAvatar: React.SFC<IUserAvatarProps> = (props: IUserAvatarProps) => {
  const scale = calcScale(props.size);
  const size = props.size || DEFAULT_SIZE;
  const hasAvatarSrc: boolean = !!props.src;
  const abbr = !hasAvatarSrc && getDisplayNameAbbr(props.displayName);
  const color = hasAvatarSrc ? '#fff' : getAvatarColor(props.userColor);
  return (
    <StyledAvatar {...props} color={color} scale={scale} abbr={abbr} size={size}>
      {hasAvatarSrc && <img src={props?.src || ''} />}
    </StyledAvatar>
  );
};

export default UserAvatar;
