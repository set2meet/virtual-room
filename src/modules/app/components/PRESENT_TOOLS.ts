/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { CommonIconsList } from '../../../ioc/client/types/interfaces';

export interface ITools {
  [key: string]: {
    name: string;
    icon: CommonIconsList[keyof CommonIconsList];
  };
}

// TODO Room\index.tsx must use component field with common attrs
const tools: ITools = {
  whiteboard: {
    name: 'Whiteboard',
    icon: 'brush',
  },
  codeEditor: {
    name: 'Code Editor',
    icon: 'codeEditor',
  },
  webphone: {
    name: 'Conference',
    icon: 'conference',
  },
  notepad: {
    name: 'Notepad',
    icon: 'notepad',
  },
  presentations: {
    name: 'Presentation',
    icon: 'presentation',
  },
  graph2d: {
    name: 'Graph 2D',
    icon: 'graph2d',
  },
};

export default tools;
