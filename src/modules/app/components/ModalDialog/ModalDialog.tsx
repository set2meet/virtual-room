import React from 'react';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { IStoreState } from '../../../../ioc/common/ioc-interfaces';
import { connect } from 'react-redux';
import { ModalContainer, ModalBody } from './ModalDialog.style';
import { IAppModalDialogDefinition } from '../../../../ioc/client/types/interfaces';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

type ModalDialogBackdrop = 'static' | boolean;

interface IModalDialogStateProps {
  modal?: IAppModalDialogDefinition;
}

interface IModalDialogDispatchProps {
  onHide: ActionCreator<AnyAction>;
}

type IModalDialogProps = IModalDialogStateProps & IModalDialogDispatchProps;

export class ModalDialog extends React.Component<IModalDialogProps> {
  public render() {
    const { modal, onHide } = this.props;

    if (!modal) {
      return null;
    }

    const ModalView = modal.view;
    const containerSize = ModalView.SIZE || ({} as any);
    const backdrop: ModalDialogBackdrop = modal.closeOnOutsideClick === false ? 'static' : true;
    const containerProps = { onHide, backdrop, show: true };
    const modalProps = {
      onHide,
      ...modal.props,
      container: modal.container,
    };

    return (
      <ModalContainer {...containerProps} {...containerSize}>
        <ModalBody>
          <ModalView {...modalProps} />
        </ModalBody>
      </ModalContainer>
    );
  }
}

export default connect<IModalDialogStateProps, IModalDialogDispatchProps>(
  (state: IStoreState) => ({
    modal: state.app.view.modalDialog,
  }),
  (dispatch) => {
    const actionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        onHide: actionCreators.hideModalDialog,
      },
      dispatch
    );
  }
)(ModalDialog);
