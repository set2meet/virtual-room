/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import styled, { css } from 'styled-components';
import { Modal } from 'react-bootstrap';
import { IRequiredTheme } from '../../../../ioc/client/types/interfaces';

export interface IModalViewProps extends IRequiredTheme {
  width: number;
  height: number;
}

const styledModalContainer = ({ width, height }: IModalViewProps) => css`
  display: flex !important;
  align-items: center;
  justify-content: center;

  &.modal {
    padding: 0 !important;
    overflow: hidden;

    .modal-dialog {
      border: 0;

      width: ${width ? `${width}px;` : ''};
      height: ${height ? `${height}px;` : ''};
    }

    .modal-content {
      height: 100%;
    }

    .modal-body {
      padding: 0;
      height: 100%;
    }
  }
`;

const styledModalBody = css`
  background-color: ${(props) => props.theme.backgroundColorSecondary};
  color: ${(props) => props.theme.primaryTextColor};
  font-size: 18px;
`;

export const ModalContainer = styled(Modal)<IModalViewProps>`
  ${styledModalContainer};
`;
export const ModalBody = styled(Modal.Body)`
  ${styledModalBody};
`;
