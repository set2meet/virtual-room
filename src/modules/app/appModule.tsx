import Socket = SocketIOClient.Socket;
import App from '../../client/routes/App/App';
import appReducer from './redux/appReducer';
import { IS2MModule, TS2MModuleFactory } from '../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleError } from '../../ioc/client/providers/ModuleProvider/types/ModuleError';
import { appMiddleware } from './redux/middleware/appMiddleware';
import apiReduxMiddleware from './redux/api/reduxMiddleware';
import { TAppModuleInitOptions } from './types/TAppModuleInitOptions';

/**
 * @param socket
 * @param stateKey
 * @param moduleInitOptions
 */
const createAppModule: TS2MModuleFactory<TAppModuleInitOptions> = async (
  socket: Socket,
  stateKey: string = 'app',
  moduleInitOptions: TAppModuleInitOptions
): Promise<IS2MModule> => {
  if (!moduleInitOptions || !moduleInitOptions.config) {
    throw new Error(ModuleError.REQUIRE_INITIAL_OPTIONS);
  }
  const { config } = moduleInitOptions;

  return {
    rootComponent: moduleInitOptions.skipRootMount ? null : App,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: appReducer(config),
        },
        middlewares: [appMiddleware(stateKey)(socket, config), apiReduxMiddleware('api')(socket)],
      },
    ],
  };
};

export default createAppModule;
