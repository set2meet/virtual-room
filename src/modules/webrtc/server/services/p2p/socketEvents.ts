/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import IORedis from 'ioredis';
import { atob, decodeSessionId } from './utils';
import { Socket } from 'socket.io';
import { SERVICE_EVENT_MESSAGE, SERVICE_EVENT_TYPE, SERVICE_CONNECTION_MEDIA_MODE } from '../../../common/types/p2p/constants';
import { IServerMetaObjects } from '../../../../../ioc/server/types/interfaces';

type ReddisSessionState = {
  roomId: string;
  users: string[];
};

type ExistedPeersMap = Record<string, 1>;

type SocketHandler = (meta: IServerMetaObjects, payload: any) => void;

const REDIS_SESSSIONS_KEY = 'p2p-sessions';

const { DESKTOP: mediaModeDesktop } = SERVICE_CONNECTION_MEDIA_MODE;

const parseToken = (token: string): any => {
  return JSON.parse(atob(token.split('.')[1]));
};

const getRoomIdFromSessionId = (sessionId: string): string => {
  const sessionInfo = decodeSessionId(sessionId);

  return sessionInfo ? sessionInfo.roomId : null;
};

const setSessionState = async (pubClient: IORedis.Redis, sessionId: string, state: ReddisSessionState) => {
  await pubClient.hset(REDIS_SESSSIONS_KEY, sessionId, JSON.stringify(state));
};

const getSessionState = async (pubClient: IORedis.Redis, sessionId: string): Promise<ReddisSessionState> => {
  const sessionState = await pubClient.hget(REDIS_SESSSIONS_KEY, sessionId);

  if (sessionState) {
    return JSON.parse(sessionState);
  }

  return null;
};

const addUserToSession = async (pubClient: IORedis.Redis, sessionId: string, userId: string): Promise<boolean> => {
  const state = await getSessionState(pubClient, sessionId);

  if (!state) {
    return false;
  }

  const inx = state.users.indexOf(userId);

  if (inx === -1) {
    state.users.push(userId);
    await setSessionState(pubClient, sessionId, state);

    return true;
  }

  return false;
};

const removeUserFromSession = async (pubClient: IORedis.Redis, sessionId: string, userId: string) => {
  const state = await getSessionState(pubClient, sessionId);

  if (!state) {
    return;
  }

  const inx = state.users.indexOf(userId);

  if (inx === -1) {
    return;
  }

  state.users.splice(inx, 1);

  await setSessionState(pubClient, sessionId, state);
};

const disconnectUserFromSession: SocketHandler = async (meta, payload) => {
  const {
    socketAdapter,
    redis: { pubClient },
  } = meta;
  const { src, sessionId } = payload;

  await removeUserFromSession(pubClient, sessionId, src);

  const state = await getSessionState(pubClient, sessionId);

  // no state - session was closed
  if (!state) {
    return;
  }

  const { users, roomId } = state;

  // has state - we are just leave the room
  socketAdapter.emit({
    target: {
      roomId,
      users,
    },
    event: SERVICE_EVENT_MESSAGE,
    payload: {
      type: SERVICE_EVENT_TYPE.PEER_LEAVE,
      payload,
    },
  });
};

const socketHandlerEnums: Record<string, SocketHandler> = {
  /**
   *
   * @param config object is similar to application config;
   * @param payload
   */
  [SERVICE_EVENT_TYPE.CONNECT]: async (config, payload) => {
    const {
      socketAdapter,
      redis: { pubClient },
    } = config;

    const { sessionId } = payload;
    const { userId } = parseToken(payload.token);

    const state = await getSessionState(pubClient, sessionId);
    const socket = socketAdapter.getRoomClientSocket(state.roomId, userId);
    if (!socket) {
      console.log(`Error: connection was failed for user '${userId}: no socket connection found'`);
      return;
    }

    if (state) {
      await addUserToSession(pubClient, sessionId, userId);

      // say the client that he has connected
      socket.emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.CONNECT_SUCCESS,
        payload: { userId, sessionId },
      });

      // remove clietn from session on disconnect
      socket.on('disconnect', () => {
        disconnectUserFromSession(config, { src: userId, sessionId });
      });
    } else {
      socket.emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.CONNECT_FAILURE,
        payload: { userId, sessionId },
        message: `[P2P] session ${sessionId} not found`,
      });
    }
  },
  [SERVICE_EVENT_TYPE.DISCONNECT]: async (meta, payload) => {
    disconnectUserFromSession(meta, payload);
  },
  [SERVICE_EVENT_TYPE.PEER_OFFER]: async (meta, payload) => {
    const { socketAdapter } = meta;
    const { sessionId, dst: peerId } = payload;
    const roomId = getRoomIdFromSessionId(sessionId);

    socketAdapter.emit({
      target: {
        roomId,
        users: [peerId],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.PEER_OFFER,
        payload,
      },
    });
  },
  [SERVICE_EVENT_TYPE.PEER_ANSWER]: async (meta, payload) => {
    const { socketAdapter } = meta;
    const { sessionId, dst: peerId } = payload;
    const roomId = getRoomIdFromSessionId(sessionId);

    socketAdapter.emit({
      target: {
        roomId,
        users: [peerId],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.PEER_ANSWER,
        payload,
      },
    });
  },
  [SERVICE_EVENT_TYPE.PEER_CANDIDATE]: async (meta, payload) => {
    const { socketAdapter } = meta;
    const { sessionId, dst: peerId } = payload;
    const roomId = getRoomIdFromSessionId(sessionId);

    socketAdapter.emit({
      target: {
        roomId,
        users: [peerId],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.PEER_CANDIDATE,
        payload,
      },
    });
  },
  [SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP]: async (meta, payload) => {
    const {
      socketAdapter,
      redis: { pubClient },
    } = meta;
    const userId = payload.src;
    const { sessionId } = payload;
    const mediaMode = mediaModeDesktop;
    const state = await getSessionState(pubClient, sessionId);

    if (!state) {
      return;
    }

    const { roomId, users } = state;

    // send call request for other users in this session
    users.forEach((peerId) => {
      if (peerId === userId) {
        return;
      }

      socketAdapter.emit({
        target: {
          roomId,
          users: [userId],
        },
        event: SERVICE_EVENT_MESSAGE,
        payload: {
          type: SERVICE_EVENT_TYPE.CALL_REQUEST,
          payload: { userId: peerId, sessionId, mediaMode },
        },
      });
    });
  },
  [SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP_STOP]: async (meta, payload) => {
    const {
      socketAdapter,
      redis: { pubClient },
    } = meta;
    const userId = payload.src;
    const { sessionId } = payload;
    const mediaMode = mediaModeDesktop;
    const state = await getSessionState(pubClient, sessionId);

    if (!state) {
      return;
    }

    const { roomId, users } = state;

    socketAdapter.emit({
      target: {
        roomId,
        users: users.filter((id) => id !== userId),
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP_STOP,
        payload: { userId, sessionId, mediaMode },
      },
    });
  },
  [SERVICE_EVENT_TYPE.CALL_REQUEST]: async (meta, payload) => {
    const { socketAdapter } = meta;
    const { target, ...targetPayload } = payload;

    // redirect to user
    if (!target) {
      // console.log(`[P2P] - can't redirect call request, no target found`);
      return;
    }

    const roomId = getRoomIdFromSessionId(payload.sessionId);

    socketAdapter.emit({
      target: {
        roomId,
        users: [target],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.CALL_REQUEST,
        payload: { ...targetPayload },
      },
    });
  },
  [SERVICE_EVENT_TYPE.PEER_PUBLISH]: async (meta, payload) => {
    const {
      socketAdapter,
      redis: { pubClient },
    } = meta;
    const { userId, sessionId } = payload;
    const state = await getSessionState(pubClient, sessionId);
    const roomId = getRoomIdFromSessionId(sessionId);

    if (!state) {
      // console.log(`[P2P] connect revision for undefined session with id ${sessionId}`);
      return;
    }

    const { mediaMode, except } = payload;
    const existedConnections: ExistedPeersMap = except.reduce(
      (map: ExistedPeersMap, id: string) => ((map[id] = 1), map),
      { [userId]: 1 } as ExistedPeersMap
    );
    console.log('[p2p] call request from publish', userId);
    state.users
      .filter((id) => !existedConnections[id])
      .forEach((peerId) => {
        console.log('[p2p] call request from publish ', userId, 'to', peerId);
        socketAdapter.emit({
          target: {
            roomId,
            users: [userId],
          },
          event: SERVICE_EVENT_MESSAGE,
          payload: {
            type: SERVICE_EVENT_TYPE.CALL_REQUEST,
            payload: { userId: peerId, sessionId, mediaMode },
          },
        });
      });
  },
  [SERVICE_EVENT_TYPE.PEER_OFFER_REDIRECT]: async (meta, payload) => {
    const { socketAdapter } = meta;
    const { sessionId, dst: peerId } = payload;

    const roomId = getRoomIdFromSessionId(sessionId);

    socketAdapter.emit({
      target: {
        roomId,
        users: [peerId],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.PEER_OFFER_REDIRECT,
        payload,
      },
    });
  },
  [SERVICE_EVENT_TYPE.PEER_CONNECTION_FAILED]: async (meta, payload) => {
    const {
      socketAdapter,
      redis: { pubClient },
    } = meta;
    const { src, dst, sessionId, mediaMode } = payload;
    const roomId = getRoomIdFromSessionId(sessionId);
    const state = await getSessionState(pubClient, sessionId);

    if (!state) {
      console.log(`[P2P] attempt to reconnect to a non-existent session with id ${sessionId}`);
      return;
    }

    const users = state.users.filter((id) => id === dst || id === src);

    if (users.length !== 2) {
      console.log(
        `[P2P] can't do reconnection between users ${src} and ${dst}, becouse one of then not in the session (${users.join(
          ','
        )})`
      );
      return;
    }

    console.log('[p2p] connection failed, another try for', src, dst);
    socketAdapter.emit({
      target: {
        roomId,
        users: [dst],
      },
      event: SERVICE_EVENT_MESSAGE,
      payload: {
        type: SERVICE_EVENT_TYPE.CALL_REQUEST,
        payload: { userId: src, sessionId, mediaMode },
      },
    });
  },
};

export default (clientSocket: Socket, appMetaObjects: IServerMetaObjects) => {
  clientSocket.on(SERVICE_EVENT_MESSAGE, async (data: any) => {
    const handler = socketHandlerEnums[data.type];

    if (handler) {
      handler(appMetaObjects, data.payload);
    }
  });
};
