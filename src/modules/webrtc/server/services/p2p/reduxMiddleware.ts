/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';
import IORedis from 'ioredis';
import { btoa, encodeSessionId, decodeSessionId } from './utils';
import { SERVICE_NAME, REDIS_SESSSIONS_KEY } from '../../../common/types/p2p/constants';
import {
  IServerMiddleware,
  IServerMiddlewareContext,
  IServerMiddlewareNext,
} from '../../../../../ioc/server/types/interfaces';
import { webrtcConstants } from '../../../../../ioc/common/ioc-constants';

const API_KEY = 'free-to-use';
const { SERVICE_ACTIONS } = webrtcConstants;

const TOKEN_TIME_TEMPORARTY = 120000; // 5 min in ms
const TOKEN_TIME = 3600000; // 1 hour in ms
const ONE_SECOND = 1000; // 1 sec in ms

const getSessionId = async (context: IServerMiddlewareContext): Promise<string> => {
  const { roomId } = context.action.meta.target;
  const sessionId = encodeSessionId(roomId);

  await context.appMetaObjects.redis.pubClient.hset(
    REDIS_SESSSIONS_KEY,
    sessionId,
    JSON.stringify({
      roomId,
      users: [],
    })
  );

  // todo: use JWS here?
  return sessionId;
};

const generateToken = (sessionId: string, data: any): string => {
  return `${btoa(SERVICE_NAME)}.${btoa(JSON.stringify(data))}`;
};

const getToken = (userId: string, roomId: string, sessionId: string, temporary: boolean = false): string => {
  const expire: number = Date.now() + (temporary ? TOKEN_TIME_TEMPORARTY : TOKEN_TIME);

  return generateToken(sessionId, {
    expireTime: expire / ONE_SECOND,
    roomId,
    userId,
  });
};

const isValidSessionId = (sessionId: string) => {
  const sessionInfo = decodeSessionId(sessionId);

  return sessionInfo ? sessionInfo.serviceName === SERVICE_NAME : false;
};

const isSessionExisted = async (pubClient: IORedis.Redis, sessionId: string): Promise<boolean> => {
  const sessionState = await pubClient.hget(REDIS_SESSSIONS_KEY, sessionId);

  return !!sessionState;
};

const middlewareEnum: {
  [key: string]: IServerMiddleware;
} = {
  [SERVICE_ACTIONS.START_SESSION]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    next();

    const { action, appMetaObjects } = context;

    const session = {
      sessionId: await getSessionId(context),
      serviceName: SERVICE_NAME,
    };

    appMetaObjects.dispatchAction(
      {
        ...action,
        ...session,
        type: SERVICE_ACTIONS.SESSION_STARTED,
        meta: {
          ...action.meta,
          session,
        },
      },
      _noop
    );
  },
  [SERVICE_ACTIONS.END_SESSION]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    next();

    const { action, appMetaObjects } = context;
    const { redis, logger } = appMetaObjects;
    const { sessionId, timeSessionClosed } = action;
    const hasSessionWithId = await isSessionExisted(appMetaObjects.redis.pubClient, sessionId);

    if (hasSessionWithId) {
      await redis.pubClient.hdel(REDIS_SESSSIONS_KEY, context.action.sessionId);
    }

    context.appMetaObjects.dispatchAction(
      {
        ...context.action,
        type: SERVICE_ACTIONS.SESSION_ENDED,
      },
      _noop
    );

    logger.webrtcP2p.info(`Close session for webrtc service name ${SERVICE_NAME}`, {
      sessionId,
      timeSessionClosed,
    });
  },
  [SERVICE_ACTIONS.GET_PARAMS]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    const { user, temporary } = context.action;
    let { sessionId } = context.action;
    let token: string = '';

    if (temporary) {
      sessionId = getSessionId(context);
    }

    context.action.type = SERVICE_ACTIONS.SET_PARAMS;
    context.action.meta.target.users = [user.id];

    // in case sessionId from other service,
    // return empty token
    if (temporary || isValidSessionId(sessionId)) {
      token = getToken(user.id, user.roomId, sessionId, temporary);
    }

    context.action.params = {
      apiKey: API_KEY,
      sessionId,
      token,
    };

    next();
  },
};

const serverMiddleware: IServerMiddleware = async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
  if (context.action.serviceName === SERVICE_NAME && middlewareEnum[context.action.type]) {
    middlewareEnum[context.action.type](context, next);
  } else {
    next();
  }
};

export default serverMiddleware;
