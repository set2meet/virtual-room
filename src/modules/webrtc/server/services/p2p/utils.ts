/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { SERVICE_NAME } from '../../../common/types/p2p/constants';

const SESSION_ID_SPLITTER = ':';

type SessionIdInfo = {
  roomId: string;
  serviceName: string;
  timeStart: number;
};

export const now = () => Date.now();

export const btoa = (value: string): string => {
  return new Buffer(value).toString('base64');
};

export const atob = (value: string): string => {
  return new Buffer(value, 'base64').toString();
};

export const encodeSessionId = (roomId: string): string => {
  return btoa([SERVICE_NAME, roomId, now()].join(SESSION_ID_SPLITTER));
};

export const decodeSessionId = (sessionId: string): SessionIdInfo => {
  let decodedSession: string;

  try {
    decodedSession = atob(sessionId);
  } catch (err) {
    return null;
  }

  if (!decodedSession) {
    return null;
  }

  const decodedDataArr = decodedSession.split(SESSION_ID_SPLITTER);

  // tslint:disable-next-line
  if (decodedDataArr.length !== 3) {
    return null;
  }

  return {
    serviceName: decodedDataArr[0],
    roomId: decodedDataArr[1],
    timeStart: parseInt(decodedDataArr[2], 10),
  };
};
