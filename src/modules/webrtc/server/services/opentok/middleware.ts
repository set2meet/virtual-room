/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import OpenTok from 'opentok';
import _noop from 'lodash/noop';
import { WebrtcServiceType } from '../../../common/types/WebrtcServiceType';
import {
  IServerMiddleware,
  IServerMiddlewareContext,
  IServerMiddlewareNext,
} from '../../../../../ioc/server/types/interfaces';
import { webrtcConstants } from '../../../../../ioc/common/ioc-constants';
import { ITokBoxParams } from './ITokBoxParams';
import serverEntityProvider from '../../../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { LogErrorCode } from '../../../../../ioc/server/ioc-constants';

const { SERVICE_ACTIONS } = webrtcConstants;
const TOKEN_TIME_TEMPORARTY = 120000; // 5 min in ms
const TOKEN_TIME = 3600000; // 1 hour in ms
const ONE_SECOND = 1000; // 1 sec in ms

// @TODO: cached instance should be managed inside composition root, not import-level execution
let openTokSDKCache = null;

const getOpenTokSDK = () => {
  if (!openTokSDKCache) {
    const tokboxParams: ITokBoxParams = serverEntityProvider.getConfig().webrtc.tokbox;
    const API_KEY = tokboxParams.API_KEY;
    const API_SECRET = tokboxParams.API_SECRET;

    // create opentok sdk object
    openTokSDKCache = new OpenTok(API_KEY, API_SECRET);
  }

  return openTokSDKCache;
};

// return existed token (if not expired) or create new one
const getToken = (userId: string, sessionId: string, temporary: boolean = false): Promise<string> =>
  new Promise(async (res) => {
    const expire: number = Date.now() + (temporary ? TOKEN_TIME_TEMPORARTY : TOKEN_TIME);
    let value: string = '';

    // in case sessionId from other service,
    // return empty token
    try {
      value = getOpenTokSDK().generateToken(sessionId, {
        expireTime: expire / ONE_SECOND,
        data: `userId=${userId}`,
      });
    } catch (err) {
      // nothing todo;
    }

    res(value);
  });

const getSessionId = (context: IServerMiddlewareContext): Promise<string> =>
  new Promise(async (res) => {
    getOpenTokSDK().createSession({ mediaMode: 'routed' }, async (error, session) => {
      if (error) {
        context.appMetaObjects.logger.webrtcOpentok.error(LogErrorCode.MODULE, error.message, {
          error,
          descr: '[webrtc/server/opentok] Can not create session id',
        });
        res(undefined);
      } else {
        res(session.sessionId);
      }
    });
  });

const middlewareEnum: {
  [key: string]: IServerMiddleware;
} = {
  [SERVICE_ACTIONS.START_SESSION]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    next();

    const { action, appMetaObjects } = context;

    const session = {
      sessionId: await getSessionId(context),
      serviceName: WebrtcServiceType.OPENTOK,
    };

    appMetaObjects.dispatchAction(
      {
        ...action,
        ...session,
        type: SERVICE_ACTIONS.SESSION_STARTED,
        meta: {
          ...action.meta,
          session,
        },
      },
      _noop
    );
  },
  [SERVICE_ACTIONS.END_SESSION]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    next();

    const { action, appMetaObjects } = context;
    const { logger } = appMetaObjects;
    const { sessionId, timeSessionClosed } = action;

    context.appMetaObjects.dispatchAction(
      {
        ...context.action,
        type: SERVICE_ACTIONS.SESSION_ENDED,
      },
      _noop
    );

    logger.webrtcOpentok.info(`Close session for the ${WebrtcServiceType.OPENTOK} webrtc service`, {
      sessionId,
      service: WebrtcServiceType.OPENTOK,
      timeSessionClosed,
    });
  },
  [SERVICE_ACTIONS.GET_PARAMS]: async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    const { user, temporary } = context.action;
    let { sessionId } = context.action;

    if (temporary) {
      sessionId = await getSessionId(context);
    }

    const token = await getToken(user.id, sessionId, temporary);
    const API_KEY: string = serverEntityProvider.getConfig().webrtc.tokbox.API_KEY;

    context.action.type = SERVICE_ACTIONS.SET_PARAMS;
    context.action.meta.target.users = [user.id];
    context.action.params = {
      apiKey: API_KEY,
      sessionId,
      token,
    };

    next();
  },
};

// TODO middleware must be in webphone and call api service methods
const serverMiddleware: IServerMiddleware = async (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
  if (context.action.serviceName === WebrtcServiceType.OPENTOK && middlewareEnum[context.action.type]) {
    middlewareEnum[context.action.type](context, next);
  } else {
    next();
  }
};

export default serverMiddleware;
