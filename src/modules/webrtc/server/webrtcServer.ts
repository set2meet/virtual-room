/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import registerP2PSocketEvents from './services/p2p/socketEvents';
import webrtcP2PServerMiddleware from './services/p2p/reduxMiddleware';
import webrtcOpentokServerMiddleware from './services/opentok/middleware';

export { registerP2PSocketEvents, webrtcOpentokServerMiddleware, webrtcP2PServerMiddleware };
