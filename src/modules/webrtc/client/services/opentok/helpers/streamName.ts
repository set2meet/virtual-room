/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/*
 * All stream names encode as <userId:devices|desktop:datetimecreation>
 * Example: stream from jonh-smith from his screen will have name: "jonh-smith:desktop:1568388191543"
 *
 * Date time creation is very important - the events from opentok may be in wrong sequence
 * Example: user change his camera, but sequence of events will be
 *   1) stream created "jonh-smith:devices:1568388317077"
 *   2) stream destroyd "jonh-smith:devices:1568388297455"
 * and in the model the final stream may be removed at all. So to avoid this error marks each stream with date time
 */
const SPLITTER = ':';

export type StreamMediaType = 'devices' | 'desktop';

export type ParsedStreamName = {
  userId: string;
  type: StreamMediaType;
  date: number;
};

export const decodeStreamName = (name: string): ParsedStreamName => {
  const data = name.split(SPLITTER);

  return {
    userId: data[0],
    date: parseInt(data[2], 10),
    type: data[1] as StreamMediaType,
  };
};

export const encodeStreamName = (userId: string, type: StreamMediaType): string => {
  return [userId, type, Date.now()].join(SPLITTER);
};
