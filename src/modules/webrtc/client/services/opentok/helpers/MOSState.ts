/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import {
  calcBitRate,
  calculateMOS,
  calcTotalPackets,
  getPacketsLost,
  bitrateByDimention,
  transmissionRatingFactor,
} from './utils';
import { detect, BrowserInfo } from 'detect-browser';

const MAX_SCORE = 5;
const MIN_VIDEO_BITRATE = 30000;
const browser: BrowserInfo = detect() as BrowserInfo;
const isEDGE = browser.name === 'edge';

type SS = OT.SubscriberStats;
type TS = OT.IncomingTrackStats;

const nthRecord = <T>(list: T[], n: number): T | undefined => {
  return n <= 0 ? list[list.length + n - 1] : list[n];
};

const calcVideoScore = (lastStats: TS, currStats: TS, stream: OT.Stream, interval: number): number => {
  if (!(lastStats && currStats)) {
    return 1; // min mos value
  }

  const sizeBitrate = bitrateByDimention(stream.videoDimensions);
  const baseBitrate = calcBitRate(currStats, lastStats, interval);

  if (baseBitrate < MIN_VIDEO_BITRATE) {
    return 1;
  }

  const bitrate = Math.min(baseBitrate, sizeBitrate);

  /* tslint:disable */
  return Math.min(
    MAX_SCORE,
    (Math.log(bitrate / MIN_VIDEO_BITRATE) / Math.log(sizeBitrate / MIN_VIDEO_BITRATE)) * 4 + 1
  );
  /* tslint:enable */
};

const calcAudioScore = (lastStats: TS, currStats: TS, stream: OT.Stream): number => {
  if (!currStats || !lastStats || !stream) {
    return 0;
  }

  const totalAudioPackets = calcTotalPackets(currStats, lastStats);

  if (totalAudioPackets === 0) {
    return 0;
  }

  const packetLossRatio = (getPacketsLost(currStats) - getPacketsLost(lastStats)) / totalAudioPackets;

  return calculateMOS(transmissionRatingFactor(packetLossRatio));
};

export default class MOSState {
  private stats: SS[] = [];

  public push(stats: SS, stream: OT.Stream) {
    this.stats.push(stats);

    if (this.stats.length < 2) {
      return;
    }

    const { hasVideo } = stream;
    const currStats = nthRecord(this.stats, 0);
    const lastStats = nthRecord(this.stats, -1);
    const interval = currStats.timestamp - lastStats.timestamp;
    const audioScore = calcAudioScore(lastStats.audio, currStats.audio, stream);
    const videoScore = hasVideo ? calcVideoScore(lastStats.video, currStats.video, stream, interval) : NaN;

    // edge want give recieved bytes stats, ignore video stats for edge
    if (hasVideo && !isEDGE) {
      return Math.round((audioScore + videoScore) / 2);
    }

    return audioScore;
  }
}
