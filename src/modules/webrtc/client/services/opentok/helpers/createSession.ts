/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as OT from '@opentok/client';

type WebRTCOpentokCredentials = {
  apiKey: string;
  sessionId: string;
  token: string;
};
type WebRTCOpentokCreateSession = (credentials: WebRTCOpentokCredentials) => Promise<OT.Session>;

const createSession: WebRTCOpentokCreateSession = (credentials) =>
  new Promise((resolve, reject) => {
    const { apiKey, sessionId, token } = credentials;

    const session = OT.initSession(apiKey, sessionId);

    session.once('sessionConnected', () => {
      resolve(session);
    });

    session.connect(token, (err: OT.OTError) => {
      if (err) {
        reject();
      }
    });
  });

export default createSession;
