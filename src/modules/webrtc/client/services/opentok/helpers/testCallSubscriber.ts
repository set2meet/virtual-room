/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as OT from '@opentok/client';
import { WebRTCOpentokTestSubscriber } from './testCall';
import { IWebrtcTestCallConfig } from '../../../../../../ioc/common/ioc-interfaces';

type OTTestCallState = {
  session: OT.Session;
  publisher: OT.Publisher;
  subscriber: OT.Subscriber;
};
type StreamCreatedEvent = OT.Event<'streamCreated', OT.Publisher> & { stream: OT.Stream };
type WebRTCOpentokCreateTestSubscriber = (
  session: OT.Session,
  config: IWebrtcTestCallConfig
) => Promise<WebRTCOpentokTestSubscriber>;

const subscriberDefaultParams: OT.SubscriberProperties = {
  insertDefaultUI: false,
  testNetwork: true,
};

const buildStopMethod = (state: OTTestCallState) => () => {
  const { session, publisher, subscriber } = state;

  if (subscriber) {
    state.session.unsubscribe(subscriber);
  }
  if (publisher) {
    session.unpublish(publisher);
  }

  session.disconnect();
};

const createTestSubscriber: WebRTCOpentokCreateTestSubscriber = (session, config) =>
  new Promise((resolve, reject) => {
    const { audioOnly } = config;
    const state: OTTestCallState = {
      session,
      publisher: null,
      subscriber: null,
    };

    const stop = buildStopMethod(state);

    const publisherOptions: OT.PublisherProperties = {
      name: 'test-call-publisher',
      insertDefaultUI: false,
      showControls: false,
      publishAudio: true,
      publishVideo: audioOnly ? false : true,
      audioSource: config.audioTrack,
      videoSource: config.videoTrack,
    };

    state.publisher = OT.initPublisher('publisher', publisherOptions, (createErr: OT.OTError) => {
      if (createErr) {
        return reject();
      }

      session.publish(state.publisher, (publishError?: OT.OTError) => {
        if (publishError) {
          return reject();
        }
      });
    });

    state.publisher.once('streamCreated', (event: StreamCreatedEvent) => {
      state.subscriber = session.subscribe(
        event.stream,
        undefined,
        subscriberDefaultParams,
        (subscribeError?: OT.OTError) => {
          return subscribeError
            ? reject()
            : resolve({
                subscriber: state.subscriber,
                stop,
              });
        }
      );
    });
  });

export default createTestSubscriber;
