/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IWebrtcService } from '../../../../common/types/IWebrtcService';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { LogErrorCode } from '../../../../../logger/common/LogErrorCode';

const importExternal = <Lib>(url: string, globalKey: string = ''): Promise<Lib | undefined> => {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');

    script.src = url;
    script.async = true;
    script.onload = () => resolve(window[globalKey]);
    script.onerror = reject;

    document.body.appendChild(script);
  });
};

export const buildOpenTokService = async (): Promise<IWebrtcService> => {
  const opentokBundleURL = clientEntityProvider.getConfig().webrtc?.tokbox?.clientBundle;
  try {
    if (opentokBundleURL) {
      if (!window.OT) {
        await importExternal(opentokBundleURL);
      }
      const WebrtcServiceOpentok = (await import('../OpenTokService')).default;
      return new WebrtcServiceOpentok();
    }
  } catch (error) {
    clientEntityProvider
      .getLogger()
      .webrtcOpentok.error(
        LogErrorCode.MODULE,
        `Can't load opentok bundle, make sure you specified correct path in config.`,
        error
      );
  }

  return null;
};
