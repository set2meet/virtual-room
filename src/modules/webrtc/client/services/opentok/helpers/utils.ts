/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
type TS = OT.IncomingTrackStats;
type VideoSize = {
  width: number;
  height: number;
};

const ONE_SEC = 1000;
const h = (x: number): number => (x < 0 ? 0 : 1);

export const getPacketsLost = (ts: TS): number => ts.packetsLost || 0;

export const getPacketsReceived = (ts: TS): number => ts.packetsReceived || 0;

export const getTotalPackets = (ts: TS): number => getPacketsLost(ts) + getPacketsReceived(ts);

export const calcTotalPackets = (curr: TS, last: TS): number => getTotalPackets(curr) - getTotalPackets(last);

export const calcRecievedPackets = (curr: TS, last: TS): number => getPacketsReceived(curr) - getPacketsReceived(last);

export const calcRecievedBytes = (curr: TS, last: TS): number => curr.bytesReceived - last.bytesReceived;

export const getPacketsLostDiff = (curr: TS, last: TS): number => getPacketsLost(curr) - getPacketsLost(last);

/* tslint:disable */

export const calcBitRate = (curr: TS, last: TS, interval: number): number =>
  (8 * calcRecievedBytes(curr, last)) / (interval / ONE_SEC) || 0;

export const bitrateByDimention = (size: VideoSize) => {
  // power function maps resolution to target bitrate, based on rumor config
  // values, with r^2 = 0.98. We're ignoring frame rate, assume 30.
  return 10 ** (2.069924867 * Math.log10(size.width * size.height) ** 0.6250223771);
};

// Calculate the transmission rating factor, R
export const transmissionRatingFactor = (packetLossRatio: number): number => {
  const d = 1000 / 24;
  const a = 0; // ILBC: a=10
  const b = 19.8;
  const c = 29.7;
  const delayImpairment = (0.024 * d + 0.11) * (d - 177.3) * h(d - 177.3);
  const equipmentImpairment = (a + b) * Math.log(1 + c * packetLossRatio);

  return 94.2 - delayImpairment - equipmentImpairment;
};

// Calculate the Mean Opinion Score based on R
export const calculateMOS = (R: number): number => {
  if (R < 0) {
    return 1;
  }
  if (R > 100) {
    return 4.5;
  }

  return 1 + 0.035 * R + (7.1 / 1000000) * R * (R - 60) * (100 - R);
};

/* tslint:enable */
