/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import MOSState from './MOSState';
import { WebRTCOpentokTestSubscriber } from './testCall';
import { IWebrtcTestCallConfig } from '../../../../common/types/IWebrtcService';

const MAX_MOS_LOG_LEN = 10;

const sum = (arr: number[]) => arr.reduce((v, i) => v + i, 0);

const getAverageMosValue = (logs: number[]): number => {
  const list = logs.slice(-MAX_MOS_LOG_LEN);

  return Math.round(sum(list) / list.length);
};

const collectTestCallStats = (call: WebRTCOpentokTestSubscriber, config: IWebrtcTestCallConfig) => {
  const mosState = new MOSState();
  const { subscriber } = call;
  const mosLog: number[] = [];

  const timer: number = window.setInterval(() => {
    subscriber.getStats((error?: OT.OTError, stats?: OT.SubscriberStats) => {
      if (!stats) {
        return;
      }

      const mos = mosState.push(stats, subscriber.stream);

      if (mos) {
        mosLog.push(mos);
      }
    });
  }, config.interval / 2);

  return {
    stop: () => {
      call.stop();
      window.clearInterval(timer);
      return getAverageMosValue(mosLog);
    },
  };
};

export default collectTestCallStats;
