/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _throttle from 'lodash/throttle';
import _transform from 'lodash/transform';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import EventEmitter from 'events';
import * as OT from '@opentok/client';
import {
  IWebrtcService,
  IWebrtcTestCallConfig,
  IWebrtcServiceAccessParams,
} from '../../../../../ioc/common/ioc-interfaces';

import createSession from './helpers/createSession';
import createTestSubscriber from './helpers/testCallSubscriber';
import collectTestCallStats from './helpers/testCallStatsCollector';
import { WebRTCOpentokTestCall } from './helpers/testCall';
import { encodeStreamName, decodeStreamName } from './helpers/streamName';
import { WebrtcServiceType } from '../../../common/types/WebrtcServiceType';
import { webrtcConstants } from '../../../../../ioc/common/ioc-constants';
import { LogErrorCode } from '../../../../../ioc/client/ioc-constants';

const PUBLISHER_READY_TOGGLE_STREAMS = 'publisher-ready:toggle-streams-from-devices';

type PublisherStatus = 'none' | 'try-publishing' | 'ready';
type MediaStreamMap = Record<string, MediaStream>;

const MediaTrackKind: Record<number, Record<string, string>> = {
  [0]: {
    audio: 'mic',
    video: 'cam',
  },
  [1]: {
    video: 'desktop',
  },
};

/** --- interfaces --- */

interface IOTWebrtcParticipantData {
  stream: OT.Stream;
  subscriber: OT.Subscriber | OT.Publisher;
  voiceActivity: {
    isTalking: boolean;
    timestamp: number;
  } | null;
}

interface IOTWebrtcParticipantsMap {
  [key: string]: IOTWebrtcParticipantData;
}

interface IWebrtcStreamInfo {
  name: string;
  publishAudio: boolean;
  publishVideo: boolean;
  audioSource?: MediaStreamTrack;
  videoSource?: MediaStreamTrack;
  desktopSource?: MediaStreamTrack;
}

type IMediaTrackName = 'audioSource' | 'videoSource';

interface IMessageSubscribers {
  type: string;
  callback: (data: string) => void;
}

/** --- constants --- */

const WS_PREFIX = '[Webrtc Opentok Service]';
const { SERVICE_EVENTS } = webrtcConstants;
const SUBSCRIBER_DEFAULT_PARAMS = {
  insertDefaultUI: false,
};
const PUBLISHER_CHANGE_PUBLISH_PROPS_TIME = 500;
const TEST_CALL_FORCE_CANCEL = 'TEST_CALL_FORCE_CANCEL';

// Undocumented OpenTok feature. May be broken after OT update
const getNativeStreamFromSubscriber = (subscriber: OT.Subscriber | OT.Publisher): MediaStream | null => {
  if (!subscriber) {
    return null;
  }
  return (subscriber as any)._.webRtcStream();
};

/** --- service --- */

class OpenTokService extends EventEmitter implements IWebrtcService {
  public name = WebrtcServiceType.OPENTOK;

  private session: OT.Session;
  private publisher: OT.Publisher;
  private publisherStatus: PublisherStatus = 'none';
  private publisherDesktop: OT.Publisher;
  private isConnected: boolean;
  private streamInfo: IWebrtcStreamInfo = {
    name: undefined,
    publishAudio: false,
    publishVideo: false,
    audioSource: undefined,
    videoSource: undefined,
    desktopSource: undefined,
  };
  private participants: IOTWebrtcParticipantsMap = {};
  private subscribedEvents: string[] = [];
  private messageSubscribersQueue: IMessageSubscribers[] = [];
  private testCall: WebRTCOpentokTestCall;

  private logger = clientEntityProvider.getLogger().webrtcOpentok;

  private handlerPublisherStreamCreated = (
    evt: OT.Event<'streamCreated', OT.Publisher> & {
      stream: OT.Stream;
    }
  ) => {
    this.createParticipant(evt.stream, this.publisher);

    this.publisherStatus = 'ready';
    this.emit(PUBLISHER_READY_TOGGLE_STREAMS);
  };

  private handlerSessionConnected = () => {
    this.logger.info('Connect to session was success');
    this.emit(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS);
    window.console.log(`${WS_PREFIX} session connected`);
  };

  private handlerSessionDisconnected = (
    evt: OT.Event<'sessionDisconnected', OT.Session> & {
      reason: string;
      target: OT.Session;
    }
  ) => {
    this.logger.info('Disconnected from session');
    window.console.log(`${WS_PREFIX} session disconnected`, evt.reason);
    // "clientDisconnected"

    this.isConnected = false;
    this.sessionRemoveAllListeners();
    this.emit(SERVICE_EVENTS.SESSION_DISCONNECTED);
    this.session = null;
  };

  private handlerSessionConnectedSuccessfully = () => {
    this.isConnected = true;
  };

  private sessionPublishStream = (source: OT.Publisher) => {
    this.session.publish(source, (err) => {
      if (err) {
        // todo: dispatch correct action
        window.console.log(`${WS_PREFIX} Session publish target error.\n`, err);
      } else {
        const stream = getNativeStreamFromSubscriber(source);

        if (stream) {
          const { userId } = decodeStreamName(source.stream.name);

          this.emit(SERVICE_EVENTS.USER_STREAM_ADDED, userId, stream);
        }
      }
    });
  };

  private sessionPublish = (target?: OT.Publisher) => {
    if (target) {
      // publish only target
      this.sessionPublishStream(target);
    } else {
      // publish all existed streams from self
      if (this.publisherDesktop) {
        this.sessionPublishStream(this.publisherDesktop);
      }
      if (this.publisher) {
        this.sessionPublishStream(this.publisher);
      }
    }
  };

  private sessionUnpublish = (target?: OT.Publisher) => {
    if (target) {
      // unpublish target only
      const stream = getNativeStreamFromSubscriber(target);

      this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, stream);
      this.session.unpublish(target);
    } else {
      // unpublish all
      this.unpublishDesktop();
      const stream = getNativeStreamFromSubscriber(target);

      this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, stream);
      this.session.unpublish(this.publisher);
    }
  };

  private _sessionPublishDesktop(): void {
    this.session.publish(this.publisherDesktop, (err: any) => {
      if (err) {
        window.console.log(`${WS_PREFIX} Session publish desktop error.\n`, err);
      } else {
        const track = this.streamInfo.desktopSource;

        this.logger.info('Publish desktop stream', {
          trackId: track.id,
          trackEnabled: track.enabled,
          trackMuted: track.muted,
        });
      }
    });
  }

  private getMediaTrack = (trackName: IMediaTrackName): MediaStreamTrack | boolean => {
    return this.streamInfo[trackName];
  };

  private publisherCreate = () => {
    if (!this.streamInfo) {
      // no stream info
      // for example no permissions granted to use media devices
      return;
    }

    this.publisherStatus = 'try-publishing';
    this.publisher = OT.initPublisher(
      'publisher',
      {
        insertDefaultUI: false,
        publishAudio: this.streamInfo.publishAudio,
        publishVideo: this.streamInfo.publishVideo,
        audioSource: this.getMediaTrack('audioSource'),
        videoSource: this.getMediaTrack('videoSource'),
        name: encodeStreamName(this.streamInfo.name, 'devices'),
      },
      (err: OT.OTError) => {
        if (err) {
          this.emit(SERVICE_EVENTS.PARTICIPANT_CREATE_ERROR, err);
        }
      }
    );

    this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, {
      userId: this.streamInfo.name,
      tracks: [{ kind: 'cam', track: this.streamInfo.videoSource }],
    });

    this.publisher.on('streamCreated', this.handlerPublisherStreamCreated);
  };

  private publisherDestroy = () => {
    if (this.publisher) {
      if (this.publisher.stream) {
        this.participants[this.streamInfo.name] = null;
      }
      this.publisher.off('streamCreated', this.handlerPublisherStreamCreated);
      this.publisher.destroy();
    }

    this.publisher = null;
  };

  private handlerSessionStreamCreated = (
    evt: OT.Event<'streamCreated', OT.Session> & {
      stream: OT.Stream;
    }
  ) => {
    console.log('[OPENTOK] new stream created', decodeStreamName(evt.stream.name));
    this.createParticipant(evt.stream);
  };

  private handlerSessionStreamDestroyed = (
    evt: OT.Event<'streamDestroyed', OT.Session> & {
      stream: OT.Stream;
      reason: string;
    }
  ) => {
    console.log('[OPENTOK] stream removed', decodeStreamName(evt.stream.name));
    const stream = evt.stream;
    const { userId, type } = decodeStreamName(stream.name);

    const participant = this.participants[userId];

    // check: participant for destroyed stream exists
    if (!participant) {
      return; // unkonown stream was destroyed ???
    }

    // check: current participant stream is the same as destroyed
    if (participant.stream && participant.stream.name !== stream.name) {
      return; // incorrect sequence ot opentok events (created -> destroyed previous)
    }

    // cleare info about this participant
    this.participants[userId] = undefined;

    // tell that natie stream was destroyed
    if (participant.subscriber) {
      const nativeStream = getNativeStreamFromSubscriber(participant.subscriber);

      this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, nativeStream);
    }

    this.logger.info('Peer connection with user was destroyed', { userId });

    if (participant.stream && userId !== this.streamInfo.name) {
      const tracks: any =
        type === 'desktop'
          ? [{ kind: 'desktop', track: null }]
          : [
              { kind: 'cam', track: null },
              { kind: 'mic', track: null },
            ];

      this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, { userId, tracks });
    }
  };

  private subscribeOnSessionMessages = (type: string, callback: (data: string) => void) => {
    (this.session as any).on(`signal:${type}`, (evt: any) => {
      const { data } = evt;

      if (data && data.length) {
        callback(data);
      }
    });
  };

  private republishPublisher = () => {
    // destroy publisher
    if (this.publisher) {
      // unpublish one from the session
      this.sessionUnpublish(this.publisher);
      // destroy all publisher;
      this.publisherDestroy();
    }

    // create new publisher
    this.publisherCreate();
    // pubslish new publisher
    this.sessionPublish(this.publisher);
  };

  private sessionRemoveAllListeners() {
    this.session.off('sessionConnected', this.handlerSessionConnected);
    this.session.off('streamCreated', this.handlerSessionStreamCreated);
    this.session.off('streamDestroyed', this.handlerSessionStreamDestroyed);
    this.session.off('sessionDisconnected', this.handlerSessionDisconnected);
  }

  constructor() {
    super();

    this.on(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS, this.handlerSessionConnectedSuccessfully);
  }

  public subscribe(event: string, listener: (...args: any[]) => void) {
    super.on(event, listener);

    if (this.subscribedEvents.indexOf(event) === -1) {
      this.subscribedEvents.push(event);
    }
  }

  public unsubscribe() {
    this.subscribedEvents.forEach((event: string) => {
      this.removeAllListeners(event);
    });
    this.subscribedEvents.length = 0;
  }

  public connect(apiKey: string, sessionId: string, token: string) {
    this.session = OT.initSession(apiKey, sessionId);

    this.session.on('sessionConnected', this.handlerSessionConnected);
    this.session.on('streamCreated', this.handlerSessionStreamCreated);
    this.session.on('streamDestroyed', this.handlerSessionStreamDestroyed);
    this.session.on('sessionDisconnected', this.handlerSessionDisconnected);
    this.session.once('sessionReconnecting', () => {
      this.logger.info('Connection with media server lost');
      this.emit(SERVICE_EVENTS.SESSION_CONNECTION_LOST);
    });
    this.session.once('sessionReconnected', () => {
      this.logger.info('Connection with media server restored');
      this.emit(SERVICE_EVENTS.SESSION_CONNECTION_RESTORED);
    });

    this.messageSubscribersQueue.forEach((rec: IMessageSubscribers) => {
      this.subscribeOnSessionMessages(rec.type, rec.callback);
    });
    this.messageSubscribersQueue.length = 0;

    this.session.connect(token, (err: OT.OTError) => {
      if (err) {
        this.emit(SERVICE_EVENTS.SESSION_CONNECT_ERROR, err);
        this.logger.error(LogErrorCode.MODULE, 'Connect to session was failed', {
          errorName: err.name,
          errorMessage: err.message,
        });
      }
    });

    this.logger.info('Try connect to session');
  }

  public disconnect() {
    // disconnect from opentok
    if (this.session) {
      this.sessionUnpublish();
      this.session.disconnect();
    } else {
      this.emit(SERVICE_EVENTS.SESSION_DISCONNECTED);
    }
    // set publish media to false by default
    if (this.streamInfo) {
      this.streamInfo.publishAudio = false;
      this.streamInfo.publishVideo = false;
    }
  }

  public reconnect(apiKey: string, sessionId: string, token: string) {
    const onSessionEnd = new Promise<void>((res) => {
      // disconnect previous active session
      if (this.session) {
        // remove all listeners from current session
        this.sessionRemoveAllListeners();

        // unpublish all targets
        if (this.publisher) {
          this.session.unpublish(this.publisher);
        }
        if (this.publisherDesktop) {
          this.session.unpublish(this.publisherDesktop);
        }

        // disconnect
        this.session.once('sessionDisconnected', () => res());

        try {
          this.session.disconnect();
        } catch (err) {
          // nothing todo
          // on win 10 under the chrome
          // in builded version of app
          // an error is generated
          // about wrong session status transition,
          // just use try/catch to avoid this mysterious error
        }
      } else {
        res();
      }
    });

    onSessionEnd.then(() => {
      this.logger.info('Try to reconnect');
      this.connect(apiKey, sessionId, token);
    });
  }

  public pusblishStreams(name: string, audioTrack?: MediaStreamTrack, videoTrack?: MediaStreamTrack) {
    if (!this.publisher) {
      return;
    }

    if (this.isConnected) {
      this.streamInfo.audioSource = audioTrack;
      this.streamInfo.videoSource = videoTrack;
      this.republishPublisher();
    }
  }

  public publisherUpdateStream(
    name: string,
    audioSource: MediaStreamTrack | null,
    videoSource: MediaStreamTrack | null,
    publishAudio?: boolean,
    publishVideo?: boolean
  ) {
    this.logger.debug('Publish stream', {
      audioSource: audioSource
        ? ` audio track ${audioSource.id} (muted=${audioSource.muted} state=${audioSource.readyState})`
        : '',
      videoSource: videoSource
        ? ` video track ${videoSource.id} (muted=${videoSource.muted} state=${videoSource.readyState})`
        : '',
      audioPassedMuteStates: publishAudio || this.streamInfo.publishAudio,
      videoPassedMuteStates: publishVideo || this.streamInfo.publishVideo,
    });

    // update stream info
    this.streamInfo.name = name;
    this.streamInfo.audioSource = audioSource;
    this.streamInfo.videoSource = videoSource;
    if (publishAudio !== undefined) {
      this.streamInfo.publishAudio = publishAudio;
    }
    if (publishVideo !== undefined) {
      this.streamInfo.publishVideo = publishVideo;
    }

    // update publisher, if connected
    if (this.isConnected) {
      if (this.publisherStatus === 'try-publishing') {
        this.removeListener(PUBLISHER_READY_TOGGLE_STREAMS, this.republishPublisher);
        this.once(PUBLISHER_READY_TOGGLE_STREAMS, this.republishPublisher);
      } else {
        this.republishPublisher();
      }
    } else {
      // we have to send a message for participants from host about reconnect for updating mediaElements
      this.republishPublisher();
    }
  }

  private createParticipant(stream: OT.Stream, publisher?: OT.Publisher): void {
    const { userId, type } = decodeStreamName(stream.name);
    const isDesktopStream = type === 'desktop';

    this.logger.debug(`Peer connected`, { userId, type });

    const subscriber: OT.Subscriber | OT.Publisher =
      publisher ||
      this.session.subscribe(stream, undefined, SUBSCRIBER_DEFAULT_PARAMS, (err) => {
        if (err) {
          // todo: dispatch correct action
          window.console.log(`${WS_PREFIX} Can\'t create subscriber.\n`, err);
        }
        // very important condition - opentok create subscriber from publisher by self
        // and generate event with dead stream!
        else if (userId !== this.streamInfo.name) {
          const ns = getNativeStreamFromSubscriber(subscriber);
          const tracks = ns.getTracks().map((track) => ({
            track,
            kind: MediaTrackKind[+isDesktopStream][track.kind],
          }));

          // if track ends - just log it for analise
          tracks.forEach((rec) => {
            rec.track.addEventListener('ended', () => {
              this.logger.debug('Peer track from stream ended', {
                userId,
                id: ns.id,
                isActive: ns.active,
                isFromDesktop: isDesktopStream,
                track: {
                  kind: rec.track.kind,
                  id: rec.track.id,
                  readyState: rec.track.readyState,
                  enabled: rec.track.enabled,
                },
              });
            });
          });

          this.emit(SERVICE_EVENTS.USER_STREAM_ADDED, userId, ns);
          this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, { userId, tracks });

          const at = ns ? ns.getAudioTracks()[0] : null;
          const vt = ns ? ns.getVideoTracks()[0] : null;

          this.logger.debug('Peer added stream', {
            userId,
            stream: {
              id: ns.id,
              active: ns.active,
              isDesktop: isDesktopStream,
            },
            audiotrack: at
              ? {
                  id: at.id,
                  enabled: at.enabled,
                  muted: at.muted,
                  state: at.readyState,
                }
              : null,
            audioTrack: {
              id: vt.id,
              enabled: vt.enabled,
              muted: vt.muted,
              state: vt.readyState,
            },
          });
        }
      });

    // store stream in private model
    // todo: rename it to subscribers(participants) media info
    this.participants[userId] = {
      stream,
      subscriber,
      voiceActivity: null,
    };
  }

  private changePublisherPublishProps = _throttle(
    // Firefox/Safari: if very quickly turn on the microphone and then the camera,
    //                 the video stream from camera didn't bind to common stream
    (type: 'audio' | 'video', value: boolean) => {
      if (type === 'audio') {
        this.publisher.publishAudio(value);
      } else {
        this.publisher.publishVideo(value);
      }
    },
    PUBLISHER_CHANGE_PUBLISH_PROPS_TIME
  );

  private _toggleAudioStream = () => {
    this.changePublisherPublishProps('audio', this.streamInfo.publishAudio);
  };

  public toggleAudioStream(value: boolean) {
    const publishAudio = !value;

    this.streamInfo.publishAudio = publishAudio;

    if (this.publisher) {
      if (this.publisherStatus === 'ready') {
        this._toggleAudioStream();
      } else {
        this.removeListener(PUBLISHER_READY_TOGGLE_STREAMS, this._toggleAudioStream);
        this.once(PUBLISHER_READY_TOGGLE_STREAMS, this._toggleAudioStream);
      }
    }
  }

  private _toggleVideoStream = () => {
    const { publishVideo } = this.streamInfo;

    this.changePublisherPublishProps('video', publishVideo);
  };

  public toggleVideoStream(value: boolean) {
    const publishVideo = !value;

    this.streamInfo.publishVideo = publishVideo;

    if (this.publisher) {
      if (this.publisherStatus === 'ready') {
        this._toggleVideoStream();
      } else {
        this.removeListener(PUBLISHER_READY_TOGGLE_STREAMS, this._toggleVideoStream);
        this.once(PUBLISHER_READY_TOGGLE_STREAMS, this._toggleVideoStream);
      }
    }
  }

  public publishDesktop(userId: string, streamTrack: MediaStreamTrack) {
    this.publisherDesktop = OT.initPublisher(
      'publisher-desktop',
      {
        insertDefaultUI: false,
        publishAudio: false,
        publishVideo: true,
        audioSource: false,
        videoSource: streamTrack,
        name: encodeStreamName(userId, 'desktop'),
      },
      (err: OT.OTError) => {
        if (err) {
          // todo
        }
      }
    );

    this.streamInfo.desktopSource = streamTrack;

    if (this.isConnected) {
      this._sessionPublishDesktop();
    } else {
      this.once(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS, () => {
        this._sessionPublishDesktop();
      });
    }
  }

  public unpublishDesktop() {
    if (this.publisherDesktop) {
      const track = this.streamInfo.desktopSource;

      if (this.session) {
        this.session.unpublish(this.publisherDesktop);
      }
      this.publisherDesktop.destroy();
      this.publisherDesktop = null;

      this.streamInfo.desktopSource = null;
      this.logger.info('Stop publish desktop stream', { trackId: track.id });
    }
  }

  public sendMessage(type: string, data: string) {
    if (!this.session) {
      return;
    }

    this.session.signal({ data, type }, (err) => {
      // todo: what to do on error?
    });
  }

  public onMessage(type: string, callback: (data: string) => void) {
    if (this.session) {
      this.subscribeOnSessionMessages(type, callback);
    } else {
      this.messageSubscribersQueue.push({ type, callback });
    }
  }

  public updatePublishStreams(audioTrack: MediaStreamTrack, videoTrack: MediaStreamTrack) {
    if (!this.publisher) {
      return;
    }

    this.logger.debug('Re-Publish stream with', {
      audioTrack: {
        id: audioTrack.id,
        muted: audioTrack.muted,
        enabled: audioTrack.enabled,
        readyState: audioTrack.readyState,
      },
      videoTrack: {
        id: videoTrack.id,
        muted: videoTrack.muted,
        enabled: videoTrack.enabled,
        readyState: videoTrack.readyState,
      },
    });

    this.streamInfo.audioSource = audioTrack;
    this.streamInfo.videoSource = videoTrack;

    // republish new streams
    this.republishPublisher();
  }

  public getUserStreams(): { [s: string]: MediaStream } {
    return _transform(
      this.participants,
      (res: MediaStreamMap, part: IOTWebrtcParticipantData, userId: string) => {
        if (part) {
          const stream = getNativeStreamFromSubscriber(part.subscriber);

          if (stream) {
            res[userId] = stream;
          }
        }
      },
      {}
    );
  }

  public testCallStart(
    params: IWebrtcServiceAccessParams,
    config: IWebrtcTestCallConfig,
    onRuntimeError: () => void
  ): Promise<undefined> {
    return new Promise((resolve, reject) => {
      let testNotCanceled = true;

      const forceCancel = () => {
        testNotCanceled = false;
      };

      this.once(TEST_CALL_FORCE_CANCEL, forceCancel);

      Promise.resolve()
        .then(() => createSession(params))
        .then((session) => {
          if (testNotCanceled) {
            // on connection error
            session.once('sessionReconnecting', () => {
              if (this.testCall) {
                this.testCall.stop();
              }

              forceCancel();
              onRuntimeError();
            });

            return createTestSubscriber(session, config);
          }
        })
        .then((meeting) => {
          if (testNotCanceled) {
            this.testCall = collectTestCallStats(meeting, config);
            this.removeListener(TEST_CALL_FORCE_CANCEL, forceCancel);
            return resolve(); // signal that we started real test call
          }
        })
        .catch(reject);
    });
  }

  public testCallStop(): number {
    this.emit(TEST_CALL_FORCE_CANCEL);

    if (!this.testCall) {
      return NaN;
    }

    const mos = this.testCall.stop();

    this.testCall = null;

    return mos;
  }
}

export default OpenTokService;
