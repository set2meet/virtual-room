/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { EventEmitter } from 'events';
import { mock } from 'jest-mock-extended';
import * as OT from '@opentok/client';
import setupIoCClientForTest from '../../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { WebrtcServiceType } from '../../../common/types/WebrtcServiceType';
import { IClientEntityProviderInternals } from '../../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';

class MockSession extends EventEmitter {
  public connect = jest.fn();
  public subscribe = jest.fn();
}

const mockSession = new MockSession();

jest.mock('@opentok/client', () => ({
  initSession: () => mockSession,
}));

beforeAll(setupIoCClientForTest());

describe('opentok webrtc', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('getUserStreams should returns empty if stream is not created', async (done) => {
    const webrtcServices = await (clientEntityProvider as IClientEntityProviderInternals).getWebRTCServices();
    const webrtc = clientEntityProvider.getUtils().getWebrtcServiceByName(WebrtcServiceType.OPENTOK, webrtcServices);

    webrtc.connect('apyKey', 'sessionId', 'token');

    mockSession.emit('sessionConnected');
    mockSession.emit('streamCreated', {
      stream: mock<OT.Stream>({
        name: 'stream1',
      }),
    });

    const userStreams = webrtc.getUserStreams();

    expect(userStreams).toEqual({});
    done();
  });

  test('getUserStreams should returns object with streams', async (done) => {
    const webrtcServices = await (clientEntityProvider as IClientEntityProviderInternals).getWebRTCServices();
    const webrtc = clientEntityProvider.getUtils().getWebrtcServiceByName(WebrtcServiceType.OPENTOK, webrtcServices);

    webrtc.connect('apyKey', 'sessionId', 'token');

    mockSession.subscribe.mockReturnValue(
      mock<OT.Subscriber>({
        _: {
          webRtcStream: jest.fn(() => 'ot_private_stream'),
        },
      } as any)
    );
    mockSession.emit('sessionConnected');
    mockSession.emit('streamCreated', {
      stream: mock<OT.Stream>({
        name: 'stream1',
      }),
    });

    const userStreams = webrtc.getUserStreams();

    expect(userStreams).toEqual({
      stream1: 'ot_private_stream',
    });
    done();
  });
});
