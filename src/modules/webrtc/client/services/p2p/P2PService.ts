/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _transform from 'lodash/transform';
import EventEmitter from 'events';
import Peer from './helpers/peer';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import {
  SERVICE_NAME,
  SERVICE_EVENT_MESSAGE,
  SERVICE_EVENT_TYPE,
  SERVICE_CONNECTION_MEDIA_MODE,
} from '../../../common/types/p2p/constants';
import {
  IWebrtcService,
  IWebrtcTestCallConfig,
  IWebrtcServiceAccessParams,
} from '../../../../../ioc/common/ioc-interfaces';
import testRTCConnection from './helpers/test.rtc-connection';
import { hasActiveTracks } from './helpers/utils';
import { PeerErrorType, PeerConnectionError } from './helpers/types';
import { webrtcConstants } from '../../../../../ioc/common/ioc-constants';
import { LogEvent, LogErrorCode } from '../../../../../ioc/client/ioc-constants';

const { SERVICE_EVENTS } = webrtcConstants;
const TIMEOUT_BEFORE_RECONNECT = 1000;

type LocalPeerMediaInfo = {
  peerId?: string; // same as userId
  publishAudio?: boolean;
  publishVideo?: boolean;
  audioTrack?: MediaStreamTrack;
  videoTrack?: MediaStreamTrack;
  desktopTrack?: MediaStreamTrack;
  stream?: MediaStream; // required only for recording
};

type RemoteParticipant = {
  userId: string;
  stream: MediaStream;
};

type MediaStreamMap = Record<string, MediaStream>;

type P2PParticipantsInfo = Record<string, RemoteParticipant>;

const { DEVICES: mediaModeDevices, DESKTOP: mediaModeDesktop } = SERVICE_CONNECTION_MEDIA_MODE;

enum TrackChangeStatus {
  Nothing = 0,
  Replace = 1,
  Add = 2,
}

const MediaTrackKind: Record<number, Record<string, string>> = {
  [mediaModeDevices]: {
    audio: 'mic',
    video: 'cam',
  },
  [mediaModeDesktop]: {
    video: 'desktop',
  },
};

const getTrackChangeStatus = (track1?: MediaStreamTrack, track2?: MediaStreamTrack): TrackChangeStatus => {
  if (track1 && track2 && track1.id !== track2.id) {
    return TrackChangeStatus.Replace;
  } else if (!track1 && track2) {
    return TrackChangeStatus.Add;
  }
  return TrackChangeStatus.Nothing;
};

class P2PService extends EventEmitter implements IWebrtcService {
  public name = SERVICE_NAME;

  private _sessionId: string = '';
  private _localPeer: Peer = null;
  private _localPeerMediaInfo: LocalPeerMediaInfo = {};
  private _participants: P2PParticipantsInfo = {};
  private _subscribedEvents: string[] = [];
  private _isConnectionLost: boolean = false;
  private _isConnected: boolean = false;
  private _testCallInProgress: boolean = false;

  private logger = clientEntityProvider.getLogger().webrtcP2p;

  private _clearAllListeners() {
    const socket = clientEntityProvider.getSocket();

    socket.off(SERVICE_EVENT_MESSAGE, this._baseSocketEventHandler);
  }

  // todo: remove and patch Peer to work with tracks only
  private _getLocalStream(mediaMode: SERVICE_CONNECTION_MEDIA_MODE): MediaStream {
    const { audioTrack, videoTrack, desktopTrack } = this._localPeerMediaInfo;
    const stream = new MediaStream();

    if (mediaMode === mediaModeDevices) {
      if (audioTrack) {
        stream.addTrack(audioTrack);
      }
      if (videoTrack) {
        stream.addTrack(videoTrack);
      }
    } else {
      if (desktopTrack) {
        stream.addTrack(desktopTrack);
      }
    }

    return stream;
  }

  private _createParticipant(userId: string, mediaMode: SERVICE_CONNECTION_MEDIA_MODE, stream: MediaStream) {
    const modeDevice = mediaMode === mediaModeDevices;

    this._logStream(`Peer '${userId}' add stream`, stream);

    if (modeDevice) {
      const userInfo = this._participants[userId];
      const { stream: prevStream } = userInfo || { stream: null };

      this._participants[userId] = {
        userId,
        stream,
      };

      if (prevStream) {
        this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, prevStream);
      }

      this.emit(SERVICE_EVENTS.USER_STREAM_ADDED, userId, stream);
    }

    const tracks = stream.getTracks().map((track) => ({
      track,
      kind: MediaTrackKind[mediaMode][track.kind],
    }));

    this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, { userId, tracks });
  }

  private _removeParticipant = (userId: string) => {
    // recording
    const participant = this._participants[userId];

    if (!participant) {
      return;
    }

    const { stream } = participant;

    if (stream) {
      this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, stream);
    }

    // basic functionality
    this._participants[userId] = null;
    this.logger.report(LogEvent.WsP2p, {
      userId,
      message: 'Peer connection with user is destroyed',
    });
    this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, {
      userId,
      tracks: [
        { kind: 'cam', track: null },
        { kind: 'mic', track: null },
      ],
    });
  };

  private _callRequest = (payload: any) => {
    const { userId, mediaMode, inversed, hasStream } = payload;

    // call makes only peer with active stream
    const stream = this._getLocalStream(mediaMode);
    const isStreamActive = hasActiveTracks(stream);

    /** @todo here we get local stream for any request and in case
     * when local stream is not active (e.g. we block micro in browser) (isStreamActive == false)
     * we can't subscribe to streams from other users, because we subscribe to their streams only in _callPeer()
     * function
     * Related issue: EPMSM-911
     */
    // can call to other peer only if local peer has active stream
    if (isStreamActive) {
      this._callPeer(userId, stream, mediaMode);
      this.logger.report(LogEvent.WsP2p, {
        userId,
        mediaMode,
        message: `Peer make call to user by media mode ${mediaMode}`,
      });

      // if call request by mediaModeDevices - check existance of mediaModeDesktop connection
      if (mediaMode === mediaModeDesktop) {
        return;
      }

      if (!this._localPeer.getConnectionByMediaMode(userId, mediaModeDesktop)) {
        // simulate call request
        this._callRequest({
          userId,
          mediaMode: mediaModeDesktop,
          hasStream: !!this._localPeerMediaInfo.desktopTrack,
        });
      }

      return;
    }

    // if request inversed and local peer and has no active stream too - do nothing
    // don't create connection between this "empty" peers for mediaMode
    // in other case - redirect call-request to peer-target
    // if this request not already inversed from peer without stream
    if (!inversed && hasStream) {
      clientEntityProvider.getSocket().emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.CALL_REQUEST,
        payload: {
          userId: this._localPeer.id,
          sessionId: this._sessionId,
          mediaMode,
          inversed: true,
          hasStream: false,
          target: userId,
        },
      });

      this.logger.report(LogEvent.WsP2p, {
        userId,
        mediaMode,
        message: `Peer call-request with a user by mode '${mediaMode}' was redirected`,
      });
    }
  };

  private _callPeer(peerId: string, stream: MediaStream, mediaMode: SERVICE_CONNECTION_MEDIA_MODE): void {
    const call = this._localPeer.call(peerId, stream, mediaMode);

    this._logStream(
      `Peer '${this._localPeer.id}' make call to '${peerId}' by mode '${mediaMode}' with local stream`,
      stream
    );

    if (!call) {
      return;
    }

    call.on('stream', (remoteStream: MediaStream) => {
      this._createParticipant(call.peer, mediaMode, remoteStream);
    });
  }

  private _createPublisher(userId: string) {
    this._localPeer = new Peer({
      id: userId,
      secure: true,
      debug: 3,
      socket: clientEntityProvider.getSocket(),
      config: clientEntityProvider.getConfig().webrtc.p2p.RTCConfiguration,
      sessionId: this._sessionId,
    });

    // add handler to answer incoming call
    this._localPeer.on('call', (call) => {
      this.logger.report(LogEvent.WsP2p, {
        callPeer: call.peer,
        mediaMode: call.mediaMode,
        message: 'Incoming call from peer with media mode',
      });

      // incaming call: answer with an A/V stream
      // const stream = call.mediaMode === mediaModeDevices ? this._getLocalStream(call.mediaMode) : undefined;
      const stream = this._getLocalStream(call.mediaMode);

      call.answer(stream);

      // subscribe on remote stream
      call.on('stream', (remoteStream: MediaStream) => {
        this._createParticipant(call.peer, call.mediaMode, remoteStream);
      });
    });

    this._localPeer.on('peer-leave', this._removeParticipant);

    // on webrtc error - send event to server side about this
    this._localPeer.on(PeerErrorType.WebRTC, ({ connection }: PeerConnectionError) => {
      clientEntityProvider.getSocket().emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.PEER_CONNECTION_FAILED,
        payload: {
          ...connection,
          src: this._localPeer.id,
          sessionId: this._sessionId,
        },
      });
    });
  }

  private _baseSocketEventHandler = (data: any) => {
    const { type, payload } = data;

    // on failed connect
    if (type === SERVICE_EVENT_TYPE.CONNECT_FAILURE) {
      this.logger.error(LogErrorCode.MODULE, 'Connect to session was failed');
      this.emit(SERVICE_EVENTS.SESSION_CONNECT_ERROR);
    }

    // on success connect
    if (type === SERVICE_EVENT_TYPE.CONNECT_SUCCESS) {
      this.logger.info('Connect to session was success');

      this._isConnected = true;
      this._sessionId = payload.sessionId;

      this._createPublisher(payload.userId);

      this.emit(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS);

      return;
    }

    // on call request from other user in the same ssession
    if (type === SERVICE_EVENT_TYPE.CALL_REQUEST) {
      if (!this._isConnected) {
        this.logger.error(LogErrorCode.MODULE, 'Error: call request for not-connected participant', {
          userId: payload.userId,
        });
        return;
      }

      this._callRequest(payload);
    }

    // remote peer stop sharing desktop
    if (type === SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP_STOP) {
      const { userId } = payload;

      this.logger.info('Peer stop sharing desktop', { userId });

      this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, {
        userId,
        tracks: [
          {
            kind: 'desktop',
            track: null,
          },
        ],
      });
    }
  };

  private _sessionPublishDesktop() {
    const socket = clientEntityProvider.getSocket();

    socket.emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP,
      payload: {
        src: this._localPeerMediaInfo.peerId,
        sessionId: this._sessionId,
      },
    });

    // loggering
    const dt = this._localPeerMediaInfo.desktopTrack;

    this.logger.info('Publish desktop stream', {
      videoTrackId: dt.id,
      enabled: dt.enabled,
      muted: dt.muted,
    });
  }

  private _sessionUnpublishDesktop() {
    const socket = clientEntityProvider.getSocket();
    const { desktopTrack } = this._localPeerMediaInfo;

    this.logger.info('Stop publish desktop stream', {
      videoTrackId: desktopTrack.id,
    });

    this._localPeerMediaInfo.desktopTrack = undefined;

    desktopTrack.stop();
    desktopTrack.enabled = false;

    socket.emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP_STOP,
      payload: {
        src: this._localPeerMediaInfo.peerId,
        sessionId: this._sessionId,
      },
    });

    // this._localPeer.removeTrack(desktopTrack, mediaModeDesktop);
    this._localPeer.closeMediaConnections(mediaModeDesktop, { onlyWithEnededStreams: true });
  }

  private _publishToSession = (mediaMode: SERVICE_CONNECTION_MEDIA_MODE, except: string[] = []) => {
    clientEntityProvider.getSocket().emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.PEER_PUBLISH,
      payload: {
        except,
        mediaMode,
        userId: this._localPeer.id,
        sessionId: this._sessionId,
      },
    });
  };

  private get _existedConnections(): string[] {
    return this._localPeer.getConnectionsByMediaMode(mediaModeDevices);
  }

  private _handleSocketConnect = () => {
    if (this._isConnected && this._isConnectionLost) {
      this._isConnectionLost = false;
      this.emit(SERVICE_EVENTS.SESSION_CONNECTION_RESTORED);
    }
  };

  private _handleSocketDisconnect = () => {
    if (!this._isConnected) {
      return;
    }

    if (this._localPeer) {
      this._localPeer.removeAllListeners();
      this._localPeer.cleanUp();
    }

    // probably we have problems with the Internet and we are disconnected

    this._isConnectionLost = true;
    this.emit(SERVICE_EVENTS.SESSION_CONNECTION_LOST);
  };

  constructor() {
    super();
  }

  public subscribe(event: string, listener: (...args: any[]) => void) {
    super.on(event, listener);

    if (this._subscribedEvents.indexOf(event) === -1) {
      this._subscribedEvents.push(event);
    }
  }

  public unsubscribe() {
    this._subscribedEvents.forEach((event: string) => {
      this.removeAllListeners(event);
    });
    this._subscribedEvents.length = 0;
  }

  public connect(apiKey: string, sessionId: string, token: string) {
    const socket = clientEntityProvider.getSocket();

    socket.on(SERVICE_EVENT_MESSAGE, this._baseSocketEventHandler);

    socket.emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.CONNECT,
      payload: {
        apiKey,
        sessionId,
        token,
      },
    });

    socket.on('connect', this._handleSocketConnect);
    socket.on('disconnect', this._handleSocketDisconnect);

    this.logger.info('Try connect to session');

    // temporary for development purpose only
    if ((module as any).hot) {
      (window as any).peer = this;

      (window as any).showConnections = () => {
        console.log(` >>>>>>> ${this._localPeer.id} <<<<<<<`);
        (this._localPeer as any)._connections.forEach((connList: any, peerId: string) => {
          console.log(` --------- ${peerId} ---------`);
          connList.forEach((conn: any) => {
            console.log(`  ${conn.connectionId} - ${conn.mediaMode}`);
          });
        });
      };
    }
  }

  public disconnect() {
    if (!this._isConnected) {
      this.emit(SERVICE_EVENTS.SESSION_DISCONNECTED);
      return;
    }

    this._isConnected = false;
    this._isConnectionLost = false;

    const socket = clientEntityProvider.getSocket();

    socket.emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.DISCONNECT,
      payload: {
        src: this._localPeer.id,
        sessionId: this._sessionId,
      },
    });

    this._sessionId = '';
    this._clearAllListeners();
    this._localPeer.destroy();
    this._localPeer = null;
    this.emit(SERVICE_EVENTS.SESSION_DISCONNECTED);

    this.logger.info('Disconnected from session');

    // recording
    const { stream } = this._localPeerMediaInfo;

    if (stream) {
      this.emit(SERVICE_EVENTS.USER_STREAM_REMOVED, stream);
    }
  }

  public reconnect(apiKey: string, sessionId: string, token: string) {
    setTimeout(() => {
      const socket = clientEntityProvider.getSocket();

      socket.emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.CONNECT,
        payload: {
          apiKey,
          sessionId,
          token,
        },
      });
    }, TIMEOUT_BEFORE_RECONNECT);
  }

  // tslint:disable:cyclomatic-complexity
  public publisherUpdateStream(
    _name: string,
    _audioTrack: MediaStreamTrack | null,
    _videoTrack: MediaStreamTrack | null,
    _publishAudio?: boolean,
    _publishVideo?: boolean
  ) {
    const { audioTrack, videoTrack, publishAudio, publishVideo } = this._localPeerMediaInfo;

    this._localPeerMediaInfo.peerId = _name;

    const audioTrackStatus = getTrackChangeStatus(audioTrack, _audioTrack);
    const videoTrackStatus = getTrackChangeStatus(videoTrack, _videoTrack);

    this.emit(SERVICE_EVENTS.PARTICIPANT_UPDATE_MEDIA_STREAM_TRACK, {
      userId: _name,
      tracks: [{ track: _videoTrack, kind: 'cam' }],
    });

    if (_audioTrack) {
      // emit event about streams
      const stream = new MediaStream();
      const userId = _name;

      stream.addTrack(_audioTrack);

      this._localPeerMediaInfo.stream = stream;
      this.emit(SERVICE_EVENTS.USER_STREAM_ADDED, userId, stream);
    }

    // dont replace dead track on nothing (undefined)
    // possible case new track will appear
    // and then we can easily replace it

    if (audioTrackStatus) {
      this._localPeerMediaInfo.audioTrack = _audioTrack;
    }
    if (videoTrackStatus) {
      this._localPeerMediaInfo.videoTrack = _videoTrack;
    }
    if (_publishAudio !== undefined) {
      this._localPeerMediaInfo.publishAudio = _publishAudio;
    }
    if (_publishVideo !== undefined) {
      this._localPeerMediaInfo.publishVideo = _publishVideo;
    }

    // if mic muted - mute audio track
    if (!publishAudio) {
      this.toggleAudioStream(true);
    }
    // if cam muted - mute video track
    if (!publishVideo) {
      this.toggleVideoStream(true);
    }

    // logger - variable for logger info
    const logger: {
      common: string;
      audio: string;
      video: string;
    } = {
      common: '',
      audio: '',
      video: '',
    };

    if (this._isConnected) {
      const add = TrackChangeStatus.Add;
      const replace = TrackChangeStatus.Replace;

      // if new track created - required renegotiation
      if (audioTrackStatus === add || videoTrackStatus === add) {
        logger.common = 'renegotiation';
        this._localPeer.closeMediaConnections(mediaModeDevices);
      } else {
        // just replace tracks in all current connections
        if (audioTrackStatus === replace) {
          logger.audio = 'replace';
          this._localPeer.replaceTrack(_audioTrack);
        }
        if (videoTrackStatus === replace) {
          logger.video = 'replace';
          this._localPeer.replaceTrack(_videoTrack);
        }
      }

      this._publishToSession(mediaModeDevices, this._existedConnections);
    }

    this.logger.debug('Peer update stream', {
      loggerBy: logger.common || logger.audio,
      audioTrackFrom: audioTrack ? audioTrack.id : null,
      audioTrackTo: _audioTrack ? _audioTrack.id : null,
      videoTrackFrom: videoTrack ? videoTrack.id : null,
      videoTrackTo: _videoTrack ? _videoTrack.id : null,
    });
  }
  // tslint:enable

  public toggleAudioStream(value: boolean) {
    const { audioTrack } = this._localPeerMediaInfo;

    this._localPeerMediaInfo.publishAudio = !value;

    if (audioTrack) {
      audioTrack.enabled = this._localPeerMediaInfo.publishAudio;
    }
  }

  public toggleVideoStream(value: boolean) {
    const { videoTrack } = this._localPeerMediaInfo;

    this._localPeerMediaInfo.publishVideo = !value;

    if (videoTrack) {
      videoTrack.enabled = this._localPeerMediaInfo.publishVideo;
    }
  }

  public publishDesktop(userId: string, streamTrack: MediaStreamTrack) {
    this._localPeerMediaInfo.desktopTrack = streamTrack;

    if (this._isConnected) {
      this._sessionPublishDesktop();
    } else {
      this.once(SERVICE_EVENTS.SESSION_CONNECT_SUCCESS, () => {
        this._sessionPublishDesktop();
      });
    }
  }

  public unpublishDesktop() {
    if (this._isConnected) {
      this._sessionUnpublishDesktop();
    }
  }

  public sendMessage(type: string, data: string) {
    // todo
  }

  public onMessage(type: string, callback: (data: string) => void) {
    // todo
  }

  public updatePublishStreams(audioTrack: MediaStreamTrack, videoTrack: MediaStreamTrack) {
    this.logger.info('Peer update stream (2)');
    this.publisherUpdateStream(this._localPeerMediaInfo.peerId, audioTrack, videoTrack);
  }

  public getUserStreams(): { [s: string]: MediaStream } {
    const collector = {};
    const { stream } = this._localPeerMediaInfo;

    if (stream) {
      collector[this._localPeer.id] = stream;
    }

    return _transform(
      this._participants,
      (res: MediaStreamMap, part: RemoteParticipant, userId: string) => {
        if (part) {
          res[userId] = part.stream;
        }
      },
      collector
    );
  }

  public testCallStart(
    params: IWebrtcServiceAccessParams,
    config: IWebrtcTestCallConfig,
    onRuntimeError: () => void
  ): Promise<undefined> {
    const { RTCConfiguration } = clientEntityProvider.getConfig().webrtc.p2p;

    this._testCallInProgress = true;

    return new Promise(() => {
      const testFinished = (results: number): undefined => {
        if (this._testCallInProgress && config.callback) {
          this._testCallInProgress = false;
          config.callback(results);
        }

        return void 0;
      };

      Promise.resolve()
        .then(() => testRTCConnection(RTCConfiguration))
        .then(testFinished);
    });
  }

  public testCallStop(): number {
    this._testCallInProgress = false;

    return 2;
  }

  private _logStream(message: string, stream: MediaStream) {
    const s = stream;
    const at = s ? s.getAudioTracks()[0] : null;
    const vt = s ? s.getVideoTracks()[0] : null;
    const ai = at ? ` a=${at.enabled ? 1 : 0}${at.muted ? 0 : 1}'${at.readyState}'${at.id}'` : '';
    const vi = vt ? ` v=${vt.enabled ? 1 : 0}${vt.muted ? 0 : 1}'${vt.readyState}'${vt.id}'` : '';
    const streamInfo = `(id=${stream.id} status=${stream.active}${ai}${vi})`;

    this.logger.debug(message + ' ' + streamInfo);
  }
}

export default P2PService;
