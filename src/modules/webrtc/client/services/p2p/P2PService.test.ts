/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import P2PService from './P2PService';
import setupIoCClientForTest from '../../../../../test/utils/ioc/testIoCClient';

beforeAll(setupIoCClientForTest());

describe('webrtc', () => {
  test('getUserStreams should not return uninitialized streams', () => {
    const webrtc = new P2PService();

    expect(webrtc.getUserStreams()).toEqual({});
  });
});
