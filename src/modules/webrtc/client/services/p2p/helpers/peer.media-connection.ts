/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Peer from './peer';
import { utils } from './utils';
import _throttle from 'lodash/throttle';
import { Negotiator } from './negotiator';
import {
  PeerAnswerOption,
  PeerServerMessage,
  PeerBaseConnection,
  PeerConnectionType,
  ConnectionEventType,
} from './types';
import { SERVICE_EVENT_TYPE, SERVICE_CONNECTION_MEDIA_MODE } from '../../../../common/types/p2p/constants';
import { hasActiveTracks } from './utils';

const ID_PREFIX = 'mc_';
const ADD_STREAM_THROTTLE_TIME = 500;

type RTCSendersHashMap = Record<string, RTCRtpSender>;

/**
 * Wraps the streaming interface between two Peers.
 */
export class PeerMediaConnection extends PeerBaseConnection {
  private _isMaster: boolean = true;
  private _negotiator: Negotiator;
  private _localStream: MediaStream;
  private _remoteStream: MediaStream;
  private _mediaMode: SERVICE_CONNECTION_MEDIA_MODE;
  public peerConnection: RTCPeerConnection;

  public get type() {
    return PeerConnectionType.Media;
  }

  public get isOpen() {
    return this._open;
  }

  public get mediaMode() {
    return this._mediaMode;
  }

  public get localStream(): MediaStream {
    return this._localStream;
  }

  public get remoteStream(): MediaStream {
    return this._remoteStream;
  }

  constructor(peerId: string, provider: Peer, options: any) {
    super(peerId, provider, options);

    const { connectionId } = options;

    if (connectionId) {
      this._isMaster = false;
    }

    this._mediaMode = options._mediaMode;
    this._localStream = this.options._stream;

    this.connectionId = connectionId || ID_PREFIX + utils.randomToken();

    this._negotiator = new Negotiator(this);

    if (this._localStream) {
      this._negotiator.startConnection({
        _stream: this._localStream,
        originator: true,
      });
    }
  }

  public addStream = _throttle((remoteStream: MediaStream) => {
    this._remoteStream = remoteStream;
    this.emit(ConnectionEventType.Stream, remoteStream);
  }, ADD_STREAM_THROTTLE_TIME);

  public handleMessage(message: PeerServerMessage): void {
    const { type, payload } = message;

    switch (type) {
      case SERVICE_EVENT_TYPE.PEER_ANSWER:
        // Forward to negotiator
        this._negotiator.handleSDP(type, payload.sdp);
        this._open = true;
        break;
      case SERVICE_EVENT_TYPE.PEER_CANDIDATE:
        this._negotiator.handleCandidate(payload.candidate);
        break;
      default:
        console.warn(`[P2P] Unrecognized message type:${type} from peer:${this.peer}`);
        break;
    }
  }

  public answer(stream: MediaStream, options: PeerAnswerOption = {}): void {
    if (this._localStream) {
      console.warn('[P2P] Local stream already exists on this MediaConnection. Are you answering a call twice?');
      return;
    }

    this._localStream = stream;

    if (options && options.sdpTransform) {
      this.options.sdpTransform = options.sdpTransform;
    }

    this._negotiator.startConnection({ ...this.options._payload, _stream: stream });

    // Retrieve lost messages stored because PeerConnection not set up.
    const messages = this.provider.getMessages(this.connectionId);

    for (const message of messages) {
      this.handleMessage(message);
    }

    this._open = true;
  }

  public answerOnRenegotiation(options: any) {
    // https://stackoverflow.com/questions/44932655/failed-to-set-remote-answer-sdp-failed-to-push-down-transport-description-fail
    // To work around this ensure that the offer/answer direction remains consistent.
    // Meaning, if Firefox generates the initial offer, it generates all subsequent offers as well.

    this._negotiator.handleSDP('OFFER', options.sdp);
  }

  public addLocalStream(stream: MediaStream) {
    stream.getTracks().forEach((track) => {
      this.peerConnection.addTrack(track);
    });
    this._negotiator.makeRenegotiation(this._isMaster);
  }

  public makeRenegotiation() {
    this._negotiator.makeRenegotiation(this._isMaster);
  }

  public replaceLocalStream(stream: MediaStream) {
    this._localStream = stream;

    const rtcSenders: RTCSendersHashMap = this.peerConnection.getSenders().reduce((map, sender) => {
      const { track } = sender;

      if (track) {
        map[track.kind] = sender;
      }

      return map;
    }, {} as RTCSendersHashMap);

    stream.getTracks().forEach((track) => {
      const sender = rtcSenders[track.kind];

      if (sender) {
        this.peerConnection.removeTrack(sender);
      }

      this.peerConnection.addTrack(track);
    });

    this._negotiator.makeRenegotiation(this._isMaster);
  }

  /**
   * Exposed functionality for users.
   */

  /** Allows user to close connection. */
  public close(): void {
    if (this._negotiator) {
      this._negotiator.cleanup();
      this._negotiator = null;
    }

    this._localStream = null;
    this._remoteStream = null;

    if (this.provider) {
      this.provider.removeConnection(this);
    }

    if (this.options && this.options._stream) {
      this.options._stream = null;
    }

    if (!this.open) {
      return;
    }

    this._open = false;

    super.emit(ConnectionEventType.Close);
  }

  public hasActiveStream(): boolean {
    return hasActiveTracks(this._localStream) || hasActiveTracks(this._remoteStream);
  }
}
