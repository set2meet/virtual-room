/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _noop from 'lodash/noop';
import { RTCPeerConnection } from './adapter';

const WAIT_ICE_CANDIDATE_TIME = 2000;

type TestRTCTransportType = 'tcp' | 'udp';

type RTCCandidate = {
  type: RTCIceCandidateType;
  protocol: string;
  address: string;
};

const offerParams: any = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true,
  voiceActivityDetection: true,
};

const rtcOptional = { optional: [{ DtlsSrtpKeyAgreement: true }] };

const parseCandidate = (candidate: string): RTCCandidate => {
  const template = 'candidate:';
  const pos = candidate.indexOf(template) + template.length;
  const fields = candidate.substr(pos).split(' ');
  const type = fields[7] as RTCIceCandidateType; // tslint:disable-line

  return {
    type,
    protocol: fields[2], // tslint:disable-line
    address: fields[4], // tslint:disable-line
  };
};

const isTypeRelay = (candidate: RTCCandidate): boolean => {
  return candidate.type === 'relay';
};

const isTypeSrflx = (candidate: RTCCandidate): boolean => {
  return candidate.type === 'srflx';
};

const buildConfigWithTransportType = (config: RTCConfiguration, transport: TestRTCTransportType): RTCConfiguration => {
  const rtcTransport = '?transport=' + transport;
  const iceServers = config.iceServers
    .filter((rec) => rec.urls.indexOf('transport') === -1)
    .map((rec) => {
      const { urls, credential, username } = rec;
      const iceServer: RTCIceServer = {
        urls: urls + rtcTransport,
      };

      if (urls.indexOf('turn') === 0) {
        iceServer.credential = credential;
        iceServer.username = username;
      }

      return iceServer;
    });

  return { iceServers };
};

const testRTCConnection = (config: RTCConfiguration, transport: TestRTCTransportType): Promise<boolean> =>
  new Promise((resolve) => {
    const rtcConfig = buildConfigWithTransportType(config, transport);

    let wait: number;
    let pc: RTCPeerConnection;
    let success: boolean = false;

    const finish = () => resolve(success);

    const startWait = () => {
      wait = window.setTimeout(finish, WAIT_ICE_CANDIDATE_TIME);
    };

    try {
      pc = new RTCPeerConnection(
        {
          iceServers: rtcConfig.iceServers,
          sdpSemantics: 'unified-plan',
        },
        rtcOptional
      );
    } catch (err) {
      resolve(false);
    }
    pc.onicecandidate = (evt) => {
      if ((evt.currentTarget as any).signalingState === 'closed') {
        return;
      }

      if (evt.candidate) {
        const candidate = parseCandidate(evt.candidate.candidate);

        success = success || isTypeRelay(candidate) || isTypeSrflx(candidate);

        clearTimeout(wait);
        startWait();
      }
    };

    pc.createOffer(offerParams).then((offer) => {
      pc.setLocalDescription(offer).then(_noop, _noop);
    }, _noop);

    startWait();
  });

export default (config: RTCConfiguration): Promise<number> =>
  new Promise((resolve, reject) => {
    Promise.all([testRTCConnection(config, 'udp'), testRTCConnection(config, 'tcp')]).then((result) => {
      // 1. convert to byte string
      // 2. convert byte to integer
      resolve(parseInt([+result[0], +result[1]].join(''), 2));
    });
  });
