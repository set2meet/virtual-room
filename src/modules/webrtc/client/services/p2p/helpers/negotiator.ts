/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { utils } from './utils';
import { RTCPeerConnection, RTCSessionDescription, RTCIceCandidate } from './adapter';
import { PeerBaseConnection } from './types';
import { SERVICE_EVENT_TYPE, SERVICE_EVENT_MESSAGE } from '../../../../common/types/p2p/constants';
import { PeerErrorType, PeerConnectionType, ConnectionEventType } from './types';
import { PeerMediaConnection } from './peer.media-connection';
import clientEntityProvider from '../../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { LogEvent } from 'src/ioc/client/ioc-constants';

/**
 * Manages all negotiations between Peers.
 */
export class Negotiator {
  constructor(readonly connection: PeerBaseConnection) {}

  /** Returns a PeerConnection object set up correctly (for data, media). */
  public startConnection(options: any) {
    const peerConnection = this._startPeerConnection();

    // Set the connection's PC.
    this.connection.peerConnection = peerConnection;

    if (this.connection.type === PeerConnectionType.Media && options._stream) {
      this._addTracksToConnection(options._stream, peerConnection);
    }

    if (options.originator) {
      this._makeOffer();
    } else {
      this.handleSDP('OFFER', options.sdp);
    }
  }

  public makeRenegotiation(isMaster: boolean) {
    if (isMaster) {
      this._makeOffer();
    } else {
      this._redirectOffer();
    }
  }

  /** Start a PC. */
  private _startPeerConnection(): RTCPeerConnection {
    let optional = {};

    if (this.connection.type === PeerConnectionType.Media) {
      // Interop req for chrome.
      optional = { optional: [{ DtlsSrtpKeyAgreement: true }] };
    }

    const peerConnection = new RTCPeerConnection(this.connection.provider.rtcConfig, optional);

    this._setupListeners(peerConnection);

    return peerConnection;
  }

  /** Set up various WebRTC listeners. */
  private _setupListeners(peerConnection: RTCPeerConnection) {
    const connection = this.connection as PeerMediaConnection;
    const { provider, connectionId } = connection;
    const connectionType = this.connection.type;
    const peerId = this.connection.peer;

    // ICE candidates
    peerConnection.onicecandidate = (evt) => {
      const { candidate } = evt;

      if (!candidate) {
        return;
      }

      provider.socket.emit(SERVICE_EVENT_MESSAGE, {
        type: SERVICE_EVENT_TYPE.PEER_CANDIDATE,
        payload: {
          src: provider.id,
          dst: peerId,
          candidate,
          connectionId,
          connectionType,
          sessionId: provider.sessionId,
        },
      });
    };

    peerConnection.oniceconnectionstatechange = () => {
      let iceError: string;

      switch (peerConnection.iceConnectionState) {
        case 'failed':
          iceError = `Negotiation of connection to ${peerId} failed.`;
          break;
        case 'closed':
          iceError = `Negotiation of connection to ${peerId} closed.`;
          break;
        case 'disconnected':
          iceError = `Negotiation of connection to ${peerId} disconnected.`;
          break;
        case 'completed':
          peerConnection.onicecandidate = utils.noop;
          break;
      }

      if (iceError) {
        this._error(new Error(`Invalid ice state: ${peerConnection.iceConnectionState}`), iceError);
      }

      this.connection.emit(ConnectionEventType.IceStateChanged, peerConnection.iceConnectionState);
    };

    // media connection.
    peerConnection.ontrack = (evt) => {
      let stream = evt.streams[0];
      const mediaConnection = provider.getConnection(peerId, connectionId) as PeerMediaConnection;

      if (!stream && evt.track) {
        stream = new MediaStream();
        stream.addTrack(evt.track);
      }

      if (mediaConnection.type === PeerConnectionType.Media) {
        this._addStreamToMediaConnection(stream, mediaConnection);
      }
    };
  }

  public cleanup(): void {
    const { peerConnection } = this.connection;

    if (!peerConnection) {
      return;
    }

    this.connection.peerConnection = null;

    // unsubscribe from all PeerConnection's events
    peerConnection.oniceconnectionstatechange = utils.noop;
    peerConnection.onicecandidate = utils.noop;
    peerConnection.ontrack = utils.noop;

    const peerConnectionNotClosed = peerConnection.signalingState !== 'closed';
    const dataChannelNotClosed = false;

    if (peerConnectionNotClosed || dataChannelNotClosed) {
      peerConnection.close();
    }
  }

  private _redirectOffer() {
    const { provider, peer, connectionId, type } = this.connection;

    provider.socket.emit(SERVICE_EVENT_MESSAGE, {
      type: SERVICE_EVENT_TYPE.PEER_OFFER_REDIRECT,
      payload: {
        src: provider.id,
        dst: peer,
        connectionId,
        connectionType: type,
        connectionMode: (this.connection as PeerMediaConnection).mediaMode,
        sessionId: provider.sessionId,
      },
    });
  }

  private _makeOffer(): Promise<void> {
    const { provider, peerConnection } = this.connection;

    return new Promise(async (success, failed) => {
      const onError = (err: Error, msg: string) => {
        this._error(err, msg);
        failed();
      };
      const onSuccess = () => {
        success();
      };

      try {
        const offer = await peerConnection.createOffer({
          ...this.connection.options.constraints,
          offerToReceiveAudio: true,
          offerToReceiveVideo: true,
          voiceActivityDetection: true,
        });
        const { sdpTransform } = this.connection.options;

        if (sdpTransform && typeof sdpTransform === 'function') {
          offer.sdp = this.connection.options.sdpTransform(offer.sdp) || offer.sdp;
        }

        try {
          await peerConnection.setLocalDescription(offer);

          provider.socket.emit(SERVICE_EVENT_MESSAGE, {
            type: SERVICE_EVENT_TYPE.PEER_OFFER,
            payload: {
              sdp: offer,
              src: provider.id,
              dst: this.connection.peer,
              connectionId: this.connection.connectionId,
              connectionType: this.connection.type,
              connectionMode: (this.connection as PeerMediaConnection).mediaMode,
              metadata: this.connection.metadata,
              browser: utils.browser,
              sessionId: provider.sessionId,
            },
          });

          onSuccess();
        } catch (errSetDescr) {
          onError(
            errSetDescr,
            `Error: set local description on answer (connection=${this._connectionInfo}, ${errSetDescr.name}, ${errSetDescr.message}`
          );
        }
      } catch (errOffer) {
        onError(
          errOffer,
          `Error: can't create offer (connection=${this._connectionInfo}, ${errOffer.name}, ${errOffer.message}`
        );
      }
    });
  }

  private async _makeAnswer(): Promise<void> {
    const { provider, peerConnection } = this.connection;

    try {
      const answer = await peerConnection.createAnswer();
      const { sdpTransform } = this.connection.options;

      if (sdpTransform && typeof sdpTransform === 'function') {
        answer.sdp = this.connection.options.sdpTransform(answer.sdp) || answer.sdp;
      }

      try {
        await peerConnection.setLocalDescription(answer);

        provider.socket.emit(SERVICE_EVENT_MESSAGE, {
          type: SERVICE_EVENT_TYPE.PEER_ANSWER,
          payload: {
            src: provider.id,
            sdp: answer,
            dst: this.connection.peer,
            connectionId: this.connection.connectionId,
            connectionType: this.connection.type,
            connectionMode: (this.connection as PeerMediaConnection).mediaMode,
            browser: utils.browser,
            sessionId: provider.sessionId,
          },
        });
      } catch (errSetDescr) {
        this._error(errSetDescr, `Error: can't set local description on answer for ${this.connection.peer}`);
      }
    } catch (errMakeAnswer) {
      this._error(errMakeAnswer, `Error: can't create answer as ${provider.id} for ${this.connection.peer}`);
    }
  }

  /** Handle an SDP. */
  public async handleSDP(type: string, sdpOptions: any): Promise<void> {
    const sdp = new RTCSessionDescription(sdpOptions);
    const { peerConnection } = this.connection;
    const self = this; // this is required for await

    try {
      await peerConnection.setRemoteDescription(sdp);

      if (type === 'OFFER') {
        await self._makeAnswer();
      }
    } catch (err) {
      this._error(err, `Error: can't set remote description for ${this.connection.peer}`);
    }
  }

  /** Handle a candidate. */
  public async handleCandidate(ice: any): Promise<void> {
    const { peerConnection } = this.connection;
    const iceCandidate = new RTCIceCandidate(ice);

    peerConnection.addIceCandidate(iceCandidate).catch((err) => {
      const msg = `[P2P] Error: failed to handle candidate for ${this.connection.peer}`;

      clientEntityProvider.getLogger().webrtcP2p.report(LogEvent.WsP2pNegotiator, {
        errorName: err.name,
        errorMessage: err.message,
        message: msg,
      });
      window.console.error(msg, err);
    });
  }

  private _addTracksToConnection(stream: MediaStream, peerConnection: RTCPeerConnection): void {
    if (!peerConnection.addTrack) {
      return console.error(`[P2P] Your browser does't support RTCPeerConnection#addTrack. Ignored.`);
    }

    stream.getTracks().forEach((track) => {
      peerConnection.addTrack(track, stream);
    });
  }

  private _addStreamToMediaConnection(stream: MediaStream, mediaConnection: PeerMediaConnection): void {
    mediaConnection.addStream(stream);
  }

  private get _connectionInfo(): string {
    return `connection id=${this.connection.connectionId} with ${this.connection.peer}`;
  }

  private _error(err: Error, msg: string) {
    const message = `[P2P] Negotatitor ${msg} | Caused by error (${err.name}) ${err.message}`;
    const conn = this.connection as PeerMediaConnection;

    // not found any subscriptions on current event
    // conn.emit(ConnectionEventType.Error, error);

    conn.provider.emit(PeerErrorType.WebRTC, {
      connection: {
        mediaMode: conn.mediaMode,
        dst: conn.peer,
      },
    });

    window.console.log(message);

    if (conn.isOpen) {
      conn.close();
    }

    clientEntityProvider.getLogger().webrtcP2p.report(LogEvent.WsP2pNegotiator, {
      errorName: err.name,
      errorMessage: err.message,
      message: msg,
    });
  }
}
