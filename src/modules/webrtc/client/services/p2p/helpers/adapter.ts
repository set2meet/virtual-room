/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// @ts-ignore
import { adapter as _ } from 'webrtc-adapter';

export const RTCSessionDescription =
  // @ts-ignore
  window.RTCSessionDescription || window.mozRTCSessionDescription;

export const RTCPeerConnection =
  // @ts-ignore
  window.RTCPeerConnection ||
  // @ts-ignore
  window.mozRTCPeerConnection ||
  // @ts-ignore
  window.webkitRTCPeerConnection;

export const RTCIceCandidate =
  // @ts-ignore
  window.RTCIceCandidate || window.mozRTCIceCandidate;
