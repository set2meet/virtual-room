/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Peer from './peer';
import { EventEmitter } from 'events';
import { SERVICE_EVENT_TYPE } from '../../../../common/types/p2p/constants';

export type PeerAnswerOption = {
  sdpTransform?: (...args: any[]) => void;
};

export type PeerOptions = {
  id: string;
  config: RTCConfiguration;
  socket: SocketIOClient.Socket;
  secure?: boolean;
  debug?: number;
  sessionId: string;
};

export type PeerConnectionInfo = {
  id: string;
  src: string;
  dst: string;
};

export type PeerConnectionError = {
  connection: PeerConnectionInfo;
};

export type PeerRTCConfig = RTCConfiguration & {
  sdpSemantics: string;
};

export type PeerServerMessage = {
  type: SERVICE_EVENT_TYPE;
  payload: any;
  src: string;
};

export enum PeerConnectionType {
  Data = 'data',
  Media = 'media',
}

export enum ConnectionEventType {
  Open = 'open',
  Stream = 'stream',
  StreamUpdated = 'stream-updated',
  Data = 'data',
  Close = 'close',
  Error = 'error',
  IceStateChanged = 'iceStateChanged',
}

export enum PeerEventType {
  Open = 'open',
  Close = 'close',
  Connection = 'connection',
  Call = 'call',
  Disconnected = 'disconnected',
  Error = 'error',
}

export enum PeerErrorType {
  BrowserIncompatible = 'browser-incompatible',
  Disconnected = 'disconnected',
  InvalidID = 'invalid-id',
  InvalidKey = 'invalid-key',
  Network = 'network',
  PeerUnavailable = 'peer-unavailable',
  SslUnavailable = 'ssl-unavailable',
  ServerError = 'server-error',
  SocketError = 'socket-error',
  SocketClosed = 'socket-closed',
  UnavailableID = 'unavailable-id',
  WebRTC = 'webrtc',
}

export abstract class PeerBaseConnection extends EventEmitter {
  protected _open = false;

  public readonly metadata: any;

  public connectionId: string;

  public peerConnection: RTCPeerConnection;

  abstract get type(): PeerConnectionType;

  get open() {
    return this._open;
  }

  constructor(readonly peer: string, public provider: Peer, readonly options: any) {
    super();

    this.metadata = options.metadata;
  }

  public abstract close(): void;

  public abstract handleMessage(message: PeerServerMessage): void;

  public abstract makeRenegotiation(): void;

  public abstract answerOnRenegotiation(oprions: any): void;
}
