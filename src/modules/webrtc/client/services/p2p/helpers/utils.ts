/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { RTCPeerConnection } from './adapter';

const DEFAULT_CONFIG = {
  iceServers: [{ urls: 'stun:stun.l.google.com:19302' }],
  sdpSemantics: 'unified-plan',
};

// tslint:disable-next-line
export class utils {
  public static noop(): void {
    // nothing to do
  }

  // Returns the current browser.
  public static readonly browser: string = ((global) => {
    // @ts-ignore
    if (global.RTCPeerConnection) {
      return 'Supported';
    }

    // @ts-ignore
    if (global.mozRTCPeerConnection) {
      return 'Firefox';
    }
    // @ts-ignore
    if (global.webkitRTCPeerConnection) {
      return 'Chrome';
    }

    return 'Unsupported';
  })(window);

  // Lists which features are supported
  public static readonly supports = (() => {
    if (typeof RTCPeerConnection === 'undefined') {
      return {};
    }

    let data = true;
    let audioVideo = true;

    let binaryBlob = false;
    let sctp = false;
    // @ts-ignore
    const onnegotiationneeded = !!window.webkitRTCPeerConnection;

    let pc;
    let dc;

    try {
      pc = new RTCPeerConnection(DEFAULT_CONFIG, {
        optional: [{ RtpDataChannels: true }],
      });
    } catch (e) {
      data = false;
      audioVideo = false;
    }

    if (data) {
      try {
        dc = pc.createDataChannel('_PEERJSTEST');
      } catch (e) {
        data = false;
      }
    }

    if (data) {
      // Binary test
      try {
        dc.binaryType = 'blob';
        binaryBlob = true;
      } catch (e) {
        // nothing to do
      }

      // Reliable test.
      // Unfortunately Chrome is a bit unreliable about whether or not they
      // support reliable.
      const reliablePC = new RTCPeerConnection(DEFAULT_CONFIG, {});

      try {
        const reliableDC = reliablePC.createDataChannel('_PEERJSRELIABLETEST', {});

        sctp = reliableDC.ordered;
      } catch (e) {
        /* noop */
      }

      reliablePC.close();
    }

    // FIXME: not really the best check...
    if (audioVideo) {
      audioVideo = !!pc.addStream;
    }

    if (pc) {
      pc.close();
    }

    return {
      audioVideo,
      data,
      binaryBlob,
      sctp,
      onnegotiationneeded,
    };
  })();

  // Ensure alphanumeric ids
  public static validateId(id: string): boolean {
    // Allow empty ids
    return !id || /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/.test(id);
  }

  public static randomToken(): string {
    // tslint:disable-next-line
    return Date.now().toString(36);
  }

  public static isSecure(): boolean {
    return location.protocol === 'https:';
  }
}

export const hasActiveTracks = (stream?: MediaStream): boolean => {
  return stream
    ? stream.getTracks().reduce((has, track) => has || (track.readyState === 'live' && !track.muted), false)
    : false;
};

export const stopStream = (stream?: MediaStream): void => {
  if (!stream) {
    return;
  }

  stream.getTracks().forEach((track) => {
    track.stop();
    track.dispatchEvent(new Event('ended'));
  });
};
