/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import EventEmitter from 'events';
import {
  PeerOptions,
  PeerRTCConfig,
  PeerBaseConnection,
  PeerServerMessage,
  PeerErrorType,
  PeerEventType,
} from './types';
import { PeerMediaConnection } from './peer.media-connection';
import { SERVICE_EVENT_MESSAGE, SERVICE_EVENT_TYPE, SERVICE_CONNECTION_MEDIA_MODE } from '../../../../common/types/p2p/constants';

type CloseMediaConnectionOptions = {
  userId?: string;
  onlyWithEnededStreams?: boolean;
};

const { DEVICES: mediaModeDevices } = SERVICE_CONNECTION_MEDIA_MODE;

const DEFAULT_CONFIG = {
  sdpSemantics: 'unified-plan',
};

class Peer extends EventEmitter {
  private _id: string;
  private _lastServerId: string;
  private _config: PeerRTCConfig;
  public readonly sessionId: string;
  private _socket: SocketIOClient.Socket;
  private _destroyed = false; // Connections have been killed
  private _disconnected = false; // Connection to PeerServer killed but P2P connections still active
  private _open = false; // Sockets and such are not yet open.
  private readonly _connections: Map<string, PeerBaseConnection[]> = new Map(); // All connections for this peer.
  private readonly _lostMessages: Map<string, PeerServerMessage[]> = new Map(); // message.src => [ list of messages ]

  constructor(options: PeerOptions) {
    super();

    this._id = options.id;
    this._config = {
      ...DEFAULT_CONFIG,
      ...options.config,
    };
    this.sessionId = options.sessionId;
    this._socket = options.socket;
    this._initializeServerConnection();
  }

  public get rtcConfig() {
    return this._config;
  }

  public get socket() {
    return this._socket;
  }

  public get id() {
    return this._id;
  }

  private _initialize(id: string): void {
    this._id = id;
  }

  private _initializeServerConnection() {
    this._socket.on(SERVICE_EVENT_MESSAGE, this._handleMessage);
    this._socket.on('disconnect', this._handleSocketDisconnect);
    this._socket.on('connect', this._handleSocketConnect);
    this._handleSocketConnect();
  }

  private _handleSocketDisconnect = () => {
    if (!this._disconnected) {
      this.emit(PeerErrorType.Network, 'Lost connection to server.');
      this.disconnect();
    }
  };

  private _handleSocketConnect = () => {
    this._open = true;
  };

  private _handleMessage = (message: PeerServerMessage): void => {
    const { type, payload } = message;
    const peerId = payload.src;

    switch (type) {
      case SERVICE_EVENT_TYPE.PEER_OFFER_REDIRECT: {
        const { src, connectionId } = payload;
        const connection = this.getConnection(src, connectionId);

        if (connection) {
          connection.makeRenegotiation();
        }

        break;
      }
      case SERVICE_EVENT_TYPE.PEER_SHARE_DESKTOP_STOP: {
        const { userId, mediaMode } = payload;

        this.closeMediaConnections(mediaMode, { userId, onlyWithEnededStreams: true });

        break;
      }
      case SERVICE_EVENT_TYPE.PEER_LEAVE: {
        // Another peer has closed its connection to this peer.
        this._cleanupPeer(peerId);
        this._connections.delete(peerId);
        super.emit('peer-leave', peerId);
        break;
      }
      case SERVICE_EVENT_TYPE.PEER_EXPIRE: {
        // The offer sent to a peer has expired without response.
        this.emit(PeerErrorType.PeerUnavailable, `Could not connect to peer ${peerId}`);
        break;
      }
      case SERVICE_EVENT_TYPE.PEER_OFFER: {
        // we should consider switching this to CALL/CONNECT, but this is the least breaking option.
        const connectionId = payload.connectionId;
        // let connection = this.getConnection(peerId, connectionId);
        let connection = this.getConnectionByMediaMode(peerId, payload.connectionMode);

        if (connection) {
          connection.close();
          this.removeConnection(connection);
          // connection.answerOnRenegotiation(payload);
        } // else {
        connection = new PeerMediaConnection(peerId, this, {
          connectionId,
          _payload: payload,
          _mediaMode: payload.connectionMode,
          metadata: payload.metadata,
        });

        this._addConnection(peerId, connection);
        this.emit(PeerEventType.Call, connection);
        // }

        // Find messages.
        const messages = this.getMessages(connectionId);

        for (const msg of messages) {
          connection.handleMessage(msg);
        }

        break;
      }
      case SERVICE_EVENT_TYPE.PEER_ANSWER:
      case SERVICE_EVENT_TYPE.PEER_CANDIDATE: {
        if (!payload) {
          console.warn(`[P2P] You received a malformed message from ${peerId} of type ${type}`);
          return;
        }

        const connectionId = payload.connectionId;
        const connection = this.getConnection(peerId, connectionId);

        if (connection && connection.peerConnection) {
          // Pass it on.
          connection.handleMessage(message);
        } else if (connectionId) {
          // Store for possible later use
          this._storeMessage(connectionId, message);
        } else {
          console.warn('[P2P] You received an unrecognized message:', message);
        }

        break;
      }
    }
  };

  /**
   * Stores messages without a set up connection, to be claimed later.
   */
  private _storeMessage(connectionId: string, message: PeerServerMessage): void {
    if (!this._lostMessages.has(connectionId)) {
      this._lostMessages.set(connectionId, []);
    }

    this._lostMessages.get(connectionId).push(message);
  }

  public getMessages(connectionId: string): PeerServerMessage[] {
    const messages = this._lostMessages.get(connectionId);

    if (messages) {
      this._lostMessages.delete(connectionId);
      return messages;
    }

    return [];
  }

  /**
   * Returns a MediaConnection to the specified peer. See documentation for a
   * complete list of options.
   */
  public call(peerId: string, localStream: MediaStream, mediaMode: SERVICE_CONNECTION_MEDIA_MODE): PeerMediaConnection {
    if (this._disconnected) {
      console.warn(
        '[P2P] You cannot connect to a new Peer because you called' +
          '.disconnect() on this Peer and ended your connection with the' +
          'server. You can create a new Peer to reconnect.'
      );
      this.emit(PeerErrorType.Disconnected, 'Cannot connect to new Peer after disconnecting from server.');
      return;
    }

    if (!localStream) {
      console.error('[P2P] To call a peer, you must provide a stream from your browser');
      return;
    }

    const connection = this.getConnectionByMediaMode(peerId, mediaMode);

    // if connection with peer (and media mode) existed - close it
    if (connection) {
      connection.close();
      this.removeConnection(connection);
    }

    // if no connection with peer and media mode - create one
    const mediaConnection = new PeerMediaConnection(peerId, this, {
      _mediaMode: mediaMode,
      _stream: localStream,
    });

    this._addConnection(peerId, mediaConnection);

    return mediaConnection;
  }

  private _addConnection(peerId: string, connection: PeerBaseConnection): void {
    if (!this._connections.has(peerId)) {
      this._connections.set(peerId, []);
    }

    this._connections.get(peerId).push(connection);
  }

  public removeConnection(connection: PeerBaseConnection): void {
    const connections = this._connections.get(connection.peer);

    if (connections) {
      const index = connections.indexOf(connection);

      if (index !== -1) {
        connections.splice(index, 1);
      }
    }

    // remove from lost messages
    this._lostMessages.delete(connection.connectionId);
  }

  public getConnectionsByMediaMode(mediaMode: SERVICE_CONNECTION_MEDIA_MODE): string[] {
    const connections: string[] = [];

    this._connections.forEach((peerConn, peerId) => {
      const connection = peerConn.filter((conn: PeerMediaConnection) => {
        return conn.mediaMode === mediaMode;
      })[0];

      if (connection) {
        connections.push(peerId);
      }
    });

    return connections;
  }

  public getConnectionByMediaMode(peerId: string, mediaMode: SERVICE_CONNECTION_MEDIA_MODE): PeerMediaConnection {
    const peerConnections = this._connections.get(peerId);

    if (!peerConnections) {
      return null;
    }

    return peerConnections.filter((conn: PeerMediaConnection) => {
      return conn.mediaMode === mediaMode;
    })[0] as PeerMediaConnection;
  }

  public hasConnectionByMediaMode(peerId: string, mediaMode: SERVICE_CONNECTION_MEDIA_MODE): boolean {
    return !!this.getConnectionByMediaMode(peerId, mediaMode);
  }

  /** Retrieve a data/media connection for this peer. */
  public getConnection(peerId: string, connectionId: string): PeerBaseConnection {
    const connections = this._connections.get(peerId);

    if (!connections) {
      return null;
    }

    for (const connection of connections) {
      if (connection.connectionId === connectionId) {
        return connection;
      }
    }

    return null;
  }

  /*private _abort(type: PeerErrorType, message: any): void {
    console.error('Aborting!');

    if (!this._lastServerId) {
      this.destroy();
    } else {
      this.disconnect();
    }

    this.emit(type, message);
  }*/

  /**
   * Destroys the Peer: closes all active connections as well as the connection
   *  to the server.
   * Warning: The peer can no longer create or accept connections after being
   *  destroyed.
   */
  public destroy(): void {
    if (!this._destroyed) {
      this._cleanup();
      this.disconnect();
      this._destroyed = true;
    }
  }

  public cleanUp() {
    this._cleanup();
  }

  /** Disconnects every connection on this peer. */
  private _cleanup(): void {
    for (const peerId of this._connections.keys()) {
      this._cleanupPeer(peerId);
      this._connections.delete(peerId);
    }

    // TODO: correct close peer
    // this.emit(PeerEventType.Close);
  }

  /**
   * Closes all connections to this peer.
   */
  private _cleanupPeer(peerId: string): void {
    const connections = this._connections.get(peerId);

    if (!connections) {
      return;
    }

    for (const connection of connections) {
      connection.close();
    }
  }

  /**
   * Disconnects the Peer's connection to the PeerServer. Does not close any
   *  active connections.
   * Warning: The peer can no longer create or accept connections after being
   *  disconnected. It also cannot reconnect to the server.
   */
  public disconnect(): void {
    setTimeout(() => {
      if (!this._disconnected) {
        this._disconnected = true;
        this._open = false;
        this._lastServerId = this._id;
        this._id = null;
        this.emit(PeerEventType.Disconnected, this._id);
      }
    }, 0);
  }

  /**
   * Attempts to reconnect with the same ID.
   */
  public reconnect(): void {
    if (this._disconnected && !this._destroyed) {
      this._disconnected = false;
      this._initializeServerConnection();
      this._initialize(this._lastServerId);
    } else if (this._destroyed) {
      throw new Error('This peer cannot reconnect to the server. It has already been destroyed.');
    } else if (!this._disconnected && !this._open) {
      // Do nothing. We're still connecting the first time.
      console.error(`[P2P] In a hurry? We're still trying to make the initial connection!`);
    } else {
      throw new Error(`[P2P] Peer ${this._id} cannot reconnect because it is not disconnected from the server!`);
    }
  }

  public replaceTrack(newTrack: MediaStreamTrack, mediaMode: SERVICE_CONNECTION_MEDIA_MODE = mediaModeDevices): void {
    const { kind } = newTrack;

    this._connections.forEach((peerConnections) => {
      peerConnections.forEach((connection) => {
        if ((connection as PeerMediaConnection).mediaMode === mediaMode) {
          const { peerConnection } = connection;

          if (!peerConnection) {
            return;
          }

          const sender = peerConnection.getSenders().filter((rtcSender) => {
            const { track } = rtcSender;

            return track ? track.kind === kind : false;
          })[0];

          if (sender) {
            sender.replaceTrack(newTrack);
          } else {
            console.error(new Error(`[P2P] can't replace track ${kind} - sender don't exists`));
          }
        }
      });
    });
  }

  public closeMediaConnections(
    mediaMode: SERVICE_CONNECTION_MEDIA_MODE,
    options: CloseMediaConnectionOptions = {}
  ): string[] {
    const peerIds: string[] = [];
    const { userId, onlyWithEnededStreams } = options;
    const connectionsToClose: PeerMediaConnection[] = [];

    this._connections.forEach((peerConnections, peerId: string) => {
      if (userId && userId !== peerId) {
        return;
      }

      peerConnections.forEach((connection) => {
        const conn: PeerMediaConnection = connection as PeerMediaConnection;
        const correctMediaMode = conn.mediaMode === mediaMode;
        const closeAnyWay = !onlyWithEnededStreams || (onlyWithEnededStreams && !conn.hasActiveStream());

        // if connection has remote connection - dont close it
        if (correctMediaMode && closeAnyWay) {
          connectionsToClose.push(conn);
        }
      });
    });

    connectionsToClose.forEach((connection) => {
      peerIds.push(connection.peer);
      connection.close();
      this.removeConnection(connection);
    });

    return peerIds;
  }
}

export default Peer;
