/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import WebrtcServiceP2P from './p2p/P2PService';
import { TWebRTCServices } from '../../../../ioc/common/ioc-interfaces';
import { buildOpenTokService } from './opentok/helpers/buildOpenTokService';

const webrtcServicesFactory = async (): Promise<TWebRTCServices> => {
  const result: TWebRTCServices = {};
  const p2pService = new WebrtcServiceP2P();
  const opentokService = await buildOpenTokService();

  if (p2pService) {
    result.p2p = p2pService;
  }
  if (opentokService) {
    result.opentok = opentokService;
  }

  return result;
};

export default webrtcServicesFactory;
