import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { IStoreState } from '../../../../../ioc/client/types/interfaces';
import { WebrtcServiceType } from '../../../../../ioc/common/ioc-constants';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

interface IServiceProviderInnerStateProps {
  status: string | symbol;
  serviceName: WebrtcServiceType;
}

interface IServiceProviderDispatchProps {
  changeWebrtcService: ActionCreator<AnyAction>;
}

type IServiceProviderProps = IServiceProviderInnerStateProps & IServiceProviderDispatchProps;

export class ServiceProvider extends React.Component<IServiceProviderProps> {
  private _requiredChangeService: boolean = false;

  private _changeService() {
    this._resetRequiredStatus();
    this.props.changeWebrtcService(this.props.serviceName);
  }

  private _delayChangeService() {
    this._requiredChangeService = true;
  }

  private _resetRequiredStatus() {
    this._requiredChangeService = false;
  }

  public componentDidMount() {
    const serviceName = this.props.serviceName;

    if (serviceName) {
      this.props.changeWebrtcService(serviceName);
    }
  }

  public componentDidUpdate(prevProps: IServiceProviderProps) {
    const { status } = this.props;
    const oldServiceName: WebrtcServiceType = prevProps.serviceName;
    const newServiceName: WebrtcServiceType = this.props.serviceName;
    const { NONE: statusNone } = clientEntityProvider.getConstants().webrtcConstants.CONNECTION_STATUS;
    const canChangeService: boolean = status === statusNone;
    const needsChangeService: boolean = oldServiceName !== newServiceName;

    if (needsChangeService) {
      // we can change service if status of connection is none
      if (canChangeService) {
        this._changeService();
      } else {
        this._delayChangeService();
      }
    } else {
      // service not changed - but status change
      if (this._requiredChangeService) {
        this._changeService();
      }
    }
  }

  public render() {
    return this.props.children;
  }
}

export default connect<IServiceProviderInnerStateProps, IServiceProviderDispatchProps>(
  (state: IStoreState) => {
    const { webrtcService, state: roomState } = state.room;
    const { services, defaultServiceInx, selectedServiceInx } = state.app.webrtcConfig;
    const defaultWebrtcServiceName = services[defaultServiceInx].name;
    const settingsWebrtcServiceName = services[selectedServiceInx].name;
    const roomWebrtcServiceName = webrtcService.serviceName || defaultWebrtcServiceName;
    const userInTheRoom = !!roomState?.id;
    const isOpenedSession = userInTheRoom && webrtcService.sessionId;
    // if the room the new one - there no webrtc service name in the state
    const webrtcServiceName: WebrtcServiceType = isOpenedSession
      ? roomWebrtcServiceName || settingsWebrtcServiceName
      : settingsWebrtcServiceName;

    return {
      serviceName: webrtcServiceName,
      status: state.webphone.connectionStatus,
    };
  },
  (dispatch) => {
    const actionCreators = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        changeWebrtcService: actionCreators.changeWebrtcService,
      },
      dispatch
    );
  }
)(ServiceProvider);
