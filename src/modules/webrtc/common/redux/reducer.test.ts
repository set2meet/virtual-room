/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import reducer, { INITIAL_STATE } from './reducer';
import { AnyAction } from 'redux';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

beforeAll(setupIoCClientForTest());

describe('webrtc reducer', () => {
  const action: AnyAction = { type: '@@INIT' };

  it('should use INITIAL_STATE for undefined and null incoming state', () => {
    const ownAction: AnyAction = {
      type: clientEntityProvider.getConstants().webrtcConstants.SERVICE_ACTIONS.CHANGE_SERVICE,
      serviceName: '',
    };

    expect(reducer(undefined, action)).toEqual(INITIAL_STATE);
    expect(reducer(null, action)).toEqual(INITIAL_STATE);
    expect(reducer(undefined, ownAction)).toEqual({
      ...INITIAL_STATE,
      serviceName: ownAction.serviceName,
    });
    expect(reducer(null, ownAction)).toEqual({
      ...INITIAL_STATE,
      serviceName: ownAction.serviceName,
    });
  });
});
