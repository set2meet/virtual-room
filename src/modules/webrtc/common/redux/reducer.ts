/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { IWebRTCServiceReduxReducer, IWebRTCServiceReduxState } from './types';
import { webrtcConstants } from '../../../../ioc/common/ioc-constants';

const { SERVICE_ACTIONS } = webrtcConstants;

export const INITIAL_STATE: IWebRTCServiceReduxState = {
  sessionId: null,
  serviceName: null,
};

const reducers = {
  [SERVICE_ACTIONS.CHANGE_SERVICE](state: IWebRTCServiceReduxState, action: AnyAction) {
    return {
      ...state,
      serviceName: action.serviceName,
    };
  },
  [SERVICE_ACTIONS.SESSION_STARTED](state: IWebRTCServiceReduxState, action: AnyAction) {
    return {
      ...state,
      sessionId: action.sessionId,
      serviceName: action.serviceName,
    };
  },
  [SERVICE_ACTIONS.SESSION_ENDED](state: IWebRTCServiceReduxState, action: AnyAction) {
    return {
      ...state,
      sessionId: '',
    };
  },
};

const reducer: IWebRTCServiceReduxReducer = (state, action) => {
  state = state || INITIAL_STATE;
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};

export default reducer;
