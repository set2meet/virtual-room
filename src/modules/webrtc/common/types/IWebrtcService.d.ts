/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { EventEmitter } from 'events';

export interface IWebrtcServiceAccessParams {
  sessionId: string;
  apiKey: string;
  token: string;
}

export type IWebrtcTestCallConfig = {
  audioTrack?: MediaStreamTrack;
  videoTrack?: MediaStreamTrack;
  audioOnly: boolean;
  interval: number;
  duration: number;
  callback?: (result: number) => void;
};

export interface IWebrtcService extends EventEmitter {
  readonly name: string;
  connect: (apiKey: string, sessionId: string, token: string) => void;
  reconnect: (apiKey: string, sessionId: string, token: string) => void;
  disconnect: () => void;
  publisherUpdateStream: (
    userId: string,
    audioSource?: MediaStreamTrack,
    videoSource?: MediaStreamTrack,
    publishAudio?: boolean,
    publishVideo?: boolean
  ) => void;
  toggleAudioStream: (value: boolean) => void;
  toggleVideoStream: (value: boolean) => void;
  subscribe: (event: string | symbol, listener: (...args: any[]) => void) => void;
  unsubscribe: () => void;
  publishDesktop: (userId: string, streamTrack: MediaStreamTrack) => void;
  unpublishDesktop: () => void;
  sendMessage: (type: string, data: string) => void;
  onMessage: (type: string, callback: (data: string) => void) => void;
  updatePublishStreams: (audioTrack: MediaStreamTrack, videoTrack: MediaStreamTrack) => void;
  getUserStreams: () => { [s: string]: MediaStream };
  testCallStart: (
    params: IWebrtcServiceAccessParams,
    config: IWebrtcTestCallConfig,
    onRuntimeError: () => void
  ) => Promise<undefined>;
  testCallStop: () => number;
}

export type TWebRTCServices = Record<string, IWebrtcService>;
