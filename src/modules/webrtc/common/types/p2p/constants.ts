/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { WebrtcServiceType } from '../WebrtcServiceType';

export const SERVICE_NAME = WebrtcServiceType.P2P;
export const REDIS_SESSSIONS_KEY = 'p2p-sessions';
export const SERVICE_EVENT_MESSAGE = 'webrtc-message:p2p';

export enum SERVICE_EVENT_TYPE {
  CONNECT = 'session-connect',
  CONNECT_SUCCESS = 'session-connect-success',
  CONNECT_FAILURE = 'session-connect-failure',
  DISCONNECT = 'session-disconnect',
  CALL_REQUEST = 'call-request',
  PEER_PUBLISH = 'peer-publish',
  PEER_OFFER = 'peer-offer',
  PEER_ANSWER = 'peer-answer',
  PEER_LEAVE = 'peer-leave',
  PEER_CANDIDATE = 'peer-candidate',
  PEER_EXPIRE = 'peer-expire',
  PEER_OFFER_REDIRECT = 'peer-offer-redirect',
  PEER_CONNECTION_FAILED = 'peer-conection-failed',
  PEER_SHARE_DESKTOP = 'peer-share-desktop',
  PEER_SHARE_DESKTOP_STOP = 'peer-share-desktop-stop',
}

export enum SERVICE_CONNECTION_MEDIA_MODE {
  NONE = 0,
  DEVICES = 1,
  DESKTOP = 2,
  ALL = 3,
}
