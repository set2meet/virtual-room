/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IFullscreenActionCreators } from './types/IFullscreenActionCreators';
import { FullscreenAction } from './types/FullscreenAction';

const actionCreators: IFullscreenActionCreators = {
  fullscreenInit: (node: HTMLElement) => {
    return {
      type: FullscreenAction.FULLSCREEN_INIT,
      node,
    };
  },
  fullscreenToggle: () => {
    return {
      type: FullscreenAction.FULLSCREEN_TOGGLE,
    };
  },
  fullscreenSetStatus: (status: boolean) => {
    return {
      type: FullscreenAction.FULLSCREEN_SET_STATUS,
      status,
    };
  },
};

export default actionCreators;
