/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, Reducer } from 'redux';
import { IFullscreenReduxState } from './types/IFullscreenReduxState';
import { FullscreenAction } from './types/FullscreenAction';

const INITIAL_STATE: IFullscreenReduxState = {
  status: false,
  enabled: false,
};

const reducers: Record<string, Reducer<IFullscreenReduxState>> = {};

reducers[FullscreenAction.FULLSCREEN_INIT] = (state, action) => {
  return {
    ...state,
    enabled: action.enabled,
  };
};

reducers[FullscreenAction.FULLSCREEN_SET_STATUS] = (state, action) => {
  return {
    ...state,
    status: action.status,
  };
};

export default (state = INITIAL_STATE, action: AnyAction): IFullscreenReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
