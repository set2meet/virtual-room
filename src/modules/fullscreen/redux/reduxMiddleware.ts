/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _get from 'lodash/get';
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware, TMiddlewareAPI } from '../../../ioc/common/ioc-interfaces';
import actionCreators from './actionCreators';
import Socket = SocketIOClient.Socket;
import { AnyAction, Dispatch, Store } from 'redux';
import FullscreenService from '../services/service';
import { IFullscreenReduxState } from './types/IFullscreenReduxState';
import { FullscreenAction } from './types/FullscreenAction';

let service: FullscreenService;

const getModuleState = (store: TMiddlewareAPI, keyStore: string): IFullscreenReduxState => {
  return _get(store.getState(), keyStore);
};

const middlewareEnum: IAppReduxEnumMiddleware = {
  [FullscreenAction.FULLSCREEN_INIT](store, next, action, socket, storeKey) {
    service = new FullscreenService(action.node);

    next({
      type: action.type,
      enabled: service.enabled,
    });

    service.onFullscreenChange((status) => {
      const state: IFullscreenReduxState = getModuleState(store, storeKey);

      if (state.status !== status) {
        store.dispatch(actionCreators.fullscreenSetStatus(status));
      }
    });
  },
  [FullscreenAction.FULLSCREEN_TOGGLE](store, next, action, socket, storeKey) {
    const state: IFullscreenReduxState = getModuleState(store, storeKey);

    if (state.status) {
      service.turnOff();
    } else {
      service.turnOn();
    }
  },
};

const authMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => {
  return (next: Dispatch<AnyAction>) => async (action: AnyAction) => {
    if (middlewareEnum[action.type]) {
      return middlewareEnum[action.type](store, next, action, socket, storeKey);
    } else {
      next(action);
    }
  };
};

export default authMiddleware;
