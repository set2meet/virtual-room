/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import actionCreators from './redux/actionCreators';
import { IVRModule } from '../../ioc/client/types/interfaces';
import { IFullscreenActionCreators } from './redux/types/IFullscreenActionCreators';
import { FullscreenAction } from './redux/types/FullscreenAction';

interface IFullscreen extends IVRModule<IFullscreenActionCreators> {
  actionTypes: typeof FullscreenAction;
  actionCreators: IFullscreenActionCreators;
}

const fullscreenClientModule: IFullscreen = {
  actionTypes: FullscreenAction,
  actionCreators,
};

export default fullscreenClientModule;
