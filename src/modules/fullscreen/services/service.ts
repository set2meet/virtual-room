/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
interface IFSHTMLElement extends HTMLElement {
  webkitRequestFullscreen: () => void;
  mozRequestFullScreen: () => void;
  msRequestFullscreen: () => void;
}

type FSEventHandler = (status: boolean) => void;

type FullScreenProps = {
  element: string;
  enabled: string;
  request: string;
  change: string;
  exit: string;
};

const getFullscreenProps = (target: IFSHTMLElement): FullScreenProps => {
  if (target.requestFullscreen) {
    return {
      element: 'fullscreenElement',
      enabled: 'fullscreenEnabled',
      request: 'requestFullscreen',
      change: 'fullscreenchange',
      exit: 'exitFullscreen',
    };
  }

  if (target.webkitRequestFullscreen) {
    return {
      element: 'webkitFullscreenElement',
      enabled: 'webkitFullscreenEnabled',
      request: 'webkitRequestFullscreen',
      change: 'webkitfullscreenchange',
      exit: 'webkitExitFullscreen',
    };
  }

  if (target.mozRequestFullScreen) {
    return {
      element: 'mozFullScreenElement',
      enabled: 'mozFullScreenEnabled',
      request: 'mozRequestFullScreen',
      change: 'mozfullscreenchange',
      exit: 'mozCancelFullScreen',
    };
  }

  if (target.msRequestFullscreen) {
    return {
      element: 'msFullscreenElement',
      enabled: 'msFullscreenEnabled',
      request: 'msRequestFullscreen',
      change: 'MSFullscreenChange',
      exit: 'msExitFullscreen',
    };
  }
};

export default class FullScreenService {
  private target: IFSHTMLElement;
  private props: FullScreenProps;
  private listeners: FSEventHandler[] = [];

  private _callListeners(status: boolean) {
    this.listeners.forEach((fn: FSEventHandler) => fn(status));
  }

  private get _isFullScreenMode() {
    return !!(document as any)[this.props.element];
  }

  private _checkFullscreenMode = () => {
    setTimeout(() => {
      this._callListeners(this._isFullScreenMode);
    }, 0);
  };

  constructor(target: IFSHTMLElement) {
    this.target = target;
    this.props = getFullscreenProps(target);
    window.addEventListener(this.props?.change, this._checkFullscreenMode, false);
  }

  public turnOn() {
    if (this.props) {
      (this.target as any)[this.props.request]();
      this._callListeners(true);
    }
  }

  public turnOff() {
    if (this.props) {
      (document as any)[this.props.exit]();
    }
  }

  public get enabled() {
    return this.props ? (document as any)[this.props.enabled] : false;
  }

  public get element() {
    return this.props ? (document as any)[this.props.element] : null;
  }

  public onFullscreenChange(fn: FSEventHandler) {
    this.listeners.push(fn);
  }
}
