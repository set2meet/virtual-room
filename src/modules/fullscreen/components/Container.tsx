import * as React from 'react';
import actionCreators from '../redux/actionCreators';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { IFullscreenContainerProps } from './types/IFullscreenContainerProps';
import { IRequiredTheme, IStoreState } from '../../../ioc/client/types/interfaces';

interface IContainerStateProps {
  status: boolean;
}

type IContainerProps = IFullscreenContainerProps & IContainerStateProps;

const FSContainer = styled.div<IContainerProps>`
  ${({ mode, theme, status }: IRequiredTheme & IContainerProps) => css`
    ${status ? `background: ${theme.backgroundColor}` : ''};
    position: relative;
    text-align: center;
    height: 100%;
    width: 100%;
    padding: 15px;
    margin: 0;
    border: 0;
    left: 0;
    top: 0;

    > div {
      display: inline-block;
      position: relative;
      height: 100%;
      padding: 0;
      margin: 0;
      border: 0;
      ${mode === 'stretch' ? 'width: 100%' : ''};
    }
  `};
`;

interface IFullscreenContainerDispatchProps {
  init: (node: HTMLElement) => void;
}

export class FullScreenContainer extends React.Component<IContainerProps & IFullscreenContainerDispatchProps> {
  private _init = (node: HTMLDivElement) => {
    if (node) {
      this.props.init(node);
    }
  };

  public render() {
    return (
      <FSContainer ref={this._init} mode={this.props.mode} status={this.props.status}>
        <div>{this.props.children}</div>
      </FSContainer>
    );
  }
}

export default connect<IContainerStateProps, IFullscreenContainerDispatchProps>(
  (state: IStoreState) => ({
    status: state.fullscreen.status,
  }),
  {
    init: actionCreators.fullscreenInit,
  }
)(FullScreenContainer);
