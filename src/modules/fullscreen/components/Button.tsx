import * as React from 'react';
import actionCreators from '../redux/actionCreators';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { IFullscreenButtonProps } from './types/IFullscreenButtonProps';
import { IRequiredTheme, IStoreState } from '../../../ioc/client/types/interfaces';
import { ICommonIcons } from '../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const BTTN_SIZE = 32;
const BTTN_POSITION = 10;

type FSButtonToggleProps = IFullscreenButtonProps & {
  status: boolean;
  icons: ICommonIcons;
};

const FSButtonToggle = styled.button<FSButtonToggleProps>`
  ${({ theme, colorMode = 'default', status, icons }: IRequiredTheme & FSButtonToggleProps) => css`
    background-color: ${colorMode === 'default' ? theme.backgroundColorThird : theme.primaryTextColor};
    position: absolute;
    bottom: ${BTTN_POSITION}px;
    right: ${BTTN_POSITION}px;
    border: none;
    padding: 0;
    box-shadow: 1px 1px 6px 2px rgba(0, 0, 0, 0.12);
    height: ${BTTN_SIZE}px;
    width: ${BTTN_SIZE}px;
    text-align: center;
    cursor: pointer;
    outline: none;
    display: inline-block;
    z-index: 3000;

    &:before {
      font-size: 20px;
      font-family: ${icons.fontFamily};
      content: ${status ? icons.code.fullscreenOff : icons.code.fullscreenOn};
      color: ${colorMode === 'default' ? theme.primaryTextColor : theme.backgroundColorSecondary};
    }
  `};
`;

interface IFullscreenButtonStateProps {
  status: boolean;
  className?: string;
}

interface IFullscreenButtonDispatchProps {
  toggle: () => void;
}

export class FullScreenButton extends React.Component<
  IFullscreenButtonProps & IFullscreenButtonStateProps & IFullscreenButtonDispatchProps
> {
  public render() {
    const icons = clientEntityProvider.getIcons();

    return (
      <FSButtonToggle
        icons={icons}
        onClick={this.props.toggle}
        status={this.props.status}
        className={this.props.className}
        colorMode={this.props.colorMode}
      />
    );
  }
}

export default connect<IFullscreenButtonStateProps, IFullscreenButtonDispatchProps>(
  (state: IStoreState) => ({
    status: state.fullscreen.status,
  }),
  {
    toggle: actionCreators.fullscreenToggle,
  }
)(FullScreenButton);
