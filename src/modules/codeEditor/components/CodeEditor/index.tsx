import * as React from 'react';
import { ICodeEditorProps } from './ICodeEditorProps';
import styled from 'styled-components';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import templateString from './template.html';
import template from 'lodash/template';

const StyledIFrame = styled.iframe`
  border: 0;
  width: 100%;
  height: 100%;
`;

// TODO
// assets/modules/CodeEditor/editor.html has config to connect to FireBase (for collaborating)
// rewrite to local server (huge task) or change config for each customer
export class CodeEditor extends React.Component<ICodeEditorProps> {
  public shouldComponentUpdate(
    nextProps: Readonly<ICodeEditorProps>,
    nextState: Readonly<{}>,
    nextContext: any
  ): boolean {
    return false;
  }

  public render() {
    const source = template(templateString)({
      ...clientEntityProvider.getConfig().codeEditor,
      roomId: this.props.roomId,
    });

    return <StyledIFrame srcDoc={source} />;
  }
}

export default CodeEditor;
