/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as Router from 'koa-router';
import rp from 'request-promise';
import fs from 'fs';
import path from 'path';
import { IAppPresentationsServiceConfig } from '../../../ioc/common/types/config/IAppPresentationServiceConfig';
import serverEntityProvider from '../../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';

const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const OK = 200;

export default (router: Router) => {
  const serviceConfig: IAppPresentationsServiceConfig = serverEntityProvider.getConfig().presentations?.service;

  if (!serviceConfig) {
    console.log('\nError: not found config for presentations converter service');
    process.exit(1);
  }

  router.post('/presentations/upload', async (ctx, next) => {
    const presentationFile = (ctx.request as any).files.file;
    const waitTillFileReady = new Promise((resolve, reject) => {
      const socket = require('socket.io-client')(serviceConfig.wss, {
        transports: ['websocket'], // To fix XHR Polling Error https://github.com/socketio/socket.io-client/issues/1097
        rejectUnauthorized: false, // TODO Need to sign https connection with certificate
      });

      socket.on('connect', () => {
        if (presentationFile) {
          socket.emit('filename', presentationFile.name);
          socket.emit('filesize', presentationFile.size);
        } else {
          const errMsg = 'Error: No file provided';
          console.log(errMsg);
          socket.disconnect();
          reject(errMsg);
        }
      });

      socket.on('ready', () => {
        const stream = fs.createReadStream(presentationFile.path);

        stream.on('readable', () => {
          socket.emit('filechunks', stream.read());
        });

        stream.on('end', () => {
          socket.emit('end', 'file has been send');
        });
      });

      socket.on('done', (data: any) => {
        socket.disconnect();
        resolve(true);
      });

      socket.on('connect_error', () => {
        const errMsg = 'Error: Cant Open Socket';
        console.log(errMsg);
        socket.disconnect();
        reject(errMsg);
      });

      socket.on('stream_error', () => {
        const errMsg = 'Error: Cant write presentation file';
        console.log(errMsg);
        socket.disconnect();
        reject(errMsg);
      });

      socket.on('convert_error', () => {
        const errMsg = 'Error: Cant convert file';
        console.log(errMsg);
        socket.disconnect();
        reject(errMsg);
      });
    });

    await waitTillFileReady
      .then(() => {
        const ext = path.extname(path.resolve(presentationFile.name)); // get extension
        const filename = path.basename(path.resolve(presentationFile.name), ext); // get basename w/o extension

        ctx.status = OK;
        ctx.body = `${filename}.pdf`;
      })
      .catch((e) => {
        ctx.body = 'Cant Connect to Presentation Service! ' + e;
        ctx.status = BAD_REQUEST;
      });

    await next();
  });
  router.get('/presentations/:filename', async (ctx, next) => {
    const ext = path.extname(path.resolve(ctx.params.filename)); // get extension
    const filename = path.basename(path.resolve(ctx.params.filename), ext); // get basename w/o extension

    console.log(`${serviceConfig.url + serviceConfig.path.download}/${filename}.pdf --- === ---`);
    ctx.body = await rp
      .get(`${serviceConfig.url + serviceConfig.path.download}/${filename}.pdf`, {
        method: 'GET',
        encoding: null,
        rejectUnauthorized: false, // TODO Need to sign https connection with certificate
        headers: {
          'Content-type': 'application/pdf',
        },
      })
      .then(
        (result) => result,
        () => {
          ctx.status = NOT_FOUND;
          return 'Presentation Not Found! Unable to Download file';
        }
      );

    await next();
  });
};
