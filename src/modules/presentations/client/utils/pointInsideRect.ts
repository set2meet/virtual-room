/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export function pointInsideRect(point: any, rect: any) {
  const left = rect.left || rect.x;
  const top = rect.top || rect.y;

  return point.x >= left && point.x <= left + rect.width && point.y >= top && point.y <= top + rect.height;
}
