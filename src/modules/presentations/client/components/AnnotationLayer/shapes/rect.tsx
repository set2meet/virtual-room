import React from 'react';

export default class Rect extends React.PureComponent<any> {
  public prepareData() {
    const { x: sx, y: sy } = this.props.path[0];
    const { x: ex, y: ey } = this.props.path[this.props.path.length - 1];

    const rect = {
      x: Math.min(sx, ex),
      y: Math.min(sy, ey),
      width: Math.abs(ex - sx),
      height: Math.abs(ey - sy),
    };

    return rect;
  }

  public render() {
    const rect = this.prepareData();

    return (
      <rect
        x={rect.x}
        y={rect.y}
        width={rect.width}
        height={rect.height}
        stroke={this.props.color}
        strokeWidth={this.props.strokeWidth}
        fill="none"
      />
    );
  }
}
