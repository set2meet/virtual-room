import React from 'react';
import { connect } from 'react-redux';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { v4 as guid } from 'uuid';
import _get from 'lodash/get';
import _throttle from 'lodash/throttle';
import styled from 'styled-components';
import { styledSvg } from './AnnotationLayer.style';
import { IStoreState } from '../../../../../ioc/client/types/interfaces';
import actionCreators from '../../../common/redux/actionCreators';
import { getCurrentPageAnnotations, getSelectedColor, getSelectedTool } from '../../redux/selectors';
import { pointInsideRect } from '../../utils/pointInsideRect';
import Line from './shapes/line';
import Rect from './shapes/rect';
import Ellipse from './shapes/ellipse';
import Pen from './shapes/pen';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IPresentationsReduxState } from '../../../common/redux/types/IPresentationsReduxState';
import { ICanvasViewport } from '../../../common/types/ICanvasViewport';
import { PresentationTool } from '../../types/constants/PresentationTool';
import { ILocalPresentationAnnotationToolsReduxState } from '../../redux/types/localReduxStateTypes';

interface IAnnotationLayerProps {
  page: number;
  localStatePath: string;
  roomStatePath: string;
}

interface IAnnotationLayerDispatchProps {
  updateAnnotationTools: ActionCreator<AnyAction>;
  sendAction: ActionCreator<AnyAction>;
}

interface IAnnotationLayerStateProps {
  localState: ILocalPresentationAnnotationToolsReduxState;
  roomState: IPresentationsReduxState;
  shapes: any[];
  selectedTool: string;
  color: string;
  isModerator: boolean;
  viewport: ICanvasViewport;
}

type IAnnotationLayer = IAnnotationLayerProps & IAnnotationLayerStateProps & IAnnotationLayerDispatchProps;

type TEventListener = 'addEventListener' | 'removeEventListener';

const STROKE_WIDTH = 3;
const THROTTLE_TIMEOUT = 50;

const AnnotationLayerSvg = styled.svg`
  ${styledSvg};
`;

export const mapTools: any = {};

mapTools[PresentationTool.LINE] = Line;
mapTools[PresentationTool.RECT] = Rect;
mapTools[PresentationTool.ELLIPSE] = Ellipse;
mapTools[PresentationTool.PEN] = Pen;

class AnnotationLayer extends React.Component<IAnnotationLayer> {
  private startPoint: { x: number; y: number };
  private rect: ClientRect;
  private _svg: SVGElement;

  constructor(props: any) {
    super(props);

    this.startPoint = null;
  }

  public componentDidMount() {
    if (this.props.isModerator) {
      this.manageEventHandlers('addEventListener');
    }

    this.onResize();
  }

  public componentDidUpdate(prevProps: IAnnotationLayerProps) {
    if (prevProps.page !== this.props.page) {
      this.startPoint = null;
    }
  }

  public componentWillUnmount(): void {
    this.manageEventHandlers('removeEventListener');
  }

  public manageEventHandlers(action: TEventListener): void {
    const link = this._svg;

    link[action]('mousedown', this.mouseDown);
    link[action]('mousemove', this.mouseMove);
    link[action]('mouseup', this.mouseUp);
    link[action]('mouseenter', this.onResize);
  }

  private onResize = () => {
    if (this._svg) {
      this.rect = this._svg.getBoundingClientRect();
    }
  };

  private mousePos = (e: MouseEvent): { x: number; y: number } => {
    const round = 2;
    const scaleFactor = this.props.viewport ? this.props.viewport.width / this.rect.width : 0;

    return {
      x: (round * Math.round(e.clientX / round) - this.rect.left) * scaleFactor,
      y: (round * Math.round(e.clientY / round) - this.rect.top) * scaleFactor,
    };
  };

  private mouseDown = (e: MouseEvent) => {
    this.onResize();

    if (pointInsideRect({ x: e.clientX, y: e.clientY }, this.rect)) {
      this.startPoint = this.mousePos(e);

      const mouseTracker = {
        id: guid().toString(),
        type: this.props.selectedTool,
        path: [this.mousePos(e)],
        color: this.props.color,
      };

      this.props.updateAnnotationTools({ mouseTracker });
    }
  };

  private mouseMove = (e: MouseEvent) => {
    const { localState } = this.props;

    if (this.startPoint && localState.mouseTracker) {
      const { mouseTracker } = localState;

      mouseTracker.path = [...localState.mouseTracker.path, this.mousePos(e)];
      this.props.updateAnnotationTools({ mouseTracker });
    }
  };

  private mouseUp = (e: MouseEvent) => {
    const { localState, shapes } = this.props;

    if (localState.mouseTracker) {
      const lastPos = this.mousePos(e);

      if (localState.mouseTracker) {
        localState.mouseTracker.path = [...localState.mouseTracker.path, lastPos];

        shapes.push(Object.assign({}, localState.mouseTracker));
      }

      this._updateAnnotationPage(shapes);
    }

    this.startPoint = null;
  };

  private _updateAnnotationPage(shapes: any[]) {
    // additional check to prevent non-moderator from modifying annotations
    if (!this.props.isModerator) {
      return;
    }
    this.props.sendAction(actionCreators.updateAnnotationPage(this.props.page, shapes));
  }

  public render() {
    let viewBox;

    if (this.props.viewport) {
      const { width, height } = this.props.viewport;

      viewBox = `0 0 ${width} ${height}`;
    }
    const sharedState = this.props.localState;
    const shapes = this.props.shapes.map((shape: any) => {
      if (!shape.id) {
        // for backward compatibility
        return null;
      }
      const path = shape.path;
      const ShapeClass = mapTools[shape.type];

      if (!ShapeClass) {
        console.error(`no react view for figure ${shape}`);
      }

      return ShapeClass ? (
        <ShapeClass key={shape.id} path={path} color={shape.color} strokeWidth={STROKE_WIDTH} />
      ) : null;
    });

    let currentDrawingShape = null;

    if (this.startPoint && sharedState && sharedState.mouseTracker && sharedState.mouseTracker.type) {
      const MouseTrackerClass = mapTools[sharedState.mouseTracker.type];

      if (MouseTrackerClass) {
        currentDrawingShape = (
          <MouseTrackerClass
            key={sharedState.mouseTracker.id}
            color={sharedState.mouseTracker.color}
            strokeWidth={STROKE_WIDTH}
            path={sharedState.mouseTracker.path}
          />
        );
      }
    }

    return (
      <AnnotationLayerSvg id="annotationLayer" ref={(canvas: any) => (this._svg = canvas)} viewBox={viewBox}>
        {shapes}
        {currentDrawingShape}
      </AnnotationLayerSvg>
    );
  }
}

export default connect(
  (state: IStoreState, ownProps: IAnnotationLayerProps) => {
    const selectors = clientEntityProvider.getSelectors();
    const localState = _get(state, ownProps.localStatePath);
    const roomState = _get(state, ownProps.roomStatePath);

    return {
      localState,
      shapes: getCurrentPageAnnotations(roomState, ownProps.page),
      selectedTool: getSelectedTool(localState),
      color: getSelectedColor(localState),
      isModerator: selectors.appSelectors.isOwner(state),
      viewport: roomState.viewport,
    };
  },
  (dispatch) => ({
    sendAction: bindActionCreators(clientEntityProvider.appActionCreators.sendRoomAction, dispatch),
    updateAnnotationTools: _throttle(
      bindActionCreators(actionCreators.updateAnnotationTools, dispatch),
      THROTTLE_TIMEOUT
    ),
  })
)(AnnotationLayer);
