/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import toolBoxIcons from './AnnotationTools.icons.svg';
import { TOOLS_DATA, SWITCH } from './AnnotationTools.data';
import { PresentationTool } from '../../types/constants/PresentationTool';
import { css, FlattenSimpleInterpolation } from 'styled-components';
import { IToolItem } from './IToolItem';

const toolBoxHeight = 34;
const toolBoxMargin = 8;
const toolBoxInnerPadding = 8;
const toolBoxItemSize = toolBoxHeight - toolBoxInnerPadding;
const toolBoxIconSize = 32;
const toolBoxSvgHeight = 18;

export const styledAnnotationToolsContainer = () => css`
  top: 10px;
  right: 10px;
  height: ${toolBoxHeight}px;
  position: absolute;
  display: inline-flex;
  align-items: flex-start;
  z-index: 3;
`;

export const styledToolButtonContainer = () => css`
  height: ${toolBoxHeight}px;
  background-color: #fff;
  box-shadow: 0 1px 4px 2px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  border-radius: 2px 0 0 2px;

  > hr {
    width: 1px;
    height: ${toolBoxSvgHeight}px;
    margin: ${toolBoxInnerPadding}px;
    border: none;
    background-color: #a7a7a7;
  }
`;

export interface IStyledToolBoxButton {
  selectedTool: boolean;
}

interface IStyledSwitchContainer {
  isCollapsed: boolean;
}

const toolBoxSelectedButtonDelta = 3;

const styledToolBoxButtonSelected = css`
  opacity: 1;

  &:before {
    content: ' ';
    position: absolute;
    top: 0;
    left: 0;
    border-radius: 4px;
    width: ${toolBoxHeight - 2 * toolBoxSelectedButtonDelta}px;
    height: ${toolBoxHeight - 2 * toolBoxSelectedButtonDelta}px;
    background-color: rgba(0, 0, 0, 0.1);
  }
`;

const styledToolBoxButtonIcons = TOOLS_DATA.filter((rec) =>
  Object.values(PresentationTool).includes(rec.id as PresentationTool)
).reduce((accumulatedCss: FlattenSimpleInterpolation, tool: IToolItem, inx: number) => {
  return css`
        ${accumulatedCss}
      &[name="${tool.id}"] {
        background-position: 0 -${inx * toolBoxIconSize}px;
      }
    `;
}, css``);

const toolboxButtonStyle = css`
  background-image: url(${toolBoxIcons});
  background-repeat: no-repeat;
  background-position: 0 0;
  background-size: 32px 288px;
  background-color: transparent;
  position: relative;

  outline: none;
  margin: 0 4px;
  border: 0;
  padding: 0;
  border-radius: 4px;
  width: ${toolBoxItemSize}px;
  height: ${toolBoxItemSize}px;
  cursor: pointer;
`;

export const styledToolBoxButton = ({ selectedTool }: IStyledToolBoxButton) => css`
  ${toolboxButtonStyle};

  &:not(:last-child) {
    margin-right: ${toolBoxMargin}px;
  }

  ${styledToolBoxButtonIcons};
  ${selectedTool ? styledToolBoxButtonSelected : ''};
`;

export const styledSwitch = () => css`
  ${toolboxButtonStyle};
  
  &[name="${SWITCH}"] {
    background-position: 0 -164px;
  }
`;

export const styledSwitchContainer = ({ isCollapsed }: IStyledSwitchContainer) => css`
  background-color: #575757;
  width: ${toolBoxHeight};
  height: ${toolBoxHeight};
  display: flex;
  align-items: center;
  text-align: center;
  box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
  border-radius: ${isCollapsed ? '2px' : '0 2px 2px 0'};
`;
