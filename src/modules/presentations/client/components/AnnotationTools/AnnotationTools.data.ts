/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { PresentationTool } from '../../types/constants/PresentationTool';
import { IToolItem } from './IToolItem';

export const SWITCH = 'switch';
export const STUFF_SEPARATOR = 'separator';
export const PALETTE = 'palette';

export const TOOLS_DATA: IToolItem[] = [
  { id: PresentationTool.PEN, name: 'pen' },
  { id: PresentationTool.RECT, name: 'rect' },
  { id: PresentationTool.ELLIPSE, name: 'ellipse' },
  { id: PresentationTool.LINE, name: 'line' },
  { id: PALETTE, name: 'palette' },
  { id: PresentationTool.ERASER, name: 'eraser' },
];
