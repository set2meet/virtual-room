import React, { MouseEvent } from 'react';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _get from 'lodash/get';

import {
  IStyledToolBoxButton,
  styledAnnotationToolsContainer,
  styledSwitch,
  styledSwitchContainer,
  styledToolBoxButton,
  styledToolButtonContainer,
} from './AnnotationTools.style';
import styled from 'styled-components';
import { PALETTE, STUFF_SEPARATOR, SWITCH, TOOLS_DATA } from './AnnotationTools.data';
import { IAppStateProps } from '../../../../../ioc/client/types/interfaces';
import actionCreators from '../../../common/redux/actionCreators';
import Palette from '../Palette';
import { IToolItem } from './IToolItem';

const ToolButton = styled.button<IStyledToolBoxButton>`
  ${styledToolBoxButton};
`;
const ToolButtonContainer = styled.div`
  ${styledToolButtonContainer};
`;
const Switch = styled.button`
  ${styledSwitch};
`;
const SwitchContainer = styled.div`
  ${styledSwitchContainer};
`;

interface IAnnotationToolsProps {
  localState: any;
  isModerator: boolean;
}

interface IAnnotationToolsStateProps {
  color: string;
  selectedTool: string;
  isCollapsed: boolean;
}

interface IDispatchAnnotationToolsProps {
  updateAnnotationTools: ActionCreator<AnyAction>;
}

class AnnotationTools extends React.Component<
  IAnnotationToolsProps & IAnnotationToolsStateProps & IDispatchAnnotationToolsProps
> {
  private _changeTool = (evt: MouseEvent<HTMLButtonElement>) => {
    const toolId = (evt.target as HTMLButtonElement).name;

    this.props.updateAnnotationTools({ selectedTool: toolId });
  };

  private _togglePanel = () => {
    this.props.updateAnnotationTools({ isCollapsed: !this.props.isCollapsed });
  };

  public render() {
    const { selectedTool } = this.props;
    const AnnotationToolsContainer = styled.div`
      ${styledAnnotationToolsContainer};
    `;

    return (
      <AnnotationToolsContainer>
        {!this.props.isCollapsed && (
          <ToolButtonContainer>
            {TOOLS_DATA.map(({ id }: IToolItem, inx: number) => {
              if (id === STUFF_SEPARATOR) {
                return <hr key={inx} />;
              }

              if (id === PALETTE) {
                return <Palette key={id} color={this.props.color} />;
              }

              return <ToolButton key={id} name={id} onClick={this._changeTool} selectedTool={selectedTool === id} />;
            })}
          </ToolButtonContainer>
        )}
        <SwitchContainer isCollapsed={this.props.isCollapsed}>
          <Switch name={SWITCH} onClick={this._togglePanel} />
        </SwitchContainer>
      </AnnotationToolsContainer>
    );
  }
}

export default connect<IAnnotationToolsStateProps, IDispatchAnnotationToolsProps, IAnnotationToolsProps>(
  (state: IAppStateProps, ownProps: IAnnotationToolsProps) => {
    return {
      selectedTool: _get(ownProps.localState, 'selectedTool'),
      color: _get(ownProps.localState, 'color'),
      isCollapsed: _get(ownProps.localState, 'isCollapsed'),
    };
  },
  (dispatch) => ({
    updateAnnotationTools: bindActionCreators(actionCreators.updateAnnotationTools, dispatch),
  })
)(AnnotationTools);
