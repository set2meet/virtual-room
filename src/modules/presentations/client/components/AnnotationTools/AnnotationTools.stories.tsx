import AnnotationTools from './AnnotationTools';
import React from 'react';
import { boolean, color, select } from '@storybook/addon-knobs';
import { PresentationTool } from '../../types/constants/PresentationTool';
import { DEFAULT_ANNOTATION_COLOR } from '../../types/constants/common';

export default {
  component: AnnotationTools,
  title: 'presentations/AnnotationTools',
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const Default = () => (
  <AnnotationTools
    isModerator={boolean('isModerator', true)}
    localState={{
      selectedTool: select('selectedTool', PresentationTool, PresentationTool.LINE),
      color: color('color', DEFAULT_ANNOTATION_COLOR),
      isCollapsed: boolean('isCollapsed', false),
    }}
  />
);
