import * as React from 'react';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import styled, { css } from 'styled-components';

import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

import { getStorePath } from '../../redux/stateConfig';
import actionCreators from '../../../common/redux/actionCreators';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const PagesControlPanelContainer = styled.div`
  ${({ theme }: IRequiredTheme) => css`
    min-width: 100px;
    height: 34px;
    box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
    background-color: #f8f8f8;
    display: flex;
    align-items: center;

    & > input {
      display: none;
    }

    & > div {
      & > button,
      & > button:not(:disabled):hover {
        background-color: #f8f8f8;
        padding: 0 15px;

        &:before {
          color: ${theme.backgroundColor};
        }
      }
    }
  `};
`;

const PagesContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  line-height: 34px;
`;

export interface IPagesControlPanelStateProps {
  currentPage: number;
  maxPages: number;
  isOwner: boolean;
  isLoading: boolean;
  isRendering: boolean;
}

export interface IPagesControlPanelDispatchProps {
  sendAction: ActionCreator<AnyAction>;
}

type PagesControlPanelProps = IPagesControlPanelStateProps & IPagesControlPanelDispatchProps;

class PagesControlPanel extends React.Component<PagesControlPanelProps> {
  private _goNextPage = () => {
    this.props.sendAction(actionCreators.setPage(Math.min(this.props.currentPage + 1, this.props.maxPages)));
  };

  private _goPrevPage = () => {
    this.props.sendAction(actionCreators.setPage(Math.max(this.props.currentPage - 1, 1)));
  };

  public render() {
    const { SimpleToggleButton } = clientEntityProvider.getComponentProvider().UI;

    if (this.props.isLoading || this.props.isRendering) {
      return null;
    }
    return (
      <PagesControlPanelContainer>
        {this.props.isOwner && (
          <SimpleToggleButton icon="arrowLeft" fontSize="22px" buttonSize="34px" onClick={this._goPrevPage} />
        )}
        <PagesContainer>
          {this.props.currentPage} / {this.props.maxPages}
        </PagesContainer>
        {this.props.isOwner && (
          <SimpleToggleButton icon="arrowRight" fontSize="22px" buttonSize="34px" onClick={this._goNextPage} />
        )}
      </PagesControlPanelContainer>
    );
  }
}

export default connect(
  (state: IStoreState) => {
    const selectors = clientEntityProvider.getSelectors();
    const localState = _get(state, getStorePath());
    const roomState = _get(state, getStorePath());

    return {
      isLoading: roomState.isLoading,
      isRendering: roomState.isRendering,
      isOwner: selectors.appSelectors.isOwner(state),
      currentPage: _get(localState, 'pages.current', 0),
      maxPages: _get(localState, 'pages.max', 0),
    };
  },
  (dispatch: any) => ({
    sendAction: bindActionCreators(clientEntityProvider.getAppActionCreators().sendRoomAction, dispatch),
  })
)(PagesControlPanel);
