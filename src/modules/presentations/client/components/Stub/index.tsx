import React from 'react';
import styled from 'styled-components';

import backgroundImage from './empty-state.svg';

const StubContainer = styled.div`
  display: ${(props) => (props.hidden ? 'none' : 'flex')};
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const Image = styled.div`
  width: 170px;
  height: 260px;
  background-image: url(${backgroundImage});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  margin-bottom: 30px;
`;

const Header = styled.div`
  font-weight: bold;
  font-size: 20px;
  margin-bottom: 10px;
`;

const Sub = styled.div``;

export interface IStubProps {
  isOwner: boolean;
  isVisible?: boolean;
}

const Stub = ({ isOwner, isVisible = true }: IStubProps) => {
  if (isVisible) {
    return (
      <StubContainer>
        <Image />
        <Header>{isOwner ? 'Upload your file to share' : 'Waiting for host to share presentation'}</Header>
        {isOwner && <Sub>Supported file types: .pptx, .ppt, .pdf</Sub>}
        {isOwner && <Sub>Max file size: 5 MB</Sub>}
      </StubContainer>
    );
  }
  return null;
};

export default Stub;
