import React, { createRef } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import _debounce from 'lodash/debounce';
import { getLocalStorePath, getStorePath } from '../../redux/stateConfig';
import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import PDFJS from 'pdfjs-dist';
import pdfjsWorker from 'pdfjs-dist/build/pdf.worker.entry';

import { PDFDocumentProxy, PDFPageProxy } from 'pdfjs-dist';
import resolveUrl from 'resolve-url';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import actionCreators from '../../../common/redux/actionCreators';

import './text_layer_builder.css';
import { PDFRenderTask } from 'pdfjs-dist';
import AnnotationLayer from '../AnnotationLayer';
PDFJS.GlobalWorkerOptions.workerSrc = pdfjsWorker;

// tslint:disable-next-line
const TextLayerBuilder = require('pdfjs-dist/lib/web/text_layer_builder').TextLayerBuilder as any;

const PresentationViewContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const TextLayer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
`;

const CanvasContainerStyled = styled.div`
  position: relative;
`;

const CanvasContainer = (props: { children: React.ReactNode; isVisible: boolean }) => {
  return <CanvasContainerStyled hidden={props.isVisible}>{props.children}</CanvasContainerStyled>;
};

const RESIZE_DEBOUNCE_MS = 300;

export interface IPresentationViewDispatchProps {
  sendAction: ActionCreator<AnyAction>;
  showNotification: ActionCreator<AnyAction>;
  setLocalPresentation: ActionCreator<AnyAction>;
  setPresentationRendering: ActionCreator<AnyAction>;
}

export interface IPresentationViewStateProps {
  uploadedPresentationLink: string;
  page: number;
  currentDoc: PDFDocumentProxy;
  currentPage: PDFPageProxy;
  currentLink: string;
  isOwner: boolean;
  isLoading: boolean;
  isRendering: boolean;
  shared: boolean;
}

const Indicator = styled.div`
  height: 100%;
  display: ${(props) => (props.hidden ? 'none' : 'flex')};
  align-items: center;
  justify-content: center;
  font-size: 18px;
`;

interface ILoader {
  isVisible: boolean;
}

export const Loader = (props: ILoader) => {
  const { Spinner } = clientEntityProvider.getComponentProvider().UI;

  if (props.isVisible) {
    return (
      <Indicator>
        <Spinner />
        Processing presentation...
      </Indicator>
    );
  }
  return null;
};

class PresentationView extends React.Component<IPresentationViewStateProps & IPresentationViewDispatchProps> {
  private canvasRef = createRef<HTMLCanvasElement>();
  private textLayerRef = createRef<HTMLDivElement>();
  private ContainerRef = createRef<HTMLDivElement>();
  private renderArray: PDFRenderTask[] = [];

  constructor(props: IPresentationViewStateProps & IPresentationViewDispatchProps) {
    super(props);

    window.addEventListener('resize', this.redrawPage);
  }

  public async componentDidMount(): Promise<void> {
    this.props.setPresentationRendering(true);
    if (this.props.currentLink !== this.props.uploadedPresentationLink && this.props.uploadedPresentationLink !== '') {
      await this._loadDocument(this.props.uploadedPresentationLink, this.props.currentLink ? 1 : this.props.page);
    } else {
      // force sync page number
      if (this.props.page !== _get(this.props, 'currentPage.pageIndex', 0) + 1) {
        this._setPage(this.props.page);
      }
      await this._drawPage();
    }
  }

  public async componentDidUpdate(prevProps: IPresentationViewStateProps) {
    // perform all queued renders beforehand
    await Promise.all(this.renderArray);

    this.renderArray = [];

    if (
      this.props.uploadedPresentationLink &&
      this.props.uploadedPresentationLink !== prevProps.uploadedPresentationLink
    ) {
      await this._loadDocument(this.props.uploadedPresentationLink, 1);
    }

    if (this.props.currentDoc !== prevProps.currentDoc) {
      await this._setPage(this.props.page);
    }

    // async parse new page(page number change)
    if (this.props.page !== prevProps.page) {
      await this._setPage(this.props.page);
    }

    // draw if state of parsed page changed
    if (this.props.currentPage !== prevProps.currentPage) {
      // use debounced function to avoid frequent page changes
      await this.redrawPage();
    }
  }

  public componentWillUnmount(): void {
    window.removeEventListener('resize', this.redrawPage);
  }

  private redrawPage = _debounce(async () => {
    await this._drawPage();
  }, RESIZE_DEBOUNCE_MS);

  private _loadDocument = async (url: string, initialPage: number) => {
    if (!url) {
      return false;
    }

    const { service } = clientEntityProvider.getConfig();
    const docUrl = resolveUrl(service.url, service.path, 'presentations/', url);

    const httpClient = clientEntityProvider.getHttpClient();

    try {
      const response = await httpClient.get(docUrl, { responseType: 'arraybuffer' });
      let currentDoc;

      try {
        currentDoc = (await PDFJS.getDocument({ data: response.data })) as any;
      } catch (e) {
        this.props.showNotification({
          hideProgressBar: true,
          type: 'error',
          content: `Error while render presentation. No worker or data is spoiled`,
        });
        if (this.props.isOwner) {
          this.props.sendAction(actionCreators.resetPresentationState());
          this._setViewport();
        }

        return false;
      }

      if (this.props.isOwner) {
        this.props.sendAction(actionCreators.setNumberOfPages(currentDoc.numPages, initialPage));

        this._setViewport();
      }

      this.props.setLocalPresentation({
        currentDoc,
        currentLink: url,
      });
    } catch {
      this.props.showNotification({
        hideProgressBar: true,
        type: 'error',
        content: `Error loading presentation: ${docUrl}`,
      });
      if (this.props.isOwner) {
        this.props.sendAction(actionCreators.resetPresentationState());
        this._setViewport();
      }
    }
  };

  private _setViewport = () => {
    const timeout = 500; // cuz we take ref. And ref will be unavailable until it not render. TODO:Maybe ref later

    setTimeout(() => {
      this.props.sendAction(
        actionCreators.setViewport({
          width: this.canvasRef.current.width,
          height: this.canvasRef.current.height,
        })
      );
    }, timeout);
  };

  private _setPage = async (page: number) => {
    if (!this.props.currentDoc) {
      return;
    }

    let currentPage;

    try {
      currentPage = await this.props.currentDoc.getPage(page);
    } catch {
      currentPage = await this.props.currentDoc.getPage(1);
    } finally {
      this.props.setLocalPresentation({ currentPage });
    }
  };

  private _drawPage = async () => {
    if (!this.props.currentPage) {
      return;
    }
    const canvas = this.canvasRef.current;
    const textLayer = this.textLayerRef.current;

    const canvasContext = canvas.getContext('2d');

    if (canvasContext) {
      while (textLayer.firstChild) {
        textLayer.removeChild(textLayer.firstChild);
      }

      const { offsetWidth, offsetHeight } = this.ContainerRef.current;

      const unscaledViewport = (this.props.currentPage as any).getViewport(1);
      const scale = Math.min(offsetHeight / unscaledViewport.height, offsetWidth / unscaledViewport.width);

      const viewport = (this.props.currentPage as any).getViewport(scale);

      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // this.setViewport() // cause memory leak;

      this.renderArray.push(
        this.props.currentPage.render({
          canvasContext,
          viewport,
        })
      );
      const textLayerInstance = new TextLayerBuilder({
        textLayerDiv: textLayer,
        pageIndex: this.props.currentPage.pageNumber,
        viewport,
      });

      const textContent = await this.props.currentPage.getTextContent();

      textLayerInstance.setTextContent(textContent);
      this.renderArray.push(textLayerInstance.render());
      this.props.setPresentationRendering(false);
    }
  };

  private _isLoading = () => this.props.isLoading || this.props.isRendering;

  public render() {
    return (
      <PresentationViewContainer ref={this.ContainerRef}>
        <Loader isVisible={this._isLoading()} />

        <CanvasContainer isVisible={this._isLoading()}>
          <canvas ref={this.canvasRef} />
          <TextLayer ref={this.textLayerRef} className="textLayer" />
          <AnnotationLayer
            page={this.props.page}
            localStatePath={`${getLocalStorePath()}.annotationTools`}
            roomStatePath={getStorePath()}
          />
        </CanvasContainer>
      </PresentationViewContainer>
    );
  }
}

export default connect(
  (state: IStoreState) => {
    const localRoomState = _get(state, getStorePath());
    const localState = _get(state, getLocalStorePath());
    const selectors = clientEntityProvider.getSelectors();

    return {
      page: _get(localRoomState.pages, 'current', 1),
      uploadedPresentationLink: localRoomState.uploadedPresentationLink,
      isLoading: localRoomState.isLoading,
      isRendering: localRoomState.isRendering,
      shared: localRoomState.shared,
      currentPage: localState.currentPage,
      currentDoc: localState.currentDoc,
      currentLink: localState.currentLink,
      isOwner: selectors.appSelectors.isOwner(state),
    };
  },
  (dispatch: any) => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      sendAction: bindActionCreators(appActionCreators.sendRoomAction, dispatch),
      showNotification: bindActionCreators(appActionCreators.showNotification, dispatch),
      setLocalPresentation: bindActionCreators(actionCreators.setLocalPresentation, dispatch),
      setPresentationRendering: bindActionCreators(actionCreators.setPresentationRendering, dispatch),
    };
  }
)(PresentationView);
