import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import _get from 'lodash/get';

import { getStorePath, getLocalStorePath } from '../../redux/stateConfig';
import UploadSharePanel from '../UploadSharePanel';
import PresentationView, { Loader } from '../PresentationView';
import PagesControlPanel from '../PagesControlPanel';
import AnnotationTools from '../AnnotationTools';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import actionCreators from '../../../common/redux/actionCreators';
import Stub from '../Stub';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const PresentationsContainer = styled.div`
  ${({ theme }: IRequiredTheme) => `
  background: #ebebeb;
  width: 115vh;
  height: 100%;
  color: ${theme.backgroundColor};
  position: relative;
`};
`;

const UploadSharePanelContainer = styled.div`
  position: absolute;
  left: 10px;
  top: 10px;
`;

const PagesControlPanelContainer = styled.div`
  position: absolute;
  left: 10px;
  bottom: 10px;
  z-index: 3;
`;

export interface IPresentationsStateProps {
  shared: boolean;
  uploadedPresentationLink: string;
  isOwner: boolean;
  annotationToolsState: any;
  isLoading: boolean;
  isRendering: boolean;
}

type PresentationsProps = IPresentationsStateProps;

class Presentations extends React.Component<PresentationsProps> {
  private _canViewPresentation = () => {
    return this.props.uploadedPresentationLink && (this.props.isOwner || this.props.shared);
  };
  private _isLoading = () => this.props.isLoading || this.props.isRendering;

  public render() {
    return (
      <PresentationsContainer>
        {this._canViewPresentation() && <PresentationView />}

        {!this._canViewPresentation() ? (
          this._isLoading() ? (
            <Loader isVisible={this._isLoading()} />
          ) : (
            <Stub isOwner={this.props.isOwner} />
          )
        ) : null}

        {this.props.isOwner && (
          <UploadSharePanelContainer>
            <UploadSharePanel />
          </UploadSharePanelContainer>
        )}
        {this._canViewPresentation() && (
          <React.Fragment>
            <PagesControlPanelContainer>
              <PagesControlPanel />
            </PagesControlPanelContainer>
            {this.props.isOwner && (
              <AnnotationTools localState={this.props.annotationToolsState} isModerator={this.props.isOwner} />
            )}
          </React.Fragment>
        )}
      </PresentationsContainer>
    );
  }
}

export default connect(
  (state: IStoreState): IPresentationsStateProps => {
    const localRoomState = _get(state, getStorePath());
    const localState = _get(state, getLocalStorePath());
    const selectors = clientEntityProvider.getSelectors();

    return {
      shared: localRoomState.shared,
      isOwner: selectors.appSelectors.isOwner(state),
      uploadedPresentationLink: localRoomState.uploadedPresentationLink,
      isLoading: localRoomState.isLoading,
      isRendering: localRoomState.isRendering,
      annotationToolsState: localState.annotationTools,
    };
  },
  {
    setPresentationLoading: actionCreators.setPresentationLoading,
  }
)(Presentations);
