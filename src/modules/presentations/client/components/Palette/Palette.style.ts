/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export interface ICircle {
  color: string;
}

const CELL_RADIUS = 14;
const CONTAINER_WIDTH = 120;

export const styledCircle = ({ color }: ICircle) => `
  width: ${CELL_RADIUS}px;
  height: ${CELL_RADIUS}px;
  border-radius: 50%;
  background-color: ${color};
  margin: 0 auto;
  border: solid 1px #f3f3f3;
`;

export const styledButtonContainer = () => `
  background-color: transparent;
  outline: none;
  border: none;
  padding: 5px 7px;
`;

export const styledColorPicker = () => `
  position: absolute;
  top: 34px;
  left: ${CONTAINER_WIDTH}px;
  background-color: #f8f8f8;
  max-width: ${CONTAINER_WIDTH}px;
  display: flex;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.15);
  padding: 3px;
  border-radius: 2px;
`;
