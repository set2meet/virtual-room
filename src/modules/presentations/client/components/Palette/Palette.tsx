import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { ICircle, styledCircle, styledButtonContainer, styledColorPicker } from './Palette.style';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import actionCreators from '../../../common/redux/actionCreators';
import { DEFAULT_ANNOTATION_COLOR } from '../../types/constants/common';

interface IPaletteProps {
  color: string;
}

interface IPaletteDispatchProps {
  updateAnnotationTools: ActionCreator<AnyAction>;
}

interface IPaletteState {
  isColoPickerVisible: boolean;
}

type IPalette = IPaletteDispatchProps & IPaletteProps;

const Circle = styled.div<ICircle>`
  ${styledCircle};
`;
const ButtonContainer = styled.button`
  ${styledButtonContainer};
`;
const ColorPicker = styled.div`
  ${styledColorPicker};
`;

const availableColors = [
  DEFAULT_ANNOTATION_COLOR,
  '#ffff00',
  '#008002',
  '#0a00ff',
  '#00ffff',
  '#ee82ee',
  '#808080',
  '#ffffff',
];

class Palette extends React.Component<IPalette> {
  public state: IPaletteState = {
    isColoPickerVisible: false,
  };

  private toggleColorPicker = () => {
    this.setState({
      isColoPickerVisible: !this.state.isColoPickerVisible,
    });
  };

  private setColor = (color: string) => {
    return () => this.props.updateAnnotationTools({ color });
  };

  public render() {
    return (
      <React.Fragment>
        <ButtonContainer onClick={this.toggleColorPicker}>
          <Circle color={this.props.color} />
        </ButtonContainer>
        {this.state.isColoPickerVisible && (
          <ColorPicker>
            {availableColors.map((color: string, index: number) => (
              <ButtonContainer key={index} onClick={this.setColor(color)}>
                <Circle color={color} />
              </ButtonContainer>
            ))}
          </ColorPicker>
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  () => ({}),
  (dispatch) => ({
    updateAnnotationTools: bindActionCreators(actionCreators.updateAnnotationTools, dispatch),
  })
)(Palette);
