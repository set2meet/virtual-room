import * as React from 'react';
import { createRef } from 'react';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import styled from 'styled-components';
import resolveUrl from 'resolve-url';

import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

import { getStorePath } from '../../redux/stateConfig';
import SUPPORTED_TYPES from '../../types/constants/SUPPORTED_TYPES';
import { IRequiredTheme } from '../../../../../ioc/client/types/interfaces';

const SupportedExts = SUPPORTED_TYPES.join(',');

const UploadSharePanelDiv = styled.div`
  ${({ theme }: IRequiredTheme) => `
  height: 34px;
  box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
  background-color: #f8f8f8;
  display: flex;
  align-items: center;

  & > input {
    display: none;
  }

  & > div:nth-child(2) {
    & > button,
    & > button:not(:disabled):hover {
      background-color: #f8f8f8;
      padding: 0 15px;
      border-right: 1px solid rgba(0, 0, 0, 0.05);

      &:before {
        color: ${theme.backgroundColor};
      }
    }
  }

  & > div:last-child {
    & > button {
      height: 26px;
    }
  }
`};
`;

const ShareButtonContainer = styled.div`
  width: 100%;
  display: flex;
  padding: 0 12px;
  justify-content: center;
`;

export interface IUploadSharePanelStateProps {
  uploadedPresentationLink: string;
  shared: boolean;
  isLoading: boolean;
  isRendering: boolean;
}

export interface IUploadSharePanelDispatchProps {
  sendAction: ActionCreator<AnyAction>;
  showNotification: ActionCreator<AnyAction>;
  setPresentationLoading: ActionCreator<AnyAction>;
}

// const getExtension = (fname: string): string => {
//   // TODO:: export it to some components container or replace with lodash
//   return fname.substring(fname.lastIndexOf('.'), fname.length);
// };

const cutExtension = (fname: string): string => {
  // TODO:: same as get ext
  return fname.replace(fname.substring(fname.lastIndexOf('.'), fname.length), '');
};

type UploadSharePanelProps = IUploadSharePanelStateProps & IUploadSharePanelDispatchProps;

class UploadSharePanel extends React.Component<UploadSharePanelProps> {
  private fileRef = createRef<HTMLInputElement>();

  public render() {
    const { SimpleToggleButton, Button } = clientEntityProvider.getComponentProvider().UI;
    const { shared } = this.props;

    return (
      <UploadSharePanelDiv>
        <input type="file" ref={this.fileRef} accept={SupportedExts} onChange={this._onUpload} />
        <SimpleToggleButton
          icon="upload"
          fontSize="14px"
          buttonSize="22px"
          onClick={this._uploadFile}
          disabled={this.props.isLoading || this.props.isRendering}
        />
        <ShareButtonContainer>
          <Button
            bsStyle={shared ? 'warning' : 'default'}
            disabled={this.props.isLoading || this.props.isRendering}
            title={`${shared ? 'STOP SHARING' : 'SHARE'}`}
            onClick={this._togglePresentationSharing}
          />
        </ShareButtonContainer>
      </UploadSharePanelDiv>
    );
  }

  private _isAlreadyUploaded = (file: File, uploadedPresentationLink: string) => {
    const loadingFileName = cutExtension(file.name);
    const alreadyLoadedFileName = cutExtension(uploadedPresentationLink);

    return loadingFileName === alreadyLoadedFileName;
  };

  private _isUploadValid = (file: File) => {
    return SUPPORTED_TYPES.includes(file.type);
  };

  private _uploadFile = () => {
    this.fileRef.current.click();
  };

  /**
   * File input stores information about file and dont let upload file with same name, we handle it in other place
   * @private
   */
  private _resetFileInput = () => {
    this.fileRef.current.value = '';
  };

  private _onUpload = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files[0];

    if (!file) {
      // prevent error while file load canceled but js trying to continue script
      return;
    }

    if (this._isAlreadyUploaded(file, this.props.uploadedPresentationLink)) {
      this._resetFileInput();
      return;
    }

    if (!this._isUploadValid(file)) {
      this.props.showNotification({
        hideProgressBar: true,
        type: 'error',
        content: 'Invalid file type',
      });
      return;
    }

    const { service } = clientEntityProvider.getConfig();
    const postUrl = resolveUrl(service.url, service.path, 'presentations/upload');
    const formData = new FormData();
    const httpClient = clientEntityProvider.getHttpClient();
    const actionCreators = clientEntityProvider.getAppActionCreators();

    formData.append('file', file);

    this.props.setPresentationLoading(true);
    this.props.sendAction(actionCreators.clearAnnotations());

    httpClient
      .post(postUrl, formData, { headers: { 'Content-Type': 'multipart/form-data' } })
      .then((response) => {
        const uploadURL = response.data;

        this.props.sendAction(actionCreators.setPresentationLink(uploadURL));
        this._resetFileInput();
      })
      .catch((error) => {
        this.props.showNotification({
          hideProgressBar: true,
          type: 'error',
          content: `Failed to connect to server`,
        });
        this.props.sendAction(actionCreators.resetPresentationState());
        this.props.setPresentationLoading(false);
      });
  };

  private _togglePresentationSharing = () => {
    const actionCreators = clientEntityProvider.getAppActionCreators();

    if (this.props.shared) {
      this.props.sendAction(actionCreators.unsharePresentation());
    } else {
      this.props.sendAction(actionCreators.sharePresentation());
    }
  };
}

export default connect(
  (state: IStoreState) => {
    const localState = _get(state, getStorePath());
    const roomState = _get(state, getStorePath());

    return {
      uploadedPresentationLink: localState.uploadedPresentationLink,
      shared: localState.shared,
      isLoading: roomState.isLoading,
      isRendering: roomState.isRendering,
    };
  },
  (dispatch: any) => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      setPresentationLoading: bindActionCreators(appActionCreators.setPresentationLoading, dispatch),
      sendAction: bindActionCreators(appActionCreators.sendRoomAction, dispatch),
      showNotification: bindActionCreators(appActionCreators.showNotification, dispatch),
    };
  }
)(UploadSharePanel);
