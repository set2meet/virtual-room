/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction, Dispatch, Store } from 'redux';
import actionCreators from '../../common/redux/actionCreators';
import Socket = SocketIOClient.Socket;
import _get from 'lodash/get';
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IAppActionCreators } from '../../../../ioc/common/ioc-interfaces';
import { PresentationsAction } from '../../common/redux/types/PresentationsAction';
import { PresentationTool } from '../types/constants/PresentationTool';

let presentationsMiddlewareEnumInstance: IAppReduxEnumMiddleware;

const resolvePresentationsMiddlewareEnum = (): IAppReduxEnumMiddleware => {
  const appActionCreators: IAppActionCreators = clientEntityProvider.getAppActionCreators();

  if (!presentationsMiddlewareEnumInstance) {
    presentationsMiddlewareEnumInstance = {
      [PresentationsAction.UPDATE_ANNOTATION_TOOLS](store: any, next, action) {
        const state = store.getState();
        const page = _get(state, 'room.presentations.pages.current');

        if (page && action.object.selectedTool === PresentationTool.ERASER) {
          store.dispatch(appActionCreators.sendRoomAction(actionCreators.clearAnnotationPage(page)));
        } else {
          next(action);
        }
      },
      [PresentationsAction.RESET_PRESENTATION_STATE](store: any, next, action) {
        store.dispatch(appActionCreators.sendRoomAction(actionCreators.clearAnnotations()));
        next(action);
      },

      /**
       * Catch when presentation uploaded and router returns link to download, then off loader
       */
      [PresentationsAction.SET_PRESENTATION_LINK](store: any, next, action) {
        store.dispatch(appActionCreators.sendRoomAction(actionCreators.setPresentationRendering(true)));
        store.dispatch(appActionCreators.sendRoomAction(actionCreators.setPresentationLoading(false)));

        next(action);
      },

      [PresentationsAction.SET_NUMBER_OF_PAGES](store: any, next, action) {
        setTimeout(
          () => store.dispatch(appActionCreators.sendRoomAction(actionCreators.setPresentationRendering(false))),
          1000
        );
        next(action);
      },
    };
  }

  return presentationsMiddlewareEnumInstance;
};

const presentationsMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: Store) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  const presentationsMiddlewareEnum = resolvePresentationsMiddlewareEnum();
  if (presentationsMiddlewareEnum[action.type]) {
    return presentationsMiddlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default presentationsMiddleware;
