/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import _omit from 'lodash/omit';
import { DEFAULT_ANNOTATION_COLOR } from '../types/constants/common';
import { PresentationsAction } from '../../common/redux/types/PresentationsAction';
import { PresentationTool } from '../types/constants/PresentationTool';
import { ILocalPresentationsReduxState } from './types/localReduxStateTypes';

const INITIAL_STATE: ILocalPresentationsReduxState = {
  currentDoc: null,
  currentPage: null,
  currentLink: '',
  annotationTools: {
    selectedTool: PresentationTool.LINE,
    color: DEFAULT_ANNOTATION_COLOR,
    isCollapsed: true,
  },
};

const reducers = {
  [PresentationsAction.SET_LOCAL_PRESENTATION](
    state: ILocalPresentationsReduxState,
    { localPresentation }: AnyAction
  ): ILocalPresentationsReduxState {
    return {
      ...state,
      ...localPresentation,
    };
  },
  [PresentationsAction.UPDATE_ANNOTATION_TOOLS](state: ILocalPresentationsReduxState, action: AnyAction) {
    return {
      ...state,
      annotationTools: {
        ...state.annotationTools,
        ...action.object,
      },
    };
  },

  /**
   * Reset all presentation params to avoid error while load new presentation with old params in state;
   * @param state
   * @param action
   */
  [PresentationsAction.RESET_PRESENTATION_STATE](state: ILocalPresentationsReduxState, action: AnyAction) {
    return {
      ...INITIAL_STATE,
    };
  },

  [PresentationsAction.CLEAR_ANNOTATION_PAGE](state: ILocalPresentationsReduxState) {
    return {
      ...state,
      annotationTools: _omit(state.annotationTools, 'mouseTracker'),
    };
  },
  [PresentationsAction.CLEAR_ANNOTATIONS](state: ILocalPresentationsReduxState) {
    return {
      ...state,
      annotationTools: _omit(state.annotationTools, 'mouseTracker'),
    };
  },
};

export default (state = INITIAL_STATE, action: AnyAction): ILocalPresentationsReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
