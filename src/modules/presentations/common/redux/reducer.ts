/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import _uniqBy from 'lodash/uniqBy';
import { IPresentationsReduxState } from './types/IPresentationsReduxState';
import { PresentationsAction } from './types/PresentationsAction';

const INITIAL_STATE: IPresentationsReduxState = {
  isRendering: false,
  isLoading: false,
  shared: false,
  uploadedPresentationLink: '',
  pages: {
    current: 0,
    max: 0,
  },
  annotationPages: {},
  viewport: null,
};

const reducers = {
  [PresentationsAction.SHARE_PRESENTATION](state: IPresentationsReduxState): IPresentationsReduxState {
    return {
      ...state,
      shared: true,
    };
  },
  [PresentationsAction.UNSHARE_PRESENTATION](state: IPresentationsReduxState): IPresentationsReduxState {
    return {
      ...state,
      shared: false,
    };
  },
  [PresentationsAction.SET_PRESENTATION_LINK](
    state: IPresentationsReduxState,
    { link }: AnyAction
  ): IPresentationsReduxState {
    return {
      ...state,
      uploadedPresentationLink: link,
    };
  },
  [PresentationsAction.SET_NUMBER_OF_PAGES](
    state: IPresentationsReduxState,
    { numOfPages, initialPage }: AnyAction
  ): IPresentationsReduxState {
    return {
      ...state,
      pages: {
        current: initialPage || 1,
        max: numOfPages,
      },
    };
  },
  [PresentationsAction.SET_PAGE](state: IPresentationsReduxState, { page }: AnyAction): IPresentationsReduxState {
    return {
      ...state,
      pages: {
        ...state.pages,
        current: page,
      },
    };
  },
  [PresentationsAction.RESET_PRESENTATION_STATE](
    state: IPresentationsReduxState,
    { page }: AnyAction
  ): IPresentationsReduxState {
    return {
      ...INITIAL_STATE,
    };
  },

  [PresentationsAction.SET_PRESENTATION_RENDERING](
    state: IPresentationsReduxState,
    { isRendering }: AnyAction
  ): IPresentationsReduxState {
    return {
      ...state,
      isRendering,
    };
  },

  [PresentationsAction.SET_PRESENTATION_LOADING](
    state: IPresentationsReduxState,
    { isLoading }: AnyAction
  ): IPresentationsReduxState {
    return {
      ...state,
      isLoading,
    };
  },
  [PresentationsAction.UPDATE_ANNOTATION_PAGE](state: IPresentationsReduxState, action: AnyAction) {
    return {
      ...state,
      annotationPages: {
        ...state.annotationPages,
        [action.page]: _uniqBy(action.shapes, 'id'),
      },
    };
  },
  [PresentationsAction.CLEAR_ANNOTATION_PAGE](state: IPresentationsReduxState, action: AnyAction) {
    return {
      ...state,
      annotationPages: {
        ...state.annotationPages,
        [action.page]: [] as any,
      },
    };
  },
  [PresentationsAction.CLEAR_ANNOTATIONS](state: IPresentationsReduxState) {
    return {
      ...state,
      annotationPages: {},
    };
  },
  [PresentationsAction.SET_VIEWPORT](state: IPresentationsReduxState, action: AnyAction) {
    return {
      ...state,
      viewport: action.viewport,
    };
  },
};

export default (state: IPresentationsReduxState | null, action: AnyAction): IPresentationsReduxState => {
  state = state || INITIAL_STATE;
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
