/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ActionCreator, AnyAction } from 'redux';

export interface IPresentationsActionCreators {
  sharePresentation: ActionCreator<AnyAction>;
  unsharePresentation: ActionCreator<AnyAction>;
  setPresentationLink: ActionCreator<AnyAction>;
  setNumberOfPages: ActionCreator<AnyAction>;
  setPage: ActionCreator<AnyAction>;
  resetPresentationState: ActionCreator<AnyAction>;
  setPresentationLoading: ActionCreator<AnyAction>;
  setPresentationRendering: ActionCreator<AnyAction>;
  setLocalPresentation: ActionCreator<AnyAction>;
  updateAnnotationTools: ActionCreator<AnyAction>;
  updateAnnotationPage: ActionCreator<AnyAction>;
  clearAnnotationPage: ActionCreator<AnyAction>;
  clearAnnotations: ActionCreator<AnyAction>;
  initAnnotations: ActionCreator<AnyAction>;
  setViewport: ActionCreator<AnyAction>;
}
