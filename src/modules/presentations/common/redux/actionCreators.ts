/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ICanvasViewport } from '../types/ICanvasViewport';
import { PresentationsAction } from './types/PresentationsAction';
import { IPresentationsActionCreators } from './types/IPresentationsActionCreators';

const actionCreators: IPresentationsActionCreators = {
  sharePresentation: () => ({
    type: PresentationsAction.SHARE_PRESENTATION,
  }),
  unsharePresentation: () => ({
    type: PresentationsAction.UNSHARE_PRESENTATION,
  }),
  setPresentationLink: (link: string) => ({
    link,
    type: PresentationsAction.SET_PRESENTATION_LINK,
  }),
  setNumberOfPages: (numOfPages: number, initialPage: number) => ({
    numOfPages,
    initialPage,
    type: PresentationsAction.SET_NUMBER_OF_PAGES,
  }),
  setPage: (page: number) => ({
    page,
    type: PresentationsAction.SET_PAGE,
  }),
  resetPresentationState: () => ({
    type: PresentationsAction.RESET_PRESENTATION_STATE,
  }),
  setPresentationLoading: (isLoading: boolean) => ({
    isLoading,
    type: PresentationsAction.SET_PRESENTATION_LOADING,
  }),
  setPresentationRendering: (isRendering: boolean) => ({
    isRendering,
    type: PresentationsAction.SET_PRESENTATION_RENDERING,
  }),
  setLocalPresentation: (localPresentation: object) => ({
    localPresentation,
    type: PresentationsAction.SET_LOCAL_PRESENTATION,
  }),
  updateAnnotationTools: (object: object) => ({
    type: PresentationsAction.UPDATE_ANNOTATION_TOOLS,
    object,
  }),
  updateAnnotationPage: (page: number, shapes: any[], stopPropagation: boolean = false) => ({
    type: PresentationsAction.UPDATE_ANNOTATION_PAGE,
    page,
    shapes,
    stopPropagation,
  }),
  clearAnnotationPage: (page: number) => ({
    type: PresentationsAction.CLEAR_ANNOTATION_PAGE,
    page,
  }),
  clearAnnotations: () => ({
    type: PresentationsAction.CLEAR_ANNOTATIONS,
  }),
  initAnnotations: () => ({
    type: PresentationsAction.INIT_ANNOTATIONS,
  }),
  setViewport: (viewport: ICanvasViewport) => ({
    type: PresentationsAction.SET_VIEWPORT,
    viewport,
  }),
};

export default actionCreators;
