/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { Action, AnyAction, Dispatch, Middleware } from 'redux';
import * as SocketIO from 'socket.io';
import * as SocketIOClient from 'socket.io-client';
import * as React from 'react';
import * as ioredis from 'ioredis';
import { ButtonProps } from 'react-bootstrap';
import Socket = SocketIOClient.Socket;
import { IModuleStorageKey, IStoreState, TMiddlewareAPI } from '../../ioc/common/ioc-interfaces';
import { IBreakpointProps } from '../app/components/Breakpoint/Breakpoint.types';
import { IServerLogger } from '../../ioc/server/types/interfaces';
import TServerConfig from '../../server/config/types/TServerConfig';

/** typical module plugin can have only following exports */
export interface IVRModule<ReduxActionCreatorsType> {
  actionCreators?: ReduxActionCreatorsType;
  actionTypes?: Record<string, string>;
  storageKeys?: Record<string, IModuleStorageKey<any>>;
  ui?: any;
  constants?: any;
  serverMiddleware?: IServerMiddleware | any; // TODO interface: function or init
}

/** server middleware interfaces */
export type IServerMiddleware = (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => void;

export interface IServerMiddlewareOptionalContext {
  clientSocket?: SocketIO.Socket;
}

type SocketAdapterEmitData = {
  target: {
    roomId?: string;
    users?: string[];
  };
  event: string;
  payload: any;
};

export type SocketIOAdapter = {
  emit: (data: SocketAdapterEmitData) => void;
  getRoomParticipants: (roomId: string) => Promise<Record<string, 1>>;
  getRoomClientSocket: (roomId: string, userId: string) => SocketIO.Socket;
};

export interface IServerMetaObjects {
  socketAdapter: SocketIOAdapter;
  logger: IServerLogger;
  redis: {
    pubClient: ioredis.Redis;
    subClient: ioredis.Redis;
  };
  dispatchAction: (
    action: AnyAction,
    next: IServerMiddlewareNext,
    extraContext?: IServerMiddlewareOptionalContext
  ) => void;
  config: TServerConfig;
}

export interface IServerMiddlewareContext extends IServerMiddlewareOptionalContext {
  action: AnyAction;
  appMetaObjects: IServerMetaObjects;
}

export type IServerMiddlewareNext = (err?: any) => void;

export interface IAppReduxEnumMiddleware<OptionsType = void> {
  [key: string]: IAppReduxMiddlewareExecutor<OptionsType>;
}

/**
 * executor extends redux middleware to use socket and path to store key
 */
type IAppReduxMiddlewareExecutor<OptionsType> = (
  store: TMiddlewareAPI,
  next: Dispatch,
  action: AnyAction,
  socket: Socket,
  storeKey: string,
  options?: OptionsType
) => any;

/**
 * binds storeKey to IAppReduxMiddleware, to use in IAppReduxMiddlewareExecutor
 */
export type IModuleReduxMiddleware<OptionsType = void> = (storeKey: string) => IAppReduxMiddleware<OptionsType>;

/**
 * binds socket to Middleware, to use in IAppReduxMiddlewareExecutor
 */
export type IAppReduxMiddleware<OptionsType = void> = (
  socket: Socket,
  options?: OptionsType
) => Middleware<{}, IStoreState>;

/** -------- local modules definitions & declarations -------- */

export enum CommonIconsList {
  pencil = 'pencil',
  trash = 'trash',
  userTemp = 'userTemp',
  arrowUp = 'arrowUp',
  arrowRight = 'arrowRight',
  arrowLeft = 'arrowLeft',
  arrowDown = 'arrowDown',
  pause = 'pause',
  play = 'play',
  close = 'close',
  close2 = 'close2',
  headset = 'headset',
  hand = 'hand',
  handMuted = 'handMuted',
  videoCamera = 'videoCamera',
  videoCameraMuted = 'videoCameraMuted',
  microphone = 'microphone',
  microphoneMuted = 'microphoneMuted',
  speaker = 'speaker',
  messages = 'messages',
  messagesStroked = 'messagesStroked',
  user = 'user',
  users = 'users',
  usersStroked = 'usersStroked',
  desktop = 'desktop',
  smile = 'smile',
  attach = 'attach',
  send = 'send',
  link = 'link',
  exit = 'exit',
  linkMirror = 'linkMirror',
  closeRound = 'closeRound',
  menuDotted = 'menuDotted',
  phoneTube = 'phoneTube',
  fullscreenOn = 'fullscreenOn',
  fullscreenOff = 'fullscreenOff',
  brush = 'brush',
  codeEditor = 'codeEditor',
  asset = 'asset',
  notepad = 'notepad',
  addParticipant = 'addParticipant',
  unlock = 'unlock',
  conference = 'conference',
  checkboxOn = 'checkboxOn',
  checkboxOff = 'checkboxOff',
  checkMark = 'checkMark',
  speedometer = 'speedometer',
  presentation = 'presentation',
  upload = 'upload',
  graph2d = 'graph2d',
  endCall = 'endCall',
  leave = 'leave',
  construct = 'construct',
  copy = 'copy',
  download = 'download',
  eye = 'eye',
  delete = 'delete',
}

/*** UI ***/
declare namespace vrCommon {
  namespace client {
    namespace UI {
      export interface ISimpleToggleButton {
        icon: CommonIconsList[keyof CommonIconsList];
        active?: boolean;
        disabled?: boolean;
        onClick?: React.MouseEventHandler<HTMLButtonElement>;
        tooltip?: {
          'data-tooltip': string;
          'data-tooltip-pos'?: string;
        };
        fontSize?: string;
        buttonSize?: string;
      }

      export interface IUserAvatarProps {
        src?: string;
        size?: string;
        userColor?: string;
        displayName: string;
      }

      export interface ISidePanelProps {
        open: boolean;
        width: number;
        position: 'left' | 'right';
        onToggle: () => void;
        indicatorValue?: number;
      }

      export interface IButtonProps extends ButtonProps {
        icon?: string;
        title?: string;
        style?: React.CSSProperties;
      }

      export interface IRoundButtonProps extends ISimpleToggleButton {
        indicator?: number;
      }

      export interface IWaitingPageProps {
        text: string;
      }

      export interface IBrowserNotSupportedOwnProps {
        projectName: string;
        techSupportEMail?: string;
      }

      export interface ICommonUI {
        GlobalStyles: any; // TODO React.ComponentClass<IRequiredTheme> not works since styled-component 4.1 update
        SimpleToggleButton: React.ComponentClass<ISimpleToggleButton>;
        Button: React.ComponentClass<IButtonProps>;
        Spinner: React.ComponentClass<any>;
        SidePanel: React.ComponentClass<ISidePanelProps>;
        UserAvatar: React.SFC<IUserAvatarProps>;
        RoundButton: React.ComponentClass<any>;
        WaitingPage: React.ComponentClass<any>;
        BrowserNotSupported: React.ComponentClass<IBrowserNotSupportedOwnProps>;
        Checkbox: React.FunctionComponent<any>;
        Breakpoint: React.FunctionComponent<IBreakpointProps>;
      }

      export interface ICommonIcons {
        fontFamily: string;
        code: Record<CommonIconsList, string>;
      }
    }
  }
}

/*** Notification module ***/
declare namespace vrCommon {
  export namespace client {
    export type ActionMetadata = {
      sender: string;
      roomId?: string;
      target?: {
        roomId?: string;
        users?: string[];
      };
      session?: {
        sessionId: string;
        serviceName: string;
      };
      isHost?: boolean;
      user?: {
        id: string;
        name: string;
      };
      time: string;
      hostname: string;
      browserId: string;
      clientSocketId?: string;
    };

    export interface IAnyAction extends Action<string> {
      type: string;
      meta: ActionMetadata;
      [key: string]: any;
    }
  }
}

export { vrCommon };
