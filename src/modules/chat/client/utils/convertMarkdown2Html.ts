/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as marked from 'marked';

const renderer = new marked.Renderer();

renderer.link = (href: string, title: string, text: string): string => {
  const link = marked.Renderer.prototype.link.call(renderer, href, title, text);

  // extra replace because marked not working with escaped _ in links correctly.
  // (http://my\_domain must be translated to http://my_domain, but marked keeps \)

  return link.replace('<a', '<a target="_blank"').replace(/\\_|%5C_/g, '_');
};

marked.setOptions({ renderer });

const convertMarkdown2Html = (input: string): string => marked.parse(input);

export default convertMarkdown2Html;
