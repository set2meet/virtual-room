/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Socket = SocketIOClient.Socket;
import { IS2MModule, TS2MModuleFactory } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleError } from '../../../ioc/client/providers/ModuleProvider/types/ModuleError';
import chatReducer from './redux/chatReducer';
import reduxMiddleware from './redux/clientMiddleware';
import { TChatModuleInitOptions } from './types/TChatModuleInitOptions';

const createChatModule: TS2MModuleFactory = async (
  socket: Socket,
  stateKey: string = 'chat',
  moduleInitOptions: TChatModuleInitOptions
): Promise<IS2MModule> => {
  if (!moduleInitOptions || !moduleInitOptions.actionCreators) {
    throw new Error(ModuleError.REQUIRE_INITIAL_OPTIONS);
  }

  return {
    rootComponent: null,
    reduxModules: [
      {
        id: stateKey,
        reducersToMerge: {
          room: {
            [stateKey]: chatReducer,
          },
        },
        initialActions: [moduleInitOptions.actionCreators.initChatModule()],
        middlewares: [reduxMiddleware(stateKey)(socket)],
      },
    ],
  };
};

export default createChatModule;
