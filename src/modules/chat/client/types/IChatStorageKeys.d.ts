/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { IModuleStorageKey } from '../../../../ioc/client/providers/StorageManager/types';

export interface IChatStorageKeys extends Record<string, IModuleStorageKey<any>> {
  CHAT_PLAY_SOUND_KEY: IModuleStorageKey<boolean>;
}
