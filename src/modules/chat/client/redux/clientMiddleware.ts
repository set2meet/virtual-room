/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppReduxEnumMiddleware, IModuleReduxMiddleware, TMiddlewareAPI } from '../../../../ioc/common/ioc-interfaces';
import { AnyAction, Dispatch } from 'redux';
import Socket = SocketIOClient.Socket;
import { default as actionCreators } from '../../common/redux/actionCreators';
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ChatAction } from '../../common/redux/types/ChatAction';

const syncConfigWithLocalStorage = (store: TMiddlewareAPI) => {
  const CHAT_PLAY_SOUND_KEY = clientEntityProvider.getStorageKeys().CHAT_PLAY_SOUND_KEY;
  const playSoundOnNewMessage = clientEntityProvider.getStorageManager().get<boolean>(CHAT_PLAY_SOUND_KEY, true);

  store.dispatch(actionCreators.configToggleSoundMessage(playSoundOnNewMessage));
};

const middlewareEnum: IAppReduxEnumMiddleware = {
  [ChatAction.CHAT_MODULE_INIT](store, next, action) {
    syncConfigWithLocalStorage(store);
    next(action);
  },
  [ChatAction.CONFIG_MESSAGE_SOUND_TOGGLE](store, next, action) {
    const CHAT_PLAY_SOUND_KEY = clientEntityProvider.getStorageKeys().CHAT_PLAY_SOUND_KEY;

    clientEntityProvider.getStorageManager().set<boolean>(CHAT_PLAY_SOUND_KEY, action.value);
    next(action);
  },
};

const clientMiddleware: IModuleReduxMiddleware = (storeKey: string) => (socket: Socket) => (store: TMiddlewareAPI) => (
  next: Dispatch<AnyAction>
) => (action: AnyAction) => {
  if (middlewareEnum[action.type]) {
    return middlewareEnum[action.type](store, next, action, socket, storeKey);
  }

  return next(action);
};

export default clientMiddleware;
