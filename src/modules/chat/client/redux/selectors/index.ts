/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import numberOfUnreadMessages from './numberOfUnreadMessages';
import unreadMessages from './unreadMessages';
import { IChatSelectors } from './IChatSelectors';

const chatSelectors: IChatSelectors = {
  numberOfUnreadMessages,
  unreadMessages,
};

export default chatSelectors;
