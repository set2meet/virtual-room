/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { findIndex as _findIndex } from 'lodash';
import { IChatReduxState } from '../../../common/redux/types/IChatReduxState';

// is user guest
// @param {IApp} state - app state
export default (state: IChatReduxState): number => {
  if (!state) {
    return 0;
  }

  const indexOfUnread = _findIndex(state.history, { id: state.unreadId });

  if (indexOfUnread < 0) {
    return 0;
  }

  return state.history.length - indexOfUnread;
};
