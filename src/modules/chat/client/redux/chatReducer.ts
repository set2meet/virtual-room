/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { ChatAction } from '../../common/redux/types/ChatAction';
import {
  INITIAL_STATE as COMMON_INITIAL_STATE,
  reducers as commonReducers,
} from '../../common/redux/chatReducer';
import { IChatReduxState } from '../../../../ioc/common/ioc-interfaces';

const INITIAL_STATE: IChatReduxState = {
  ...COMMON_INITIAL_STATE,
};

const reducers = {
  ...commonReducers,
  [ChatAction.CONFIG_MESSAGE_SOUND_TOGGLE](state: IChatReduxState, action: AnyAction) {
    return {
      ...state,
      config: {
        ...state.config,
        playSoundOnNewMessage: action.value,
      },
    };
  },
};

export default (state: IChatReduxState | null, action: AnyAction): IChatReduxState => {
  state = state || INITIAL_STATE;
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }
  return state;
};
