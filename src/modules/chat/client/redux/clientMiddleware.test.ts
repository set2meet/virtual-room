/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import clientEntityProvider from '../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Socket = SocketIOClient.Socket;
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientMiddleware from './clientMiddleware';
import { mock } from 'jest-mock-extended';
import { Store } from 'redux';
import { IModuleReduxMiddleware } from '../../../../ioc/common/ioc-interfaces';
import { storageKeys } from '../storageKeys';

beforeAll(setupIoCClientForTest());

const create = (testMiddleware: IModuleReduxMiddleware) => {
  const socket = mock<Socket>();
  const store = mock<Store>();
  const next = jest.fn();

  const middleware = testMiddleware('')(socket)(store)(next);

  const invoke = (action) => middleware(action);

  return { store, next, invoke };
};

describe('chat client middleware', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    clientEntityProvider.getStorageManager().removeAll();
  });

  it('should save playSoundOnNewMessage to storage', () => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();
    const storageManager = clientEntityProvider.getStorageManager();
    const { invoke, next } = create(clientMiddleware);

    invoke(appActionCreators.configToggleSoundMessage(true));
    expect(storageManager.get<boolean>(storageKeys.CHAT_PLAY_SOUND_KEY)).toEqual(true);
    expect(next).toBeCalledTimes(1);
  });

  it('should sync storage with state', () => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();
    const storageManager = clientEntityProvider.getStorageManager();
    const { store, invoke } = create(clientMiddleware);

    storageManager.set<boolean>(storageKeys.CHAT_PLAY_SOUND_KEY, true);
    invoke(appActionCreators.initChatModule());
    expect(store.dispatch).toHaveBeenNthCalledWith(1, appActionCreators.configToggleSoundMessage(true));
  });
});
