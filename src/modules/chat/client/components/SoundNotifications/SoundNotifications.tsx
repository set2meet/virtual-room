import React from 'react';
import { connect } from 'react-redux';
import { isEmpty as _isEmpty, some as _some, debounce as _debounce, isEqual as _isEqual, get as _get } from 'lodash';
import { IChatSoundNotificationsProps } from './IChatSoundNotificationsProps';
import chatSelectors from '../../redux/selectors';
import incomingMessageSound from '../../assets/incoming-message.mp3';
import { IStoreState } from '../../../../../ioc/common/ioc-interfaces';
import { IChatMessageItem } from '../../../common/types/IChatMessageItem';
import { IChatPanelState } from '../Panel/types/IChatPanelState';

const DEBOUNCE_LIMIT = 300; // milliseconds

interface IChatSoundNotificationsStateProps {
  unreadMessages: IChatMessageItem[];
  playSoundOnNewMessage: boolean;
}

type SoundNotificationsProps = IChatSoundNotificationsProps & IChatSoundNotificationsStateProps;

export class SoundNotifications extends React.Component<SoundNotificationsProps> {
  private readonly debouncedPlaySound: (soundFile: string) => Promise<void>;

  public state: IChatPanelState = {
    messageItem: null,
    rawText: '',
  };

  constructor(props: SoundNotificationsProps) {
    super(props);
    this.debouncedPlaySound = _debounce(this.playSound, DEBOUNCE_LIMIT, { leading: true, trailing: false });
  }

  public async componentDidUpdate(prevProps: SoundNotificationsProps) {
    if (_isEqual(prevProps.unreadMessages, this.props.unreadMessages)) {
      return;
    }
    await this.notifyOnUnread();
  }

  private async notifyOnUnread() {
    if (!this.props.playSoundOnNewMessage || _isEmpty(this.props.unreadMessages)) {
      return;
    }

    // do not play sound for message author
    const hasUnreadMessages = _some(
      this.props.unreadMessages,
      (msg: IChatMessageItem): boolean => msg.user.id !== this.props.userId
    );

    if (hasUnreadMessages) {
      await this.debouncedPlaySound(incomingMessageSound);
    }
  }

  private playSound = (soundFile: string): Promise<void> => {
    const sound = new Audio(soundFile);

    return sound.play();
  };

  public render() {
    return <React.Fragment />;
  }
}

export default connect<IChatSoundNotificationsStateProps>(
  (state: IStoreState, ownProps: IChatSoundNotificationsProps): IChatSoundNotificationsStateProps => ({
    unreadMessages: chatSelectors.unreadMessages(state.room.chat),
    playSoundOnNewMessage: _get(state, `${ownProps.statePath}.config.playSoundOnNewMessage`),
  })
)(SoundNotifications);
