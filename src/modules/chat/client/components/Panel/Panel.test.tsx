import { mountStory, setupStorybookForTest } from '../../../../../test/utils/storybook';
import { enzymeCleanup } from '../../../../../test/utils/utils';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../../../../ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { IStoreExt } from '../../../../../test/utils/reduxMockEnhancer';
import { TStore } from '../../../../../ioc/client/types/interfaces';

beforeAll(setupStorybookForTest());
afterEach(enzymeCleanup);

describe('<Panel />', () => {
  it('add message', async (done) => {
    const wrapper = await mountStory('chat/Panel', 'WithMessage', true);
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();
    const TEST_MESSAGE = 'test message';

    (store as TStore & IStoreExt).clearActions();
    wrapper.find('Panel').setState({ rawText: TEST_MESSAGE }).update();
    expect(wrapper.find('.send-message').length).not.toBe(0);
    wrapper.find('.send-message').last().simulate('click');
    const receivedAction = (store as TStore & IStoreExt).getActions()[0].action;

    expect(receivedAction.msg.text).toEqual(TEST_MESSAGE);
    expect(receivedAction.type).toEqual(clientEntityProvider.getAppActionTypes().ADD_MESSAGE);

    done();
  });
  it('edit message in text field', async (done) => {
    const wrapper = await mountStory('chat/Panel', 'WithMessage', true);
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

    (store as TStore & IStoreExt).clearActions();
    wrapper.find('MenuItem').first().find('a').simulate('click');
    expect(wrapper.find('.send-message').length).not.toBe(0);
    expect(wrapper.find('.cancel-edit-message').length).not.toBe(0);
    wrapper.find('.send-message').last().simulate('click');
    const receivedAction = (store as TStore & IStoreExt).getActions()[0].action;

    expect(receivedAction.type).toEqual(clientEntityProvider.getAppActionTypes().EDIT_MESSAGE);
    done();
  });
  it('remove message', async (done) => {
    const wrapper = await mountStory('chat/Panel', 'WithMessage', true);
    const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

    (store as TStore & IStoreExt).clearActions();
    wrapper.find('MenuItem').last().find('a').simulate('click');
    const receivedAction = (store as TStore & IStoreExt).getActions()[0].action;

    expect(receivedAction.type).toEqual(clientEntityProvider.getAppActionTypes().REMOVE_MESSAGE);
    done();
  });
});
