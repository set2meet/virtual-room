import * as React from 'react';
import { connect } from 'react-redux';
import { v4 as guid } from 'uuid';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import TurndownService from 'turndown';
import { cloneDeep as _cloneDeep, get as _get } from 'lodash';
import styled, { withTheme } from 'styled-components';
import ScrollbarArea from 'react-custom-scrollbars';

import actions from '../../../common/redux/actionCreators';
import MessageInput from '../MessageInput';
import MessageItem from '../MessageItem';
import convertMarkdown2Html from '../../utils/convertMarkdown2Html';
import { IStoreState, TRoomUser } from '../../../../../ioc/common/ioc-interfaces';

import {
  styledBottomButtonPanel,
  styledBottomPanel,
  styledChatMainPanel,
  styledConversationPanel,
} from './Panel.style';

import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IChatMessageItem } from '../../../common/types/IChatMessageItem';
import { IChatPanelState } from './types/IChatPanelState';
import { IChatPanelProps } from './types/IChatPanelProps';
import { ChatMessageItemStatus } from '../../../common/types/ChatMessageItemStatus';

const ChatMainPanel = withTheme(
  styled.div`
    ${styledChatMainPanel};
  `
);
const ConversationPanel = withTheme(
  styled.div`
    ${styledConversationPanel};
  `
);
const BottomPanel = withTheme(
  styled.div`
    ${styledBottomPanel};
  `
);
const BottomButtonPanel = withTheme(
  styled.div`
    ${styledBottomButtonPanel};
  `
);

const turndownService = new TurndownService();

interface IChatPanelStateProps {
  history: IChatMessageItem[];
  unreadId: string;
  user: TRoomUser;
  playSoundOnNewMessage: boolean;
  userColors: any;
}

interface IChatPanelDispatchProps {
  sendAction: ActionCreator<AnyAction>;
  showNotification: ActionCreator<AnyAction>;
}

const convertMilliseconds2Seconds = (ms: number): number => {
  return Math.floor(ms / 1000);
};

const regexp = /<[/]?(p|br)>/g;

const isEmptyMessage = (input: string): boolean => {
  return !input.replace(regexp, '').trim();
};

type ChatProps = IChatPanelStateProps & IChatPanelDispatchProps & IChatPanelProps;

const UPDATE_TEXT_INTERVAL = 100;

export class Panel extends React.Component<ChatProps> {
  public state: IChatPanelState = {
    messageItem: null,
    rawText: '',
  };

  private scrollBar: ScrollbarArea;
  private rawContentText: string = '';

  private conversationScrollbarRef = (scroll: ScrollbarArea) => {
    if (scroll && scroll !== this.scrollBar) {
      this.scrollBar = scroll;
    }
  };

  private getUserColor = (user: TRoomUser) => {
    return this.props.userColors[user.id];
  };

  private newMessageItem = (user: TRoomUser): IChatMessageItem => {
    const newUser: TRoomUser = {
      ...user,
    };

    return {
      id: guid().toString(),
      user: newUser,
      time: null,
      text: '',
      status: ChatMessageItemStatus.DEFAULT,
      userColor: this.getUserColor(user),
    };
  };

  private scrollHistoryToUnread(): void {
    const scrollTop = _get(document.getElementById(this.props.unreadId), 'offsetTop', 0);

    if (scrollTop) {
      setTimeout(() => {
        this.scrollBar.scrollTop(scrollTop);
      }, 10);
    } else {
      this.scrollBar.scrollToBottom();
    }
  }

  public constructor(props: ChatProps) {
    super(props);

    this.state.messageItem = this.newMessageItem(props.user);
  }

  private startNewState = (): void => {
    this.setState({
      rawText: '',
      messageItem: this.newMessageItem(this.props.user),
    });

    this.rawContentText = '';
  };

  private timerId: number;

  private onMsgChange = (content: string): void => {
    clearTimeout(this.timerId);

    this.rawContentText = content;

    this.timerId = window.setTimeout(() => {
      this.setState({ rawText: content });
    }, UPDATE_TEXT_INTERVAL);
  };

  private onKeyDown = (e: KeyboardEvent): void => {
    if (!e.shiftKey && !e.ctrlKey && !e.metaKey && e.key === 'Enter') {
      this.sendMessage();
    }
  };

  public async componentDidMount() {
    this.scrollHistoryToUnread();
  }

  public async componentDidUpdate(prevProps: ChatProps) {
    if (prevProps.userColors !== this.props.userColors) {
      this.setState({
        messageItem: {
          ...this.state.messageItem,
          userColor: this.getUserColor(this.props.user),
        },
      });
    }
    this.scrollBar.scrollToBottom();
  }

  private sendMessage = () => {
    const markdownContent = turndownService.turndown(this.rawContentText || this.state.rawText).trim();

    if (markdownContent) {
      const messageItem = _cloneDeep(this.state.messageItem);

      messageItem.text = markdownContent;
      messageItem.time = messageItem.time || convertMilliseconds2Seconds(Date.now());

      if (messageItem.status === ChatMessageItemStatus.EDITED) {
        this.props.sendAction(actions.editMessage(messageItem));
      } else {
        this.props.sendAction(actions.addMessage(messageItem));
      }

      this.startNewState();
    }
  };

  private editMessage = (messageItem: IChatMessageItem) => {
    messageItem.status = ChatMessageItemStatus.EDITED;

    this.setState({
      rawText: convertMarkdown2Html(messageItem.text),
      messageItem,
    });
  };

  private removeMessage = (messageItem: IChatMessageItem) => {
    messageItem.status = ChatMessageItemStatus.REMOVED;
    messageItem.text = '';

    this.props.sendAction(actions.removeMessage(messageItem));
  };

  public render() {
    const htmlText = this.state.rawText;
    const { UI } = clientEntityProvider.getComponentProvider();

    return (
      <ChatMainPanel>
        <ConversationPanel>
          <ScrollbarArea autoHide={true} ref={this.conversationScrollbarRef}>
            {this.props.history.map((rec: IChatMessageItem) => (
              <MessageItem
                key={rec.id}
                item={rec}
                editable={rec.user.id === this.props.user.id}
                onEditMessage={this.editMessage}
                onRemoveMessage={this.removeMessage}
              />
            ))}
          </ScrollbarArea>
        </ConversationPanel>

        <BottomPanel>
          <MessageInput
            value={htmlText}
            onChange={this.onMsgChange}
            onKeyDown={this.onKeyDown}
            showNotification={this.props.showNotification}
          />

          <BottomButtonPanel>
            {this.state.messageItem.status === ChatMessageItemStatus.EDITED && (
              <UI.Button className="cancel-edit-message" bsStyle="link" icon="close2" onClick={this.startNewState} />
            )}
            {!isEmptyMessage(htmlText) && (
              <UI.Button className="send-message" bsStyle="link" icon="send" onClick={this.sendMessage} />
            )}
          </BottomButtonPanel>
        </BottomPanel>
      </ChatMainPanel>
    );
  }
}

export default connect<IChatPanelStateProps, IChatPanelDispatchProps, IChatPanelProps>(
  (state: IStoreState, ownProps: IChatPanelProps): IChatPanelStateProps => {
    const selectors = clientEntityProvider.getSelectors();

    return {
      user: selectors.appSelectors.getUser(state),
      history: _get(state, `${ownProps.statePath}.history`),
      unreadId: _get(state, `${ownProps.statePath}.unreadId`),
      playSoundOnNewMessage: _get(state, `${ownProps.statePath}.config.playSoundOnNewMessage`, true),
      userColors: _get(state, 'room.state.userColors'),
    };
  },
  (dispatch: any) => {
    const appActionCreators = clientEntityProvider.getAppActionCreators();

    return {
      sendAction: bindActionCreators(appActionCreators.sendRoomAction, dispatch),
      showNotification: bindActionCreators(appActionCreators.showNotification, dispatch),
    };
  }
)(Panel);
