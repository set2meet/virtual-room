import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import Skeleton from 'react-loading-skeleton';
import React, { ComponentType } from 'react';
import { withIoCFactory } from '../../../../../test/utils/utils';
import { setRoom, setUser } from '../../../../../test/utils/actions';
import { connectDecorator } from '../../../../../../.storybook/decorators/reduxDecorators';
import { select, text } from '@storybook/addon-knobs';
import { IChatMessageItem } from '../../../common/types/IChatMessageItem';
import { ChatMessageItemStatus } from '../../../common/types/ChatMessageItemStatus';
import withModule from '../../../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { IChatPanelProps } from './types/IChatPanelProps';
import withStoryRootIndicator from '../../../../../../.storybook/decorators/storyRootIndicator';

const panelFactory = () =>
  clientEntityProvider.resolveComponentSuspended<IChatPanelProps>(
    ComponentRegistryKey.Chat_PANEL,
    // tslint:disable-next-line:no-magic-numbers
    <Skeleton width={100} height={34} />
  );

const SuspendedChatPanel: ComponentType<IChatPanelProps> = withIoCFactory<IChatPanelProps>(panelFactory);

export default {
  title: 'chat/Panel',
  component: SuspendedChatPanel,
  decorators: [
    connectDecorator(),
    withStoryRootIndicator,
    withModule(ModuleRegistryKey.Chat),
    withModule(ModuleRegistryKey.RoomModule),
  ],
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const Default = (props) => {
  setUser(props, { id: 'user123', roomId: '123' });
  setRoom(props, '123', 'user123');

  return <SuspendedChatPanel statePath="room.chat" />;
};

export const WithMessage = (props) => {
  const user = setUser(props, { id: 'user123', roomId: '123' });
  setRoom(props, '123', 'user123');
  const message: Partial<IChatMessageItem> = {
    id: '1',
    user: user.user,
    text: text('message', 'message'),
    status: select('status', ChatMessageItemStatus, ChatMessageItemStatus.DEFAULT),
  };

  props.dispatch(clientEntityProvider.getAppActionCreators().addMessage(message));

  return <SuspendedChatPanel statePath="room.chat" />;
};
