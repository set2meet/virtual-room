/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ITheme } from '../../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export interface IWithThemePanel {
  theme: ITheme;
}

export const styledChatMainPanel = () => `
  display: grid;
  grid-template-columns: 1fr; 
  grid-template-rows: minmax(50%, 1fr) minmax(3em, auto); 
  padding: 15px;
  height: 100%; 
`;

export const styledConversationPanel = (props: IWithThemePanel) => {
  const commonIcons = clientEntityProvider.getIcons();

  return `
    overflow: hidden;
    position: relative;  
    margin-bottom: 20px;
    border-bottom: 2px solid ${props.theme.backgroundColorThird};
  
    & > div {
      position: absolute !important;
      overflow: hidden !important;
      width: 100% !important;
      height: 100% !important;
      
      & > :first-child {
        padding-right: 15px;
      }
    }
    
    &:empty {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-items: center;
      
      &:after,
      &:before {
        position: relative;
        top: calc(50% - 36px);      
      }
      
      &:before {
        font-family: ${commonIcons.fontFamily};
        content: ${commonIcons.code.messagesStroked};
        font-size: 72px;
      }
      &:after {
        content: "No conversation yet";
      }
    }
  `;
};

export const styledBottomButtonPanel = (props: IWithThemePanel) => `
  position: relative;

  .send-message, .cancel-edit-message {
    position: absolute;
    right: 0px;
    height: auto;
    width: auto;
    &:before {
      font-size: 18px;
    }
  }

  .send-message {        
    bottom: 0;
    &:before {
      color: white;
    }
  }

  .cancel-edit-message {
    top: 0;
  }
`;

export const styledBottomPanel = (props: IWithThemePanel) => `
  display: grid;
  grid-template-columns: 1fr 0px;

  .quill { 
    overflow-x: hidden;
    position: relative;
    background: ${props.theme.backgroundColorThird};
    min-height: 65px;
  
    & > .ql-container.ql-snow {
      border: 0;
      height: auto;
      
      & > .ql-editor {
        height: auto;
        padding-right: 25px;
        overflow: visible;
        caret-color: ${props.theme.primaryColor};

        & p
        {
          & > a {
            text-decoration: none;
            color: white;
            pointer-events: none;
         }
        }
      }
    }
  } 
`;
