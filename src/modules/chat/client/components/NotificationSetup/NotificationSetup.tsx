import * as React from 'react';
import styled from 'styled-components';
import { get as _get } from 'lodash';

import { styledTitle } from './NotificationSetup.style';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { IAppActionCreators } from '../../../../../ioc/common/ioc-interfaces';

const Title = styled.div`
  ${styledTitle};
`;

export interface INotificationSetupDispatchProps {
  toggleSoundOnNewMessage: (value: boolean) => void;
}

export interface INotificationSetupStateProps {
  playSoundOnNewMessage: boolean;
}

export type INotificationSetupProps = INotificationSetupStateProps & INotificationSetupDispatchProps;

/**
 * UI
 */
export class NotificationSetup extends React.Component<INotificationSetupProps> {
  public render() {
    const { UI } = clientEntityProvider.getComponentProvider();

    return (
      <React.Fragment>
        <Title>Chat message sound</Title>
        <UI.Checkbox
          value={this.props.playSoundOnNewMessage}
          onChange={this.props.toggleSoundOnNewMessage}
          label="Play sound when receiving a new chat message"
        />
      </React.Fragment>
    );
  }
}

export default connect<INotificationSetupStateProps, INotificationSetupDispatchProps>(
  (state: any) => {
    const playSoundOnNewMessage = _get(state, 'room.chat.config.playSoundOnNewMessage');

    return {
      playSoundOnNewMessage,
    };
  },
  (dispatch) => {
    const actionCreators: IAppActionCreators = clientEntityProvider.getAppActionCreators();

    return bindActionCreators(
      {
        toggleSoundOnNewMessage: actionCreators.configToggleSoundMessage as any,
      },
      dispatch
    );
  }
)(NotificationSetup);
