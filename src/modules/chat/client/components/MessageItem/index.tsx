import * as React from 'react';
import {
  messageItemContainer,
  userNameContainer,
  timeContainer,
  messageContainer,
  styledDropDownButtonTitle,
} from './MessageItem.style';
import styled, { withTheme } from 'styled-components';
import * as moment from 'moment';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import convertMarkdown2Html from '../../utils/convertMarkdown2Html';
import _cloneDeep from 'lodash/cloneDeep';
import { IChatMessageItem } from '../../../common/types/IChatMessageItem';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

export interface IMessageItem {
  editable: boolean;
  onEditMessage: (item: IChatMessageItem) => void;
  onRemoveMessage: (item: IChatMessageItem) => void;
  item: IChatMessageItem;
}

const MessageItemContainer = withTheme(styled.div`
  ${messageItemContainer};
`);
const UserNameContainer = withTheme(styled.div`
  ${userNameContainer};
`);
const TimeContainer = withTheme(styled.div`
  ${timeContainer};
`);

const MessageContainer = withTheme(styled.div`
  ${messageContainer};
`);

const StyledDropDownButtonTitle = styled.span`
  ${styledDropDownButtonTitle};
`;

interface IMessageItemState {
  isOpened: boolean;
}

class Index extends React.Component<IMessageItem> {
  public state: IMessageItemState = {
    isOpened: false,
  };

  private editMessage = () => {
    const { item, onEditMessage } = this.props;

    onEditMessage(_cloneDeep(item));
  };

  private removeMessage = () => {
    const { item, onRemoveMessage } = this.props;

    onRemoveMessage(_cloneDeep(item));
  };

  private toggleVisibility = () => {
    this.setState({ isOpened: !this.state.isOpened });
  };

  public render() {
    const { UI } = clientEntityProvider.getComponentProvider();

    const { item, editable } = this.props;
    const messageText = convertMarkdown2Html(item.text);
    const m = moment.unix(item.time);
    const formattedTime = m.format('h:mma');
    const user = item.user;

    return (
      <MessageItemContainer id={item.id}>
        <UI.UserAvatar {...user} userColor={item.userColor} />
        <UserNameContainer>{user.displayName}</UserNameContainer>
        <TimeContainer>{formattedTime}</TimeContainer>
        <MessageContainer data-type={item.status} dangerouslySetInnerHTML={{ __html: messageText }} />

        {editable && (
          <DropdownButton
            className="edit-message"
            id={`edit-message-menu-${item.id}`}
            title={<StyledDropDownButtonTitle />}
            noCaret
            pullRight
            open={this.state.isOpened}
            onToggle={this.toggleVisibility}
          >
            <MenuItem onClick={this.editMessage}>Edit</MenuItem>
            <MenuItem divider />
            <MenuItem onClick={this.removeMessage}>Delete</MenuItem>
          </DropdownButton>
        )}
      </MessageItemContainer>
    );
  }
}

export default Index;
