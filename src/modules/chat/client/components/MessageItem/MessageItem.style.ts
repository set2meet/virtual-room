/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ITheme } from '../../../../../ioc/common/ioc-interfaces';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { css } from 'styled-components';
import { ChatMessageItemStatus } from '../../../common/types/ChatMessageItemStatus';

interface IThemable {
  theme: ITheme;
}

export const messageItemContainer = (props: IThemable) => css` 
  font-size: 13px;
  display: grid;
  grid-gap: 5px;
  grid-template-columns: max-content 1fr max-content max-content;
  grid-template-areas:
    "avatar name time time"
    "avatar message message edit";
  & > :nth-child(1) {
    grid-area: avatar;
  }
  & > :nth-child(2) {
    grid-area: name;
  }
  & > :nth-child(3) {
    grid-area: time;
  }
  & > :nth-child(4) {
    grid-area: message; 
  }
  & > :nth-child(5) {
    grid-area: edit;
    visibility: hidden;
    align-self: start;
    
    &.open {
      visibility: visible !important;            
    }
    
    .dropdown-menu {
      min-width: 60px;
      border: 0;
      right: 5px;
      background-color: ${props.theme.backgroundColorSecondary};
      box-shadow: 0 5px 14px #000000;

      & .divider {
        height: 1px;
        margin: 0;
        overflow: hidden;
        background-color: rgba(140,140,140,0.3);
      }
        
      a {
        color: ${props.theme.fontColor};
        outline: none;
      }
      a:hover {
        color: ${props.theme.primaryColor}
        background-color: transparent;
      }
    }

    & > .edit-message {       
      margin: 0;
      padding: 0;
      background: transparent !important;
      color: ${props.theme.fontColor} !important;
      border: 0;
      height: 24px;
      
      &:hover {
        color: ${props.theme.primaryColor} !important;
      }
    }
  }

  &:hover {
    .edit-message {
      visibility: visible;
    }
  }    
 `;

export const userNameContainer = (props: IThemable) => `
  color:  ${props.theme.primaryColor};
  text-overflow: ellipsis;
  white-space:  nowrap;
  overflow: hidden;
`;

export const timeContainer = () => `
  opacity: .6;
`;

export const messageContainer = () => `
  word-break: break-word;

  &[data-type='${ChatMessageItemStatus.REMOVED}']:after {
    content: "(removed)";
    opacity: 0.6;
  }
  
  &[data-type='${ChatMessageItemStatus.EDITED}'] > :last-child:after {
    content: " (edited)";
    opacity: 0.6;
  }
`;

export const styledDropDownButtonTitle = () => {
  const commonIcons = clientEntityProvider.getIcons();

  return `
    pointer-events: none;
  
    &:before {
      display: inline-block;
      height: 100%;
      text-align: left;
      content: ${commonIcons.code.menuDotted};
      font-family: ${commonIcons.fontFamily};
    }
  `;
};
