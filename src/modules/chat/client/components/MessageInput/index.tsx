import 'react-quill/dist/quill.snow.css';
import * as React from 'react';
import ReactQuill from 'react-quill';
import ScrollbarArea from 'react-custom-scrollbars';
import styled from 'styled-components';
import clientEntityProvider from '../../../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

const SymbolCounter = styled.div`
  position: fixed;
  z-index: 10;
  right: 0.5rem;
  margin-top: 0.5rem;
  width: 2rem;
  float: right;
  font-size: 0.7rem;
`;

const QUILL_MODULES = {
  toolbar: false,
  clipboard: {
    matchVisual: false,
  },
};

const MAX_INPUT_HEIGHT = 240; // in px
const QUILL_FORMATS = ['bold', 'italic', 'link', 'blockquote', 'image'];

const COUNTER_THRESHOLD = 20;

export interface IMessageInputProps {
  onChange: (text: string) => void;
  onKeyDown: (e: KeyboardEvent) => void;
  value: string;
  key?: number;
  showNotification?: (arg: { hideProgressBar: boolean; type: string; content: string }) => void | null;
}

/**
 * Gets the approximate file size with an error nearly ~10%
 * @param str
 */
function getFilesizeFromBase64String(str: string) {
  const stringLength = str.length - 'data:image/jpeg;base64,'.length;

  // Dont ask me what numbers mean - i found it as they presented and they're work
  // tslint:disable-next-line:no-magic-numbers
  const sizeInBytes = 4 * Math.ceil(stringLength / 3) * 0.5624896334383812;

  // tslint:disable-next-line:no-magic-numbers
  return Math.ceil(sizeInBytes / 1024);
}

/**
 * Counts characters without tags
 * @param str
 * @return number
 */
const strCounter = (str: string): number => {
  return str.replace(/<[^>]+>/g, '').length; // clear from tags
};

/**
 * Displays counter in chat input while it runs out of threshold
 * @param props
 * @return React.ReactElement
 */
const Counter = (props: { text: string }): React.ReactElement => {
  const MESSAGE_LIMIT = clientEntityProvider.getConfig().chat.maxMessageLength;
  const remain = MESSAGE_LIMIT - strCounter(props.text);
  if (remain <= COUNTER_THRESHOLD) {
    return <SymbolCounter>{remain}</SymbolCounter>;
  }
  return null;
};

class MessageInput extends React.Component<IMessageInputProps> {
  private scroll: ScrollbarArea;

  public render() {
    return (
      <ScrollbarArea autoHeight={true} autoHeightMax={MAX_INPUT_HEIGHT} ref={this.setScrollbarRef}>
        <Counter text={this.props.value} />
        <ReactQuill
          value={this.props.value}
          modules={QUILL_MODULES}
          formats={QUILL_FORMATS}
          placeholder="Type a message here"
          onChange={this.onMsgChange}
          onKeyDown={this.props.onKeyDown}
        />
      </ScrollbarArea>
    );
  }

  private setScrollbarRef = (scroll: ScrollbarArea) => {
    if (scroll && scroll !== this.scroll) {
      this.scroll = scroll;
    }
  };

  /**
   * Fires each time textarea changed - no matter type you or add data via d n drop or ctrl+c;
   * @link clientEntityProvider.config.chat.maxUploadFileSize as lfsize (limited file size)
   * @param content
   */
  private onMsgChange = (content: string) => {
    const image = /src="([^"]+)"/g;
    const lfsize = clientEntityProvider.getConfig().chat.maxUploadFileSize;
    const MESSAGE_LIMIT = clientEntityProvider.getConfig().chat.maxMessageLength;

    if (content.length > 0) {
      const result = image.exec(content);
      const base64image = result !== null ? result[1] : null;

      if (base64image && base64image.length > 0) {
        const sizeInKb = getFilesizeFromBase64String(base64image);

        if (sizeInKb > lfsize) {
          content = '';
          this.props.showNotification({
            hideProgressBar: true,
            type: 'error',
            content: `The file size must not exceed ${lfsize / 1000} MB`,
          });
        }
      }

      if (strCounter(content) > MESSAGE_LIMIT) {
        content = '<p>' + content.replace(/<[^>]+>/g, '').slice(0, MESSAGE_LIMIT) + '</p>';
      }
    }

    this.props.onChange(content);

    if (this.scroll) {
      this.scroll.scrollToBottom();
    }
  };
}

export default MessageInput;
