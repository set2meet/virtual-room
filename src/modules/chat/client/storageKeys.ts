/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { Storages } from '../../../ioc/client/providers/StorageManager/storages/Storages';
import { IChatStorageKeys } from './types/IChatStorageKeys';

export const storageKeys: IChatStorageKeys = {
  CHAT_PLAY_SOUND_KEY: {
    key: `chat:playSoundOnNewMessage`,
    storage: Storages.LOCAL_STORAGE,
  },
};
