/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ChatAction } from './types/ChatAction';
import { IChatActionCreators } from './types/IChatActionCreators';
import { IChatMessageItem } from '../types/IChatMessageItem';

const actionCreators: IChatActionCreators = {
  initChatModule: () => ({
    type: ChatAction.CHAT_MODULE_INIT,
  }),
  addMessage: (msg: IChatMessageItem) => ({
    type: ChatAction.ADD_MESSAGE,
    msg,
  }),
  editMessage: (msg: IChatMessageItem) => ({
    type: ChatAction.EDIT_MESSAGE,
    msg,
  }),
  removeMessage: (msg: IChatMessageItem) => ({
    type: ChatAction.REMOVE_MESSAGE,
    msg,
  }),
  clearChat: (sessionId: string) => ({
    type: ChatAction.CLEAR_CHAT,
    sessionId,
  }),
  setAllRead: () => ({
    type: ChatAction.SET_READ_ALL,
  }),
  configToggleSoundMessage: (value: boolean) => ({
    type: ChatAction.CONFIG_MESSAGE_SOUND_TOGGLE,
    value,
  }),
};

export default actionCreators;
