/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { AnyAction } from 'redux';
import { findIndex as _findIndex, last as _last } from 'lodash';
import { IChatReduxState } from './types/IChatReduxState';
import { IChatMessageItem } from '../types/IChatMessageItem';
import { ChatAction } from './types/ChatAction';
import { ChatMessageItemStatus } from '../types/ChatMessageItemStatus';
const xss = require('xss');

export const INITIAL_STATE: IChatReduxState = {
  history: [],
  unreadId: null,
  config: {
    playSoundOnNewMessage: true,
  },
};

const isAllowToAppend = (prev: IChatMessageItem, current: IChatMessageItem): boolean => {
  const MIN_SECONDS_FOR_APPEND = 60;

  return (
    prev &&
    prev.status !== ChatMessageItemStatus.EDITED &&
    prev.user.id === current.user.id &&
    current.time - prev.time < MIN_SECONDS_FOR_APPEND
  );
};

export const reducers = {
  [ChatAction.ADD_MESSAGE](state: IChatReduxState, action: AnyAction) {
    const messageItem: IChatMessageItem = action.msg as IChatMessageItem;
    const prevMessageItem = state.history[state.history.length - 1];
    const newHistory = [...state.history];

    if (isAllowToAppend(prevMessageItem, messageItem)) {
      const newText = prevMessageItem.text + '\n\r' + xss(messageItem.text);

      newHistory[state.history.length - 1] = {
        id: prevMessageItem.id,
        text: newText,
        user: messageItem.user,
        status: prevMessageItem.status,
        time: messageItem.time,
        userColor: messageItem.userColor,
      };
    } else {
      newHistory.push({
        ...messageItem,
        text: xss(messageItem.text),
      });
    }

    return {
      ...state,
      history: newHistory,
      unreadId: state.unreadId || _last(newHistory).id,
    };
  },
  [ChatAction.EDIT_MESSAGE](state: IChatReduxState, action: AnyAction) {
    const newMessageItem: IChatMessageItem = action.msg as IChatMessageItem;
    const existsMessageItem = state.history.filter((item) => item.id === newMessageItem.id)[0];
    const newHistory = [...state.history];

    if (existsMessageItem) {
      const index = state.history.indexOf(existsMessageItem);

      newHistory[index] = {
        ...newMessageItem,
        text: xss(newMessageItem.text),
      };
    }

    return {
      ...state,
      history: newHistory,
    };
  },
  [ChatAction.REMOVE_MESSAGE](state: IChatReduxState, action: AnyAction) {
    const removedMessageItem: IChatMessageItem = action.msg as IChatMessageItem;
    const removeIndex = _findIndex(state.history, { id: removedMessageItem.id });
    const history = [...state.history];
    // adjust unread messages
    let unreadIndex = _findIndex(state.history, { id: state.unreadId });

    // remove message before unread
    if (unreadIndex > removeIndex) {
      unreadIndex--;
    }

    if (removeIndex > -1) {
      history.splice(removeIndex, 1);
    }

    let unreadId = null;

    if (history[unreadIndex]) {
      unreadId = history[unreadIndex].id;
    }

    return {
      ...state,
      history,
      unreadId,
    };
  },
  [ChatAction.CLEAR_CHAT](state: IChatReduxState): IChatReduxState {
    return {
      ...state,
      history: [],
      unreadId: null,
    };
  },
  [ChatAction.SET_READ_ALL](state: IChatReduxState): IChatReduxState {
    return {
      ...state,
      unreadId: null,
    };
  },
};

export default (state = INITIAL_STATE, action: AnyAction): IChatReduxState => {
  if (reducers.hasOwnProperty(action.type)) {
    return reducers[action.type](state, action);
  }

  return state;
};
