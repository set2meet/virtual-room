/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export enum ChatAction {
  CHAT_MODULE_INIT = 'CHAT:INIT_MODULE',
  ADD_MESSAGE = 'CHAT:ADD_MESSAGE',
  EDIT_MESSAGE = 'CHAT:EDIT_MESSAGE',
  REMOVE_MESSAGE = 'CHAT:REMOVE_MESSAGE',
  CLEAR_CHAT = 'CHAT:CLEAR_CHAT',
  SET_READ_ALL = 'CHAT:SET_READ_ALL',
  CONFIG_MESSAGE_SOUND_TOGGLE = 'CHAT:CONFIG_MESSAGE_SOUND_TOGGLE',
}
