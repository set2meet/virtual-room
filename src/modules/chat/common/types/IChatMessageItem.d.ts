/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TRoomUser } from '../../../../ioc/client/providers/AuthProvider/types/common/TAuthTokens';
import { ChatMessageItemStatus } from './ChatMessageItemStatus';

export interface IChatMessageItem {
  id: string;
  user: TRoomUser;
  time: number;
  text: string;
  status: ChatMessageItemStatus;
  userColor: string;
}
