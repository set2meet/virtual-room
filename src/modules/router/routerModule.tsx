import { ConnectedRouter, ConnectedRouterProps, connectRouter, routerMiddleware } from 'connected-react-router';
import * as React from 'react';
import Socket = SocketIOClient.Socket;
import { ComponentType } from 'react';
import { IS2MModule, TS2MModuleFactory } from '../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { TRouterModuleInitOptions } from './TRouterModuleInitOptions';

const createRouterModule: TS2MModuleFactory<TRouterModuleInitOptions> = async (
  socket: Socket,
  stateKey: string = 'router',
  moduleInitOptions?: TRouterModuleInitOptions
): Promise<IS2MModule> => {
  if (!moduleInitOptions || !moduleInitOptions.history) {
    throw new Error(
      'Module was init without required options! Try passing the correct initOptions to .resolveModuleSuspended'
    );
  }
  const { history } = moduleInitOptions;
  const WrappedRouter: ComponentType<ConnectedRouterProps> = (props) => (
    <ConnectedRouter {...props} history={history}>
      {props.children}
    </ConnectedRouter>
  );

  return {
    rootComponent: WrappedRouter,
    reduxModules: [
      {
        id: stateKey,
        reducerMap: {
          [stateKey]: connectRouter(history),
        },
        middlewares: [routerMiddleware(history)],
      },
    ],
  };
};

export default createRouterModule;
