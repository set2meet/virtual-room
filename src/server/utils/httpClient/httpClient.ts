/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { isNull } from 'lodash';
import resolveUrl from 'resolve-url';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { TAuthTokens } from '../../../ioc/client/types/interfaces';
import { HttpStatusCode } from '../../../types/httpStatusCode';
import { HttpClientError } from './HttpClientError';

interface IAxiosConfigExt extends AxiosRequestConfig {
  _retry: boolean;
}
interface IAxiosErrorExt extends AxiosError {
  config: IAxiosConfigExt;
}

const addBearerHeader = (axiosConfig: AxiosRequestConfig) => {
  const storageManager = clientEntityProvider.getStorageManager();
  const tokens: TAuthTokens = storageManager.get(clientEntityProvider.getStorageKeys().AUTH_TOKEN_KEY);

  axiosConfig.headers.Authorization = !isNull(tokens) ? `Bearer ${tokens.accessToken}` : '';
  return axiosConfig;
};

const ROUTE_TOKEN = '/token';
const ROUTE_VALIDATE_TOKEN = '/validate-token';

/**
 * Updates token if expired
 * @param originalRequest
 * @param client
 */
const updateTokenIfExpired = (originalRequest: IAxiosConfigExt, client) => {
  originalRequest._retry = true;
  const storageManager = clientEntityProvider.getStorageManager();
  const AUTH_TOKEN_KEY = clientEntityProvider.getStorageKeys().AUTH_TOKEN_KEY;
  const tokens: TAuthTokens = storageManager.get(AUTH_TOKEN_KEY);

  return client
    .post(ROUTE_TOKEN, {
      tokens,
    })
    .then((res) => {
      if (res.status === HttpStatusCode.CREATED) {
        const { data } = res;
        const newTokens: TAuthTokens = {
          accessToken: data.access_token,
          refreshToken: data.refresh_token,
          strategy: tokens.strategy,
        };

        storageManager.set<TAuthTokens>(AUTH_TOKEN_KEY, newTokens);

        // we need new tokens in body only for validate-token route
        // cuz if we not modify failed request - we will validate expired tokens and receive bad request
        // in other situations there formData or any other content (even binary)
        // and we do not need to overwrite body

        if (originalRequest.url.includes(ROUTE_VALIDATE_TOKEN)) {
          originalRequest.data = {
            tokens: newTokens,
          };
        }

        axios.defaults.headers.common.Authorization = 'Bearer ' + newTokens.accessToken;
        originalRequest.headers.Authorization = 'Bearer ' + newTokens.accessToken;

        return axios(originalRequest);
      }
    })
    .catch((e) => {
      if (e.response?.status === HttpStatusCode.BAD_REQUEST) {
        const error = new Error('Sorry, your session has expired. Please refresh the page to continue.');

        // TODO need to handle tokens through redux, and save error messages somewhere inside redux state
        // TODO and show message box if message exists
        // @ts-ignore
        error.errorType = HttpClientError.session_expired;

        throw error;
      }

      throw e;
    });
};

const requestInterceptor = (response: AxiosResponse): AxiosResponse => response;
const errorHandlerInterceptor = (error: IAxiosErrorExt, client: AxiosInstance) => {
  const originalRequest = error.config;
  if (error.response?.status === HttpStatusCode.UNAUTHORIZED && !originalRequest._retry) {
    return updateTokenIfExpired(originalRequest, client);
  }

  if (
    error.request?.responseType === 'blob' &&
    error.response?.data instanceof Blob &&
    error.response?.data?.type.toLowerCase().indexOf('json') !== -1
  ) {
    return new Promise((resolve, reject) => {
      // https://github.com/axios/axios/issues/815
      const reader = new FileReader();

      reader.onload = () => {
        error.response.data = JSON.parse(reader.result as string);

        resolve(Promise.reject(error));
      };

      reader.onerror = () => {
        reject(error);
      };

      reader.readAsText(error.response.data);
    });
  }

  return Promise.reject(error);
};

export const httpClient = () => {
  const config = clientEntityProvider.getConfig();
  const service = config.service;

  const client: AxiosInstance = axios.create({
    baseURL: resolveUrl(service.url, service.path),
  });

  client.interceptors.request.use(addBearerHeader);

  client.interceptors.response.use(
    (response: AxiosResponse) => requestInterceptor(response),
    (error: IAxiosErrorExt) => errorHandlerInterceptor(error, client)
  );

  return client;
};
