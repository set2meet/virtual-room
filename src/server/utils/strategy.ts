/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import passportCustom, { Strategy } from 'passport-custom';
import api, { bearerToken } from '@s2m/resource-management';
import parseToken from '@s2m/resource-management/lib/utils/parseToken';

const CustomStrategy = passportCustom.Strategy;

const urlsWhitelist = ['/config', '/login', '/token'];
export const createCustomStrategy = (): Strategy =>
  new CustomStrategy(async (req: any, done) => {
    if (urlsWhitelist.includes(req.url)) {
      // i hope you will avoid using req.user in non-auth routes cuz it will be empty
      return done(null, {});
    }

    try {
      const token = bearerToken(req.header.authorization);
      // TODO::extend resource-management interfaces
      const user: any = await api().fetchUserInfo(token);
      const parsedToken: any = parseToken(token);
      user.roles = parsedToken.payload.realm_access.roles;
      done(null, user);
    } catch (e) {
      done(null, false);
    }
  });
