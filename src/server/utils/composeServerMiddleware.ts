/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IServerMiddleware, IServerMiddlewareContext, IServerMiddlewareNext } from '../../ioc/server/types/interfaces';

const composeServerMiddlware = (middleware: IServerMiddleware[]): IServerMiddleware => {
  if (!Array.isArray(middleware)) {
    throw new TypeError('Middleware stack must be an array!');
  }
  for (const fn of middleware) {
    if (typeof fn !== 'function') {
      throw new TypeError('Middleware must be composed of functions!');
    }
  }

  /**
   * @param {Object} context
   * @return {Promise}
   * @api public
   */
  const middlewareDispatch = (context: IServerMiddlewareContext, next: IServerMiddlewareNext) => {
    // last called middleware #
    let index = -1;
    return dispatch(0).catch((e) => {
      console.error(`Error in server middleware: ${e}`);
    });

    function dispatch(i: number) {
      if (i <= index) {
        return Promise.reject(new Error('next() called multiple times'));
      }
      index = i;
      const fn = middleware[i];

      if (i === middleware.length) {
        next();
      }
      if (!fn) {
        return Promise.resolve();
      }
      try {
        return Promise.resolve(fn(context, dispatch.bind(null, i + 1)));
      } catch (err) {
        return Promise.reject(err);
      }
    }
  };

  return middlewareDispatch;
};

export default composeServerMiddlware;
