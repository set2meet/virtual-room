/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import SocketSessionsMap from './sessions';
import * as hookEmit from './hooks/emit';
import * as hookGetRoomParticipants from './hooks/getRoomParticipants';

type SocketIOAdapterHookInput = {
  key: 'emit' | 'getRoomParticipants';
  data: any; // todo
};

export default (io: SocketIO.Namespace, sessions: SocketSessionsMap) => {
  // build adapter hooks
  const handlers = {
    emit: hookEmit.handler(io, sessions),
    getRoomParticipants: hookGetRoomParticipants.handler(io, sessions),
  };

  const wrapper = (key: string) => (data: any) => ({ key, data });

  (io.adapter as any).customHook = (data: SocketIOAdapterHookInput, cb: any) => {
    const handle = handlers[data.key];

    if (handle) {
      handle(data.data, (args: any) => {
        cb(args);
      });
    }
  };

  return {
    emit: hookEmit.request(io, wrapper('emit')),
    getRoomParticipants: hookGetRoomParticipants.request(io, wrapper('getRoomParticipants')),
    getRoomClientSocket: (roomId: string, userId: string) => sessions.getByFilter(userId, { roomId }),
  };
};
