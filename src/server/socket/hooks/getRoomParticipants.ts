/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import {
  SocketIOAdapterHookRequestBuilder as RequestBuilder,
  SocketIOAdapterHookHandlerBuilder as HandlerBuilder,
} from './_';

type HookDataInput = string;
type HookDataOutput = Record<string, 1>;

export const request: RequestBuilder<HookDataInput, HookDataOutput> = (io, wrapper) => (data) =>
  new Promise((resolve, reject) => {
    if (!data) {
      // log about it;
      return reject();
    }

    (io.adapter as any).customRequest(wrapper(data), (err: Error, answer: HookDataOutput[]) => {
      if (err) {
        // log error;
        reject(err);
      } else {
        resolve(Object.assign({}, ...answer));
      }
    });
  });

export const handler: HandlerBuilder<HookDataInput, HookDataOutput> = (io, sessions) => (data, cb) => {
  cb(
    sessions
      .getUsersInTheRoom(data)
      .reduce((output: HookDataOutput, userId: string) => ((output[userId] = 1), output), {})
  );
};
