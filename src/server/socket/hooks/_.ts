/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import SocketSessionsMap from '../sessions';

type RequestWrapper<D> = (data: D) => { key: string; data: D };
type SocketIOAdapteRequestBuilder<Fn, D> = (io: SocketIO.Namespace, wrapper: RequestWrapper<D>) => Fn;
type SocketIOAdapteHandlerBuilder<Fn> = (io: SocketIO.Namespace, sessions: SocketSessionsMap) => Fn;

type SocketIOAdapterHookRequest<D, A> = (data: D) => Promise<A>;
export type SocketIOAdapterHookRequestBuilder<D, A> = SocketIOAdapteRequestBuilder<SocketIOAdapterHookRequest<D, A>, D>;

type SocketIOAdapterHookHandler<D, A> = (data: D, cb: (answer: A) => void) => void;
export type SocketIOAdapterHookHandlerBuilder<D, A> = SocketIOAdapteHandlerBuilder<SocketIOAdapterHookHandler<D, A>>;
