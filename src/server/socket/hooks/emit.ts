/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import {
  SocketIOAdapterHookRequestBuilder as RequestBuilder,
  SocketIOAdapterHookHandlerBuilder as HandlerBuilder,
} from './_';

type HookDataInput = {
  target: {
    roomId?: string;
    users?: string[];
  };
  event: string;
  payload: any;
};
type HookDataOutput = null;

const emit = (socket: SocketIO.Namespace, event: string, payload: any) => {
  if (event === 'action') {
    socket.emit('action', {
      action: {
        ...payload,
        SERVER: 1,
      },
    });
  } else {
    socket.emit(event, payload);
  }
};

export const request: RequestBuilder<HookDataInput, HookDataOutput> = (io, wrapper) => (data) =>
  new Promise((resolve, reject) => {
    const hasTargetRoom = !!data.target.roomId;
    const hasTargetUsers = !!data.target.users;

    if (!(hasTargetRoom || hasTargetUsers)) {
      // log about it;
      return reject();
    }

    if (!data.event) {
      // log about it;
      return reject();
    }

    // simplify behaviour for the room only messages
    if (hasTargetRoom && !hasTargetUsers) {
      const {
        event,
        payload,
        target: { roomId },
      } = data;

      return emit(io.to(`rooms/${roomId}`), event, payload);
    }

    if (!hasTargetUsers) {
      // log about it;
      return reject();
    }

    (io.adapter as any).customRequest(wrapper(data), (err: Error) => {
      if (err) {
        // log error;
        reject(err);
      } else {
        resolve(null);
      }
    });
  });

export const handler: HandlerBuilder<HookDataInput, HookDataOutput> = (io, sessions) => (data, cb) => {
  const {
    payload,
    event,
    target: { roomId, users },
  } = data;

  // if no any uers target
  if (!(users && users.length)) {
    return cb(null);
  }

  // target may be roomId & userId
  // so, make corresponded filter
  const filter = roomId ? { roomId } : null;

  users.forEach((userId) => {
    const sockets = sessions.get(userId, filter);
    if (sockets && sockets.length) {
      if (event === 'action') {
        sockets.forEach((socket) => {
          socket.emit('action', { action: payload });
        });
      } else {
        sockets.forEach((socket) => socket.emit(event, payload));
      }
    }
  });

  cb(null);
};
