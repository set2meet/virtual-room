/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import path from 'path';
import moduleAlias from 'module-alias';

// TODO::remove module alias
moduleAlias.addAliases({
  // so path aliases can work in compiled code
  src: path.resolve(__dirname, '../../'),
});

import * as http from 'http';
import * as https from 'https';
import SocketIO from 'socket.io';
import redisAdapter from 'socket.io-redis';
import serverEntityProvider from '../../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import SocketSessionsMap from './sessions';
import createSocketAdapter from './adapter';
import jwt from 'jsonwebtoken';

/**
 * Method works in two cases:
 * First case is 'default' auth in S2M,
 * I won't describe it there.
 * Second case is 'sso with IDP',
 * In second case in preferred_username placed absolutely wrong data, maybe, its userId from third-party SSO.
 * Token provided by keycloak;
 * @param jwtToken
 * @deprecated in SETMEET-2269, commit #4178d52
 */
const getUserIdFromToken = (jwtToken: string): string => {
  const decodedToken = jwt.decode(jwtToken) as any;

  if (decodedToken.hasOwnProperty('sub')) {
    return decodedToken.sub;
  } else {
    return decodedToken.preferred_username || decodedToken.id;
  }
};

const errors = {
  invalidAuthorization: {
    code: 1,
    message: 'authorization error',
  },
};

export default (server: https.Server | http.Server, namespace: string) => {
  // create socket server instance
  const io = SocketIO(server, {
    origins: '*:*',
    path: '/ws',
    pingTimeout: 3000,
    pingInterval: 3000,
  });

  // session map: userId <=> socketId
  const sessions = new SocketSessionsMap([
    {
      filterKey: 'roomId',
      actionKey: 'roomId',
      addActionType: serverEntityProvider.getActionTypes().ROOM_JOIN_SUCCESS,
      delActionType: serverEntityProvider.getActionTypes().ROOM_LEAVE,
    },
  ]);

  // create adapter for multiple instances
  io.adapter(redisAdapter(serverEntityProvider.getConfig().redisConfig));

  const ions = io.of(namespace);

  // middleware: check authorization
  ions.use(async (socket, next) => {
    // const { token } = socket.handshake.query;
    // try {
    //   const user = await api().fetchUserInfo(token);
    //
    //   if (user && user.id) {
    //     next();
    //   }
    // } catch (e) {
    //   next(errors.invalidAuthorization);
    // }
    const success = true; // await checkTokenValidness(token);

    if (success) {
      next();
    } else {
      next(errors.invalidAuthorization);
    }
  });

  // middleware: update sessions map
  ions.use((socket, next) => {
    const { token } = socket.handshake.query;
    const userId = getUserIdFromToken(token);

    sessions.add(userId, socket);
    socket.on('disconnect', () => sessions.del(userId, socket.id));

    next();
  });

  const socketIO = ions;
  const socketAdapter = createSocketAdapter(socketIO, sessions);

  return { socketIO, socketAdapter };
};
