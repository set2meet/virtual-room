/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import _find from 'lodash/find';
import { Socket } from 'socket.io';

// SS = socket sessions
// SSF = socket sessions filter

type SSFConfig = {
  filterKey: string;
  actionKey: string;
  addActionType: string;
  delActionType: string;
};
type SSRec = { socket: Socket };
type SSFRec = Record<string, any>;

class SocketSessions {
  private _sessions: Map<string, SSRec & SSFRec> = new Map();

  public add(socket: Socket) {
    this._sessions.set(socket.id, { socket });
  }

  public del(id: string) {
    this._sessions.delete(id);

    return this._sessions.size === 0;
  }

  public update(id: string, key: string, value: any) {
    const rec = this._sessions.get(id);

    if (rec) {
      rec[key] = value;
    }
  }

  public find(): SocketIO.Socket[] {
    const sockets: SocketIO.Socket[] = [];

    this._sessions.forEach((rec) => {
      sockets.push(rec.socket);
    });

    return sockets.length ? sockets : null;
  }

  public findByFilter(filter: SSFRec = {}): SocketIO.Socket {
    let socket: SocketIO.Socket = null;

    this._sessions.forEach((rec) => {
      if (filter && _find([rec], filter)) {
        socket = rec.socket;
      }
    });

    return socket;
  }
}

class SocketSessionsMap {
  private _map: Record<string, SocketSessions> = {};

  constructor(private _filterActions: SSFConfig[]) {}

  public add(userId: string, socket: SocketIO.Socket) {
    const socketSessions = (this._map[userId] = this._map[userId] || new SocketSessions());

    // handle filters by actions
    socket.on('action', (action) => {
      this._filterActions.forEach((rec) => {
        if (action.type === rec.addActionType) {
          socketSessions.update(socket.id, rec.filterKey, action[rec.actionKey]);
        }
        if (action.type === rec.delActionType) {
          socketSessions.update(socket.id, rec.filterKey, undefined);
        }
      });
    });

    // just store socket connection
    socketSessions.add(socket);
  }

  public del(userId: string, socketId: string) {
    const rec = this._map[userId];

    if (rec) {
      if (rec.del(socketId)) {
        delete this._map[userId];
      }
    }
  }

  public get(userId: string, filter?: SSFRec): SocketIO.Socket[] {
    const rec = this._map[userId];

    if (!rec) {
      return;
    }

    if (filter) {
      const socket = this.getByFilter(userId, filter);

      return socket ? [socket] : [];
    }

    return rec.find();
  }

  public getByFilter(userId: string, filter: SSFRec): SocketIO.Socket {
    const rec = this._map[userId];

    return rec ? rec.findByFilter(filter) : null;
  }

  public getUsersInTheRoom(roomtId: string): string[] {
    return Object.keys(this._map).filter((userId) => {
      return !!this._map[userId].findByFilter({ roomtId });
    });
  }
}

export default SocketSessionsMap;
