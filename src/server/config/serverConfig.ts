/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { readConfig, IVRConfig, IConfigWrapper } from 'env-config';
import TServerConfig from './types/TServerConfig';

export default async function buildServerConfig(): Promise<TServerConfig & IConfigWrapper> {
  return await readConfig<IVRConfig>();
}
