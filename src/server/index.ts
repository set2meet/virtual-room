/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { noop as _noop, set as _set } from 'lodash';
import * as https from 'https';
import * as http from 'http';
import path from 'path';
import { Socket } from 'socket.io';
import IORedis from 'ioredis';
import { AnyAction } from 'redux';
import formidable from 'koa2-formidable';
import createSocket from './socket/index';
import Koa from 'koa'; // core
import cors from '@koa/cors';
import Router from 'koa-router';
import passport from 'koa-passport';
import koaLogger from 'koa-bunyan';
import {
  IServerMetaObjects,
  IServerMiddlewareNext,
  IServerMiddlewareOptionalContext,
} from '../ioc/server/types/interfaces';
import buildClientConfig from './config/clientConfig';
import serverEntityProvider from '../ioc/server/providers/ServerEntityProvider/ServerEntityProvider';
import { IServerEntityProviderInternals } from '../ioc/server/providers/ServerEntityProvider/types/IServerEntityProvider';
import bodyParser from 'koa-bodyparser';
import { CURRENT_STRATEGY, LogErrorCode } from '../ioc/server/ioc-constants';
import { IBackendTransportConfig } from '@s2m/logger';

const SEND_MESSAGE_AUTHORIZED_TIMEOUT = 500;

// use aliases src/ for imports
process.env.NODE_CONFIG_DIR = path.join(process.cwd(), 'config/');

/*
Creates server with its configs
 */
const createServer = async () => {
  await (serverEntityProvider as IServerEntityProviderInternals).initialize();
  const serverMiddleware = serverEntityProvider.getServerMiddleware();
  const logger = serverEntityProvider.getLogger();
  const config = serverEntityProvider.getConfig();

  if (!config.redisConfig) {
    console.log('Error: not found redis config');
    process.exit(1);
  }

  const PORT = config.project.port;
  const app = new Koa();

  const router = new Router();
  let server = null;

  app.use(cors());
  app.use(formidable({}));
  app.use(bodyParser());
  app.use(
    koaLogger(logger.server, {
      level: config.logLevel || 'info',
      timeLimit: 100,
    })
  );

  passport.use(serverEntityProvider.getCustomStrategy());

  app.use(passport.initialize());

  let socketIONamespace = '/';

  if (process.env.NODE_ENV === 'development') {
    server = https.createServer(config.httpsOptions, app.callback());
  } else {
    socketIONamespace = '/api/v1/';
    server = http.createServer(app.callback());
  }

  /**
   * socketIO server instance
   */
  const { socketIO, socketAdapter } = createSocket(server, socketIONamespace);
  logger.server.startToSync(({
    serverFactory: () => socketIO,
  } as any) as IBackendTransportConfig);

  /**
   * meta objects for modules
   */
  const appMetaObjects: IServerMetaObjects = {
    socketAdapter,
    logger,
    redis: {
      pubClient: new IORedis(config.redisConfig),
      subClient: new IORedis(config.redisConfig),
    },
    dispatchAction: null,
    config,
  };

  // helper method to allow middleware call another round of actions
  const dispatchAction = (
    action: AnyAction,
    next: IServerMiddlewareNext,
    extraContext?: IServerMiddlewareOptionalContext
  ) => {
    serverMiddleware(
      {
        action,
        appMetaObjects,
        ...extraContext,
      },
      next
    );
  };

  appMetaObjects.dispatchAction = dispatchAction;

  // call first action 'INIT'
  dispatchAction({ type: '@APP_SERVER:REDUX:INIT' }, _noop);

  /**
   * on websocket connection if user authorized, listen for user actions
   */
  socketIO.on('connection', (clientSocket: Socket) => {
    setTimeout(() => clientSocket.emit('authorized'), SEND_MESSAGE_AUTHORIZED_TIMEOUT);

    clientSocket.on('action', (action: AnyAction) => {
      _set(action, 'meta.clientSocketId', clientSocket.id);
      dispatchAction(action, _noop, { clientSocket });
    });

    (serverEntityProvider as IServerEntityProviderInternals).getRegisterSocketEvents()(clientSocket, appMetaObjects);
  });

  (serverEntityProvider as IServerEntityProviderInternals).getRegisterRoutes()(router, appMetaObjects);

  /**
   * client config
   */
  router.get('/config', async (ctx, next) => {
    ctx.body = buildClientConfig(serverEntityProvider.getConfig().cleanupSecrets());
    await next();
  });

  app.use(passport.authenticate(CURRENT_STRATEGY, { session: false }));
  app.use(router.routes()).use(router.allowedMethods());
  server.listen(PORT);
};

createServer().catch(async (e) => {
  serverEntityProvider.getLogger().server.error(LogErrorCode.SERVER, e.toString());
  await (serverEntityProvider as IServerEntityProviderInternals).destroy();
});
