import React, { MouseEvent } from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../ioc/common/ioc-interfaces';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { Panel } from 'react-bootstrap';
import styled, { ThemeProps, withTheme } from 'styled-components';

import * as BootstrapButton from 'react-bootstrap/lib/Button';
import { ITheme } from '../../../ioc/client/types/interfaces';
import { styledContainer } from './Dashboard.style';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { TRoomUser } from '../../../ioc/client/providers/AuthProvider/types/common/TAuthTokens';

const StyledDashboard = styled.div`
  ${styledContainer};
`;

interface IDashboardStateProps {
  user: TRoomUser;
  disableRecordings: boolean;
}

interface IDashboardDispatchProps {
  sendAction: ActionCreator<AnyAction>;
  gotoRecordings: (userId: string) => any;
}

export class Dashboard extends React.Component<IDashboardStateProps & IDashboardDispatchProps & ThemeProps<ITheme>> {
  private _joinMyRoom = (evt: MouseEvent<BootstrapButton>) => {
    const appActions = clientEntityProvider.getActionCreators();
    const sessionId = clientEntityProvider.getBrowser().info.sessionId;

    this.props.sendAction(appActions.createJoinRoom(this.props.user.id, sessionId, false, this.props.user.id));
  };

  private _joinTemporaryRoom = () => {
    const appActions = clientEntityProvider.getActionCreators();
    const sessionId = clientEntityProvider.getBrowser().info.sessionId;

    this.props.sendAction(appActions.createJoinRoom(this.props.user.id, sessionId, true));
  };

  private _gotoRecordings = () => {
    this.props.gotoRecordings(this.props.user.id);
  };

  public render() {
    const { Button } = clientEntityProvider.getComponentProvider().UI;
    const { disableRecordings } = this.props;

    return (
      <StyledDashboard className="s2m-dashboard-body">
        <Panel>
          <div>
            <img src={this.props.theme.oneTimeRoomPageLogo} />
          </div>
          <div>Start a new meeting in the default Virtual Room. No meeting data is preserved</div>
          <div>
            <Button onClick={this._joinTemporaryRoom}>MEETING IN ONE-TIME ROOM</Button>
          </div>
        </Panel>
        <Panel>
          <div>
            <img src={this.props.theme.myRoomPageLogo} />
          </div>
          <div>Start a new meeting in your own personal room</div>
          <div>
            <Button data-id={this.props.user.roomId} onClick={this._joinMyRoom}>
              MEETING IN MY ROOM
            </Button>
          </div>
        </Panel>
        {!disableRecordings && (
          <Panel>
            <div>
              <img src={this.props.theme.customizePageLogo} />
            </div>
            <div>Review and manage your meeting recordings</div>
            <div>
              <Button onClick={this._gotoRecordings}>MEETING RECORDINGS</Button>
            </div>
          </Panel>
        )}
      </StyledDashboard>
    );
  }
}

export default connect<IDashboardStateProps, IDashboardDispatchProps>(
  ({ auth }: IStoreState) => {
    const config = clientEntityProvider.getConfig();

    return {
      user: auth.user,
      disableRecordings: config.project.gui.disableRecordings,
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        sendAction: appActions.sendAction,
        gotoRecordings: appActions.gotoRecordings,
      },
      dispatch
    );
  }
)(withTheme(Dashboard));
