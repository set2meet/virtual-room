/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IRequiredTheme } from '../../../ioc/client/types/interfaces';

export const styledContainer = ({ theme }: IRequiredTheme) => `
  height: 100%;
  display: flex;
  justify-content: center;
  padding-top: 90px;
  .panel {
    padding: 30px;
    border: none;
    border-radius: 0;
    background-color: ${theme.backgroundColorSecondary};
    margin-right: 30px;
    width: 340px;
    height: 380px;
    text-align: center;

    div {
      display: flex;
      margin-bottom: 40px;
      justify-content: center;
      img {
        width: 215px;
        height: 140px;
      }
    }

    button {
      width: 250px;
    }
  }
`;
