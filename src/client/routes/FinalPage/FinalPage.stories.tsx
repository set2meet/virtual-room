import React, { ComponentType } from 'react';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { withIoCFactory } from '../../../test/utils/utils';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

// make sure that props interface are exported separately from the component code to don't break the lazy-loading idea
const finalPageFactory = () => clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.FinalPage);

// In order to workaround import order issues each ioc factory call should be wrapped inside render function
const SuspendedFinalPage: ComponentType = withIoCFactory(finalPageFactory);

export default {
  title: 'routes/FinalPage',
  component: SuspendedFinalPage,
};

export const Default = () => <SuspendedFinalPage />;
