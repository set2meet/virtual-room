import React, { Component } from 'react';
import styled from 'styled-components';
import { IRequiredTheme } from '../../../ioc/client/types/interfaces';

const StyledFinalPage = styled.div`
  ${({ theme }: IRequiredTheme) => `
  margin-top: 200px;
  display: flex;
  align-items: center;
  flex-direction: column;

  .s2m-logo-short {
    width: 140px;
    height: 88px;
    margin-bottom: 50px;   
    background-image: url(${theme.logoShort});
    background-repeat: no-repeat;
    background-position: center bottom;
    background-size: contain;
  }

  .s2m-wp-text {
    display: flex;
    align-items: center;
    font-size: 22px;

    *:first-child {
      margin-right: 20px;
    }
  }
`};
`;

class FinalPage extends Component {
  public render() {
    return (
      <StyledFinalPage>
        <div className="s2m-logo-short" />
        <div className="s2m-wp-text">Thank you for joining!</div>
      </StyledFinalPage>
    );
  }
}

export default FinalPage;
