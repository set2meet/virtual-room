import React from 'react';
import index from '../../assets/index.html';

export default {
  title: 'Preloader',
  parameters: {
    storyshots: false,
    themeDecorator: {
      disable: true,
    },
  },
};

export const Default = () => <div style={{ height: '100%' }} dangerouslySetInnerHTML={{ __html: index }} />;
