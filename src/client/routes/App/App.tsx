import React from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../ioc/common/ioc-interfaces';
import { Route, RouteComponentProps, Switch } from 'react-router';
import Dashboard from '../Dashboard';
import Room from '../Room';
import Recordings from '../Recordings';
import Video from '../Video';
import Login from '../Login';
import ModalDialog from '../../../modules/app/components/ModalDialog';
import styled, { css, ThemeProvider } from 'styled-components';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import _get from 'lodash/get';
import { isGuest as isGuestSelector } from '../../../modules/app/redux/selectors';
import {
  IBrowser,
  IRequiredTheme,
  IThemes,
  TRoomUser,
  TWebphoneModuleInitOptions,
} from '../../../ioc/client/types/interfaces';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import HeaderSkeleton from '../../../modules/app/components/Header/HeaderSkeleton';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { TDynamicModuleWrapperProps } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { StateMessage } from '../Room/StateMessage';

const StyledApp = styled.div`
  ${({ theme }: IRequiredTheme) => css`
    display: flex;
    flex-direction: column;
    height: 100%;
    background-color: ${theme.backgroundColor};
    color: ${theme.fontColor};
    font-family: 'Roboto';
    .s2m-header {
      height: 70px;
    }
    .s2m-body {
      display: inline-block;
      width: 100%;
      height: calc(100% - 70px);
    }
  `};
`;

const ConnectedSwitch = connect(({ router }: IStoreState) => ({
  location: router.location,
}))(Switch);

export interface IAppStateProps {
  user: TRoomUser;
  realm: string;
  isGuest: boolean;
  projectName: string;
  techSupportEMail: string;
  userMediaCheckRoute: string;
  disableRecordings: boolean;
}

export class App extends React.Component<IAppStateProps> {
  private themes: IThemes = clientEntityProvider.getThemes();
  private browser: IBrowser = clientEntityProvider.getBrowser();

  public render() {
    const { WebPhone, UI, FinalPage } = clientEntityProvider.getComponentProvider();
    const Header = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.Header, <HeaderSkeleton />);
    const { browser, themes } = this;
    const { realm, user, isGuest } = this.props;
    const selectedTheme = _get(themes, realm, themes.defaultTheme);
    const { projectName, techSupportEMail, userMediaCheckRoute, disableRecordings } = this.props;
    const WebrtcWrapper = ({ children, fallback }) => {
      const MediaModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Media);
      const WebphoneModule = clientEntityProvider.resolveModuleSuspended<
        TDynamicModuleWrapperProps,
        TWebphoneModuleInitOptions
      >(
        ModuleRegistryKey.Webphone,
        {
          webRTCServices: clientEntityProvider.getWebRTCServices(),
        },
        null
      );
      const WebrtcModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Webrtc, null);

      return (
        <MediaModule fallback={fallback}>
          <WebphoneModule fallback={fallback}>
            <WebrtcModule fallback={fallback}>{children}</WebrtcModule>
          </WebphoneModule>
        </MediaModule>
      );
    };
    const RoomRoute = (props: RouteComponentProps) => {
      const RoomFallback = <UI.WaitingPage text={StateMessage.EnteringRoom} />;
      const RoomModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.RoomModule, null);
      const RecordingModule = clientEntityProvider.resolveModuleSuspended(
        ModuleRegistryKey.Recording,
        {
          webRTCServices: clientEntityProvider.getWebRTCServices(),
        },
        null
      );
      return (
        <RoomModule fallback={RoomFallback}>
          <RecordingModule fallback={RoomFallback}>
            <WebrtcWrapper fallback={RoomFallback}>
              <Room {...props} />
            </WebrtcWrapper>
          </RecordingModule>
        </RoomModule>
      );
    };
    const UserSetupRoute = (props: RouteComponentProps) => {
      return (
        <WebrtcWrapper fallback={<UI.WaitingPage text="Initializing..." />}>
          <WebPhone.UserSetup />
        </WebrtcWrapper>
      );
    };

    return (
      <ThemeProvider theme={selectedTheme}>
        <StyledApp className="s2m-vr">
          <UI.GlobalStyles theme={selectedTheme} />
          <div className="s2m-header">
            <ConnectedSwitch>
              <Header />
            </ConnectedSwitch>
          </div>
          <div className="s2m-body">
            <ConnectedSwitch>
              {!browser.supportWebRTC && (
                <UI.BrowserNotSupported projectName={projectName} techSupportEMail={techSupportEMail} />
              )}
              <Route path={userMediaCheckRoute} component={UserSetupRoute} />
              {isGuest && <Route path="/thanks/" component={FinalPage} />}
              {!user && <Route component={Login} />}
              <Route path="/room/:roomId" render={RoomRoute} />
              {isGuest && <Route component={Login} />}
              {!disableRecordings && <Route path="/recordings/:userId?" component={Recordings} />}
              <Route path="/video/" component={Video} />
              <Route component={Dashboard} />
            </ConnectedSwitch>
          </div>
          <ModalDialog />
        </StyledApp>
      </ThemeProvider>
    );
  }
}

export default connect<IAppStateProps, {}>(
  (state: IStoreState) => {
    const config = clientEntityProvider.getConfig();

    return {
      user: state.auth.user,
      realm: _get(state, 'auth.tokens.realm'),
      isGuest: isGuestSelector(state),
      projectName: config.project.name,
      techSupportEMail: config.project.contacts.support,
      userMediaCheckRoute: config.project.routes.userMediaCheck,
      disableRecordings: 'gui' in config.project ? config.project.gui.disableRecordings : false,
    };
  },
  () => ({})
)(App);
