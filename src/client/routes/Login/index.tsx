import React from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../ioc/common/ioc-interfaces';
import resolveUrl from 'resolve-url';
import isGuest from '../../../modules/app/redux/selectors/isGuest';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';

interface ILoginStateProps {
  remoteServiceUrl: string;
  showGuestScreen: boolean;
  isGuest: boolean;
}

interface ILoginDispatchProps {
  showNotification: ActionCreator<AnyAction>;
}

export class Login extends React.Component<ILoginStateProps & ILoginDispatchProps> {
  public render() {
    const { LoginPage } = clientEntityProvider.getComponentProvider().Auth;

    // dont reconnect guest if he is already logged in and try to navigate to any page but /room/%id%
    // see also App.tsx for routing
    return (
      <LoginPage
        showGuestScreen={this.props.showGuestScreen}
        reconnectGuest={!this.props.isGuest && this.props.showGuestScreen}
        authParams={clientEntityProvider.config.auth}
        serviceParams={this.props.remoteServiceUrl}
        showNotification={this.props.showNotification}
      />
    );
  }
}

export default connect<ILoginStateProps, ILoginDispatchProps>(
  (store: IStoreState) => {
    const { service } = clientEntityProvider.getConfig();

    return {
      showGuestScreen: store.router.location.pathname.indexOf('room') > -1,
      remoteServiceUrl: resolveUrl(service.url, service.path),
      isGuest: isGuest(store),
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        showNotification: appActions.showNotification,
      },
      dispatch
    );
  }
)(Login);
