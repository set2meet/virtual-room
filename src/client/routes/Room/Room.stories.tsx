import React from 'react';
import Room from './index';
import { connectDecorator } from '../../../../.storybook/decorators/reduxDecorators';
import { Route } from 'react-router';
import { getUserWithTokens } from '../../../test/mocks/auth';
import { boolean, select, text } from '@storybook/addon-knobs';
import { setRoom } from '../../../test/utils/actions';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { STORYBOOK_SOCKET_AUTHORIZED_ACTION, TOnMockClientActionCallback } from '../../../test/utils/serverSocket';
import { action } from '@storybook/addon-actions';
import { WebrtcServiceType } from '../../../ioc/common/ioc-constants';
import storyRootIndicator from '../../../../.storybook/decorators/storyRootIndicator';
import { createMemoryHistory } from 'history';
import withModule from '../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

const onActionCallback: TOnMockClientActionCallback = (reduxAction, serverSocket) => {
  action('sendSocketEvent')(reduxAction);
};

const ROOM_ID = 'room123';

export default {
  title: 'routes/Room',
  component: Room,
  decorators: [
    connectDecorator(),
    storyRootIndicator,
    withModule(ModuleRegistryKey.Webrtc),
    withModule(ModuleRegistryKey.Webphone),
    withModule(ModuleRegistryKey.RoomModule),
  ],
  parameters: {
    redux: {
      enable: true,
    },
    serverSocket: {
      onActionCallback,
    },
    IoCInitializationOptions: {
      historyFactory: () =>
        createMemoryHistory({
          initialEntries: [`/room/${ROOM_ID}`],
        }),
    },
  },
};

export const DefaultWithoutSocket = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  props.dispatch(clientEntityProvider.getAppActionCreators().loginSuccess(getUserWithTokens({ id: userId, roomId })));
  setRoom(props, roomId, userId);
  return <Route path="/room/:roomId" component={Room} />;
};

DefaultWithoutSocket.story = {
  parameters: {
    storyshots: false,
    serverSocket: false,
  },
};

export const Owner = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  const isOwner = boolean('isOwner', true);
  const ownerId = isOwner ? userId : 'other123';
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.changeWebrtcService(WebrtcServiceType.P2P));

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  setRoom(props, roomId, ownerId);
  return <Route path="/room/:roomId" component={Room} />;
};

export const OwnerWithHeader = (props) => Owner(props);

OwnerWithHeader.story = {
  parameters: {
    themeDecorator: {
      includeHeader: true,
    },
  },
};

export const Initializing = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  props.dispatch(actionCreators.webphoneUserSetupComplete());
  return <Route path="/room/:roomId" component={Room} />;
};

export const WaitForTheHostPage = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  const ownerId = 'other123';
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  props.dispatch(actionCreators.webphoneUserSetupComplete());
  setRoom(props, roomId, ownerId);
  return <Route path="/room/:roomId" component={Room} />;
};

export const WithError = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  setRoom(props, roomId, userId);
  const RoomErrors = clientEntityProvider.getConstants().RoomJoinError;
  const errorCode = select('errorCode', RoomErrors, RoomErrors.ALREADY_JOINED);
  props.dispatch({ type: clientEntityProvider.getAppActionTypes().ROOM_CANNOT_JOIN, errorCode });

  return <Route path="/room/:roomId" component={Room} />;
};

export const SessionJustEnded = (props) => {
  const userId = text('userId', '123');
  const roomId = text('roomId', ROOM_ID);
  const actionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(actionCreators.changeWebrtcService(WebrtcServiceType.P2P));
  props.dispatch(actionCreators.loginSuccess(getUserWithTokens({ id: userId, roomId })));
  props.dispatch(actionCreators.sendAction(STORYBOOK_SOCKET_AUTHORIZED_ACTION));
  props.dispatch(actionCreators.webphoneUserSetupComplete());
  props.dispatch({ type: clientEntityProvider.getConstants().webrtcConstants.SERVICE_ACTIONS.SESSION_ENDED });

  return <Route path="/room/:roomId" component={Room} />;
};
