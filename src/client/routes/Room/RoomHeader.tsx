import React from 'react';
import { connect } from 'react-redux';
import isOwner from '../../../modules/app/redux/selectors/isOwner';
import { IStoreState, IRecordingButtonOwnProps } from '../../../ioc/client/types/interfaces';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import autoStartSession from '../../../modules/app/redux/selectors/autoStartSession';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { push, RouterAction } from 'connected-react-router';
import HeaderSplitter from '../../../modules/app/components/HeaderSplitter';
import { Location } from 'history';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';

interface IRoomHeaderStateProps {
  roomId: string;
  isRoomOwner: boolean;
  user: any;
  location: Location | null;
  sessionId: string;
  landingPageURL: string;
  isReconnecting: boolean;
  autoStartSession: boolean;
  disableRecordings: boolean;
}

interface IRoomHeaderDispatchProps {
  changeLocation?: (path: string) => RouterAction;
  sendAction?: ActionCreator<AnyAction>;
}

export class RoomHeader extends React.Component<IRoomHeaderDispatchProps & IRoomHeaderStateProps> {
  private _gotoDashboard() {
    this.props.changeLocation('/');
  }

  private gotoFinalPage() {
    this.props.changeLocation('/thanks/');
  }

  private _leaveRoom = () => {
    const { roomId, user } = this.props;
    const appActions = clientEntityProvider.getActionCreators();

    if (user.guest) {
      this.gotoFinalPage();
    } else if (roomId) {
      this.props.sendAction(appActions.leaveRoom(roomId, user.id));
    } else {
      this._gotoDashboard();
    }
  };

  public componentWillUnmount(): void {
    const { roomId, user } = this.props;
    const appActions = clientEntityProvider.getActionCreators();

    if (roomId) {
      this.props.sendAction(appActions.leaveRoom(roomId, user.id));
    }
  }

  public render() {
    const { isRoomOwner, sessionId, isReconnecting, roomId, disableRecordings } = this.props;
    const { UI, WebPhone } = clientEntityProvider.getComponentProvider();
    const RecordingButton = clientEntityProvider.resolveComponentSuspended<IRecordingButtonOwnProps>(
      ComponentRegistryKey.Recording_Button
    );

    return (
      <React.Fragment>
        {roomId && sessionId && !disableRecordings && (
          <RecordingButton isOwner={isRoomOwner} sessionId={sessionId} isReconnecting={isReconnecting} />
        )}
        <HeaderSplitter />
        <UI.Button icon="leave" title="LEAVE ROOM" bsStyle="secondary" onClick={this._leaveRoom} />
        {isRoomOwner && <WebPhone.ButtonSession />}
      </React.Fragment>
    );
  }
}

export default connect<any, IRoomHeaderDispatchProps>(
  (state: IStoreState) => {
    const {
      config,
      constants: { webrtcConstants },
    } = clientEntityProvider;
    const sessionId = state.room?.webrtcService?.sessionId;
    const isConnected = state.webphone?.connectionStatus === webrtcConstants.CONNECTION_STATUS.ESTABLISHED;
    const isReconnecting = !isConnected && state.room.recording?.isRecording;

    return {
      user: state.auth.user,
      roomId: state.room.state.id,
      isRoomOwner: isOwner(state),
      isReconnecting,
      landingPageURL: config.project.urls.home,
      autoStartSession: autoStartSession(state),
      sessionId: (isConnected || isReconnecting) && sessionId ? sessionId : null,
      disableRecordings: 'gui' in config.project ? config.project.gui.disableRecordings : false,
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        changeLocation: push,
        sendAction: appActions.sendAction,
      },
      dispatch
    );
  }
)(RoomHeader);
