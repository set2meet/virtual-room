import React from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../ioc/common/ioc-interfaces';
import { RouteComponentProps } from 'react-router';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ActionCreator, AnyAction, bindActionCreators } from 'redux';
import { autoStartSession, connectedToServer, getUserColors } from '../../../modules/app/redux/selectors';
import { get as _get } from 'lodash';
import { isOwner as isOwnerSelector } from '../../../modules/app/redux/selectors/index';
import PRESENT_TOOLS from '../../../modules/app/components/PRESENT_TOOLS';
import ReconnectingOverlay from '../../../modules/app/components/ReconnectingOverlay';
import styled, { withTheme } from 'styled-components';
import {
  styledAsidePanel,
  styledBasePanle,
  styledCenterPanel,
  styledControlsPanel,
  styledControlsPanelGroup,
  styledPanelContainer,
} from './Room.style';
import {
  IChatSoundNotificationsProps,
  IFullscreenButtonProps,
  IFullscreenContainerProps,
  IGraph2DOwnProps,
  IWhiteboardProps,
  TRoomUser,
} from '../../../ioc/client/types/interfaces';
import { TDynamicModuleWrapperProps } from '../../../ioc/client/providers/ModuleProvider/types/IS2MModule';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { FallbackRegistryKey } from '../../../ioc/client/providers/FallbackProvider/types/FallbackRegistryKey';
import Skeleton, { SkeletonProps } from 'react-loading-skeleton';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { StateMessage } from './StateMessage';

const SIDE_PANEL_LEFT_WIDTH = 60;
const SIDE_PANEL_RIGHT_WIDTH = 295;

type ActiveTool = IFullscreenContainerProps & {
  defaultButton: boolean;
  reactNode: React.ReactNode;
  buttonButtonColorMode: IFullscreenButtonProps['colorMode'];
};

interface IRoomStateProps {
  picture?: string;
  fullscreenStatus: boolean;
  locationRoomId: string;
  stateRoomId: string;
  user: TRoomUser;
  room: any;
  isJoinedToTheRoom: boolean;
  isUserSetupComplete: boolean;
  isRoomOwner: boolean;
  isLeftPanelVisible: boolean;
  isRightPanelVisible: boolean;
  currentTool: string;
  isSessionJustEnded: boolean;
  autoStartSession: boolean;
  numberOfUnreadMessages: number;
  connectedToServer: boolean;
  dontShowUserSetup: boolean;
  userColor: string;
}

interface IRoomDispatchProps {
  sendAction: ActionCreator<AnyAction>;
  roomToggleLeftPanelVisibility: ActionCreator<AnyAction>;
  roomToggleRightPanelVisibility: ActionCreator<AnyAction>;
}

export class RoomPage extends React.Component<IRoomStateProps & IRoomDispatchProps, { visible: boolean }> {
  private readonly PanelContainer = styled.div`
    ${styledPanelContainer};
  `;
  private readonly PanelCenter = styled.div`
    ${styledCenterPanel};
  `;
  private readonly PanelAside = withTheme(
    styled.div`
      ${styledAsidePanel};
    `
  );
  private readonly PanelBase = withTheme(
    styled.div`
      ${styledBasePanle};
    `
  );
  private readonly PanelControls = styled.div`
    ${styledControlsPanel};
  `;
  private readonly PanelControlsGroup = styled.div`
    ${styledControlsPanelGroup};
  `;

  private _joinRoomStatus: 'none' | 'connecting' = 'none';

  private get isUserSetupComplete(): boolean {
    if (localStorage.getItem('robot')) {
      return true;
    }

    return this.props.isRoomOwner ? true : this.props.isUserSetupComplete || this.props.dontShowUserSetup;
  }

  private _getStateMessage = () => {
    if (this.props.stateRoomId === null && _get(this.props.room, 'state.error.message')) {
      return _get(this.props.room, 'state.error.message');
    }

    if (this.props.isSessionJustEnded) {
      return StateMessage.ThankYou;
    }

    return !this.props.room.state.ownerId || this.props.room.state.ownerId === this.props.user.id
      ? StateMessage.EnteringRoom
      : StateMessage.WaitForHost;
  };

  private get _activeTool(): ActiveTool {
    const { CodeEditor, Notepad, WebPhone } = clientEntityProvider.getComponentProvider();
    const PresentationsModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps>(
      ModuleRegistryKey.Presentations,
      <Skeleton width="115vh" height="100%" />
    );
    const WhiteboardModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps & IWhiteboardProps>(
      ModuleRegistryKey.Whiteboard
    );
    const Graph2DModule = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps & IGraph2DOwnProps>(
      ModuleRegistryKey.Graph2D
    );
    const FullscreenButton = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.Fullscreen_Button);
    let mode: IFullscreenContainerProps['mode'] = 'stretch';
    let reactNode: React.ReactNode;
    let defaultButton: boolean = true;
    let buttonButtonColorMode: IFullscreenButtonProps['colorMode'] = 'default';

    switch (this.props.currentTool) {
      case PRESENT_TOOLS.whiteboard.name: {
        mode = 'contain';
        buttonButtonColorMode = 'inversed';
        reactNode = (
          <WhiteboardModule
            isModerator={!!this.props.isRoomOwner}
            localStatePath="whiteboard"
            statePath="room.whiteboard"
            userId={this.props.user.id}
            roomId={this.props.stateRoomId}
            userColor={this.props.userColor}
          />
        );
        break;
      }
      case PRESENT_TOOLS.codeEditor.name: {
        reactNode = <CodeEditor roomId={this.props.stateRoomId} />;
        break;
      }
      case PRESENT_TOOLS.notepad.name: {
        reactNode = <Notepad roomId={this.props.stateRoomId} />;
        break;
      }
      case PRESENT_TOOLS.presentations.name: {
        mode = 'contain';
        buttonButtonColorMode = 'inversed';
        reactNode = <PresentationsModule />;
        break;
      }
      case PRESENT_TOOLS.graph2d.name: {
        reactNode = <Graph2DModule roomId={this.props.stateRoomId} isOwner={this.props.isRoomOwner} />;
        break;
      }
      default: {
        defaultButton = false;
        reactNode = (
          <WebPhone.BaseScreen
            roomId={this.props.stateRoomId}
            autoStartSession={this.props.autoStartSession}
            fullscreenStatus={this.props.fullscreenStatus}
            fullscreenButton={FullscreenButton}
          />
        );
      }
    }

    return { defaultButton, reactNode, mode, buttonButtonColorMode };
  }

  private joinRoom() {
    const appActions = clientEntityProvider.getActionCreators();
    const sessionId = clientEntityProvider.getBrowser().info.sessionId;

    this._joinRoomStatus = 'connecting';
    this.props.sendAction(appActions.joinRoom(this.props.locationRoomId, this.props.user.id, sessionId));
  }

  private getRoomInfo() {
    const appActions = clientEntityProvider.getActionCreators();

    this.props.sendAction(appActions.getRoomInfo(this.props.locationRoomId, ['webrtcService.serviceName']));
  }

  public componentDidMount() {
    if (this.isUserSetupComplete && this.props.connectedToServer) {
      this.joinRoom();
    } else if (!this.isUserSetupComplete) {
      this.getRoomInfo();
    }

    window.addEventListener('beforeunload', () => {
      localStorage.removeItem('robot');
    });
  }

  public shouldComponentUpdate(nextProps: IRoomStateProps): boolean {
    if (this.props.stateRoomId && !nextProps.stateRoomId) {
      // user leave room, and no render needed
      return false;
    }

    return true;
  }

  public componentDidUpdate(prevProps: IRoomStateProps) {
    if (this.isUserSetupComplete && this.props.connectedToServer && this._joinRoomStatus === 'none') {
      this.joinRoom();
    }
    if (prevProps.connectedToServer && !this.props.connectedToServer) {
      // disconnected, for example
      this._joinRoomStatus = 'none';
    }
  }

  private _renderRoomJoinError(errorCode: number): React.ReactNode {
    const { RoomJoinError } = clientEntityProvider.constants;
    const { UI, RoomIsFull } = clientEntityProvider.getComponentProvider();

    if (errorCode === RoomJoinError.IS_FULL) {
      return <RoomIsFull />;
    }

    const errorText: Record<number, StateMessage> = {
      [RoomJoinError.WRONG_ID]: StateMessage.WrongId,
      [RoomJoinError.ALREADY_JOINED]: StateMessage.AlreadyJoined,
    };

    return <UI.WaitingPage text={errorText[errorCode]} />;
  }

  public render() {
    const errorCode = this.props.room.state.error.code;
    const { UI, WebPhone } = clientEntityProvider.getComponentProvider();
    const NotificationModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Notification, null);
    const { PanelContainer, PanelAside, PanelCenter, PanelBase, PanelControls, PanelControlsGroup } = this;

    const Notifications = <NotificationModule />;

    if (errorCode) {
      return this._renderRoomJoinError(errorCode);
    }

    if (this.isUserSetupComplete && this.props.isJoinedToTheRoom) {
      const { fullscreenStatus } = this.props;
      const activeTool = this._activeTool;
      const DarkSkeleton = clientEntityProvider.resolveFallback<SkeletonProps>(FallbackRegistryKey.DarkSkeleton);
      const FullscreenModule = clientEntityProvider.resolveModuleSuspended<IFullscreenContainerProps>(
        ModuleRegistryKey.Fullscreen
      );
      const FullscreenButton = clientEntityProvider.resolveComponentSuspended<IFullscreenButtonProps>(
        ComponentRegistryKey.Fullscreen_Button
      );
      const ChatModuleFallback = <DarkSkeleton width={'283px'} />;
      const ChatModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Chat, {
        actionCreators: clientEntityProvider.getAppActionCreators(),
      });
      const SoundNotifications = clientEntityProvider.resolveComponentSuspended<IChatSoundNotificationsProps>(
        ComponentRegistryKey.Chat_SoundNotifications
      );
      const SidebarPanel = clientEntityProvider.resolveComponentSuspended(
        ComponentRegistryKey.SidebarPanel,
        ChatModuleFallback
      );
      const CollaborationPanel = clientEntityProvider.resolveComponentSuspended(
        ComponentRegistryKey.CollaborationPanel
      );
      const { RoundButton } = clientEntityProvider.getComponentProvider().UI;

      return (
        <React.Fragment>
          <PanelContainer>
            <PanelAside>
              <UI.SidePanel
                position="left"
                width={SIDE_PANEL_LEFT_WIDTH}
                open={this.props.isLeftPanelVisible}
                onToggle={this.props.roomToggleLeftPanelVisibility}
              >
                <CollaborationPanel />
              </UI.SidePanel>
            </PanelAside>
            <PanelCenter>
              <PanelBase>
                <FullscreenModule mode={activeTool.mode}>
                  {activeTool.reactNode}
                  {activeTool.defaultButton && <FullscreenButton colorMode={activeTool.buttonButtonColorMode} />}
                  {fullscreenStatus && Notifications}
                </FullscreenModule>
              </PanelBase>
              <PanelControls>
                <PanelControlsGroup>
                  <WebPhone.ButtonMicrophone />
                  <WebPhone.ButtonCamera />
                  <WebPhone.ButtonScreenSharing container={RoundButton} />
                </PanelControlsGroup>
              </PanelControls>
              {!fullscreenStatus && Notifications}
            </PanelCenter>
            <PanelAside>
              <ChatModule fallback={ChatModuleFallback}>
                <UI.SidePanel
                  position="right"
                  width={SIDE_PANEL_RIGHT_WIDTH}
                  open={this.props.isRightPanelVisible}
                  onToggle={this.props.roomToggleRightPanelVisibility}
                  indicatorValue={this.props.numberOfUnreadMessages}
                >
                  <SidebarPanel />
                </UI.SidePanel>
                {this.props.user && (
                  <SoundNotifications userId={this.props.user.id} statePath="room.chat" fallback={null} />
                )}
              </ChatModule>
            </PanelAside>
          </PanelContainer>
          <ReconnectingOverlay />
        </React.Fragment>
      );
    }

    if (!this.isUserSetupComplete) {
      return (
        <WebPhone.UserSetup selfContained={false} userId={this.props.user?.id} roomId={this.props.locationRoomId} />
      );
    }

    return <UI.WaitingPage text={this._getStateMessage()} />;
  }
}

export default connect<IRoomStateProps, IRoomDispatchProps>(
  (state: IStoreState, ownProps: RouteComponentProps<{ roomId: string }>) => {
    const { chatSelectors } = clientEntityProvider.getSelectors();
    const { user } = state.auth;
    const isOwner = isOwnerSelector(state);
    const stateRoomId = state.room.state.id;
    const { isLeftPanelVisible, isRightPanelVisible, isSessionJustEnded } = state.app.view.room;
    const userColors = getUserColors(state);

    return {
      isReady: !!stateRoomId,
      stateRoomId,
      user,
      userColor: userColors[user?.id] || userColors.default,
      fullscreenStatus: !!state.fullscreen?.status,
      locationRoomId: ownProps.match.params.roomId,
      room: state.room,
      isLeftPanelVisible,
      isRightPanelVisible,
      isSessionJustEnded,
      isJoinedToTheRoom: _get(state, 'room.state.id') && (isOwner || _get(state, 'room.webrtcService.sessionId')),
      isUserSetupComplete: _get(state, 'webphone.userSetup.isComplete'),
      dontShowUserSetup: _get(state, 'webphone.userSetup.dontShowAgain'),
      isRoomOwner: isOwner,
      currentTool: state.room.state.currentTool,
      autoStartSession: autoStartSession(state),
      numberOfUnreadMessages: chatSelectors.numberOfUnreadMessages(state.room.chat),
      connectedToServer: connectedToServer(state),
    };
  },
  (dispatch) => {
    const appActions = clientEntityProvider.getActionCreators();

    return bindActionCreators(
      {
        sendAction: appActions.sendAction,
        roomToggleLeftPanelVisibility: appActions.roomToggleLeftPanelVisibility,
        roomToggleRightPanelVisibility: appActions.roomToggleRightPanelVisibility,
      },
      dispatch
    );
  }
)(RoomPage);
