import React, { ComponentType } from 'react';
import { text } from '@storybook/addon-knobs';
import video from '../../../test/assets/record_sample.mp4';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import Skeleton from 'react-loading-skeleton';
import { withIoCFactory } from '../../../test/utils/utils';

// make sure that props interface are exported separately from the component code to don't break the lazy-loading idea
const videoFactory = () =>
  clientEntityProvider.resolveComponentSuspended(
    ComponentRegistryKey.VideoPage,
    // tslint:disable-next-line:no-magic-numbers
    <Skeleton width={100} height={34} />
  );

// In order to workaround import order issues each ioc factory call should be wrapped inside render function
const SuspendedVideo: ComponentType<any> = withIoCFactory(videoFactory);

export default {
  title: 'routes/Video',
  component: SuspendedVideo,
  parameters: {
    redux: {
      enable: true,
    },
  },
};

export const WithVideo = () => <SuspendedVideo url={text('url', video)} />;
