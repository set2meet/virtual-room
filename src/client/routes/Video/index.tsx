import React from 'react';
import styled, { css } from 'styled-components';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';

const Page = styled.div`
  ${css`
    height: 100%;
  `}
`;

export default (props: any) => {
  const VideoPlayer = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.Recording_VideoPlayer);
  const NotificationModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Notification, null);

  return (
    <Page>
      <VideoPlayer {...props} />
      <NotificationModule />
    </Page>
  );
};
