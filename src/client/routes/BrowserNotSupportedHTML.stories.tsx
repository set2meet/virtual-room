import React from 'react';
import html from '../assets/not-supported-browser.html';

export default {
  title: 'routes/BrowserNotSupportedHTMLPage',
  parameters: {
    docs: { disable: true },
    storyshots: false,
    themeDecorator: {
      disable: true,
    },
  },
};

export const Default = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: html,
    }}
  />
);
