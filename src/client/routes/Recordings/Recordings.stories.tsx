import React from 'react';
import { connectDecorator } from '../../../../.storybook/decorators/reduxDecorators';
import { TRecordItem } from '@s2m/recording-db/lib/types';
import { select, text } from '@storybook/addon-knobs';
import ModalDialog from '../../../modules/app/components/ModalDialog';
import { getUserWithTokens } from '../../../test/mocks/auth';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import Recordings from './index';
import { buildRecordItem } from '../../../test/mocks/recording';
import withStoryRootIndicator from '../../../../.storybook/decorators/storyRootIndicator';
import withModule from '../../../../.storybook/decorators/moduleDecorator';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { createMemoryHistory } from 'history';
import { action } from '@storybook/addon-actions';
import { History, Location } from 'history';
import { match } from 'react-router';

export default {
  title: 'routes/Recordings',
  component: Recordings,
  decorators: [connectDecorator(), withStoryRootIndicator, withModule(ModuleRegistryKey.Recording)],
  parameters: {
    redux: {
      enable: true,
    },
    IoCInitializationOptions: {
      historyFactory: () =>
        createMemoryHistory({
          initialEntries: [`/recordings/`],
        }),
    },
  },
};

const userWithTokens = getUserWithTokens();

const recordStatuses = ['processing', 'available', 'recording', 'error', 'removing', 'failed', 'random'];

const setRecordsAction = (records: TRecordItem[]) => {
  const appActionTypes = clientEntityProvider.getAppActionTypes();

  return {
    type: appActionTypes.ROOM_RECORDING_SET_RECORDS,
    meta: { target: { users: [userWithTokens.user.id] } },
    records,
  };
};

const matchMock = ({
  params: {
    userId: '',
  },
} as unknown) as match;
const history = ({
  replace: action('replace'),
  push: action('push'),
} as unknown) as History;

export const Fetching = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  return <Recordings match={matchMock} location={({} as unknown) as Location} history={history} />;
};

export const WithRecords = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  const records: TRecordItem[] = [
    ...recordStatuses.map((status, index) =>
      buildRecordItem({
        id: `rec${index}`,
        src: `src${index}`,
        name: text('name', `Record ${index}`, `Record ${index}`),
        status: select('status', recordStatuses, status, `Record ${index}`),
      })
    ),
    buildRecordItem({ id: `rec7`, src: '', name: 'Without link', status: 'processing' }),
    buildRecordItem({
      id: `rec8`,
      src: '',
      name: 'With remove button',
      createdBy: userWithTokens.user.id,
      meta: {
        isOwner: true,
        isViewer: true,
      },
      status: 'available',
    }),
    buildRecordItem({
      id: `rec9`,
      src: 'src9',
      name: 'With size',
      status: 'available',
      size: '123',
      resolution: '123',
    }),
  ];
  props.dispatch(setRecordsAction(records));
  return (
    <>
      <Recordings match={matchMock} location={({} as unknown) as Location} history={history} />;
      <ModalDialog />
    </>
  );
};

export const Empty = (props) => {
  const appActionCreators = clientEntityProvider.getAppActionCreators();

  props.dispatch(appActionCreators.loginSuccess(userWithTokens));
  props.dispatch(setRecordsAction([]));
  return <Recordings match={matchMock} location={({} as unknown) as Location} history={history} />;
};
