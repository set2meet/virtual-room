import React from 'react';
import styled from 'styled-components';
import clientEntityProvider from '../../../ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { ModuleRegistryKey } from '../../../ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { ComponentRegistryKey } from '../../../ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import { RouteComponentProps } from 'react-router';

const RecordingsPage = styled.div`
  position: relative;
  height: 100%;
  overflow: hidden;
`;

const RecordingsRoute: React.FunctionComponent<RouteComponentProps> = (props) => {
  const config = clientEntityProvider.getConfig();
  const disableRecordings = 'gui' in config.project ? config.project.gui.disableRecordings : false;
  const RecordingModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Recording);
  const Recordings = clientEntityProvider.resolveComponentSuspended<RouteComponentProps>(
    ComponentRegistryKey.Recording_Recordings
  );
  const NotificationModule = clientEntityProvider.resolveModuleSuspended(ModuleRegistryKey.Notification, null);

  if (disableRecordings) {
    window.location.href = config.project.urls.home;
    return <></>;
  } else {
    return (
      <React.Fragment>
        {!disableRecordings && (
          <RecordingsPage>
            <RecordingModule>
              <Recordings {...props} />
            </RecordingModule>
            <NotificationModule />
          </RecordingsPage>
        )}
      </React.Fragment>
    );
  }
};

export default RecordingsRoute;
