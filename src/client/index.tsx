import React from 'react';
import ReactDOM from 'react-dom';
import AppLoader from '../modules/app/components/AppLoader/AppLoader';
import { AppContainer } from 'react-hot-loader';

function onContentLoad() {
  const appContainerNode = document.getElementById('s2mVirtualRoomContainer');

  ReactDOM.render(
    <AppContainer>
      <AppLoader />
    </AppContainer>,
    appContainerNode
  );
}

document.addEventListener('DOMContentLoaded', onContentLoad);
