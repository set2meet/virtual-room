/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/**
 * common  module interfaces
 * WARN: you should avoid using it, just declare appropriate typings inside server/client interface packages
 * @deprecated
 */
import { vrCommon } from '../../modules/common';
import { ITheme } from '../../modules/app/entities/themes/types/ITheme';
import ICommonIcons = vrCommon.client.UI.ICommonIcons;

export { IClientConfig, IAppConfigService } from '../client/types/IClientConfig';
export { IStoreState } from '../../modules/app/entities/store/types/IStoreState';
export { TStore } from '../../modules/app/entities/store/types/TStore';
export { TMiddlewareAPI } from '../../modules/app/entities/store/types/TMiddlewareAPI';

export { IMediaReduxState, UserMediaErrorName, IScreenSharingSupportInfo } from '../../modules/media/types/mediaTypes';
import { IRecordingActionCreators } from '../../modules/recording/src/common/redux/actionCreators/IRecordingActionCreators';
import { IRecordingActionTypes } from '../../modules/recording/src/common/redux/RecordingAction';
import { IMediaActionCreators } from '../../modules/media/types/mediaTypes';

import { IAuthActionCreators } from '../../modules/auth/client/redux/types/IAuthActionCreators';
import { IConnectionActionCreators } from '../../modules/connection/redux/types/IConnectionActionCreators';
import { IAPIActionCreators } from '../../modules/app/redux/api/actionCreators/IAPIActionCreators';
import { IRoomActionCreators } from '../../modules/room/common/redux/actionCreators/IRoomActionCreators';
import { INotificationActionCreators } from '../../modules/notification/redux/types/INotificationActionCreators';

import { IChatActionCreators } from '../../modules/chat/common/redux/types/IChatActionCreators';
import { ChatAction } from '../../modules/chat/common/redux/types/ChatAction';

import { IPresentationsActionCreators } from '../../modules/presentations/common/redux/types/IPresentationsActionCreators';

import { IConnectionActionTypes } from '../../modules/connection/redux/types/ConnectionAction';
import { IRoomActionTypes } from '../../modules/room/common/redux/types/RoomAction';
import { TWebphoneActionTypes } from '../../modules/webphone/common/redux/types/WebphoneAction';
import { IAuthActionTypes } from '../../modules/auth/common/redux/AuthAction';
import { IBrowserInfo } from '../../modules/app/entities/browser/types/browserTypes';
import { INotificationActionTypes } from '../../modules/notification/redux/types/NotificationAction';

export { IModuleStorageKey } from '../client/providers/StorageManager/types';
export { TAuthTokens, TRoomUser } from '../client/providers/AuthProvider/types/common/TAuthTokens';

// whiteboard common types
import { IWhiteboardRoomReduxState } from '../../modules/whiteboard/common/redux/types/IWhiteboardRoomReduxState';
import { APIAction } from './ioc-constants';

import { WhiteboardAction } from '../../modules/whiteboard/common/redux/types/constants/WhiteboardAction';
import { IWhiteboardActionCreators } from '../../modules/whiteboard/common/redux/types/IWhiteboardActionCreators';

import { MediaAction } from '../../modules/media/redux/types/MediaAction';

import { TAppActionTypes } from '../../modules/app/redux/types/AppAction';
import { IAppModuleActionCreators } from '../../modules/app/redux/actionCreators/IAppActionCreators';

export type IAppActionTypes = IConnectionActionTypes &
  TAppActionTypes &
  IRoomActionTypes &
  TWebphoneActionTypes &
  IAuthActionTypes &
  typeof ChatAction &
  IRecordingActionTypes &
  typeof WhiteboardAction &
  INotificationActionTypes &
  typeof MediaAction &
  typeof APIAction;

export { IAuthActionTypes, IRecordingActionTypes, IRoomActionTypes };

import { TWebphoneActionCreators } from '../../modules/webphone/common/redux/actionCreators';

export type IAppActionCreators = IAuthActionCreators &
  IAppModuleActionCreators &
  IConnectionActionCreators &
  IAPIActionCreators &
  IRoomActionCreators &
  INotificationActionCreators &
  IChatActionCreators &
  IRecordingActionCreators &
  IMediaActionCreators &
  TWebphoneActionCreators &
  IWhiteboardActionCreators &
  IPresentationsActionCreators;

import TActionMetadata = vrCommon.client.ActionMetadata;
export { TActionMetadata };

export {
  IAppReduxEnumMiddleware,
  IModuleReduxMiddleware,
  IServerMiddleware,
  IServerMiddlewareContext,
  IServerMiddlewareNext,
  IServerMetaObjects,
  IServerMiddlewareOptionalContext,
} from '../../modules/common';

export { ITheme, ICommonIcons, IBrowserInfo };

// presentations common types
export { ILocalPresentationsReduxState } from '../../modules/presentations/client/redux/types/localReduxStateTypes';
export { IPresentationsReduxState } from '../../modules/presentations/common/redux/types/IPresentationsReduxState';

// graph2d
import { IGraph2D } from '../../modules/graph2d/common/types';

export type IGraph2DRoomReduxState = IGraph2D.ReduxStateRoom;

export { IWhiteboardRoomReduxState };

// webrtc common types
import { IWebRTCServiceReduxState } from '../../modules/webrtc/common/redux/types';
import { IAppConfigProject } from '../../modules/app/types/app';
import { IApiRoomParticipant, IApiRoomSettings } from '../../modules/room/common/types/interfaces';

export { IApiRoomParticipant, IApiRoomSettings };
export { IAppConfigProject };
export {
  IWebrtcTestCallConfig,
  IWebrtcService,
  IWebrtcServiceAccessParams,
  TWebRTCServices,
} from '../../modules/webrtc/common/types/IWebrtcService';

export { IWebRTCServiceReduxState };

// chat
export { IChatReduxState } from '../../modules/chat/common/redux/types/IChatReduxState';

// recording
export { IRecordingReduxState } from '../../modules/recording/src/common/redux/reducer/IRecordingReduxState';

// IStoreState
export { IRecordingLocalReduxState } from '../../modules/recording/src/client/redux/reducer/IRecordingLocalReduxState';
export { IAuthReduxState } from '../../modules/auth/client/redux/types/IAuthReduxState';
export { IGlobalRoomReduxState } from '../../modules/room/common/redux/types/IGlobalRoomReduxState';
export { IRoomReduxState } from '../../modules/room/common/redux/types/IRoomReduxState';
export { IWebPhoneReduxState } from '../../modules/webphone/client/redux/types/IWebPhoneReduxState';
export { IConnectionReduxState } from '../../modules/connection/redux/types/IConnectionReduxState';
export { IFullscreenReduxState } from '../../modules/fullscreen/redux/types/IFullscreenReduxState';

// webphone types
export { TWebPhoneParticipantsInfo as TWebPhoneRoomReduxState } from '../../modules/webphone/common/redux/types/TWebPhoneParticipantsInfo';
