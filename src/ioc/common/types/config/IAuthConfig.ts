/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

/**
 * IAuthConfig is partial of IAppConfig
 * @link src/client/types/IAppConfig
 */
export interface IAuthConfig {
  strategy: string;
  realmlist: any;
  localUrl: string;
  realm: string;
  allowFakeUsers?: boolean;
  allowChangeAuthMethods?: boolean;
}
