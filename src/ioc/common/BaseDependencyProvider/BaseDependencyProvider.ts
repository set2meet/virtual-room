/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IDependencyContainer } from '../types/IDependencyContainer';
import { Container } from 'inversify';

export abstract class BaseDependencyProvider<RegistryKey> {
  private container: IDependencyContainer;

  constructor() {
    this.initializeContainer();
  }

  protected initializeContainer() {
    this.container = new Container() as IDependencyContainer;
  }

  protected getDependency<DependencyType>(registryKey: RegistryKey) {
    return this.container.get<DependencyType>(registryKey.toString());
  }

  protected setDependency<DependencyType = any>(registryKey: RegistryKey, value: DependencyType) {
    this.container.bind<DependencyType>(registryKey.toString()).toConstantValue(value);
  }

  public destroy() {
    this.container.unbindAll();
  }
}
