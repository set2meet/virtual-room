/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
// client only interfaces should be declared here
import { vrCommon } from '../../../modules/common';
import { UserMediaLocalStorage } from 'src/modules/media/types/mediaTypes';
import {
  IWebrtcService,
  IWebrtcTestCallConfig,
  IAppActionCreators,
  IAppActionTypes,
} from '../../common/ioc-interfaces';
import { IBrowser, RecommendedBrowser } from '../../../modules/app/entities/browser/types/browserTypes';

import { IRequiredTheme, ITheme } from '../../../modules/app/entities/themes/types/ITheme';
import { IFullscreenButtonProps } from '../../../modules/fullscreen/components/types/IFullscreenButtonProps';
import { IFullscreenContainerProps } from '../../../modules/fullscreen/components/types/IFullscreenContainerProps';
import ISimpleToggleButtonProps = vrCommon.client.UI.ISimpleToggleButton;
import { UMSEvent } from '../../../modules/media/services/types/IUMSEvent';

export { TAuthTokens, TRoomUser } from '../providers/AuthProvider/types/client/AuthProvider.interfaces';

export { IModuleStorageKey } from '../providers/StorageManager/types';

export { IAppRecordingPreviewConfig } from './IClientConfig';

export { IAppConfigService, IAppRecordingConfig } from './IClientConfig';

export { IAppStateProps } from '../../../client/routes/App/App';

export { IScreenSharingSupportInfo } from '../../../modules/media/types/mediaTypes';

export { IAppModalDialogDefinition } from '../../../modules/app/types/IAppModalDialog';

export { TRouterModuleInitOptions } from '../../../modules/router/TRouterModuleInitOptions';
export { TAppModuleInitOptions } from '../../../modules/app/types/TAppModuleInitOptions';

export { IIndicatorProps, IStyledProps } from '../../../modules/app/components/Indicator/Indicator.types';

export { CommonIconsList, IVRModule } from '../../../modules/common/index';

export { IChatMessageItem } from '../../../modules/chat/common/types/IChatMessageItem';

import IButtonProps = vrCommon.client.UI.IButtonProps;

export type { IButtonProps };

// Storage Keys
import { IChatStorageKeys } from '../../../modules/chat/client/types/IChatStorageKeys';
import IMediaStorageKeys = UserMediaLocalStorage.IMediaStorageKeys;
import { IAuthStorageKeys } from '../../../modules/auth/client/storage/IAuthStorageKeys';
import { IBrowserStorageKeys } from '../../../modules/app/entities/browser/types/browserTypes';
import { IWebphoneStorageKeys } from '../../../modules/webphone/client/services/IWebphoneStorageKeys';

export type IAppStorageKeys = IChatStorageKeys &
  IAuthStorageKeys &
  IBrowserStorageKeys &
  IWebphoneStorageKeys &
  IMediaStorageKeys;

export {
  IAppActionCreators,
  IAppActionTypes,
  ITheme,
  IRequiredTheme,
  IWebrtcService,
  IWebrtcTestCallConfig,
  IBrowser,
  RecommendedBrowser,
  IFullscreenContainerProps,
  ISimpleToggleButtonProps,
  IFullscreenButtonProps,
  UMSEvent,
};

export { IComponents } from '../providers/ComponentProvider/types/IComponents';

export { TEnhancerFactory, TStoreFactory } from '../../../modules/app/entities/store/types/TStoreFactory';
export { IStoreState } from '../../../modules/app/entities/store/types/IStoreState';
export {
  TClientSocketFactory,
  TInitializationOptions,
} from '../providers/ClientEntityProvider/types/TInitializationOptions';
export { ICompositionRootProps } from '../components/CompositionRoot/ICompositionRootProps';
export { TStore } from '../../../modules/app/entities/store/types/TStore';
export { THistoryFactory } from '../../../modules/app/entities/history/types/THistoryFactory';
export { THistory, THistoryLocationState } from '../../../modules/app/entities/history/types/THistory';

// chat
import { IChatPanelProps } from '../../../modules/chat/client/components/Panel/types/IChatPanelProps';
import { IChatSoundNotificationsProps } from '../../../modules/chat/client/components/SoundNotifications/IChatSoundNotificationsProps';

export { IChatPanelProps, IChatSoundNotificationsProps };

// recording
export { IRecordingReduxState } from '../../../modules/recording/src/common/redux/reducer/IRecordingReduxState';
export { IRecordingButtonOwnProps } from '../../../modules/recording/src/client/components/RecordingButton/IRecordingButtonProps';

// graph2d
import { IGraph2D } from '../../../modules/graph2d/common/types';
export type IGraph2DOwnProps = IGraph2D.IModuleProps;

// whiteboard
import { IAppConfigProject } from '../../../modules/app/types/app';
export { IAppConfigProject };
export { IWhiteboardProps } from '../../../modules/whiteboard/client/components/Whiteboard/IWhiteboardProps';

// webrtc
export { TWebRTCServicesFactory } from '../../../modules/webrtc/client/services/TWebRTCServicesFactory';
export { TWebRTCServices } from '../../../modules/webrtc/common/types/IWebrtcService';

// webphone
export { TWebphoneModuleInitOptions } from '../../../modules/webphone/client/types/TWebphoneModuleInitOptions';
export { IThemes } from '../../../modules/app/entities/themes/types/ITheme';

export { IClientLogger, TLoggerClientModule } from '../../../modules/logger/client/types/types';
