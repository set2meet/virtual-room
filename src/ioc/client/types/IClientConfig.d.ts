/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAppConfigProject } from '../../../modules/app/types/app';
import { IAuthConfig } from '../../common/types/config/IAuthConfig';
import { IAppPresentationsServiceConfig } from '../../common/types/config/IAppPresentationServiceConfig';
import { TFirebaseConfig, IWebrtcServicesConfig } from 'env-config';

export interface IAppConfigService {
  url: string;
  path: string;
}

export interface IAppRecordingConfig {
  enabled: boolean;
  service?: IAppConfigService;
}

export interface IAppRecordingPreviewConfig {
  service?: IAppConfigService;
}

export interface IAppPresentationsConfig {
  service: IAppPresentationsServiceConfig;
}

export interface IAppFeedbackServiceConfig {
  route: string;
  host: string;
  port: number;
  secure: boolean;
  auth: {
    user: string;
    pass: string;
  };
  params: {
    from: string;
    to: string;
    subject: string;
  };
}

export interface IClientConfig {
  auth: IAuthConfig;
  service: IAppConfigService;
  project: IAppConfigProject;
  clientConfig: {
    autoStartOnDemandSession?: boolean;
    enableMicOnSessionStart?: boolean;
  };
  logger: {
    routes: {
      getLogs: string;
    };
    saveReduxActions?: boolean;
  };
  notepad: TFirebaseConfig;
  codeEditor: TFirebaseConfig;
  chromeExtensionId?: string; // TODO
  recording: IAppRecordingConfig;
  recordingPreview: IAppRecordingPreviewConfig;
  presentations: IAppPresentationsConfig;
  feedback: any;
  webrtc: IWebrtcServicesConfig;
  chat: {
    maxUploadFileSize: number;
    maxMessageLength: number;
  };
}
