/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import StorageManager from './StorageManager';
import { defaultStorageFactory } from './storages/storageFactory';
import { mock } from 'jest-mock-extended';
import { TStore } from '../../types/interfaces';
import MemoryStorage from './storages/MemoryStorage/MemoryStorage';
import { Storages } from './storages/Storages';

describe('StorageManager', () => {
  const store = mock<TStore>();

  const storageKeys = {
    LOCAL_STORAGE_KEY: {
      key: `key1`,
      storage: Storages.LOCAL_STORAGE,
    },
    SESSION_STORAGE_KEY: {
      key: `key2`,
      storage: Storages.SESSION_STORAGE,
    },
    WITH_POSTFIX: {
      key: `key3`,
      postfix: () => '123',
      storage: Storages.MEMORY_STORAGE,
    },
  };

  const testObject = {
    test: true,
  };

  const storageManager = new StorageManager(storageKeys, store, defaultStorageFactory);

  beforeEach(() => {
    storageManager.removeAll();
  });

  it('get/has', (done) => {
    expect(storageManager.get<boolean>(storageKeys.LOCAL_STORAGE_KEY)).toBeNull();

    expect(storageManager.has(storageKeys.LOCAL_STORAGE_KEY)).toBeFalsy();

    expect(storageManager.get<boolean>(storageKeys.LOCAL_STORAGE_KEY, true)).toBeTruthy();
    expect(storageManager.has(storageKeys.LOCAL_STORAGE_KEY)).toBeFalsy();

    done();
  });

  it('get/set', (done) => {
    storageManager.set<typeof testObject>(storageKeys.LOCAL_STORAGE_KEY, testObject);
    expect(storageManager.has(storageKeys.LOCAL_STORAGE_KEY)).toBeTruthy();
    expect(storageManager.get(storageKeys.LOCAL_STORAGE_KEY)).toEqual(testObject);

    const testObject2 = {
      test: false,
    };

    storageManager.set<typeof testObject>(storageKeys.LOCAL_STORAGE_KEY, testObject2);
    expect(storageManager.get(storageKeys.LOCAL_STORAGE_KEY)).toEqual(testObject2);

    done();
  });

  it('keys removed after each tests', (done) => {
    expect(storageManager.get(storageKeys.LOCAL_STORAGE_KEY)).toBeNull();
    done();
  });

  it('set/remove', (done) => {
    storageManager.set<typeof testObject>(storageKeys.LOCAL_STORAGE_KEY, testObject);
    expect(storageManager.has(storageKeys.LOCAL_STORAGE_KEY)).toBeTruthy();
    storageManager.remove(storageKeys.LOCAL_STORAGE_KEY);
    expect(storageManager.has(storageKeys.LOCAL_STORAGE_KEY)).toBeFalsy();
    done();
  });

  it('should restore dynamic keys', () => {
    const memoryStorage = MemoryStorage.createInstance();

    memoryStorage.setItem(`${StorageManager.PREFIX}:${storageKeys.WITH_POSTFIX.key}:123`, 'VALUE');
    const newStorageManager = new StorageManager(storageKeys, store, defaultStorageFactory);

    expect(newStorageManager.get<string>(storageKeys.WITH_POSTFIX, null, false)).toBe('VALUE');
  });

  it('should save to different storages', () => {
    storageManager.set<typeof testObject>(storageKeys.LOCAL_STORAGE_KEY, testObject);
    expect(localStorage.getItem(`${StorageManager.PREFIX}:${storageKeys.LOCAL_STORAGE_KEY.key}`)).toBe(
      JSON.stringify(testObject)
    );
    expect(sessionStorage.length).toBe(0);

    storageManager.set<typeof testObject>(storageKeys.SESSION_STORAGE_KEY, testObject);
    expect(sessionStorage.getItem(`${StorageManager.PREFIX}:${storageKeys.SESSION_STORAGE_KEY.key}`)).toBe(
      JSON.stringify(testObject)
    );
  });
});
