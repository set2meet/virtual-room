/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import MemoryStorage from './MemoryStorage';

describe('MemoryStorage', () => {
  const memoryStorage = MemoryStorage.createInstance();

  beforeEach(() => {
    memoryStorage.clear();
  });

  it('singleton', () => {
    const memoryStorage2 = MemoryStorage.createInstance();

    expect(memoryStorage).toBe(memoryStorage2);
  });

  it('methods', () => {
    memoryStorage.setItem('item1', 'value1');
    expect(memoryStorage.getItem('item1')).toBe('value1');

    memoryStorage.setItem('item1', 'value2');
    expect(memoryStorage.getItem('item1')).toBe('value2');
    expect(memoryStorage.length).toBe(1);
    expect(memoryStorage.key(0)).toBe('item1');

    memoryStorage.removeItem('item1');
    expect(memoryStorage.length).toBe(0);
    expect(memoryStorage.getItem('item1')).toBe(null);
    expect(memoryStorage.key(0)).toBe(null);

    memoryStorage.setItem('item1', 'value1');
    memoryStorage.setItem('item2', 'value2');
    expect(memoryStorage.length).toBe(2);
    expect(memoryStorage.key(0)).toBe('item1');
    expect(memoryStorage.key(1)).toBe('item2');
    memoryStorage.clear();
    expect(memoryStorage.length).toBe(0);
  });
});
