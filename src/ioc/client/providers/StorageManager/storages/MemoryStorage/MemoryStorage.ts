/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
export default class MemoryStorage implements Storage {
  protected storage: { [key: string]: any } = {};

  private static _instance: Storage;

  private constructor() {}

  public static createInstance() {
    return this._instance || (this._instance = new this());
  }

  public clear(): void {
    const keys = Object.keys(this.storage);

    keys.forEach((key) => {
      delete this.storage[key];
    });
  }
  public setItem(key, value) {
    this.storage[key] = value;
  }
  public getItem(key) {
    return key in this.storage ? this.storage[key] : null;
  }
  public removeItem(key) {
    delete this.storage[key];
  }
  get length() {
    return Object.keys(this.storage).length;
  }
  public key(i) {
    const keys = Object.keys(this.storage);

    return keys[i] || null;
  }
}
