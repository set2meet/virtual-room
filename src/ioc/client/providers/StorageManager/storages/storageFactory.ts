/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TStorageFactory } from './types';
import MemoryStorage from './MemoryStorage/MemoryStorage';
import { Storages } from './Storages';

export const defaultStorageFactory: TStorageFactory = (storage: Storages): Storage => {
  if (!storage) {
    return localStorage;
  }

  switch (storage) {
    case Storages.LOCAL_STORAGE:
      return localStorage;
    case Storages.SESSION_STORAGE:
      return sessionStorage;
    case Storages.MEMORY_STORAGE:
      return MemoryStorage.createInstance();
  }
};
