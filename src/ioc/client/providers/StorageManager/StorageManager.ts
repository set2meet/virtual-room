/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IModuleStorageKey } from './types';
import { Storages } from './storages/Storages';
import { TStore, IAppStorageKeys } from '../../types/interfaces';
import { TStorageFactory } from './storages/types';

class StorageKey<V> {
  constructor(key: string, storage: Storages, storageFactory: TStorageFactory) {
    this.storage = storageFactory(storage);
    this.key = key;
  }

  public key: string;
  public storage: Storage;

  public toString(): string {
    return this.key;
  }

  public setItem(item: V): void {
    if (typeof item === 'string') {
      this.storage.setItem(this.key, item);
      return;
    }
    this.storage.setItem(this.key, JSON.stringify(item));
  }

  public getItem(defaultValue: V = null, parse = true): V {
    const str = this.storage.getItem(this.key);

    if (str === null) {
      return defaultValue;
    }

    if (!parse) {
      return (str as unknown) as V;
    }

    try {
      return JSON.parse(str) as V;
    } catch (error) {
      return defaultValue;
    }
  }

  public removeItem(): void {
    this.storage.removeItem(this.key);
  }

  public exists(): boolean {
    return this.storage.getItem(this.key) !== null;
  }
}

export default class StorageManager {
  /**
   * Contains all keys from storages, supported by `moduleKeys`
   * @private
   */
  private readonly keys: Map<string, StorageKey<any>> = new Map();

  private readonly store: TStore;
  private readonly storageFactory: TStorageFactory;

  public static readonly PREFIX = '@s2m';

  public constructor(moduleKeys: Partial<IAppStorageKeys>, store: TStore, storageFactory: TStorageFactory) {
    this.storageFactory = storageFactory;

    Object.values(moduleKeys).map((value: IModuleStorageKey<any>) => {
      // restore keys from storages
      const valueKey = `${StorageManager.PREFIX}:${value.key}`;

      if (value.postfix) {
        // Find dynamic keys in storages
        const storage = this.storageFactory(value.storage);
        const regex = new RegExp('^' + valueKey + ':.*');

        for (let i = 0; i < storage.length; i++) {
          const key = storage.key(i);

          if (key.match(regex)) {
            this.keys.set(key, new StorageKey<any>(key, value.storage, this.storageFactory));
          }
        }
      } else {
        this.keys.set(valueKey, new StorageKey<any>(valueKey, value.storage, this.storageFactory));
      }
    });
    this.store = store;
  }

  private getKey(moduleKey: IModuleStorageKey<any>): string {
    if (moduleKey.postfix) {
      return `${StorageManager.PREFIX}:${moduleKey.key}:${moduleKey.postfix(this.store.getState())}`;
    }
    return `${StorageManager.PREFIX}:${moduleKey.key}`;
  }

  public set = <T>(storageKey: IModuleStorageKey<T>, item: T) => {
    if (!storageKey || !storageKey.key) {
      return;
    }
    const obj = this.getKeyObject<T>(storageKey, true);

    obj.setItem(item);
  };

  private getKeyObject<T>(moduleKey: IModuleStorageKey<T>, createIfNotExists = false): StorageKey<T> {
    const key = this.getKey(moduleKey);
    let currentInstance = this.keys.get(key);

    if (!currentInstance) {
      const newInstance = new StorageKey<T>(key, moduleKey.storage, this.storageFactory);

      if (createIfNotExists || newInstance.exists()) {
        currentInstance = newInstance;
        this.keys.set(key, currentInstance);
      }
    }

    return currentInstance;
  }

  public get = <T>(moduleKey: IModuleStorageKey<T>, defaultValue: T = null, parse = true): T => {
    const obj = this.getKeyObject<T>(moduleKey);

    if (!obj) {
      return defaultValue;
    }

    return obj.getItem(defaultValue, parse);
  };

  public has = (moduleKey: IModuleStorageKey<any>): boolean => {
    const obj = this.getKeyObject<any>(moduleKey);

    return !!obj;
  };

  public removeAll = () => {
    this.keys.forEach((value) => {
      value.removeItem();
    });
    this.keys.clear();
  };

  public remove = (moduleKey: IModuleStorageKey<any>) => {
    const obj: StorageKey<any> = this.getKeyObject(moduleKey);

    if (obj) {
      obj.removeItem();
      this.keys.delete(obj.key);
    }
  };
}
