/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import { AnyAction } from 'redux';
import { IUtilsProvider } from './IUtilsProvider';
import { WebrtcServiceType } from '../../../common/ioc-constants';
import { IWebrtcService } from '../../types/interfaces';
import { TWebRTCServices } from '../../../common/ioc-interfaces';
import { sleep } from '../../../../modules/app/utils/utils';

export class UtilsProvider extends BaseDependencyProvider<{}> implements IUtilsProvider {
  public isReduxDynamicModulesAction = (action: AnyAction) => {
    const reduxDynamicModulesActionTypes = [
      '@@Internal/ModuleManager/ModuleAdded',
      '@@Internal/ModuleManager/SeedReducers',
      '@@Internal/ModuleManager/ModuleRemoved',
    ];

    return reduxDynamicModulesActionTypes.includes(action.type);
  };

  public getWebrtcServiceByName(name: WebrtcServiceType, webrtcServices: TWebRTCServices): IWebrtcService {
    const webrtcServicesList: IWebrtcService[] = Object.keys(webrtcServices).map((key) => webrtcServices[key]);

    return webrtcServicesList.filter((service) => service.name === name)[0];
  }

  public sleep = (duration: number = 0) => {
    return sleep(duration);
  };
}
