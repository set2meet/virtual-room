/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../ClientEntityProvider/types/IClientEntityProvider';
import { WebrtcServiceType } from '../../../common/ioc-constants';

beforeAll(setupIoCClientForTest());

describe('UtilsProvider', () => {
  it('getWebrtcServiceByName', async (done) => {
    const webrtcServices = await (clientEntityProvider as IClientEntityProviderInternals).getWebRTCServices();
    const getWebrtcServiceByName = clientEntityProvider.getUtils().getWebrtcServiceByName;

    expect(getWebrtcServiceByName(null, webrtcServices)).toBeUndefined();
    expect(getWebrtcServiceByName(WebrtcServiceType.P2P, webrtcServices).name).toEqual(WebrtcServiceType.P2P);
    done();
  });
  it('isReduxDynamicModulesAction', () => {
    const isReduxDynamicModulesAction = clientEntityProvider.getUtils().isReduxDynamicModulesAction;

    expect(isReduxDynamicModulesAction({ type: '@@Internal/ModuleManager/ModuleAdded' })).toBeTruthy();
    expect(isReduxDynamicModulesAction({ type: '@@INIT' })).toBeFalsy();
  });
});
