import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import { ComponentRegistryKey } from '../ComponentProvider/types/ComponentRegistryKey';
import { ModuleRegistryKey } from '../ModuleProvider/types/ModuleRegistryKey';
import WhiteboardSkeleton from '../../../../modules/whiteboard/client/components/Whiteboard/WhiteboardSkeleton';
import React from 'react';
import { IFallbackProvider } from './types/IFallbackProvider';
import DarkSkeleton from './DarkSekeleton/DarkSkeleton';
import { FallbackRegistryKey } from './types/FallbackRegistryKey';
import { TFallbackProviderRegistryKey } from './types/TFallbackProviderRegistryKey';
import Graph2dSkeleton from '../../../../modules/graph2d/client/components/Graph2dSkeleton';
import ParticipantsSkeleton from '../../../../modules/webphone/client/components/Participants/ParticipantsSkeleton';

type TFallbackRegistry = {
  [key in TFallbackProviderRegistryKey]: React.ComponentType<any>;
};

export class FallbackProvider extends BaseDependencyProvider<TFallbackProviderRegistryKey>
  implements IFallbackProvider {
  protected readonly DEFAULT_SKELETON: JSX.Element = (<DarkSkeleton width={100} height={100} />);

  public resolveFallbackJSX(key: TFallbackProviderRegistryKey): JSX.Element {
    try {
      const Fallback = this.getDependency<React.ComponentType>(key);

      return <Fallback />;
    } catch (e) {
      return this.DEFAULT_SKELETON;
    }
  }

  public resolveFallback<FallbackProps>(key: TFallbackProviderRegistryKey): React.ComponentType<FallbackProps> {
    return this.getDependency<React.ComponentType<FallbackProps>>(key);
  }

  private setupFallbacksRegistry(): TFallbackRegistry {
    return {
      [ModuleRegistryKey.Graph2D]: Graph2dSkeleton,
      [ModuleRegistryKey.Whiteboard]: WhiteboardSkeleton,
      [FallbackRegistryKey.DarkSkeleton]: DarkSkeleton,
      [ComponentRegistryKey.WebPhone_Participants]: ParticipantsSkeleton,
    };
  }

  public bindFallbacks() {
    const registry = this.setupFallbacksRegistry();

    Object.keys(registry).forEach((registryKey) => {
      const fallback = registry[registryKey];

      this.setDependency((registryKey as unknown) as ComponentRegistryKey | ModuleRegistryKey, fallback);
    });
  }
}
