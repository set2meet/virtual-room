import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import React from 'react';
import { FallbackProvider } from './FallbackProvider';
import { IClientEntityProviderInternals } from '../ClientEntityProvider/types/IClientEntityProvider';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';

beforeEach(setupIoCClientForTest());

describe('FallbackProvider', () => {
  it('should bind and resolve fallback', async (done) => {
    const fallbackProvider = (clientEntityProvider as IClientEntityProviderInternals).getFallbackProvider() as FallbackProvider;
    const TestFallback = () => <></>;
    const KEY = 'KEY';

    const setupRegistry = jest.spyOn(fallbackProvider, 'setupFallbacksRegistry' as any).mockImplementation(() => ({
      KEY: TestFallback,
    }));

    fallbackProvider.bindFallbacks();
    expect(setupRegistry).toBeCalledTimes(1);
    expect(fallbackProvider.resolveFallbackJSX(KEY as any)).toEqual(<TestFallback />);
    expect(fallbackProvider.resolveFallback(KEY as any)).toBe(TestFallback);
    done();
  });
});
