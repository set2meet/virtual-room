import React from 'react';
import Skeleton, { SkeletonTheme, SkeletonProps } from 'react-loading-skeleton';
import styled, { css } from 'styled-components';
import { IRequiredTheme } from '../../../../../modules/app/entities/themes/types/ITheme';

const Container = styled.div<SkeletonProps>`
  ${({ height }: SkeletonProps & IRequiredTheme) => css`
    height: ${height};
    padding: 6px;
    > * {
      height: ${height};
    }
  `}
`;

const DarkSkeleton = ({ height = '100%', width = '100%', ...props }: SkeletonProps) => {
  if (typeof height === 'number') {
    height = `${height}px`;
  }
  return (
    <Container height={height}>
      <SkeletonTheme color={'#444'} highlightColor={'#333333'}>
        <Skeleton width={width} height={'100%'} {...props} />
      </SkeletonTheme>
    </Container>
  );
};

export default DarkSkeleton;
