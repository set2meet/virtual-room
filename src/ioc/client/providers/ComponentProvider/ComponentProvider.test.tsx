import { ComponentRegistryKey } from './types/ComponentRegistryKey';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import HeaderSkeleton from '../../../../modules/app/components/Header/HeaderSkeleton';
import React from 'react';
import { enzymeCleanup, InitModulesWrapper } from '../../../../test/utils/utils';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import ComponentProvider from './ComponentProvider';

beforeEach(setupIoCClientForTest());
afterEach(enzymeCleanup);

describe('ComponentProvider', () => {
  it('ComponentProvider should unbind components on destroy call', () => {
    const componentProvider = clientEntityProvider.getComponentProvider();
    const Button = componentProvider.resolveComponentSuspended(ComponentRegistryKey.Button);

    expect(Button).toBeDefined();

    componentProvider.destroy();

    expect(() => componentProvider.resolveComponentSuspended(ComponentRegistryKey.Button)).toThrowError(
      /No matching bindings found for serviceIdentifier:.*/
    );
    clientEntityProvider.destroy();
  });
  it('Suspended component shouldnt be re-rendered if props are updated in parent component', async (done) => {
    const componentProvider = clientEntityProvider.getComponentProvider();
    const Wrapper = (props) => {
      const Header = componentProvider.resolveComponentSuspended(ComponentRegistryKey.Header, <HeaderSkeleton />);
      return <Header />;
    };
    const mounted = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <Wrapper />
        </InitModulesWrapper>
      )
    );
    const wrapper = mounted.update().find(Wrapper);
    const header = mounted.find('Header').instance();

    if (!header.componentWillUnmount) {
      header.componentWillUnmount = jest.fn();
    }

    const componentWillUnmount = jest.spyOn(header, 'componentWillUnmount');

    wrapper.update().setProps({ a: '123' });

    expect(componentWillUnmount).not.toHaveBeenCalled();
    done();
  });
  it('should bind and resolve component', async (done) => {
    const componentProvider = clientEntityProvider.getComponentProvider() as ComponentProvider;
    const TestComponent = () => <></>;
    const KEY = 'KEY';

    const setupComponentRegistry = jest
      .spyOn(componentProvider, 'setupComponentRegistry' as any)
      .mockImplementation(() => ({
        KEY: () => new Promise((resolve) => resolve({ default: TestComponent })),
      }));

    componentProvider.bindLazyComponents();
    expect(setupComponentRegistry).toBeCalledTimes(1);

    const Wrapper = componentProvider.resolveComponentSuspended(KEY as any);

    expect(Wrapper).toBeDefined();

    const mounted = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <Wrapper />
        </InitModulesWrapper>
      )
    );

    expect(mounted.find(TestComponent).exists()).toBeTruthy();
    done();
  });
});
