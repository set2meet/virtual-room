/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { generateUID } from '../../../../common/utils/generateUID';

export enum ComponentRegistryKey {
  // app
  CollaborationPanel = generateUID(),
  SidebarPanel = generateUID(),
  Header = generateUID(),

  // common
  GlobalStyles = generateUID(),
  SimpleToggleButton = generateUID(),
  Button = generateUID(),
  Spinner = generateUID(),
  SidePanel = generateUID(),
  UserAvatar = generateUID(),
  RoundButton = generateUID(),
  WaitingPage = generateUID(),
  BrowserNotSupported = generateUID(),
  Checkbox = generateUID(),
  Indicator = generateUID(),

  // layout
  Chat_PANEL = generateUID(),
  Chat_SoundNotifications = generateUID(),
  Chat_NotificationSetup = generateUID(),

  Feedback_Panel = generateUID(),

  Room_Header = generateUID(),

  WebPhone_ButtonSession = generateUID(),
  WebPhone_BaseScreen = generateUID(),
  WebPhone_ButtonScreenSharing = generateUID(),
  WebPhone_ButtonMicrophone = generateUID(),
  WebPhone_ButtonCamera = generateUID(),
  WebPhone_Participants = generateUID(),
  WebPhone_PreCallTest = generateUID(),
  WebPhone_UserSetup = generateUID(),

  Auth_ChangePassword = generateUID(),
  Auth_LoginPage = generateUID(),
  Auth_LogoutButton = generateUID(),

  CodeEditor = generateUID(),
  Notepad = generateUID(),

  Fullscreen_Button = generateUID(),

  Recording_Recordings = generateUID(),
  Recording_Button = generateUID(),
  Recording_VideoPlayer = generateUID(),

  Media_Devices = generateUID(),
  Media_MediaSettingsAudio = generateUID(),
  Media_MediaSettingsVideo = generateUID(),
  Media_UnpluggedDeviceWarning = generateUID(),
  Media_MediaDeviceSelectAudioInput = generateUID(),
  Media_MediaDeviceSelectVideoInput = generateUID(),
  Media_MediaDeviceAudioOutputExample = generateUID(),
  Media_MediaDeviceTestMicrophone = generateUID(),
  Media_MediaDeviceTestCamera = generateUID(),

  RoomIsFool = generateUID(),

  // Pages
  FinalPage = generateUID(),
  VideoPage = generateUID(),
}
