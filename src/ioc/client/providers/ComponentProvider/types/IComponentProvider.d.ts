/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/**
 * WARN: this is exposed separately from the rest interfaces in order to optimize entry point code splitting.
 * @see AppLoader source for more
 */

import { ComponentRegistryKey } from './ComponentRegistryKey';
import { ILoadableWrapper } from '../../../components/LoadableWrapper/loadableWrapper';
import { ComponentType } from 'react';
import { IComponents } from '../../../types/interfaces';

export type TComponentRegistryEntity<P = {}, Options = {}> = (
  factoryInitOptions?: Options
) => Promise<{ default: ComponentType<P> }>;

export interface IComponentProvider {
  resolveComponent<P = {}>(componentKey: ComponentRegistryKey): ILoadableWrapper<P>;
  resolveComponentSuspended<P = {}>(componentKey: ComponentRegistryKey, fallback?: JSX.Element): ILoadableWrapper<P>;
  destroy(): void;
}

/**
 * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
 */
export interface IComponentProviderDeprecated extends IComponents, IComponentProvider {}
