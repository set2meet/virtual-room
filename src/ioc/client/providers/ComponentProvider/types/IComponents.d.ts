/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { vrCommon } from '../../../../../modules/common';
import React, { ComponentType } from 'react';
import { IUIMedia } from '../../../../../modules/media/types/mediaTypes';
import { ICodeEditorProps } from '../../../../../modules/codeEditor/components/CodeEditor/ICodeEditorProps';
import { INotepadProps } from '../../../../../modules/notepad/components/Notepad/INotepadProps';
import { IUIWebPhone } from '../../../../../modules/webphone/client/components/IUIWebPhone';
import { IAuthComponents } from '../../../../../modules/auth/client/components/IAuthComponents';

/**
 * Old components module definition, used for backward compatibility.
 * @deprecated Use ComponentProvider.resolveComponentSuspended instead
 */
export interface IComponents {
  WebPhone: IUIWebPhone;
  UI: vrCommon.client.UI.ICommonUI;
  Auth: IAuthComponents;
  CodeEditor: React.ComponentClass<ICodeEditorProps>;
  Notepad: React.ComponentClass<INotepadProps>;
  Media: IUIMedia;
  RoomIsFull: ComponentType<any>;
  FinalPage: ComponentType<any>;
  VideoPage: ComponentType<any>;
}
