import React, { ComponentType } from 'react';
import Skeleton from 'react-loading-skeleton';
import { ComponentRegistryKey } from './types/ComponentRegistryKey';
import loadableWrapper, { ILoadableWrapper } from '../../components/LoadableWrapper/loadableWrapper';
import { IComponents } from '../../types/interfaces';
import { IComponentProvider, TComponentRegistryEntity } from './types/IComponentProvider';
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';

type TComponentRegistry = {
  [key in ComponentRegistryKey]: TComponentRegistryEntity;
};

export default class ComponentProvider extends BaseDependencyProvider<ComponentRegistryKey>
  implements IComponents, IComponentProvider {
  private readonly DEFAULT_LOAD_DELAY: number = 1000;
  /**
   * @todo remove when all deprecated usage (see @deprecated section) is removed
   * @deprecated DEFAULT_SKELETON is used in cases with deprecated direct components access:
   * clientEntityProvider.getComponentProvider().resolveComponentSuspended
   * @protected
   */
  protected readonly DEFAULT_SKELETON: JSX.Element = (<Skeleton width={100} height={100} />);

  private mockLazyFactory(oldDependency: ComponentType): TComponentRegistryEntity {
    return () =>
      clientEntityProvider
        .getUtils()
        .sleep(this.DEFAULT_LOAD_DELAY)
        .then(() => ({ default: oldDependency }));
  }

  protected setupComponentRegistry(): TComponentRegistry {
    const oldSyncComponents = this;

    return {
      [ComponentRegistryKey.Button]: this.mockLazyFactory(oldSyncComponents.UI.Button),

      [ComponentRegistryKey.SimpleToggleButton]: this.mockLazyFactory(oldSyncComponents.UI.SimpleToggleButton),
      [ComponentRegistryKey.Spinner]: this.mockLazyFactory(oldSyncComponents.UI.Spinner),
      [ComponentRegistryKey.SidePanel]: this.mockLazyFactory(oldSyncComponents.UI.SidePanel),
      [ComponentRegistryKey.RoundButton]: this.mockLazyFactory(oldSyncComponents.UI.RoundButton),
      [ComponentRegistryKey.UserAvatar]: this.mockLazyFactory(oldSyncComponents.UI.UserAvatar),
      [ComponentRegistryKey.WaitingPage]: this.mockLazyFactory(oldSyncComponents.UI.WaitingPage),
      [ComponentRegistryKey.BrowserNotSupported]: this.mockLazyFactory(oldSyncComponents.UI.BrowserNotSupported),
      [ComponentRegistryKey.Checkbox]: this.mockLazyFactory(oldSyncComponents.UI.Checkbox),
      [ComponentRegistryKey.GlobalStyles]: this.mockLazyFactory(oldSyncComponents.UI.GlobalStyles),

      [ComponentRegistryKey.WebPhone_ButtonSession]: this.mockLazyFactory(oldSyncComponents.WebPhone.ButtonSession),
      [ComponentRegistryKey.WebPhone_BaseScreen]: this.mockLazyFactory(oldSyncComponents.WebPhone.BaseScreen),
      [ComponentRegistryKey.WebPhone_ButtonScreenSharing]: this.mockLazyFactory(
        oldSyncComponents.WebPhone.ButtonScreenSharing
      ),
      [ComponentRegistryKey.WebPhone_ButtonMicrophone]: this.mockLazyFactory(
        oldSyncComponents.WebPhone.ButtonMicrophone
      ),
      [ComponentRegistryKey.WebPhone_ButtonCamera]: this.mockLazyFactory(oldSyncComponents.WebPhone.ButtonCamera),
      [ComponentRegistryKey.WebPhone_PreCallTest]: this.mockLazyFactory(oldSyncComponents.WebPhone.PreCallTest),
      [ComponentRegistryKey.WebPhone_UserSetup]: this.mockLazyFactory(oldSyncComponents.WebPhone.UserSetup),

      [ComponentRegistryKey.Auth_ChangePassword]: this.mockLazyFactory(oldSyncComponents.Auth.ChangePassword),
      [ComponentRegistryKey.Auth_LoginPage]: this.mockLazyFactory(oldSyncComponents.Auth.LoginPage),
      [ComponentRegistryKey.Auth_LogoutButton]: this.mockLazyFactory(oldSyncComponents.Auth.LogoutButton),

      [ComponentRegistryKey.CodeEditor]: this.mockLazyFactory(oldSyncComponents.CodeEditor),
      [ComponentRegistryKey.Notepad]: this.mockLazyFactory(oldSyncComponents.Notepad),

      [ComponentRegistryKey.Media_Devices]: this.mockLazyFactory(oldSyncComponents.Media.Devices),
      [ComponentRegistryKey.Media_MediaSettingsAudio]: this.mockLazyFactory(oldSyncComponents.Media.MediaSettingsAudio),
      [ComponentRegistryKey.Media_MediaSettingsVideo]: this.mockLazyFactory(oldSyncComponents.Media.MediaSettingsVideo),
      [ComponentRegistryKey.Media_UnpluggedDeviceWarning]: this.mockLazyFactory(
        oldSyncComponents.Media.UnpluggedDeviceWarning
      ),
      [ComponentRegistryKey.Media_MediaDeviceTestCamera]: this.mockLazyFactory(
        oldSyncComponents.Media.MediaDeviceTestCamera
      ),
      [ComponentRegistryKey.Media_MediaDeviceSelectAudioInput]: this.mockLazyFactory(
        oldSyncComponents.Media.MediaDeviceSelectAudioInput
      ),
      [ComponentRegistryKey.Media_MediaDeviceSelectVideoInput]: this.mockLazyFactory(
        oldSyncComponents.Media.MediaDeviceSelectVideoInput
      ),
      [ComponentRegistryKey.Media_MediaDeviceAudioOutputExample]: this.mockLazyFactory(
        oldSyncComponents.Media.MediaDeviceAudioOutputExample
      ),
      [ComponentRegistryKey.Media_MediaDeviceTestMicrophone]: this.mockLazyFactory(
        oldSyncComponents.Media.MediaDeviceTestMicrophone
      ),

      [ComponentRegistryKey.RoomIsFool]: this.mockLazyFactory(oldSyncComponents.RoomIsFull),

      [ComponentRegistryKey.FinalPage]: this.mockLazyFactory(oldSyncComponents.FinalPage),
      [ComponentRegistryKey.VideoPage]: this.mockLazyFactory(oldSyncComponents.VideoPage),

      /**
       * @TODO:
       * If we use lazy imports with regular one within the same codebase bundle size would increase twice.
       * To workaround this i suggest to iteratively replace the old factories (above) with below lazy one.
       * All you need is to make sure that whole client code that uses some component is rewritten to `resolveComponent`
       * and just replace the old factory with the new one using the same registry key (just uncomment it and delete old)
       */

      // [ComponentRegistryKey.Button]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/Button" */
      //     `../../../../modules/common/src/ui/components/Button`
      //   ),
      [ComponentRegistryKey.Indicator]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/Indicator" */
          `../../../../modules/app/components/Indicator`
        ),
      // [ComponentRegistryKey.SimpleToggleButton]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/SimpleToggleButton" */
      //     `../../../../modules/common/src/ui/components/SimpleToggleButton`
      //   ),
      // [ComponentRegistryKey.Spinner]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/Spinner" */
      //     `../../../../modules/common/src/ui/components/Spinner`
      //   ),
      // [ComponentRegistryKey.SidePanel]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/SidePanel" */
      //     `../../../../modules/common/src/ui/components/SidePanel`
      //   ),
      // [ComponentRegistryKey.RoundButton]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/RoundButton" */
      //     `../../../../modules/common/src/ui/components/RoundButton`
      //   ),
      // [ComponentRegistryKey.UserAvatar]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/UserAvatar" */
      //     `../../../../modules/common/src/ui/components/UserAvatar`
      //   ),
      // [ComponentRegistryKey.WaitingPage]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/WaitingPage" */
      //     `../../../../modules/common/src/ui/components/WaitingPage`
      //   ),
      // [ComponentRegistryKey.BrowserNotSupported]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/BrowserNotSupported" */
      //     `../../../../modules/common/src/ui/components/BrowserNotSupported`
      //   ),
      // [ComponentRegistryKey.Checkbox]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/Checkbox" */
      //     `../../../../modules/common/src/ui/components/Checkbox`
      //   ),
      // [ComponentRegistryKey.GlobalStyles]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/GlobalStyles" */
      //     `../../../../modules/common/src/ui/components/GlobalStyles`
      //   ),
      //
      [ComponentRegistryKey.Chat_SoundNotifications]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/SoundNotifications" */
          `../../../../modules/chat/client/components/SoundNotifications/SoundNotifications`
        ),
      [ComponentRegistryKey.Chat_NotificationSetup]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/NotificationSetup" */
          `../../../../modules/chat/client/components/NotificationSetup/NotificationSetup`
        ),
      [ComponentRegistryKey.Chat_PANEL]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/Panel" */
          `../../../../modules/chat/client/components/Panel/Panel`
        ),
      // [ComponentRegistryKey.WebPhone_ButtonSession]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/ButtonSession" */
      //     `../../../../modules/webphone/src/components/ButtonSession`
      //   ),
      // [ComponentRegistryKey.WebPhone_BaseScreen]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/BaseScreen" */
      //     `../../../../modules/webphone/src/components/BaseScreen`
      //   ),
      // [ComponentRegistryKey.WebPhone_ButtonScreenSharing]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/ButtonScreenSharing" */
      //     `../../../../modules/webphone/src/components/ButtonScreenSharing`
      //   ),
      // [ComponentRegistryKey.WebPhone_ButtonMicrophone]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/ButtonMicrophone" */
      //     `../../../../modules/webphone/src/components/ButtonMicrophone`
      //   ),
      // [ComponentRegistryKey.WebPhone_ButtonCamera]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/ButtonCamera" */
      //     `../../../../modules/webphone/src/components/ButtonCamera`
      //   ),
      [ComponentRegistryKey.WebPhone_Participants]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/Participants" */
          `../../../../modules/webphone/client/components/Participants/Participants`
        ),
      // [ComponentRegistryKey.WebPhone_PreCallTest]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/PreCallTest" */
      //     `../../../../modules/webphone/src/components/PreCallTest`
      //   ),
      // [ComponentRegistryKey.WebPhone_UserSetup]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/UserSetup" */
      //     `../../../../modules/webphone/src/components/UserSetup`
      //   ),
      // [ComponentRegistryKey.Auth_ChangePassword]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/ChangePassword" */
      //     `../../../../modules/auth/src/client/ui/ChangePassword/ChangePassword`
      //   ),
      // [ComponentRegistryKey.Auth_LoginPage]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/LoginPage" */
      //     `../../../../modules/auth/src/client/ui/LoginPage`
      //   ),
      // [ComponentRegistryKey.Auth_LogoutButton]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/LogoutButton" */
      //     `../../../../modules/auth/src/client/ui/LogoutButton`
      //   ),
      //
      // [ComponentRegistryKey.CodeEditor]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/CodeEditor" */
      //     `../../../../modules/CodeEditor/src/components/CodeEditor`
      //   ),
      // [ComponentRegistryKey.Notepad]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/Notepad" */
      //     `../../../../modules/Notepad/src/components/Notepad`
      //   ),
      //
      [ComponentRegistryKey.Fullscreen_Button]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/FullscreenButton" */
          `../../../../modules/fullscreen/components/Button`
        ),
      [ComponentRegistryKey.Recording_Recordings]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/Recordings" */
          `../../../../modules/recording/src/client/components/Recordings/Recordings`
        ),
      [ComponentRegistryKey.Recording_Button]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/RecordingButton" */
          `../../../../modules/recording/src/client/components/RecordingButton/RecordingButton`
        ),
      [ComponentRegistryKey.Recording_VideoPlayer]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/RecordViewerPanel" */
          `../../../../modules/recording/src/client/components/VideoPlayer/VideoPlayer`
        ),
      //
      // [ComponentRegistryKey.Media_Devices]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/Devices" */
      //     `../../../../modules/media/src/components/Devices`
      //   ),
      // [ComponentRegistryKey.Media_MediaSettingsAudio]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/MediaSettingsAudio" */
      //     `../../../../modules/media/src/components/MediaSettingsAudio/MediaSettingsAudio`
      //   ),
      // [ComponentRegistryKey.Media_MediaSettingsVideo]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/MediaSettingsVideo" */
      //     `../../../../modules/media/src/components/MediaSettingsVideo/MediaSettingsVideo`
      //   ),
      // [ComponentRegistryKey.Media_UnpluggedDeviceWarning]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/UnpluggedDeviceWarning" */
      //     `../../../../modules/media/src/components/UnpluggedDeviceWarning/UnpluggedDeviceWarning`
      //   ),
      // [ComponentRegistryKey.Media_MediaDeviceSelectAudioInput]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/MediaDeviceSelectAudioInput" */
      //     `../../../../modules/media/src/components/MediaDeviceSelectAudioInput/index`
      //   ),
      // [ComponentRegistryKey.Media_MediaDeviceSelectVideoInput]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/MediaDeviceSelectVideoInput" */
      //     `../../../../modules/media/src/components/MediaDeviceSelectVideoInput/index`
      //   ),
      // [ComponentRegistryKey.Media_MediaDeviceAudioOutputExample]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/MediaSettingsAudioLevelSample" */
      //     `../../../../modules/media/src/components/MediaSettingsAudioLevelSample/MediaSettingsAudioLevelSample`
      //   ),
      // [ComponentRegistryKey.Media_MediaDeviceTestMicrophone]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/TestMicrophone" */
      //     `../../../../modules/media/src/components/TestMicrophone/TestMicrophone`
      //   ),
      // [ComponentRegistryKey.Media_MediaDeviceTestCamera]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/TestCamera" */
      //     `../../../../modules/media/src/components/TestCamera/TestCamera`
      //   ),
      //
      [ComponentRegistryKey.Feedback_Panel]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/FeedbackPanel" */
          `../../../../modules/feedback/client/components/FeedbackPanel/FeedbackPanel`
        ),

      [ComponentRegistryKey.Room_Header]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/RoomHeader" */
          `../../../../client/routes/Room/RoomHeader`
        ),

      // app module:

      // [ComponentRegistryKey.RoomIsFool]: () => import(
      //     /* webpackPrefetch: true, webpackChunkName: "lazy/components/RoomIsFull" */
      //     `../../client/components/RoomIsFull`
      //   ),
      [ComponentRegistryKey.Header]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/Header" */
          `../../../../modules/app/components/Header`
        ),
      [ComponentRegistryKey.CollaborationPanel]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/CollaborationPanel" */
          `../../../../modules/app/components/CollaborationPanel/CollaborationPanel`
        ),
      [ComponentRegistryKey.SidebarPanel]: () =>
        import(
          /* webpackPrefetch: true, webpackChunkName: "lazy/components/SidebarPanel" */
          `../../../../modules/app/components/SidebarPanel`
        ),
    };
  }

  /**
   * Usually you don't need this method, just use ComponentProvider.resolveComponentSuspended
   */
  public resolveComponent<P = {}>(componentKey: ComponentRegistryKey): ILoadableWrapper<P> {
    return this.getDependency<ILoadableWrapper<P>>(componentKey);
  }

  public resolveComponentSuspended<P = {}>(
    componentKey: ComponentRegistryKey,
    fallback: JSX.Element = this.DEFAULT_SKELETON
  ): ILoadableWrapper<P> {
    const Component = this.resolveComponent<P>(componentKey);

    if (!Component.initialFallback) {
      Component.setFallback(fallback);
    }
    return Component;
  }

  public bindLazyComponents() {
    const componentRegistry = { ...this.setupComponentRegistry() };

    Object.keys(componentRegistry).forEach((registryKey) => {
      const currentFactory: TComponentRegistryEntity = componentRegistry[registryKey];

      this.setDependency<ILoadableWrapper>(
        (registryKey as unknown) as ComponentRegistryKey,
        loadableWrapper(currentFactory)
      );
    });
  }

  /**
   * @deprecated this method is deprecated and is currently used only for backawrd compatibility purposes.
   * Use .resolveComponent instead.
   */
  public bindSyncComponents({
    WebPhone,
    UI,
    Auth,
    CodeEditor,
    Notepad,
    Media,
    RoomIsFull,
    FinalPage,
    VideoPage,
  }: IComponents) {
    this.WebPhone = WebPhone;
    this.UI = UI;
    this.Auth = Auth;
    this.Notepad = Notepad;
    this.Media = Media;
    this.RoomIsFull = RoomIsFull;
    this.CodeEditor = CodeEditor;
    this.FinalPage = FinalPage;
    this.VideoPage = VideoPage;
  }

  // backward compatibility bindings

  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public WebPhone;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public UI;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public Auth;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public CodeEditor;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public Notepad;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public Media;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public RoomIsFull;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public FinalPage;
  /**
   * @deprecated: Use ComponentProvider.resolveComponentSuspended instead
   */
  public VideoPage;
}
