import React from 'react';
import { enzymeCleanup, InitModulesWrapper } from '../../../../test/utils/utils';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import { IClientEntityProviderInternals } from '../ClientEntityProvider/types/IClientEntityProvider';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { TDynamicModuleWrapperProps } from '../ModuleProvider/types/IS2MModule';
import { TRouterModuleInitOptions } from '../../../../modules/router/TRouterModuleInitOptions';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import { ModuleRegistryKey } from './types/ModuleRegistryKey';
import ModuleProvider from './ModuleProvider';
import { IModuleProvider } from './types/IModuleProvider';

beforeEach(setupIoCClientForTest());

describe('ModuleProvider', () => {
  afterEach(() => {
    jest.restoreAllMocks();
    jest.clearAllMocks();
    enzymeCleanup();
    clientEntityProvider.destroy();
  });
  it('ModuleProvider should unbind components on destroy call', () => {
    const moduleProvider = clientEntityProvider.getModuleProvider();
    const AppModule = moduleProvider.resolveModuleSuspended(ModuleRegistryKey.AppModule);

    expect(AppModule).toBeDefined();

    moduleProvider.destroy();

    expect(() => moduleProvider.resolveModuleSuspended(ModuleRegistryKey.AppModule)).toThrowError(
      /No matching bindings found for serviceIdentifier:.*/
    );
  });
  it('Suspended module shouldnt be re-rendered if props are updated in parent component', async (done) => {
    const RouterModuleWrapper = (props) => {
      const RouterModule = clientEntityProvider.resolveModuleSuspended<
        TDynamicModuleWrapperProps,
        TRouterModuleInitOptions
      >(ModuleRegistryKey.RouterModule, {
        history: (clientEntityProvider as IClientEntityProviderInternals).getHistory(),
      });
      return (
        <InitModulesWrapper withRouterModule={false}>
          <RouterModule />
        </InitModulesWrapper>
      );
    };
    const mounted = await loadAsyncTree(EnzymeMountTracker.mount(<RouterModuleWrapper text={'123'} />));
    const componentWillUnmount = jest.spyOn(mounted.update().find('Router').instance(), 'componentWillUnmount');

    mounted.setProps({ text: '1234' }).update();
    expect(componentWillUnmount).not.toHaveBeenCalled();
    done();
  });
  it('should bind and resolve module', async (done) => {
    const moduleProvider = clientEntityProvider.getModuleProvider() as ModuleProvider;
    const TestComponent = () => <></>;
    const KEY = 'KEY';

    const setupModuleRegistry = jest.spyOn(moduleProvider, 'setupModuleRegistry' as any).mockImplementation(() => ({
      KEY: () => new Promise((resolve) => resolve({ default: TestComponent })),
    }));

    moduleProvider.bindLazyModules();
    expect(setupModuleRegistry).toBeCalledTimes(1);

    const Wrapper = moduleProvider.resolveModuleSuspended(KEY as any);

    expect(Wrapper).toBeDefined();

    const mounted = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <Wrapper />
        </InitModulesWrapper>
      )
    );

    expect(mounted.find(TestComponent).exists()).toBeTruthy();
    done();
  });
  it('check resolveModuleSuspended overload', () => {
    const moduleProvider = clientEntityProvider.getModuleProvider() as ModuleProvider & IModuleProvider;
    const TestComponent = () => <></>;
    const TestFallback = <></>;
    const KEY = 'KEY';
    const KEY2 = 'KEY2';

    jest.spyOn(moduleProvider, 'setupModuleRegistry' as any).mockImplementation(() => ({
      KEY: () => new Promise((resolve) => resolve({ default: TestComponent })),
      KEY2: () => new Promise((resolve) => resolve({ default: TestComponent })),
    }));

    moduleProvider.bindLazyModules();

    const Wrapper1 = moduleProvider.resolveModuleSuspended(KEY as any, {}, TestFallback);
    const Wrapper2 = moduleProvider.resolveModuleSuspended(KEY2 as any, TestFallback);

    expect(Wrapper1.initialFallback).toEqual(Wrapper2.initialFallback);
    expect(Wrapper1.moduleFactoryInitOptions).toEqual({});
    expect(Wrapper2.moduleFactoryInitOptions).toBeUndefined();
  });
  it('should delay loading first time', async (done) => {
    const loadDelay: number = 500;
    const moduleProvider = clientEntityProvider.getModuleProvider() as ModuleProvider;
    const TestComponent = () => <></>;
    const KEY = 'KEY';

    jest.spyOn(moduleProvider, 'setupModuleRegistry' as any).mockImplementation(() => ({
      KEY: () => new Promise((resolve) => resolve({ default: TestComponent })),
    }));

    moduleProvider.bindLazyModules();
    const Wrapper = moduleProvider.resolveModuleSuspended(KEY as any);

    expect(Wrapper).toBeDefined();

    let loadStartStamp: number = Date.now();
    const mounted = await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <Wrapper />
        </InitModulesWrapper>
      )
    );
    let loadDuration: number = Date.now() - loadStartStamp;

    expect(mounted.find(TestComponent).exists()).toBeTruthy();
    expect(loadDuration >= loadDelay).toBeTruthy();

    // mount second time
    loadStartStamp = Date.now();
    await loadAsyncTree(
      EnzymeMountTracker.mount(
        <InitModulesWrapper>
          <Wrapper />
        </InitModulesWrapper>
      )
    );
    loadDuration = Date.now() - loadStartStamp;
    expect(loadDuration < loadDelay).toBeTruthy();
    done();
  });
});
