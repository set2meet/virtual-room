/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { ModuleRegistryKey } from './ModuleRegistryKey';
import { ILoadableWrapper } from '../../../components/LoadableWrapper/loadableWrapper';

export interface IModuleProvider {
  resolveModuleSuspended<P = {}>(moduleKey: ModuleRegistryKey, fallback?: JSX.Element): ILoadableWrapper<P>;
  resolveModuleSuspended<P = {}, Options = {}>(
    moduleKey: ModuleRegistryKey,
    moduleFactoryInitOptions: Options,
    fallback?: JSX.Element
  ): ILoadableWrapper<P>;
  destroy(): void;
}
