/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { generateUID } from '../../../../common/utils/generateUID';

export enum ModuleRegistryKey {
  AppModule = generateUID(),
  Auth = generateUID(),
  RouterModule = generateUID(),
  Connection = generateUID(),
  Chat = generateUID(),
  Fullscreen = generateUID(),
  Graph2D = generateUID(),
  Media = generateUID(),
  Notification = generateUID(),
  Presentations = generateUID(),
  Recording = generateUID(),
  RoomModule = generateUID(),
  Whiteboard = generateUID(),
  Webrtc = generateUID(),
  Webphone = generateUID(),
}
