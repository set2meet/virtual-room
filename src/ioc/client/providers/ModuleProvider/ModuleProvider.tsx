/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import {
  IS2MModule,
  TCustomModuleWrapper,
  TDynamicModuleWrapper,
  TDynamicModuleWrapperProps,
  TS2MModuleFactory,
} from './types/IS2MModule';
import { timeout } from 'promise-timeout';
import { IModuleTuple } from 'redux-dynamic-modules-core';
import React, { ComponentType } from 'react';
import { DynamicModuleLoader } from '../../components/DynamicModuleLoader/DynamicModuleLoader';
import Socket = SocketIOClient.Socket;
import loadableWrapper, {
  IDefaultLoadableWrapperProps,
  ILoadableWrapper,
} from '../../components/LoadableWrapper/loadableWrapper';
import { ModuleRegistryKey } from './types/ModuleRegistryKey';
import { IModuleProvider } from './types/IModuleProvider';
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import { IFallbackProvider } from '../FallbackProvider/types/IFallbackProvider';

type TModuleRegistryEntity<P = {}, Options = {}> = (
  moduleInitOptions?: Options
) => Promise<{ default: TDynamicModuleWrapper<P> }>;

type TModuleRegistry = {
  [key in ModuleRegistryKey]: TModuleRegistryEntity;
};

export default class ModuleProvider extends BaseDependencyProvider<ModuleRegistryKey> implements IModuleProvider {
  public static readonly LOAD_TIMEOUT: number = 20000;

  private readonly DEFAULT_LOAD_DELAY: number = 500;

  private loadedModuleMap: Record<string, boolean> = {};

  constructor(
    private fallbackProvider: IFallbackProvider,
    private clientSocket: Socket,
    private moduleWrapper?: TCustomModuleWrapper
  ) {
    super();
  }

  public destroy() {
    super.destroy();
    this.loadedModuleMap = {};
  }

  protected wrapModuleFactory(
    lazyModuleFactory: () => Promise<{ default: TS2MModuleFactory }>,
    stateKey: string
  ): TModuleRegistryEntity {
    const socket = this.clientSocket;

    return async (moduleInitOptions) => {
      const loadStartStamp: number = Date.now();
      const moduleFactory: TS2MModuleFactory = (await lazyModuleFactory()).default;
      const module: IS2MModule = await moduleFactory(socket, stateKey, moduleInitOptions);
      const { reduxModules = [], rootComponent: RootComponent } = module;
      const loadDuration: number = Date.now() - loadStartStamp;

      if (!this.loadedModuleMap[stateKey] && loadDuration < this.DEFAULT_LOAD_DELAY) {
        await clientEntityProvider.getUtils().sleep(this.DEFAULT_LOAD_DELAY - loadDuration);
        this.loadedModuleMap[stateKey] = true;
      }

      return {
        default: (this.moduleWrapper || this.defaultModuleWrapper)(reduxModules, RootComponent),
      };
    };
  }

  protected defaultModuleWrapper<P>(
    reduxModules: IModuleTuple,
    RootComponent?: ComponentType
  ): TDynamicModuleWrapper<P> {
    return (props: TDynamicModuleWrapperProps<P>) => {
      const rootComponent = RootComponent ? <RootComponent {...props} /> : <>{props.children}</>;

      return (
        <DynamicModuleLoader skipUnmountCleanup={true} modules={reduxModules}>
          {rootComponent}
        </DynamicModuleLoader>
      );
    };
  }

  protected setupModuleRegistry(): TModuleRegistry {
    return {
      [ModuleRegistryKey.RouterModule]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/routerModule" */
            `../../../../modules/router/routerModule`
          ),
        'router'
      ),
      [ModuleRegistryKey.AppModule]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/appModule" */
            `../../../../modules/app/appModule`
          ),
        'app'
      ),
      [ModuleRegistryKey.Auth]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/authModule" */
            `../../../../modules/auth/client/authModule`
          ),
        'auth'
      ),
      [ModuleRegistryKey.Connection]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/connectionModule" */
            `../../../../modules/connection/connectionModule`
          ),
        'connection'
      ),
      [ModuleRegistryKey.Chat]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/chatModule" */
            `../../../../modules/chat/client/chatModule`
          ),
        'chat'
      ),
      [ModuleRegistryKey.Fullscreen]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/fullscreenModule" */
            `../../../../modules/fullscreen/fullscreenModule`
          ),
        'fullscreen'
      ),
      [ModuleRegistryKey.Graph2D]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/graph2dModule" */
            `../../../../modules/graph2d/client/graph2dModule`
          ),
        'graph2d'
      ),
      [ModuleRegistryKey.Media]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/mediaModule" */
            `../../../../modules/media/mediaModule`
          ),
        'media'
      ),
      [ModuleRegistryKey.Notification]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/notificationModule" */
            `../../../../modules/notification/notificationModule`
          ),
        'toasts'
      ),
      [ModuleRegistryKey.Presentations]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/presentationsModule" */
            `../../../../modules/presentations/client/presentationsModule`
          ),
        'presentations'
      ),
      [ModuleRegistryKey.Recording]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/recordingModule" */
            `../../../../modules/recording/recordingModule`
          ),
        'recording'
      ),
      [ModuleRegistryKey.RoomModule]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/roomModule" */
            `../../../../modules/room/client/roomModule`
          ),
        'room'
      ),
      [ModuleRegistryKey.Whiteboard]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/whiteboardModule" */
            `../../../../modules/whiteboard/client/whiteboardModule`
          ),
        'whiteboard'
      ),
      [ModuleRegistryKey.Webrtc]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/webrtcModule" */
            `../../../../modules/webrtc/client/webrtcModule`
          ),
        'webrtcService'
      ),
      [ModuleRegistryKey.Webphone]: this.wrapModuleFactory(
        () =>
          import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/modules/webphoneModule" */
            `../../../../modules/webphone/client/webphoneModule`
          ),
        'webphone'
      ),
    };
  }

  private resolveModule<P = {}, Options = {}>(
    moduleKey: ModuleRegistryKey,
    moduleFactoryInitOptions: Options,
    fallback?: JSX.Element
  ): ILoadableWrapper<P> {
    const Module = this.getDependency<ILoadableWrapper<P>>(moduleKey);

    if (!Module.initialFallback) {
      Module.setFallback(fallback);
    }

    if (!Module.moduleFactoryInitOptions) {
      Module.setModuleInitialOptions(moduleFactoryInitOptions);
    }

    return Module;
  }

  /**
   * Same as resolveComponentSuspended, but with moduleFactoryInitOptions bindings
   * @param moduleKey
   * @param fallback
   * @param moduleFactoryInitOptions
   */
  public resolveModuleSuspended<P = {}>(moduleKey: ModuleRegistryKey, fallback?: JSX.Element): ILoadableWrapper<P>;
  public resolveModuleSuspended<P = {}, Options = {}>(
    moduleKey: ModuleRegistryKey,
    moduleFactoryInitOptions: Options,
    fallback?: JSX.Element
  ): ILoadableWrapper<P> {
    if (!fallback) {
      if (React.isValidElement(moduleFactoryInitOptions) || moduleFactoryInitOptions === null) {
        // empty options & fallback case
        return this.resolveModule<P>(moduleKey, undefined, (moduleFactoryInitOptions as unknown) as JSX.Element);
      } else {
        // default fallback and defined | default options case
        return this.resolveModule<P, Options>(
          moduleKey,
          moduleFactoryInitOptions,
          this.fallbackProvider.resolveFallbackJSX(moduleKey)
        );
      }
    } else {
      return this.resolveModule<P, Options>(moduleKey, moduleFactoryInitOptions, fallback);
    }
  }

  public bindLazyModules() {
    const moduleRegistry: TModuleRegistry = { ...this.setupModuleRegistry() };

    Object.keys(moduleRegistry).forEach((registryKey) => {
      const currentFactory: TModuleRegistryEntity = moduleRegistry[registryKey];

      this.setDependency<ILoadableWrapper>(
        (registryKey as unknown) as ModuleRegistryKey,
        loadableWrapper(({ moduleFactoryInitOptions }: IDefaultLoadableWrapperProps) =>
          timeout(currentFactory(moduleFactoryInitOptions), ModuleProvider.LOAD_TIMEOUT)
        )
      );
    });
  }
}
