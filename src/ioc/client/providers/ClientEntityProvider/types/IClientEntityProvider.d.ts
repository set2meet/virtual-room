/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import Socket = SocketIOClient.Socket;
import { AxiosInstance } from 'axios';
import { IComponentProvider, IComponentProviderDeprecated } from '../../ComponentProvider/types/IComponentProvider';
import {
  TInitializationOptions,
  ITheme,
  IThemes,
  TStore,
  THistory,
  IAppStorageKeys,
  IBrowser,
  IClientLogger,
} from '../../../types/interfaces';
import { IModuleProvider } from '../../ModuleProvider/types/IModuleProvider';
import {
  IAppActionCreators,
  IAppActionTypes,
  IClientConfig,
  ICommonIcons,
  TWebRTCServices,
} from '../../../../common/ioc-interfaces';
import { IUserManager } from '../../AuthProvider/types/client/AuthProvider.interfaces';
import { IUserMediaService } from '../../../../../modules/media/services/types/IUserMediaService';
import { IAppConstants } from './IAppConstants';
import StorageManager from '../../StorageManager/StorageManager';
import { IFallbackProvider } from '../../FallbackProvider/types/IFallbackProvider';
import { IUtilsProvider } from '../../UtilsProvider/IUtilsProvider';
import { IWebphoneSelectors } from '../../../../../modules/webphone/client/redux/selectors/IWebphoneSelectors';
import { IChatSelectors } from '../../../../../modules/chat/client/redux/selectors/IChatSelectors';
import { IAppSelectors } from '../../../../../modules/app/redux/selectors/types/IAppSelectors';
import { IRecordingsClient } from '../../../../../modules/recording/src/client/services/RecordingsClient/IRecordingsClient';

export interface ISelectors {
  chatSelectors: IChatSelectors;
  appSelectors: IAppSelectors;
  webphoneSelectors: IWebphoneSelectors;
}

export interface IAudioContextExtended extends AudioContext {
  createMediaStreamDestination: () => MediaStreamAudioDestinationNode;
}

/**
 * @deprecated this methods are deprecated, use IClientEntityProvider ones
 */
export interface IClientEntityProviderDeprecatedProps {
  /**
   * @deprecated property getter is deprecated, use getAppActionCreators() instead
   */
  appActionCreators: IAppActionCreators;
  /**
   * @deprecated property getter is deprecated, use getConstants() instead
   */
  constants: IAppConstants;
  /**
   * @deprecated property getter is deprecated, use getDefaultTheme() instead
   */
  defaultTheme: ITheme;
  /**
   * @deprecated property getter is deprecated, use getConfig() instead
   */
  config: IClientConfig;
  /**
   * @deprecated property getter is deprecated, use getWebAudioContext() instead
   */
  webAudioContext: IAudioContextExtended;
}

/**
 * @deprecated this methods are deprecated. Refer individual method description to knew more.
 */
export interface IClientEntityProviderDeprecatedMethods {
  /**
   * @deprecated this method is for internal usage only and is exposed now just for backward compatibility.
   * You should rely on resolveComponentSuspended instead
   */
  getComponentProvider(): IComponentProviderDeprecated;
  /**
   * @deprecated this method is for internal usage only.
   * You should rely on resolveModuleSuspended instead
   */
  getModuleProvider(): IModuleProvider;
}

export interface IClientEntityProvider
  extends IComponentProvider,
    IModuleProvider,
    IFallbackProvider,
    IClientEntityProviderDeprecatedProps,
    IClientEntityProviderDeprecatedMethods {
  destroy(): Promise<void>;
  getThemes(): IThemes;
  getBrowser(): IBrowser;
  getIcons(): ICommonIcons;
  getSelectors(): ISelectors;
  getSocket(): Socket;
  getConfig(): IClientConfig;
  getConstants(): IAppConstants;
  getLogger(): IClientLogger;
  getWebAudioContext(): IAudioContextExtended;
  getAppActionCreators(): IAppActionCreators;
  getStorageKeys(): IAppStorageKeys;
  getStorageManager(): StorageManager;
  getUserManager(): IUserManager;
  getActionCreators(): IAppActionCreators;
  getAppActionTypes(): IAppActionTypes;
  getHttpClient(): AxiosInstance;
  getRecordingsClient(): IRecordingsClient;
  getUserMediaService(): IUserMediaService;
  getWebRTCServices(): Promise<TWebRTCServices>;
  getUtils(): IUtilsProvider;
}

/**
 * Internal package methods, no support is guaranteed
 */
export interface IClientEntityProviderInternals extends IClientEntityProvider {
  isInitialized: boolean;
  initialize(initializationOptions?: TInitializationOptions): Promise<void>;
  getComponentProvider(): IComponentProviderDeprecated;
  getFallbackProvider(): IFallbackProvider;
  getStore(): TStore;
  getHistory(): THistory;
}
