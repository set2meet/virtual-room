/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IClientConfig } from '../../../../common/ioc-interfaces';
import { TCustomModuleWrapper } from '../../ModuleProvider/types/IS2MModule';
import Socket = SocketIOClient.Socket;
import { TEnhancerFactory, THistoryFactory, TStoreFactory } from '../../../types/interfaces';
import { TStorageFactory } from '../../StorageManager/storages/types';

export type TClientSocketFactory = (config: IClientConfig) => Promise<Socket>;

export type TInitializationOptions = {
  configFactory?: () => Promise<IClientConfig>;
  storageFactory?: TStorageFactory;
  storeEnhancerFactory?: TEnhancerFactory;
  customModuleWrapper?: TCustomModuleWrapper;
  socketFactory?: TClientSocketFactory;
  storeFactory?: TStoreFactory;
  historyFactory?: THistoryFactory;
  onDestroyStart?: () => void;
  onRecycleComplete?: () => void;
};
