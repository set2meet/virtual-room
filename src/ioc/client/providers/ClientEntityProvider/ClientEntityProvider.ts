/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import {
  IAppActionCreators,
  IAppActionTypes,
  IClientConfig,
  ICommonIcons,
  TWebRTCServices,
} from '../../../common/ioc-interfaces';
import { ui as componentsLibrary } from '../../../../modules/app/components/commonUiSync';
import roomClientModule from '../../../../modules/room/client/roomClient';
import apiClientModule from '../../../../modules/app/redux/api/client';
import notificationClientModule from '../../../../modules/notification/notificationClient';
import connection from '../../../../modules/connection/connectionClient';
import auth from '../../../../modules/auth/client/authCient';
import * as media from '../../../../modules/media/mediaClient';
import { webphoneClientModule } from '../../../../modules/webphone/client/webphoneClient';
import recordingActionCreators from '../../../../modules/recording/src/common/redux/actionCreators/actionCreators';
import recordingActionTypes from '../../../../modules/recording/src/common/redux/RecordingAction';
import RecordingsClient from '../../../../modules/recording/src/client/services/RecordingsClient/RecordingsClient';
import { actionCreators as presentationActionCreators } from '../../../../modules/presentations/client/presentationsClient';
import * as appSelectors from '../../../../modules/app/redux/selectors';
import icons from '../../../../modules/app/entities/icons';
import * as themes from '../../../../modules/app/entities/themes';
import competentumTheme from '../../../../modules/app/entities/themes/competentum';
import resolveUrl from 'resolve-url';
import ComponentProvider from '../ComponentProvider/ComponentProvider';
import ModuleProvider from '../ModuleProvider/ModuleProvider';
import createClientSocket from '../../../../modules/app/entities/clientSocket';
import { buildConfig } from '../../../../modules/app/entities/config';
import VideoPage from '../../../../client/routes/Video/index';
import RoomIsFull from '../../../../modules/app/components/RoomIsFull';
import { CodeEditor } from '../../../../modules/codeEditor/codeEditorClient';
import { Notepad } from '../../../../modules/notepad/notepadClient';
import browser from '../../../../modules/app/entities/browser';
import initStore from '../../../../modules/app/entities/store/store';
import * as chat from '../../../../modules/chat/client/chatClient';
import { appActionCreators } from '../../../../modules/app/redux/actionCreators/appActionCreators';
import FinalPage from '../../../../client/routes/FinalPage/FinalPage';
import {
  IAppStorageKeys,
  IComponents,
  IThemes,
  TClientSocketFactory,
  THistory,
  THistoryFactory,
  TInitializationOptions,
  TStore,
  TWebRTCServicesFactory,
  IBrowser,
  IClientLogger,
} from '../../types/interfaces';
import RoomJoinError from '../../../../modules/room/common/types/constants/RoomJoinError';
import { TCustomModuleWrapper } from '../ModuleProvider/types/IS2MModule';
import { ComponentRegistryKey } from '../ComponentProvider/types/ComponentRegistryKey';
import { ILoadableWrapper } from '../../components/LoadableWrapper/loadableWrapper';
import { IUserManager } from '../AuthProvider/types/client/AuthProvider.interfaces';
import { ClientRegistryKey } from './types/ClientRegistryKey';
import { AxiosInstance } from 'axios';
import { httpClient as buildHttpClient } from '../../../../server/utils/httpClient/httpClient';
import { IComponentProvider } from '../ComponentProvider/types/IComponentProvider';
import { IModuleProvider } from '../ModuleProvider/types/IModuleProvider';
import {
  IAudioContextExtended,
  IClientEntityProvider,
  IClientEntityProviderInternals,
  ISelectors,
} from './types/IClientEntityProvider';
import { IUserMediaService } from '../../../../modules/media/services/types/IUserMediaService';
import { TStoreFactory } from '../../../../modules/app/entities/store/types/TStoreFactory';
import createHistory from '../../../../modules/app/entities/history/history';
import { IAppConstants } from './types/IAppConstants';
import StorageManager from '../StorageManager/StorageManager';
import { TStorageFactory } from '../StorageManager/storages/types';
import { defaultStorageFactory } from '../StorageManager/storages/storageFactory';
import { ModuleRegistryKey } from '../ModuleProvider/types/ModuleRegistryKey';
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import { IAppRecordingPreviewConfig } from '../../types/IClientConfig';
import { FallbackProvider } from '../FallbackProvider/FallbackProvider';
import { IFallbackProvider } from '../FallbackProvider/types/IFallbackProvider';
import * as whiteboard from '../../../../modules/whiteboard/client/whiteboardClient';
import { UtilsProvider } from '../UtilsProvider/UtilsProvider';
import { IUtilsProvider } from '../UtilsProvider/IUtilsProvider';
import { TFallbackProviderRegistryKey } from '../FallbackProvider/types/TFallbackProviderRegistryKey';
import React from 'react';
import { APIAction, webrtcConstants } from '../../../common/ioc-constants';
import Socket = SocketIOClient.Socket;
import { IRecordingsClient } from '../../../../modules/recording/src/client/services/RecordingsClient/IRecordingsClient';
import { AppAction } from '../../../../modules/app/redux/types/AppAction';

class ClientEntityProvider extends BaseDependencyProvider<ClientRegistryKey> implements IClientEntityProviderInternals {
  public isInitialized: boolean = false;

  private currentInitializationOptions: TInitializationOptions = {};

  private static _instance: ClientEntityProvider;

  private constructor() {
    super();
  }

  public static createInstance(): IClientEntityProviderInternals {
    return this._instance || (this._instance = new this());
  }

  // bindings initialization, should be called on top of composition root
  public async initialize(initializationOptions: TInitializationOptions = {}): Promise<void> {
    if (this.isInitialized) {
      return Promise.resolve();
    }

    this.currentInitializationOptions = initializationOptions;
    this.bindUtils();
    const config = await this.bindConfig(initializationOptions.configFactory);
    const socket = await this.bindClientSocket(config, initializationOptions.socketFactory);

    const actionCreators = await this.bindActionCreators();
    const actionTypes = this.bindActionTypes();
    const store = await this.bindStore(
      socket,
      actionCreators,
      actionTypes,
      initializationOptions.storeEnhancerFactory,
      initializationOptions.storeFactory
    );
    const storageKeys = await this.bindStorageKeys();

    await this.bindStorageManager(storageKeys, store, initializationOptions.storageFactory);

    this.initSyncBindings(config);
    await this.bindUserMediaService();

    this.bindAppConstants();
    await this.bindUserManager(config);
    await this.bindHistory(initializationOptions.historyFactory);

    await this.bindLogger(socket);

    const fallbackProvider = this.bindFallbacks();

    this.bindModules(fallbackProvider, socket, initializationOptions.customModuleWrapper);
    this.bindComponents();
    this.isInitialized = true;
  }

  private async bindStorageManager(
    storageKeys: IAppStorageKeys,
    store: TStore,
    storageFactory: TStorageFactory = defaultStorageFactory
  ): Promise<StorageManager> {
    const storageManager = new StorageManager(storageKeys, store, storageFactory);

    this.setDependency<StorageManager>(ClientRegistryKey.StorageManager, storageManager);
    return storageManager;
  }

  private async bindUserManager(config: IClientConfig): Promise<IUserManager> {
    const createUserManager = (
      await import(
        /* webpackPrefetch: true, webpackChunkName: "lazy/ioc/providers/AuthProvider" */
        `../AuthProvider/AuthProvider`
      )
    ).default;
    const userManager = createUserManager();
    const servicePath = resolveUrl(config.service.url, config.service.path);

    userManager.loadConfig({ ...config.auth, localUrl: servicePath });

    this.setDependency<IUserManager>(ClientRegistryKey.UserManager, userManager);
    return userManager;
  }

  private async bindStorageKeys(): Promise<IAppStorageKeys> {
    const storageKeys = {
      ...chat.storageKeys,
      ...browser.browserStorageKeys,
      ...auth.storageKeys,
      ...webphoneClientModule.storageKeys,
      ...media.storageKeys,
    };

    this.setDependency<IAppStorageKeys>(ClientRegistryKey.StorageKeys, storageKeys);

    return storageKeys;
  }

  private async bindActionCreators(): Promise<IAppActionCreators> {
    const actionCreators: IAppActionCreators = {
      ...appActionCreators,
      ...chat.actionCreators,
      ...auth.actionCreators,
      ...connection.actionCreators,
      ...apiClientModule.actionCreators,
      ...roomClientModule.actionCreators,
      ...webphoneClientModule.actionCreators,
      ...media.actionCreators,
      ...notificationClientModule.actionCreators,
      ...recordingActionCreators,
      ...presentationActionCreators,
      ...whiteboard.actionCreators,
    };

    this.setDependency<IAppActionCreators>(ClientRegistryKey.ActionCreators, actionCreators);

    return actionCreators;
  }

  private async bindHistory(historyFactory: THistoryFactory = createHistory): Promise<THistory> {
    const history = historyFactory();

    this.setDependency<THistory>(ClientRegistryKey.History, history);
    return history;
  }

  private async bindLogger(socket: Socket) {
    const createLogger = (
      await import(
        /* webpackPrefetch: true, webpackChunkName: "lazy/ioc/modules/logger" */
        '../../../../modules/logger/client/loggerProvider'
      )
    ).default;
    const clientLogger = await createLogger(socket);

    this.setDependency<IClientLogger>(ClientRegistryKey.Logger, clientLogger);
  }

  private async bindStore(
    socket,
    actionCreators,
    actionTypes,
    storeEnhancerFactory,
    storeFactory: TStoreFactory = initStore
  ): Promise<TStore> {
    const store = await storeFactory(socket, actionCreators, actionTypes, storeEnhancerFactory);

    this.setDependency<TStore>(ClientRegistryKey.Store, store);

    return store;
  }

  private initSyncBindings(config: IClientConfig) {
    this.bindBrowser();

    this.setDependency<ICommonIcons>(ClientRegistryKey.Icons, icons);

    this.bindWebAudioContext();

    this.bindWebRTCServicesFactory();

    this.bindLoadedThemes();
    this.bindSelectors();

    const httpClient = this.bindHTTPClient();

    this.bindRecordingsClient(config.recordingPreview, httpClient);
  }

  private bindActionTypes(): IAppActionTypes {
    const actionTypes: IAppActionTypes = {
      ...chat.ChatAction,
      ...auth.actionTypes,
      ...connection.actionTypes,
      ...APIAction,
      ...roomClientModule.actionTypes,
      ...webphoneClientModule.actionTypes,
      ...media.actionTypes,
      ...recordingActionTypes,
      ...AppAction,
      ...whiteboard.WhiteboardAction,
      ...notificationClientModule.actionTypes,
    };

    this.setDependency<IAppActionTypes>(ClientRegistryKey.ActionTypes, actionTypes);

    return actionTypes;
  }

  private bindBrowser() {
    this.setDependency<IBrowser>(ClientRegistryKey.Browser, browser.browserData());
  }

  private bindLoadedThemes() {
    const loadedThemes = {
      ...themes,
      Competentum: Object.assign({}, themes.defaultTheme, competentumTheme),
    };

    this.setDependency<IThemes>(ClientRegistryKey.Themes, loadedThemes);
  }

  private bindSelectors() {
    const iocSelectors: ISelectors = {
      chatSelectors: chat.selectors,
      appSelectors,
      webphoneSelectors: webphoneClientModule.selectors,
    };

    this.setDependency<ISelectors>(ClientRegistryKey.Selectors, iocSelectors);
  }

  private bindUtils() {
    const utilsProvider = new UtilsProvider();

    this.setDependency(ClientRegistryKey.Utils, utilsProvider);
  }

  private bindFallbacks(): IFallbackProvider {
    const fallbackProvider = new FallbackProvider();

    fallbackProvider.bindFallbacks();

    this.setDependency(ClientRegistryKey.Fallbacks, fallbackProvider);
    return fallbackProvider;
  }

  private bindModules(
    fallbackProvider: IFallbackProvider,
    clientSocket: Socket,
    customModuleWrapper?: TCustomModuleWrapper
  ) {
    const moduleProvider: ModuleProvider = new ModuleProvider(fallbackProvider, clientSocket, customModuleWrapper);

    moduleProvider.bindLazyModules();

    this.setDependency<IModuleProvider>(ClientRegistryKey.Modules, moduleProvider);
  }

  private bindComponents() {
    const componentProvider: ComponentProvider = new ComponentProvider();

    /**
     * @TODO:
     * This setter are using for backward compatibility and should be removed with sync module to lazy rewrite
     * Several component imports are referencing the entities before their declaration inside import resolution code
     * to avoid it currently we should control the dependency graph using composition root.
     */
    componentProvider.bindSyncComponents({
      WebPhone: webphoneClientModule.ui,
      UI: componentsLibrary,
      Auth: auth.ui,
      CodeEditor,
      Notepad,
      Media: media.ui,
      RoomIsFull,
      FinalPage,
      VideoPage,
    });

    // currently some lazy components are fallback to sync one, so we need to bind them after the sync one
    componentProvider.bindLazyComponents();

    this.setDependency<IComponents & IComponentProvider>(ClientRegistryKey.UI, componentProvider);
  }

  private bindWebRTCServicesFactory() {
    this.setDependency(
      ClientRegistryKey.WebrtcServicesFactory,
      async () =>
        (
          await import(
            /* webpackPrefetch: true, webpackChunkName: "lazy/ioc/webrtcServices" */
            `../../../../modules/webrtc/client/services/webrtcServicesFactory`
          )
        ).default
    );
  }

  private async bindClientSocket(
    config: IClientConfig,
    socketFactory: TClientSocketFactory = this.buildClientSocket
  ): Promise<Socket> {
    const socket: Socket = await socketFactory(config);

    this.setDependency<Socket>(ClientRegistryKey.Socket, socket);
    return socket;
  }

  private buildClientSocket(config): Promise<Socket> {
    return Promise.resolve(createClientSocket(config));
  }

  private async bindConfig(configBuilder: () => Promise<IClientConfig> = buildConfig): Promise<IClientConfig> {
    const config: IClientConfig = await configBuilder();

    this.setDependency<IClientConfig>(ClientRegistryKey.Config, config);
    return config;
  }

  private bindWebAudioContext() {
    // https://stackoverflow.com/questions/45528945/audiocontext-samplerate-returning-null-after-being-read-8-times
    const webAudioContext: IAudioContextExtended = new ((window as any).AudioContext ||
      (window as any).webkitAudioContext)();

    this.setDependency<IAudioContextExtended>(ClientRegistryKey.WebAudioContext, webAudioContext);
  }

  private bindAppConstants(): IAppConstants {
    const constants: IAppConstants = {
      webrtcConstants,
      connection: connection.constants,
      RoomJoinError,
    };

    this.setDependency<IAppConstants>(ClientRegistryKey.Constants, constants);

    return constants;
  }

  private bindHTTPClient(): AxiosInstance {
    const client = buildHttpClient();

    this.setDependency<AxiosInstance>(ClientRegistryKey.HTTPClient, client);

    return client;
  }

  private bindRecordingsClient(config: IAppRecordingPreviewConfig, httpClient: AxiosInstance) {
    this.setDependency<IRecordingsClient>(ClientRegistryKey.RecordingsClient, new RecordingsClient(config, httpClient));
  }

  // private getters
  private _getComponentProvider(): IComponentProvider {
    return this.getDependency<IComponentProvider>(ClientRegistryKey.UI);
  }

  private _getModuleProvider(): IModuleProvider {
    return this.getDependency<IModuleProvider>(ClientRegistryKey.Modules);
  }

  public getFallbackProvider(): IFallbackProvider {
    return this.getDependency<IFallbackProvider>(ClientRegistryKey.Fallbacks);
  }

  public getUtils(): IUtilsProvider {
    return this.getDependency<IUtilsProvider>(ClientRegistryKey.Utils);
  }

  private async bindUserMediaService() {
    const UserMediaService = (
      await import(
        /* webpackPrefetch: true, webpackChunkName: "lazy/ioc/UserMediaService" */
        '../../../../modules/media/services'
      )
    ).default;

    this.setDependency(ClientRegistryKey.UserMediaService, new UserMediaService());
  }

  public async destroy(): Promise<void> {
    if (!this.isInitialized) {
      return;
    }
    const { onDestroyStart, onRecycleComplete } = this.currentInitializationOptions;

    if (onDestroyStart) {
      onDestroyStart();
    }

    this._getComponentProvider().destroy();
    this._getModuleProvider().destroy();
    this.getFallbackProvider().destroy();
    (this.getUtils() as UtilsProvider).destroy();
    super.destroy();
    this.isInitialized = false;

    if (onRecycleComplete) {
      onRecycleComplete();
    }
  }

  public getThemes(): IThemes {
    return this.getDependency<IThemes>(ClientRegistryKey.Themes);
  }

  public getBrowser(): IBrowser {
    return this.getDependency<IBrowser>(ClientRegistryKey.Browser);
  }

  public getIcons(): ICommonIcons {
    return this.getDependency<ICommonIcons>(ClientRegistryKey.Icons);
  }

  public getSelectors(): ISelectors {
    return this.getDependency<ISelectors>(ClientRegistryKey.Selectors);
  }

  public getSocket(): Socket {
    return this.getDependency<Socket>(ClientRegistryKey.Socket);
  }

  public getConfig(): IClientConfig {
    return this.getDependency<IClientConfig>(ClientRegistryKey.Config);
  }

  public getStore(): TStore {
    return this.getDependency<TStore>(ClientRegistryKey.Store);
  }

  public getHistory(): THistory {
    return this.getDependency<THistory>(ClientRegistryKey.History);
  }

  public getLogger(): IClientLogger {
    return this.getDependency<IClientLogger>(ClientRegistryKey.Logger);
  }

  public getWebAudioContext(): IAudioContextExtended {
    return this.getDependency<IAudioContextExtended>(ClientRegistryKey.WebAudioContext);
  }

  public getAppActionCreators(): IAppActionCreators {
    return this.getDependency<IAppActionCreators>(ClientRegistryKey.ActionCreators);
  }

  public getStorageKeys(): IAppStorageKeys {
    return this.getDependency<IAppStorageKeys>(ClientRegistryKey.StorageKeys);
  }

  public getStorageManager(): StorageManager {
    return this.getDependency<StorageManager>(ClientRegistryKey.StorageManager);
  }

  public getUserManager(): IUserManager {
    return this.getDependency<IUserManager>(ClientRegistryKey.UserManager);
  }

  public getHttpClient(): AxiosInstance {
    return this.getDependency<AxiosInstance>(ClientRegistryKey.HTTPClient);
  }

  public getRecordingsClient(): IRecordingsClient {
    return this.getDependency<IRecordingsClient>(ClientRegistryKey.RecordingsClient);
  }

  public resolveFallbackJSX(key: TFallbackProviderRegistryKey): JSX.Element {
    return this.getFallbackProvider().resolveFallbackJSX(key);
  }

  public resolveFallback<FallbackProps>(key: TFallbackProviderRegistryKey): React.ComponentType<FallbackProps> {
    return this.getFallbackProvider().resolveFallback<FallbackProps>(key);
  }

  // proxy shorthands
  public resolveComponent<P = {}>(componentKey: ComponentRegistryKey): ILoadableWrapper<P> {
    return this._getComponentProvider().resolveComponent<P>(componentKey);
  }
  public resolveComponentSuspended<P = {}>(
    componentKey: ComponentRegistryKey,
    fallback: JSX.Element = this.resolveFallbackJSX(componentKey)
  ): ILoadableWrapper<P> {
    return this._getComponentProvider().resolveComponentSuspended<P>(componentKey, fallback);
  }

  public resolveModuleSuspended<P = {}>(moduleKey: ModuleRegistryKey, fallback?: JSX.Element): ILoadableWrapper<P>;
  public resolveModuleSuspended<P = {}, Options = {}>(
    moduleKey: ModuleRegistryKey,
    moduleFactoryInitOptions: Options,
    fallback?: JSX.Element
  ): ILoadableWrapper<P> {
    const moduleProvider = this._getModuleProvider();

    return moduleProvider.resolveModuleSuspended(moduleKey, moduleFactoryInitOptions, fallback);
  }

  /**
   * @deprecated should use getAppActionCreators instead
   */
  public getActionCreators(): IAppActionCreators {
    return this.getAppActionCreators();
  }

  public getAppActionTypes(): IAppActionTypes {
    return this.getDependency<IAppActionTypes>(ClientRegistryKey.ActionTypes);
  }

  /**
   * @deprecated: this method is used only for backward compatibility purposes and would be removed in future.
   * Use `clientEntityProvider.resolveComponentSuspended` instead.
   */
  public getComponentProvider(): IComponentProvider & IComponents {
    return this._getComponentProvider() as IComponentProvider & IComponents;
  }

  public getModuleProvider(): IModuleProvider {
    return this._getModuleProvider();
  }

  public getUserMediaService(): IUserMediaService {
    return this.getDependency<IUserMediaService>(ClientRegistryKey.UserMediaService);
  }

  private getWebRTCServicesFactory(): TWebRTCServicesFactory {
    return this.getDependency<TWebRTCServicesFactory>(ClientRegistryKey.WebrtcServicesFactory);
  }

  public async getWebRTCServices(): Promise<TWebRTCServices> {
    try {
      return this.getDependency<TWebRTCServices>(ClientRegistryKey.WebrtcServices);
    } catch (e) {
      if (e.message?.match(/No matching bindings found for serviceIdentifier:.*/)) {
        const webrtcServicesFactory = await this.getWebRTCServicesFactory()();
        const webrtcServices = await webrtcServicesFactory();

        this.setDependency(ClientRegistryKey.WebrtcServices, webrtcServices);

        return webrtcServices;
      } else {
        throw e;
      }
    }
  }

  public getConstants(): IAppConstants {
    return this.getDependency<IAppConstants>(ClientRegistryKey.Constants);
  }

  // modules
  get appActionCreators(): IAppActionCreators {
    return this.getAppActionCreators();
  }

  get constants() {
    return this.getConstants();
  }

  get defaultTheme() {
    return this.getDependency<IThemes>(ClientRegistryKey.Themes).defaultTheme;
  }

  get config() {
    return this.getConfig();
  }

  get webAudioContext() {
    return this.getWebAudioContext();
  }
}

const clientEntityProvider: IClientEntityProvider = ClientEntityProvider.createInstance();

export default clientEntityProvider;
