import setupIoCClientForTest from '../../../../test/utils/ioc/testIoCClient';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from './types/IClientEntityProvider';
import MemoryStorage from '../StorageManager/storages/MemoryStorage/MemoryStorage';
import StorageManager from '../StorageManager/StorageManager';

describe('ClientEntityProvider', () => {
  beforeEach(async (done) => {
    jest.restoreAllMocks();
    jest.clearAllMocks();
    await setupIoCClientForTest()(done);
  });

  it('ClientEntityProvider should unbind entities on destroy call', async (done) => {
    const storageManager = clientEntityProvider.getStorageManager();
    const memoryStorage = MemoryStorage.createInstance();
    const config = clientEntityProvider.getConfig();
    const componentProvider = clientEntityProvider.getComponentProvider();
    const moduleProvider = clientEntityProvider.getModuleProvider();

    const componentProviderDestroy = jest.spyOn(componentProvider, 'destroy');
    const moduleProviderDestroy = jest.spyOn(moduleProvider, 'destroy');

    expect(config).toBeDefined();

    storageManager.set<string>({ key: 'test' }, 'value');
    expect(memoryStorage.getItem(`${StorageManager.PREFIX}:test`)).toBe('value');

    await clientEntityProvider.destroy();

    expect(componentProviderDestroy).toBeCalledTimes(1);
    expect(moduleProviderDestroy).toBeCalledTimes(1);

    expect(() => clientEntityProvider.getConfig()).toThrowError(/No matching bindings found for serviceIdentifier:.*/);
    expect(memoryStorage.getItem(`${StorageManager.PREFIX}:test`)).toBeNull();
    done();
  });

  it('ClientEntityProvider should correctly reinit after recycle', async (done) => {
    const clientEntityProviderInternals: IClientEntityProviderInternals = clientEntityProvider as IClientEntityProviderInternals;

    await clientEntityProviderInternals.destroy();

    expect(() => clientEntityProviderInternals.getConfig()).toThrowError(
      /No matching bindings found for serviceIdentifier:.*/
    );

    await setupIoCClientForTest()();

    expect(clientEntityProviderInternals.isInitialized).toBeTruthy();
    expect(clientEntityProviderInternals.getConfig()).toBeDefined();

    done();
  });
  it('should call lifecycle functions', async (done) => {
    const clientEntityProviderInternals: IClientEntityProviderInternals = clientEntityProvider as IClientEntityProviderInternals;

    await clientEntityProviderInternals.destroy();

    const onDestroyStart = jest.fn();
    const onRecycleComplete = jest.fn();

    await setupIoCClientForTest({ onDestroyStart, onRecycleComplete })();
    await clientEntityProvider.destroy();

    expect(onDestroyStart).toBeCalledTimes(1);
    expect(onRecycleComplete).toBeCalledTimes(1);
    jest.clearAllMocks();

    await clientEntityProvider.destroy();
    expect(onDestroyStart).toBeCalledTimes(0);
    done();
  });

  it('public getters', async (done) => {
    const clientEntityProviderInternals: IClientEntityProviderInternals = clientEntityProvider as IClientEntityProviderInternals;

    const services = await clientEntityProviderInternals.getWebRTCServices();
    const themes = clientEntityProviderInternals.getThemes();
    const browser = clientEntityProviderInternals.getBrowser();
    const icons = clientEntityProviderInternals.getIcons();
    const selectors = clientEntityProviderInternals.getSelectors();
    const socket = clientEntityProviderInternals.getSocket();
    const config = clientEntityProviderInternals.getConfig();
    const store = clientEntityProviderInternals.getStore();
    const history = clientEntityProviderInternals.getHistory();
    const logger = clientEntityProviderInternals.getLogger();
    const webAudioContext = clientEntityProviderInternals.getWebAudioContext();
    const appActionCreators = clientEntityProviderInternals.getActionCreators();
    const storageKeys = clientEntityProviderInternals.getStorageKeys();
    const storageManager = clientEntityProviderInternals.getStorageManager();
    const userManager = clientEntityProviderInternals.getUserManager();
    const httpClient = clientEntityProviderInternals.getHttpClient();
    const recordingClient = clientEntityProviderInternals.getRecordingsClient();
    const actionTypes = clientEntityProviderInternals.getAppActionTypes();
    const userMediaService = clientEntityProviderInternals.getUserMediaService();
    const utils = clientEntityProviderInternals.getUtils();
    const constants = clientEntityProviderInternals.getConstants();

    expect(services).toHaveProperty('p2p');
    expect(themes).toHaveProperty('defaultTheme');
    expect(browser).toHaveProperty('info');
    expect(icons).toHaveProperty('code');
    expect(selectors).toHaveProperty('appSelectors');
    expect(socket).toHaveProperty('connected');
    expect(config).toHaveProperty('service');
    expect(store).toHaveProperty('dispatch');
    expect(history).toHaveProperty('location');
    expect(logger).toHaveProperty('recordingCapture');
    expect(webAudioContext).toHaveProperty('destination');
    expect(appActionCreators).toHaveProperty('initAuthModule');
    expect(storageKeys).toHaveProperty('AUTH_TOKEN_KEY');
    expect(storageManager).toHaveProperty('get');
    expect(userManager).toHaveProperty('getUser');
    expect(recordingClient).toHaveProperty('downloadRecord');
    expect(httpClient).toHaveProperty('post');
    expect(actionTypes).toHaveProperty('SEND');
    expect(userMediaService).toBeDefined();
    expect(utils).toHaveProperty('isReduxDynamicModulesAction');
    expect(utils).toHaveProperty('sleep');
    expect(constants).toHaveProperty('webrtcConstants');

    done();
  });
});
