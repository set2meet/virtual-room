/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { LoginStrategies } from '../constants';

/**
 * @todo unite with TToken from resource-management package
 * @internal WARN: avoid using it directly! Just proxy needed types though client/server realizations.
 */
export type TAuthTokens = {
  accessToken: string;
  refreshToken: string;
  expiresIn?: number;
  refreshExpiresIn?: number;
  strategy: LoginStrategies;
};

/**
 * @todo unite with TUserInfo from resource-management package
 * @internal WARN: avoid using it directly! Just proxy needed types though client/server realizations.
 */
export type TRoomUser = {
  id: any;
  email: string;
  guest: boolean;
  roomId: string;
  displayName: string;
  realmName?: string;
  picture?: string;
};
