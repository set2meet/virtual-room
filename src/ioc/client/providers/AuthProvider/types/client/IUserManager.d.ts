/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { IAuthProvider, TRoomUser, TUser } from './AuthProvider.interfaces';
import { IAuthConfig } from '../../../../../common/types/config/IAuthConfig';

export interface IUserManager {
  AuthProvider: IAuthProvider;
  loadConfig(data: IAuthConfig): IUserManager;
  getAuthFromStorage(): Promise<boolean>;
  getUser(): TRoomUser;
  checkSSO(): Promise<any>;
  loginSSO(): Promise<boolean>;
  logout(): void;
  loginForm(
    data: { username: string; password: string; realm?: string; guestMode?: boolean },
    callback?: any
  ): Promise<any>;
  getFullUser(): TUser;
  loginWithToken(): Promise<boolean>;
  loginAsGuest(name?: string, fakeGuest?: false): Promise<TUser>;
}
