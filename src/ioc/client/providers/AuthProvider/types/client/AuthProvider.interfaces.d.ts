/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TAuthTokens, TRoomUser } from '../common/TAuthTokens';

// re-export for client code usage
export { TAuthTokens, TRoomUser };

export type TUser = {
  user: TRoomUser;
  tokens: TAuthTokens;
};

export type IFormLogin = {
  loginUrl: string;
  username: string;
  password: string;
  realm: string;
  guestMode: any;
  strategy: any;
};

export interface IAdapterProps {
  realm: string;
  url: string;
  clientId: string;
}

export interface IAdapter {
  initialized: boolean;
  authenticated?: boolean;
  token?: string; // accessToken
  refreshToken?: string;

  init(object: any): Promise<any>;

  updateToken(): Promise<any>;

  loadUserInfo(): Promise<any>;
}

export interface IAuthProvider {
  getStrategies: () => any;
}

export { IUserManager } from './IUserManager';
