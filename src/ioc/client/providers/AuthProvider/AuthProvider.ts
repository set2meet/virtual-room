/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Keycloak, { KeycloakInstance } from 'keycloak-js';
import resolveUrl from 'resolve-url';
import jwt from 'jsonwebtoken';
import parseToken from '@s2m/resource-management/lib/utils/parseToken';
import publicKey from '../../../../modules/auth/server/keycloack/keys';
import { Md5 } from 'ts-md5';
import { IClientConfig } from '../../../common/ioc-interfaces';
import {
  IAdapter,
  IAdapterProps,
  IAuthProvider,
  IUserManager,
  TAuthTokens,
  TRoomUser,
  TUser,
} from './types/client/AuthProvider.interfaces';
import clientEntityProvider from '../ClientEntityProvider/ClientEntityProvider';
import { LoginStrategies } from './types/constants';
import { IAuthConfig } from '../../../common/types/config/IAuthConfig';

interface IUserInfoResponse {
  IAM_user_id: string;
  name: string;
  sub: string;
  email: string;
  picture: string | null;
}

/**
 * keycloak instance contain onLoad event, but IAdapter not. To detect wrong behavior need to check prop 'onLoad'
 * If you're trying to use wrong adapter for ex;
 */
interface IConfigRequire extends Pick<IClientConfig, 'auth'> {
  localUrl: string;
  onLoad?: () => {};
}

enum KeycloakActionsOnLoad {
  login = 'login-required',
  check = 'check-sso',
}

/**
 * Class form adapter is type of adapter which one used while strategy is 'form'.
 * Its provide all required methods and collecting all required data
 * Shouldn't be used directly, only as property of CAuthManager;
 * All props pass in while adapter initialized by CAuthManager;
 * Singleton. But the way i meant it to use you shouldn care about multiple instances;
 */
class CFormAdapter implements IAdapter {
  private realm: string;
  private url: string;
  private clientId: string;

  public initialized: boolean;
  public token: string;
  public refreshToken: string;

  constructor({ realm, url, clientId }: IAdapterProps) {
    this.realm = realm;
    this.url = url;
    this.clientId = clientId;
    this.initialized = true;
  }

  public init(data: IConfigRequire): Promise<any> {
    if (data && data.onLoad && data.onLoad.length > 0) {
      throw new Error('Seems you`re using wrong adapter for selected auth method');
    }
    const httpClient = clientEntityProvider.getHttpClient();

    return httpClient
      .post(this.url, data)
      .then((resp) => resp.data)
      .catch((e) => {
        throw new Error(`${e.toString()}`);
      });
  }

  /**
   * Function will send data to updateToken;
   * WIP
   */
  public async updateToken() {
    return true;
  }

  /**
   * Get User Info
   * WIP
   */
  public async loadUserInfo(): Promise<any> {
    return true;
  }

  public getAuthData(): object {
    return { realm: this.realm, clientId: this.clientId };
  }
}

/**
 * Encapsulates auth required data from config and return ready-to-work authProvider instance;
 * @return self
 * Future props: setNewRealm, reloadRealm, getAltRealm
 */
class CAuthProvider implements IAuthProvider {
  private adapter: KeycloakInstance | IAdapter;
  private authConfig: IAuthConfig;
  private realmlist: any; // TODO::type later

  /**
   * Use init to configure all props
   * @param appConfig
   */
  public init(appConfig: IAuthConfig): CAuthProvider {
    if (!appConfig.realm || !appConfig.strategy || !appConfig.realmlist) {
      throw new Error('Error: No default realm and strategy provided in config. ');
    }

    this.setDefaultRealmConfig(appConfig);
    return this;
  }

  /**
   * Setting default props while initialization;
   * @param appConfig
   */
  public setDefaultRealmConfig(appConfig: IAuthConfig): void {
    this.authConfig = { ...appConfig };
    this.realmlist = this.authConfig.realmlist;
    // this.realmConfig = this.authConfig.realmlist[this.strategy][0];
    // this.clientId = this.realmConfig.clientId;
  }

  public getStrategies() {
    if (this.realmlist) {
      return Object.keys(this.realmlist);
    }
  }

  /**
   * Create instance of AuthAdapter. Its possible to init any adapter, not kk only
   * @default Keycloak
   */
  private initAdapter(strategy: LoginStrategies): KeycloakInstance | IAdapter {
    // hack to get first realm as 'default'
    const defaultRealm = Object.keys(this.authConfig.realmlist[strategy])[0];
    const realmConfig = this.realmlist[strategy][defaultRealm];

    switch (strategy) {
      case LoginStrategies.STRATEGY_FORM:
        this.adapter = new CFormAdapter({
          url: resolveUrl(this.authConfig.localUrl, 'login'), // cuz we use our server-side login to kk as its should continue on server
          clientId: realmConfig.resource,
          realm: defaultRealm,
        });
        break;
      case LoginStrategies.STRATEGY_SSO:
        this.adapter = Keycloak({
          url: realmConfig.url,
          clientId: realmConfig.clientId,
          realm: defaultRealm,
        });
    }

    return this.adapter;
  }

  /**
   * Method to sync userId encoded in JWT tokens with backend;
   * @param tokens
   */
  public syncBackend(tokens: TAuthTokens): Promise<any> {
    const url = resolveUrl(this.authConfig.localUrl, 'validate-token');
    const httpClient = clientEntityProvider.getHttpClient();

    return httpClient
      .post(url, { tokens })
      .then(() => {
        return true;
      })
      .catch((e) => {
        console.log('failed to access validate route');
        return false;
      });
  }

  /**
   * Get adapter instance;
   */
  public getAdapter(strategy?: LoginStrategies): KeycloakInstance | IAdapter {
    let adapter;

    if (strategy) {
      adapter = this.initAdapter(strategy);
    } else {
      adapter = this.adapter;
    }

    if (adapter.hasOwnProperty('onLoad')) {
      return adapter as KeycloakInstance;
    } else {
      return adapter as IAdapter;
    }
  }

  /**
   * Create jwt-encoded key-pair, encoded by our publicKey;
   * @param data
   */
  public signJwt(data: object | string): string {
    const secret = publicKey;

    return jwt.sign(data, secret);
  }

  public logout(): void {
    this.reset();
  }

  /**
   * Reset strategy;
   * Mostly required for logOut chain;
   */
  public reset(): void {
    this.adapter = null;
  }
}

class CUserManager implements IUserManager {
  public AuthProvider: CAuthProvider;
  private user: TRoomUser;
  private tokens: TAuthTokens;

  // tslint:disable-next-line:no-shadowed-variable
  constructor(AuthProvider: CAuthProvider) {
    this.AuthProvider = AuthProvider;
  }

  /**
   * Initializing adapter. As i meant before, you shouldn't use adapters directly. So this method prepares AuthAdapter
   * and fill required data
   * @param data
   */
  public loadConfig(data: IAuthConfig): CUserManager {
    this.AuthProvider = this.AuthProvider.init(data);
    return this;
  }

  public isGuest() {
    return this.user.guest;
  }

  /**
   * Providing Login via Form strategy;
   * All login operations should be through AuthProvider;
   * UserManger not responsible for auth operations!
   * @param data
   * @param callback
   */
  public async loginForm(
    data: { username: string; password: string; realm?: string; guestMode?: boolean },
    callback?: any
  ): Promise<any> {
    const adapter = this.AuthProvider.getAdapter(LoginStrategies.STRATEGY_FORM);

    return (
      adapter
        // @ts-ignore
        .init({ ...data, realm: adapter.realm, strategy: LoginStrategies.STRATEGY_FORM })
        .then((resp: { user: TRoomUser; tokens: TAuthTokens }) => {
          this.user = resp.user;
          this.tokens = { ...resp.tokens, strategy: LoginStrategies.STRATEGY_FORM };
          this.saveAuthToStorage();
          return true;
        })
        .catch((error: any) => {
          throw new Error(error);
        })
    );
  }

  /**
   * Login through sso strategy. Doesnt used for now 19.03.20 but will later.
   */
  public loginSSO(): Promise<boolean> {
    if (this.AuthProvider) {
      const adapter = this.AuthProvider.getAdapter(LoginStrategies.STRATEGY_SSO);

      return adapter
        .init({ onLoad: KeycloakActionsOnLoad.login })
        .then((authenticated: boolean) => {
          if (authenticated) {
            return adapter
              .loadUserInfo()
              .then((userInfo: IUserInfoResponse) => {
                const parsedToken = parseToken(adapter.token);
                const CDN_HOST = 'https://static.cdn.epam.com/avatar/';
                const LARGE = 'large';
                const hash = Md5.hashStr(`${LARGE}_${userInfo.IAM_user_id}`);
                const url = CDN_HOST + hash + '.jpg';
                let picture;

                try {
                  picture = userInfo.email.includes('@epam.com') ? url : userInfo.picture;
                } catch (e) {
                  picture = null;
                }

                this.user = {
                  displayName: userInfo.name,
                  guest: false,
                  id: userInfo.sub,
                  email: userInfo.email,
                  roomId: userInfo.sub,
                  picture,
                  realmName: parsedToken.realmName,
                };

                this.tokens = {
                  accessToken: adapter.token,
                  refreshToken: adapter.refreshToken,
                  strategy: LoginStrategies.STRATEGY_SSO,
                };
                this.saveAuthToStorage();
                return true;
              })
              .catch((e: any) => {
                console.log('Keycloak: Login faild');
                return false;
              });
          }
        })
        .catch((e: string) => {
          console.log(e.toString());
          return false;
        });
    }
  }

  /**
   * Save current data to session/localstorage;
   */
  public saveAuthToStorage(): void {
    const { AUTH_TOKEN_KEY, AUTH_USER_KEY } = clientEntityProvider.getStorageKeys();
    const storageManager = clientEntityProvider.getStorageManager();

    storageManager.set(AUTH_USER_KEY, this.user);
    storageManager.set(AUTH_TOKEN_KEY, this.tokens);
  }

  /**
   * Load data from storage and set as current;
   */
  public async getAuthFromStorage(): Promise<boolean> {
    const { AUTH_TOKEN_KEY, AUTH_USER_KEY } = clientEntityProvider.getStorageKeys();
    const user = clientEntityProvider.getStorageManager().get(AUTH_USER_KEY);
    const tokens = clientEntityProvider.getStorageManager().get(AUTH_TOKEN_KEY);

    if (user && Object.keys(user).length > 0 && tokens && Object.keys(tokens).length > 0) {
      this.user = user;
      this.tokens = tokens;
      return true;
    } else {
      console.log('Session user is empty... Required re-login');
      return false;
    }
  }

  public checkSSO() {
    const strategy = this.tokens.strategy;

    if (strategy === LoginStrategies.STRATEGY_SSO) {
      const adapter = this.AuthProvider.getAdapter(strategy);

      return adapter
        .init({
          onLoad: KeycloakActionsOnLoad.check,
          token: this.tokens.accessToken,
          refreshToken: this.tokens.refreshToken,
        })
        .then((e: any) => {
          if (e) {
            console.log('Keycloak: Re-login success'); // TODO:remove later
            const accessToken = adapter.token;
            const refreshToken = adapter.refreshToken;

            this.setTokens({ accessToken, refreshToken, strategy });
          }
        })
        .catch((e) => console.log(e));
    }

    // fake promise gag for future features and old compatibility
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }

  /**
   * Applying while performed login after closed tab/refresh page etc.
   * Taking stored on local/session storage tokens and return fake true to avoid token validation at server
   * If not guest, continue full validation cycle;
   */
  public async loginWithToken(): Promise<boolean> {
    const { strategy } = this.getTokens();

    if (strategy && strategy === LoginStrategies.STRATEGY_GUEST) {
      return true;
    }

    return this.AuthProvider.syncBackend(this.tokens);
  }

  public createGuest(name?: string, fakeGuest?: boolean): TUser {
    const user = {
      displayName: name || 'Guest' + Math.ceil(Math.random() * 100),
      email: '',
      guest: !fakeGuest,
      roomId: `${Md5.hashStr(Math.random().toString(10))}`,
      id: `${Md5.hashStr(Math.random().toString(10))}`,
    };

    const tokens = {
      refreshToken: this.AuthProvider.signJwt(user),
      accessToken: this.AuthProvider.signJwt(user),
      strategy: LoginStrategies.STRATEGY_GUEST,
    };

    return { user, tokens };
  }

  /**
   * Login as guest method. By default, it will check local/session storage and then fill required data into UserManager;
   */
  public async loginAsGuest(name?: string, fakeGuest?: false) {
    if (!this.user && !this.tokens) {
      const { user, tokens } = this.createGuest(name, fakeGuest);

      this.user = user;
      this.tokens = tokens;

      this.saveAuthToStorage();
    }

    return this.getFullUser();
  }

  public getFullUser(): TUser & { strategy: string } {
    return { user: this.user, tokens: this.tokens, strategy: this.tokens.strategy };
  }

  public getUser(): TRoomUser {
    return this.user;
  }

  public setTokens(tokens: TAuthTokens): void {
    this.tokens.accessToken = tokens.accessToken;
    this.tokens.refreshToken = tokens.refreshToken;
    this.tokens.strategy = tokens.strategy;

    this.saveAuthToStorage();
  }

  /**
   * Make separate tokens/strategy getters instead of store them in one entity
   * @deprecated;
   */
  public getTokens(): TAuthTokens {
    return this.tokens;
  }

  public wipeUser() {
    this.user = null;
    this.tokens = null;
    clientEntityProvider.getStorageManager().removeAll();
  }

  /**
   * Logout method.
   * Also could be used for wipe data and clear all initialized adapters etc;
   */
  public logout(): void {
    this.AuthProvider.logout();

    this.wipeUser();
  }
}

/**
 * @TODO: this should be splitted into multiple files (adapter, provider, usermanager and manager factory)
 */
export default function createUserManager(): CUserManager {
  return new CUserManager(new CAuthProvider());
}
