import React from 'react';

export interface ILoadableContext {
  loadablePromises: Array<Promise<React.ReactNode>>;
  pushNewPromise: (promise: Promise<React.Component>) => void;
}

/**
 * Context for LoadableWrapper that contains arr with Promises. And func for pushing this promises
 */
export const LoadableContext = React.createContext<ILoadableContext>({
  loadablePromises: [],
  pushNewPromise: () => undefined,
});
