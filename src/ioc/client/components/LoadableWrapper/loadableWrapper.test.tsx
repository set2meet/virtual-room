import React from 'react';
import loadableWrapper, { ILoadableWrapper } from './loadableWrapper';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { enzymeCleanup } from '../../../../test/utils/utils';
import { LoadableContext } from './loadableContext';
import { sleep } from '../../../../modules/app/utils/utils';

afterEach(enzymeCleanup);

describe('LoadableWrapper', () => {
  const loadFn = async () => {
    await sleep();
    return {
      default: () => <h2>123</h2>,
    };
  };
  const TestingFallback = () => <h2>Loading...</h2>;

  it('Loadable wrapper should support direct fallback switch', async (done) => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn);
    const Loadable2: ILoadableWrapper = loadableWrapper(loadFn);

    Loadable.setFallback(<TestingFallback />);
    expect(Loadable.initialFallback).toEqual(<TestingFallback />);
    expect(Loadable2.initialFallback).toBeUndefined();

    const loadableComponent = EnzymeMountTracker.mount(<Loadable />);

    expect(loadableComponent.find('LoadFallbackWrapper').children().length).toBe(1);

    expect(loadableComponent.exists(TestingFallback)).toBeTruthy();
    done();
  });
  it('it should render nothing if fallback is not provided', () => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn);
    const loadableComponent = EnzymeMountTracker.mount(<Loadable />);

    expect(loadableComponent.find('LoadFallbackWrapper').children().length).toBe(0);
  });
  it('Loadable wrapper should support initial module options switch', async (done) => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn);
    const Loadable2: ILoadableWrapper = loadableWrapper(loadFn);

    Loadable.setModuleInitialOptions({});
    expect(Loadable.moduleFactoryInitOptions).toEqual({});
    expect(Loadable2.moduleFactoryInitOptions).toBeUndefined();

    done();
  });
  it('we could set fallback in options', () => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn, { fallback: <TestingFallback /> });

    expect(Loadable.initialFallback).toEqual(<TestingFallback />);

    const TestingFallback2 = () => <h2>Loading2...</h2>;

    Loadable.setFallback(<TestingFallback2 />);
    expect(Loadable.initialFallback).not.toEqual(<TestingFallback2 />);
  });
  it('should unmount LoadFallbackWrapper after resolve', async (done) => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn, { fallback: <TestingFallback /> });
    const loadableComponent = EnzymeMountTracker.mount(<Loadable />);

    expect(loadableComponent.find('LoadFallbackWrapper').length).toBe(1);
    await sleep(10);
    loadableComponent.update();
    expect(loadableComponent.find('LoadFallbackWrapper').length).toBe(0);
    done();
  });
  it('should add promise on load and unmount LoadFallbackWrapper after resolve', async (done) => {
    const Loadable: ILoadableWrapper = loadableWrapper(loadFn, { fallback: <TestingFallback /> });
    let loadablePromise: Promise<void> = null;
    const pushNewPromise = jest.fn((promise) => {
      loadablePromise = promise;
    });

    const loadableComponent = EnzymeMountTracker.mount(
      <LoadableContext.Provider value={{ pushNewPromise, loadablePromises: [] }}>
        <Loadable />
      </LoadableContext.Provider>
    );

    expect(pushNewPromise).toBeCalledTimes(1);
    expect(loadablePromise).not.toBeNull();

    expect(loadableComponent.find('LoadFallbackWrapper').length).toBe(1);
    await loadablePromise;
    loadableComponent.update();
    expect(loadableComponent.find('LoadFallbackWrapper').length).toBe(0);

    done();
  });
});
