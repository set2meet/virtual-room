import React, { ComponentClass, Context } from 'react';
import loadable, {
  DefaultComponent,
  LoadableComponentMethods,
  OptionsWithoutResolver,
  OptionsWithResolver,
} from '@loadable/component';
import { ILoadableContext, LoadableContext } from './loadableContext';

/**
 * Wrapper for async components. If it runs in test environment it pushes Promises in array from Context.
 * For cascade rendering for async testing.
 * @TODO: need to solve following gaps
 * * it should also support lib load approach
 * * it should be rewritten to onload callback usage when it gets merged
 * * it should fix the LoadableComponentMethods implementation typings issue (:63)
 * https://github.com/gregberge/loadable-components/pull/619
 */

export interface IDefaultLoadableWrapperProps {
  fallback?: JSX.Element;
  moduleFactoryInitOptions?: {};
}

interface ILoadableComponentMethodsExt {
  setFallback(fallback?: JSX.Element): void;
  initialFallback: JSX.Element;
  setModuleInitialOptions(moduleFactoryInitOptions: {}): void;
  moduleFactoryInitOptions: {};
}

export interface ILoadableWrapper<Props = {}>
  extends ComponentClass<Props & IDefaultLoadableWrapperProps>,
    LoadableComponentMethods<Props>,
    ILoadableComponentMethodsExt {}

export default function loadableWrapper<Props>(
  loadFn: (props: Props) => Promise<DefaultComponent<Props>>,
  options?: OptionsWithoutResolver<Props>
): ILoadableWrapper<Props>;
export default function loadableWrapper<Props, Module = DefaultComponent<Props>>(
  loadFn: (props: Props) => Promise<Module>,
  options: OptionsWithResolver<Props, Module>
): ILoadableWrapper<Props> {
  const BasicLoadableComponent = loadable<Props & IDefaultLoadableWrapperProps, Module>(loadFn, {
    ...options,
    fallback: withLazyIndicator(options?.fallback),
  });

  return (class Component extends React.Component<Props & IDefaultLoadableWrapperProps>
    implements LoadableComponentMethods<Props> {
    public static initialFallback: JSX.Element = options?.fallback;
    public static moduleFactoryInitOptions: {};

    public preload(props?: Props): void {
      return BasicLoadableComponent.preload(props);
    }

    public load(props?: Props): Promise<React.ComponentType<Props>> {
      return BasicLoadableComponent.load(props);
    }

    public static setFallback(fallback: JSX.Element): void {
      if (!this.initialFallback) {
        this.initialFallback = fallback;
      }
    }

    public static setModuleInitialOptions(moduleFactoryInitOptions: {}): void {
      if (!this.moduleFactoryInitOptions) {
        this.moduleFactoryInitOptions = moduleFactoryInitOptions;
      }
    }

    public render() {
      return (
        <BasicLoadableComponent
          {...this.props}
          fallback={withLazyIndicator(this.props.fallback || Component.initialFallback)}
          moduleFactoryInitOptions={Component.moduleFactoryInitOptions}
        />
      );
    }
    // doesn't see LoadableComponentMethods implementation, this is temp solution (see above todo)
  } as unknown) as ILoadableWrapper<Props>;
}

interface ILoadFallbackWrapperProps {
  customFallback: JSX.Element;
}

interface ILoadFallbackWrapperState {
  onLoadComplete: () => void;
}

const withLazyIndicator = (customFallback) => <LoadFallbackWrapper customFallback={customFallback || null} />;

class LoadFallbackWrapper extends React.Component<ILoadFallbackWrapperProps, ILoadFallbackWrapperState> {
  public static contextType: Context<ILoadableContext> = LoadableContext;
  public state: ILoadFallbackWrapperState = {
    onLoadComplete: null,
  };

  public componentDidMount(): void {
    if (process.env.NODE_ENV === 'test') {
      const { pushNewPromise } = this.context;

      if (pushNewPromise) {
        // make sure we have right context (storyshoot test, not regular one)
        pushNewPromise(
          new Promise<void>((resolve) => this.setState({ onLoadComplete: resolve }))
        );
      }
    }
  }

  public shouldComponentUpdate(
    nextProps: Readonly<ILoadFallbackWrapperProps>,
    nextState: Readonly<ILoadFallbackWrapperState>,
    nextContext: Context<ILoadableContext>
  ): boolean {
    if (nextProps !== this.props) {
      return true;
    }

    // we don't want to render on state | context changes
    return false;
  }

  public render() {
    return this.props.customFallback || null;
  }

  /**
   * Currently there is no better way to detect required component load completion.
   * If we rely on .load as we does it before this would lead to multiple load callback call
   * @see https://github.com/gregberge/loadable-components/issues/438
   * @see https://github.com/gregberge/loadable-components/pull/619
   */
  public async componentWillUnmount() {
    const { onLoadComplete } = this.state;

    if (onLoadComplete) {
      // we can't use getUtils().sleep as ioc isn't ready yet
      setTimeout(() => onLoadComplete());
      // there is no need to cleanup onload state when component gets unmounted
    }
  }
}
