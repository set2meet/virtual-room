import loadableWrapper from '../LoadableWrapper/loadableWrapper';
import React from 'react';
import { ICompositionRootProps } from '../../types/interfaces';
import { ClientEntityProviderContext } from '../../providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { IClientEntityProviderInternals } from '../../providers/ClientEntityProvider/types/IClientEntityProvider';

type TClientEntityProviderInternalsModule = { default: IClientEntityProviderInternals };

const CompositionRoot = loadableWrapper<ICompositionRootProps>(async (props: ICompositionRootProps) => {
  const clientEntityProviderModule: TClientEntityProviderInternalsModule = (await import(
    /* webpackPrefetch: true, webpackChunkName: "lazy/ioc/CompositionRoot" */
    `../../providers/ClientEntityProvider/ClientEntityProvider`
  )) as TClientEntityProviderInternalsModule;
  const clientEntityProvider: IClientEntityProviderInternals = clientEntityProviderModule.default;

  await clientEntityProvider.initialize(props.IoCInitializationOptions || {});

  const WillUnmountWrapper = class extends React.Component {
    public componentWillUnmount(): void {
      clientEntityProvider.destroy();
    }
    public render = () => null;
  };

  const CompositionRootWrapper = class extends React.Component<ICompositionRootProps> {
    public render() {
      return (
        <>
          <ClientEntityProviderContext.Provider value={{ clientEntityProvider }}>
            {this.props.children}
          </ClientEntityProviderContext.Provider>
          <WillUnmountWrapper />
        </>
      );
    }
  };

  return {
    default: CompositionRootWrapper,
  };
});

export default CompositionRoot;
