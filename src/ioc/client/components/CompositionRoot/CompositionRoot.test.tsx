import clientEntityProvider from '../../providers/ClientEntityProvider/ClientEntityProvider';
import { IClientEntityProviderInternals } from '../../providers/ClientEntityProvider/types/IClientEntityProvider';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import CompositionRoot from './CompositionRoot';
import React from 'react';
import { getTestIoCInitConfig } from '../../../../test/utils/ioc/config/testInitConfig';
import { loadAsyncTree } from '../../../../test/utils/storybook';
import ContextWrapper from '../../../../../.storybook/decorators/LazyResolver/LoadableContextWrapper';

describe('<CompositionRoot/>', () => {
  it('should initialize clientEntityProvider on mount and destroy on unmount', async (done) => {
    const clientEntityProviderInternals = clientEntityProvider as IClientEntityProviderInternals;

    expect(clientEntityProviderInternals.isInitialized).toBeFalsy();

    const root = EnzymeMountTracker.mount(
      <ContextWrapper>
        <CompositionRoot IoCInitializationOptions={getTestIoCInitConfig()}>
          <></>
        </CompositionRoot>
      </ContextWrapper>
    );

    await loadAsyncTree(root);

    expect(clientEntityProviderInternals.isInitialized).toBeTruthy();
    root.unmount();
    expect(clientEntityProviderInternals.isInitialized).toBeFalsy();
    done();
  });
  it('should destroy clientEntityProvider after child willUnmount calls', async (done) => {
    const clientEntityProviderInternals = clientEntityProvider as IClientEntityProviderInternals;

    const WillUnmountWrapper = class extends React.Component {
      public componentWillUnmount(): void {
        expect(clientEntityProviderInternals.isInitialized).toBeTruthy();
      }
      public render = () => <></>;
    };
    const root = EnzymeMountTracker.mount(
      <ContextWrapper>
        <CompositionRoot IoCInitializationOptions={getTestIoCInitConfig()}>
          <WillUnmountWrapper />
        </CompositionRoot>
      </ContextWrapper>
    );

    await loadAsyncTree(root);
    root.unmount();

    done();
  });
});
