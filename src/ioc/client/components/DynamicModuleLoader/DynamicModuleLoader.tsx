import * as React from 'react';
import { IDynamicallyAddedModule, IModuleStore, IModuleTuple } from 'redux-dynamic-modules-core';
import { IClientEntityProviderInternals } from '../../providers/ClientEntityProvider/types/IClientEntityProvider';
import { Context } from 'react';
import {
  ClientEntityProviderContext,
  TClientEntityProviderContext,
} from '../../providers/ClientEntityProvider/context/ClientEntityProviderContext';

// Source is partially copied from
// tslint:disable-next-line:max-line-length
// https://github.com/microsoft/redux-dynamic-modules/blob/e731bf4ed3b20beb268d369e5ecc25a3d6b817f6/packages/redux-dynamic-modules-react/src/DynamicModuleLoader.tsx

export interface IDynamicModuleLoaderProps {
  modules: IModuleTuple;
  skipUnmountCleanup?: boolean;
}

interface IDynamicModuleLoaderImplProps extends IDynamicModuleLoaderProps {
  store: IModuleStore<any>;
  skipUnmountCleanup?: boolean;
}

interface IDynamicModuleLoaderImplState {
  readyToRender: boolean;
  addedModules: IDynamicallyAddedModule;
}

export class DynamicModuleLoaderImpl extends React.Component<IDynamicModuleLoaderImplProps, IDynamicModuleLoaderImplState> {
  public state: IDynamicModuleLoaderImplState = {
    readyToRender: false,
    addedModules: null,
  };

  public componentDidMount() {
    if (this.props.store && this.props.modules) {
      this.addModules();
      this.setState({ readyToRender: true });
    }
  }

  public render(): React.ReactNode {
    if (this.state.readyToRender) {
      return (
        <>
          {this.renderBody()}
          <AddedModulesCleanup cleanup={this.cleanup} />
        </>
      );
    }

    return null;
  }

  private renderBody(): React.ReactNode {
    return this.props.children
      ? typeof this.props.children === 'function'
        ? this.props.children()
        : this.props.children
      : null;
  }

  private addModules(): void {
    const { store, modules } = this.props;
    const addedModules = store.addModules(modules);

    this.setState({
      addedModules,
      readyToRender: true,
    });
  }

  /**
   * Unregister sagas and reducers
   */
  private cleanup = () => {
    const { addedModules } = this.state;

    if (addedModules && !this.props.skipUnmountCleanup) {
      addedModules.remove();
    }
  };
}

interface IAddedModulesCleanupProps {
  cleanup: () => void;
}

/**
 * This component is rendered as the last child of DynamicModuleLoaderImpl
 * so react runs willUnmount on connected(react-redux) children before this
 * cleanup and allows them to unsubscribe from store before dynamic reducers
 * removing (and avoid errors in selectors)
 */
class AddedModulesCleanup extends React.Component<IAddedModulesCleanupProps> {
  public render() {
    return null;
  }

  public componentWillUnmount() {
    this.props.cleanup();
  }
}

/**
 * The DynamicModuleLoader adds a way to register a module on mount
 * When this component is initialized, the reducer and saga from the module passed as props will be registered with the system
 * On unmount, they will be unregistered (if there is no skipUnmountCleanup provided)
 *
 * Why don't we use the original one:
 * * it uses unsupported react/redux hacks to gather store when we can simply request it from context inside VR sources
 * * here is no way to temporally disable module cleanup on unmount which is not how s2m redux was initially designed
 * * it seems to be incompatible with vr react-redux version
 * * it is simply doesn't work and codebase is too hacky and tiny to fix
 * (at least currently we don't have much time to investigate the problems)
 *
 */
export class DynamicModuleLoader extends React.Component<IDynamicModuleLoaderProps> {
  public static contextType: Context<TClientEntityProviderContext> = ClientEntityProviderContext;

  public render() {
    return (
      <ClientEntityProviderContext.Consumer>
        {({ clientEntityProvider }) => {
          if (!clientEntityProvider) {
            return null;
          }
          const store = (clientEntityProvider as IClientEntityProviderInternals).getStore();

          return (
            <DynamicModuleLoaderImpl
              store={store}
              modules={this.props.modules}
              skipUnmountCleanup={this.props.skipUnmountCleanup}
            >
              {this.props.children}
            </DynamicModuleLoaderImpl>
          );
        }}
      </ClientEntityProviderContext.Consumer>
    );
  }
}
