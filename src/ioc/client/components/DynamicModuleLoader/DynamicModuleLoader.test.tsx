import { createStore } from 'redux-dynamic-modules-core';
import React from 'react';
import { Provider, connect } from 'react-redux';
import { DynamicModuleLoaderImpl } from './DynamicModuleLoader';
import EnzymeMountTracker from '../../../../test/utils/EnzymeMountTracker';
import { enzymeCleanup } from '../../../../test/utils/utils';

function testMapStateToProps(state) {
  return { foo: state.foo || 'not found' };
}

const TestComponent = connect(testMapStateToProps)((props) => (
  <div className="testOutput">{JSON.stringify(props)}</div>
));

afterEach(enzymeCleanup);

describe('DynamicModuleLoaderImpl tests', () => {
  let store;
  let testModule;

  beforeEach(() => {
    store = createStore({});

    testModule = {
      id: 'test',
      reducerMap: {
        foo: () => {
          return { bar: 'testState' };
        },
      },
    };
  });

  it('Works in normal mode', () => {
    const renderedComponent = EnzymeMountTracker.mount(
      <Provider store={store}>
        <DynamicModuleLoaderImpl store={store} modules={[testModule]}>
          <TestComponent />
        </DynamicModuleLoaderImpl>
      </Provider>
    );

    expect(renderedComponent).toBeTruthy();

    const output = renderedComponent.find('.testOutput');

    expect(output.html()).toContain('testState');
  });

  it('Works in strict mode', () => {
    class TestHarness extends React.Component<{}, { useDML: boolean }> {
      constructor(props: {}) {
        super(props);
        this.state = { useDML: true };
      }

      public render() {
        const onBtnClick = () => {
          this.setState({ useDML: !this.state.useDML });
        };

        return (
          <React.StrictMode>
            <Provider store={store}>
              {this.state.useDML ? (
                <DynamicModuleLoaderImpl store={store} modules={[testModule]}>
                  <TestComponent />
                </DynamicModuleLoaderImpl>
              ) : (
                <TestComponent />
              )}
            </Provider>
            <button className="toggle" onClick={onBtnClick} />
          </React.StrictMode>
        );
      }
    }
    const renderedComponent = EnzymeMountTracker.mount(<TestHarness />);

    expect(renderedComponent).toBeTruthy();

    const button = renderedComponent.find('.toggle');

    button.simulate('click');

    const output = renderedComponent.find('.testOutput');

    expect(output.html()).toContain('not found');
  });
});
