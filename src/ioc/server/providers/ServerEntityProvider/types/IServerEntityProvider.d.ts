/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { TServerAppActionTypes } from './TServerAppActionTypes';
import { IServerMiddleware } from '../../../../../modules/common';
import { Strategy } from 'passport';
import { IServerLogger, IRecordingDb, IRecordingSubPub } from '../../../types/interfaces';
import { ReducersMapObject } from 'redux';
import { TRegisterRoutes } from './TRegisterRoutes';
import { TRegisterSocketEvents } from './TRegisterSocketEvents';
import { IServerUtilsProvider } from '../../ServerUtilsProvider/IServerUtilsProvider';
import IServerAppConfig from '../../../../../server/config/types/TServerConfig';
import { TInitializationOptions } from './TInitializationOptions';
import { IConfigWrapper } from 'env-config';

export interface IServerEntityProvider {
  /**
   * @deprecated in SETMEET-1911, commit #e06b643
   * @todo SETMEET-1945 (try then remove this)
   */
  getConstants(): any;

  getAppReducers(): ReducersMapObject;

  getActionTypes(): TServerAppActionTypes;

  getServerMiddleware(): IServerMiddleware;

  getCustomStrategy(): Strategy;

  getLogger(): IServerLogger;

  getUtils(): IServerUtilsProvider;

  getRecordingDb(): IRecordingDb;

  getRecordingSubPub(): IRecordingSubPub;

  getConfig(): IServerAppConfig & IConfigWrapper;
}

export interface IServerEntityProviderInternals extends IServerEntityProvider {
  isInitialized: boolean;

  initialize(initializationOptions?: TInitializationOptions): Promise<void>;

  destroy(): Promise<void>;

  getRegisterRoutes(): TRegisterRoutes;

  getRegisterSocketEvents(): TRegisterSocketEvents;
}
