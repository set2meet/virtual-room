import serverEntityProvider from './ServerEntityProvider';
import { IServerEntityProviderInternals } from './types/IServerEntityProvider';
import setupIoCServerForTest from '../../../../test/utils/ioc/testIoCServer';

describe('ServerEntityProvider', () => {
  beforeEach(async (done) => {
    jest.restoreAllMocks();
    jest.clearAllMocks();
    await setupIoCServerForTest()(done);
  });

  it('ClientEntityProvider should unbind entities on destroy call', async (done) => {
    const serverEntityProviderInternal = serverEntityProvider as IServerEntityProviderInternals;
    const actionTypes = serverEntityProviderInternal.getActionTypes();

    expect(actionTypes).toBeDefined();

    await serverEntityProviderInternal.destroy();

    expect(() => serverEntityProviderInternal.getActionTypes()).toThrowError(
      /No matching bindings found for serviceIdentifier:.*/
    );
    done();
  });

  it('ClientEntityProvider should correctly reinit after recycle', async (done) => {
    const serverEntityProviderInternal = serverEntityProvider as IServerEntityProviderInternals;

    await serverEntityProviderInternal.destroy();

    expect(() => serverEntityProviderInternal.getActionTypes()).toThrowError(
      /No matching bindings found for serviceIdentifier:.*/
    );

    await setupIoCServerForTest()();

    expect(serverEntityProviderInternal.isInitialized).toBeTruthy();
    expect(serverEntityProviderInternal.getActionTypes()).toBeDefined();

    done();
  });

  it('public getters', async (done) => {
    const serverEntityProviderInternals = serverEntityProvider as IServerEntityProviderInternals;

    const constants = serverEntityProviderInternals.getConstants();
    const reducers = serverEntityProviderInternals.getAppReducers();
    const serverMiddleware = serverEntityProviderInternals.getServerMiddleware();
    const strategy = serverEntityProviderInternals.getCustomStrategy();
    const logger = serverEntityProviderInternals.getLogger();
    const recordingDb = serverEntityProviderInternals.getRecordingDb();
    const recordingSubPub = serverEntityProviderInternals.getRecordingSubPub();
    const actionTypes = serverEntityProviderInternals.getActionTypes();
    const registerSocketEvents = serverEntityProviderInternals.getRegisterSocketEvents();
    const registerRoutes = serverEntityProviderInternals.getRegisterRoutes();

    expect(recordingDb).toBeDefined();
    expect(recordingSubPub).toBeDefined();
    expect(constants).toHaveProperty('CONNECTION_STATUS');
    expect(reducers).toHaveProperty('room');
    expect(serverMiddleware).toBeDefined();
    expect(strategy).toHaveProperty('name');
    expect(logger).toHaveProperty('auth');
    expect(actionTypes).toHaveProperty('SEND');
    expect(registerSocketEvents).toBeDefined();
    expect(registerRoutes).toBeDefined();

    done();
  });
});
