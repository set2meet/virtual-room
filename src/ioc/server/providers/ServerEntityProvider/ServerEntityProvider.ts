/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import * as logger from '../../../../modules/logger/server/loggerServer';
import { getRecordingDb, IRecordingDb, getRecordingSubPub, IRecordingSubPub } from '@s2m/recording-db';
import { combineReducers, ReducersMapObject } from 'redux';
import { ServerRegistryKey } from './types/ServerRegistryKey';
import { TServerAppActionTypes } from './types/TServerAppActionTypes';
import roomServer from '../../../../modules/room/server/roomServer';
import auth from '../../../../modules/auth/server/authServer';
import recordingActionTypes from '../../../../modules/recording/src/common/redux/RecordingAction';
import recordingRoomReducer from '../../../../modules/recording/src/common/redux/reducer/reducer';
import recordingMiddleware from '../../../../modules/recording/src/server/middleware/middleware';
import globalReducer from '../../../../modules/app/entities/store/globalReducer';
import * as chat from '../../../../modules/chat/chatServer';
import webrtcReducer from '../../../../modules/webrtc/common/redux/reducer';
import { reducer as webphoneParticipantsReducer } from '../../../../modules/webphone/webphoneServer';
import * as whiteboard from '../../../../modules/whiteboard/server/whiteboardServer';
import { roomReducer as graph2dReducer } from '../../../../modules/graph2d/graph2dServer';
import { IServerMetaObjects, IServerMiddleware } from '../../../../modules/common';
import composeServerMiddleware from '../../../../server/utils/composeServerMiddleware';
import {
  registerP2PSocketEvents,
  webrtcP2PServerMiddleware,
  webrtcOpentokServerMiddleware,
} from '../../../../modules/webrtc/server/webrtcServer';
import passport from 'passport';
import { createCustomStrategy } from '../../../../server/utils/strategy';
import { IServerLogger } from '../../types/interfaces';
import { IServerEntityProvider, IServerEntityProviderInternals } from './types/IServerEntityProvider';
import { IServerReduxState } from './types/IServerReduxState';
import Router from 'koa-router';
import {
  reducer as presentationsReducer,
  registerRoutes as registerPresentationsRoutes,
} from '../../../../modules/presentations/server/presentationsServer';
import { registerRoutes as registerFeedbackRoutes } from '../../../../modules/feedback/server/feedbackServer';
import { TRegisterRoutes } from './types/TRegisterRoutes';
import { Socket } from 'socket.io';
import { TRegisterSocketEvents } from './types/TRegisterSocketEvents';
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import { APIAction } from '../../../../modules/app/redux/api/APIAction';
import { ServerUtilsProvider } from '../ServerUtilsProvider/ServerUtilsProvider';
import { IServerUtilsProvider } from '../ServerUtilsProvider/IServerUtilsProvider';
import { webrtcConstants } from '../../../common/ioc-constants';
import IServerAppConfig from '../../../../server/config/types/TServerConfig';
import TServerConfig from '../../../../server/config/types/TServerConfig';
import { TInitializationOptions } from './types/TInitializationOptions';
import buildServerConfig from '../../../../server/config/serverConfig';
import { IConfigWrapper, ConfigWrapper } from 'env-config';

class ServerEntityProvider extends BaseDependencyProvider<ServerRegistryKey> implements IServerEntityProviderInternals {
  private static _instance: ServerEntityProvider;

  public isInitialized: boolean = false;

  private constructor() {
    super();
  }

  public static createInstance() {
    return this._instance || (this._instance = new this());
  }

  public async initialize(initializationOptions: TInitializationOptions = {}): Promise<void> {
    if (this.isInitialized) {
      return Promise.resolve();
    }
    await this.bindConfig(initializationOptions.configFactory);
    await this.bindLogger();

    this.bindConstants();
    const actionTypes = this.bindActionTypes();
    const appServerReducer = this.bindReducers(actionTypes);

    this.bindServerMiddleware(appServerReducer);
    this.bindRecordingDataBaseManager();

    this.bindPassportStrategy();
    this.bindRegisterRoutes();
    this.bindRegisterSocketEvents();
    this.bindUtils();

    this.isInitialized = true;
  }

  public getConstants() {
    return this.getDependency<any>(ServerRegistryKey.Constants);
  }

  public getAppReducers(): ReducersMapObject<IServerReduxState> {
    return this.getDependency<ReducersMapObject<IServerReduxState>>(ServerRegistryKey.AppReducers);
  }

  public getActionTypes(): TServerAppActionTypes {
    return this.getDependency<TServerAppActionTypes>(ServerRegistryKey.ActionTypes);
  }

  public getServerMiddleware(): IServerMiddleware {
    return this.getDependency<IServerMiddleware>(ServerRegistryKey.ServerMiddleware);
  }

  public getCustomStrategy(): passport.Strategy {
    return this.getDependency<passport.Strategy>(ServerRegistryKey.StrategyCacheKey);
  }

  public getLogger(): IServerLogger {
    return this.getDependency<IServerLogger>(ServerRegistryKey.Logger);
  }

  public getRecordingDb(): IRecordingDb {
    return this.getDependency<IRecordingDb>(ServerRegistryKey.RecordingDb);
  }

  public getRecordingSubPub(): IRecordingSubPub {
    return this.getDependency<IRecordingSubPub>(ServerRegistryKey.RecordingSubPub);
  }

  public getRegisterRoutes(): TRegisterRoutes {
    return this.getDependency<TRegisterRoutes>(ServerRegistryKey.RegisterRoutes);
  }

  public getRegisterSocketEvents(): TRegisterSocketEvents {
    return this.getDependency<TRegisterSocketEvents>(ServerRegistryKey.RegisterSocketEvents);
  }

  public getUtils(): IServerUtilsProvider {
    return this.getDependency<IServerUtilsProvider>(ServerRegistryKey.Utils);
  }

  public getConfig(): IServerAppConfig & IConfigWrapper {
    return this.getDependency(ServerRegistryKey.Config);
  }

  public async destroy(): Promise<void> {
    if (!this.isInitialized) {
      return;
    }
    super.destroy();
    this.isInitialized = false;
  }

  private async bindConfig(configFactory?: () => Promise<TServerConfig>) {
    if (configFactory) {
      this.setDependency(
        ServerRegistryKey.Config,
        new ConfigWrapper(((await configFactory()) as unknown) as Record<string, unknown>)
      );
    } else {
      this.setDependency(ServerRegistryKey.Config, await buildServerConfig());
    }
  }

  private bindActionTypes(): TServerAppActionTypes {
    const actionTypes: TServerAppActionTypes = {
      ...APIAction,
      ...roomServer.actionTypes,
      ...recordingActionTypes,
    };

    this.setDependency<TServerAppActionTypes>(ServerRegistryKey.ActionTypes, actionTypes);

    return actionTypes;
  }

  private bindReducers(actionTypes: TServerAppActionTypes): ReducersMapObject<IServerReduxState> {
    const appServerReducer = {
      room: globalReducer(
        combineReducers({
          state: roomServer.reducer,
          chat: chat.reducer,
          webrtcService: webrtcReducer,
          webphone: webphoneParticipantsReducer,
          whiteboard: whiteboard.roomReducer,
          recording: recordingRoomReducer,
          presentations: presentationsReducer,
          graph2d: graph2dReducer,
        }),
        actionTypes
      ),
    };

    this.setDependency<ReducersMapObject<IServerReduxState>>(ServerRegistryKey.AppReducers, appServerReducer);

    return appServerReducer;
  }

  private bindRecordingDataBaseManager() {
    this.setDependency<IRecordingDb>(ServerRegistryKey.RecordingDb, getRecordingDb());
    this.setDependency<IRecordingSubPub>(ServerRegistryKey.RecordingSubPub, getRecordingSubPub());
  }

  private bindServerMiddleware(appServerReducer: ReducersMapObject<IServerReduxState>) {
    // TODO order is important because room send actions via SocketIO if target.roomId = room, refactor to correct case
    const serverMiddleware: IServerMiddleware = composeServerMiddleware([
      webrtcP2PServerMiddleware,
      webrtcOpentokServerMiddleware,
      recordingMiddleware,
      roomServer.serverMiddleware('state', appServerReducer.room),
    ]);

    this.setDependency<IServerMiddleware>(ServerRegistryKey.ServerMiddleware, serverMiddleware);
  }

  private bindConstants() {
    const constants = {
      ...webrtcConstants,
    };

    this.setDependency<any>(ServerRegistryKey.Constants, constants);
  }

  private async bindLogger(): Promise<void> {
    const serverLogger = await logger.loggerProvider();

    this.setDependency<IServerLogger>(ServerRegistryKey.Logger, serverLogger);
  }

  private bindPassportStrategy() {
    this.setDependency<passport.Strategy>(ServerRegistryKey.StrategyCacheKey, createCustomStrategy());
  }

  private bindRegisterRoutes() {
    const registerRoutes: TRegisterRoutes = (router: Router, appMetaObjects: IServerMetaObjects) => {
      registerPresentationsRoutes(router);
      registerFeedbackRoutes(router, appMetaObjects);
      auth.registerRoutes(router);
      roomServer.registerRoutes(router, appMetaObjects);
    };

    this.setDependency<TRegisterRoutes>(ServerRegistryKey.RegisterRoutes, registerRoutes);
  }

  private bindRegisterSocketEvents() {
    const registerSocketEvents: TRegisterSocketEvents = (socket: Socket, appMetaObjects: IServerMetaObjects) => {
      whiteboard.registerSocketEvents(socket, appMetaObjects);
      registerP2PSocketEvents(socket, appMetaObjects);
    };

    this.setDependency<TRegisterSocketEvents>(ServerRegistryKey.RegisterSocketEvents, registerSocketEvents);
  }

  private bindUtils() {
    const serverUtilsProvider = new ServerUtilsProvider();

    this.setDependency(ServerRegistryKey.Utils, serverUtilsProvider);
  }
}

const serverEntityProvider: IServerEntityProvider = ServerEntityProvider.createInstance();

export default serverEntityProvider;
