/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { BaseDependencyProvider } from '../../../common/BaseDependencyProvider/BaseDependencyProvider';
import { IServerUtilsProvider } from './IServerUtilsProvider';
import { WebrtcServiceType } from '../../../../modules/webrtc/common/types/WebrtcServiceType';
import { IWebrtcServicesConfig, IWebrtcServicesWrapper } from 'env-config';

export class ServerUtilsProvider extends BaseDependencyProvider<{}> implements IServerUtilsProvider {
  public isWebrtcServiceConfigured(
    service: WebrtcServiceType,
    servicesConfig: IWebrtcServicesConfig & IWebrtcServicesWrapper
  ): boolean {
    return !!servicesConfig.getServiceConfigByType(service);
  }

  public getFirstAvailableWebrtcService = (
    servicesConfig: IWebrtcServicesConfig & IWebrtcServicesWrapper
  ): WebrtcServiceType => {
    const serviceKey = Object.keys(WebrtcServiceType).find((currentType) =>
      this.isWebrtcServiceConfigured(WebrtcServiceType.P2P[currentType], servicesConfig)
    );

    return serviceKey ? WebrtcServiceType[serviceKey] : WebrtcServiceType.P2P;
  };
}
