/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { vrCommon } from '../../../modules/common';
import IAnyAction = vrCommon.client.IAnyAction;

export { TAuthTokens } from '../../client/providers/AuthProvider/types/common/TAuthTokens';
export { IServerLogger, TLoggerServerModule } from '../../../modules/logger/server/types/types';
export { IRecordingDb, TRecordingMessage, IRecordingSubPub } from '@s2m/recording-db';

export { IServerMetaObjects, SocketIOAdapter } from '../../../modules/common';

export {
  IServerMiddleware,
  IServerMiddlewareContext,
  IServerMiddlewareNext,
  IServerMiddlewareOptionalContext,
} from '../../../modules/common';

export { IAnyAction };
