/*
 * Copied from https://static.opentok.com/v2.17.1/js/opentok.d.ts
 * We assume that spec has different license than implementation (/v2.17.1//js/opentok.min.js)
 */

declare namespace OT {
  export type OTError = {
    name: string;
    message: string;
  };

  export type Dimensions = {
    width: number;
    height: number;
  };

  export type ScreenSharingCapabilityResponse = {
    extensionInstalled?: boolean;
    supported: boolean;
    supportedSources: {
      application?: boolean;
      screen?: boolean;
      window?: boolean;
    };
    extensionRequired?: string;
    extensionRegistered?: boolean;
  };

  export function checkScreenSharingCapability(callback: (response: ScreenSharingCapabilityResponse) => void): void;

  export function checkSystemRequirements(): number;

  export type Device = {
    kind: 'audioInput' | 'videoInput';
    deviceId: string;
    label: string;
  };

  export function getDevices(callback: (error: OTError | undefined, devices?: Device[]) => void): void;

  export function setProxyUrl(proxyUrl: string): void;

  export function getSupportedCodecs(): Promise<{
    videoEncoders: Array<'H264' | 'VP8'>;
    videoDecoders: Array<'H264' | 'VP8'>;
  }>;

  export type WidgetStyle = {
    audioLevelDisplayMode: 'auto' | 'on' | 'off';
    backgroundImageURI: string;
    buttonDisplayMode: 'auto' | 'on' | 'off';
    nameDisplayMode: 'auto' | 'on' | 'off';
  };

  export type WidgetProperties = {
    fitMode?: 'cover' | 'contain';
    insertDefaultUI?: boolean;
    insertMode?: 'replace' | 'after' | 'before' | 'append';
    showControls?: boolean;
    width?: string | number;
    height?: string | number;
  };

  export type PublisherStyle = WidgetStyle & {
    archiveStatusDisplayMode: 'auto' | 'off';
  };

  export type GetUserMediaProperties = {
    audioSource?: string | null | boolean | MediaStreamTrack;
    disableAudioProcessing?: boolean;
    facingMode?: 'user' | 'environment' | 'left' | 'right';
    // tslint:disable-next-line:no-magic-numbers
    frameRate?: 30 | 15 | 7 | 1;
    maxResolution?: Dimensions;
    resolution?: '1280x960' | '1280x720' | '640x480' | '640x360' | '320x240' | '320x180';
    videoSource?: string | null | boolean | MediaStreamTrack;
  };

  export type PublisherProperties = WidgetProperties &
    GetUserMediaProperties & {
      audioBitrate?: number;
      audioFallbackEnabled?: boolean;
      mirror?: boolean;
      name?: string;
      publishAudio?: boolean;
      publishVideo?: boolean;
      style?: Partial<PublisherStyle>;
    };

  export type SubscriberStyle = WidgetStyle & {
    videoDisabledDisplayMode: 'auto' | 'on' | 'off';
    audioBlockedDisplayMode: 'auto' | 'on' | 'off';
  };

  export type SubscriberProperties = WidgetProperties & {
    audioVolume?: number;
    preferredFrameRate?: number;
    preferredResolution?: Dimensions;
    style?: Partial<SubscriberStyle>;
    subscribeToAudio?: boolean;
    subscribeToVideo?: boolean;
    testNetwork?: boolean;
  };

  export class Connection {
    public connectionId: string;
    public creationTime: number;
    public data: string;
  }

  export class Stream {
    public connection: Connection;
    public creationTime: number;
    public frameRate: number;
    public hasAudio: boolean;
    public hasVideo: boolean;
    public name: string;
    public streamId: string;
    public videoDimensions: {
      width: number;
      height: number;
    };
    public videoType: 'camera' | 'screen';
  }

  export type Event<Type, Target> = {
    type: Type;
    cancelable: boolean;
    target: Target;

    isDefaultPrevented(): boolean;
    preventDefault(): void;
  };

  export type ExceptionEvent = Event<'exception', {}> & {
    code: number;
    message: string;
    title: string;
  };

  export type VideoDimensionsChangedEvent<Target> = Event<'videoDimensionsChanged', Target> & {
    oldValue: Dimensions;
    newValue: Dimensions;
  };

  class OTEventEmitter<EventMap> {
    public on<EventName extends keyof EventMap>(
      eventName: EventName,
      callback: (event: EventMap[EventName]) => void,
      context?: object
    ): void;

    public on(eventName: string, callback: (event: Event<string, any>) => void, context?: object): void;

    public on(eventMap: object, context?: object): void;

    public once<EventName extends keyof EventMap>(
      eventName: EventName,
      callback: (event: EventMap[EventName]) => void,
      context?: object
    ): void;

    public once(eventName: string, callback: (event: Event<string, any>) => void, context?: object): void;

    public once(eventMap: object, context?: object): void;

    public off<EventName extends keyof EventMap>(
      eventName?: EventName,
      callback?: (event: EventMap[EventName]) => void,
      context?: object
    ): void;

    public off(eventName?: string, callback?: (event: Event<string, any>) => void, context?: object): void;

    public off(eventMap: object, context?: object): void;
  }

  export class Publisher extends OTEventEmitter<{
    accessAllowed: Event<'accessAllowed', Publisher>;
    accessDenied: Event<'accessDenied', Publisher>;
    accessDialogClosed: Event<'accessDialogClosed', Publisher>;
    accessDialogOpened: Event<'accessDialogOpened', Publisher>;

    audioLevelUpdated: Event<'audioLevelUpdated', Publisher> & {
      audioLevel: number;
    };

    destroyed: Event<'destroyed', Publisher>;

    mediaStopped: Event<'mediaStopped', Publisher> & {
      track: MediaStreamTrack | undefined;
    };

    streamCreated: Event<'streamCreated', Publisher> & {
      stream: Stream;
    };

    streamDestroyed: Event<'streamDestroyed', Publisher> & {
      stream: Stream;
      reason: string;
    };

    videoDimensionsChanged: VideoDimensionsChangedEvent<Publisher>;

    videoElementCreated: Event<'videoElementCreated', Publisher> & {
      element: HTMLVideoElement | HTMLObjectElement;
    };
  }> {
    public accessAllowed: boolean;
    public element?: HTMLElement | undefined;
    public id?: string;
    public stream?: Stream;
    public session?: Session;

    public destroy(): void;
    public getImgData(): string | null;
    public getStats(callback: (error?: OTError, stats?: PublisherStatsArr) => void): void;
    public getRtcStatsReport(): Promise<PublisherRtcStatsReportArr>;
    public getStyle(): PublisherProperties;
    public publishAudio(value: boolean): void;
    public publishVideo(value: boolean): void;
    public cycleVideo(): Promise<{ deviceId: string }>;
    public setAudioSource(audioSource: string | null | boolean | MediaStreamTrack): Promise<undefined>;
    public getAudioSource(): MediaStreamTrack;
    public setStyle<Style extends keyof PublisherStyle>(style: Style, value: PublisherStyle[Style]): void;
    public videoWidth(): number | undefined;
    public videoHeight(): number | undefined;
  }

  export function getUserMedia(properties?: GetUserMediaProperties): Promise<MediaStream>;

  export function initPublisher(
    targetElement?: HTMLElement | string,
    properties?: PublisherProperties,
    callback?: (error?: OTError) => void
  ): Publisher;

  export function log(message: string): void;

  export function off(eventName?: 'exception', callback?: (event: ExceptionEvent) => void, context?: object): void;

  export function on(eventName: 'exception', callback: (event: ExceptionEvent) => void, context?: object): void;

  export function once(eventName: 'exception', callback: (event: ExceptionEvent) => void, context?: object): void;

  export class Session extends OTEventEmitter<{
    archiveStarted: Event<'archiveStarted', Session> & {
      id: string;
      name: string;
    };

    archiveStopped: Event<'archiveStopped', Session> & {
      id: string;
      name: string;
    };

    connectionCreated: Event<'connectionCreated', Session> & {
      connection: Connection;
    };

    connectionDestroyed: Event<'connectionDestroyed', Session> & {
      connection: Connection;
      reason: string;
    };

    sessionConnected: Event<'sessionConnected', Session>;

    sessionDisconnected: Event<'sessionDisconnected', Session> & {
      reason: string;
    };

    sessionReconnected: Event<'sessionReconnected', Session>;
    sessionReconnecting: Event<'sessionReconnecting', Session>;

    signal: Event<'signal', Session> & {
      type?: string;
      data?: string;
      from: Connection;
    };

    streamCreated: Event<'streamCreated', Session> & {
      stream: Stream;
    };

    streamDestroyed: Event<'streamDestroyed', Session> & {
      stream: Stream;
      reason: string;
    };

    streamPropertyChanged: Event<'streamPropertyChanged', Session> & {
      stream: Stream;
    } & (
        | { changedProperty: 'hasAudio'; oldValue: boolean; newValue: boolean }
        | { changedProperty: 'hasVideo'; oldValue: boolean; newValue: boolean }
        | { changedProperty: 'videoDimensions'; oldValue: Dimensions; newValue: Dimensions }
      );
  }> {
    public capabilities: {
      forceDisconnect: number;
      forceUnpublish: number;
      publish: number;
      subscribe: number;
    };

    public connection?: Connection;
    public sessionId: string;

    public connect(token: string, callback: (error?: OTError) => void): void;
    public disconnect(): void;
    public forceDisconnect(connection: Connection, callback: (error?: OTError) => void): void;
    public forceUnpublish(stream: Stream, callback: (error?: OTError) => void): void;
    public getPublisherForStream(stream: Stream): Publisher | undefined;
    public getSubscribersForStream(stream: Stream): [Subscriber];
    // tslint:disable-next-line:ban-types
    public publish(publisher: Publisher, properties?: Object, callback?: (error?: OTError) => void): void;
    // tslint:disable-next-line:ban-types
    public publish(targetElement: string, properties: Object): Publisher;

    public signal(signal: { type?: string; data?: string; to?: Connection }, callback: (error?: OTError) => void): void;

    public subscribe(
      stream: Stream,
      targetElement?: HTMLElement | string,
      properties?: SubscriberProperties,
      callback?: (error?: OTError) => void
    ): Subscriber;

    public unpublish(publisher: Publisher): void;
    public unsubscribe(subscriber: Subscriber): void;
  }

  export function initSession(
    partnerId: string,
    sessionId: string,
    options?: {
      connectionEventsSuppressed?: boolean;
      iceConfig?: {
        includeServers: 'all' | 'custom';
        transportPolicy: 'all' | 'relay';
        customServers: Array<{
          urls: string | string[];
          username?: string;
          credential?: string;
        }>;
      };
      ipWhitelist?: boolean;
    }
  ): Session;

  export type IncomingTrackStats = {
    bytesReceived: number;
    packetsLost: number;
    packetsReceived: number;
  };

  export type OutgoingTrackStats = {
    bytesSent: number;
    packetsLost: number;
    packetsSent: number;
  };

  export type SubscriberStats = {
    audio: IncomingTrackStats;
    video: IncomingTrackStats & { frameRate: number };
    timestamp: number;
  };

  export type PublisherStats = {
    audio: OutgoingTrackStats;
    video: OutgoingTrackStats & { frameRate: number };
    timestamp: number;
  };

  export type PublisherStatContainer = {
    subscriberId?: string;
    connectionId?: string;
    stats: PublisherStats;
  };

  export type PublisherStatsArr = PublisherStatContainer[];

  export type PublisherRtcStatsReportContainer = {
    subscriberId?: string;
    connectionId?: string;
    rtcStatsReport: RTCStatsReport;
  };

  export type PublisherRtcStatsReportArr = PublisherRtcStatsReportContainer[];

  export class Subscriber extends OTEventEmitter<{
    audioLevelUpdated: Event<'audioLevelUpdated', Subscriber> & {
      audioLevel: number;
    };

    connected: Event<'connected', Subscriber>;

    destroyed: Event<'destroyed', Subscriber> & {
      reason: string;
    };

    videoDimensionsChanged: VideoDimensionsChangedEvent<Subscriber>;

    videoDisabled: Event<'videoDisabled', Subscriber> & {
      reason: string;
    };

    videoDisableWarning: Event<'videoDisableWarning', Subscriber>;
    videoDisableWarningLifted: Event<'videoDisableWarningLifted', Subscriber>;

    videoElementCreated: Event<'videoElementCreated', Subscriber> & {
      element: HTMLVideoElement | HTMLObjectElement;
    };

    videoEnabled: Event<'videoEnabled', Subscriber> & {
      reason: string;
    };
  }> {
    public element?: HTMLElement;
    public id?: string;
    public stream?: Stream;

    public getAudioVolume(): number;
    public getImgData(): string | null;
    public getStats(callback: (error?: OTError, stats?: SubscriberStats) => void): void;
    public getRtcStatsReport(): Promise<RTCStatsReport>;
    public restrictFrameRate(value: boolean): void;
    public setAudioVolume(volume: number): void;
    public setPreferredFrameRate(frameRate: number): void;
    public setPreferredResolution(resolution: Dimensions): void;
    public subscribeToAudio(value: boolean): void;
    public subscribeToVideo(value: boolean): void;

    public setStyle<Style extends keyof SubscriberStyle>(style: Style, value: SubscriberStyle[Style]): void;

    public videoHeight(): number | undefined;
    public videoWidth(): number | undefined;
  }

  export function registerScreenSharingExtension(kind: string, id: string, version: number): void;

  export function reportIssue(callback: (error?: OTError, reportId?: string) => void): void;

  export function setLogLevel(level: number): void;

  export function upgradeSystemRequirements(): void;

  export function unblockAudio(): Promise<undefined>;
}

declare module '@opentok/client' {
  export = OT;
}
