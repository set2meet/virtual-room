/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { RowDataPacket, OkPacket, FieldPacket, QueryOptions, ConnectionOptions, PoolOptions } from 'mysql';
import { EventEmitter } from 'events';
export * from './index';
// tslint:disable:unified-signatures
// tslint:disable:interface-name
export interface Connection extends EventEmitter {
  config: ConnectionOptions;
  threadId: number;

  connect(): Promise<void>;
  ping(): Promise<void>;

  beginTransaction(): Promise<void>;
  commit(): Promise<void>;
  rollback(): Promise<void>;

  changeUser(options: ConnectionOptions): Promise<void>;

  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;

  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;

  unprepare(sql: string): void;

  end(options?: any): Promise<void>;

  destroy(): void;

  pause(): void;

  resume(): void;

  escape(value: any): string;
  escapeId(value: string): string;
  escapeId(values: string[]): string;

  format(sql: string, values?: any | any[] | { [param: string]: any }): string;
}

export interface PoolConnection extends Connection {
  release(): void;
}

export interface Pool extends EventEmitter {
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions
  ): Promise<[T, FieldPacket[]]>;
  query<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;

  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    sql: string,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions
  ): Promise<[T, FieldPacket[]]>;
  execute<T extends RowDataPacket[][] | RowDataPacket[] | OkPacket | OkPacket[]>(
    options: QueryOptions,
    values: any | any[] | { [param: string]: any }
  ): Promise<[T, FieldPacket[]]>;

  getConnection(): Promise<PoolConnection>;
  on(event: 'connection', listener: (connection: PoolConnection) => any): this;
  on(event: 'acquire', listener: (connection: PoolConnection) => any): this;
  on(event: 'release', listener: (connection: PoolConnection) => any): this;
  on(event: 'enqueue', listener: () => any): this;
  end(): Promise<void>;
}

export function createConnection(connectionUri: string): Promise<Connection>;
export function createConnection(config: ConnectionOptions): Promise<Connection>;
export function createPool(config: PoolOptions): Pool;
