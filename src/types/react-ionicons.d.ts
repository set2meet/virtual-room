/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import React from 'react';

interface IIoniconProps {
  icon: string; // Icon of ionicons. See https://ionicframework.com/docs/ionicons/.
  className?: string; // Name of a CSS class

  // style
  style?: any;
  color?: string; // Icon color. Allow string (blue, red, cyan...), rgb, rgba and hexadecimal colors.
  fontSize?: string; // Icon size. Allow all units (px, em, %, pt...).

  // animation
  shake?: boolean; // Apply beat animation to icon
  beat?: boolean; // Apply shake animation to icon
  rotate?: boolean; // Apply rotate animation to icon

  // functions
  onClick?: (e: any) => any; // Pass a function to execute onClick
}

declare class Ionicon extends React.Component<IIoniconProps> {}

export default Ionicon;
