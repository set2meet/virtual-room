/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
/** Declaration file generated by dts-gen */
import * as Koa from 'koa';

declare module 'koa' {
  interface IRequest {
    body: any;
    rawBody: string;
  }
}

declare function koa2_formidable(opt: any): any;

declare namespace koa2_formidable {
  const prototype: {};
}

export = koa2_formidable;
