/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
declare interface FieldPacket {
  constructor: {
    name: 'FieldPacket';
  };
  catalog: string;
  charsetNr: number;
  db: string;
  decimals: number;
  default: any;
  flags: number;
  length: number;
  name: string;
  orgName: string;
  orgTable: string;
  protocol41: boolean;
  table: string;
  type: number;
  zerofill: boolean;
}

export = FieldPacket;
