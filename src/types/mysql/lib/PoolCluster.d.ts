/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import Connection = require('./Connection');
import PoolConnection = require('./PoolConnection');
import { EventEmitter } from 'events';

declare namespace PoolCluster {
  export interface PoolClusterOptions {
    /**
     * If true, PoolCluster will attempt to reconnect when connection fails. (Default: true)
     */
    canRetry?: boolean;

    /**
     * If connection fails, node's errorCount increases. When errorCount is greater than removeNodeErrorCount,
     * remove a node in the PoolCluster. (Default: 5)
     */
    removeNodeErrorCount?: number;

    /**
     * If connection fails, specifies the number of milliseconds before another connection attempt will be made.
     * If set to 0, then node will be removed instead and never re-used. (Default: 0)
     */
    restoreNodeTimeout?: number;

    /**
     * The default selector. (Default: RR)
     * RR: Select one alternately. (Round-Robin)
     * RANDOM: Select the node by random function.
     * ORDER: Select the first node available unconditionally.
     */
    defaultSelector?: string;
  }
}

declare class PoolCluster extends EventEmitter {
  public config: PoolCluster.PoolClusterOptions;

  public add(config: PoolCluster.PoolClusterOptions): void;
  public add(group: string, config: PoolCluster.PoolClusterOptions): void;

  public end(): void;

  public getConnection(callback: (err: NodeJS.ErrnoException | null, connection: PoolConnection) => void): void;
  public getConnection(
    group: string,
    callback: (err: NodeJS.ErrnoException | null, connection: PoolConnection) => void
  ): void;
  public getConnection(
    group: string,
    selector: string,
    callback: (err: NodeJS.ErrnoException | null, connection: PoolConnection) => void
  ): void;

  public of(pattern: string, selector?: string): PoolCluster;

  public on(event: string, listener: Function): this;
  public on(event: 'remove', listener: (nodeId: number) => void): this;
  public on(event: 'connection', listener: (connection: PoolConnection) => void): this;
}

export = PoolCluster;
