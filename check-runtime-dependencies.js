/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const IORedis = require('ioredis');
const config = require('config');

const redis = new IORedis(config.get('redisConfig'));

let redisIsRunning = false;
redisIsRunning = redis.status === 'connecting';

const redisStatus = redisIsRunning ? 'success' : 'error';

let table = `
-------------------------
| Redis     |  ${redisStatus}  |
-------------------------
`;

console.log('checking runtime dependencies');
console.log(table);

if (!redisIsRunning) {
  throw new Error(`Please start redis server`);
}
