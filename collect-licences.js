/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const fs = require('fs-extra');

const licensesArr = fs.readJsonSync('./dist/public/virtual-room.oss-licenses.json');
const resultObject = {};

licensesArr.forEach((licenceObj) => {
  const license = `${licenceObj.hasOwnProperty('license') ? licenceObj.license : 'MISSING LICENSE'}`;
  resultObject[license] = typeof resultObject[license] === 'undefined' ? 1 : resultObject[license] + 1;
});

fs.writeFileSync('./license-summary.json', JSON.stringify(resultObject));
