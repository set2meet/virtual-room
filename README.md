# Virtual room

## Pre requirements

- nodejs >= 12
- docker
- git

## Local Installation

### Set2Meet Project Structure

```
 s2m
 |-- env-config              - configuration build
 |-- s2m-infra               - docker-compose orchestrator (contains all nessasary services for local development)
 |-- virtual-room            - core app
     |-- config
          |-- default.json   - virtual room config
 |-- recording-capture       - recording service (contains mediaserver)
 |-- recording-processing    - contains processing operations over recordings
 |-- recording-preview       - allows to view recordings
```

### Install Infrastructure

1. `git clone git@gitlab.com:set2meet/s2m-infra.git`
2. `cd s2m-infra`
3. run redis `docker-compose up -d redis`
4. run local aws stack `docker-compose up -d localstack`
5. local keycloak `docker-compose up -d keycloak` (contains necessary users/roles needed for s2m )
   - s2m users: **user1/user1** and **user2/user2**;
   - keycloak admin user: **admin/admin**

### Virtual Room

##### Prepare Config

1. add `127.0.0.1 keycloak` to `C:\Windows\System32\drivers\etc\hosts` file (the similar file also exits somewhere on macos),
   it needs for SSO authentication and default local config uses `keycloak` host instead of `localhost`,
   also don't forget to check the host `ping keycloak`, probably you need to refresh dns `ipconfig /flushdns`

##### Virtual Room

1. `git clone git@gitlab.com:set2meet/virtual-room.git`
2. `git checkout develop`
3. `cd virtual-room`
4. `npm install -g typescript`
5. `npm i`
6. `npm run build`
7. `npm run start:local`
  - **Alternatively** if you don't want to use Vault for _config.json_ generation you can generate it by youself from env-config repo
  - Go to env-config repo
  -   Generate config.json and fill the secret gaps with your own credentials
  -   Put the config.json file into the _config/_ folder inside the _virtual-room_ folder
  -   `npm run dev`
8. https certificate
   - If there are SSL errors in browser console (like NET::ERR_CERT_AUTHORITY_INVALID in Chrome) due to self signed certificates, then
     open each address in next tab and accept all certificates.
   - There are untrusted cert in dev config.
     One can generate trusted certificate and use it for %localhostIP% dev(replace with default one).
     Instruction can be found here: https://github.com/FiloSottile/mkcert.

### Install Recording Service

#### Recording-Capture

1. `git@gitlab.com:set2meet/recording/recording-capture.git`
2. `cd recording-capture`
3. go to _env-config_ repo
4. build **config.json** and add your credentials instead of secrets
5. Put the **config.json** file into the _config/_ folder inside the _recording_ folder
6. run docker with mounted **config.json** file
7. `cd s2m-infra`
8. `docker-compose up -d recording-capture`

#### Recording-Processing

1. `git@gitlab.com:set2meet/recording/recording-processing.git`
2. `cd recording-processing`
3. go to _env-config_ repo
4. build **config.json** and add your credentials instead of secrets
5. Put the **config.json** file into the _config/_ folder inside the _recording_ folder
6. run docker with mounted **config.json** file
7. `cd s2m-infra`
8. `docker-compose up -d recording-processing`

#### Recording-Preview

1. `git@gitlab.com:set2meet/recording/recording-preview.git`
2. `cd recording-processing`
3. go to _env-config_ repo
4. build **config.json** and add your credentials instead of secrets
5. Put the **config.json** file into the _config/_ folder inside the _recording_ folder
6. run docker with mounted **config.json** file
7. `cd s2m-infra`
8. `docker-compose up -d recording-preview`

##### Development

To allow self-signed https - go to both front and backend pages and allow unsafe connections
(i.e. https://localhost:3001/ and https://localhost:3040/)

For sso credentials see confluence or ask tech lead.

Pushing to git:

- Code must be without git errors
- Code must build without errors. In dev mode plugins are taken from the their dev folders (on the same folder level).
  But for build ensure that in node_modules/@competentum are actual dist and index.d.ts plugins files (copy them).

Before the push of any code you must to be sure that build of `virtual-room` works without error, so you should copy your module to `virtual-room` and run build.
If you are working on a module as variant you can use `npm link` in dev mode.
If you are going to commit something in `virtual-room` and you are working with `npm link` you should unlink (just remove `node_modules\@competentum` and/or `npm i`) because currently `npm run build` is not configured well for `npm link`.

## Docker

### Setup

In order to use ssh-key mapping inside docker you should enable buildkit and experemental features using your daemon.json

```json
{
  "experimental": true,
  "features": {
    "buildkit": true
  }
}
```

See https://stackoverflow.com/a/55153182/3388811 for more

### Build virtual-room

Run following from repository folder

```cmd
docker build -t virtual-room:dev --ssh default="~/.ssh/id_rsa" --build
-arg STAND=development --file=Multistage.Dockerfile .
```

where `~\.ssh\id_rsa` is path to your ssh key with access to git@gitlab.com

Use -arg STAND=... for choose environment: local, development, new-production

### Build vr-static

vr-static is nginx image for development and production environment in AWS.
For buld it use command:

```
docker build -t vr-static:dev --ssh default="~/.ssh/id_rsa" --build-arg STAND=development --target=vr-static  --file=Multistage.Dockerfile .
```

### Run

TBD with Dockerfile rework (current doesn't work at all)

## Testing

Tests are performed during ci/cd process.
To run all tests use `npm test`.

Config file : `dev/test/jest.config.js`  
Tests example: [APISampleModule](./src/test/synthetic/APISampleModule), covers following cases:

- container component coverage: story- and non-story- based cases
- redux mocking
- redux: action creators, middleware, reducers coverage
- async rendering
- api and socket mocking

### Snapshot testing

To test your application with storyshots just run the command `npm run test`

If you want to fix/update your storyshots run the command `npm run test -- -u`

If you don't want to include your story to storyshots you can:

- _Preferable way_ add param `storyshots: {disable: true}` for particular part of story

```js
export const Warning = () => (
  <VRButton disabled={false} bsStyle="warning">
    Warning button
  </VRButton>
);

Warning.parameters = {
  storyshots: { disable: true },
};
```

or
add `storyshots: false` into default story parameters

```js
export default {
  title: 'Button',
  component: VRButton,
  parameters: {
    storyshots: false,
  },
};
```

- Set the specific title with `(no-storyshots)`

If you want to test your async Component (with async import) you should use `LoadableContextWrapper` and your Component should use `LoadableWrapper`

All snapshots's files will be placed near his `Component.stories` file.

Config file for storyshots `.storybook/Storyshot.test.tsx`

##Licenses

Gather licenses script and pipeline can fail if values in licenseOverrides section are not in actual state.

To collect licences from other repositories we have to download it (in the pipeline) and use following script:
`node ./dev/aggregate-licenses -p PATH/ -o OUTPUT_PATH/`
You can find the current version that wrapped by npm in package.json
`npm run aggregate-licenses`

##Media server options

### [P2P / Mesh](https://webrtcglossary.com/mesh/)

This is the default and most cheap option which doesn't require a lot of server resources.

##### Procs

- almost free to use
- zero setup

##### Cons

- only for small meetings (<5 users) because of higher client resource usage
- strictly sensitive to client hardware

### [Tokbox / MCU](https://www.vonage.com/communications-apis/?icmp=mainnav_products_communicationsapis#Communications)

This is optional to use proprietary solution we support if you need to add more quality.

##### Procs

- better call quality and user experience (client performance)
- supports large amount of participants (see their docs)

##### Cons

- you need to purchase the license to use it and also pay per each session
- you need to perform some extra setup to legally use it

##### Usage

To use tokbox as your media server backend you need to

- purchase your own license from https://www.vonage.com/
- setup tokbox usage inside config under the `webrtc`
  - specify your API key and secret for `webrtc.tokbox` inside vault
  - add link to `@opentok/client` [v2.17.1](https://static.opentok.com/v2.17.1/js/opentok.min.js) distributive for `webrtc.tokbox.clientBundle`
  - (optional) make it default option using `webrtc.default` or simply switch media server through GUI for concrete room under the `settings -> room -> media server`

###### Tokbox config example

```json
{
  "webrtc": {
    "default": "@PEER_TO_PEER",
    "tokbox": {
      "API_KEY": "SOME_KEY",
      "API_SECRET": "SOME_SECRET",
      "clientBundle": "https://my.bundle.hosting/v2.17.1/js/opentok.min.js"
    }
  }
}
```
