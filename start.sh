#!/bin/bash

APPHOME=/home/ubuntu/set2meet
APPNAME=virtual-room

function info {
 echo "[DEBUG] $*" >&2
}

function die {
 echo "[ERROR] $*" >&2
 exit 1
}

cd ${APPHOME} || die "App home not found"

PID=`ps ax | grep node | grep ${APPNAME} | while read pid rest; do echo $pid; done`
[ -z "${PID}" ] || (kill -9 ${PID} && sleep 1)

node /usr/bin/forever ${APPNAME}/dist/server/server/index.js 2> out.err 1> out.log < /dev/null &

info "Starting"

(timeout -k10s 10s tail -f out.log out.err) || true

ps ax | grep node | grep ${APPNAME}

info "Finished"