#!/bin/bash
find ! -name unpack-restart-vr.sh ! -name config.gz ! -name dist.tar.gz ! -delete
# unpack application
tar -zxvf dist.tar.gz
# unpack config
mkdir config
tar -xvf config.gz -C config
#copy config files
mkdir dist/server/server/config/
mv config/server/* dist/server/server/config/
mkdir dist/public/config
mv config/client/* dist/public/config/
# update nodemon.json file
configExt=js
loggerPrefix="[VR Build] (schell exec):"
loggerErrorPrefix="ERROR ${loggerPrefix}"
nodemonConfigFile=nodemon.json
archiveConfig=config.gz
# check existance of config archive
if [ -e ${archiveConfig} ]
then
  echo "${loggerPrefix} archive found > config"
else
  echo "${loggerErrorPrefix} not found archive ${archiveConfig}"
  exit 1
fi
# find config environment name
serverConfigEnvPath=$(tar tf ${archiveConfig} | grep .js | grep server | grep -v default)
serverConfigEnvName=$(basename "$serverConfigEnvPath" ".${configExt}")
if [ -z "$serverConfigEnvName" ]
then
  echo "${loggerErrorPrefix}] not found server config in ${archiveConfig}"
  exit 1
else
  echo "${loggerPrefix} ts-config-env value is '${serverConfigEnvName}'"
fi
# replace environment value in nodemon.json
sed -i "s,\"NODE_CONFIG_ENV\": \"[^\"]*\",\"NODE_CONFIG_ENV\": \"$serverConfigEnvName\",g" ${nodemonConfigFile}
