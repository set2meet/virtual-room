# syntax=docker/dockerfile:1.0.0-experimental

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

#------------------------------------------
# test
#------------------------------------------
#FROM node:12.18.3-stretch-slim AS test
FROM node:14 AS test

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN npm set unsafe-perm true

# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/workdir/virtual-room
# Copy all required modules and build Virtual-Room
COPY ./ ./
# Copy coverage-summary.old.json from store branch
RUN --mount=type=ssh,id=default \
    git clone -b coverage-log --single-branch git@git.epam.com:epm-s2m/virtual-room.git coverage-tmp && \
    cp -v coverage-tmp/dev/test/coverage/coverage-summary.old.json dev/test/coverage/coverage-summary.old.json && \
    rm -rf coverage-tmp

RUN --mount=type=ssh,id=default npm install
RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git#1.13.0' -- -module virtual-room -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}
ENTRYPOINT []
CMD ["/bin/bash", "-c", "source ./dev/ci/test-ci-script.sh ${COVER_DEGR} ${DEGRAD_PCT}"]

###########################################
# builder
###########################################
FROM node:14 AS builder

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN npm set unsafe-perm true

# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/workdir/virtual-room
# Copy all required modules and build Virtual-Room
COPY ./ ./
RUN --mount=type=ssh,id=default npm install
RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git#1.13.0' -- -module virtual-room -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}
RUN npm run build



###########################################
# vr-static
###########################################
FROM nginx:stable-alpine AS vr-static

ARG STAND

COPY ./docker/conf/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/conf/${STAND}.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/workdir/virtual-room/dist/public/ /usr/share/nginx/set2meet/


###########################################
# VirtualRoom
###########################################
FROM node:14-alpine AS vr

# Install forever
RUN npm i -g forever

WORKDIR /usr/set2meet
# Copy config
COPY  --from=builder /usr/workdir/virtual-room/config ./config/
# Copy Virtual Room files
COPY  --from=builder /usr/workdir/virtual-room/node_modules ./virtual-room/node_modules
COPY  --from=builder /usr/workdir/virtual-room/dist ./virtual-room/dist
COPY  --from=builder /usr/workdir/virtual-room/package.json ./virtual-room/package.json
COPY  --from=builder /usr/workdir/virtual-room/package-lock.json ./virtual-room/package-lock.json
COPY  --from=builder /usr/workdir/virtual-room/nodemon.json ./virtual-room/nodemon.json

EXPOSE 3040
CMD ["/usr/local/bin/forever", "virtual-room/dist/server/server/index.js", "2> out.err", "1> out.log < /dev/null &"]
