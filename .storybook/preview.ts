/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { addDecorator, addParameters } from '@storybook/react';
import ThemeDecorator from './decorators/themeDecorator';
import { withClientIoC } from './decorators/withClientIoC';
import { withLazyResolver } from './decorators/LazyResolver/withLazyResolver';
import withStoryRootIndicator from './decorators/storyRootIndicator';
import { withKnobs } from '@storybook/addon-knobs';
import defaultReduxDecorators from './decorators/withRedux';
import { withServerSocket } from './decorators/socketDecorator';

addParameters({
  options: {
    showRoots: true,
  },
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
});

// Should be added last to indicate story root
addDecorator(withStoryRootIndicator);

addDecorator(withKnobs);

addDecorator(ThemeDecorator);

addDecorator(defaultReduxDecorators);

// WARN: should be called before ioc usage
addDecorator(withClientIoC);

addDecorator(withLazyResolver);

addDecorator(withServerSocket);

addParameters({
  backgrounds: [
    { name: 'storybook', value: '#ffffff' },
    /**
     * @TODO: Need to replace with clientEntitityProvider.getDefaultTheme().backgroundColor
     * (prev value was entities.defaultTheme.backgroundColor, but we can't use it here before ioc initialization)
     */
    { name: 'set2meet', value: 'black', default: true },
    { name: 'twitter', value: '#00aced' },
    { name: 'facebook', value: '#3b5998' },
  ],
});
