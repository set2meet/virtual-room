import React from 'react';
import path from 'path';
import initStoryshots, { Stories2SnapsConverter } from '@storybook/addon-storyshots';
import toJson from 'enzyme-to-json';
import { styleSheetSerializer } from 'jest-styled-components/serializer';
import { addSerializer } from 'jest-specific-snapshot';
import { loadAsyncTree } from '../src/test/utils/storybook';
import { StoryRootIndicator } from './decorators/storyRootIndicator';
import EnzymeMountTracker from '../src/test/utils/EnzymeMountTracker';
import { enzymeCleanup } from '../src/test/utils/utils';

addSerializer(styleSheetSerializer);

// https://github.com/styled-components/jest-styled-components/issues/102
// https://stackoverflow.com/questions/48974583/jest-snapshot-test-failing-due-to-react-navigation-generated-key
Math.random = jest.fn(() => 1);
// tslint:disable-next-line:no-magic-numbers
Date.now = jest.fn(() => 123);
// Mock user agent in order to run tests on different OS
Object.defineProperty(navigator, 'userAgent', {
  get() {
    return 'Mozilla/5.0 (win32) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0';
  },
});

afterEach(enzymeCleanup);

initStoryshots({
  asyncJest: true,
  test: async ({
    story,
    context,
    done, // --> callback passed to test method when asyncJest option is true
  }) => {
    const converter = new Stories2SnapsConverter();
    const snapshotFilename = converter.getSnapshotFileName(context);
    const storyTree = EnzymeMountTracker.mount(story.storyFn({ IoCInitializationOptions: {} }));

    // check if story mounts successfully
    expect(storyTree.exists()).toBeTruthy();

    const tree = await loadAsyncTree(storyTree);

    // check if async tree loads successfully
    expect(tree.exists()).toBeTruthy();

    if (snapshotFilename) {
      // Create path for our stories
      const storyPath = path.resolve(process.cwd(), 'src', snapshotFilename);

      const storyRoot = tree
        .update()
        .find(StoryRootIndicator) // we want to compare the story content without default decorator sequence
        .last();

      // If this snapshot (at this path) exists - compare. If not - create.
      expect(toJson(storyRoot)).toMatchSpecificSnapshot(storyPath);
    }

    done();
  },
  storyKindRegex: /^((?!.*?\(no-storyshots\)).)*$/, // Here we can set restrictions for filenames
});
