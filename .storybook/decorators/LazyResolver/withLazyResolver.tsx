import React from 'react';
import LoadableContextWrapper from './LoadableContextWrapper';

export const withLazyResolver = (Story, context) => {
  return <LoadableContextWrapper>{Story(context)}</LoadableContextWrapper>;
};
