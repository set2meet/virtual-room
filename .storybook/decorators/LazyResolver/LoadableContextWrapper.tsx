import React, { Component } from 'react';
import { LoadableContext } from '../../../src/ioc/client/components/LoadableWrapper/loadableContext';

export interface IContextWrapperProps {
  children: React.ReactNode;
}

interface IContextWrapperState {
  loadablePromises: Array<Promise<React.ReactNode>>;
  pushNewPromise: (promise: Promise<React.Component>) => void;
}

/**
 * Wrapper for storyshot testing that provides React Context for children that contains arr with Promises from
 * loadable components
 */
export default class ContextWrapper extends Component<IContextWrapperProps, IContextWrapperState> {
  constructor(props) {
    super(props);

    this.state = {
      loadablePromises: [],
      pushNewPromise: this.pushNewPromise,
    };
  }

  /**
   * Push new Loader Promise into the state
   * @param promise
   */
  public pushNewPromise = (promise: Promise<React.Component>) => {
    // @ts-ignore
    this.setState((state) => ({
      loadablePromises: [...state.loadablePromises, promise],
    }));
  };

  public shouldComponentUpdate(
    nextProps: Readonly<IContextWrapperProps>,
    nextState: Readonly<IContextWrapperState>,
    nextContext: any
  ): boolean {
    // component shouldn't update on state updates
    return this.state.loadablePromises === nextState.loadablePromises;
  }

  public render() {
    return <LoadableContext.Provider value={this.state}>{this.props.children}</LoadableContext.Provider>;
  }
}
