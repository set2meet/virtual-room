import * as React from 'react';

export const StoryRootIndicator = (props) => props.children;

const withStoryRootIndicator = (Story, context) => <StoryRootIndicator>{Story(context)}</StoryRootIndicator>;

export default withStoryRootIndicator;
