import React from 'react';
import { ModuleRegistryKey } from '../../src/ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import { TDynamicModuleWrapperProps } from '../../src/ioc/client/providers/ModuleProvider/types/IS2MModule';
import { LoadIndicator } from '../common/components/LoadIndicator';
import { ClientEntityProviderContext } from '../../src/ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { IClientEntityProvider } from '../../src/ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';

type TInitialOptionsFactory<Options> = (clientEntityProvider: IClientEntityProvider) => Options;

const moduleDefaultOptions: Record<ModuleRegistryKey, TInitialOptionsFactory<any>> = {
  [ModuleRegistryKey.Webphone]: (clientEntityProvider) => ({
    webRTCServices: clientEntityProvider.getWebRTCServices(),
  }),
  [ModuleRegistryKey.Chat]: (clientEntityProvider) => ({
    actionCreators: clientEntityProvider.getAppActionCreators(),
  }),
};

export const getModuleWrapper = <Options extends {} = undefined>(
  moduleKey: ModuleRegistryKey,
  initialOptionsFactory?: TInitialOptionsFactory<Options>
) => (props) => {
  return (
    <ClientEntityProviderContext.Consumer>
      {({ clientEntityProvider }) => {
        if (!clientEntityProvider) {
          return null;
        }
        const optionsFactory = initialOptionsFactory || moduleDefaultOptions[moduleKey];
        const options = optionsFactory && optionsFactory(clientEntityProvider);

        const Module = clientEntityProvider.resolveModuleSuspended<TDynamicModuleWrapperProps, Options>(
          moduleKey,
          options
        );
        return <Module fallback={<LoadIndicator>{`Loading Module...`}</LoadIndicator>}>{props.children}</Module>;
      }}
    </ClientEntityProviderContext.Consumer>
  );
};

const withModule = <Options extends {}>(
  moduleKey: ModuleRegistryKey,
  initialOptionsFactory?: TInitialOptionsFactory<Options>
) => (Story, context) => {
  const ModuleWrapper = getModuleWrapper<Options>(moduleKey, initialOptionsFactory);

  return <ModuleWrapper>{Story(context)}</ModuleWrapper>;
};

export default withModule;
