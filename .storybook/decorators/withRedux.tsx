/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import React from 'react';
import { AppModuleWrapper } from './appModuleDecorator';
import { RouterModuleWrapper } from './routerDecorator';
import { makeDecorator } from '@storybook/addons';
import {
  IStoreExt,
  IWithReduxWrapperSettings,
  ReduxWrapper,
  TActionsFactory,
  withReduxDecoratorProperties,
} from '@s2m/storybook-addon-redux';
import { IClientEntityProviderInternals } from '../../src/ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { TStore } from '../../src/ioc/client/types/interfaces';
import { Provider } from 'react-redux';
import { ClientEntityProviderContext } from '../../src/ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { getModuleWrapper } from './moduleDecorator';
import { ModuleRegistryKey } from '../../src/ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';

interface IDefaultReduxDecoratorsSettings extends IWithReduxWrapperSettings {
  parameters: {
    actionsFactory?: TActionsFactory;
    enable: boolean;
  };
}

const defaultReduxDecorators = makeDecorator({
  ...withReduxDecoratorProperties,
  wrapper: (getStory, storyContext, settings: IDefaultReduxDecoratorsSettings) => {
    if (!settings.parameters?.enable) {
      return getStory(storyContext);
    }
    const shouldConnectToSocket = !!storyContext.parameters?.serverSocket;
    const EmptyWrapper = (props) => props.children;

    return (
      <ClientEntityProviderContext.Consumer>
        {({ clientEntityProvider }) => {
          if (!clientEntityProvider) {
            return null;
          }
          const reduxWrapperSettings: IWithReduxWrapperSettings = {
            ...settings,
            options: {
              ...settings.options,
              Provider,
              store: (clientEntityProvider as IClientEntityProviderInternals).getStore() as TStore & IStoreExt,
            },
          };
          const ConnectionModuleWrapper = shouldConnectToSocket
            ? getModuleWrapper(ModuleRegistryKey.Connection)
            : EmptyWrapper;
          const AuthModuleWrapper = getModuleWrapper(ModuleRegistryKey.Auth);
          const MediaModuleWrapper = getModuleWrapper(ModuleRegistryKey.Media);
          return (
            <ReduxWrapper {...reduxWrapperSettings}>
              <RouterModuleWrapper>
                <ConnectionModuleWrapper>
                  <AuthModuleWrapper>
                    <AppModuleWrapper>
                      <MediaModuleWrapper>{getStory(storyContext)}</MediaModuleWrapper>
                    </AppModuleWrapper>
                  </AuthModuleWrapper>
                </ConnectionModuleWrapper>
              </RouterModuleWrapper>
            </ReduxWrapper>
          );
        }}
      </ClientEntityProviderContext.Consumer>
    );
  },
});

export default defaultReduxDecorators;
