import React from 'react';
import styled, { css, ThemeProvider } from 'styled-components';
import { IRequiredTheme, IStoreState } from '../../src/ioc/client/types/interfaces';
import { ClientEntityProviderContext } from '../../src/ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';
import { makeDecorator, OptionsParameter, WrapperSettings } from '@storybook/addons';
import { connect } from 'react-redux';
import { Switch } from 'react-router';
import { ComponentRegistryKey } from '../../src/ioc/client/providers/ComponentProvider/types/ComponentRegistryKey';
import HeaderSkeleton from '../../src/modules/app/components/Header/HeaderSkeleton';

export interface IWithThemeDecoratorParams {
  disable?: boolean;
  isHeader?: boolean;
  includeHeader?: boolean;
  centered?: boolean;
}

export interface IWithThemeDecoratorSettings extends WrapperSettings {
  options: OptionsParameter;
  parameters: IWithThemeDecoratorParams;
}

export interface IStyledStorybookAppProps extends IRequiredTheme {
  includeHeader: boolean;
}

const StyledApp = styled.div<IStyledStorybookAppProps>`
  ${({ theme, includeHeader = false }: IStyledStorybookAppProps) => css`
    overflow: hidden;
    height: 100%;
    color: ${theme.fontColor};
    font-family: 'Roboto';
    .s2m-header {
      height: 70px;
    }
    .s2m-body {
      display: inline-block;
      width: 100%;
      height: ${includeHeader ? 'calc(100% - 70px)' : '100%'};
    }
  `}
`;

const Centered = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

// TODO: this is a ConnectedSwitch from routes/App/App.tsx, we should reuse component
const ConnectedSwitch = connect(({ router }: IStoreState) => ({
  location: router.location,
}))(Switch);

const ThemeDecorator = (Story, context, params: IWithThemeDecoratorParams) => {
  return (
    <ClientEntityProviderContext.Consumer>
      {({ clientEntityProvider }) => {
        if (!clientEntityProvider) {
          return null;
        }

        const { defaultTheme } = clientEntityProvider;
        const { UI } = clientEntityProvider.getComponentProvider();
        const Header = clientEntityProvider.resolveComponentSuspended(ComponentRegistryKey.Header, <HeaderSkeleton />);

        return (
          <ThemeProvider theme={defaultTheme}>
            <StyledApp className="s2m-vr" includeHeader={params.includeHeader}>
              <UI.GlobalStyles theme={defaultTheme} />
              {(params.isHeader || params.includeHeader) && (
                <div className="s2m-header">
                  <ConnectedSwitch>{params.isHeader ? Story(context) : <Header />}</ConnectedSwitch>
                </div>
              )}
              {!params.isHeader && (
                <div className="s2m-body">
                  {params.centered ? <Centered>{Story(context)}</Centered> : Story(context)}
                </div>
              )}
            </StyledApp>
          </ThemeProvider>
        );
      }}
    </ClientEntityProviderContext.Consumer>
  );
};

const withThemeDecorator = makeDecorator({
  name: 'withThemeDecorator',
  parameterName: 'themeDecorator',
  skipIfNoParametersOrOptions: false,
  allowDeprecatedUsage: true,
  wrapper: (story, context, { options, parameters }: IWithThemeDecoratorSettings) => {
    return ThemeDecorator(story, context, parameters || {});
  },
});

export default withThemeDecorator;
