import React from 'react';
import { connect } from 'react-redux';

export const connectDecorator = (mapStateToProps?) => (Story, context) => {
  const ConnectedStory = connect(mapStateToProps)(Story.bind(context));
  return <ConnectedStory />;
};
