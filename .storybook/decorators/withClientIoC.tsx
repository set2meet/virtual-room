import React from 'react';
import { getStorybookIoCInitConfig } from '../common/utils/ioc/storybookInitConfig';
import CompositionRoot from '../../src/ioc/client/components/CompositionRoot/CompositionRoot';
import { makeDecorator, OptionsParameter, StoryContext, WrapperSettings } from '@storybook/addons';
import { StoryGetter } from '@storybook/addons/dist/types';
import { TInitializationOptions } from '../../src/ioc/client/types/interfaces';
import { LoadIndicator } from '../common/components/LoadIndicator';

export interface IWithClientIocSettings extends WrapperSettings {
  options: OptionsParameter;
  parameters: TInitializationOptions;
}

/**
 * You can pass IoCInitializationOptions in stories:
 * Component.story = {
 *   parameters: {
 *     IoCInitializationOptions: {
 *       ...,
 *     },
 *   },
 * };
 */
export const withClientIoC = makeDecorator({
  name: 'withClientIoC',
  parameterName: 'IoCInitializationOptions',
  skipIfNoParametersOrOptions: false,
  allowDeprecatedUsage: true,
  wrapper: (Story: StoryGetter, context: StoryContext, { options, parameters }: IWithClientIocSettings) => {
    const IoCInitializationOptions: TInitializationOptions = {
      ...getStorybookIoCInitConfig(),
      ...(parameters || {}),
      ...(context.IoCInitializationOptions || {}),
    };

    // we don't want to call Story outside of render method here because of ioc initialization flow
    const BoundStory = (props) => Story(context);

    return (
      <CompositionRoot
        IoCInitializationOptions={IoCInitializationOptions}
        fallback={<LoadIndicator>{'Loading IoC...'}</LoadIndicator>}
      >
        <BoundStory />
      </CompositionRoot>
    );
  },
});
