import React from 'react';
import { makeDecorator, OptionsParameter, StoryContext } from '@storybook/addons';
import { StoryGetter } from '@storybook/addons/dist/types';
import { createMockServerSocket, TOnMockClientActionCallback } from '../../src/test/utils/serverSocket';
import { STORYBOOK_CONFIG_SERVICE } from '../common/utils/ioc/storybookInitConfig';
import { Server } from 'mock-socket';

export interface IWithServerSocketSettings {
  options: OptionsParameter;
  parameters: {
    onActionCallback: TOnMockClientActionCallback;
  };
}

interface IServerSocketWrapperState {
  mockServer: Server;
}

/**
 * Component.story = {
 *   parameters: {
 *     serverSocket: {
 *       onActionCallback: ...,
 *     },
 *   },
 * };
 */
export const withServerSocket = makeDecorator({
  name: 'withServerSocket',
  parameterName: 'serverSocket',
  skipIfNoParametersOrOptions: true,
  allowDeprecatedUsage: true,
  wrapper: (Story: StoryGetter, context: StoryContext, { parameters }: IWithServerSocketSettings) => {
    const ServerSocketWrapper = class extends React.Component<{}, IServerSocketWrapperState> {
      public componentDidMount() {
        const mockServer = createMockServerSocket(STORYBOOK_CONFIG_SERVICE, parameters?.onActionCallback);
        this.setState({ mockServer });
      }

      public componentWillUnmount(): void {
        this.state.mockServer.stop();
      }

      public render() {
        return <>{this.props.children}</>;
      }
    };

    return <ServerSocketWrapper>{Story(context)}</ServerSocketWrapper>;
  },
});
