import { ModuleRegistryKey } from '../../src/ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import * as React from 'react';
import { TDynamicModuleWrapperProps } from '../../src/ioc/client/providers/ModuleProvider/types/IS2MModule';
import { LoadIndicator } from '../common/components/LoadIndicator';
import { TAppModuleInitOptions } from '../../src/modules/app/types/TAppModuleInitOptions';
import { ClientEntityProviderContext } from '../../src/ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';

export const AppModuleWrapper = (props) => {
  return (
    <ClientEntityProviderContext.Consumer>
      {({ clientEntityProvider }) => {
        if (!clientEntityProvider) {
          return null;
        }

        const AppModule = clientEntityProvider.resolveModuleSuspended<
          TDynamicModuleWrapperProps,
          TAppModuleInitOptions
        >(ModuleRegistryKey.AppModule, {
          config: clientEntityProvider.getConfig(),
          // here we are skipping root component render to be able to connect App dependencies before their splitting
          skipRootMount: true,
        });
        return (
          <AppModule fallback={<LoadIndicator>{'Loading App Module...'}</LoadIndicator>}>{props.children}</AppModule>
        );
      }}
    </ClientEntityProviderContext.Consumer>
  );
};

const withAppModule = (Story, context) => {
  return <AppModuleWrapper>{Story(context)}</AppModuleWrapper>;
};

export default withAppModule;
