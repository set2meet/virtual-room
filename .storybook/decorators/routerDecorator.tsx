import { ModuleRegistryKey } from '../../src/ioc/client/providers/ModuleProvider/types/ModuleRegistryKey';
import * as React from 'react';
import { TDynamicModuleWrapperProps } from '../../src/ioc/client/providers/ModuleProvider/types/IS2MModule';
import { LoadIndicator } from '../common/components/LoadIndicator';
import { TRouterModuleInitOptions } from '../../src/modules/router/TRouterModuleInitOptions';
import { IClientEntityProviderInternals } from '../../src/ioc/client/providers/ClientEntityProvider/types/IClientEntityProvider';
import { ClientEntityProviderContext } from '../../src/ioc/client/providers/ClientEntityProvider/context/ClientEntityProviderContext';

export const RouterModuleWrapper = ({ children }) => {
  return (
    <ClientEntityProviderContext.Consumer>
      {({ clientEntityProvider }) => {
        if (!clientEntityProvider) {
          return null;
        }

        const RouterModule = clientEntityProvider.resolveModuleSuspended<
          TDynamicModuleWrapperProps,
          TRouterModuleInitOptions
        >(ModuleRegistryKey.RouterModule, {
          history: (clientEntityProvider as IClientEntityProviderInternals).getHistory(),
        });
        return (
          <RouterModule fallback={<LoadIndicator>{'Loading Router Module...'}</LoadIndicator>}>{children}</RouterModule>
        );
      }}
    </ClientEntityProviderContext.Consumer>
  );
};

const withRouter = (Story, context) => {
  return <RouterModuleWrapper>{Story(context)}</RouterModuleWrapper>;
};

export default withRouter;
