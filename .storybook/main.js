/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
const buildConfig = require('../dev/webpack.config.js');

module.exports = {
  stories: ['../src/**/*.stories.(tsx|mdx)'],
  addons: [
    '@storybook/addon-storysource', // cause bug with TS < 3.6.6
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-docs',
    '@storybook/addon-viewport',
    '@storybook/addon-knobs',
    '@storybook/addon-backgrounds',
    '@s2m/storybook-addon-redux',
  ],
  webpackFinal: async (config) => {
    const custom = await buildConfig();

    config.module.rules = config.module.rules.filter(
      (rule) => rule.test && !rule.test.toString().includes('woff') && !rule.test.toString().includes('mp3')
    );
    config.module.rules.push(...custom.module.rules);
    config.resolve.extensions.push(...custom.resolve.extensions);
    config.plugins = (config.plugins || []).concat(custom.plugins);
    config.externals = {
      ...custom.externals,
      ...config.externals,
    };
    return config;
  },
};
