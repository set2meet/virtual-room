import styled from 'styled-components';
import * as React from 'react';

export interface ILoadIndicatorProps {
  children?: string;
}

const LoadContainer = styled.div`
  font-size: 1.5em;
  color: white;
  position: absolute;
  top: 50%;
  left: 50%;
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);

  .indicatorBody {
    animation: gentleBlink 5s linear infinite;
  }

  @keyframes gentleBlink {
    25% {
      opacity: 0;
    }

    50% {
      opacity: 1;
    }

    100% {
      opacity: 1;
    }
  }
`;

// we can't rely on codebase spinner because of dependency load order, so use it's simplified version for decorator loading indication
export function LoadIndicator(props: ILoadIndicatorProps) {
  return (
    <LoadContainer>
      <div className={'indicatorBody'}>{props.children || 'loading...'}</div>
    </LoadContainer>
  );
}
