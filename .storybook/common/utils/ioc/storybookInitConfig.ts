/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { set as _set } from 'lodash';
import resolveUrl from 'resolve-url';
import { IClientConfig, IAppConfigService } from '../../../../src/ioc/common/ioc-interfaces';
import buildClientConfig from '../../../../src/server/config/clientConfig';
import { withReduxEnhancer } from '@s2m/storybook-addon-redux';
import { StoreEnhancer } from 'redux';
import { TInitializationOptions } from '../../../../src/ioc/client/types/interfaces';
import clientEntityProvider from '../../../../src/ioc/client/providers/ClientEntityProvider/ClientEntityProvider';
import { SocketIO } from 'mock-socket';
import { createMemoryHistory } from 'history';
import { MemoryHistoryBuildOptions } from 'history/createMemoryHistory';
import buildServerConfig from '../../../../src/server/config/serverConfig';
import TServerConfig from '../../../../src/server/config/types/TServerConfig';

export const STORYBOOK_CONFIG_SERVICE: Readonly<IAppConfigService> = {
  url: `/`,
  path: '/api/v1/',
};

// WARN: make sure not to use here any test env dependencies incompatible with storybook entry point
// @see webpack config for more
export const getStorybookIoCInitConfig = (): TInitializationOptions => ({
  configFactory: async () => {
    const config = buildClientConfig(
      process.env.NODE_CONFIG ? ((process.env.NODE_CONFIG as unknown) as TServerConfig) : await buildServerConfig()
    );

    return Promise.resolve({
      ...config,
      service: STORYBOOK_CONFIG_SERVICE,
      logger: {
        saveReduxActions: false,
      },
    } as IClientConfig);
  },
  historyFactory: (options?: MemoryHistoryBuildOptions) => createMemoryHistory(options),
  // @TODO: current withReduxEnhancer redux version is incompatible with VR one, we need to update it
  storeEnhancerFactory: () => [(withReduxEnhancer as unknown) as StoreEnhancer],
  socketFactory: async (config) => {
    const remoteUrl = resolveUrl(config.service.url, config.service.path);
    const socket = (SocketIO(remoteUrl) as unknown) as SocketIOClient.Socket;
    socket.connect = (): SocketIOClient.Socket => socket;
    socket.id = '1';
    _set(socket, 'io.opts.query', {});
    return socket;
  },
  onDestroyStart: () => {
    clientEntityProvider.getStorageManager().removeAll();
  },
});
